/* 
 * GPIODev                                       gpiodev_mod.c
 *
 * Copyright (C) 2006-2008 Motorola, Inc.
 */

/*
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 05-Oct-2006  Motorola    Initial revision.
 * 03-Nov-2006  Motorola    Adjusted ArgonLV-related defines.
 * 26-Nov-2006  Motorola    Added support for LJ7.1 GPU signals.
 * 20-Jan-2007  Motorola    Export pin names from gpiodev_pins.h
 * 26-Jan-2007  Motorola    Bluetooth current drain improvements.
 * 20-Aug-2007  Motorola    Removed Marco specific signal code in relation to xPIXL.
 * 17-Apr-2008  Motorola    Adjusted Nevis-related defines.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/fcntl.h>
#include <linux/errno.h>
#include <linux/poll.h>
#include <linux/interrupt.h>
#include <asm/uaccess.h>
#include <asm/irq.h>
#include <asm/mot-gpio.h>
#include <asm/arch/gpio.h>
#include <linux/autoconf.h>

#ifdef CONFIG_MOT_FEAT_BRDREV
#include <asm/boardrev.h>
#endif /* CONFIG_MOT_FEAT_BRDREV */

#include "linux/gpiodev.h"

/* declared in gpiodev_pins.c */
extern const iomux_pin_name_t gpiodev_iomux_pin[MAX_GPIODEV_IOMUX_GIPIN];

MODULE_DESCRIPTION("GPIODev creates an interface to GPIO through user space");
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Motorola, Inc.");


#define GPIODEV_NAME  "gpiodev"
#define BLUETOOTHDIR    "bluetooth"

/**
 * Convert a GPIODEV_GPIOINTTYPE_* value to a value suitable for use with
 * gpio_config.
 */
#define CONFIG_INTTYPE_TO_FSL_INTTYPE(cfg) (cfg>>1)


/**
 * Convert a GPIODEV_EDIOINTTYPE_* value to an IRQT_* value for use with
 * set_irq_type.
 */
#define EDIOINTTYPE_TO_IRQT(cfg) \
    (((cfg&GPIODEV_INTERRUPT_MASK)==GPIODEV_EDIOINTTYPE_LLEV) ? IRQT_LOW : \
    ((cfg&GPIODEV_INTERRUPT_MASK)==GPIODEV_EDIOINTTYPE_REDG) ? IRQT_RISING : \
    ((cfg&GPIODEV_INTERRUPT_MASK)==GPIODEV_EDIOINTTYPE_FEDG) ? IRQT_FALLING : \
    IRQT_BOTHEDGE)


/*
 * GPIODev configuration value representing an inoperable state
 */
#define GPIODEV_INVALID_CONFIG 0xFF


/*
 * GPIODev state flags
 */
#define GPIODEV_OPEN           0x01
#define GPIODEV_LOWLEVELACCESS 0x02
#define GPIODEV_CONFIGURED     0x04
#define GPIODEV_RETAINSTATE    0x08

/*
 * GPIODEVICE defines a GPIO
 *     signal - index into the initial_gpio_settings array;
 *              if set to a valid value, this will override the pin and
 *              signo attributes in the data structure
 *     pin - pin number of the GPIO
 *     signo - the pin number relative to its port
 *     port - the port the pin belongs to
 *     device_name - name which shows up in /dev
 *     iomux_out_function - corresponding function used by the iomux
 *     iomux_in_function - corresponding function used by the iomux
 *     static_config - the default configuration of a GPIO
 *     current_config - the current configuration the GPIO is in
 *     init_config - disposition of the GPIO direction register at init
 *     flags -  status of the device node (see GPIODev state flags above)
 *     interrupt - true if there is a pending interrupt; false otherwise
 *     event_queue - queue to sleep on while waiting for an interrupt
 */
typedef struct GPIODEVICE
{
    enum gpio_signal  signal;
    enum gpiodev_iomux_pins pin;
    unsigned char     signo;
    unsigned int      port;
    char*             device_name;
    unsigned char     iomux_out_function;
    unsigned char     iomux_in_function;
    unsigned char     static_config;
    unsigned char     current_config;
    unsigned char     init_config;
    unsigned char     flags;
    unsigned char     interrupted;
    __u32             (*get_data)(void);
    void              (*set_data)(__u32 data);
    wait_queue_head_t event_queue;
}GPIODEVICE;

typedef struct EDIODEVICE
{
    unsigned char     pin;
    char*             device_name;
    unsigned long     irq;
    unsigned char     static_config;
    unsigned char     current_config;
    unsigned char     flags;
    unsigned char     interrupted;
    wait_queue_head_t event_queue;
}EDIODEVICE;


static unsigned long ConfigureGPIO (GPIODEVICE*, unsigned char);
static unsigned long ConfigureEDIO (EDIODEVICE*, unsigned char);


/*
 * gpio_devs[] is the actual description of each GPIO to be supported
 */
GPIODEVICE gpio_devs[] = {
    {
        GPIO_SIGNAL_BT_POWER,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        BLUETOOTHDIR "/power",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        gpio_bluetooth_power_get_data,
        gpio_bluetooth_power_set_data
    },
    {
        GPIO_SIGNAL_BT_WAKE_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        BLUETOOTHDIR "/btwake",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
#if defined(CONFIG_MACH_SCMA11REF) || defined(CONFIG_MACH_ARGONLVPHONE)
    {
        GPIO_SIGNAL_GPS_RESET,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gps_reset",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED,
        0,
        NULL,
        NULL
    },
#endif /* CONFIG_MACH_SCMA11REF || CONFIG_MACH_ARGONLVPHONE */
    {
        GPIO_SIGNAL_CAM_RST_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "camera_reset_b",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#if defined(CONFIG_MACH_SCMA11PHONE)
    {
        GPIO_SIGNAL_CAM_PD,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "camera_ext_pwrdn",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#ifndef CONFIG_MACH_XPIXL
    {
        GPIO_SIGNAL_SER_EN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "ser_en",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#endif
#if defined(CONFIG_MACH_NEVIS)    
    {
        GPIO_SIGNAL_SER_RST_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "display1_sreset",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#else
    {
        GPIO_SIGNAL_DISP_SD,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
       "display1_sd",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#endif /*CONFIG_MACH_NEVIS*/    
#endif /* CONFIG_MACH_SCMA11PHONE */
    {
        GPIO_SIGNAL_DISP_RST_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "display1_reset",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#if defined(CONFIG_MACH_ARGONLVPHONE)
    {
        GPIO_SIGNAL_LCD_SD,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "display1_sd",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_CLI_RST_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "display2_reset",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_SERDES_RESET_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "ser_en",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_GPS_PWR_EN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gps_pwr_en",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED,
        0
    },
    {
        GPIO_SIGNAL_BT_HOST_WAKE_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        BLUETOOTHDIR "/hostwake",
        0,
        0,
        GPIODEV_INPUT|GPIODEV_INT_DISABLED,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_INPUT|GPIODEV_INT_DISABLED,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
    {
        GPIO_SIGNAL_GPU_DPD_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_dpd_b",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
    {
        GPIO_SIGNAL_GPU_RESET_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_reset_b",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
    {
        GPIO_SIGNAL_APP_CLK_EN_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "app_clk_en_b",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
    {
        GPIO_SIGNAL_GPU_VCORE1_EN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_vcore1_en",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
    {
        GPIO_SIGNAL_GPU_VCORE2_EN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_vcore2_en",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0,
        NULL,
        NULL
    },
    {
        GPIO_SIGNAL_CAM_EXT_PWRDN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "camera_ext_pwrdn",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_CAM_INT_PWRDN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "camera_int_pwrdn",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_GPU_DPD,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_dpd",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_GPU_RESET,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_reset",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_GPU_CORE1_EN,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_core1_en",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_GPU_A2,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_a2",
        0,
        0,
        GPIODEV_OUTPUT,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_OUTPUT,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
    {
        GPIO_SIGNAL_GPU_INT_B,
        INVALID_GPIODEV_IOMUX_GIPIN,
        0,
        GPIO_INVALID_PORT,
        "gpu_int_b",
        0,
        0,
        GPIODEV_INPUT|GPIODEV_INT_DISABLED,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_INPUT|GPIODEV_INT_DISABLED,
        GPIODEV_CONFIGURED|GPIODEV_RETAINSTATE,
        0
    },
#endif /* CONFIG_MACH_ARGONLVPHONE */
    {
        MAX_GPIO_SIGNAL,
        0, 
        0, 
        0, 
        "gpio_raw",
        0,
        0,
        0,
        0,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_LOWLEVELACCESS,
        0,
        NULL,
        NULL
    }
};

#define GPIO_DEVICE_COUNT   (sizeof(gpio_devs)/sizeof(gpio_devs[0]))


EDIODEVICE edio_devs[] = {
#if defined(CONFIG_ARCH_MXC91231)
    {
        ED_INT2,
        "bp_panic",
        INT_EXT_INT2,
        GPIODEV_INPUT|GPIODEV_INT_ENABLED|GPIODEV_EDIOINTTYPE_FEDG,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_CONFIGURED,
        0
    },
    {
        ED_INT1,
        BLUETOOTHDIR "/hostwake",
        INT_EXT_INT1,
        GPIODEV_INPUT|GPIODEV_INT_DISABLED,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_CONFIGURED,
        0
    },
#endif /* CONFIG_ARCH_MXC91231 */
    {
        0,
        "edio_raw",
        0,
        0,
        GPIODEV_INVALID_CONFIG,
        GPIODEV_LOWLEVELACCESS,
        0
    }
};

#define EDIO_DEVICE_COUNT   (sizeof(edio_devs)/sizeof(edio_devs[0]))


static struct file_operations gpiodev_fops  = {0};
static struct file_operations ediodev_fops  = {0};
static unsigned long          gpiodev_major = 0;
static unsigned long          ediodev_major = 0;


/*
 * GPIODev_ISR handles an interrupt on a GPIO triggered by the parameters
 * provided when setting up the line. The interrupt that occurs here will
 * be for a specific GPIO previously configured for a particular interrupt.
 * The function will wake up any process waiting for this interrupt to be 
 * triggered.
 */
static irqreturn_t GPIODev_ISR (int irq, void* param, struct pt_regs* regs)
{
    GPIODEVICE* dev;

    dev = (GPIODEVICE*)param;

    dev->interrupted = 1;

    disable_irq(MXC_GPIO_TO_IRQ(GPIO_PORT_SIG_TO_MXC_GPIO(
                    dev->port, dev->signo)));
    wake_up_interruptible(&dev->event_queue);

    return IRQ_HANDLED;
}

/*
 * EDIODev_ISR handles an interrupt on a EDIO triggered by the parameters
 * provided when setting up the line. The interrupt that occurs here will
 * be for a specific EDIO previously configured for a particular interrupt.
 * The function will wake up any process waiting for this interrupt to be 
 * triggered.
 */
static irqreturn_t EDIODev_ISR (int irq, void* param, struct pt_regs* regs)
{
    EDIODEVICE* dev;

    dev = (EDIODEVICE*)param;

    dev->interrupted = 1;

    disable_irq(dev->irq);
    wake_up_interruptible(&dev->event_queue);

    return IRQ_HANDLED;
}

/*
 * ConfigureGPIO configures the way a GPIO operates and adjusts the 
 * current configuration byte appropriately. Any resources in use
 * prior to the reconfiguration will be released and the new configuration
 * will take effect.
 */
static unsigned long ConfigureGPIO (GPIODEVICE* dev, unsigned char newconfig)
{
    unsigned long result;

    /* Free any currently consumed resources */
    if((dev->current_config&GPIODEV_INTENABLE_MASK) && (dev->current_config != GPIODEV_INVALID_CONFIG))
    {
        disable_irq(MXC_GPIO_TO_IRQ(GPIO_PORT_SIG_TO_MXC_GPIO(dev->port,
                        dev->signo)));
        gpio_free_irq(
                      dev->port, 
                      dev->signo, 
                      GPIO_LOW_PRIO
                     );

        dev->current_config = dev->current_config&GPIODEV_DIRECTION_MASK;
        dev->interrupted    = 0;
    }

    /* Change to the desired configuration */
    if(newconfig == GPIODEV_INVALID_CONFIG)
    {
        gpio_config(
                    dev->port, 
                    dev->signo, 
                    GPIODEV_INPUT, 
                    GPIO_INT_NONE
                   );
    }
    else if(newconfig&GPIODEV_INTENABLE_MASK)
    {
        result = gpio_request_irq(
                                  dev->port, 
                                  dev->signo, 
                                  GPIO_LOW_PRIO, 
                                  GPIODev_ISR, 
                                  0, 
                                  GPIODEV_NAME, 
                                  dev
                                 );
        if(result)
            return -EBUSY;

        gpio_config(
                    dev->port, 
                    dev->signo, 
                    newconfig&GPIODEV_DIRECTION_MASK, 
                    CONFIG_INTTYPE_TO_FSL_INTTYPE(newconfig&GPIODEV_INTERRUPT_MASK)
                   );

        enable_irq(MXC_GPIO_TO_IRQ(GPIO_PORT_SIG_TO_MXC_GPIO(dev->port,
                        dev->signo)));
    }
    else
    {
        gpio_config(
                    dev->port, 
                    dev->signo, 
                    newconfig&GPIODEV_DIRECTION_MASK, 
                    GPIO_INT_NONE
                   );
    }

    dev->current_config = newconfig;

    return 0;
}

/*
 * ConfigureEDIO configures the way a EDIO operates and adjusts the 
 * current configuration byte appropriately. Any resources in use
 * prior to the reconfiguration will be released and the new configuration
 * will take effect.
 */
static unsigned long ConfigureEDIO (EDIODEVICE* dev, unsigned char newconfig)
{
    unsigned long result;

    /* Free any currently consumed resources */
    if((dev->current_config&GPIODEV_INTENABLE_MASK) && (dev->current_config != GPIODEV_INVALID_CONFIG))
    {
        disable_irq(dev->irq);
        free_irq(dev->irq, dev);

        dev->current_config = dev->current_config&GPIODEV_DIRECTION_MASK;
        dev->interrupted    = 0;
    }

    /* Change to the desired configuration */
    if((newconfig != GPIODEV_INVALID_CONFIG)
            && (newconfig&GPIODEV_INTENABLE_MASK))
    {
        set_irq_type(dev->irq,
                EDIOINTTYPE_TO_IRQT(newconfig&GPIODEV_INTERRUPT_MASK));
        result = request_irq(dev->irq, &EDIODev_ISR, SA_INTERRUPT,
                dev->device_name, dev);
        if(result)
            return -EBUSY;
    }

    dev->current_config = newconfig;

    return 0;
}


/*
 * GPIODev_Open handles a userspace open() to our driver. This function takes
 * the specified device out of the default state by configuring the IOMUX
 * to the appropriate state and configuring the GPIO.
 */
static int GPIODev_Open (struct inode* inode, struct file* filp)
{
    GPIODEVICE*   dev;
    unsigned long minor;
    unsigned long result;    

    minor = MINOR(inode->i_rdev);

    if(minor >= GPIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &gpio_devs[minor];
    if(dev->flags&GPIODEV_OPEN)
        return -EBUSY;

    if(dev->flags&GPIODEV_CONFIGURED && !(dev->flags&GPIODEV_RETAINSTATE))
    {
        if(IS_VALID_GPIODEV_IOMUX_GIPIN(dev->pin)) {
            iomux_config_mux(gpiodev_iomux_pin[dev->pin],
                    dev->iomux_out_function, dev->iomux_in_function);
        }
        result = ConfigureGPIO(dev, dev->static_config);
        if(result < 0)
        {
#ifdef CONFIG_ARCH_MXC91231
            if(IS_VALID_GPIODEV_IOMUX_GIPIN(dev->pin)) {
                iomux_config_mux(gpiodev_iomux_pin[dev->pin],
                        OUTPUTCONFIG_DEFAULT, INPUTCONFIG_NONE);
            }
#endif
            return result;
        }
    }

    dev->flags |= GPIODEV_OPEN;

    return 0;
}

/*
 * EDIODev_Open handles a userspace open() to our driver. This function takes
 * the specified device out of the default state by configuring the IOMUX
 * to the appropriate state and configuring the EDIO.
 */
static int EDIODev_Open (struct inode* inode, struct file* filp)
{
    EDIODEVICE*   dev;
    unsigned long minor;
    unsigned long result;    

    minor = MINOR(inode->i_rdev);

    if(minor >= EDIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &edio_devs[minor];
    if(dev->flags&GPIODEV_OPEN)
        return -EBUSY;

    if(dev->flags&GPIODEV_CONFIGURED && !(dev->flags&GPIODEV_RETAINSTATE))
    {
        result = ConfigureEDIO(dev, dev->static_config);
        if(result < 0)
            return result;
    }

    dev->flags |= GPIODEV_OPEN;

    return 0;
}

/*
 * GPIODev_Close handles a userspace close() on a device previously
 * opened. The close reverses any initialization done on a device.
 */
static int GPIODev_Close (struct inode* inode, struct file* filp)
{
    GPIODEVICE*   dev;
    unsigned long minor;

    minor = MINOR(inode->i_rdev);

    if(minor >= GPIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &gpio_devs[minor];

    if(!(dev->flags&GPIODEV_OPEN))
        return 0;

    dev->flags ^= GPIODEV_OPEN;

    if((dev->flags&GPIODEV_CONFIGURED) && !(dev->flags&GPIODEV_RETAINSTATE))
    {
        ConfigureGPIO(dev, GPIODEV_INVALID_CONFIG);

#ifdef CONFIG_ARCH_MXC91231
        if(IS_VALID_GPIODEV_IOMUX_GIPIN(dev->pin)) {
            iomux_config_mux(gpiodev_iomux_pin[dev->pin], OUTPUTCONFIG_DEFAULT,
                    INPUTCONFIG_NONE);
        }
#endif
    }

    return 0;
}

/*
 * EDIODev_Close handles a userspace close() on a device previously
 * opened. The close reverses any initialization done on a device.
 */
static int EDIODev_Close (struct inode* inode, struct file* filp)
{
    EDIODEVICE*   dev;
    unsigned long minor;

    minor = MINOR(inode->i_rdev);

    if(minor >= EDIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &edio_devs[minor];

    if(!(dev->flags&GPIODEV_OPEN))
        return 0;

    dev->flags ^= GPIODEV_OPEN;

    if((dev->flags&GPIODEV_CONFIGURED) && !(dev->flags&GPIODEV_RETAINSTATE))
        ConfigureEDIO(dev, GPIODEV_INVALID_CONFIG);

    return 0;
}


/*
 * GPIODev_Read handles a userspace read() on an open device. 
 * The read will only read a single byte. A read on a GPIO
 * configured for output will return the value currently in the output
 * register.
 */
static ssize_t GPIODev_Read (
                             struct file* filp, 
                             char* buf, 
                             size_t count, 
                             loff_t* f_pos
                            )
{
    GPIODEVICE*   dev;
    unsigned long minor;
    unsigned long result;
    unsigned char value;

    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    if(minor >= GPIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &gpio_devs[minor];
    if(!(dev->flags&GPIODEV_OPEN) || !(dev->flags&GPIODEV_CONFIGURED))
        return -EBADF;

    if(count < 1)
        return 0;

    if(dev->get_data) {
        value = (dev->get_data)();
    } else {
        value = gpio_get_data(dev->port, dev->signo);
    }

    result = copy_to_user(buf, &value, 1);
    if(result)
        return -EFAULT;

    return 1;
}

/*
 * EDIODev_Read handles a userspace read() on an open device. 
 * The read will only read a single byte. A read on a EDIO
 * configured for output will return the value currently in the output
 * register.
 */
static ssize_t EDIODev_Read (
                             struct file* filp, 
                             char* buf, 
                             size_t count, 
                             loff_t* f_pos
                            )
{
    EDIODEVICE*   dev;
    unsigned long minor;
    unsigned long result;
    unsigned char value;

    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    if(minor >= EDIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &edio_devs[minor];
    if(!(dev->flags&GPIODEV_OPEN) || !(dev->flags&GPIODEV_CONFIGURED))
        return -EBADF;

    if(count < 1)
        return 0;

    value = edio_get_data(dev->pin);

    result = copy_to_user(buf, &value, 1);
    if(result)
        return -EFAULT;

    return 1;
}

/*
 * GPIODev_Write handles a userspace write() on an open device. The write
 * must be one byte in length or the call will fail. A write on a GPIO
 * configured for input will not affect the line's status.
 */
static ssize_t GPIODev_Write (
                              struct file* filp, 
                              const char* buf, 
                              size_t count, 
                              loff_t* f_pos
                             )
{
    GPIODEVICE*   dev;
    unsigned long minor;
    unsigned long result;
    unsigned char value;

    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    if(minor >= GPIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &gpio_devs[minor];

    if(!(dev->flags&GPIODEV_OPEN) || !(dev->flags&GPIODEV_CONFIGURED))
        return -EBADF;

    if((dev->current_config&GPIODEV_DIRECTION_MASK) != GPIODEV_OUTPUT)
        return -EINVAL;

    if(count < 1)
        return 0;

    value = 0;

    result = copy_from_user(&value, buf, 1);
    if(result)
        return -EFAULT;

    if(value > 1)
        return -EINVAL;

    if(dev->set_data) {
        (dev->set_data)(value);
    } else {
        gpio_set_data(dev->port, dev->signo, value);
    }

    return 1;
}

/*
 * EDIODev_Write handles a userspace write() on an open device. The write
 * must be one byte in length or the call will fail. A write on a EDIO
 * configured for input will not affect the line's status.
 */
static ssize_t EDIODev_Write (
                              struct file* filp, 
                              const char* buf, 
                              size_t count, 
                              loff_t* f_pos
                             )
{
    EDIODEVICE*   dev;
    unsigned long minor;
    unsigned long result;
    unsigned char value;

    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    if(minor >= EDIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &edio_devs[minor];

    if(!(dev->flags&GPIODEV_OPEN) || !(dev->flags&GPIODEV_CONFIGURED))
        return -EBADF;

    if((dev->current_config&GPIODEV_DIRECTION_MASK) != GPIODEV_OUTPUT)
        return -EINVAL;

    if(count < 1)
        return 0;

    value = 0;

    result = copy_from_user(&value, buf, 1);
    if(result)
        return -EFAULT;

    if(value > 1)
        return -EINVAL;

    edio_set_data(dev->pin, value);

    return 1;
}


/*
 * GPIODev_IOCtl handles the configuration of a GPIO. The following commands are
 * supported:
 * GPIODEV_RETAIN_STATE - ensures a device will retain its state upon close
 * GPIODEV_REVERT_STATE - ensures a device will revert back to a default value upon close
 * GPIODEV_GET_CONFIG - returns the 1 byte configuration of the GPIO
 * GPIODEV_SET_CONFIG - sets up the 1 byte configuration of the GPIO
 * GPIODEV_INT_REENABLE - If the line is responsive to interrupts, ints will be
 *                        reenabled.
 * GPIODEV_GET_LOWLEVELCONFIG - Get the low level details of a device. If the device doesn't
 *                              allow this operation an error is returned.
 * GPIODEV_SET_LOWLEVELCONFIG - Set the low level details of a device. If the device
 *                              doesn't allow this operation an error is returned. If
 *                              the low level configuration to be set is invalid an
 *                              error is returned.
 *
 * The configuration byte is in the following format:
 *     Bit 1   - Direction (1-output 0-input)
 *     Bit 2-3 - Interrupt method
 *     Bit 4   - Interrupts disabled (0-disabled 1-enabled)
 */
static int GPIODev_IOCtl (
                          struct inode* inode, 
                          struct file* filp, 
                          unsigned int cmd, 
                          unsigned long arg
                         )
{
    GPIODEV_LOWLEVEL_CONFIG llconf;
    GPIODEVICE*             dev;
    unsigned long           minor;
    unsigned long           result;
    unsigned char           value;

    minor = MINOR(inode->i_rdev);

    if(minor >= GPIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &gpio_devs[minor];
    if(!(dev->flags&GPIODEV_OPEN))
        return -EBADF;

    result = 0;

    switch(cmd)
    {
        case GPIODEV_GET_CONFIG:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            result = copy_to_user((unsigned char*)arg, &dev->current_config, 1);
            if(result)
                return -EFAULT;
        }break;

        case GPIODEV_SET_CONFIG:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            result = copy_from_user(&value, (unsigned char*)arg, 1);
            if(result)
                return -EFAULT;

            if(((value&GPIODEV_DIRECTION_MASK) != GPIODEV_INPUT) && (value&GPIODEV_INTENABLE_MASK))
                return -EINVAL;

            result = ConfigureGPIO(dev, value);

            return result;
        }break;

        case GPIODEV_INT_REENABLE:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            if(!(dev->current_config&GPIODEV_INTENABLE_MASK))
                return -EINVAL;

            dev->interrupted = 0;
            enable_irq(MXC_GPIO_TO_IRQ(GPIO_PORT_SIG_TO_MXC_GPIO(
                            dev->port, dev->signo)));
        }break;

        case GPIODEV_GET_LOWLEVELCONFIG:
        {
            if(!(dev->flags&GPIODEV_LOWLEVELACCESS))
                return -EINVAL;

            llconf.pin                   = dev->pin;
            llconf.signo                 = dev->signo;
            llconf.port                  = dev->port;
            llconf.irq                   = 0;
            llconf.iomux_input_function  = dev->iomux_in_function;
            llconf.iomux_output_function = dev->iomux_out_function;
            llconf.static_config         = dev->static_config;

            result = copy_to_user((unsigned int*)arg, &llconf, sizeof(GPIODEV_LOWLEVEL_CONFIG));
            if(result)
                return -EFAULT;
        }break;

        case GPIODEV_SET_LOWLEVELCONFIG:
        {
            if(!(dev->flags&GPIODEV_LOWLEVELACCESS))
                return -EINVAL;

            result = copy_from_user(&llconf, (unsigned int*)arg, sizeof(GPIODEV_LOWLEVEL_CONFIG));
            if(result)
                return -EFAULT;

            if(((llconf.static_config&GPIODEV_DIRECTION_MASK) != GPIODEV_INPUT) && (llconf.static_config&GPIODEV_INTENABLE_MASK))
                return -EINVAL;

            if(dev->flags&GPIODEV_CONFIGURED)
                ConfigureGPIO(dev, GPIODEV_INVALID_CONFIG);

            dev->pin                = llconf.pin;
            dev->signo              = llconf.signo;
            dev->port               = llconf.port;
            dev->iomux_in_function  = llconf.iomux_input_function;
            dev->iomux_out_function = llconf.iomux_output_function;
            dev->static_config      = llconf.static_config;

            dev->flags |= GPIODEV_CONFIGURED;

            result = ConfigureGPIO(dev, dev->static_config);
            if(result) {
                return result;
            }

            /* configure the iomux settings if they've set the pin */
            if(IS_VALID_GPIODEV_IOMUX_GIPIN(dev->pin)) {
                iomux_config_mux(gpiodev_iomux_pin[dev->pin],
                        dev->iomux_out_function,
                        dev->iomux_in_function);
            }
        }break;

        case GPIODEV_RETAIN_STATE:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            dev->flags |= GPIODEV_RETAINSTATE;
        }break;

        case GPIODEV_REVERT_STATE:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            dev->flags &= ~GPIODEV_RETAINSTATE;
        }break;

        default:return -EINVAL;
    }

    return 0;
}

/*
 * EDIODev_IOCtl handles the configuration of a GPIO. The following commands are
 * supported:
 * GPIODEV_RETAIN_STATE - ensures a device will retain its state upon close
 * GPIODEV_REVERT_STATE - ensures a device will revert back to its default state upon close
 * GPIODEV_GET_CONFIG - returns the 1 byte configuration of the EDIO
 * GPIODEV_SET_CONFIG - sets up the 1 byte configuration of the EDIO
 * GPIODEV_INT_REENABLE - If the line is responsive to interrupts, the int will
 *                        be reenabled
 * GPIODEV_GET_LOWLEVELCONFIG - Get the low level details of a device. If the device doesn't
 *                              allow this operation an error is returned.
 * GPIODEV_SET_LOWLEVELCONFIG - Set the low level details of a device. If the device
 *                              doesn't allow this operation an error is returned. If
 *                              the low level configuration to be set is invalid an
 *                              error is returned.
 *
 * The configuration byte is in the following format:
 *     Bit 1   - Direction (1-output 0-input)
 *     Bit 2-3 - Interrupt method
 *     Bit 4   - Interrupts disabled (0-disabled 1-enabled)
 */
static int EDIODev_IOCtl (
                          struct inode* inode, 
                          struct file* filp, 
                          unsigned int cmd, 
                          unsigned long arg
                         )
{
    GPIODEV_LOWLEVEL_CONFIG llconf;
    EDIODEVICE*             dev;
    unsigned long           minor;
    unsigned long           result;
    unsigned char           value;

    minor = MINOR(inode->i_rdev);

    if(minor >= EDIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &edio_devs[minor];
    if(!(dev->flags&GPIODEV_OPEN))
        return -EBADF;

    result = 0;

    switch(cmd)
    {
        case GPIODEV_GET_CONFIG:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            result = copy_to_user((unsigned char*)arg, &dev->current_config, 1);
            if(result)
                return -EFAULT;
        }break;

        case GPIODEV_SET_CONFIG:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            result = copy_from_user(&value, (unsigned char*)arg, 1);
            if(result)
                return -EFAULT;

            if(((value&GPIODEV_DIRECTION_MASK) != GPIODEV_INPUT) && (value&GPIODEV_INTENABLE_MASK))
                return -EINVAL;

            result = ConfigureEDIO(dev, value);

            return result;
        }break;

        case GPIODEV_INT_REENABLE:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            if(!(dev->current_config&GPIODEV_INTENABLE_MASK))
                return -EINVAL;

            dev->interrupted = 0;

            enable_irq(dev->irq);
        }break;

        case GPIODEV_GET_LOWLEVELCONFIG:
        {
            if(!(dev->flags&GPIODEV_LOWLEVELACCESS))
                return -EINVAL;

            llconf.pin                   = dev->pin;
            llconf.signo                 = 0;
            llconf.port                  = 0;
            llconf.irq                   = dev->irq;
            llconf.iomux_input_function  = 0;
            llconf.iomux_output_function = 0;
            llconf.static_config         = dev->static_config;

            result = copy_to_user((unsigned int*)arg, &llconf, sizeof(GPIODEV_LOWLEVEL_CONFIG));
            if(result)
                return -EFAULT;
        }break;

        case GPIODEV_SET_LOWLEVELCONFIG:
        {
            if(!(dev->flags&GPIODEV_LOWLEVELACCESS))
                return -EINVAL;

            result = copy_from_user(&llconf, (unsigned int*)arg, sizeof(GPIODEV_LOWLEVEL_CONFIG));
            if(result)
                return -EFAULT;

            if(((llconf.static_config&GPIODEV_DIRECTION_MASK) != GPIODEV_INPUT) && (llconf.static_config&GPIODEV_INTENABLE_MASK))
                return -EINVAL;

            if(dev->flags&GPIODEV_CONFIGURED)
                ConfigureEDIO(dev, GPIODEV_INVALID_CONFIG);

            dev->pin                = llconf.pin;
            dev->irq                = llconf.irq;
            dev->static_config      = llconf.static_config;

            dev->flags |= GPIODEV_CONFIGURED;

            result = ConfigureEDIO(dev, dev->static_config);
            if(result)
                return result;
        }break;

        case GPIODEV_RETAIN_STATE:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            dev->flags |= GPIODEV_RETAINSTATE;
        }break;

        case GPIODEV_REVERT_STATE:
        {
            if(!(dev->flags&GPIODEV_CONFIGURED))
                return -EBADF;

            dev->flags &= ~GPIODEV_RETAINSTATE;
        }break;

        default:return -EINVAL;
    }

    return 0;
}

/*
 * GPIODev_Poll handles a user's select/poll call. This function will
 * return whatever functionality is currently available on a device.
 * The intended use is to wait for interrupts in a select call. By
 * waiting on an exception, a select call will block until an interrupt
 * occurs.
 */
static unsigned int GPIODev_Poll (struct file* filp, poll_table* table)
{
    GPIODEVICE*   dev;
    unsigned long minor;
    unsigned long mask;

    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    if(minor >= GPIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &gpio_devs[minor];
    if(!(dev->flags&GPIODEV_OPEN) || !(dev->flags&GPIODEV_CONFIGURED))
        return -EBADF;

    poll_wait(filp, &dev->event_queue, table);

    mask = POLLIN|POLLRDNORM;

    if((dev->current_config&GPIODEV_DIRECTION_MASK) == GPIODEV_OUTPUT)
        mask |= POLLOUT|POLLWRNORM;

    if(dev->interrupted)
        mask |= POLLPRI;

    return mask;
}

/*
 * EDIODev_Poll handles a user's select/poll call. This function will
 * return whatever functionality is currently available on a device.
 * The intended use is to wait for interrupts in a select call. By
 * waiting on an exception, a select call will block until an interrupt
 * occurs.
 */
static unsigned int EDIODev_Poll (struct file* filp, poll_table* table)
{
    EDIODEVICE*   dev;
    unsigned long minor;
    unsigned long mask;

    minor = MINOR(filp->f_dentry->d_inode->i_rdev);

    if(minor >= EDIO_DEVICE_COUNT)
        return -ENODEV;

    dev = &edio_devs[minor];
    if(!(dev->flags&GPIODEV_OPEN) || !(dev->flags&GPIODEV_CONFIGURED))
        return -EBADF;

    poll_wait(filp, &dev->event_queue, table);

    mask = POLLIN|POLLRDNORM;

    if((dev->current_config&GPIODEV_DIRECTION_MASK) == GPIODEV_OUTPUT)
        mask |= POLLOUT|POLLWRNORM;

    if(dev->interrupted)
        mask |= POLLPRI;

    return mask;
}

/*
 * init_module sets up the character driver, configures the GPIOs/EDIOs specified
 * in the gpio_devs/edio_devs list, and creates entries in /dev corresponding to
 * each configured GPIO/EDIO.
 */
int init_module (void)
{
    GPIODEVICE*   gpiodev;
    EDIODEVICE*   ediodev;
    unsigned long index;
    unsigned long result;

    gpiodev_fops.open    = &GPIODev_Open;
    gpiodev_fops.release = &GPIODev_Close;
    gpiodev_fops.read    = &GPIODev_Read;
    gpiodev_fops.write   = &GPIODev_Write;
    gpiodev_fops.ioctl   = &GPIODev_IOCtl;
    gpiodev_fops.poll    = &GPIODev_Poll;

    ediodev_fops.open    = &EDIODev_Open;
    ediodev_fops.release = &EDIODev_Close;
    ediodev_fops.read    = &EDIODev_Read;
    ediodev_fops.write   = &EDIODev_Write;
    ediodev_fops.ioctl   = &EDIODev_IOCtl;
    ediodev_fops.poll    = &EDIODev_Poll;

    result = register_chrdev(0, "gpiodev", &gpiodev_fops);
    if(result < 0)   
    {
        printk("Failed to register gpiodev, register_chrdev() returned %lu\n", result);
 
        return result;
    }

    gpiodev_major = result;

    result = register_chrdev(0, "ediodev", &ediodev_fops);
    if(result < 0)   
    {
        printk("Failed to register ediodev, register_chrdev() returned %lu\n", result);
 
        unregister_chrdev(gpiodev_major, "gpiodev");

        return result;
    }

    ediodev_major = result;
        
    devfs_mk_dir(GPIODEV_NAME);
    devfs_mk_dir(GPIODEV_NAME "/" BLUETOOTHDIR);

    for(index = 0; index < GPIO_DEVICE_COUNT; index++)
    {
        gpiodev = &gpio_devs[index];

        /* if the signal attributed is set, update port and signo */
        if(GPIO_SIGNAL_IS_VALID(gpiodev->signal)) {
            gpiodev->port = initial_gpio_settings[gpiodev->signal].port;
            gpiodev->signo = initial_gpio_settings[gpiodev->signal].sig_no;
        }

        /* if the port is invalid and the device is not set for low 
         * level access, skip this device */
        if( (gpiodev->port==GPIO_INVALID_PORT) &&
            !(gpiodev->flags & GPIODEV_LOWLEVELACCESS) ) {
            continue;
        }

        if(gpiodev->flags&GPIODEV_CONFIGURED)
        {
            if(!(gpiodev->flags&GPIODEV_RETAINSTATE))
            {
#ifdef CONFIG_ARCH_MXC91231
                if(IS_VALID_GPIODEV_IOMUX_GIPIN(gpiodev->pin)) {
                    iomux_config_mux(gpiodev_iomux_pin[gpiodev->pin],
                            OUTPUTCONFIG_DEFAULT, INPUTCONFIG_NONE);
                }
#endif

                ConfigureGPIO(gpiodev, gpiodev->init_config);
            }
            else
            {
                if(IS_VALID_GPIODEV_IOMUX_GIPIN(gpiodev->pin)) {
                    iomux_config_mux(gpiodev_iomux_pin[gpiodev->pin],
                            gpiodev->iomux_out_function,
                            gpiodev->iomux_in_function);
                }

                result = ConfigureGPIO(gpiodev, gpiodev->static_config);
                if(result < 0)
                {
#ifdef CONFIG_ARCH_MXC91231
                    if(IS_VALID_GPIODEV_IOMUX_GIPIN(gpiodev->pin)) {
                        iomux_config_mux(gpiodev_iomux_pin[gpiodev->pin],
                                OUTPUTCONFIG_DEFAULT, INPUTCONFIG_NONE);
                    }
#endif

                    printk("Warning: Device \"%s\" could not be configured properly\n", gpiodev->device_name);

                }
            }
        }

        init_waitqueue_head(&gpiodev->event_queue);

        result = devfs_mk_cdev(
                               MKDEV(gpiodev_major, index),
                               S_IFCHR|S_IRUSR|S_IWUSR|S_IWGRP|S_IRGRP,
                               GPIODEV_NAME "/%s", 
                               gpiodev->device_name
                              );
        if(result < 0)
            printk("Warning: Device \"%s\" could not be created\n", gpiodev->device_name);
    }

    for(index = 0; index < EDIO_DEVICE_COUNT; index++)
    {
        ediodev = &edio_devs[index];

        if(ediodev->flags&GPIODEV_CONFIGURED)
        {
            if(!(ediodev->flags&GPIODEV_RETAINSTATE))
                ConfigureEDIO(ediodev, GPIODEV_INVALID_CONFIG);
            else
            {
                result = ConfigureEDIO(ediodev, ediodev->static_config);
                if(result < 0)
                {
                    ConfigureEDIO(ediodev, GPIODEV_INVALID_CONFIG);

                    printk("Warning: Device \"%s\" could not be configured properly\n", ediodev->device_name);
                }
            }
        }

        init_waitqueue_head(&ediodev->event_queue);

        result = devfs_mk_cdev(
                               MKDEV(ediodev_major, index),
                               S_IFCHR|S_IRUSR|S_IWUSR|S_IWGRP|S_IRGRP,
                               GPIODEV_NAME "/%s", 
                               ediodev->device_name
                              );
        if(result < 0)
            printk("Warning: Device \"%s\" could not be created\n", ediodev->device_name);
    }

    return 0;
}


/*
 * cleanup_module unregisters the character drivers and removes the /dev
 * entries. If for some reason a GPIO/EDIO still has a reference count, interrupts
 * are disabled on to effectively 'shut it down.'
 */
void cleanup_module (void)
{
    GPIODEVICE*   gpiodev;
    EDIODEVICE*   ediodev;
    unsigned long index;

    unregister_chrdev(gpiodev_major, "gpiodev");
    unregister_chrdev(gpiodev_major, "ediodev");

    for(index = 0; index < GPIO_DEVICE_COUNT; index++)
    {
        gpiodev = &gpio_devs[index];

        devfs_remove(GPIODEV_NAME "/%s", gpiodev->device_name);

        if(gpiodev->flags&GPIODEV_CONFIGURED)
        {
            ConfigureGPIO(gpiodev, GPIODEV_INVALID_CONFIG);
#ifdef CONFIG_ARCH_MXC91231
            if(IS_VALID_GPIODEV_IOMUX_GIPIN(gpiodev->pin)) {
                iomux_config_mux(gpiodev_iomux_pin[gpiodev->pin],
                        OUTPUTCONFIG_DEFAULT, INPUTCONFIG_NONE);
            }
#endif
        }
    }

    for(index = 0; index < EDIO_DEVICE_COUNT; index++)
    {
        ediodev = &edio_devs[index];

        devfs_remove(GPIODEV_NAME "/%s", ediodev->device_name);

        if(ediodev->flags&GPIODEV_CONFIGURED)
            ConfigureEDIO(ediodev, GPIODEV_INVALID_CONFIG);
    }

    devfs_remove(GPIODEV_NAME);
}
