/* 
 * gpiodev_pins.h - SCM-A11 version of exportable pin list.
 *
 * Copyright 2007 Motorola, Inc.
 */

/*
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 20-Jan-2007  Motorola    Initial revision.
 */

#ifndef __GPIODEV_PINS_H__
#define __GPIODEV_PINS_H__

/**
 * Provide an exportable interface to the IOMUX pins defined by Freescale.
 */
enum gpiodev_iomux_pins {
    AP_CLE_GIPIN = 0,
    AP_ALE_GIPIN,
    AP_CE_B_GIPIN,
    AP_RE_B_GIPIN,
    AP_WE_B_GIPIN,
    AP_WP_B_GIPIN,
    AP_BSY_B_GIPIN,
    AP_U1_TXD_GIPIN,
    AP_U1_RXD_GIPIN,
    AP_U1_RTS_B_GIPIN,
    AP_U1_CTS_B_GIPIN,
    AP_AD1_TXD_GIPIN,
    AP_AD1_RXD_GIPIN,
    AP_AD1_TXC_GIPIN,
    AP_AD1_TXFS_GIPIN,
    AP_AD2_TXD_GIPIN,
    AP_AD2_RXD_GIPIN,
    AP_AD2_TXC_GIPIN,
    AP_AD2_TXFS_GIPIN,
    AP_OWDAT_GIPIN,
    AP_IPU_LD17_GIPIN,
    AP_IPU_D3_VSYNC_GIPIN,
    AP_IPU_D3_HSYNC_GIPIN,
    AP_IPU_D3_CLK_GIPIN,
    AP_IPU_D3_DRDY_GIPIN,
    AP_IPU_D3_CONTR_GIPIN,
    AP_IPU_D0_CS_GIPIN,
    AP_IPU_LD16_GIPIN,
    AP_IPU_D2_CS_GIPIN,
    AP_IPU_PAR_RS_GIPIN,
    AP_IPU_D3_PS_GIPIN,
    AP_IPU_D3_CLS_GIPIN,
    AP_IPU_RD_GIPIN,
    AP_IPU_WR_GIPIN,
    AP_IPU_LD0_GIPIN,
    AP_IPU_LD1_GIPIN,
    AP_IPU_LD2_GIPIN,
    AP_IPU_LD3_GIPIN,
    AP_IPU_LD4_GIPIN,
    AP_IPU_LD5_GIPIN,
    AP_IPU_LD6_GIPIN,
    AP_IPU_LD7_GIPIN,
    AP_IPU_LD8_GIPIN,
    AP_IPU_LD9_GIPIN,
    AP_IPU_LD10_GIPIN,
    AP_IPU_LD11_GIPIN,
    AP_IPU_LD12_GIPIN,
    AP_IPU_LD13_GIPIN,
    AP_IPU_LD14_GIPIN,
    AP_IPU_LD15_GIPIN,
    AP_KPROW4_GIPIN,
    AP_KPROW5_GIPIN,
    AP_GPIO_AP_B17_GIPIN,
    AP_GPIO_AP_B18_GIPIN,
    AP_KPCOL3_GIPIN,
    AP_KPCOL4_GIPIN,
    AP_KPCOL5_GIPIN,
    AP_GPIO_AP_B22_GIPIN,
    AP_GPIO_AP_B23_GIPIN,
    AP_CSI_D0_GIPIN,
    AP_CSI_D1_GIPIN,
    AP_CSI_D2_GIPIN,
    AP_CSI_D3_GIPIN,
    AP_CSI_D4_GIPIN,
    AP_CSI_D5_GIPIN,
    AP_CSI_D6_GIPIN,
    AP_CSI_D7_GIPIN,
    AP_CSI_D8_GIPIN,
    AP_CSI_D9_GIPIN,
    AP_CSI_MCLK_GIPIN,
    AP_CSI_VSYNC_GIPIN,
    AP_CSI_HSYNC_GIPIN,
    AP_CSI_PIXCLK_GIPIN,
    AP_I2CLK_GIPIN,
    AP_I2DAT_GIPIN,
    AP_GPIO_AP_C8_GIPIN,
    AP_GPIO_AP_C9_GIPIN,
    AP_GPIO_AP_C10_GIPIN,
    AP_GPIO_AP_C11_GIPIN,
    AP_GPIO_AP_C12_GIPIN,
    AP_GPIO_AP_C13_GIPIN,
    AP_GPIO_AP_C14_GIPIN,
    AP_GPIO_AP_C15_GIPIN,
    AP_GPIO_AP_C16_GIPIN,
    AP_GPIO_AP_C17_GIPIN,
    AP_ED_INT0_GIPIN,
    AP_ED_INT1_GIPIN,
    AP_ED_INT2_GIPIN,
    AP_ED_INT3_GIPIN,
    AP_ED_INT4_GIPIN,
    AP_ED_INT5_GIPIN,
    AP_ED_INT6_GIPIN,
    AP_ED_INT7_GIPIN,
    AP_U2_DSR_B_GIPIN,
    AP_U2_RI_B_GIPIN,
    AP_U2_CTS_B_GIPIN,
    AP_U2_DTR_B_GIPIN,
    AP_KPROW0_GIPIN,
    AP_KPROW1_GIPIN,
    AP_KPROW2_GIPIN,
    AP_KPROW3_GIPIN,
    AP_KPCOL0_GIPIN,
    AP_KPCOL1_GIPIN,
    AP_KPCOL2_GIPIN,
    SP_U3_TXD_GIPIN,
    SP_U3_RXD_GIPIN,
    SP_U3_RTS_B_GIPIN,
    SP_U3_CTS_B_GIPIN,
    SP_USB_TXOE_B_GIPIN,
    SP_USB_DAT_VP_GIPIN,
    SP_USB_SE0_VM_GIPIN,
    SP_USB_RXD_GIPIN,
    SP_UH2_TXOE_B_GIPIN,
    SP_UH2_SPEED_GIPIN,
    SP_UH2_SUSPEND_GIPIN,
    SP_UH2_TXDP_GIPIN,
    SP_UH2_RXDP_GIPIN,
    SP_UH2_RXDM_GIPIN,
    SP_UH2_OVR_GIPIN,
    SP_UH2_PWR_GIPIN,
    SP_SD1_DAT0_GIPIN,
    SP_SD1_DAT1_GIPIN,
    SP_SD1_DAT2_GIPIN,
    SP_SD1_DAT3_GIPIN,
    SP_SD1_CMD_GIPIN,
    SP_SD1_CLK_GIPIN,
    SP_SD2_DAT0_GIPIN,
    SP_SD2_DAT1_GIPIN,
    SP_SD2_DAT2_GIPIN,
    SP_SD2_DAT3_GIPIN,
    SP_GPIO_Shared26_GIPIN,
    SP_SPI1_CLK_GIPIN,
    SP_SPI1_MOSI_GIPIN,
    SP_SPI1_MISO_GIPIN,
    SP_SPI1_SS0_GIPIN,
    SP_SPI1_SS1_GIPIN,
    SP_SD2_CMD_GIPIN,
    SP_SD2_CLK_GIPIN,
    SP_SIM1_RST_B_GIPIN,
    SP_SIM1_SVEN_GIPIN,
    SP_SIM1_CLK_GIPIN,
    SP_SIM1_TRXD_GIPIN,
    SP_SIM1_PD_GIPIN,
    SP_UH2_TXDM_GIPIN,
    SP_UH2_RXD_GIPIN,
    MAX_GPIODEV_IOMUX_GIPIN,
    INVALID_GPIODEV_IOMUX_GIPIN = MAX_GPIODEV_IOMUX_GIPIN
};

#endif /* __GPIODEV_PINS_H__ */
