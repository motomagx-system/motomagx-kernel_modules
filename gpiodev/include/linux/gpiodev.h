/* 
 * GPIODev                                       gpiodev.h
 *
 * Copyright 2006-2007 Motorola, Inc.
 */

/*
 * This library is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
 * USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 05-Oct-2006  Motorola    Initial revision.
 * 20-Jan-2007  Motorola    Use pins defined in gpiodev_pins.h
 */

#ifndef _GPIODEV_H_
#define _GPIODEV_H_

#include "linux/gpiodev_pins.h"

#define IS_VALID_GPIODEV_IOMUX_GIPIN(n)  ((n>=0)&&(n<MAX_GPIODEV_IOMUX_GIPIN))

/*
 * GPIODEV_LOWLEVEL_CONFIG is used to specify the low level configuration
 * of a device. This structure is passed as a parameter to an ioctl()
 * with the type of either GPIODEV_GET_LOWLEVELCONFIG or
 * GPIODEV_SET_LOWLEVEL_CONFIG. 
 *
 * pin - the pin number to associate the device with
 * signo - the signal number to associate the device with (GPIO only)
 * port - the port number to associate the device with (GPIO only) 
 * irq - the irq number to associate the device with(EDIO inputs only)
 * iomux_input_function - the iomux input function (GPIO only)
 * iomux_output_function - the iomux output function (GPIO only)
 * static_config - the default configuration of the device 
 *                 such as GPIODEV_INPUT|GPIODEV_INT_ENABLED
 */
typedef struct GPIODEV_LOWLEVEL_CONFIG
{
    enum gpiodev_iomux_pins pin;
    unsigned char  signo;
    unsigned char  port;
    unsigned char  irq;
    unsigned char  iomux_input_function;
    unsigned char  iomux_output_function;
    unsigned char  static_config;
}GPIODEV_LOWLEVEL_CONFIG;


#include <linux/ioctl.h>


#define GPIODEV_BASE 'g'

/*
 * Definition of supported ioctl()'s 
 *
 * GPIODEV_GET_CONFIG - Get the configuration byte of a particular device.
 *                      The configuration byte is a bit mask of the various
 *                      defines described below.
 *
 * GPIODEV_SET_CONFIG - Set the configuration byte of a particular device.
 *                      The ioctl() will return an error if the configuration
 *                      could not be set. The configuration byte is a bit mask
 *                      of the various defines described below.
 *
 * GPIODEV_INT_REENABLE - After an interrupt is received, this ioctl() must be
 *                        performed before another interrupt from the same device
 *                        can be received.
 *
 * GPIODEV_GET_LOWLEVELCONFIG - Get the low level configuration of a device.
 *                              An error will be returned if this operation is
 *                              not allowed on a particular GPIO.
 *
 * GPIODEV_SET_LOWLEVELCONFIG - Set the low level configuartion of a device.
 *                              An error will be returned if this operation is
 *                              not allowed on a particular GPIO.
 *
 */
#define GPIODEV_GET_CONFIG         _IOR(GPIODEV_BASE, 0, int*)
#define GPIODEV_SET_CONFIG         _IOW(GPIODEV_BASE, 1, int*)
#define GPIODEV_INT_REENABLE       _IOW(GPIODEV_BASE, 2, void)
#define GPIODEV_GET_LOWLEVELCONFIG _IOR(GPIODEV_BASE, 3, GPIODEV_LOWLEVEL_CONFIG*)
#define GPIODEV_SET_LOWLEVELCONFIG _IOW(GPIODEV_BASE, 4, GPIODEV_LOWLEVEL_CONFIG*)
#define GPIODEV_RETAIN_STATE       _IO(GPIODEV_BASE, 5)
#define GPIODEV_REVERT_STATE       _IO(GPIODEV_BASE, 6)


/*
 * The GPIO_*_MASK is used to help parse the one byte 
 * configuration value passed to an ioctl from the user. 
 * This is explained a bit more later on.
 */
#define GPIODEV_DIRECTION_MASK 0x01
#define GPIODEV_INTERRUPT_MASK 0x06
#define GPIODEV_INTENABLE_MASK 0x08


/*
 * Definition of the format of the configuration byte
 *
 * GPIODEV_INPUT / GPIODEV_OUT - Sets up the GPIO as an input or an output
 *
 * GPIODEV_*INTTYPE_LLEV - Sets the interrupt type as level low sensitive 
 * GPIODEV_*INTTYPE_HLEV - Sets the interrupt type as level high sensitive
 * GPIODEV_*INTTYPE_BEDG - Sets the interrupt type as both edge sensitive 
 * GPIODEV_*INTTYPE_REDG - Sets the interrupt type as rising edge sensitive
 * GPIODEV_*INTTYPE_FEDG - Sets the interrupt type as falling edge sensitive
 *
 * GPIODEV_INT_ENABLED / GPIODEV_INT_DISABLED - Enables or disables the interrupts
 *                                              on a particular device.
 *
 */
#define GPIODEV_INPUT  0x00
#define GPIODEV_OUTPUT 0x01

#define GPIODEV_GPIOINTTYPE_LLEV (0x00<<1)
#define GPIODEV_GPIOINTTYPE_HLEV (0x01<<1)
#define GPIODEV_GPIOINTTYPE_REDG (0x02<<1)
#define GPIODEV_GPIOINTTYPE_FEDG (0x03<<1)

#define GPIODEV_EDIOINTTYPE_LLEV (0x00<<1)
#define GPIODEV_EDIOINTTYPE_REDG (0x01<<1)
#define GPIODEV_EDIOINTTYPE_FEDG (0x02<<1)
#define GPIODEV_EDIOINTTYPE_BEDG (0x03<<1)

#define GPIODEV_INT_ENABLED  (0x01<<3)
#define GPIODEV_INT_DISABLED (0x00<<3)


#endif /* #ifndef _GPIODEV_H_ */
