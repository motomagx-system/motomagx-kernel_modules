/*
 * Copyright (C) 2007 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 */

#ifndef __DMLBM_H__
#define __DMLBM_H__

#include <linux/list.h>
#include "logicalMsg.h"

#define LOGOBUF_COUNT 20
#define LOGIBUF_TOTAL_SIZE sizeof(struct logi_buff_head)+sizeof(struct logi_pkg)*LOGOBUF_COUNT 



#define LBM_BUFF_CMD_MIN 4 /* one rcvd msg+one for ack+whatever in transmit */
#define LBM_BUFF_RCV_MIN 6 /* To trancieve, there must be a buffer to receive a */
                       /*new message and there must be space to plave an ack*/
                       /*for this message   */



#define LBM_SEQ_MAX 0xFFFFFFFF

/*
 * Message buffer stares
 */
typedef enum
  {
    LBM_EMPTY,        /*00*/      /* The buffer is empty and free to use */
    LBM_MSG_READY,    /*01*/      /* A message is ready to send          */
    LBM_ACK_READY,    /*02*/      /* An ACK is ready to send  */
    LBM_DMY_SENDING,  /*03*/      /* dummy data during transmision  */
    LBM_MSG_SENDING,  /*04*/      /* message during transmision */
    LBM_MSG_RESENDING,/*05*/      /* message is resending */
    LBM_ACK_SENDING,  /*06*/      /* ACK during transmision  */
    LBM_MSG_SENT,     /*07*/      /* A message has been send  */
    LBM_MSG_RCVD,     /*08*/      /* A message has been received  */
    LBM_ACK_RCVD,     /*09*/      /* An ACK has been received */
    LBM_RESERVED,     /*10*/      /* the buffer is reserved */
  }LBM_MSG;


/*!
 * This structure is the logival driver buffer object
 * It contains all internal variables
 * If there is no elements/messages in the pointer will be set to NULL
 */
struct logi_buff_head {



  /*!
   * The first empty message buffer
   */
  struct list_head msg_emptys_head;
  
  /*!
   * The first message in the send que
   */
  struct list_head msg_transmits_head;

  /*!
   * the last transmitted message
   */
  struct list_head msg_received_head;

  unsigned int host_seq_no;
  unsigned int dm_seq_no;


  int no_of_empty;

};



/* External */

/*!
 * Creates the logiBuffer object: alocate memory initializes the buffers
 *
 * @return a pointer to the buffer object or NULL if error.
 */
struct logi_buff_head *logi_buff_create(void);

/*!
 * Releases the logiBuffer: deallocate the memory
 *
 * @param  *_logi_buff_head  A pointer the buffer object  
 */
int logi_buff_release(struct logi_buff_head* pkg_buff_head );

/*!
 * Resets the package buffers to a initila state
 *
 * @param  *pkg_buff_head  A pointer the logical buffer object 
 */
void logi_buff_reset(struct logi_buff_head *pkg_buff_head);

/*!
 * Finds a free package buffer and set it as reserved
 *
 * @return pointer to the package buffer or NULL if error
 */
struct logi_pkg *logi_buff_get_resvd_pkg( struct logi_buff_head* pkg_buff_head);

/*! 
 * Releases a package buffer 
 *
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 */  
void logi_buff_pkg_release( struct logi_pkg *msg, struct logi_buff_head* pkg_buff_head);

/********* Transcieve functions ******************/
/*!
 * Finds the buffer where the data should be read when a package is 
 * transmitted 
 * 
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return   Returns a pointer to the package buffer
 */
struct logi_pkg *logi_buff_get_send_pkg( struct logi_buff_head *pkg_buff_head );

/*!
 * Finds the buffer where the data should be written when a package is received 
 * 
 * @return   Returns a pointer to the package buffer
 */
struct logi_pkg *logi_buff_get_rcv_pkg( struct logi_buff_head *pkg_buff_head );

/*!
 * To mark a message have been succesfully send
 *
 * @param  *msg           A pointer to the just send package
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return   negative value if error
 */
int logi_buff_set_pkg_sent(struct logi_pkg *msg, struct logi_buff_head* pkg_buff_head);

/*!
 * To mark a message as received
 *
 * @param  *msg      A pointer to the just received package
 * @return   LBM_EMPTY(0): if dymmy package is received
 *           LBM_ACK_RCVD: if ack is received
 *           LBM_MSG_RCVD: if message is received 
 *           Returns a negative value if error
 */
int logi_buff_set_pkg_rcved(struct logi_pkg *msg, struct logi_buff_head* pkg_buff_head);

/*!
 * Place a package buffer in the transmit list and marks it ready to send
 * Note1: there is currently no priority handeling. Currwntly a FIFO.
 * Note2: ack is determined from the ack bit in the package header
 *
 * @param  *msg           A pointer to the package buffer to send
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return                 negative if error
 */
int logi_buff_place_in_trnsmt(struct logi_pkg *msg, struct logi_buff_head* pkg_buff_head);

/*!
 * Generates an ack for a given message and place it in the transmit queue
 *
 * @param  *msg           A pointer to the message to be acked
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return                A negative value if error
 */
int logi_buff_gen_ack(struct logi_pkg *msg, struct logi_buff_head* pkg_buff_head);


/*!
 * Finds a received package with the specified channel and ack state
 *
 * @param  channel   The channel number 
 * @param  *count    output: where the the number of received messages 
 *                   in buffer of the spevified channel is written.
 * @param  *pkg_buff_head  A pointer the logical buffer object or NULL 
 *                         if error
 */
struct logi_pkg *logi_buff_find_rcvd_pkg(int channel, int *count,
					  struct logi_buff_head* pkg_buff_head);
#ifdef DEBUG
/* for debug */
int logi_buff_print_all(char *p, struct logi_buff_head* pkg_buff_head);

/* for debug */
void logi_buff_kprint_status(struct logi_buff_head* pkg_buff_head);

#endif

#endif				/* __DMLBM_H__ */
