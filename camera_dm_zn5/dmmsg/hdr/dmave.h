/*
 * Copyright (c) 2007 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 02-Jul-2007  Motorola    Initial revision
 * 16-Aug-2007  Motorola    OSS compliance
 */

#ifndef __DMAVE_H__
#define __DMAVE_H__

#ifdef CONFIG_MOT_DM299_DEBUG_ALL
#define DEBUG
#endif

#define IMG_MSG_CMD_CH LMB_CH_CMD
#define IMG_MSG_DATA_CH LMB_CH_DATA1
#define IMGR_MSG_ACK_FLAG LBM_ACK_FLAG

typedef struct {
	unsigned int message_seq_num;
	unsigned short message_id;
	unsigned short message_status;
	unsigned int message_size;
	unsigned int message_time;
	unsigned int message_checksum;
	struct timeval host_time_stamp;
} dmave_msg_header_t;

#define IMGR_MSG_HEADER_LEN sizeof(dmave_msg_header_t)
#define IMGR_MSG_DATA_LEN (512)
#define IMGR_MSG_LEN (IMGR_MSG_DATA_LEN + IMGR_MSG_HEADER_LEN)

typedef struct {
	dmave_msg_header_t cam_header;
	unsigned char message_content[IMGR_MSG_DATA_LEN];
} dmave_msg_t;

#endif	 /*#ifndef __DMAVE_H__ */
