/*
 * Copyright (C) 2007-2008 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 23-May-2007  Motorola    interrupt time changed from 30 to 10 us 
 * 22-Aug-2008  Motorola    Move Logical Channel set_mode to dmave interface
 * 02-Sep-2008  Motorola    OSS Issue
 * 03-Sep-2008  Motorola    OSS Issue
 * 03-Sep-2008  Motorola    Copyright Issue
 */

#ifndef __DMDRV_H__
#define __DMDRV_H__
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/timer.h>
#include <asm/delay.h>
#include <asm-arm/mot-gpio.h> /* gpio */
#include <asm/processor.h>
#include "dmave_if.h"
#include "dmave_logi_drv.h"

#include "logicalBuffer.h"
#include "logicalMsg.h"

#define LOGIDRV_BLOCK 0
#define LOGIDRV_NON_BLOCK 1
#define LOGIDRV_WAIT_ON_ACK 2
#define LOGIDRV_MAX_CHANNEL 3

#define LOGIDRV_LOOP_TIME_OUT (3 * HZ) 

#define DM_INT_SIGN_DELAY 10 /*us*/

/* Possible error no:
 * -ENOSYS   : not implemented
 * -ECHRNG   : Channel number out of range 
 * -EBUSY    : Channel is busy
 * -EINTR    : Interrupted by user
 * -ERESTARTSYS: interrupted by signal
 * -- Fatal errors (driver must be reset) ---
 * -ETIMEDOUT: Connection timed out 
 * -ENOMEM   : internal error
 * -ENODATA  : internal error
 */


/*!
 * Initializes the logical channel driver and start up the loop
 *
 * @return a nonzero value if if failed
 */
int logi_drv_init(void);

/*!
 * Stops and cleans up the logical channel driver 
 */
void logi_drv_stop(void);

/*!
 * Resets the logical channel driver
 * This should be used after an application download
 */
int logiDrv_reset(void);

/*!
 * This function gets sends a message on the specified channel
 * and waits for an ack before it returns
 * Note1: the msg is released after it it is send
 * Note2: the msg will not be released if error
 * 
 * @param pkg      A pointer to the message object to be send.
 * @param channel  The channel number.
 * @return         Returns negative if error.
 */
int logi_drv_send_pkg( struct logi_pkg *msg, int channel);



/*!
 * Receives a msg or an ack on a specified channel
 *
 * @param pkg      A pointer to a pointer where the adress of the received 
 *                 message object should be written.
 * @param channel  The channel number.
 * @return         Returns number of awaiting messages or negative if error.
 */
int logi_drv_receive_pkg( struct logi_pkg **pkg, int channel);


/*!
 * Interrupts a ongoing logi_drv_receive_pkg() interrupt and return -EINTR.
 * Function:
 * - Check channel number
 * - Set usrint
 * - Wake up bloked threads
 *
 * @param channel the channel which should be interrupted
 * @return negative if error
 */
int logi_drv_int_receive(int channel);

/*!
 * Reserves a package buffer 
 * 
 * @return    A pointer to the package buffer or NULL if error
 */
struct logi_pkg *logi_drv_get_empty_pkg(void);

/*!
 * Releases a package buffer 
 * 
 * @param     A pointer to the package buffer which is to be relesased 
 * @return    negative if error
 */
int logi_drv_release_pkg(struct logi_pkg * pkg);

#endif				/* __DMDRV_H__ */
