/*
 * Copyright (C) 2007 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 13-Dec-2007  Motorola    Added big endian mode
 */

#ifndef __DMMSG_H__
#define __DMMSG_H__

#define LBM_CHA_ENDIA 1

#include <linux/list.h>
#include <linux/kernel.h>
#include <linux/errno.h>

/*
 * Message defines
 */

#ifndef LBM_CHA_ENDIA

#define LBM_SEQ_POS 0
#define LBM_ID_POS 4
#define LBM_CHAC_POS 5
#define LBM_STATUS_POS 6
#define LBM_SIZE_POS 8
#define LBM_TIME_POS 12
#define LBM_CHKSM_POS 16
#define LBM_DATA_POS 20 /* pay load */

#else

#define LBM_SEQ_POS 0
#define LBM_ID_POS 7
#define LBM_CHAC_POS 6
#define LBM_STATUS_POS 4
#define LBM_SIZE_POS 8
#define LBM_TIME_POS 12
#define LBM_CHKSM_POS 16
#define LBM_DATA_POS 20 /* pay load */

#endif

#define LBM_ACK_FLAG 0x80
#define LBM_ACK LBM_ACK_FLAG
#define LMB_NO_ACK 0x00

#define LMB_CH_CMD 0
#define LMB_CH_DATA1 1

#define LMB_HEADER_SIZE 20 
#define LBM_PAILOAD_SIZE 512
#define LOGIBUF_MSG_SIZE LBM_PAILOAD_SIZE + LMB_HEADER_SIZE
/*!
 * This struct is an message element on the logical channel driver
 * This object contains parameters and a pointer to the whole message including
 * the message header
 */

struct logi_pkg {

  struct list_head list;

  /*! 
   * Is the time in jiffies of when the handshake interrupt was received for this message.
   */
  u64 time; 

  /*!
   * Indicates the status of the message
   */
  int state;

  /*!
   * Indicates if any error occurred durine handeling of this message
   */
  int err_status;

  /*!
   * Pointer to the data array
   */
  unsigned char data[LOGIBUF_MSG_SIZE];

};

#ifdef DEBUG
void logi_pkg_print(struct logi_pkg *msg);
#endif


/*! 
 * Change endianess functions
 */

#ifdef LBM_CHA_ENDIA

#define CH_ENDI_CP(dest,src,count) ch_endi_cp((unsigned int*)(dest), (unsigned int*)(src), (count)/4 + ((count)%4!=0))

#define CH_ENDI(data,count) ch_endi_cp((unsigned int*)(data), (unsigned int*)(data), (count)/4 + ((count)%4!=0))

static inline int ch_endi_cp(unsigned int* dest, unsigned int* src, int count)
{
	int i = 0;

	while(i < count){
		dest[i] = ntohl(src[i]);		
		i++;
	}
	return 0;
}
#else 
#define CH_ENDI_CP(dest,src,count) memcpy((dest), (src), (count))
#define CH_ENDI(data,count) 
#endif /*LBM_CHA_ENDIA*/

/*!
 * Inline code for messagebuffers 
 */

/*!
 * Generic set get functions 
 */
/*!
 * U8 set/get
 */
static inline void logi_pkg_set_u8(int pos, unsigned char value, struct logi_pkg *msg){

  msg->data[pos] = value&0xFF;
}

static inline unsigned char logi_pkg_get_u8(int pos, struct logi_pkg *msg){

  return  msg->data[pos]&0xFF;
}

/*!
 * U16 set/get
 */
static inline void logi_pkg_set_u16(int pos, unsigned short value, struct logi_pkg *msg){

#ifndef LBM_CHA_ENDIA
	msg->data[pos] = value&0xFF;
	msg->data[pos+1] = (value>>8)&0xFF;
#else
	msg->data[pos+1] = value&0xFF;
	msg->data[pos] = (value>>8)&0xFF;
#endif

}

static inline unsigned short logi_pkg_get_u16(int pos, struct logi_pkg *msg)
{
#ifndef LBM_CHA_ENDIA
	return  (msg->data[pos])+(msg->data[pos+1]<<8);
#else
	return  (msg->data[pos+1])+(msg->data[pos]<<8);
#endif
}

/*!
 * U32 set/get
 */

static inline void logi_pkg_set_u32(int pos, unsigned int value, struct logi_pkg *msg)
{
#ifndef LBM_CHA_ENDIA
	msg->data[pos] = value&0xFF;
	msg->data[pos+1] = (value>>8)&0xFF;
	msg->data[pos+2] = (value>>16)&0xFF;
	msg->data[pos+3] = (value>>24)&0xFF;
#else
	msg->data[pos+3] = value&0xFF;
	msg->data[pos+2] = (value>>8)&0xFF;
	msg->data[pos+1] = (value>>16)&0xFF;
	msg->data[pos+0] = (value>>24)&0xFF;
#endif
}

static inline unsigned int logi_pkg_get_u32(int pos, struct logi_pkg *msg){

#ifndef LBM_CHA_ENDIA
	return (msg->data[pos])+
		(msg->data[pos+1]<<8)+
		(msg->data[pos+2]<<16)+
		(msg->data[pos+3]<<24);
#else
	return (msg->data[pos+3])+
		(msg->data[pos+2]<<8)+
		(msg->data[pos+1]<<16)+
		(msg->data[pos+0]<<24);
#endif
}

/*
 * protocol specific functions
 */
/*! 
 * Sequence number functions
 */
static inline void logi_pkg_set_seqno(unsigned int seq_no, struct logi_pkg *msg)
{
	logi_pkg_set_u32(LBM_SEQ_POS, seq_no, msg);
}

static inline unsigned int logi_pkg_get_seqno(struct logi_pkg *msg)
{
	return logi_pkg_get_u32(LBM_SEQ_POS, msg);
}

/*!
 * Acknoledgement finctions
 */
static inline unsigned int logi_pkg_get_ack(struct logi_pkg *msg)
{  
	return msg->data[LBM_CHAC_POS]&0x80;
}

static inline void logi_pkg_set_ack(struct logi_pkg *msg)
{
	msg->data[LBM_CHAC_POS]= (msg->data[LBM_CHAC_POS]|0x80)&0x83;
}

static inline void logi_pkg_clear_ack(struct logi_pkg *msg)
{
	msg->data[LBM_CHAC_POS]= msg->data[LBM_CHAC_POS]&0x03;
}


/*!
 * Logical channel function
 */
static inline unsigned int logi_pkg_get_channel( struct logi_pkg *msg)
{
	return  msg->data[LBM_CHAC_POS]&0x03;
}

static inline void logi_pkg_set_channel(int channel, struct logi_pkg *msg)
{
	msg->data[LBM_CHAC_POS] = ((msg->data[LBM_CHAC_POS] & 0xFC) | (channel&0x03));
}

/*!
 * Message id 
 */
static inline unsigned int logi_pkg_get_id(struct logi_pkg *msg)
{
	return logi_pkg_get_u8( LBM_ID_POS, msg);
}

static inline void logi_pkg_set_id(int id, struct logi_pkg *msg)
{  
	logi_pkg_set_u8( LBM_ID_POS, id, msg);
}

/*!
 * Status functions 
 */
static inline unsigned int logi_pkg_get_status( struct logi_pkg *msg)
{  
	return logi_pkg_get_u16( LBM_STATUS_POS, msg);
}

static inline void logi_pkg_set_status(int status, struct logi_pkg *msg)
{
	logi_pkg_set_u16( LBM_STATUS_POS, status,  msg);
}

/*!
 * Get/set time 
 */
static inline unsigned int logi_pkg_get_size( struct logi_pkg *msg)
{
	return logi_pkg_get_u32( LBM_SIZE_POS, msg);
}

static inline void logi_pkg_set_size(int size, struct logi_pkg *msg)
{
	logi_pkg_set_u32( LBM_SIZE_POS, size, msg);
}


/*!
 * Get/set time 
 */
static inline unsigned int logi_pkg_get_time( struct logi_pkg *msg)
{
	return logi_pkg_get_u32( LBM_TIME_POS, msg);
}

static inline void logi_pkg_set_time(int time, struct logi_pkg *msg)
{  
	logi_pkg_set_u32( LBM_TIME_POS, time, msg);
}

/*!
 * Append/test checksum 
 */
static inline void logi_pkg_append_chksum( struct logi_pkg *msg)
{
	logi_pkg_set_u32(LBM_CHKSM_POS, 0xada7aeba, msg);
}

static inline int logi_pkg_test_chksum( struct logi_pkg *msg)
{
	/* simple check */
	switch (logi_pkg_get_u32(LBM_CHKSM_POS, msg)) {
	case 0xada7aeba:
		return 0;
	case 0x00000000:
		return 0;
	default:
		printk("ERROR->logi_pkg: check sum error, received: %x\n"
		       ,logi_pkg_get_u32(LBM_CHKSM_POS, msg) );
		return -EIO;
	}
}

static inline void logi_pkg_pr_hdr(struct logi_pkg *msg)
{
	int i = 0;

	while( i <  LMB_HEADER_SIZE){
		printk("%2.2x%2.2x ",msg->data[i], msg->data[i+1] );		
		i += 2;
	}

	printk("\n");
}


static inline int logi_pkg_chk(struct logi_pkg *pkg)
{
	int retval = 0;

	retval = logi_pkg_test_chksum(pkg);

	if(retval){
		printk("ERROR->logi_buff: checksum error in message\n");
		logi_pkg_pr_hdr(pkg);
		return retval;
	}
	
	if(logi_pkg_get_size(pkg) >  LBM_PAILOAD_SIZE ){
		printk("ERROR->logi_buff: size error in message\n");
		logi_pkg_pr_hdr(pkg);
		return -E2BIG;
	}

	return retval;
}


#endif				/* __LOGICALMSG_H__ */
