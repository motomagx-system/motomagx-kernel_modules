#ifndef __DMAVE_LOGI_DRV_H
#define __DMAVE_LOGI_DRV_H
/* DMAVE                                          <dmave_logi_drv.h>*/
/*                                                            */
/* Copyright (C) 2008 Motorola, Inc.                                 */
/*                                                            */
/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 27-Aug-2008  Motorola    Initial revision.
 */

/*! 
 * Set the mode of the logical channel driver 
 *
 * @param        mode   can be set to LDRV_RUN and LDRV_PAUSE
 */
int logi_drv_set_mode(int mode);
#define LDRV_RUN 1
#define LDRV_PAUSE 0

#endif // __DMAVE_LOGI_DRV_H
