#ifndef __DMAVE_IF_H__
#define __DMAVE_IF_H__
/* DMAVE                                          <dmave_if.h>*/
/*                                                            */
/* Copyright (C) 2007-2008 Motorola, Inc.                                 */
/*                                                            */
/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 16-Aug-2007  Motorola    Initial revision.
 * 17-Mar-2008  Motorola    Added ioctls for time sync
 * 17-Jun-2008  Motorola    pause/resume added
 * 22-Aug-2008  Motorola    Moved Logical Channel set_mode to dmave interface
 * 03-Sep-2008  Motorola    OSS Issue
 */

/*AVE host msg driver message_id's*/


#define DMMSG_IOC_NUMBER                    0xCE
#define DMMSG_IOC_GET_STATUS                _IO(DMMSG_IOC_NUMBER, 0)
#define DMMSG_IOC_GET_PKG_TIME              _IO(DMMSG_IOC_NUMBER, 1)
#define DMMSG_IOC_GET_TIME                  _IOR(DMMSG_IOC_NUMBER, 2, struct timeval)
#define DMMSG_IOC_GET_OFFSET                _IOR(DMMSG_IOC_NUMBER, 3, struct timeval)
#define DMMSG_IOC_GET_AVG_OFFSET            _IOR(DMMSG_IOC_NUMBER, 4, struct timeval)
#define DMMSG_IOC_GET_UPTIME                _IOR(DMMSG_IOC_NUMBER, 5, struct timeval)
#define DMMSG_IOC_GET_TIMEOFDAY             _IOR(DMMSG_IOC_NUMBER, 6, struct timeval)
#define DMMSG_IOC_GET_BOOT_TIME             _IOR(DMMSG_IOC_NUMBER, 7, struct timeval)
#define DMMSG_IOC_GET_DM_BOOT_OFFSET        _IOR(DMMSG_IOC_NUMBER, 8, struct timeval)
#define DMMSG_IOC_GET_JIFFIES               _IOR(DMMSG_IOC_NUMBER, 9, u64)
#define DMMMG_IOC_INT_DATA_CH               _IO(DMMSG_IOC_NUMBER, 10)
#define DMAVE_MSG_DRIVER_RESET              _IO(DMMSG_IOC_NUMBER, 11)
#define DMAVE_MSG_DRIVER_PAUSE              _IO(DMMSG_IOC_NUMBER, 12)
#define DMAVE_MSG_DRIVER_RUN                _IO(DMMSG_IOC_NUMBER, 13)
#define DMAVE_MSG_DRIVER_INTERRUPT          _IO(DMMSG_IOC_NUMBER, 14)
#define DMMSG_IOC_MPG_PAUSE                 _IO(DMMSG_IOC_NUMBER, 15)
#define DMMSG_IOC_MPG_RUN                   _IO(DMMSG_IOC_NUMBER, 16)

#endif
