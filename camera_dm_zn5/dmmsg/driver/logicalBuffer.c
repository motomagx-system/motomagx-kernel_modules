/*
 * Copyright (c) 2007-2008 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 17-Dec-2007  Motorola    Message error status added
 * 21-Jan-2008  Motorola    Retransmission changed
 * 23-May-2008  Motorola    OSS CV Fix
 */

//#define DEBUG

#include <linux/module.h>	/* Kernel module definitions */
#include <linux/init.h>
#include <linux/kernel.h>	/* We will be "in the kernel" mode of execution */

#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/timer.h>
#include <asm/uaccess.h>

#include <asm/semaphore.h>

#include "logicalBuffer.h"



DECLARE_MUTEX(logi_buff_mutex);


/* Internal prototype functions */
void logi_buff_rm_trns_dmy( struct logi_buff_head* pkg_buff_head);
struct logi_pkg *logi_buff_find_empty_pkg(struct logi_buff_head* pkg_buff_head);

/*!
 * Creates the logiBuffer object: alocate memory initializes the buffers
 *
 * @return a pointer to the buffer object or NULL if error.
 */
struct logi_buff_head *logi_buff_create(void)
{
	struct logi_buff_head *pkg_buff_head;

	pr_debug("logi_buff_create: kmalloc\n");

	pkg_buff_head = kmalloc(LOGIBUF_TOTAL_SIZE, GFP_KERNEL | __GFP_DMA);
	if (pkg_buff_head == NULL ){
		printk("ERROR-->logi_buff_create: could not allocate memory\n");
		goto out;
	}
	
	pr_debug("logi_buff_create: kmalloc pkg_buff_head = %X\n", 
		 (int)pkg_buff_head);

	logi_buff_reset(pkg_buff_head);
 out:
	pr_debug("logi_buff_create: return\n");
	return pkg_buff_head;
}

/*!
 * Releases the logiBuffer: deallocate the memory
 *
 * @param  *_logi_buff_head  A pointer the buffer object  
 */
int logi_buff_release(struct logi_buff_head *pkg_buff_head)
{
	kfree((void*)pkg_buff_head);
	return 0;
}

/*!
 * Resets the package buffers to a initila state
 *
 * @param  *_logi_buff_head  A pointer the logical buffer object 
 */
void logi_buff_reset(struct logi_buff_head *pkg_buff_head)
{
	struct logi_pkg *this_pkg;
	int i = 0;

	if ( pkg_buff_head == NULL ){
		printk("ERROR-->logi_buff: null-pointer to reset!!!\n");
		return;
	}

	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex))
		return;
	
	/* init list headers */
	INIT_LIST_HEAD(&pkg_buff_head->msg_emptys_head);
	INIT_LIST_HEAD(&pkg_buff_head->msg_transmits_head);
	INIT_LIST_HEAD(&pkg_buff_head->msg_received_head);

	/* reset sequence numbers */
	pkg_buff_head->host_seq_no = 0;
	pkg_buff_head->dm_seq_no = 0;

	/* set the package buffers */
	this_pkg = (struct logi_pkg*)(pkg_buff_head + 1);

	pr_debug("sizeof(struct logi_buff_head) = %d\n", 
		 sizeof(struct logi_buff_head));
	pr_debug("sizeof(struct logi_pkg) = %d\n", 
		 sizeof(struct logi_pkg));

	for(i = 0 ; i < LOGOBUF_COUNT ; i++){  

		/* set default paramaters */
		logi_pkg_set_channel(i/*for debug*/ , this_pkg);
		this_pkg->state = LBM_EMPTY;
		this_pkg->err_status = 0;
		logi_pkg_set_seqno(i, this_pkg);
		logi_pkg_set_size(0, this_pkg);
		logi_pkg_set_ack(this_pkg);
		logi_pkg_append_chksum(this_pkg);
		logi_pkg_set_id(i, this_pkg);
 
		/* add the item to the list */
		list_add(&this_pkg->list, &pkg_buff_head->msg_emptys_head);
    
		/* move to next msg_item */
		this_pkg = this_pkg + 1;
	} 

	pkg_buff_head->no_of_empty = LOGOBUF_COUNT;

	/* init the first dummy data */
	list_move(pkg_buff_head->msg_emptys_head.next, 
		  &pkg_buff_head->msg_transmits_head);
	this_pkg = list_entry(pkg_buff_head->msg_transmits_head.next,  
			      struct logi_pkg, list);
	/* correct the empty count */
	pkg_buff_head->no_of_empty--;
	/* ste state */
	this_pkg->state= LBM_EMPTY;
	/* set header */
	logi_pkg_set_seqno(pkg_buff_head->host_seq_no++, this_pkg);
	logi_pkg_clear_ack(this_pkg);
	logi_pkg_set_channel(0, this_pkg);
	logi_pkg_set_id(0, this_pkg);
	logi_pkg_set_status(0, this_pkg);
	logi_pkg_set_time(0, this_pkg);

	/* mutex release*/
	up(&logi_buff_mutex);

	return;
}

/*!
 * Finds the buffer where the data should be read when a package is 
 * transmitted 
 * 
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return   Returns a pointer to the package buffer or NULL if error
 */
struct logi_pkg *logi_buff_get_send_pkg(struct logi_buff_head *pkg_buff_head)
{

	struct logi_pkg *pkg;
	
	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex))
		return NULL;

	/* test if there is any messages in the transmit buffer */
	if (list_empty(&pkg_buff_head->msg_transmits_head)){
		printk("ERROR->logi_buff_get_send_pkg: no "
		       "transmit message!!\n");
		pkg = NULL;
		goto out;
	}

	/* find the first msg */
	pkg = list_entry(pkg_buff_head->msg_transmits_head.next, 
			 struct logi_pkg, list);

	switch (pkg->state){
	case LBM_EMPTY:
		pkg->state = LBM_DMY_SENDING;
		break;
	case  LBM_MSG_READY:
		pkg->state = LBM_MSG_SENDING;
		break;
	case LBM_ACK_READY:
		pkg->state = LBM_ACK_SENDING;
		break;  
	case LBM_MSG_SENT:
		pkg->state = LBM_MSG_RESENDING;
		break;
	default:
		printk("ERROR-->logi_buff_get_send_pkg: invalid prev state"
		       ":%d !!\n", pkg->state ); 
		pkg = NULL;
		goto out;
		/* break; */
	}

 out:  
	/* mutex release*/
	up(&logi_buff_mutex);

	return pkg;
  
}

/*!
 * Finds the buffer where the data should be written when a package is received 
 * 
 * @return   Returns a pointer to the package buffer
 */
struct logi_pkg *logi_buff_get_rcv_pkg(struct logi_buff_head *pkg_buff_head)
{
	struct logi_pkg *pkg;
	
	/* reserve empty buffer */
	pkg = logi_buff_get_resvd_pkg(pkg_buff_head);
	if( pkg == NULL){ 
		printk("ERROR-->logi_buff_get_rcv_pkg: no free buffer!!\n");
		goto out;
	} 
 out:
	return pkg;
}

/*!
 * To mark a message have been succesfully send
 *
 * @param  *msg           A pointer to the just send package
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return                Returns a negative value if error
 */
int logi_buff_set_pkg_sent(struct logi_pkg *msg, 
			   struct logi_buff_head *pkg_buff_head)
{
	int error_state;
	
	/* check the type of the package */
	switch ( msg->state ) {
	case LBM_MSG_SENDING:
	case LBM_MSG_RESENDING:
		/* type: message */
		/* mutex wait/gain*/
		if(down_interruptible(&logi_buff_mutex))
			return -ERESTARTSYS;
		/* change state */
		msg->state = LBM_MSG_SENT;
		/* move the messeage to back empty list, unless it is the last messager */
		if( !((msg->list.next == &pkg_buff_head->msg_transmits_head) &&
		      (msg->list.prev == &pkg_buff_head->msg_transmits_head))){
			list_move_tail( &msg->list , 
					&pkg_buff_head->msg_emptys_head);
		}
		/* mutex release*/
		up(&logi_buff_mutex);
		error_state = 0;
		break;
	case LBM_ACK_SENDING:
	case LBM_DMY_SENDING:
		/* type: ack or empty */
		/* remove message */
		msg->state = LBM_EMPTY;
		logi_buff_pkg_release(msg, pkg_buff_head);
		error_state = 0;
		break;
	default:
		/* should not happen */
		printk("Warning-->logi_buff_set_pkg_sent: msg->state" 
		       "!= LBM_MSG_READY or LBM_ACK_READY\n");
		msg->state = LBM_EMPTY;
		logi_buff_pkg_release(msg, pkg_buff_head);
		error_state = -ENOMSG;
		break;
	}
	
	return error_state;
}

/*!
 * To mark a message as received
 * Function:
 * - Check if a new package has been received by checking if the 
 *   sequence number has increased.
 * - If sequence number is not +0 og +1 return error
 * - If dummy is received (+0) release package
 * - If new package is received then set state  
 *
 * @param  *msg      A pointer to the just received package
 * @return   LBM_EMPTY(0): if dymmy package is received
 *           LBM_ACK_RCVD: if ack is received
 *           LBM_MSG_RCVD: if message is received 
 *           Returns a negative value if error
 */
int logi_buff_set_pkg_rcved(struct logi_pkg *pkg, 
			    struct logi_buff_head *pkg_buff_head)
{
	int seq_no;

	/* check if seq has increased */
	seq_no = logi_pkg_get_seqno(pkg);
	if (seq_no == pkg_buff_head->dm_seq_no + 1) {
		/* new message received */
		pkg_buff_head->dm_seq_no++;
	} else if (seq_no == pkg_buff_head->dm_seq_no) {
		/* dummy data has been received */
		logi_buff_pkg_release(pkg, pkg_buff_head);
		return 0;

	} else {
		/* the one or more messages has been lost*/
		printk("ERROR-->logi_buff: sequence number error,"
		       "pkg has been lost or DM reset occurred"
		       "(seq rx: %d expected %d)\n",
		       seq_no, pkg_buff_head->dm_seq_no);
		logi_pkg_pr_hdr(pkg);
		return -EREMOTEIO;
	}

	if(logi_pkg_chk(pkg))
		printk("Error in received message\n");

	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex))
		return -ERESTARTSYS;

	/* move msg/ack to the back og the receivedbuffer */
	list_move_tail(&pkg->list, &pkg_buff_head->msg_received_head);

	/* test if msg or ack */
	if (logi_pkg_get_ack(pkg)) {
		/* ACK received */
		/* set message state to ack-received */
		pkg->state = LBM_ACK_RCVD;
	} else {
		/* msg received */ 
		/* set state received */   
		pkg->state = LBM_MSG_RCVD;
	}
  
	/* mutex release*/
	up(&logi_buff_mutex);
 
	return pkg->state;
}

/*!
 * Finds a free package buffer and set it as reserved
 *
 * @return pointer to the package buffer or NULL if error
 */
struct logi_pkg *logi_buff_get_resvd_pkg(struct logi_buff_head *pkg_buff_head)
{
	struct logi_pkg *pkg;

	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex))
		return NULL;

	/* find empty buffer */
	if ((pkg = logi_buff_find_empty_pkg(pkg_buff_head)) == NULL ) {
		printk("ERROR-->logi_buff_get_resvd_pkg: "
		       "could not reserve buffer !!\n");
		goto out;
	}
  
	/* set the message to not empty */
	pkg->state =  LBM_RESERVED;
	pkg->err_status = 0; /* clear any error status */
	pkg_buff_head->no_of_empty--;
	pr_debug("logi_buff_get_resvd_pkg: no_of_empty-- (%d)\n", pkg_buff_head->no_of_empty);

	/* move this entry to the end of the empty list */
	list_move_tail(&pkg->list, &pkg_buff_head->msg_emptys_head);
 out:
	/* mutex release*/
	up(&logi_buff_mutex);

	return pkg;  
}


/*!
 * Place a package buffer in the transmit list and marks it ready to send
 * Note1: there is currently no priority handeling. Currwntly a FIFO.
 * Note2: ack is determined from the ack bit in the package header
 *
 * @param  *msg           A pointer to the package buffer to send
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return                 negative if error
 */
int logi_buff_place_in_trnsmt(struct logi_pkg *pkg, 
			      struct logi_buff_head *pkg_buff_head)
{

	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex))
		return -ERESTARTSYS;

	/* Change the state of the message */
	if (logi_pkg_get_ack(pkg)==LBM_ACK) {
		pkg->state = LBM_ACK_READY; 
	} else {
		pkg->state = LBM_MSG_READY; 
	}
	
	/* append the messagenumber */
	logi_pkg_set_seqno(pkg_buff_head->host_seq_no, pkg);

	/* handle overturns */
	if (pkg_buff_head->host_seq_no >= LBM_SEQ_MAX) {
		pkg_buff_head->host_seq_no = 0;
	} else {
		pkg_buff_head->host_seq_no++;	
	}
  
	/* append check sum */
	logi_pkg_append_chksum(pkg);

	/* move the message to the end of the transmits list */
	list_move_tail(&pkg->list, &pkg_buff_head->msg_transmits_head);

	/* remove dummy objects in transmit buffer */
	logi_buff_rm_trns_dmy(pkg_buff_head);
 
	/* mutex release*/
	up(&logi_buff_mutex);
   
	return 0;
}

/*!
 * Finds a received package with the specified channel and ack state
 *
 * @param  channel   The channel number 
 * @param  *count    output: where the the number of received messages 
 *                   in buffer of the spevified channel is written.
 * @param  *pkg_buff_head  A pointer the logical buffer object or NULL 
 *                         if error
 */
struct logi_pkg *logi_buff_find_rcvd_pkg(int channel, int *count,
					 struct logi_buff_head *pkg_buff_head)
{
	struct logi_pkg *pkg = NULL;
	struct logi_pkg *entry = NULL;
	struct list_head *ptr;
	*count = 0;
#ifdef DEBUG
	int i = 0;
#endif
	
	pr_debug("logi_buff_find_rcvd_pkg: ch %d \n", channel);

	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex)){
		printk("WARNING-->logi_buff_find_rcvd_pkg"
		       " interrupted by user\n");
		return NULL;
	}

	list_for_each_prev(ptr, &pkg_buff_head->msg_received_head){
		entry = list_entry(ptr, struct logi_pkg, list);

		pr_debug("logi_buff_find_rcvd_pkg: %d -> ch %d, seq %d: %d\n",
		       i++, logi_pkg_get_channel(entry),
		       logi_pkg_get_seqno(entry),*count);

		if ( logi_pkg_get_channel(entry) == channel) {
		  pkg = entry;
		  *count= *count+1;
		}
	}
  	/* mutex release*/
	up(&logi_buff_mutex);

	return pkg;
}


/*!
 * Generates an ack for a given message and place it in the transmit queue
 *
 * @param  *msg           A pointer to the message to be acked
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return                A negative value if error
 */
int logi_buff_gen_ack(struct logi_pkg *msg, 
		      struct logi_buff_head *pkg_buff_head)
{
	struct logi_pkg *ack;
	int err_status = 0;
	
	/* reserve buffer */
	ack = logi_buff_get_resvd_pkg(pkg_buff_head);
	
	/* if no free buffer then return error in some way */
	if (ack == NULL){
		printk("ERROR-->logi_buff_gen_ack: no free buffers\n");
		err_status = -1;
		goto out;
	}  

	/* Write data */
	memcpy(ack->data ,  msg->data , LMB_HEADER_SIZE);

	logi_pkg_set_size( 0, ack);

	/* set ack */
	logi_pkg_set_ack(ack);

	/* move to send */
	if (logi_buff_place_in_trnsmt(ack,  pkg_buff_head)<0){
		printk("ERROR-->logi_buff_gen_ack: "
		       "logi_buff_place_in_trnsmt returned error\n");
		err_status = -2;
		goto out;
	}
 out:
	return err_status;
}

/*! 
 * Removes the transmit dummy. 
 * Note: This finction should only be run after a new message has been 
 *       put into the transmit queue.
 * Note: The mutex handeling must be done by the calling function 
 *
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 */
void logi_buff_rm_trns_dmy( struct logi_buff_head *pkg_buff_head){

	/*  int i = 0;*/
	struct list_head *curser, *next;
	struct logi_pkg *entry;

	/* remove empty dumy data */
	list_for_each_safe(curser, next, &pkg_buff_head->msg_transmits_head) {
		
		entry = list_entry(curser, struct logi_pkg, list);
		
		switch(entry->state){
		case LBM_EMPTY:
			list_move( &entry->list , 
				   &pkg_buff_head->msg_emptys_head);
			
			pkg_buff_head->no_of_empty++;
			pr_debug("logi_buff_rm_trns_dmy: no_of_empty++ (%d)\n", pkg_buff_head->no_of_empty);
			break;
		case LBM_MSG_SENT:
			list_move_tail( &entry->list , 
				   &pkg_buff_head->msg_emptys_head);
			break;
		default:
			break;
		}
	}


#ifdef DEBUG
	if( pkg_buff_head->no_of_empty != logi_buff_count_empty(pkg_buff_head))
			printk("logi_buff_pkg_release: no_of_empty aout of order: count %d", logi_buff_count_empty(pkg_buff_head));
#endif 	
	

	return;
}

/*! 
 * Finds an empty package buffer 
 * Note: The mutex handeling must be done by the calling function 
 *
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 * @return A pointer to a empty package buffer 
 */     
struct logi_pkg *logi_buff_find_empty_pkg(struct logi_buff_head *pkg_buff_head)
{
	struct logi_pkg *pkg;

	/* Check if buffer is empty */
	if (list_empty(&pkg_buff_head->msg_emptys_head)){
		printk("ERROR-->logi_buff_get_resvd_pkg: buffer full!!\n");
		pkg = NULL;
		goto out;
	}
	
	/* find the pointer to the first empty message */
	pkg = list_entry(pkg_buff_head->msg_emptys_head.next, 
			 struct logi_pkg, list);
	
	/* check if the message is in use by other */
	if (pkg->state != LBM_EMPTY){
		/* the buffer is empty and can be used */
		printk("ERROR-->logi_buff_get_resvd_pkg: buffer full, all reserved !!\n");
		pkg = NULL;
		goto out;
	} 
 out:
	return pkg;
}

/*! 
 * Releases a package buffer 
 *
 * @param  *pkg_buff_head  A pointer the logical buffer object  
 */     
void logi_buff_pkg_release( struct logi_pkg *msg, 
			    struct logi_buff_head *pkg_buff_head)
{
	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex)){
		printk("WARNING-->logi_buff_pkg_release"
		       " interrupted by user\n");
		return;
	}
	
	/* check if message is in use */
	switch (msg->state){
	case LBM_MSG_SENDING:
	case LBM_ACK_SENDING:
	case LBM_DMY_SENDING:
	case LBM_MSG_RESENDING:
		/* package buffer is in us */
		msg->state = LBM_DMY_SENDING;
		/* package will be removed when send */
		break;
	/*   case LBM_EMPTY: */
	/*   case LBM_MSG_READY: */
	/*   case LBM_ACK_READY: */
	/*   case LBM_MSG_SENT: */
	/*   case LBM_MSG_RCVD: */
	/*   case LBM_ACK_RCVD: */
	/*   case LBM_RESERVED: */
	default:
		msg->state = LBM_EMPTY;

		/* make sure that there is at least one msg in the transmit buff*/
		if( !((msg->list.next == &pkg_buff_head->msg_transmits_head) &&
		      (msg->list.prev == &pkg_buff_head->msg_transmits_head))){
			/* move to the empty list */

			list_move( &msg->list, &pkg_buff_head->msg_emptys_head);  

			pkg_buff_head->no_of_empty++;
			pr_debug(" logi_buff_pkg_release: no_of_empty++ (%d)\n", pkg_buff_head->no_of_empty);
		} 
		break;
	}

#ifdef DEBUG
	if( pkg_buff_head->no_of_empty != logi_buff_count_empty(pkg_buff_head))
			printk("logi_buff_pkg_release: no_of_empty aout of order: count %d", logi_buff_count_empty(pkg_buff_head));
#endif 	
	/* mutex release*/
	up(&logi_buff_mutex);

	return ;
}
      
/*---------------------------------------------*/
/*------------Debug/Test-functions-------------*/
/*---------------------------------------------*/

/* int logiBuffKPrintStatus(void){ */


/* } */

/* debug */
#ifdef DEBUG
int logi_buff_count_empty(struct logi_buff_head* pkg_buff_head)
{

	int retval = 0;	
	struct list_head *curser, *next;
	struct logi_pkg *entry;
	
	/* remove empty dumy data */
	list_for_each_safe(curser, next, &pkg_buff_head->msg_emptys_head) {
		
		entry = list_entry(curser, struct logi_pkg, list);
		
		if (entry->state == LBM_EMPTY) 
			retval++;
	}
	
	return retval;
}

int logi_buff_kprint_1buff(struct list_head *buff_head)
{
	int retval = 0;	
	struct list_head *curser, *next;
	struct logi_pkg *entry;
	
	/* remove empty dumy data */
	list_for_each_safe(curser, next,  buff_head) {
	  
		retval++;
		entry = list_entry(curser, struct logi_pkg, list);

		logi_pkg_print(entry);

	}
	
	return retval;
}


/* debug */
int logi_buff_print_all(char *p, struct logi_buff_head* pkg_buff_head)
{
	struct logi_pkg *entry;
	int size = 0 ;
	int i = 0;
	char ackSt ='M';
	struct list_head *ptr;

	if (pkg_buff_head == NULL){
		size += sprintf(p+size, "ERROR-->logi_buff_print_all(): "
				"pkg_buff_head == NULL\n");
		return size;
	}
	/* mutex wait/gain*/
	if(down_interruptible(&logi_buff_mutex)){
		return -ERESTARTSYS;
	}

	size += sprintf(p+size, "size of data: %d\n", LOGIBUF_TOTAL_SIZE);

	/* print msg_emptys */
	size += sprintf(p+size, "Empty list: %X\n", 
			(unsigned int)&pkg_buff_head->msg_emptys_head );
	
	list_for_each(ptr, &pkg_buff_head->msg_emptys_head){
	
		entry = list_entry(ptr, struct logi_pkg, list);
		
		if(logi_pkg_get_ack(entry)) {
			ackSt ='A';
		} else { 
			ackSt ='M';
		}
		
		size += sprintf(p+size, "\t Item %d: ch: %d  st: %d sqn: "
				"%d s: %d ack: %c listPtr: %X\n"
				, i, logi_pkg_get_channel(entry), entry->state, 
				logi_pkg_get_seqno(entry),  logi_pkg_get_size(entry), 
				ackSt ,(unsigned int)entry->list.next );
			i++;
	}

	/* print msg_transmits */
	i = 0;
	size += sprintf(p+size, "Transmits list: %X\n", 
			(unsigned int)&pkg_buff_head->msg_transmits_head);
	
	list_for_each(ptr, &pkg_buff_head->msg_transmits_head) {

		entry = list_entry(ptr, struct logi_pkg, list);

		if(logi_pkg_get_ack(entry)) {
			ackSt ='A';
		} else { 
			ackSt ='M';
		}
		
		size += sprintf(p+size, "\t Item %d: ch: %d  st: %d sqn: %d s: "
				"%d ack: %c listPtr: %X %X\n"
				, i, logi_pkg_get_channel(entry), entry->state, 
				logi_pkg_get_seqno(entry),  logi_pkg_get_size(entry), 
				ackSt , (unsigned int)entry->list.next, 
				(unsigned int)entry->list.prev );
		i++;
	}

	/* print msg_recrived */
	i = 0;

	size += sprintf(p+size, "Not emptys list: %X\n", 
			(unsigned int)&pkg_buff_head->msg_received_head);

	list_for_each(ptr, &pkg_buff_head->msg_received_head){

		entry = list_entry(ptr, struct logi_pkg, list);

		if(logi_pkg_get_ack(entry)){
			ackSt ='A';
		} else { 
			ackSt ='M';
		}
		
		size += sprintf(p+size, "\t Item %d: ch: %d  st: %d sqn: %d s: "
				"%d ack: %c listPtr: %X \n"
				, i, logi_pkg_get_channel(entry), entry->state, 
				logi_pkg_get_seqno(entry),  logi_pkg_get_size(entry), 
				ackSt ,(unsigned int)entry->list.next );
		i++;
	}
	/* mutex release*/
	up(&logi_buff_mutex);
	
	return size;
}

void logi_buff_kprint_status(struct logi_buff_head* pkg_buff_head){

	int count = 0;
	int total = 0;
	
	printk("logi_buff: %X\n", (unsigned int)pkg_buff_head);
	
	printk("seq: host: %d, dm %d, empty: ct %d real %d\n", 
	       pkg_buff_head->host_seq_no, 
	       pkg_buff_head->dm_seq_no, 
	       pkg_buff_head->no_of_empty,
	       logi_buff_count_empty(pkg_buff_head));
  
	printk("msg_emptys\n");
	count = logi_buff_kprint_1buff(&pkg_buff_head->msg_emptys_head);
	printk("size: %d\n", count);
	total += count;

	printk("msg_transmits\n");
	count = logi_buff_kprint_1buff(&pkg_buff_head->msg_transmits_head);
	printk("size: %d\n", count);	
	total += count;

	printk("msg_received\n");
	count = logi_buff_kprint_1buff(&pkg_buff_head->msg_received_head);
	printk("size: %d\n", count);
	total += count;
	
	printk("total: %d (%d)\n", total, LOGOBUF_COUNT );

}

#endif
