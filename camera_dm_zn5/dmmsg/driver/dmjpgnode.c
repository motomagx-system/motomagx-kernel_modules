/*
 * Copyright (c) 2007-2008 Motorola, Inc.
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 16-Jun-2007  Motorola    Initial revision.
 * 27-Nov-2007  Motorola    Check of payload size added
 * 17-Dec-2007  Motorola    Byte swapping and message error handeling added
 * 29-Jan-2008  Motorola    Mutex added around read
 * 31-Mar-2008  Motorola    Increased the JPEG timeout from 1 sec to 10 sec
 * 04-Apr-2008  Motorola    IOCTL added
 * 23-May-2008  Motorola    OSS CV fix
 * 17-Jun-2008  Motorola    Pause/Resume implemented
 */

/* #define DEBUG*/ /**/ 

#include <linux/module.h>	/* Kernel module definitions */
#include <linux/init.h>
#include <linux/kernel.h>	/* We will be "in the kernel" mode of execution */
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/timer.h>
#include <asm/uaccess.h>

#include <asm/semaphore.h>

#include "dmave_if.h"
#include "dmjpgnode.h"
#include "logicalMsg.h"
#include "logicalDriver.h"
#include "dmave_if.h"

/*#define DMJPG_ERR_DUMP 0*/
/*#define DMJPG_WRITE_ERR_INTO_JPG_FILE */

#define DMJPG_TIME_OUT (10*HZ) /* 10 sec*/
#define DMJPG_TYPE_DUMP 4

DECLARE_MUTEX(jpgnode_mutex);
DECLARE_MUTEX(jpgnode_mutex_mode);
        
typedef enum{
	DMJPG_MODE_INIT,
	DMJPG_MODE_DATA,
	DMJPG_MODE_DUMP_I,
	DMJPG_MODE_DUMP,
	DMJPG_MODE_EOF
}DMJPG_MODE;


typedef enum{
	DMJPG_ERR_SIZE,
	DMJPG_ERR_TIME,
	DMJPG_ERR_SEQN,
	DMJPG_ERR_LCDR,
	DMJPG_ERR_LSZ	
}DMJPG_ERR;		

enum {
	 DMJPG_RUNNING,
	 DMJPG_PAUSED
}; 

struct strng {
	char txt[64];
	int lng;
};

struct strng dbgstrs[DMJPG_ERR_LSZ] = { 
	{"<<<ERROR-->size not valid-------", 28}, /* DMJPG_ERR_SIZE */
	{"<<<ERROR-->time decreased-------", 28}, /* DMJPG_ERR_TIME */
	{"<<<ERROR-->sequence no.decreased", 28}, /* DMJPG_ERR_SEQN */
	{"<<<ERROR-->CSPI or LC-driver----", 28}  /* DMJPG_ERR_LCDR */
};

#ifdef DMJPG_ERR_DUMP
struct error_dump{
	struct error_dump* next;
	struct logi_pkg pkg;
	int type;
	int last_time;
	int last_seq;
	int data_cnt;
};

struct error_dump* err_dmp = NULL;
#endif

static int last_time = 0;
static int last_seq = 0;
static int data_cnt = 0; 
static int node_mode = DMJPG_MODE_EOF;
static int node_status = DMJPG_RUNNING;
static struct timer_list  timer; 

static int last_stat = 0;
static unsigned int frame_time = 0;


static u64 last_jiffi = 0;
static unsigned int last_pkg_time = 0;

static struct timeval last_host_time ;
static unsigned int avg_time_dm = 0;
static unsigned int avg_time_host = 0;
static unsigned int avg_time_cnt = 0;

/*!
 * Prototypes functions 
 */
static int dmjpgnode_err_chk(struct logi_pkg* pkg, char *buf);
static int dmjpjnode_place_dump(int err_type, struct logi_pkg* pkg, char *buf);
#ifdef DMJPG_ERR_DUMP
static int dmjpgnode_read_one_dump(char *txtbuf, int cp_size, struct error_dump* err_dmp);
static int dmjpgnode_read_dump(char *ubuf, int cp_size);
#endif

static unsigned int avgTimeCalc(unsigned int time_dm, unsigned int time_host)
{
	avg_time_dm = (avg_time_dm*avg_time_cnt + time_dm)/(avg_time_cnt+1);
	avg_time_host = (avg_time_host*avg_time_cnt + time_host)/(avg_time_cnt+1);
	avg_time_cnt++;
	
	return avg_time_cnt;
}

static unsigned int caltOffset(void)
{
	return avg_time_host-avg_time_dm;

	return 0;
}
/*!
 * Call back function for the timer
 * - Make the blocked thread return
 */
static void timer_callback(unsigned long data) 
{ 
	pr_debug("dmjpgnode:timer_callback\n");
	printk("timeout \n");
	logi_drv_int_receive(LMB_CH_DATA1);
}

static int mpg_status(int cmd)
{
	int retval = 0;
	
	printk("mpg_status() entered with status %d\n", cmd);
	if(down_interruptible(&jpgnode_mutex_mode)){
		printk("NOTE->jpgnode_read: returned because of user interrupt\n");
		return -ERESTARTSYS;
	}
	
	switch (cmd){
        case DMMSG_IOC_MPG_PAUSE:
		node_status = DMJPG_PAUSED;
		if ( node_mode == DMJPG_MODE_DATA ){
			del_timer(&timer);
		}
		break;
        case DMMSG_IOC_MPG_RUN:
		node_status = DMJPG_RUNNING;
		if ( node_mode == DMJPG_MODE_DATA ){
			timer.expires = jiffies + DMJPG_TIME_OUT;
			add_timer(&timer);
		}
		break;
        default:
		printk("mpg_status(): invalid parameter\n");
		retval = -EFAULT;                        
	}
	up(&jpgnode_mutex_mode);

	return retval;
}
/*!
 * Called when node is opened 
 */
static int dmjpgnode_open(struct inode *inode, struct file *file)
{
	pr_debug("dmjpgnode_open\n");
	
	last_time = 0;
	last_seq = 0;
	data_cnt = 0; 

#ifdef DMJPG_ERR_DUMP
	if(node_mode !=  DMJPG_MODE_DUMP){
		if( err_dmp == NULL ){
			node_mode = DMJPG_MODE_INIT;
		} else {
			node_mode = DMJPG_MODE_DUMP_I;
		}
	}
#else
	node_mode = DMJPG_MODE_INIT;
#endif
	
	return 0;
}

/*!
 * Called when node is closed 
 */
static int dmjpgnode_release(struct inode *inode, struct file *file)
{
	pr_debug("dmjpgnode_release\n");
	return 0;
}

/*!
 * Reads one message from the received buffer 
 * @param *buf   user space buffer where the message is copied
 * @param count  the size of the user space buffer
 * @return size of copied data or negative value if error
 */
static ssize_t dmjpgnode_cp_usr(char *buf, size_t count, struct logi_pkg *pkg)
{
	int retval = 0;
	int num = 0;
	
	avgTimeCalc(logi_pkg_get_time(pkg), pkg->time*10);
	jiffies_to_timeval(pkg->time, &last_host_time);
	frame_time = logi_pkg_get_u32(LBM_DATA_POS, pkg);
	
	retval = dmjpgnode_err_chk(pkg, buf);
	
	if (retval < 0)	{
		printk("ERROR->dmjpgnode_read(...):dmjpgnode_err_chk"
		       " returned error %d", retval);
		return retval;
	}
	
	if (retval > LBM_PAILOAD_SIZE)
		num = LBM_PAILOAD_SIZE;
	else 
		num = retval;
	
	CH_ENDI(pkg->data+LMB_HEADER_SIZE, num);
	
	if (copy_to_user(buf, pkg->data+LMB_HEADER_SIZE , num )){
		printk("ERROR->dmjpgnode_read(...):copy_to_user returned"
		       " error %d\n", num);
		return  -EFAULT;
	} else {
		data_cnt += num;
	}
	
	return retval;
}

/*!
 * Gets a message, write it to user space and releases the message buffer
 * @param *buf   user space buffer where the message is copied
 * @param count  the size of the user space buffer
 * @return size of copied data or negative value if error
 */
static int dmjpgnode_read_pkg(char *buf, int count)
{
	int retval = 0;
	int size = 0;
	struct logi_pkg *pkg = NULL;

	if (LBM_PAILOAD_SIZE > count){
		printk("ERROR-> user space buffer to small\n");
		return -EOVERFLOW;
	}

	retval = logi_drv_receive_pkg( &pkg, LMB_CH_DATA1);

	del_timer(&timer);
 
	if (retval == -EINTR){
		retval = 0;
		pr_debug("DEBUG:dmjpgnode_read_pkg() set EOF\n");
		goto out;
	} else if (retval < 0){
		printk("ERROR->dmjpgnode_read_pkg(...):logi_drv_receive_pkg"
		       " returned error #%d\n", retval);
		goto out;
	}

	last_stat = logi_pkg_get_status(pkg);
	last_jiffi = pkg->time;
	last_pkg_time = logi_pkg_get_time(pkg);

	if((logi_pkg_get_size(pkg))> count){
		goto out;
	}
	
	if ((retval = dmjpgnode_cp_usr(buf,count, pkg)) < 0){
		printk("ERROR->dmjpgnode_read_pkg(...):dmjpgnode_cp_usr"
		       "; returned error #%d\n", retval);
		goto out;
	} else {
		size = retval;
	}

	if((retval = logi_drv_release_pkg(pkg)) < 0){
		printk("ERROR->dmjpgnode_read_pkg(...):logi_drv_release_pkg"
		       "; returned error #%d\n", retval);
		goto out;
	}
		
	
	retval = size;
 out:
	return retval;
}

/*!
 * Called when the node is read
 */
static ssize_t dmjpgnode_read(struct file *file, char *buf, size_t count, 
			  loff_t *ppos)
{
	int retval = 0;

	pr_debug("dmjpgnode_read\n");

	/* try to gain mutex*/
	if(down_trylock(&jpgnode_mutex)){
		printk("ERROR->jpgnode_read: mutex locked\n");
		return -EALREADY;
	}

	if(down_interruptible(&jpgnode_mutex_mode)){
		printk("NOTE->jpgnode_read: returned because of user interrupt\n");
		retval = -ERESTARTSYS;
		goto out;
	}
	
	switch(node_mode){
	case DMJPG_MODE_INIT:
		node_mode = DMJPG_MODE_DATA;
		break;
	case DMJPG_MODE_DATA:
		if (node_status == DMJPG_RUNNING){
			timer.expires = jiffies + DMJPG_TIME_OUT ;
			add_timer(&timer);	
		}
		break;
#ifdef DMJPG_ERR_DUMP
	case DMJPG_MODE_DUMP_I:
	case DMJPG_MODE_DUMP:
		up(&jpgnode_mutex_mode);
		retval = dmjpgnode_read_dump(buf, count);
		goto out;
#endif /* DMJPG_ERR_DUMP */
	case DMJPG_MODE_EOF:
		up(&jpgnode_mutex_mode);
		pr_debug("dmjpgnode_read EOF\n");
		retval = 0;
		goto out;
	default:		
		node_mode = DMJPG_MODE_DATA;
		break;
	}

	up(&jpgnode_mutex_mode);

	retval = dmjpgnode_read_pkg(buf, count);

	del_timer(&timer);

	if(retval == 0)
		node_mode = DMJPG_MODE_EOF;
 out:
	up(&jpgnode_mutex);

	return retval;	
}

/*!
 * Dummy function: called if data is written to node
 */
static ssize_t dmjpgnode_write(struct file *file, const char *buf,
			   size_t count, loff_t *ppos)
{
	pr_debug("dmjpgnode_write\n");
		
	return count;
}

/*!
 * 
 */
static int dmjpgnode_ioctl(struct inode *inode, struct file *file,
		       unsigned int cmd, unsigned long arg)
{
	int retval = -EAGAIN;
	static struct timeval tv_up;
	static struct timeval tv_tod;
	u64 offset_n;
	static u64 jcount = 0;

	pr_debug(KERN_INFO "dmjpgnode_ioctl called: %X, %d\n", (unsigned int)cmd, 
	       (unsigned int)arg);

	switch(cmd){
	case DMMSG_IOC_GET_STATUS:
		retval = last_stat;
		break;
	case DMMSG_IOC_GET_OFFSET:
		offset_n = caltOffset();
		copy_to_user((u64 *) arg, &offset_n,  sizeof(u64));
		break;
	case DMMSG_IOC_GET_AVG_OFFSET:
//		copy_to_user((struct timeval *) arg, &tv, sizeof(struct timeval));
		break;
	case DMMSG_IOC_GET_PKG_TIME:
		retval = frame_time; 
		break;
	case DMMSG_IOC_GET_UPTIME:
		do_gettimeofday(&tv_tod);
		jcount = get_jiffies_64();
		jiffies_to_timeval(jcount, &tv_up);
		copy_to_user((struct timeval *) arg,&tv_up,  sizeof(struct timeval));
		break;
	case DMMSG_IOC_GET_TIMEOFDAY:
		copy_to_user((struct timeval *) arg,&tv_tod,  sizeof(struct timeval));
		break;
	case DMMSG_IOC_GET_JIFFIES:
		copy_to_user((u64 *) arg, &jcount,  sizeof(u64));
		break;
	case DMMSG_IOC_GET_BOOT_TIME:
		do_gettimeofday(&tv_tod);
		jcount = get_jiffies_64();
		jiffies_to_timeval(jcount, &tv_up);
		tv_tod.tv_sec = tv_tod.tv_sec - tv_up.tv_sec;
		if(tv_tod.tv_usec >= tv_up.tv_usec){
			tv_tod.tv_usec = tv_tod.tv_usec - tv_up.tv_usec;
		} else {
			tv_tod.tv_usec = (tv_tod.tv_usec+1000000) - tv_up.tv_usec;
			tv_tod.tv_sec--;
		}
		copy_to_user((struct timeval *) arg,&tv_tod,  sizeof(struct timeval));
		retval = 0;
		break;
	case DMMSG_IOC_GET_DM_BOOT_OFFSET:
		jiffies_to_timeval(last_jiffi, &tv_tod);

		tv_up.tv_sec = last_pkg_time/1000;
		tv_up.tv_usec = (last_pkg_time%1000)*1000;

		tv_tod.tv_sec = tv_tod.tv_sec - tv_up.tv_sec;
		if(tv_tod.tv_usec >= tv_up.tv_usec){
			tv_tod.tv_usec = tv_tod.tv_usec - tv_up.tv_usec;
		} else {
			tv_tod.tv_usec = (tv_tod.tv_usec+1000000) - tv_up.tv_usec;
			tv_tod.tv_sec--;
		}
		copy_to_user((struct timeval *) arg,&tv_tod,  sizeof(struct timeval));
		retval = 0;
	case DMMMG_IOC_INT_DATA_CH:            
		logi_drv_int_receive(LMB_CH_DATA1);        
		del_timer(&timer);
		retval = 0;
		break;
	case DMMSG_IOC_MPG_PAUSE:
	case DMMSG_IOC_MPG_RUN:
		retval = mpg_status(cmd);
		break;
	default:
	  	printk("NOTE->dmjpgnode: ioctl cmd not supported: %d\n", cmd);
		break;
	}
	
	return retval;

}

static struct file_operations lab_fops = {
  .read		= dmjpgnode_read,
  .write	= dmjpgnode_write,
  .ioctl	= dmjpgnode_ioctl, 
  .open		= dmjpgnode_open,
  .release	= dmjpgnode_release, 
};

#define NAME  "jpgnode"
static int  major;

/*!
 * Initializes the jpeg node
 */
int dmjpgnode_init(void)
{
	major = register_chrdev(0, NAME, &lab_fops);

	if ( major < 0){
		printk("ERROR->dmjpgnode: registered char device failed"
		       " %d.\n", major);
	    return major;
	}

	pr_debug("dmjpgnode: loading "
		 "registered char device with major number"
		 " %d.\n", major);

	init_timer(&timer);

	timer.function = timer_callback;
	timer.data     = 0;

	return 0;
}


/*!
 * Stops the jpeg node
 */
void dmjpgnode_stop(void)
{
	unregister_chrdev(major, NAME);

	logi_drv_int_receive(LMB_CH_DATA1);

	del_timer(&timer);

	pr_debug("dmjpgnode: stopped and unloaded", major);
}



/*!
 * Functions for debug
 *************************************************/

/*!
 * Checking for various errors in a received message
 * - Check if there where any errors during transfer  
 * - Check if the size of payload is valid
 *   - Correct size if not valid 
 * - Check if the time has decreased
 * - Check if the sequence number has decreased
 * @param *pkg the message to be checked
 * @return the size of the message
 */
static int dmjpgnode_err_chk(struct logi_pkg* pkg, char *buf)
{
	int num = logi_pkg_get_size(pkg);

	if (pkg->err_status){
		num = dmjpjnode_place_dump(DMJPG_ERR_LCDR, pkg, buf);
	} else if (num > LBM_PAILOAD_SIZE){
		num = dmjpjnode_place_dump(DMJPG_ERR_SIZE, pkg, buf);
	} else if ( last_seq > logi_pkg_get_seqno(pkg)){
		num = dmjpjnode_place_dump(DMJPG_ERR_SEQN, pkg, buf);
	} /*else if ( last_time > logi_pkg_get_time(pkg)){ 
 		num = dmjpjnode_place_dump(DMJPG_ERR_TIME, pkg, buf); 
		}  */
	
	last_time = logi_pkg_get_time(pkg);
	last_seq = logi_pkg_get_seqno(pkg);

	return num;
}

/*! 
 * Places a dump into the dump list
 * - Allocates a new entry
 * - Copy data to the entry including the package with error
 * - Place the entry at the end of the list
 * @param err_type is the type of error. See enum DMJPG_ERR for types.
 * @param *pkg is the message containing the error
 * @reruen negative value if error
 */
static int dmjpjnode_place_dump(int err_type, struct logi_pkg* pkg, char *buf)
{
	char size = 0;
#ifdef DMJPG_WRITE_ERR_INTO_JPG_FILE
	char dbgtxt[64];
#endif /* DMJPG_WRITE_ERR_INTO_JPG_FILE */
#ifdef DMJPG_ERR_DUMP
	struct error_dump* ptr = err_dmp;
	struct error_dump* new;

	printk("Error catched by jpegnode\n");
	printk("Type %d err_st %d\n", err_type, pkg->err_status );
	printk("last: tm:%d sq:%d - pkg: tm:%d sq:%d - dt_cnt:%d\n",
	       last_time, last_seq, logi_pkg_get_time(pkg), 
	       logi_pkg_get_seqno(pkg), data_cnt );

	/* malloc and set a new entry */
	new =  kmalloc(sizeof(struct error_dump), GFP_KERNEL);
	
	if ( new == NULL){
		printk("ERROR->dmjpjnode_place_dump: could not allocate memory\n");
		return -ENOMEM;
	}

	new->next = NULL;
	new->type = err_type;
	new->last_time = last_time;
	new->last_seq = last_seq;
	new->data_cnt = data_cnt;

	memcpy(&new->pkg, pkg,sizeof(struct logi_pkg) );

	/* place the new entry last in the list */
	if( ptr == NULL ){
		err_dmp = new;	
		return 0;
	}

	while ( ptr->next != NULL) {
		ptr = ptr->next;
	}
	
	ptr->next = new;
#endif

#ifdef DMJPG_WRITE_ERR_INTO_JPG_FILE
	size = snprintf(dbgtxt,64,"%s%4d<<<", dbgstrs[err_type].txt,  pkg->err_status);

	if (copy_to_user(buf+LBM_PAILOAD_SIZE, dbgtxt , size  )){
		printk("ERROR->dmjpjnode_place_dump(...):copy_to_user returned"
		       " error/n" );
		return  -EFAULT;
	}
#endif /* DMJPG_WRITE_ERR_INTO_JPG_FILE */

	printk("Error catched by jpegnode\n");
	printk("Type %d err_st %d\n", err_type, pkg->err_status );
	printk("last: tm:%d sq:%d - pkg: tm:%d sq:%d - dt_cnt:%d\n",
	       last_time, last_seq, logi_pkg_get_time(pkg), 
	       logi_pkg_get_seqno(pkg), data_cnt );

	return size + LBM_PAILOAD_SIZE;
}


#ifdef DMJPG_ERR_DUMP
/*! 
 * Translate a dump entry into a text string
 * @param *txtbuf is the output text buffer 
 * @param cp_size is the size of the text buffer
 * @param *err_dmp is the dump entry
 */
static int dmjpgnode_read_one_dump(char *txtbuf, int cp_size, struct error_dump* err_dmp)
{
	int ptr = 0;
	int i = 0;
	
	ptr += snprintf(txtbuf+ptr, cp_size-ptr, "\nError #%d", err_dmp->type);
	
	switch(err_dmp->type){
	case DMJPG_ERR_SIZE:
		ptr += snprintf(txtbuf+ptr,  cp_size-ptr,":size not valid\n");
		break;
	case DMJPG_ERR_TIME:
		ptr += snprintf(txtbuf+ptr,  cp_size-ptr,":decreased time\n");
		break;
	case DMJPG_ERR_SEQN:
		ptr += snprintf(txtbuf+ptr,  cp_size-ptr,":decreased sequence#\n");
		break;
	case DMJPG_ERR_LCDR:
		ptr += snprintf(txtbuf+ptr,  cp_size-ptr,":CSPI or LC-driver error -->#%d\n",
			       err_dmp->pkg.err_status);
		break;
	default:
		ptr += snprintf(txtbuf+ptr,  cp_size-ptr,":unknown err\n");
		break;
	}
	
	ptr += snprintf(txtbuf+ptr, cp_size-ptr, "last: tm:%d sq:%d - pkg: tm:%d sq:%d - dt_cnt:%d - lcderr: %d\n",
		       err_dmp->last_time, err_dmp->last_seq, 
		       logi_pkg_get_time(&err_dmp->pkg) , logi_pkg_get_seqno(&err_dmp->pkg) , 
		       err_dmp->data_cnt, err_dmp->pkg.err_status );

	while(i < LOGIBUF_MSG_SIZE) {
		ptr += snprintf(txtbuf+ptr, cp_size-ptr, " %2.2x%2.2x %2.2x%2.2x ",
			       err_dmp->pkg.data[i+3], err_dmp->pkg.data[i+2],
			       err_dmp->pkg.data[i+1], err_dmp->pkg.data[i]);
		i += 4;
	}
	
	ptr += snprintf(txtbuf+ptr, cp_size-ptr, "<<<<<<<<<<<\n");
	
	if (ptr > cp_size){
		ptr = cp_size;
	}

	return ptr;
}


/*! 
 * Reads all dump entrys and copyes them to userspace
 * @param *ubuf is the user space buffer where the output is written 
 * @param cp_size is the size of the user space buffer
 * @return the size of the copyed data or negative value if error
 */
static int dmjpgnode_read_dump(char *ubuf, int cp_size)
{
	struct error_dump* ptr = err_dmp;
	int cnt = 0;
	int size = 0;
	char txtbuf[2048];

	if(node_mode == DMJPG_MODE_DUMP_I){
		txtbuf[0] = DMJPG_TYPE_DUMP;
		if (copy_to_user(ubuf,txtbuf , 1 )){
			printk("ERROR->dmjpgnode_read(...):copy_to_user returned"
			       " error/n" );
			return -EFAULT;
		}
		node_mode = DMJPG_MODE_DUMP;
		return 1;
	}
	
	while((ptr != NULL) && (cnt < cp_size )){
		size = dmjpgnode_read_one_dump(txtbuf,2048, ptr);
		if (size > cp_size-cnt) {
			size = cp_size-cnt;
		}
		if (copy_to_user(ubuf+cnt,txtbuf , size )){
			printk("ERROR->dmjpgnode_read(...):copy_to_user returned"
			       " error/n" );
			return -EFAULT;
		}

		err_dmp = ptr->next;
	
		kfree(ptr);
		
		cnt += size;
       		ptr = err_dmp;	
	}
	
	if( err_dmp == NULL){
		node_mode = DMJPG_MODE_EOF;
	}

	return cnt;
}

#endif /* DMJPG_ERR_DUMP */
