/*
 * Copyright (c) 2007-2008 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 17-Dec-2007  Motorola    Message error status added
 * 29-Jan-2008  Motorola    Error handeling of empty miss match updated 
 * 25-Mar-2008  Motorola    Host time stamp added
 * 23-May-2008  Motorola    OSS CV fix
 * 22-Aug-2008  Motorola    Make sure Interupts is only handled when driver is in Run state
 */

/*#define DEBUG*/ /**/

#include "logicalDriver.h"


#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/kernel.h>
#include <asm/unistd.h>
#include <asm/semaphore.h>
#include <linux/wait.h>
#include <linux/config.h>
#include <linux/version.h>
#include <linux/kthread.h>
#include <linux/rwsem.h>
#include <linux/jiffies.h>


#include <cspi.h>


//static DECLARE_WAIT_QUEUE_HEAD(waitq); 

static DECLARE_WAIT_QUEUE_HEAD(loop_waitq);
static DECLARE_WAIT_QUEUE_HEAD(trcv_waitq); 

/*! 
 * Indicates if an error has accured 
 * The variable is 0 is no error
 */
static int dm_error = 0;

/*!
 * Time stamp
 */
static u64 time_stamp = 0;


/*!
 * Indicates the mode og the driver 
 *
 *  LDRV_RUN  : Driver is running
 *  LDRV_PAUSE: Driver is paused (interrupt is disabled) 
 */
static int dm_mode = LDRV_PAUSE;

/*!
 * Indicates is and send or receive is still in progress 
 * and the reset or stop functions should wait intil they finishes
 *
 * Implemented as an read/write semaphore where MULTIBLE READERS are allowed
 * or only ONE WRITER. In this setup the send and recieve functons are the 
 * readers and and the stop and reset functions are writers. Her by must the 
 * writers wait until all reders has returned.
 */
static struct rw_semaphore sem_exit;

/*!
 * Indicates if there has been an interrupt
 * The value is 0 if no interrupt
 */
static int int_from_dm = 0;

/*! 
 * Indicate if tere is data in the send buffer to be send
 * The value indicates the number of not send messages in the buffer
 */
static int data_to_send = 0;

/*!
 * Indicates how many messages awaiting to be read on a channel 
 */
static int rxawait[LOGIDRV_MAX_CHANNEL] = { 0 }; 

/*! 
 * Indicates how many messages that are awating to be send on a channel.
 * This number dos not include ack's.
 */
static int txawait[LOGIDRV_MAX_CHANNEL] = { 0 };

/*!
 * Indicates if a channel has been interrupted bu the "user".
 */
static int usrint[LOGIDRV_MAX_CHANNEL]  = { 0 };

/*!
 * The logical channel buffer object 
 */
static struct logi_buff_head *logi_buffer = NULL;


//static int loop_timed_out = 0;
/*!
 * The timer object
 */
static struct timer_list  loop_timer; 

/*!
 * The process object for the loop
 */
struct task_struct *p;


/*!Function to check if a channel number is valid
 * 
 */
static inline int logi_drv_chk_ch(int channel, const char* name){
	if ((channel >= 0) && (channel < LOGIDRV_MAX_CHANNEL))
		return 0;
	else{
		printk("ERROR-->logi_drv_%s: received channel out of bound %d\n"
		       ,name ,channel);
		return -ECHRNG;		
	}
} 

/*!
 * Internal loop error handeling
 * Function: 
 * - Sets the error code 
 * - Wake up blocked threads
 * 
 * @param error code to be set
 */
void logi_drv_err_handeling(int error)
{
//	pr_debug("-------> Error handeling received %d\n", error);

	dm_error = error;
	
	if (waitqueue_active(&trcv_waitq)){ 
		wake_up_interruptible(&trcv_waitq); 
	}

	return;
}

/*!
 * Timer call back for the loop timeout 
 * Function: stops the timer and sets the error mode
 * to -ETIMEDOUT.
 */
static void loop_timer_callback(unsigned long data) 
{ 

	del_timer(&loop_timer);
	
	pr_debug("logi_buffer->no_of_empty is %d\n", logi_buffer->no_of_empty);

	if (int_from_dm == 0){
		printk("ERROR--> DM299 timed out \n");
		logi_drv_err_handeling(-ETIMEDOUT);
	} else {
		printk("ERROR--> User space timed out \n");
		logi_drv_err_handeling(-ETIME);
	}
	  

}
	
/*!
 * This function is a interrupt service routine 
 * Must be run when the DM299 send an IRQ
 * 
 * @param irq 
 * @param *dev_id
 * @param *regs
 * @return 
 */
irqreturn_t logi_drv_isr(int irq, void *dev_id, struct pt_regs *regs)
{
	int ret_code = 0;
	unsigned int readout = 0;

	if (dm_mode == LDRV_PAUSE) {
		pr_debug("isr ignored ----------------\n");
		return IRQ_HANDLED;
	}

	if((ret_code = gpio_signal_get_data(GPIO_SIGNAL_CAM_RST_B, &readout)) 
	   != 0) {
		printk(KERN_ERR "Encountered error %d reading GPIO signal"
		       " GPIO_SIGNAL_CAM_RST_B!\n", ret_code);
        }
	
	if (!readout){
		pr_debug("dm299 in reset\n");
		return IRQ_HANDLED;
	}
	
	pr_debug("isr ------------------------\n");
	
	int_from_dm = 1;
	
#ifdef HOST_TIME_STAMP_EN
	time_stamp = get_jiffies_64();
#endif
	if (waitqueue_active(&loop_waitq)){ 
		wake_up_interruptible(&loop_waitq); 
	} 
	
	return IRQ_HANDLED;
}

/*! Processe send messages
 * Function: 
 * - If state of a send message is LBM_MSG_SENDING  or LBM_ACK_SENDING 
 *   then a message has been send, and the data_to_send is decreased.
 * - All transmitted messages (also dummys) is marked as transmitted
 *   in the buffer. 
 *
 * @param send_pkg  The send pkg object
 * @return negative if error
 */
int logi_drv_post_send(struct logi_pkg *send_pkg) 
{
	int channel;

	channel = logi_pkg_get_channel(send_pkg);

	if (logi_drv_chk_ch(channel, "post_send")){
		return -ECHRNG;
	}

	/* check if a message has been send */
	switch (send_pkg->state) {
	case LBM_MSG_SENDING:
	case LBM_ACK_SENDING:
		data_to_send--;
		txawait[channel]--;
		break;
	case LBM_DMY_SENDING:		
	case LBM_MSG_RESENDING:
#ifdef DEBUG
		/* catch error */
		if ( data_to_send > 0)
			printk("WARNING-->logi_drv: data_to_send not zero\n");
#endif		
		break;
	default:
		printk("ERROR-->logi_drv: send pkg not right state: %d\n",
		       send_pkg->state);
		return -EILSEQ;
	}
	
	logi_buff_set_pkg_sent(send_pkg, logi_buffer);
	
	return 0;

}

/*!
 * Process received messages and ack's
 * Function: 
 * - If an message is received then variables are set.
 * - If an data msg received then an ack is also prepared.
 * 
 * @param  receive_pkg   The received pkg object
 * @param  rcvd_msg_type The message type
 * @return negative if error
 */
int logi_drv_post_recieve(struct logi_pkg *receive_pkg ) 
{
	
	int channel = 0;
	int rcvd_msg_type = 0;

	channel = logi_pkg_get_channel(receive_pkg);

	if (logi_drv_chk_ch(channel, "post_receive")){
		return -ECHRNG;
	}				

	rcvd_msg_type = logi_buff_set_pkg_rcved(receive_pkg, logi_buffer);

	if(rcvd_msg_type<0){
		printk("ERROR-->logi_drv: in receiving message %d \n",
		       rcvd_msg_type );
		return rcvd_msg_type;
	}

	switch(rcvd_msg_type){
	case LBM_MSG_RCVD:
		if (LMB_CH_DATA1 == channel) {
			/* prepare ACK */
			logi_buff_gen_ack(receive_pkg, logi_buffer);
			txawait[LMB_CH_DATA1]++;
			data_to_send++;
		} else {
			printk("ERROR-->logi_drv: unexpected pkg on channel %d received\n", channel);
		}
		
		/* note: not break, operation continues into next*/
	case LBM_ACK_RCVD:
		rxawait[channel]++;
		pr_debug("pkg on channel %d received \n", channel);
		break;
	case LBM_EMPTY:
		pr_debug("no pkg (dummy) received\n" );
		break;
	}

	return 0;
}

/*!
 * Trancieve full duplex messages 
 * Function: 
 * - Retrieves send and receive objects
 * - Run spi
 * - Runs post processing on both messages
 * - Wake up the blocked threads
 *
 * @return negative value if error
 */
int logi_drv_trancieve(void)
{
	struct logi_pkg *send_pkg;
	struct logi_pkg *receive_pkg;
	int retval = 0;
	
	pr_debug("Entering------>logi_drv_trancieve\n");

	if ((send_pkg = logi_buff_get_send_pkg( logi_buffer))==NULL){
		printk("ERROR-->logi_drv: no data to send. \n");
		return -ENODATA;
	}
  
	if ((receive_pkg = logi_buff_get_rcv_pkg( logi_buffer))==NULL){
		printk("ERROR-->logi_drv: could not reserve buffer.\n");
		return -ENOBUFS;
	}

	send_pkg->time = time_stamp;
	receive_pkg->time = time_stamp;

#ifdef DEBUG
		pr_debug("---Message beeing send---\n");
		
	        pr_debug("--ch%x,id0x%x,st0x%x,sq%x,sz%dstate%d \n",			 
			 logi_pkg_get_channel(send_pkg) ,
			 logi_pkg_get_id(send_pkg) ,
			 logi_pkg_get_status(send_pkg),
			 logi_pkg_get_seqno(send_pkg),
			 logi_pkg_get_size(send_pkg),
			 send_pkg->state);

		//				logi_pkg_print(send_pkg);
#endif
	/* run spi */
	if( (retval = cspi_rxtx((char*)send_pkg->data, (char*)receive_pkg->data, 
				LOGIBUF_MSG_SIZE, DMSPI_MESG) ) < 0) {
		printk("ERROR-->logi_drv: cspi error no. %d \n", retval);
		if (retval == -EOVERFLOW){
			receive_pkg->err_status = retval;
		} else {
			return retval;
		}
	}
#ifdef DEBUG
	printk("---Message received---\n");
	pr_debug("--ch%x,id0x%x,st0x%x,sq%x,sz%dstate%d \n",
		 logi_pkg_get_channel(receive_pkg) ,
		 logi_pkg_get_id(receive_pkg) ,
		 logi_pkg_get_status(receive_pkg), 
		 logi_pkg_get_seqno(receive_pkg),
		 logi_pkg_get_size(receive_pkg),
		 receive_pkg->state);
	//	logi_pkg_print(receive_pkg);
#endif
	if( (retval = logi_drv_post_send(send_pkg)) < 0) {
		printk("ERROR-->logi_drv: transciev\n");
		return retval;
	}
	
	if( (retval = logi_drv_post_recieve(receive_pkg)) < 0) {
		printk("ERROR-->logi_drv: transciev\n");
		return retval;
	}

	if (waitqueue_active(&trcv_waitq)){ 
		wake_up_interruptible(&trcv_waitq); 
	}
  
	pr_debug("Returning from------>logi_drv_trancieve\n");

	return 0;
}

/*!
 * Generates an signal to the DM299 and starts the timer.
 * Function:
 * - Signal using gpio GPIO_SIGNAL_CAM_PD
 * - Start timer for loop time out
 */
void logi_drv_signal_dm(void)
{
	pr_debug("void logi_drv_signal_dm(void) \n");

	gpio_signal_set_data(GPIO_SIGNAL_CAM_PD, GPIO_DATA_LOW);

	loop_timer.expires = jiffies + LOGIDRV_LOOP_TIME_OUT ;
	
	add_timer(&loop_timer);
 
	udelay(DM_INT_SIGN_DELAY);

	gpio_signal_set_data(GPIO_SIGNAL_CAM_PD, GPIO_DATA_HIGH); 
}

/*!
 * Answers the interrupt from the DM299
 * Function: 
 * - Reset the interrupt indicator
 * - Stop the timer 
 * - Run trancieve.
 */

void logi_drv_answer_dm(void)
{
	int loop_error = 0;
	
	int_from_dm = 0;

	del_timer(&loop_timer);
	
	if ( (loop_error = logi_drv_trancieve()) < 0)
		logi_drv_err_handeling(loop_error);
}

/*!
 * This function is a loop which handles the logical channel communication.
 *
 * If the host has data to send then the DM299 is interrupted to make it 
 * ready for a SPI transfer. When the DM299 is ready then it will interrupt the
 * host.
 *
 * When a interrupt is received then the DM299 is ready to trancieve and 
 * the DM299 is then answered by starting a SPI transfer. An interrupt can
 * also be received without an interrupt initial interrupt from the host. 
 *
 *  @param startup void pointer (not used)
 */
int logi_drv_loop(void *startup)
{
	int no_int_send = 1;
	
	pr_debug("int logi_drv_loop( void *parm ):entering loop \n");
  
	while(1){
		wait_event_interruptible(
			loop_waitq,
			(((int_from_dm)||(data_to_send&&no_int_send))&&
			 ((logi_buffer->no_of_empty > LBM_BUFF_RCV_MIN)||
			  (txawait[LMB_CH_CMD])))||kthread_should_stop());

		pr_debug("no_of_empty = %d\n", logi_buffer->no_of_empty );
		pr_debug("to_send = %d\n", data_to_send );  
		pr_debug("tx[0] = %d, rx[0]  = %d \n", 
			 txawait[0], rxawait[0] );
		pr_debug("tx[1] = %d, rx[1]  = %d \n", 
			 txawait[1], rxawait[1] );
		pr_debug("tx[2] = %d, rx[2]  = %d \n", 
			 txawait[2], rxawait[2] );
		pr_debug("ifdm = %d,nointsend = %d \n",
			 int_from_dm ,no_int_send );
		
	
		if(kthread_should_stop()) {

			goto out;

		} else if(logi_buffer->no_of_empty > LOGOBUF_COUNT) {

			printk("ERROR-->logi_buffer->no_of_empty >"
			       " LOGOBUF_COUNT (%d)\n", 
			       logi_buffer->no_of_empty );

			/* make threads return */
			logi_drv_err_handeling(-EDQUOT);
			down_write(&sem_exit); 
			printk("all threads exited \n");
			up_write(&sem_exit);

			logiDrv_reset();

		} else if (int_from_dm){

			pr_debug("Int from DM\n");

			no_int_send = 1;

			logi_drv_answer_dm();

		} else if (data_to_send > 0){

			pr_debug("Sending int\n");

			no_int_send = 0;

			logi_drv_signal_dm();
		}
		/* run loop again to wait for an DM ready interrupt */
	}
 out:	
	del_timer(&loop_timer);
	pr_debug("int logi_drv_loop( void *parm ):exiting loop \n");
	return 0;   
}

/*!
 * Initializes the logical channel driver
 * Function:
 * - Reset driver variables 
 * - Init wait queues 
 * - Init the buffer object
 * - Start the loop thread
 * - request interrupt
 */
int logi_drv_init(void)
{
	int rt_val = 0;
	int i = 0;
	
	pr_debug("void logi_drv_init(void)\n");

	/* init the exit semaphore */
	init_rwsem(&sem_exit);
	down_write(&sem_exit);

	/* init variables */
	int_from_dm = 0;
	data_to_send = 0;
	dm_error = 0;
	dm_mode = LDRV_PAUSE;
	for(i = 0 ; i < LOGIDRV_MAX_CHANNEL; i++){
		rxawait[i] =  0; 
		txawait[i] =  0; 
	}

	/* Init wait queues */
	init_waitqueue_head(&trcv_waitq);
	init_waitqueue_head(&loop_waitq);

	/* init buffer */
	logi_buffer = logi_buff_create();

	if (logi_buffer == NULL){
		return -ENOMEM;
	}
	
	/* start loop thread */
	pr_debug("void logi_drv_init(void):starding thread\n");

	p = kthread_create (logi_drv_loop,0,"logi_drv");

	if ( p == NULL ){
		rt_val = -ECHILD;
		goto abort1;
	}

	wake_up_process (p);
	
	/* init timer */
	init_timer(&loop_timer);
	loop_timer.function = loop_timer_callback;
	loop_timer.data     = 0;
	
	/* Setup interrupt */
	gpio_signal_request_irq(
		GPIO_SIGNAL_DM299_REQ_INT, GPIO_HIGH_PRIO,
		logi_drv_isr, SA_INTERRUPT, "logi_drv", NULL);
	
	up_write(&sem_exit);
	
	pr_debug("void logi_drv_init(void):thread started\n");

	return 0;

 abort1:
	logi_buff_release(logi_buffer);
	
	return rt_val;

}

/*!
 * Stops and cleans up the logical channel driver 
 * Function:
 * - Make all blocked threads wake up and return error
 * - Wait until all send and receive functions has returned
 * - Release the interrupt
 * - Stop the timer 
 * - Stop the loop thread and wait until it finishs 
 * - Release the buffer 
 */
void logi_drv_stop(void)
{
	pr_debug("void logi_drv_stop(void)\n");

	if (logi_buffer == NULL){
		printk("ERROR-->logi_drv:No buffer to stop!!!!\n");
		return;
	}

	logi_drv_err_handeling(-ENODEV);

	pr_debug("void logi_drv_stop(void): waiting for sleeping threads"
		 " to exit\n");

	down_write(&sem_exit); 

	pr_debug("void logi_drv_stop(void): free int\n");
	
	logi_drv_set_mode(LDRV_PAUSE);
	
	gpio_signal_free_irq(GPIO_SIGNAL_DM299_REQ_INT,
			     GPIO_LOW_PRIO);
	
	pr_debug("void logi_drv_stop(void):stopping loop:"
		 " wati for completion\n"); 
	
	del_timer(&loop_timer);

	if ( p != NULL )
		kthread_stop(p);
	else 
		printk("void logi_drv_stop(void):p == NULL!!!!");

	pr_debug("void logi_drv_stop(void):stopping loop: stopped"
		 " --> freeing memory\n");


	logi_buff_release(logi_buffer);

	logi_buffer=NULL;
	

	pr_debug("void logi_drv_stop(void): exiting\n");
	return;
}

/*!
 * Resets the logical channel driver
 * This should be used after an application download
 * Function:
 * - Make all blocked threads wake up and return error
 * - Stop the timer
 * - Reset the buffer
 * - Reset driver variables
 */
int logiDrv_reset(void)
{
	int i = 0;

	pr_debug("-----------logical channel driver reset --------------\n");

	if (!down_read_trylock(&sem_exit)){
		printk("ERROR-->sem_exit not open : driver may be stopped\n");
		return -EFAULT;
	}
	
	up_read(&sem_exit);

	pr_debug("Waiking up blocked threads\n");

	logi_drv_err_handeling(-EINTR);

	pr_debug("Waiting for them to exit\n");

	down_write(&sem_exit); 

	pr_debug("threads exited\nDeleting timer\n");

	del_timer(&loop_timer);

	logi_buff_reset(logi_buffer);

	int_from_dm = 0;
	data_to_send = 0;
	dm_error = 0;

	for(i = 0 ; i < LOGIDRV_MAX_CHANNEL; i++){
		rxawait[i] =  0; 
		txawait[i] =  0; 
	}
	
	up_write(&sem_exit);
	
	pr_debug("returning from logi_drv_reset\n");

	return 0;
}


int logi_drv_set_mode(int mode){

	int irq_init_res = 0;

	pr_debug("logi_drv_set_mode: mode = %d, prior mode %d \n", 
		 mode, dm_mode);

	switch(mode) {
	case LDRV_RUN:
		if (dm_mode == LDRV_RUN)
			break;

		dm_mode = LDRV_RUN;
		break;
	case LDRV_PAUSE:
		if (dm_mode == LDRV_PAUSE)
			break;

		dm_mode = LDRV_PAUSE;
		break;
	default:
		return -ENOSYS;
	}

	return 0;

}

/*!
 * Places a message in the send buffer
 * Function:
 * - Check channel number and if channel is in use
 * - Place message in the transmit buffer and set driver variables
 * - Wake up the loop and go to sleep
 * - Waken up when the message has been sent 
 * - Release the message and set driver variables
 *
 * @param pkg      A pointer to the message object to be send.
 * @param channel  The channel number.
 * @return         Returns negative if error.
 */
int logi_drv_send_pkg( struct logi_pkg *msg, int channel)
{
	int rt_val = 0;

	if (logi_buffer == NULL){
		printk("ERROR-->logical buffer == NULL\n");
		return -EFAULT;
	}

	if (!down_read_trylock(&sem_exit)){
		printk("ERROR-->sem_exit not open in send\n");
		return -EFAULT;
	}

	switch ( channel ) {
	case LMB_CH_CMD:
		/* Only one command at a time*/
		if ( txawait[LMB_CH_CMD] >= 1 ){
			printk("ERROR->logi_drv: CMA allready in queue ch:%d,"
			       " ql: %d\n",channel ,data_to_send );
			rt_val = -EBUSY;
			goto out;
		}
		break;
	/*Note: could be expanded with one or two data channels */
	default:
		printk("ERROR->logi_drv: sending on channel %d is not "
		       "implemted", channel );
		rt_val = -ENOSYS;
		goto out;
	}

	/* check if there is enough space to receive ack and other */
	/* msg when sending */
	if ( logi_buffer->no_of_empty < LBM_BUFF_CMD_MIN ){
		printk("ERROR->logi_drv: buffer full, not enough space"
		       " to trancieve\n");
		rt_val = -ENOSPC;
		goto out;
	}

	/* set channel */

	logi_pkg_set_channel( channel, msg);

	logi_pkg_clear_ack( msg);

 	/* move to transmit buffer */
	pr_debug("void logi_drv_send(...):PlaceTransmit\n");
	
	logi_buff_place_in_trnsmt(msg, logi_buffer);
	
	data_to_send++;
	txawait[channel]++; 

	if (waitqueue_active(&loop_waitq)){ 
		wake_up_interruptible(&loop_waitq); 
	}

	if (wait_event_interruptible(trcv_waitq, (msg->state==LBM_MSG_SENT)|
				 (msg->state==LBM_MSG_RESENDING)|
				     dm_error)){
		printk("NOTE->logi_drv_send(...):interrupted by signal \n"); 
		rt_val = -ERESTARTSYS;
		
	}
	
	if (dm_error){
		rt_val = dm_error;
	}

 out:

	logi_buff_pkg_release( msg, logi_buffer);

	up_read(&sem_exit);

	pr_debug("void logi_drv_send(...): returned %d\n", rt_val);
	
	return rt_val ;
}

/*!
 * Receives a msg or an ack on a specified channel
 * Function:
 * - Validates the channel number
 * - Wait until waked up 
 * - Looks for message in buffer
 * - Return if found or go back to wait
 *
 * @param pkg      A pointer to a pointer where the adress of the received 
 *                 message object should be written.
 * @param channel  The channel number.
 * @return         Returns number of awaiting messages or negative if error.
 */
int logi_drv_receive_pkg( struct logi_pkg **pkg, int channel)
{
	int count = 0;
	int rt_val = 0;

	pr_debug("void logi_drv_receive_pkg(channel: %d)\n", channel);

	if (logi_buffer == NULL){
		printk("ERROR-->logical buffer == NULL\n");
		return -EFAULT;
	}

	if (!down_read_trylock(&sem_exit)){
		printk("ERROR-->sem_exit not open in receive\n");
		return -EFAULT;
	}
	
	if (logi_drv_chk_ch(channel,"receive_pkg")){
		rt_val = -ECHRNG;
		goto out;
	}
	
	usrint[channel] = 0;
	
	do{
		if (wait_event_interruptible(trcv_waitq,rxawait[channel]|
		      dm_error|usrint[channel])){
			printk("NOTE->logi_drv_receive_pkg(...): "
			       "returned as result of signal\n");
			rt_val = -ERESTARTSYS;
			goto out;
		}

		if (dm_error){
			printk("NOTE->logi_drv_receive_pkg(...): returned "
			       "as result of reset or error #%d\n", dm_error);
			rt_val = dm_error;
			goto out;
		}
		
		if (usrint[channel]){
			printk("void logi_drv_receive_pkg(...): "
				 "user interrupt NOE%d\n", logi_buffer->no_of_empty);
			rt_val = -EINTR;
			goto out;
		}
		
		/* search package in channel */
		*pkg = logi_buff_find_rcvd_pkg(channel, &count , logi_buffer);

		rxawait[channel] = count;

		if ( *pkg != NULL){
			rt_val = rxawait[channel];
			goto out;
		} 

	}while (1);
 out:
	up_read(&sem_exit);

	return rt_val;
}

/*!
 * Interrupts a ongoing logi_drv_receive_pkg() interrupt and return -EINTR.
 * Function:
 * - Check channel number
 * - Set usrint
 * - Wake up bloked threads
 *
 * @param channel the channel which should be interrupted
 */
int logi_drv_int_receive(int channel)
{
	pr_debug("logi_drv_int_receive(%d) \n", channel);

	if (logi_drv_chk_ch(channel, "int_receive")){
		return -ECHRNG;
	}
	
	usrint[channel] = 1;

	if (waitqueue_active(&trcv_waitq)){ 
		wake_up_interruptible(&trcv_waitq); 
	}
	
	return 0;
}

/*!
 * Reserves a package buffer 
 * 
 * @return    A pointer to the package buffer or NULL if error
 */
struct logi_pkg *logi_drv_get_empty_pkg(void)
{
	struct logi_pkg *empty_pkg = NULL;

	pr_debug("logi_drv_get_empty_pkg\n");

	if (!down_read_trylock(&sem_exit)){
		printk("ERROR-->sem_exit not open \n");
		return NULL;
	}

	empty_pkg = logi_buff_get_resvd_pkg(logi_buffer);

	up_read(&sem_exit);

	return empty_pkg;
}

/*!
 * Releases a package buffer 
 * Function:
 * - Releases the pkg 
 * - Wake up loop if stoped by (logi_buffer->no_of_empty >= LBM_BUFF_MIN)
 * 
 * @param     A pointer to the package buffer which is to be relesased 
 */
int logi_drv_release_pkg(struct logi_pkg *pkg)
{
	int channel;
	int retval = 0;

	pr_debug("logi_drv_release_pkg\n");

	if (!down_read_trylock(&sem_exit)){
		printk("ERROR-->sem_exit not open in release\n");
		return -EFAULT;
	}
	
	switch (pkg->state) {
	case LBM_MSG_RCVD:
	case LBM_ACK_RCVD:
		channel = logi_pkg_get_channel(pkg);
		if (logi_drv_chk_ch(channel, "release_pkg")){
			retval = -ECHRNG;
			goto out;
		}
		rxawait[channel]--;		
		break;	
	case LBM_EMPTY:
		printk("ERROR-->try to release empty pkg \n");
		retval = -EFAULT;
		goto out;		
	default:
		break;
	}

	logi_buff_pkg_release( pkg, logi_buffer);

	if (waitqueue_active(&loop_waitq)){ 
		wake_up_interruptible(&loop_waitq); 
	}
 out:
	up_read(&sem_exit);

	return retval;

}

EXPORT_SYMBOL(logi_drv_set_mode);
