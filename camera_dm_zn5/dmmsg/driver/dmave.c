/*
 * Copyright (c) 2007-2008 Motorola, Inc.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 17-Dec-2007  Motorola    Byte swappint added
 * 29-Jan-2008  Motorola    Mutex added around read
 * 23-May-2008  Motorola    OSS CV fix
 * 03-Sep-2008  Motorola    OSS fix
 */
#include <linux/module.h>	/* Kernel module definitions */
#include <linux/init.h>
#include <linux/kernel.h>	/* We will be "in the kernel" mode of execution */
    
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <linux/timer.h>
#include <asm/uaccess.h>

#include <asm/semaphore.h>

#include "logicalDriver.h"
#include "logicalMsg.h"
#include "dmave.h"
#include "dmave_if.h"
#include "dmave_logi_drv.h"
#include "dmjpgnode.h"

/*============================================================================*/ 
DECLARE_MUTEX(dmave_read_mutex);

/*============================================================================*/ 
static ssize_t dmave_read(
		struct file * file, 
		char *buf, 
		size_t count, 
		loff_t * ppos )  
{
	dmave_msg_t dmave_msg;
	struct logi_pkg *imgr_logi_pkg = NULL;
	int ret_val = -EAGAIN;
	int size = 0;

	pr_debug("DM299 AVE-Host msg protocol: read called; count %d\n",
		count);

	if (IMGR_MSG_HEADER_LEN > count) {
		return -EAGAIN;
	}

	/* try to gain mutex*/
	if(down_trylock(&dmave_read_mutex)){
		printk("ERROR->dmave_read: mutex locked\n");
		return -EALREADY;
	}
	
	pr_debug(" -Get received message\n");

	if ((ret_val =
		logi_drv_receive_pkg(&imgr_logi_pkg, IMG_MSG_CMD_CH)) < 0) {
		pr_debug(" -logi_drv_receive_pkg failed\n");
		goto imgr_read_exit;
	}

	pr_debug(" - pkg address: %X", (unsigned int)imgr_logi_pkg);
	
	/* copy header to dmave_msg */
	dmave_msg.cam_header.message_id = 
		(((logi_pkg_get_ack(imgr_logi_pkg)) << 8)|  
		 logi_pkg_get_id(imgr_logi_pkg));
       	dmave_msg.cam_header.message_status = 
		logi_pkg_get_status(imgr_logi_pkg);
	dmave_msg.cam_header.message_size =
		logi_pkg_get_size(imgr_logi_pkg);
	dmave_msg.cam_header.message_time =
		logi_pkg_get_time(imgr_logi_pkg);
	dmave_msg.cam_header.message_seq_num =
		logi_pkg_get_seqno(imgr_logi_pkg);
	dmave_msg.cam_header.message_checksum =
		logi_pkg_get_u32(LBM_CHKSM_POS, imgr_logi_pkg);
	jiffies_to_timeval(imgr_logi_pkg->time, 
			   &dmave_msg.cam_header.host_time_stamp);

	size = IMGR_MSG_HEADER_LEN;

	pr_debug(" - message_size: %d",dmave_msg.cam_header.message_size);

	/* copy payload to dmave_msg */
	if(IMGR_MSG_DATA_LEN < dmave_msg.cam_header.message_size){
		printk("ERROR-> message_size overflow in dmave read\n");
		ret_val = -EIO;
		logi_drv_release_pkg(imgr_logi_pkg);
		goto imgr_read_exit;
	}

	CH_ENDI_CP(dmave_msg.message_content, 
		   &(imgr_logi_pkg->data[LBM_DATA_POS]),
		   dmave_msg.cam_header.message_size);

	size += dmave_msg.cam_header.message_size;

	/* copy dmave to user space */
	if(count <= size) {
		size = count;
	}

	pr_debug("\nSize %d,Count %d ", size, count);

	if(copy_to_user(buf, &dmave_msg, size)){
		printk("ERROR-> copy to user space in dmave read\n");
		ret_val = -EFAULT;
		goto imgr_read_exit;
	}

	if((ret_val = logi_drv_release_pkg(imgr_logi_pkg))<0){
		printk("ERROR-> in releasing message in dmave read\n");
	} else {
		ret_val = size;
	}
	
 imgr_read_exit:
	up(&dmave_read_mutex);

	return ret_val;
}


/*==================================================================================*/ 
static ssize_t dmave_write (
		struct file * file, 
		const char *buf, 
		size_t count, 
		loff_t * ppos )
{
	dmave_msg_t dmave_msg;
	struct logi_pkg *imgr_logi_pkg = NULL;
	int ret_val = -EAGAIN;
	unsigned int num_data = 0;
	pr_debug("DM299 AVE-Host msg protocol: write called\n");

	if (NULL == buf) {
		return -EAGAIN;
	}

	if (IMGR_MSG_LEN < count) {
		return -EAGAIN;
	}

	if (IMGR_MSG_HEADER_LEN > count) {
		return -EAGAIN;
	}

	pr_debug(" -write message  buf-id MSB: %d\n", *(buf + 4));
	pr_debug(" -write message  buf-id LSB: %d\n", *(buf + 5));

	pr_debug(" -Initialize payload array\n");

	pr_debug(" -Copy_from_user\n");
	if ((ret_val = copy_from_user(&dmave_msg, buf, count))) {
		pr_debug(" -copy from user failed; err: %d\n", ret_val);
		return ret_val;
	}

	pr_debug(" -write message  id: %u\n", dmave_msg.cam_header.message_id);
	pr_debug(" -write message status: %u\n",
			    dmave_msg.cam_header.message_status);
	pr_debug(" -write message   size: %u\n",
			    dmave_msg.cam_header.message_size);

	pr_debug(" -Get an empty package from logical driver\n");
	if (NULL == (imgr_logi_pkg = logi_drv_get_empty_pkg())) {
		pr_debug(" -logi_drv_get_empty_pkg failed\n");
		return -ENOMEM;
	}

	logi_pkg_set_id(dmave_msg.cam_header.message_id, imgr_logi_pkg);
	logi_pkg_set_status(dmave_msg.cam_header.message_status,
			      imgr_logi_pkg);
	logi_pkg_set_size(dmave_msg.cam_header.message_size,
			    imgr_logi_pkg);

	num_data = count - IMGR_MSG_HEADER_LEN;

	if(num_data > 0){
		CH_ENDI_CP(&(imgr_logi_pkg->data[LBM_DATA_POS]),
			   dmave_msg.message_content, num_data);
	}

	pr_debug(" -Send the package - and wait\n");

	if ((ret_val = logi_drv_send_pkg(imgr_logi_pkg, IMG_MSG_CMD_CH))) {
		printk(" -Send pkg failed with error #%d\n",ret_val );
		return ret_val;
	} else {
		pr_debug(" -Send pkg succeded\n");
		ret_val = count;
	}
	return ret_val;
}

/*==================================================================================*/ 
static int dmave_ioctl 
    (
     struct inode * inode, struct file * file, unsigned int cmd,
     unsigned long arg )  
{
	int ret_val = -EAGAIN;
	pr_debug("DM299 AVE-Host msg protocol: ioctl called\n");
	
	switch(cmd)
	{
		case DMAVE_MSG_DRIVER_RESET:
			logiDrv_reset();
			ret_val = 0;
			break;
		case DMAVE_MSG_DRIVER_PAUSE:
			logi_drv_set_mode(LDRV_PAUSE);
			ret_val = 0;
			break;
		case DMAVE_MSG_DRIVER_RUN:
			logi_drv_set_mode(LDRV_RUN);
			ret_val = 0;
			break;
		case DMAVE_MSG_DRIVER_INTERRUPT:
			logi_drv_int_receive(LMB_CH_CMD);
			ret_val = 0;
			break;
		default:
			break;
	}
	return ret_val;
}


/*==================================================================================*/ 
static int dmave_open  ( struct inode * inode, struct file * file )  {
	pr_debug("DM299 AVE-Host msg protocol: open called\n");
	return 0;
}


/*==================================================================================*/ 
static int dmave_release  ( struct inode * inode, struct file * file )  {
	pr_debug("DM299 AVE-Host msg protocol: release called\n");
	return 0;
}


/*==================================================================================*/ 
/*==================================================================================*/ 
#ifdef DEBUG
static struct proc_dir_entry *dmave_dir, *parent_dir = NULL;
static int dmave_read_proc (
	char *buf, char **start, off_t pos, int count, int *eof,
	void *data )  {
	char *p = buf;
	p += sprintf(p, "dmavedrv Status: \n");

	return (p - buf);
}
#endif /*DEBUG*/

static struct file_operations dmave_fops = {
	.read = dmave_read, 
	.write = dmave_write, 
	.ioctl = dmave_ioctl, 
	.open = dmave_open, 
	.release = dmave_release, 
};
static int major = 0;

#define NAME  "avehostdrv"
    
/* When the module is loaded, execute the driver initialization routine. */ 
static int __init dmave_init(void) 
{
	int retval = 0;
	
	pr_debug("DM299 AVE-Host msg protocol: In dmavemod_init\n");
	
	/* Use major number of 0 to get one dynamically assigned. */ 
	major = register_chrdev(0, NAME, &dmave_fops);
	
	if ( major < 0){
		printk("ERROR->dmjpgnode: registered char device failed"
	       " %d.\n", major);
	    return major;
	}
	
	
	pr_debug(" -avehostdrv; major number is: %d", major);
	    
#ifdef DEBUG
	    /* Install /proc interface */ 
	if ((dmave_dir =
		create_proc_entry("dmavedrv", 0, parent_dir)) == NULL) {
			pr_debug("DM299 AVE-Host msg protocol: Unable to create /proc entry\n");
		return -ENOMEM;
	}
	else {
		dmave_dir->read_proc = dmave_read_proc;
		dmjpgnode_init();
		logi_drv_init();
	}
#else
    retval = dmjpgnode_init();
    
    if(retval < 0){
    	printk("ERROR->dmjpgnode:  dmjpgnode init failed"
	       " %d.\n", retval);
		goto abort1;
    }
    	
    retval = logi_drv_init();

    if(retval < 0){
    	printk("ERROR->dmjpgnode: logi drv init failed"
	       " %d.\n", retval);
		goto abort2;
    }
    
#endif /*DEBUG*/
	return 0;
abort2:
	dmjpgnode_stop();
abort1:
	unregister_chrdev(major, NAME);

	return retval;
}


/* Just before the module is removed, execute the stop routine. */ 
static void __exit dmave_stop(void) 
{
	pr_debug("DM299 AVE-Host msg protocol: In dmavemod_stop.\n");
	logi_drv_stop();
	dmjpgnode_stop();
	unregister_chrdev(major, NAME);
	remove_proc_entry("dmavedrv", 0);
	return;
}

module_init(dmave_init);
module_exit(dmave_stop);
MODULE_AUTHOR("Motorola");
MODULE_DESCRIPTION("DMMSG char device driver");
#ifdef MODULE_LICENSE
    MODULE_LICENSE("GPL");
#endif /* MODULE_LICENSE  */

