/*
 * Copyright (C) 2007 Motorola, Inc.
 */ 

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 */

#include "logicalMsg.h"
#include <linux/kernel.h>

#ifdef DEBUG
void logi_pkg_print(struct logi_pkg *msg)
{
	printk("logi_pkg at 0x%X\n", (unsigned int)msg);

	printk("seq %d, id %d, ch %d ack 0x%X,st %d size %d, (st %d)\n",
	       logi_pkg_get_seqno(msg), 
	       logi_pkg_get_id(msg),
	       logi_pkg_get_channel(msg),
	       logi_pkg_get_ack(msg),
	       logi_pkg_get_status(msg),
	       logi_pkg_get_size(msg),
	       msg->state);
}
#endif
