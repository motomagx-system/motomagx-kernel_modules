/*
 * Copyright (C) 2007 Motorola.
 */
/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 12-Dec-2007  Motorola    Running MESG mode at 32 bit and ZERO clock divider.
 */

#define DEBUG
/* #undef DEBUG */

/* CSPI1 REG ADR */
#define BASE      0x50030000
#define RXDATA    BASE
#define TXDATA    0x50030004
#define CONREG    0x50030008
#define INTREG    0x5003000c
#define DMAREG    0x50030010
#define STATREG   0x50030014
#define PERIODREG 0x50030018
#define TESTREG   0x5003001c

/* EXTERNAL CLOCKS ADR */
#define AMLPMRE2 0x5004802C
#define AMLPMRE1 0x50048028

/* CONREG */
#define XCH 2
#define EN  2
#define SMC 3

/* STATREG */
#define TC 7

/* INTREG */
#define TCEN 7

/* TESTREG */
#define LBC 14

/* AMLPMRE2 */
#define CLOCK 2

/* CSPI1 SIZE */
#define FRAME_SIZE 8

/* iomux fix fixup */
#define IOMUX_COM018 0x50040018 /* IOMUX_COM +18 */


/* Default modes to place in CONREG */
#define BOOT_MODE 0x0071107B
#define MESG_MODE 0x01F0107B
#define APPL_MODE 0x0070107B
#define TEST_MODE 0x0FF1107B

/* proc debug */
#ifdef DEBUG
int cspi_read_procmem(char *buf, char **start, off_t offset,
		      int count, int *eof, void *data);
int cspi_write_procmem(struct file *f, const char *buf, unsigned long cnt,
		       void *data);
#endif
//int string_to_number(char *s);

int cspi_rxtx_dma(char *data_tx, char *data_rx, unsigned size,unsigned mode);
int cspi_app_down_int(char *data_tx,char *data_rx, unsigned size);
int cspi_init_intr(void);
int cspi_stop_intr(void);
int cspi_start(unsigned mode);
int cspi_stop(void);
int cspi_wakeup(int i);

/*
  This structure represents the CSPI SDMA channels.
 */
typedef struct {
	/*! Virtual addres of the Tx buffer */
	char *tx_buf;
       /*! Virtual addres of the Rx buffer */
	char *rx_buf;
	/*! Physical address of the SDMA buffers */
	dma_addr_t tx_phy_addr;
	dma_addr_t rx_phy_addr;
	/*! Amount of data available to transfer/receive*/
	int count;
        /* DMA Channel Numbers*/
        int txdma_ch;
        int rxdma_ch;    
	/* Offset within read buffer */
	int offset;
	int overflow_error;
        /* DMA access word size */
    unsigned long word_size:8;
}CSPI_SDMA_PARAMS_T;

int cspi_dmatxrx(CSPI_SDMA_PARAMS_T *dma_params);
/*
 * setup_iomux_cs()
 *
 * Setup iomuxing in a state where the cspi driver controlls the chip
 * select (CS) instead of letting the cspi hardware controll the
 * CS. This is done to allow transfering more than 512 bytes under one
 * CS.
 */
static inline void setup_iomux_cs(void) {
	iomux_config_mux(SP_SPI1_SS1, OUTPUTCONFIG_DEFAULT,
			 INPUTCONFIG_NONE);
	gpio_signal_set_data(GPIO_SIGNAL_DMSPI_CS, GPIO_DATA_HIGH);
}

/*
 * deconf_iomux_cs()
 *
 * Return iomuxin to the same state as before call to setup_iomux_cs()
 */
static inline void deconf_iomux_cs(void) {
	/*set spi1_ss1 back to ss1*/
	iomux_config_mux(SP_SPI1_SS1, OUTPUTCONFIG_FUNC1,
			 INPUTCONFIG_NONE);
}
