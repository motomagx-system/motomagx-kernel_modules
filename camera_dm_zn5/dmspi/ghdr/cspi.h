/*
 * Copyright (C) 2007 Motorola.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 05-Dec-2007  Motorola    Added MESG Mode.
 */


#ifndef __CSPI_INTERFACE_H__
#define __CSPI_INTERFACE_H__

int cspi_rxtx(char *data_tx, char *data_rx, unsigned size, unsigned mode);

int cspi_start(unsigned mode);

int cspi_stop(void);

typedef enum {
	DMSPI_BOOT = 0,
	DMSPI_MESG = 1,
	DMSPI_APPL = 2,
	DMSPI_TEST = 3,
	DMSPI_STOP = 255,
} cspi_mode_t;

#endif				/* __CSPI_INTERFACE_H__ */

