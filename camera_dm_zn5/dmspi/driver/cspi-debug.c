/*
 * Copyright (C) 2007 Motorola.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 05-Dec-2007  Motorola    Removed older Proc commands and added new commands 
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/module.h>
#include <asm/arch/clock.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <asm-arm/mot-gpio.h>
#include <linux/proc_fs.h>
#include <asm/dma.h>
#include <linux/dma-mapping.h>
#include <linux/random.h>
#include <asm/types.h>
#include <cspi-int.h>
#include <cspi.h>
#ifdef DEBUG
int dmspi_myprint_all(void)
{
	unsigned long reg;
	printk("##################################################\n");
	//reg = __raw_readl(IO_ADDRESS(RXDATA));
	//printk("rx_fifo:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(CONREG));
	printk("ctrl_reg:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(INTREG));
	printk("int_reg:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(STATREG));
	printk("stat_reg:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(PERIODREG));
	printk("period_reg:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(TESTREG));
	printk("test_reg:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(DMAREG));
	printk("dma_reg:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(AMLPMRE2));
	printk("AMLPMRE2:\t0x%8.8lx\n",reg);
	reg = __raw_readl(IO_ADDRESS(AMLPMRE1));
	printk("AMLPMRE1:\t0x%8.8lx\n",reg);
	printk("==================================================\n");
	return 0;
}


int cspi_read_procmem(char *buf, char **start, off_t offset,
		      int count, int *eof, void *data)
{
	int len = 0;
	len += sprintf(buf+len, "Hello\n\n");

	cspi_start(DMSPI_BOOT);
	cspi_stop();

	return len;
}

int cspi_write_procmem(struct file *f, const char *buf, unsigned long cnt,
		       void *data)
{
	int nr = 0,i = 0, j= 0, k = 0;
	unsigned long time;
	unsigned size,iteration,fails[1000],total_errors=0;
    char databuf[10];
    char *dtx,*drx=NULL;
	unsigned long reg;
	unsigned char c = 1;

	if (cnt > 10) {
		return cnt;
	}
	if (copy_from_user(databuf, buf, cnt)) {
		return 0;
	}
	nr = simple_strtol(databuf,NULL,0);
	switch(nr) {
	case 1:
		dmspi_myprint_all();
		break;
	case 2:
		printk("cspi : We got 2, calling  cspi_rxtx(DMSPI_BOOT)\n");
        size = 532;iteration=1000;
		dtx = kmalloc(size,GFP_KERNEL | __GFP_DMA);
		drx = kmalloc(size,GFP_KERNEL | __GFP_DMA);
		if (!dtx || !drx )
        {
           printk("No Memory for dtx and drx\n"); 
		   return -ENOMEM;
        }
        memset(dtx,0xff,size);
        memset(drx,0,size);
        memset(fails,0,sizeof(fails));
	    for(i=0;i<size;i++)
        {
           dtx[i] = i+1;
        }
        cspi_start(DMSPI_BOOT);
        for(i=0;i<iteration;i++)
        {
           printk("Transferring Iteration:%d\n",i);
           __set_bit(LBC,(long *)IO_ADDRESS(TESTREG));
           cspi_rxtx(dtx,drx,size,DMSPI_BOOT); 
                  
           for(j=0;j<size;j++)
           {
              if(dtx[j]!=drx[j])
              {
                 fails[i]++;
		         printk("Tx[%d]=%d \t Rx[%d]=%d\n",j,dtx[j],j,drx[j]);
              }
           }
           memset(drx,0,size);
        } 
		cspi_stop();
		printk("cspi : We got 2, calling  cspi_rxtx(DMSPI_BOOT) END\n");
        for(i=0;i<iteration;i++)
        {
           printk("Fails[%d]=%d\n",i,fails[i]);
		   total_errors+=fails[i];
           //  printk("Tx[%d]=%d \t Rx[%d]=%d\n",i,dtx[i],i,drx[i]);
       	}   
        printk("The total Errors:%d",total_errors);        
		
		kfree(dtx);
        kfree(drx);
		break;
	case 3:
        printk("cspi : We got 3, calling  App Download\n");
		size = 128;
		dtx = kmalloc(size, GFP_KERNEL | __GFP_DMA);
        drx = kmalloc(size, GFP_KERNEL | __GFP_DMA);
        memset(dtx,0,size);
        memset(drx,0,size);
		if (!dtx || !drx )
        {
           printk("No Memory for dtx and drx\n"); 
		   return -ENOMEM;
		}
	    for(i=0;i<size;i++)
        {
                 dtx[i] = i+1;
        }
	cspi_start(DMSPI_APPL);
        __set_bit(LBC,(long *)IO_ADDRESS(TESTREG));
        cspi_rxtx(dtx,drx,size,DMSPI_APPL);
	cspi_stop();
        for(i=0;i<size;i++)
           printk("Tx[%d]=%d \t Rx[%d]=%d\n",i,dtx[i],i,drx[i]);
                
	kfree(dtx);
        kfree(drx);
	break;
	case 4:
        printk("cspi : We got 4, calling  MSG Xfer \n");
        size = 532;iteration=1000;
		dtx = kmalloc(size, GFP_KERNEL | __GFP_DMA);
        drx = kmalloc(size, GFP_KERNEL | __GFP_DMA);
        memset(dtx,0,size);
        memset(drx,0,size);
		if (!dtx )
        {
          printk("No Memory for dtx and drx\n"); 
		  return -ENOMEM;
        }
	    for(i=0;i<size;i++)
        {
          dtx[i] = i+1;
        }
		cspi_start(DMSPI_MESG);
        __set_bit(LBC,(long *)IO_ADDRESS(TESTREG));
        memset(fails,0,sizeof(fails));
        for(i=0;i<iteration;i++)
        {
           printk("Iteration No:%d\n",i);
           cspi_rxtx(dtx,drx,size,DMSPI_MESG);
           for(j=0;j<size;j++)
           {
              if(dtx[j]!=drx[j])
              {
                 fails[i]++;
		         //printk("Tx[%d]=%d \t Rx[%d]=%d\n",j,dtx[j],j,drx[j]);
               }
           }
           memset(drx,0,size);
		}
        cspi_stop();
		printk("cspi : We got 4, calling  MSG Xfer End\n");     
        for(i=0;i<iteration;i++){
           printk("Fail[%d]=%d\n",i,fails[i]);
		   total_errors+=fails[i];
        }
		printk("The total Errors:%d",total_errors);
        //for(i=0;i<size/;i++)
        //printk("Tx[%d]=%d \t Rx[%d]=%d\n",i,dtx[i],i,drx[i]);
        kfree(dtx);
        kfree(drx);
		break;
	default:
		printk("cspi : We got nr=%d, not implemented\n",nr);
		break;
	}
	return cnt;
}

#endif /* #ifdef DEBUG */
