/*
 * Copyright (C) 2007 Motorola.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 20-Nov-2007  Motorola    Solved CSPI tristate mode problem.
 * 05-Dec-2007  Motorola    Initialized SDMA channels during Module Init.
 * 12-Dec-2007  Motorola    Support for 32 bit DMA.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/module.h>
#include <asm/arch/clock.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <asm-arm/mot-gpio.h>
#include <linux/proc_fs.h>
#include <asm/dma.h>
#include <linux/dma-mapping.h>
#include <linux/random.h>
#include <asm/types.h>
#include <cspi-int.h>
#include <cspi.h>

/* cspi state */
static unsigned state = DMSPI_STOP;
//#include <intr-test.c>
static struct semaphore sem;
CSPI_SDMA_PARAMS_T *cspi_dma_params=NULL;

#define LIM 1000

/*
 * cspi_start()
 *
 * Called if cspi_rxtx() was called with a mode different from current
 * state. updates the state by configuring iomux and the SPI1
 * configuration register (CONREG)
 *
 * Return 0 if ok and negative if error.
 */
int cspi_start(unsigned mode)
{
	int ret = 0;
	pr_debug("cspi: Starting CSPI1 in state/mode %u...\n",mode);
	__set_bit(CLOCK,(long *)IO_ADDRESS(AMLPMRE2));
	
	__raw_writel(0x0, IO_ADDRESS(CONREG));
	state = mode;
	
	if (state == DMSPI_BOOT) {
		__raw_writel(BOOT_MODE, IO_ADDRESS(CONREG));
		setup_iomux_cs();
	} else if (state == DMSPI_MESG) {
		__raw_writel(MESG_MODE, IO_ADDRESS(CONREG));
		setup_iomux_cs();
	} else if (state == DMSPI_APPL) {
		__raw_writel(APPL_MODE, IO_ADDRESS(CONREG));
	} else if (state == DMSPI_TEST) {
		__raw_writel(TEST_MODE, IO_ADDRESS(CONREG));
	}  else {
		ret= -ENOSYS;
		printk(KERN_ERR "cspi: State/mode %u not supported\n",state);
		goto exit;
	}
 exit:
	pr_debug("cspi: Starting CSPI1 in mode %u, completed\n",mode);
	return ret;
}

/*
 * cspi_stop()
 *
 * Depending on our current state reconfigure iomux and thereafter
 * shut down input clock to SPI1 and SPI1.
 *
 * Retrun 0 on ok and negative iff error.
 */
int cspi_stop(void)
{
	int ret = 0;
	pr_debug("cspi: Stopping CSPI1...\n");
	if (state == DMSPI_BOOT) {
		deconf_iomux_cs();
	} else if (state == DMSPI_MESG) {
		deconf_iomux_cs();
	} else if (state == DMSPI_APPL) {
	} else if (state == DMSPI_TEST) {
	} else if (state == DMSPI_STOP) {
	} else {
		printk(KERN_ERR "cspi: State/mode %u not supported\n",state);
		ret = -ENOSYS;
		goto exit;
	}
	
	__raw_writel(0x0, IO_ADDRESS(CONREG));
	
	__clear_bit(CLOCK,(long *)IO_ADDRESS(AMLPMRE2));
	state = DMSPI_STOP;
 exit:
	pr_debug("cspi: Stopping CSPI1, completed\n");
	return ret;
}

/*
 * cspi_rxtx()
 *
 * Given a pointer to a recive and a transmit buffer and the size
 * thereof transmit and recive using the specified mode. A special
 * case is when mode is set to DMSPI_STOP, this will shut down the clock
 * feeding SPI1 and SPI1. This function should only be used for
 * transmitting between a SCMA11 and a DM299.
 *
 * Return the number of bytes transmitted (could be 0) and returns
 * negative if an error occured.
 */
int cspi_rxtx(char *data_tx, char *data_rx, unsigned size, unsigned mode)
{
	int ret = 0;
	
	if (down_trylock(&sem))
		return -ERESTARTSYS;
	if (mode == DMSPI_STOP) {
		if (state != DMSPI_STOP)
			ret = cspi_stop();
		goto exit;
	}
	if (state != mode) {
		cspi_stop();
		state = mode;
		if (( ret = cspi_start(state)) != 0)
			goto exit;
	}
        pr_debug("cspi: cspi_rxtx() START : data_rx=%p, data_tx=%p, size=%d, "
	     "mode/state = %d\n",data_rx,data_tx,size,mode);
	if (state == DMSPI_BOOT) {
		ret = cspi_rxtx_dma(data_tx,data_rx,size,mode);
	} else if (state == DMSPI_MESG) {
		ret = cspi_rxtx_dma(data_tx,data_rx,size,mode);
	} else if (state == DMSPI_APPL) {
		ret = cspi_app_down_int(data_tx,NULL,size);
	} else if (state == DMSPI_TEST) {
		ret = cspi_rxtx_dma(data_tx,data_rx,size,mode);
	} else if (state == DMSPI_STOP) {
	} else {
		printk(KERN_ERR "cspi: State/mode %u not supported\n",state);
		ret = -ENOSYS;
	}
 exit:
	pr_debug("cspi: cspi: cspi_rxtx - Completed: "
	     "return=%d size=%d\n",ret,size);
	up(&sem);
	return ret;
}

/*
 * cspi_init()
 *
 * This function implements the init function of the SPI device. This
 * function is called when the module is loaded.
 *
 * Returns 0
 */
static int __init cspi_init(void)
{
	int ret = 0;
#ifdef DEBUG
	struct proc_dir_entry *e; /* proc debug */
#endif
	printk(KERN_INFO "cspi: init()\n");
	
	/* iomux fixup fix SPI1_CLK */
	iomux_config_mux(SP_SPI1_CLK, OUTPUTCONFIG_FUNC1, INPUTCONFIG_NONE);

	/* iomux fixup fix SPI1_MISO */
	iomux_config_mux(SP_SPI1_MISO, OUTPUTCONFIG_DEFAULT, 
			 INPUTCONFIG_FUNC1);
	if ((ret = cspi_init_intr()) != 0)
		return ret;

	init_MUTEX(&sem);

	cspi_dma_params = kmalloc(sizeof(CSPI_SDMA_PARAMS_T),GFP_KERNEL);
    if(cspi_dma_params == NULL)
    {
       printk("No memory\n");
	   return -ENOMEM;
       
    } 
    memset(cspi_dma_params,0,sizeof(CSPI_SDMA_PARAMS_T)); 	
	cspi_dma_params->word_size = TRANSFER_8BIT;
	ret = cspi_initdma(cspi_dma_params);

	if(ret != 0)
    {
       printk("Error from cspi_initdma()\n");
	   if(cspi_dma_params != NULL)
	   	{
		   kfree(cspi_dma_params);
		   cspi_dma_params = NULL;
	   	}   
       return ret;
    }
	else
	{
	   printk("CSPI Tx DMA Channel: %d\n",cspi_dma_params->txdma_ch);
	   printk("CSPI Rx DMA Channel: %d\n",cspi_dma_params->rxdma_ch);
	}

	/* Enable proc debugging */
#ifdef DEBUG
	e = create_proc_entry("cspi_one",0,NULL);
	if (e) {
		e->read_proc=cspi_read_procmem;
		e->write_proc=cspi_write_procmem;
		e->data=NULL;
		pr_debug("cspi: Ok creating /proc/cspi_one\n");
	}
#endif
	return ret;
}

/*
 * cspi_exit()
 *
 * Disables and shuts down SPI1, and the input clock to SPI1.
 */
static void __exit cspi_exit(void)
{
	printk(KERN_INFO "cspi: exit()\n");

	if (cspi_stop_intr() != 0)
		printk(KERN_ERR "cspi: Error in stop intr\n");

	__raw_writel(0x0, IO_ADDRESS(CONREG));
	
	__clear_bit(CLOCK,(long *)IO_ADDRESS(AMLPMRE2));

	deconf_iomux_cs();

	cspi_cleanup(cspi_dma_params);
	if(cspi_dma_params != NULL)
	{
		kfree(cspi_dma_params);
		cspi_dma_params = NULL;
	}	
	/* Remove proc_entrys */
#ifdef DEBUG
	remove_proc_entry("cspi_one", NULL /* parent dir */);
#endif
}

/*
 * Module entry points
 */
module_init(cspi_init);
module_exit(cspi_exit);

EXPORT_SYMBOL(cspi_rxtx);
EXPORT_SYMBOL(cspi_start);
EXPORT_SYMBOL(cspi_stop);

MODULE_DESCRIPTION("DMSPI char device driver");
MODULE_AUTHOR("Motorola");
MODULE_LICENSE("GPL");
