/*
 * Copyright (C) 2007 Motorola.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 * 05-Dec-2007  Motorola    Modifications related to DMA initialization during Startup.
 * 12-Dec-2007  Motorola    Support for 32 bit DMA.  
 */

/*
 * cspi-intr.c
 *
 * Implements a special purpose SCMA11 to DM299 SPI comunication
 * interface using transfer complete interupt and the SPI tx and rx 8
 * words large buffers.
 */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/module.h>
#include <asm/arch/clock.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <asm-arm/mot-gpio.h>
#include <linux/proc_fs.h>
#include <asm/dma.h>
#include <linux/dma-mapping.h>
#include <linux/random.h>
#include <asm/types.h>
#include <cspi-int.h>
#include <cspi.h>

/* Timeout in jiffies */
#define CSPI1_TIMEOUT 300

static DECLARE_WAIT_QUEUE_HEAD(waitq);

static int timed_out = 0;
static int rxtx_complet = 0;
static struct timer_list timer; 
extern CSPI_SDMA_PARAMS_T *cspi_dma_params;


/*
 * timer_callback()
 *
 * Called if we timeout waiting for a spi interupt
 */
static void timer_callback(unsigned long data) 
{ 
	timed_out = 1;

	if (waitqueue_active(&waitq)) {
		wake_up_interruptible(&waitq);
	}
}

/*
 * irqreturn_t cspi_isr()
 *
 * The spi interupt service rutine, clears the Transfer Complete bit
 * and wakes up the rx/tx function.
 */
irqreturn_t cspi_isr(int irq, void *dev_id, struct pt_regs *regs)
{
	__set_bit(TC,(long *)IO_ADDRESS(STATREG));

	rxtx_complet = 1;
	if (waitqueue_active(&waitq)) {
		wake_up_interruptible(&waitq);
	}
	return IRQ_HANDLED;
}

/*
 * wait_for_int()
 *
 * Calling this function puts the process to sleep until we get a spi
 * interupt or we time out, after which we return
 *
 * Retrun 0 if interupt occured, negative if something bad happend.
 */
int wait_for_int(void)
{
	if (wait_event_interruptible(waitq,
				     timed_out|rxtx_complet)){
		printk(KERN_ERR "cspi: Error reciving int \n");
		return -ERESTARTSYS;
	}
	if (timed_out){
		printk(KERN_ERR "cspi: Interrupted by signal timeout\n");
		return -ETIMEDOUT;
	}
	if (rxtx_complet){
		rxtx_complet = 0;
		return 0;
	}
	return -ERESTARTSYS;
}

int cspi_wakeup(int i)
{
   rxtx_complet = i;
   if (waitqueue_active(&waitq))
   {
      wake_up_interruptible(&waitq);
   }
   return IRQ_HANDLED;
}

/*
 * cspi_rxtx_dma()
 *
 * Transfer and recieve data over CSPI1 using the
 * DMA and keeping CS active durring the whole transfer.
 *
 * Return number of bytes recived which should be eq to transmitted
 * bytes. On error return relevant negativ value.
 */
int cspi_rxtx_dma(char *data_tx, char *data_rx, unsigned size, unsigned mode)
{
	int rtval = 0;
    CSPI_SDMA_PARAMS_T *dma_params=NULL;

    dma_params = cspi_dma_params;
	rxtx_complet = 0;
	timed_out = 0;
	timer.expires = jiffies + CSPI1_TIMEOUT;
	add_timer(&timer);
	
	pr_debug("cspi: cspi_rxtx_msg() : size=%u\n",size); 
	
	gpio_signal_set_data(GPIO_SIGNAL_DMSPI_CS, GPIO_DATA_LOW);
    if(NULL == data_tx && NULL == data_rx)
    {
       rtval = -EINVAL;
       printk(KERN_ERR "Invalid buffer pointers\n"); 
       goto exit;
    }
    if(size <= 0)
    {
       rtval = -EINVAL;
       printk(KERN_ERR "Invalid Count for DMA\n");
       goto exit;
    }
    /*populate the DMA parameters.*/
    dma_params->tx_buf = data_tx;
    dma_params->rx_buf = data_rx;                       
    dma_params->count = size;
    
    if(mode == DMSPI_BOOT){
       dma_params->word_size = TRANSFER_8BIT;
    }else if(mode == DMSPI_MESG){
       dma_params->word_size = TRANSFER_32BIT;
    }else{
       rtval = -ENOSYS;
       goto exit;
    }  
   
    rtval = cspi_dmamain(dma_params);
    if (rtval!=0 )
    {
       printk("Error from cspi_dmamain()\n");
       goto exit;
    }
    else
    {
      rtval = size;
    }
 exit:
    del_timer(&timer);
    gpio_signal_set_data(GPIO_SIGNAL_DMSPI_CS, GPIO_DATA_HIGH);
    __clear_bit(TCEN,(long *)IO_ADDRESS(INTREG));
    pr_debug("Exiting cspi_rxtx_dma\n");
    return rtval;
}

/*
 * cspi_app_down_int()
 *
 * This function is used to transfer DM299 application image in
 * data_tx of size bytes from SCMA11 to DM299.
 *
 * Return is number of bytes tx.
 */
int cspi_app_down_int(char *data_tx,char *data_rx, unsigned size)
{
	unsigned bufs,bytes;
	int trans = 0,rtval=0;
    CSPI_SDMA_PARAMS_T *dma_params;

    dma_params = cspi_dma_params;
	rxtx_complet = 0;
	timed_out = 0;
	timer.expires = jiffies + CSPI1_TIMEOUT;
	add_timer(&timer);
        
    if(NULL == data_tx && NULL == data_rx)
    {
       rtval = -EINVAL;
       printk(KERN_ERR "Invalid buffer pointers\n"); 
       goto exit;
    }
    if(size <= 0)
    {
       rtval = -EINVAL;
       printk(KERN_ERR "Invalid Count for DMA\n");
       goto exit;
    }

    
     //Fill the DMA paramets for app download
     dma_params->tx_buf = data_tx;  
     dma_params->rx_buf = data_rx;
     dma_params->count  = size;
     dma_params->word_size = TRANSFER_8BIT;
     
     __clear_bit(TCEN,(long *)IO_ADDRESS(INTREG)); 
     __set_bit(TC,(long *)IO_ADDRESS(STATREG)); 

     rtval = cspi_dmamain(dma_params);
     if (rtval!=0 )
     {
        printk("Error from cspi_dmamain()\n");
        goto exit;
     }
     else
     {
        trans = size;
     }
     rtval = trans;
 exit:
        del_timer(&timer);
	return rtval;
}

/*
 * cspi_init_intr()
 *
 * request the cspi1 interupt and the timer that handles error when a
 * interupt is not catched. Called when loading the driver from
 * __init.
 *
 * Return 0 on succes negative on error
 */
int cspi_init_intr(void)
{
	int irq_init_res;

	irq_init_res = request_irq(INT_CSPI1, cspi_isr,
				   SA_INTERRUPT, "cspi1", NULL);
	
	if (irq_init_res) {
		printk(KERN_ERR 
		       "cspi: cspi_init_intr(): can't setup interrupt:%d \n",
		       irq_init_res);
		return irq_init_res;
	} else {
		pr_debug("cspi: cspi_init_intr(): interrupt init at %d\n",
		       irq_init_res);
	}
	init_timer(&timer);
	timer.function = timer_callback;
	timer.data     = 0;
	
	return 0;
}

/*
 * cspi_stop_intr()
 *
 * Frees the timer and then interupt registered for cspi1
 *
 * Returns 0
 */
int cspi_stop_intr(void)
{
	pr_debug("cspi: cspi_stop_intr(): freeint irq\n");

	/* stop timer */
	del_timer(&timer);

	/* free interrupt */
	free_irq(INT_CSPI1, NULL);
	
	return 0;
}
