/*
 * Copyright (C) 2007-2008 Motorola.
 */

/* 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 */

 
 /* Date		 Author 	 Comment
  * ===========  ==========  ==================================================
  * 05-Dec-2007  Motorola	 Initial revision.
  * 12-Dec-2007  Motorola        Detect Rx Overflow Errors.
  * 23-Jul-2008  Motorola        Increase Wait delay in Rx ISR.
  * 01-Oct-2008 Motorola OSS Change log / Copyright notice issue. 
  */
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/slab.h>
#include <asm/uaccess.h>
#include <linux/module.h>
#include <asm/arch/clock.h>
#include <linux/kthread.h>
#include <linux/delay.h>
#include <linux/device.h>
#include <asm-arm/mot-gpio.h>
#include <linux/proc_fs.h>
#include <asm/dma.h>
#include <linux/dma-mapping.h>
#include <linux/random.h>
#include <asm/types.h>
#include <cspi-int.h>
#include <cspi.h>

#define TXPUSH  4
#define RXPOP   4

int cspi_initdma(CSPI_SDMA_PARAMS_T *params);
int cspi_txrxdma(CSPI_SDMA_PARAMS_T *params);
void cspi_cleanup(CSPI_SDMA_PARAMS_T *params);
void cspi_debugprint(CSPI_SDMA_PARAMS_T *params);
int cspi_dma_rxisr(CSPI_SDMA_PARAMS_T *params);
int cspi_dma_txisr(CSPI_SDMA_PARAMS_T *params);
void cspi_configspi(CSPI_SDMA_PARAMS_T *params);

/*
  int cspi_dmamain(CSPI_SDMA_PARAMS_T *params)

  This function is called by cspi_rxtx().This is called
  whenever there is data needs to be transmitted by CSPI1 DMA.

  The input parameter is Structure containing DMA paramets.
  The return value is 0 if DMA succeds, Negative value if
   DMA fails.
*/
int cspi_dmamain(CSPI_SDMA_PARAMS_T *params)
{
  int ret=0; 
  dma_request_t dma_request;
  static uint previous_mode =  TRANSFER_8BIT;

  /*Populate CSPI1 register suitably for DMA */
  cspi_configspi(params);
  if(previous_mode != params->word_size){
     ret = cspi_initdma(params);
     if(ret !=0){
     printk("Error duing Init DMA \n");
        goto Error;
     }
     previous_mode = params->word_size;
  }
  	
  memset(&dma_request,0,sizeof(dma_request_t));
  /* The following code is required as the DMA is not kicking off
     automatically(randomly). We will have to write initial data into FIFO and 
     then DMA requests are generated properly.
     Suitably we have to update count and Physical address.
  */
  if(params->tx_buf != NULL) 
  {
     if(params->word_size == TRANSFER_32BIT) 
     {
        params->tx_phy_addr = sdma_virt_to_phys((params->tx_buf)+(TXPUSH*4));
        dma_request.count = (params->count)-(TXPUSH*4);
     }
     else if(params->word_size == TRANSFER_8BIT) 
     {
        params->tx_phy_addr = sdma_virt_to_phys((params->tx_buf)+TXPUSH);
        dma_request.count = (params->count)-TXPUSH;
     }
     else
     {
       ret = -ENOSYS;
       goto Error;
     }
  
     dma_request.sourceAddr = (__u8*)params->tx_phy_addr;
  
     ret = mxc_dma_set_config(params->txdma_ch,&dma_request,0);
     if(ret !=0)
     {
        printk("DMA Set Config for Tx Channel is failed\n");
        goto Error; 
     }
   }
  
  if(params->rx_buf!=NULL)
  {
     memset(&dma_request,0,sizeof(dma_request_t));
     params->rx_phy_addr = sdma_virt_to_phys(params->rx_buf);
     dma_request.destAddr = (__u8*)params->rx_phy_addr;
     if(params->word_size == TRANSFER_32BIT) 
     {
        dma_request.count  = (params->count)-(RXPOP*4);
     }
     else if(params->word_size == TRANSFER_8BIT) 
     {
        dma_request.count  = (params->count)-RXPOP;
     }
     else
     {
        ret = -ENOSYS;
        goto Error;
     } 
     ret = mxc_dma_set_config(params->rxdma_ch,&dma_request,0);
     if(ret !=0)
     {
        printk("DMA Set Config for Rx Channel is failed\n");
        goto Error;
     }
  }
  /* Kick-off DMA */
  ret = cspi_txrxdma(params);
  if(ret != 0)
    printk("Error from the cspi_txrxdma()\n"); 
  Error:
   
  return ret;
}
			     
/*
int cspi_initdma(CSPI_SDMA_PARAMS_T *params)

This function Acquires and configures the Tx and Rx channels.
If the Rx buffer is NULL, then Rx Channel will not be requested.
If the Tx buffer is NULL, then Tx Channel will not be requested.
*/
int cspi_initdma(CSPI_SDMA_PARAMS_T *params)
{
    dma_channel_params txdma_param,rxdma_param;
    int ret=0;
 
    /* Request for Rx Channel */
	if(params->rxdma_ch == 0){
	   ret = mxc_request_dma(&(params->rxdma_ch),"CSPI1_RX");
       if(ret != 0)
       {
          printk("Error: Rx DMA Channel Request is failed\n");
          return ret;
       }
	}
    memset(&rxdma_param,0,sizeof(dma_channel_params));
    /* Rx Channel set up */
    rxdma_param.transfer_type = per_2_emi;
    rxdma_param.per_address = RXDATA;
    rxdma_param.peripheral_type = CSPI;
    rxdma_param.event_id = DMA_REQ_CSPI1_RX;
    rxdma_param.callback = cspi_dma_rxisr;
    rxdma_param.arg      = params;
    rxdma_param.bd_number = 1;
    rxdma_param.word_size = params->word_size;
    if(params->word_size == TRANSFER_8BIT)
       rxdma_param.watermark_level = 4;
    else if(params->word_size == TRANSFER_32BIT)
       rxdma_param.watermark_level = 16;
    else if(params->word_size == TRANSFER_16BIT || 
           params->word_size == TRANSFER_24BIT)
       rxdma_param.watermark_level = 8;
    else
       return -ENOSYS;        


    ret = mxc_dma_setup_channel(params->rxdma_ch,&rxdma_param);
    if(ret != 0)
    {
      printk("Error: Rx DMA Channel setup is failed\n");
      return ret;
    }
    

    /* Request for Tx Channel */

	if(params->txdma_ch == 0){
		ret = mxc_request_dma(&(params->txdma_ch),"CSPI1_TX");
        if(ret != 0)
        {
           printk("Error: Tx DMA Channel Request is failed\n");
           return ret;
        }
     }	

    memset(&txdma_param,0,sizeof(dma_channel_params));
    /* Setup the Tx-Channel */
    txdma_param.transfer_type = emi_2_per;
    txdma_param.per_address = TXDATA;
    txdma_param.peripheral_type = CSPI;
    txdma_param.event_id = DMA_REQ_CSPI1_TX;
    txdma_param.callback = cspi_dma_txisr ; 
    txdma_param.arg      = params;
    txdma_param.bd_number = 1;
    txdma_param.word_size = params->word_size;
    if(params->word_size == TRANSFER_8BIT)
       txdma_param.watermark_level = 4;
    else if(params->word_size == TRANSFER_32BIT)
       txdma_param.watermark_level = 16;
    else if(params->word_size == TRANSFER_16BIT || 
           params->word_size == TRANSFER_24BIT)
       txdma_param.watermark_level = 8;
    else
       return -ENOSYS;
  
    ret = mxc_dma_setup_channel(params->txdma_ch,&txdma_param);
    if(ret != 0)
    {
      printk("Error: Tx DMA Channel setup is failed\n");
      return ret;
    }
  return ret;
}

/*

int cspi_txrxdma(CSPI_SDMA_PARAMS_T *params)

This function starts the DMA channels.
The function accepts the DMA parameter structure, which
has details about the channels, buffer pointers etc.,
*/    
int cspi_txrxdma(CSPI_SDMA_PARAMS_T *params)
{
   int i,*buf,ret=0;
   unsigned int reg=0;

   params->overflow_error = 0;
   /* Invalidate the Cache in Rx Buffer area, before starting the Chnl*/
   if(params->rx_buf!=NULL)
   {
      consistent_sync(params->rx_buf,params->count,DMA_FROM_DEVICE);
      mxc_dma_start(params->rxdma_ch);
   }
 
   if(params->tx_buf!=NULL)
   {
       /* Make sure that Cache contents are write back to the Memory,so that
            DMA can have access to actual data to be transmitted */
      consistent_sync(params->tx_buf,params->count,DMA_TO_DEVICE);
      /* The following is required as the DMA doesn't trigger automatically.
       we will have to push some data and it starts automatically.*/
      if(params->word_size == TRANSFER_32BIT)   
      {/* fill tx buffer */
         buf = (int*)params->tx_buf;
        for (i=0;i<TXPUSH;i++) 
        {
           __raw_writel(buf[i], IO_ADDRESS(TXDATA));
	}
      }
      else if(params->word_size == TRANSFER_8BIT)
      {
         for (i=0;i<TXPUSH;i++) 
         {
            reg =  params->tx_buf[i];
           __raw_writel(reg,IO_ADDRESS(TXDATA));
	 }
      }
     else
     {
         printk("Invalid DMA data width:%d\n",params->word_size);
         return -ENOSYS;
     }
     mxc_dma_start(params->txdma_ch);
   }  

    if((ret=wait_for_int()) !=0)
    {
        return ret;
    }else 
       return params->overflow_error;
}
/*
void cspi_cleanup(CSPI_SDMA_PARAMS_T *params )

This function Clears the DMAREG and frees the DMA channels 
*/
void cspi_cleanup(CSPI_SDMA_PARAMS_T *params )
{
   if(params->rxdma_ch != 0)
     mxc_free_dma(params->rxdma_ch);
   if(params->txdma_ch != 0)
     mxc_free_dma(params->txdma_ch);
    __raw_writel(0x00000000,IO_ADDRESS(DMAREG));
}	
/*
void cspi_configspi(CSPI_SDMA_PARAMS_T *params)

This function configures the CSPI register(s) which are
necessery for DMA. This function assumes the basic CSPI
configuration is done already.  
*/
void cspi_configspi(CSPI_SDMA_PARAMS_T *params)
{
   /*Enable Tx Full and Rx Empty*/
     __raw_writel(0x00000011,IO_ADDRESS(DMAREG));
}
/*
int cspi_dma_rxisr(CSPI_SDMA_PARAMS_T *params)

This is DMA call back function. The SDMA calls this function
whenever it gets DMA Rx Interrupt.
Here we don't do much except waking up from sleep.
*/   
int cspi_dma_rxisr(CSPI_SDMA_PARAMS_T *params)
{
   uint i,index,*buf,reg;
   int ret=0,wait=10,iteration=10;

   __set_bit(TC,(long *)IO_ADDRESS(STATREG)); 

   /* Wait till remaining 4 words/bytes are received */ 
   for(i=0;i<iteration;i++)
   {
      reg = __raw_readl(IO_ADDRESS(STATREG));
      if(reg & 0x00000010)
         break;
      if(i==(iteration-1))
         printk("Error: Timeout in Rx of SPI\n");
      udelay(wait);
   }   
       
   consistent_sync(params->rx_buf,params->count,DMA_FROM_DEVICE);
    
   /*
   SDMA scripts automatically transfers only multiple of 4 bytes/words (if RH is set). 
   So here we are manually copying last 4 bytes/words.
   Remaining words are transmitted and received through DMA.
   */
   reg = __raw_readl(IO_ADDRESS(TESTREG));
   reg = reg & 0x000000F0;
   reg = reg >> 4;
   if(reg != RXPOP)
   {
      printk("!!! Error: bytes are overflowed in Rx: %d !!!\n",RXPOP-reg);
	  params->overflow_error = -EOVERFLOW;
      cspi_debugprint(params);
   }

   if(params->word_size == TRANSFER_32BIT) 
   {
      buf = (uint*)params->rx_buf;
      index = (params->count)/4 - RXPOP;
      for(i=0;i<RXPOP;i++)
        buf[index+i]=__raw_readl(IO_ADDRESS(RXDATA));
   }
   else if(params->word_size == TRANSFER_8BIT)
   {
     index = params->count - RXPOP;
     for(i=0;i<RXPOP;i++)
        params->rx_buf[index+i]=__raw_readl(IO_ADDRESS(RXDATA));
   }
   ret = (cspi_wakeup(1));
   return ret;
}

/*
int cspi_dma_txisr(CSPI_SDMA_PARAMS_T *params)

This is a Tx DMA Call back function.
This function is called whenever SDMA gets Tx Interrupt.
*/
int cspi_dma_txisr(CSPI_SDMA_PARAMS_T *params)
{

   __set_bit(TC,(long *)IO_ADDRESS(STATREG)); 

   /*Wakeup only if we are not waiting for Rx Interrupt */
   if(params->rx_buf == NULL)
   {
      cspi_wakeup(1);  
   }                   
   return 0;
}

void cspi_debugprint(CSPI_SDMA_PARAMS_T *params)
{
   unsigned int reg=0;
   dma_request_t req_params;

   dmspi_myprint_all();
   mxc_dma_get_config(params->txdma_ch,&req_params,0);
   printk("Tx Channel # :%d\n", params->txdma_ch);
   printk("Count for Tx Channel %d\n",req_params.count);
   printk("BD_done:%d\n",req_params.bd_done);
   printk("BD_CONT:%d\n",req_params.bd_cont);
   printk("BD_Error:%d\n",req_params.bd_error);

   mxc_dma_get_config(params->rxdma_ch,&req_params,0);
   printk("Rx Channel # : %d\n",params->rxdma_ch);
   printk("Count for Rx Channel %d\n",req_params.count);
   printk("BD_done:%d\n",req_params.bd_done);
   printk("BD_CONT:%d\n",req_params.bd_cont);
   printk("BD_Error:%d\n",req_params.bd_error);
}
