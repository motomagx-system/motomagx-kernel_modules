/*
 * Copyright (C) 2007 Motorola.
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
 * 02111-1307, USA
 *
 */

/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 25-May-2007  Motorola    Initial revision.
 */

void nschedule(unsigned n)
{
	unsigned k;
	for (k=0;k<n;k++) 
		schedule();
}

int rx(char *rxadr, int *l)
{
	int j = 0;
	unsigned long reg, tmp_reg;
	int *tmpp;
	char *ctmpp;
	while (__raw_readl(IO_ADDRESS(STATREG)) & 0x8) {
		if (*l >= sizeof(int)) {
			tmpp = (int *)(rxadr);
			*tmpp = ntohl(__raw_readl(IO_ADDRESS(RXDATA)));
			*l -= 4;
		} else {
			reg = __raw_readl(IO_ADDRESS(RXDATA));
			switch(*l) {
			case 3:
				tmp_reg = (reg & 0x0000ff00)>>8;
				ctmpp = rxadr+2;
				*ctmpp = (char)tmp_reg;
				*l=*l-1;
			case 2:
				tmp_reg = (reg & 0x00ff0000)>>16;
				ctmpp = rxadr+1;
				*ctmpp = (char)tmp_reg;
				*l=*l-1;
			case 1:
				tmp_reg = (reg & 0xff000000)>>24;
				*rxadr =  (char)tmp_reg;
				*l=*l-1;
			case 0:
				break;
			default:
				printk("cspi : ERROR line 196\n");
				return -1;
			}
		}
		if (j++ > 1000) {
			printk("cspi : ERROR line 201\n");
			return -1;
		}
	}
	return 0;
}


/*
 */
int cspi2_rxtx(char *data_tx, char *data_rx, unsigned size)
{
	unsigned i = size;
	unsigned j = size;
	unsigned dbk = 0;
	unsigned long reg = 0, tmp_reg = 0;
	if (size == 0)
		goto exit;
	/* loop debug */
	//MODIFY_REGISTER_32(0,1 << 14 ,_reg_SPI(TESTREG)); // enable
	MODIFY_REGISTER_32(1 << 14,0 ,_reg_SPI(TESTREG));// disable
	
	/* Empty CSPI rx buffer */
	while ((__raw_readl(IO_ADDRESS(STATREG)) & 0x8) && (dbk++ < LIM)) {
		reg = __raw_readl(IO_ADDRESS(RXDATA));
	}
	if (dbk >= LIM)
		return -1;
	
	/* set spi1_ss1 active (low) */
	gpio_signal_set_data(GPIO_SIGNAL_DMSPI_CS, GPIO_DATA_LOW);

	/* Transmit all data */
	while(i > 3) {
		reg =   (data_tx[size-i])<<24 | 
			(data_tx[size-i+1]<<16) | 
			(data_tx[size-i+2]<<8)  | 
			data_tx[size-i+3];
		dbk=0;
		while ((__raw_readl(IO_ADDRESS(STATREG)) & 0x4) && 
		       (dbk++ < LIM)) {
			schedule(); /* fifo full relax for a while */
		}
		if (dbk >= LIM)
			goto exit;
		__raw_writel(reg, IO_ADDRESS(TXDATA));
		nschedule(100);
		if (rx(&data_rx[size-j],&j) < 0)
			goto exit;
		i -= 4;
	}
	reg = 0x0;
	switch(i) {
	case 3:
		reg = reg | data_tx[size-i+2]<<8;
	case 2:
		reg = reg | data_tx[size-i+1]<<16;
	case 1:
		reg = reg | data_tx[size-i]<<24;
	default:
		break;
	}
	if ( i > 0) {
		dbk=0;
		while ((__raw_readl(IO_ADDRESS(STATREG)) & 0x4) && 
		       (dbk++ < LIM)) {
			schedule(); /* fifo full relax for a while */
		}
		if (dbk >= LIM)
			goto exit;;
		__raw_writel(reg, IO_ADDRESS(TXDATA));
	}

	/* Make sure all rx and tx buffers are empty */
	dbk = 0;
	do {
		nschedule(100);
		if (rx(&data_rx[size-j],&j) < 0)
			goto exit;
		tmp_reg = __raw_readl(IO_ADDRESS(STATREG));
		if (dbk++ > 1000) {
			printk("cspi : ERROR This should not "
			       "happen\n");
			break;
		}
	} while ((!(tmp_reg & 0x80) || (tmp_reg & 0x8)) && 
		 (dbk++ < LIM));
	if (dbk >= LIM)
		goto exit;
	
 exit:
	/* Clear TC (transfer complete) bit and negate CS*/
	MODIFY_REGISTER_32(0,1 << 7 ,_reg_SPI(STATREG));
	gpio_signal_set_data(GPIO_SIGNAL_DMSPI_CS, GPIO_DATA_HIGH);
	return size-j;
}


int cspi_app_down(char *data_tx, unsigned size)
{
	unsigned i;
	unsigned dbk = 0;
	unsigned long reg = 0;
	int trans = 0;
	if (size == 0)
		return 0;
	
	if (state != APPL)
		return -1;
	dmspi_myprint_all();
	/* Transmit all data */
	for (i=0;i<size;i++) {
		reg =   data_tx[i];
		dbk=0;
		while (__raw_readl(IO_ADDRESS(STATREG)) & 0x4) {
			schedule(); /* fifo full relax for a while */
			if (dbk++ > LIM) {
				printk("cspi : Error line 316\n");
				break;
			}
		}
		__raw_writel(reg, IO_ADDRESS(TXDATA));
		trans++;
	}
	dbk = 0;
	do {
		reg = __raw_readl(IO_ADDRESS(STATREG));
		if (dbk++ > LIM) {
			printk("cspi : Error line 327\n");
			break;
		}
	} while (!(reg & 0x8));
	dmspi_myprint_all();
	/* Clear TC (transfer complete) bit*/
	MODIFY_REGISTER_32(0,1 << 7 ,_reg_SPI(STATREG));
	
	return trans;
}
