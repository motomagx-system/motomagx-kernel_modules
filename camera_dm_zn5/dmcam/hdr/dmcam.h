/* DMCAM                                                  <dmcam.h> */
/*                                                                  */
/* Copyright (C) 2007-2008 Motorola, Inc.                           */
/*                                                                  */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License  
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 30-May-2007  Motorola    Initial revision.
 * 14-Jan-2008  Motorola    Prototype of dmcam_ioctl_power_on changed
 * 21-Jan-2008  Motorola    Removed obsolete code 
 * 
 */


#ifndef __DMCAM_H__
#define __DMCAM_H__


#define DMC_MAX_WRITE_CHUNK_SIZE PAGE_SIZE*16

#define CSI_MCLK_DIVIDER 40
#define CSI_SENS_CONF_SENS_CLKSRC_MASK  (1 << CSI_SENS_CONF_SENS_CLKSRC_SHIFT)

#define DEBUG	0

#define err_print(fmt, args...) printk(KERN_ERR "###ACS - CDL###\tFunction %s: "fmt"\n", __FUNCTION__, ##args)

#if DEBUG > 0

#define dbg_print(fmt, args...) printk(KERN_INFO "###ACS - CDL###\tFunction %s: "fmt"\n", __FUNCTION__, ##args)

#if DEBUG > 1
#define ddbg_print(fmt, args...) printk(KERN_INFO "###ACS - CDL###\tFunction %s: "fmt"\n", __FUNCTION__, ##args)
#else
#define ddbg_print(fmt, args...) ;
#endif

#else

#define dbg_print(fmt, args...)  ;
#define ddbg_print(fmt, args...) ;

#endif



#endif      /*__DMCAM_H__*/

