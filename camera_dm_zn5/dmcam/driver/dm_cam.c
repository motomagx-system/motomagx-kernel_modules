/* DMCAM                                                                    <dm_cam.c> */
/*                                                                                  */
/* Copyright (c) 2007-2008 Motorola, Inc.                                               */
/*                                                                                  */

/*
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License  
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 16-May-2007  Motorola    Initial revision.
 * 07-Nov-2007  Motorola    Added mxc_pll_request_pll(USBPLL) and mxc_pll_release_pll(USBPLL)
 *                          in dmcam_init() and dmcam_stop() respectively.
 * 07-Nov-2007  Motorola    Power on and off sequence updated
 * 22-Nov-2007  Motorola    Removed SPI tristate mode during startup
 * 27-Nov-2007  Motorola    Power off moved to release function, and check for multiple power on
 * 28-Nov-2007  Motorola    Added ioctls to handle quickview
 * 02-Jan-2008  Motorola    Added ioctls for handling viewfinder resolution change
 * 14-Jan-2008  Motorola    Made power off dependent on file pointer
 * 21-Jan-2008  Motorola    Removed obsolete code optimized dmcam_write
 * 27-Feb-2008  Motorola    Control of LED moved to LM
 * 16-Jun-2008  Motorola    GPIO_SIGNAL_I_PEEK set to low during power up.
 * 22-Aug-2008  Motorola    Make sure Logical Channel driver is in pause state when power off DM299
 * 03-Sep-2008  Motorola    OSS fix
 */

#include <linux/module.h>	/* Kernel module definitions */
#include <linux/init.h>
#include <linux/kernel.h>	/* We will be "in the kernel" mode of execution */
#include <linux/delay.h>

#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>

#include <linux/power_ic_kernel.h>
#include <linux/lights_funlights.h>


#include <asm/mot-gpio.h>
#include <asm/arch/clock.h>
#include <ipu.h>
#include <ipu_regs.h>

#include <linux/string.h>

#include <cspi.h>
#include <dmave_logi_drv.h>
#include "dmcam.h"
#include "dmcam_ioctl.h" 
#define DMOFF NULL

int mode = DMSPI_BOOT;
int next_read_proc = -1;
int proc_readout;

static int dmcam_open_cnt = 0;
static struct file *dm299_powered_file = DMOFF;

int dmcam_set_bit(unsigned long dm_reg, unsigned long dm_mask);
int dmcam_clear_bit(unsigned long dm_reg, unsigned long dm_mask);
int dmcam_test_bit_set(unsigned long dm_reg, unsigned long dm_mask);
int dmcam_test_bit_clear(unsigned long dm_reg, unsigned long dm_mask);

int dmcam_enable_DM299_mclk(void);

int dmcam_ioctl_power_on(struct file *file);
int dmcam_power_off(void);

int dmcam_ioctl_reset_dm299(void);
int dmcam_ioctl_wakeup_dm299(void);





int dmcam_set_bit(unsigned long dm_reg, unsigned long dm_mask)
{

	__raw_writel((__raw_readl(dm_reg) | dm_mask),dm_reg);

	return dmcam_test_bit_set(dm_reg, dm_mask);
}


int dmcam_clear_bit(unsigned long dm_reg,unsigned long dm_mask) 
{

    __raw_writel((__raw_readl(dm_reg) & ~dm_mask),dm_reg);   

	return dmcam_test_bit_clear(dm_reg, dm_mask);
}

int dmcam_test_bit_set(unsigned long dm_reg,unsigned long dm_mask)
{
	if ((__raw_readl(IPU_CONF) & dm_mask) == dm_mask) {
		return 0;
	}
	else {
		return -1;
	}

}

int dmcam_test_bit_clear(unsigned long dm_reg,unsigned long dm_mask)
{
	if ((__raw_readl(IPU_CONF) | ~dm_mask) == ~dm_mask) {
		return 0;
	}
	else {
		return -1;
	}

}


static int dmcam_open(struct inode *inode, struct file *file)
{
	dmcam_open_cnt++;

	return 0;
}

static int dmcam_release(struct inode *inode, struct file *file)
{
	dmcam_open_cnt--;

	if(dm299_powered_file == file){
		dmcam_power_off();
	}    
	
	if((dmcam_open_cnt == 0)&&(dm299_powered_file != DMOFF)){
		dmcam_power_off();
		err_print("ERROR->DMCAM:(dm299_powered_pid != DMOFF)&&(dmcam_open_cnt == 0) \n");
	}    

	return 0;
}


static ssize_t dmcam_read(struct file *file, char *buf, unsigned int count, loff_t *ppos)
{
	return 0;
}


static ssize_t dmcam_write(struct file *file, const char *buf, unsigned int count, loff_t *ppos)
{
	int return_value = count;
	char *send_buffer = NULL;
	char *receive_buffer = NULL;
	int chunk_size = 0;
	int ptr = 0;
	  
	ddbg_print("DMCAM_DBG: Entering dmcam_write, count: %d\n", count);
	/* check user space access */
	if (!access_ok(VERIFY_READ, buf, count)){
		err_print("DMCAM: Failed to access user space buffer\n");
		return -EFAULT;
	}

	if ((count > DMC_MAX_WRITE_CHUNK_SIZE)&&(mode==DMSPI_APPL)) {
		chunk_size = DMC_MAX_WRITE_CHUNK_SIZE;
	} else {
		chunk_size = count;
	}
	ddbg_print("DMCAM_DBG: Transferring application");
		  
	send_buffer = kmalloc(chunk_size, GFP_KERNEL|GFP_DMA);

	if (send_buffer == NULL) {
		err_print("DMCAM: Allocation of buffer failed, size: %d\n", count);
		return -ENOMEM;
	}
	ddbg_print("DMCAM_DBG: Send buffer allocated, size: %d\n", count);

	receive_buffer = kmalloc(chunk_size, GFP_KERNEL|GFP_DMA);
	
	if (receive_buffer == NULL) {
		return_value = -ENOMEM;
		err_print("DMCAM: Allocation of receive_buffer failed, size: %d\n", count);
		goto exit_free_send;
	}
	ddbg_print("DMCAM_DBG: Receive buffer allocated, size: %d\n", count);

	while(ptr < count){
		
		if ( (count - ptr) < chunk_size)
			chunk_size = (count - ptr);

		memcpy(send_buffer, buf+ptr, chunk_size);

		/* Send buffer to SPI protocol */
		if ((return_value = cspi_rxtx(send_buffer, receive_buffer, chunk_size, mode)) < 0 ) {
			err_print("DMCAM: SPI transmission failed; %d", return_value);
			goto exit_free_both;
		}		
		ptr += return_value;
	}
	return_value = ptr;

	ddbg_print("DMCAM_DBG: SPI transfer complete.\n");
 exit_free_both:
	kfree(receive_buffer);
 exit_free_send:
	kfree(send_buffer);

	ddbg_print("DMCAM_DBG: Exiting dmcam_write_ext_appl.\n");
	  
	return return_value;
	  
}

int dmcam_af_led_power(int type)
{
	switch(type){
	case DMCAM_IOC_SET_AF_LED_1:
	case DMCAM_IOC_SET_AF_LED_2:	
		lights_fl_update(LIGHTS_FL_APP_CTL_DEFAULT, 1, LIGHTS_FL_REGION_PRIVACY_IND, 1);       
		break;
	case DMCAM_IOC_AF_LED_OFF:
		lights_fl_update(LIGHTS_FL_APP_CTL_DEFAULT, 1, LIGHTS_FL_REGION_PRIVACY_IND, 0);
		break;
	default:
		err_print("ERROR->dmcam_af_led_power: led type (0x%X)unknown\n", type);
		return -EFAULT;
	}
	
	return 0;
}

static int dmcam_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
	int ret_code = 0;
	switch (cmd)
	{
	case DMCAM_IOC_POWER_ON:
		dmcam_ioctl_power_on(file);
		break;
	case DMCAM_IOC_BOOT_MODE:
		mode = DMSPI_BOOT;
		break;
	case DMCAM_IOC_APP_MODE:
		mode = DMSPI_APPL;
		break;
	case DMCAM_IOC_RESET_DM299:
		dmcam_ioctl_reset_dm299();
		break;
	case DMCAM_IOC_WAKEUP_DM299:
		dmcam_ioctl_wakeup_dm299();
		break;
	case DMCAM_IOC_SET_AF_LED_1: /* Enable/Disable AF-LED */
	case DMCAM_IOC_SET_AF_LED_2:
	case DMCAM_IOC_AF_LED_OFF:
		return dmcam_af_led_power(cmd);
	case DMCAM_IOC_CHARGE_LEVEL_HIGH:
		gpio_signal_set_data(GPIO_SIGNAL_I_PEEK, GPIO_DATA_HIGH);
		break;
	case DMCAM_IOC_CHARGE_LEVEL_LOW:
		gpio_signal_set_data(GPIO_SIGNAL_I_PEEK, GPIO_DATA_LOW);
		break;
	default:
		ret_code = -ENOTTY;
		break;    
	}
	
	return ret_code;
}

int dmcam_enable_DM299_mclk()
{
	/* Enable CSI master clock */
	/* ASCSR:CSSEL = ADPLL and CDER1:CSEN = enable*/
	ipu_csi_enable_mclk(CSI_MCLK_VF|CSI_MCLK_ENC|CSI_MCLK_RAW|CSI_MCLK_I2C, 1, 1);

	/* ACDER1:CSDIV = 40 = 26.60 Mhz when ADPLL = 532 MHz*/     
	mxc_set_clocks_div(CSI_BAUD, CSI_MCLK_DIVIDER);
	
	/* CSI_SENS_CONF:SENS_CLK_SRC = 0*/
	if (dmcam_clear_bit(CSI_SENS_CONF,CSI_SENS_CONF_SENS_CLKSRC_MASK)) {
		err_print("Error: CSI_SENS_CONF:SENS_CLK_SRC = 0 failed!");
		return -1;
	}else{
		dbg_print("CSI_SENS_CONF:SENS_CLK_SRC = 0");
	}
	
	/* Enable mclk for DM299 */
	if (dmcam_set_bit(IPU_CONF,IPU_CONF_CSI_EN)) {
		err_print("Error: Enable CSI master clock failed!");
		return -1;
	}else{
		dbg_print("Enabled CSI master clock");
	}
	return 0;
}

int dmcam_gpio_on(void)
{
	/* make sure that reset is set low during power up*/
	if(gpio_signal_set_data(GPIO_SIGNAL_CAM_RST_B, GPIO_DATA_LOW)){
		goto error_out;
	}
        
	/*Make sure reset and wakeup signals are high*/
	if(gpio_signal_set_data(GPIO_SIGNAL_CAM_PD, GPIO_DATA_HIGH)){
		goto error_out;
	}
    
	if(gpio_signal_set_data(GPIO_SIGNAL_I_PEEK, GPIO_DATA_LOW)){
		goto error_out;
	}
	
	return 0;
	
 error_out:
	printk("ERROR->dmcam_gpio_on: gpio error\n");

	return -EIO;
}


int dmcam_ioctl_power_on(struct file *file)
{
	int retval = 0;

	if (dm299_powered_file != DMOFF) {
		err_print("Attempting to power DM299 on a second time");
		return -EPERM;
	}
	
	/* Changing pin configuration from out- to input to enable DM299 interupts */
    	gpio_signal_config(GPIO_SIGNAL_DM299_REQ_INT,
			   GPIO_GDIR_INPUT,
			   GPIO_INT_FALL_EDGE);

	/* Enable mclk for DM299 */
	if(dmcam_enable_DM299_mclk()!=0){
		printk("Error-->dmcam_ioctl_power_on:mclk error\n");
		retval = -EIO;
		goto err_return;
	}
	
	if(cspi_start(DMSPI_BOOT)){
		printk("Error-->dmcam_ioctl_power_on:cspi_startup error\n");
		retval = -EIO;
		goto err_return;
	}
	
	if(dmcam_gpio_on()){
		printk("Error-->dmcam_ioctl_power_on:gpio error\n");
		retval = -EIO; 
		goto err_return;
	}
	
	/* Enable 1.3V supply for DM299 */
	dbg_print("Enabled DM299 1.3V.");
	
	if(gpio_signal_set_data(GPIO_SIGNAL_DM299_PWR_EN, GPIO_DATA_HIGH)){
		printk("Error-->dmcam_ioctl_power_on:1.3v enable error\n");
		retval = -EIO; 
		goto err_return;        
	}
	
	/* wait 96 us before 1.8v on */
	udelay(96);
	
	/* Enable power to camera */
	if(power_ic_periph_set_camera_on(POWER_IC_PERIPH_ON)){
		printk("Error-->dmcam_ioctl_power_on:power enable error\n");
		retval = -EIO; 
		goto err_return;        
	}
	dbg_print("Enabled 1.8V to camera sensor.");
	/* VCAM rise-time measured to be about 100us */
	 
	/* wait until all PLL's are initialized*/
	udelay(120);

	/* Set DM299 to be on and remember PID of the powering process */
	dm299_powered_file = file;

	printk("DMCAM: Powered on by %s (pid:%d)\n",
	       current->comm, current->pid);
	
	return 0;
 err_return:
	dmcam_power_off();
	
	return retval; 
}

int dmcam_power_off(void)
{
	/* If userspace did not set logical channel driver in pause state make sure we do it here */
	logi_drv_set_mode(LDRV_PAUSE);

	/* set the reset pin low before powering off the circuit */
	gpio_signal_set_data(GPIO_SIGNAL_CAM_RST_B, GPIO_DATA_LOW);
	
	/* Disable 1.3V supply for DM299 */
	gpio_signal_set_data(GPIO_SIGNAL_DM299_PWR_EN, GPIO_DATA_LOW);
	dbg_print("Disabled power to DM299");
	
	/* Disable power to camera */
	power_ic_periph_set_camera_on(POWER_IC_PERIPH_OFF);

	/* Power off AF-LED */
	dmcam_af_led_power(DMCAM_IOC_AF_LED_OFF);
    
	/* ASCSR:CSSEL = ADPLL and CDER1:CSEN = disable*/
	/* Disable mclk for DM299 */
	if (dmcam_clear_bit(IPU_CONF,IPU_CONF_CSI_EN)) {
		err_print("Error: Disable mclk failed!");
	}else{
		dbg_print("MCLK for DM299 disabled");
	}
	
	ipu_csi_enable_mclk(CSI_MCLK_VF|CSI_MCLK_ENC|CSI_MCLK_RAW|CSI_MCLK_I2C, 0, 0);
	
	/* Disable CSPI module */
	if (cspi_stop() != 0) {
		err_print ("DMB: Error switching off SPI!\n");
	}
	
	/*Set signals low to avoid leak current*/
	gpio_signal_set_data(GPIO_SIGNAL_CAM_PD, GPIO_DATA_LOW);
	gpio_signal_set_data(GPIO_SIGNAL_I_PEEK, GPIO_DATA_LOW);

	/* Set interupt pin to oputput ans signal low to avoid random interupts */
	gpio_signal_config(GPIO_SIGNAL_DM299_REQ_INT,
			   GPIO_GDIR_OUTPUT,
			   GPIO_INT_FALL_EDGE);
	gpio_signal_set_data(GPIO_SIGNAL_DM299_REQ_INT,GPIO_DATA_LOW);

	/* set DM299 to be off */
	dm299_powered_file = DMOFF;

	printk("DMCAM: Powered off by %s (pid:%d)\n",
	       current->comm, current->pid);

	return 0;
}


int dmcam_ioctl_reset_dm299()
{
	  /* Put DM299 in reset */
	  gpio_signal_set_data(GPIO_SIGNAL_CAM_RST_B, GPIO_DATA_LOW);
	  udelay(3);
	  gpio_signal_set_data(GPIO_SIGNAL_CAM_RST_B, GPIO_DATA_HIGH);
	  udelay(3);

	return 0;
}

int dmcam_ioctl_wakeup_dm299()
{
	  /* Wakeup DM299 */
	  gpio_signal_set_data(GPIO_SIGNAL_CAM_PD, GPIO_DATA_LOW);
	  udelay(50);
	  gpio_signal_set_data(GPIO_SIGNAL_CAM_PD, GPIO_DATA_HIGH);
	  udelay(50);

	return 0;
}


static struct file_operations dmcam_fops = {
	.read    = dmcam_read,
	.write   = dmcam_write,
	.open    = dmcam_open,
	.release = dmcam_release,
	.ioctl   = dmcam_ioctl,
};

 static int  major;
 #define NAME  "dmcam"

/* When the module is loaded, execute the driver initialization routine. */
static int __init dmcam_init(void)
{
#ifdef CONFIG_MOT_FEAT_PM
	/* Start USBPLL - moved from camera_misc */
	/* Can be moved to preserve power - must be coordinated with Wifi */
	int ret_val;
	ret_val = mxc_pll_request_pll(USBPLL);
	
	if(ret_val != 0) {
		printk("DMCAM: Unable to request USBPLL\n");
		return ret_val;
	}
#endif
 	/* Use major number of 0 to get one dynamically assigned. */
	major = register_chrdev(0, NAME, &dmcam_fops);

	printk("DMCAM: Started\n");

	return(0);
}

/* Just before the module is removed, execute the stop routine. */
static void __exit dmcam_stop(void)
{
#ifdef CONFIG_MOT_FEAT_PM
	int ret_val;
	ret_val = mxc_pll_release_pll(USBPLL);
	if(ret_val != 0) {
		printk("DMCAM: Unable to release USBPLL\n");
		/* Ignore this error & Continue with the rest of exit */
	}
#endif
	if (dm299_powered_file != DMOFF)
		dmcam_power_off();

	unregister_chrdev(major, NAME);

	printk("DMCAM: Stopped\n");
	
	return;
}

module_init(dmcam_init);
module_exit(dmcam_stop);

MODULE_AUTHOR("Motorola");
#ifdef	MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif



