/* DMCAM                                             <dmcam_ioctl.h> */
/*                                                                   */
/* Copyright (C) 2007-2008 Motorola, Inc.                                 */
/*                                                                   */

/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 30-May-2007  Motorola    Initial revision.
 * 28-Nov-2007  Motorola    Added ioctls for handling quickview
 * 02-Jan-2008  Motorola    Added ioctls for handling viewfinder resolution 
 *                          change
 * 21-Jan-2008  Motorola    Removed obsolete ioctl calls 
 */

#ifndef __DMCAM_IOCTL_H
#define __DMCAM_IOCTL_H

#include <linux/ioctl.h>

#define DMCAM_IOC_NUMBER                  0xCF

#define DMCAM_IOC_POWER_ON                _IO(DMCAM_IOC_NUMBER, 0)
#define DMCAM_IOC_POWER_OFF               _IO(DMCAM_IOC_NUMBER, 1)
#define DMCAM_IOC_BOOT_MODE               _IO(DMCAM_IOC_NUMBER, 2)
#define DMCAM_IOC_APP_MODE                _IO(DMCAM_IOC_NUMBER, 3)
#define DMCAM_IOC_RESET_DM299             _IO(DMCAM_IOC_NUMBER, 4)
#define DMCAM_IOC_WAKEUP_DM299            _IO(DMCAM_IOC_NUMBER, 5)
#define DMCAM_IOC_CHARGE_LEVEL_HIGH       _IO(DMCAM_IOC_NUMBER, 6)
#define DMCAM_IOC_CHARGE_LEVEL_LOW        _IO(DMCAM_IOC_NUMBER, 7)
#define DMCAM_IOC_SET_AF_LED_1            _IO(DMCAM_IOC_NUMBER, 8)
#define DMCAM_IOC_SET_AF_LED_2	          _IO(DMCAM_IOC_NUMBER, 9)
#define DMCAM_IOC_AF_LED_OFF              _IO(DMCAM_IOC_NUMBER, 10)

/*Path to camera driver device node*/
#define DMCAM "/dev/dmcam"

#endif /*__DMCAM_IOCTL_H*/


