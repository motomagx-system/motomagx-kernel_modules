/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : udp_daemon.c
*   FUNCTION NAME(S): device_monitor
*                     main
*                     udp_daemon
*                     udp_daemon
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation
* 28Apr2005  Motorola Inc.         Added pthread monitor
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <unistd.h>
#include <ctype.h>
#include <netdb.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#define __USE_GNU
#include <string.h>

#define BUFFER_SIZE 0xF000

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
static char sock_buffer[BUFFER_SIZE];
static char dev_buffer[BUFFER_SIZE]; 

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
#ifdef USE_SELECT /* The select operations is quite slow */
#define buffer sock_buffer
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: udp_daemon
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   The monitor using select
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation
* 13May2005  Motorola Inc.         Fixed a bug of FD_SET 
*
*****************************************************************************************/
int udp_daemon(int sockfd,
        struct sockaddr_in* destaddr,
        const char* devname)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    fd_set fds;
    struct sockaddr_in tmpaddr;
    socklen_t addrlen = sizeof(tmpaddr);
    /* open the device */
    int devfd = open(devname, O_RDWR);
    if (devfd < 0)
    {
        perror("Open device failed");
        return errno;
    }

    for (;;)
    {
        FD_ZERO(&fds);
        FD_SET(sockfd, &fds);
        FD_SET(devfd, &fds);

        int ret = select(devfd + 1,
                &fds,
                NULL, NULL, NULL);
        if (ret < 0)
        {
            perror("Select failed");
            close(devfd);
            return errno;
        }
        /* If we got data from server */
        if (FD_ISSET(sockfd, &fds))
        {
            if ((ret = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, 
                            (struct sockaddr*)&tmpaddr, &addrlen)) >= 0)
            {
                printf("got %d bytes from socket\n", ret);
                if ((ret = write(devfd, buffer, ret)) < 0)
                {
                    perror("Write to device failed");
                }
            }
            else
            {
                perror("recvfrom failed");
                close(devfd);
                return errno;
            }
        }
        if (FD_ISSET(devfd, &fds))
        {
            if ((ret = read(devfd, buffer, BUFFER_SIZE)) >= 0)
            {
                printf("got %d bytes from device\n", ret);
                if (sendto(sockfd, buffer, ret, 0, 
                                (struct sockaddr*)destaddr, sizeof(*destaddr)) < 0)
                {
                    /* failed? maybe the data is not ready. Reading from the
                     * device is not blocked.
                     */
                    perror("send data to socket failed");
                }
                if (ret == 0)
                {
                    /* it's time to exit */
                    printf("Task finished\n");
                    close(devfd);
                    return 0;
                }
            }
            else
            {
                perror("read from device failed");
                close(devfd);
                return errno;
            }
        } /* if device has data */
    } /* for */
    return 0;
}

#else
struct environment_t
{
    int sockfd;
    int devfd;
    struct sockaddr_in* destaddr;
};

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: device_monitor
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from the ipc device and redirect it to UDP
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void* device_monitor(void* p)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = 0;
    struct environment_t* env = (struct environment_t*)p;

/*--------------------------------------- CODE -----------------------------------------*/
    memset(dev_buffer, 0, BUFFER_SIZE);
    while ((ret = read(env->devfd, dev_buffer, BUFFER_SIZE)) >= 0)
    {
        printf("---->device_monitor: got: %d from pseu-dev\n", ret);
        if (sendto(env->sockfd, dev_buffer, ret, 0, 
                    (struct sockaddr*)env->destaddr, sizeof(*env->destaddr)) < 0)
        {
            perror("send data to socket failed");
        }
        if (ret == 0)
        {
            printf("Task finished\n");
            close(env->devfd);
            return 0;
        }
        memset(dev_buffer, 0, BUFFER_SIZE);
    }
    perror("read from device failed");
    close(env->devfd);
    return (void*)errno;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: udp_daemon
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from udp and then redirect it to the IPC device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Apr2005  Motorola Inc.         Initial Creation
* 19Aug2005  Motorola Inc.         Increase priority of UDP thread to  make data transmission more  efficient
*
*****************************************************************************************/
static int udp_daemon(int sockfd,
        struct sockaddr_in* destaddr,
        const char* devname)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct environment_t env;
    struct sockaddr_in tmpaddr;
    socklen_t addrlen = sizeof(tmpaddr);
    pthread_t thread_id;
    pthread_attr_t attr;
    int ret;
/*--------------------------------------- CODE -----------------------------------------*/
    /* open the device */
    env.devfd = open(devname, O_RDWR);
    if (env.devfd < 0)
    {
        perror("Open device failed");
        return errno;
    }
    /* assign the environment */
    env.sockfd = sockfd;
    env.destaddr = destaddr;
    /* create the device monitor thread */
    pthread_attr_init(&attr);
    pthread_attr_setschedpolicy(&attr, SCHED_RR);
    ret = pthread_create(&thread_id, &attr, &device_monitor, &env);
    if (ret != 0)
    {
        perror("Thread creating failed");
        return -1;
    }
    /* monitor the socket */
    memset(sock_buffer, 0, BUFFER_SIZE);
    while ((ret = recvfrom(sockfd, sock_buffer, BUFFER_SIZE, 0, 
                    (struct sockaddr*)&tmpaddr, &addrlen)) >= 0)
    {
        printf("---->upd_daemon: got %d from socket\n", ret);
        if ((ret = write(env.devfd, sock_buffer, ret)) < 0)
        {
            perror("Write to device failed");
        }
        memset(sock_buffer, 0, BUFFER_SIZE);
    }
    perror("recvfrom failed");
    close(env.devfd);

    return errno;
}
#endif /* USE_SELECT */
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   main function.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation
* 30Aug2005  Motorola Inc.         Checked null pointer and initialed some values 
*
*****************************************************************************************/
int main(int argc, char** argv)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int sockfd;
    struct sockaddr_in addr;
    struct hostent* host;
    int i;

    char* devname = NULL;
    char* destname = NULL;

    unsigned short destport = 0;
    unsigned short myport = 0;

    if (argc != 7)
    {
        fprintf(stderr, "Usage: %s -d devname -t destname:destport -p myport\n\n"
         "\t-d devname           --  specify the pseudo device name\n"
         "\t-t destname:destpor  --  specify the hostname and port of destination\n"
         "\t-p myport            --  wait data from myport\n",
                argv[0]);
        return 1;
    }
    /* parse command line options */
    /*
     * FIXME: it's not very strict, our program is just a demo, isn't it?
     */
    for (i=1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            if (argv[i][1] == 'd')
            {
                devname = argv[++i];
            }
            else if (argv[i][1] == 't')
            {
                char* colonpos = strchr(argv[++i], ':');
                if ((NULL == colonpos ) || (!isdigit(colonpos[1])))
                {
                    fprintf(stderr, "Destname '%s' not valid, should be hostname:port",
                            argv[i]);
                    return 1;
                }
                destname = (char*)strndup(argv[i], (size_t)(colonpos - argv[i]));
                destport = atoi(colonpos+1);
            }
            else if (argv[i][1] == 'p')
                myport = atoi(argv[++i]);
        }
    }
    printf("device name: %s\n"
           "dest host: %s\n"
           "dest port: %u\n"
           "my port: %u\n",
           devname, destname, destport, myport);

    if (NULL == devname)
    {
        fprintf(stderr, "ERROR: devname is null\n");
        return 1;
    }

    if (NULL == destname)
    {
        fprintf(stderr, "ERROR: destname is null\n");
        return 1;
    }

    /* create a socket, bind, then run the daemon procedure */
    sockfd = socket(PF_INET, SOCK_DGRAM, 0);

    if (sockfd <= 0) 
    {
        perror("Can't create the socket");
        return 1;
    }

    memset(&addr, 0, sizeof(struct sockaddr_in));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(myport);

    if (bind(sockfd, (struct sockaddr*)&addr, sizeof(addr))) {
        perror("Bind failed");
        close(sockfd);
        return 1;
    }
    /* fill the dest addr */
    host = (struct hostent*)gethostbyname(destname);
    if (host == NULL)
    {
        perror("gethostbyname failed");
        return 1;
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(destport);
    //inet_aton(host, &addr.sin_addr);
    memcpy((void*)&addr.sin_addr, (void*)(host->h_addr), host->h_length); 

    printf("Now entering the main loop\n");
    return udp_daemon(sockfd, &addr, devname);
}

