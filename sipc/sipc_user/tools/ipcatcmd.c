/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcatcmd.c
*   FUNCTION NAME(S): main
*                     print_usage
*                     receiver
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 28Jul2005  Motorola Inc.         Initial Creation
* 02Jun2006  Motorola Inc.         Make AT command structure complying with LPK's defination
* 21Aug2007  Motorola Inc.         Fixed OSS compliance issue
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include "ipcsocket.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
#define MIN_LPK_SVCID IPC_UNICAST_SERVICE(0x2000)
#define MAX_LPK_SVCID IPC_UNICAST_SERVICE(0x2006)
#define LPK_NODID 14
#define DEFAULT_SLEEP_INTERVAL 50
#define MAX_BUF_LEN 256
#define MAX_FILENAME_LEN 256

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
static int socket_type = IPC_CL_SOCK;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

/* the following structure MUST be same as SmartIPC_Mapping_Header_t defined
 * in file /vob/su_common/ipc/ros/include/smartipc_mapping.h.
 */
typedef struct tag_coAtCmdHeader_t
{
    char sourceID0;
    char sourceID1;
    unsigned short opcode;
    unsigned long  length;
} coAtCmdHeader_t;


/*--------------------------------------- MACROS ---------------------------------------*/
#define printerr()  printf("Last error %d = %s\n", ipcgetlasterror(), ipcgeterrortext(ipcgetlasterror()))    

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: print_usage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
static void print_usage(char * argv[])
{
    fprintf(stderr, 
        "=================================================\n"
        "Usage: \n"
        "%s [-n destnode] [-s serviceid] [-p msec] [-f cmdFile] [-co] [-h]\n\n"
        "      -n destnode  -- specify the dest node id, default %d\n"
        "      -s serviceid -- specify the dest and source service id for CL socket, \n"
        "                   -- or specify the dest server service id for CO socket, \n"
        "                   -- range %u~%u, default %u\n"
        "      -p msec      -- specify the interval milliseconds between two cmds, default %d\n"
        "      -f cmdFile   -- send AT COMMAND from cmdFile, default from input line\n"
        "      -co          -- use CO socket, default use CL socket\n"
        "      -h           -- print usage\n"
        "=================================================\n\n",
        argv[0], 
        LPK_NODID, 
        MIN_LPK_SVCID,
        MAX_LPK_SVCID,
        MAX_LPK_SVCID, 
        DEFAULT_SLEEP_INTERVAL);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: receiver
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
static void receiver(void * param)
{
    int sock = (int)param;
    struct IPCSOCK_ADDR addr;
    char buf[MAX_BUF_LEN];
    int rc;
    int fromlen;
    int o_state;
    coAtCmdHeader_t coAtCmdHdr;

    pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &o_state);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	
    while (1)
    {
        if (IPC_CL_SOCK == socket_type)
        {
            fromlen = sizeof(struct IPCSOCK_ADDR);
            rc = ipcrecvfrom(sock, buf, sizeof(buf)-1, 0, &addr, &fromlen);
            if (rc < 0)
            {
                printf("ipcrecvfrom error!\n");
                printerr();
            }
            else
            {
                buf[rc] = '\0';
                printf("Receive: %s\n", buf);
            }
        }
        else
        {
            rc = ipcrecv(sock, buf, sizeof(buf)-1, 0);
            if (rc < 0)
            {
                printf("ipcrecv error!\n");
                printerr();
            }
            else
            {
                buf[rc] = '\0';
                memcpy(&coAtCmdHdr, buf, sizeof(coAtCmdHdr));
                printf("Receive: header{0x%x,0x%x,0x%x,%lu} %s\n", 
                    coAtCmdHdr.sourceID0,
                    coAtCmdHdr.sourceID1,
                    coAtCmdHdr.opcode,
                    coAtCmdHdr.length, 
                    buf+sizeof(coAtCmdHdr));
            }
        }
    }


}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Jul2005  Motorola Inc.         Initial Creation
* 01Mar2006  Motorola Inc.         Remove deprecated SIPC definitions 
*
*****************************************************************************************/
int main(int argc, char * argv[])
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int sock;
    struct IPCSOCK_ADDR addr, to;
    int rc = 0;
    char buf[MAX_BUF_LEN];
    int len = 0;
    int i = 0;
    pthread_t rcvThrd;
    FILE * fp = NULL;
    
    unsigned long dstNode;
    unsigned short srcSvc, dstSvc;
    unsigned int sleepTime;
    char filename[MAX_FILENAME_LEN];
    int bUseFile = 0;
    coAtCmdHeader_t coAtCmdHdr;
    char buf_co[sizeof(buf)+sizeof(coAtCmdHdr)];

/*--------------------------------------- CODE -----------------------------------------*/
    print_usage(argv);

    /* set the default value */
    dstNode = LPK_NODID;
    srcSvc = dstSvc = MAX_LPK_SVCID;
    sleepTime = DEFAULT_SLEEP_INTERVAL;
    socket_type = IPC_CL_SOCK;
    
    /* parse command line options */
    for (i=1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            if (argv[i][1] == 'n')
            {
                dstNode = atoi(argv[++i]);
            }
            else if (argv[i][1] == 's')
            {
                srcSvc = dstSvc = atoi(argv[++i]);
            }
            else if (argv[i][1] == 'p')
            {
                sleepTime = atoi(argv[++i]);
            }
            else if (argv[i][1] == 'f')
            {
                strncpy(filename, argv[++i], sizeof(filename) - 1);
                filename[sizeof(filename) - 1] = '\0';
                bUseFile = 1;
            }
            else if (argv[i][1] == 'h')
            {
                return 0;
            }
            else if (strncmp(argv[i], "-co", strlen(argv[i])) == 0)
            {
                socket_type = IPC_CO_SOCK;
            }
            else
            {
                return -1;
            }
        }
    }

    if (IPC_CL_SOCK == socket_type)
    {
        srcSvc = dstSvc;
    }
    else
    {
        srcSvc = IPC_SERVICE_ANY;
    }
    
    memset(&addr, 0, sizeof(addr));
    addr.ipc_service = srcSvc;

    to.ipc_node = dstNode;
    to.ipc_service = dstSvc;

    printf(
        "socket type: %s\n"
        "dest node: %lu\n"
        "source service: %u\n"
        "dest service: %u\n"
        "send interval: %u\n"
        "command file: %s\n",
        (IPC_CO_SOCK == socket_type)?"CO":"CL",
        dstNode, 
        srcSvc,
        dstSvc, 
        sleepTime, 
        bUseFile?filename:"NULL");

    if (ipcinit(0, 0) != 0)
    {
        fprintf(stderr, "ipcinit(0, 0) failed!\n");
        return -1;
    }

    sock = ipcsocket(AF_IPC, socket_type, IPC_PROTO);

    if (sock != IPC_ERROR) 
    {
        printf("create socket OK!\n");
    }
    else
    {
        printf("create socket failed!\n");
        printerr();
        goto fail1;
    }

    rc = ipcbind(sock, &addr, sizeof(addr));
    if (rc != IPC_OK) 
    {
        printf("bind service id %u failed!\n", srcSvc);
        printerr();
        goto fail2;
    }
    else
    {
        printf("bind service id %u successfully!\n", srcSvc);
    }

    if (IPC_CO_SOCK == socket_type)
    {
        rc = ipcconnect(sock, &to, sizeof(to));
        if (rc != IPC_OK) 
        {
            printf("connect to server service %ld:%u failed!\n", dstNode, dstSvc);
            printerr();
            goto fail2;
        }
        else
        {
            printf("connect to server service %ld:%u successfully!\n", dstNode, dstSvc);
        }
    }

    rc = pthread_create(&rcvThrd, NULL, (void *)receiver, (void *)sock);
    if (rc)
    {
        fprintf(stderr, "pthread_create() failed!\n");
        goto fail2;
    }
    
    if (bUseFile)
    {
        fp = fopen(filename, "r");
        if (NULL == fp)
        {
            fprintf(stderr, "open file %s failed!\n",filename);
            goto fail2;
        }
    }
    
    while (1)
    {
        memset(buf, 0, sizeof(buf));

        if (bUseFile)
        {
            if (NULL == fgets(buf, sizeof(buf)-3, fp))
            {
                if (ferror(fp))
                {
                    fprintf(stderr, "fgets from file %s failed!\n", filename);
                }
                else if (feof(fp))
                {
                    fprintf(stderr, "finish to send file %s\n", filename);
                }

                fclose(fp);
                pthread_cancel(rcvThrd);
                pthread_join(rcvThrd, NULL);
                break;
            }
        }
        else
        {
            printf("input: >");

            fgets(buf, sizeof(buf)-3, stdin);
            if (buf[0] == '#') 
            {
                pthread_cancel(rcvThrd);
                pthread_join(rcvThrd, NULL);
                break;
            }
        }
        
        len = strlen(buf);
        if (len > 0)
        {
            while ((len > 0) && ('\n' == buf[len-1]
                ||'\r' == buf[len-1]))
            {
                buf[--len] = '\0';
            }

            buf[len++] = '\n';
            buf[len++] = '\r';
            buf[len++] = '\0';

            if (IPC_CL_SOCK == socket_type)
            {
                rc = ipcsendto(sock, buf, len, 0, &to, sizeof(to));
                if (rc < 0)
                {
                    printf("ipczsendto error!\n");
                    printerr();
                }
                else
                {
                    printf("Send: %s \n", buf);
                }
            }
            else
            {
                memset(&coAtCmdHdr, '0', sizeof(coAtCmdHdr));
                coAtCmdHdr.opcode = 0x8001;
                coAtCmdHdr.length = len;
                memcpy(buf_co, &coAtCmdHdr, sizeof(coAtCmdHdr));
                memcpy(buf_co+sizeof(coAtCmdHdr), buf, len);
                len += sizeof(coAtCmdHdr);
                rc = ipcsend(sock, buf_co, len, 0);
                if (rc < 0)
                {
                    printf("ipczsend error!\n");
                    printerr();
                }
                else
                {
                    printf("Send: header{0x%x,0x%x,0x%x,%lu} %s\n", 
                        coAtCmdHdr.sourceID0,
                        coAtCmdHdr.sourceID1,
                        coAtCmdHdr.opcode,
                        coAtCmdHdr.length, 
                        buf_co+sizeof(coAtCmdHdr));
                }
            }
        }

        if (sleepTime > 0)
        {
            usleep(sleepTime * 1000);
        }
        else
        {
            usleep(DEFAULT_SLEEP_INTERVAL * 1000);
        }
    }
fail2:
    ipcclosesocket(sock);
fail1:
    ipcclose();
    return 0;

}
