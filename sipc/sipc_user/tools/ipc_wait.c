/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2006 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_wait.c
*   FUNCTION NAME(S): handle_arg
*                     ipc_socket_and_bind
*                     main
*                     split_commands
*                     usage
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 21Apr2006  Motorola Inc.         Initial Creation 
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipcsocket.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
static int sock;
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#define ipc_perror(msg, va...) \
    printf("%s:%d: " msg " - %s\n", __FUNCTION__, __LINE__, ##va, \
            ipcgeterrortext(ipcgetlasterror()))

#define IPCEXPECT(b) \
    do { if (!(b)) {\
            ipc_perror("Unsatisfied: '"#b "'"); \
            exit(1); \
         } \
    }while(0)

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: usage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Apr2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void usage(int argc, char* argv[])
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    fprintf(stderr, 
            "Usage: %s NODE:SERVICE:{on|off|both}...\n"
            "\n"
            "Example:\n"
            "    %s 2:34567:on IPC_NODE_ANY:32888:off\n",
            argv[0], argv[0]);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_socket_and_bind
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Apr2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int ipc_socket_and_bind(int is_co, 
        unsigned short service_id, 
        struct IPCSOCK_ADDR* name)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int socket = 0;
    struct IPCSOCK_ADDR addr;
    int addrlen = sizeof(addr);
/*--------------------------------------- CODE -----------------------------------------*/

    socket = ipcsocket(AF_IPC,
            is_co? IPC_CO_SOCK: IPC_CL_SOCK,
            IPC_PROTO);
    if (socket != IPC_ERROR)
    {
        memset(&addr, 0, sizeof(addr));
        addr.ipc_service = service_id;
        if (ipcbind(socket, &addr, addrlen) == IPC_OK)
        {
            if (name != NULL)
            {
                ipcgetsockname(socket, name, &addrlen);
            }
        }
        else
        {
            ipcclosesocket(socket);
            socket = -1;
        }
    }
    return socket;
}

#define is_delimiter(c, d) (((c) == 0) || (strchr((d), (c))))

#define bypass_delimiter(p, d) \
	do { while (is_delimiter(*p, d) && (*p != 0) ) p++; } while (0);

#define bypass_word(p, d) \
	do { while ((! is_delimiter(*p, d)) && (*p != 0) ) p++;} while (0);

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: split_commands
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Apr2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int split_commands(char* buffer, char* delimiters, char* argv[], int max_arg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* p = buffer;
    int n = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    while (*p != 0)
    {
        bypass_delimiter(p, delimiters);
        argv[n ++] = p;		
	bypass_word(p, delimiters);
	if(*p != 0)
	{
            * p = 0;
            p++;		
        }
        if (n >= max_arg  ) 
        {
            break;
        }
    }
    return  n;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handle_arg
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Apr2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int handle_arg(char* arg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
    struct IPCSOCK_ADDR addr;
    int addrlen = sizeof(addr);
    int msglen = strlen(arg) + 1;
    int flags = 0;
    char* argv[3];
    char* buffer;
    int argc;
/*--------------------------------------- CODE -----------------------------------------*/

    argc = split_commands(arg, ":", argv ,3);
    
    if (argc != 3)
    {
        return -1;
    }

    addr.ipc_node = atol(argv[0]);
    if (addr.ipc_node == 0)
    {
        if (!isdigit(argv[1][0]) && (strcmp(argv[0], "IPC_NODE_ANY") != 0))
        {
            return -1;
        }
    }
    addr.ipc_service = atol(argv[1]);
    if (addr.ipc_service == 0)
    {
        if (!isdigit(argv[1][0]) && (strcmp(argv[1], "IPC_SERVICE_ANY") != 0))
        {
            return -1;
        }
    }
    if (strcmp(argv[2], "on") == 0)
    {
        flags = IPCONLINE;
    }
    else if (strcmp(argv[2], "off") == 0)
    {
        flags = IPCOFFLINE;
    }
    else if (strcmp(argv[2], "both") == 0)
    {
        flags = IPCONLINE | IPCOFFLINE;
    }

    if (NULL == (buffer = malloc(msglen)))
    {
        return -1;
    }
    msglen = 1 + snprintf(buffer, msglen, "%ld:%d:%s",
                                addr.ipc_node,
                                addr.ipc_service,
                                argv[2]);
    if ((rc = ipcnotify(sock, &addr, addrlen, buffer, msglen, flags)) < 0)
    {
        ipc_perror("Failed to register notification on [%ld:%d]\n",
                addr.ipc_node, addr.ipc_service);
    }
    free(buffer);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Apr2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int main(int argc, char* argv[])
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct IPCSOCK_ADDR addr;
    int addrlen = sizeof(addr);
    int rc = 0;
    char* p;
    int i = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    if (argc == 1)
    {
        usage(argc, argv);
        return 1;
    }

    IPCEXPECT(ipcinit(0, 0) == IPC_OK);
    IPCEXPECT((sock = ipc_socket_and_bind(0, 0, 0)) != IPC_ERROR);

    for (i = 1; i < argc; i ++)
    {
        if ((rc = handle_arg(argv[i])) < 0)
        {
            usage(argc, argv);
            break;
        }
    }

    if (i == argc)
    {
        IPCEXPECT(ipczrecvfrom(sock, &p, 0, &addr, &addrlen) > 0);
        printf("%s\n", p);
        ipczfree(p);
    }
    IPCEXPECT(ipcclosesocket(sock) == IPC_OK);
    IPCEXPECT(ipcclose() == IPC_OK);
    return rc;
}

