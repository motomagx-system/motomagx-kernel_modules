/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcdump.c
*   FUNCTION NAME(S): cleanup
*                     hex_and_ascii_print
*                     ipc_perror
*                     main
*                     parse_filter_rule
*                     usage
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 26Jul2005  Motorola Inc.         Initial Creation
* 12Aug2005  Motorola Inc.         Add timestamp 
* 29Jun2007  Motorola Inc.         Added sender task name in log message
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include "ipcsocket.h"

#include "ipc_memory_access.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern char *optarg;

int sock = 0;
struct IPCSOCK_ADDR addr, to;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#ifndef offsetof
#define offsetof(s, m) (size_t)&(((s *)0)->m)
#endif

#define ASCII_LINELENGTH 300
#define HEXDUMP_BYTES_PER_LINE 16
#define HEXDUMP_SHORTS_PER_LINE (HEXDUMP_BYTES_PER_LINE / 2)
#define HEXDUMP_HEXSTUFF_PER_SHORT 5 
#define HEXDUMP_HEXSTUFF_PER_LINE \
    (HEXDUMP_HEXSTUFF_PER_SHORT * HEXDUMP_SHORTS_PER_LINE)
#define RCFILE "ipcfl.rc"


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_perror
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static inline void ipc_perror(char *msg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    fprintf(stderr, "%s - %s\n",
            msg, ipcgeterrortext(ipcgetlasterror()));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: hex_and_ascii_print
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static void hex_and_ascii_print(register const char *ident,
        register const u_char *cp, register u_int length, register u_int oset)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    register u_int i;
    register int s1, s2;
    register int nshorts;
    char hexstuff[HEXDUMP_SHORTS_PER_LINE*HEXDUMP_HEXSTUFF_PER_SHORT+1], *hsp;
    char asciistuff[ASCII_LINELENGTH+1], *asp;
/*--------------------------------------- CODE -----------------------------------------*/
    nshorts = length / sizeof(u_short);
    i = 0;
    hsp = hexstuff; asp = asciistuff;
    while (--nshorts >= 0)
    {
        s1 = *cp++;
        s2 = *cp++;
        (void)snprintf(hsp, sizeof(hexstuff) - (hsp - hexstuff),
                       " %02x%02x", s1, s2);
        hsp += HEXDUMP_HEXSTUFF_PER_SHORT;
        *(asp++) = (isgraph(s1) ? s1 : '.');
        *(asp++) = (isgraph(s2) ? s2 : '.');
        i++;
        if (i >= HEXDUMP_SHORTS_PER_LINE)
        {
            *hsp = *asp = '\0';
            (void)printf("%s0x%04x: %-*s  %s",
                         ident, oset, HEXDUMP_HEXSTUFF_PER_LINE,
                         hexstuff, asciistuff);
            i = 0; hsp = hexstuff; asp = asciistuff;
            oset += HEXDUMP_BYTES_PER_LINE;
        }
    }

    if (length & 1)
    {
        s1 = *cp;
        (void)snprintf(hsp, sizeof(hexstuff) - (hsp - hexstuff),
                       " %02x", s1);
        hsp += 3;
        *(asp++) = (isgraph(s1) ? s1 : '.');
        ++i;
    }

    if (i > 0) 
    {
        *hsp = *asp = '\0';
        (void)printf("%s0x%04x: %-*s  %s",
                     ident, oset, HEXDUMP_HEXSTUFF_PER_LINE,
                     hexstuff, asciistuff);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: cleanup
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 13Mar2006  Motorola Inc.         Removed signal checking 
*
*****************************************************************************************/
static void cleanup(int signo)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcLogConfigMsg_t dumpreq;
/*--------------------------------------- CODE -----------------------------------------*/

    memset(&dumpreq, 0, sizeof(ipcLogConfigMsg_t));
    /* send a request to stack to remove rules */
    if (0 == sock)
    {
        exit(0);
    }

    ipcsendto(sock, (const char *)&dumpreq, sizeof(ipcLogConfigMsg_t), 0, &to, sizeof(to));

    fflush(stdout);

    ipcclosesocket(sock);

    ipcclose();

    exit (0);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: usage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 23Mar2006  Motorola Inc.         Added help information 
*
*****************************************************************************************/
void usage()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    printf("Usage: ipcdump <-n dump_node> [-d] [-f rule_file] \n"
            "               [-s my_service_id] [-r filter_rule_expression]... \n"
            "\n"
            "  <-n dump_node>: The filter rules will be set in node dump_node\n"
            "  [-d]: Destroy all filter rules\n"
            "  [-f rule_file]: specify rule_file\n"
            "  [-s my_service_id]: specify service id of dumping socket\n"
            "  [-r filter_rule_expression]: specify dump rules, syntax:\n"
            "     src_node_id:src_service_id->dest_node_id:dest_service_id{dump_data_len}\n"
            "  [-h]: Print this help message\n"
            "\n"
            "Examples:\n"
            "  Dumping all packets through node 2:\n"
            "    ipc_dump -n 2 -r \"*:*->*:*{*}\" \n\n"
            "  The syntax of the rule string:\n"
            "    src_node:src_service->dest_node:dest_service{length}[@]\n"
            "  Character '*' stands for 'all values'\n"
            "  If an ending '@' is specified, the timestamp is also printed\n");
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: parse_filter_rule
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int parse_filter_rule(char * rule, ipcLogFilter_t *filter)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char *p, *q, *tmp;
/*--------------------------------------- CODE -----------------------------------------*/
    p = rule;

    tmp = strstr(p, "->");
    if (NULL == tmp) return -1;

    q = strchr(p, ':');
    if (q != NULL)
    {
        if (p[0] == '*')
        {
            ipcWrite32(&filter->srcNode, IPC_NODE_ANY);
        }
        else
        {
            ipcWrite32(&filter->srcNode, atol(p));
        }

        if (q[1] == '*')
        {
            ipcWrite32(&filter->srcService, IPC_SERVICE_ANY);
        }
        else
        {
            ipcWrite32(&filter->srcService, atol(q + 1));
        }
    }
    else
    {
        return -1;
    }

    p = tmp + 2;
    q = strchr(p, ':');
    if (q != NULL)
    {
        if (p[0] == '*')
        {
            ipcWrite32(&filter->destNode, IPC_NODE_ANY);
        }
        else
        {
            ipcWrite32(&filter->destNode, atol(p));
        }

        if (q[1] == '*')
        {
            ipcWrite32(&filter->destService, IPC_SERVICE_ANY);
        }
        else
        {
            ipcWrite32(&filter->destService, atol(q + 1));
        }
    }
    else
    {
        return -1;
    }

    q = strchr(p, '{');
    if (NULL == q)
    {
        ipcWrite32(&filter->action, IPC_LOG_WITHOUT_DATA);
    }
    else
    {
        p = q + 1;
        if (p[0] == '*')
        {
            ipcWrite32(&filter->action, IPC_LOG_WITH_ALL_DATA);
        }
        else
        {
            ipcWrite32(&filter->action, atol(p));
        }
    }

    p = strchr(rule, '@');
    if (NULL != p)
    {
        unsigned long action = ipcRead32(&filter->action);
        action |= IPC_LOG_TIMESTAMP;
        ipcWrite32(&filter->action, action);
    }

    return 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 23Mar2006  Motorola Inc.         Fix endianess bug
* 01Mar2006  Motorola Inc.         Remove deprecated SIPC definitions
* 24May2006  Motorola Inc.         Bugfix for opening filter rule file failure 
* 19Jul2006  Motorola Inc.         Added original size of packets 
* 29Jun2007  Motorola Inc.         Added sender task name in log message
*
*****************************************************************************************/
int main(int argc, char **argv)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int len = 0;
    int rc;
    int originalDataSize = 0;
    int c;
    ipcLogConfigMsg_t dumpreq;
    static char optstr[] = "r:dn:s:f:h";
    int index = 0;
    int node_id = IPC_NODE_ANY;
    int local_srv_id = IPC_SRV_MULTI_PACKET_LOG;
    ipcLogPacketMsg_t *dumpdata = NULL;
    long sec = 0;
    long usec = 0;
    FILE *rcfd = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    memset(&dumpreq, 0, sizeof(ipcLogConfigMsg_t));

    while ((c = getopt(argc, argv, optstr)) != -1)
    {
        switch (c)
        {
            case 'r':
                /* src_node:src_srv->dest_node:dest_srv{*} */
                rc = parse_filter_rule(optarg, &dumpreq.filters[index]);
                if (rc < 0)
                {
                    printf("unknown rule: %s\n", optarg);
                    exit(1);
                }
                index++;
                break;

            case 'd':
                ipcWrite32(&dumpreq.numFilters, 0);
                break;

            case 'n':
                node_id = atoi(optarg);
                break;

            case 's':
                local_srv_id = atoi(optarg);
                break;

            case 'f':
                rcfd = fopen(optarg, "r");
                if (rcfd == NULL)
                {
                    printf("Cannot open filter rules description file %s\n", optarg);
                }
                else
                {
                    printf("Open filter rules description file %s\n", optarg);
                }
                break;

            case 'h':
                printf("Unknown params: %s\n", optarg);
                usage();
                exit(1);
                break;
        }
    }

    if (index == 0 && rcfd == NULL)
    {
        /* try to open filter rule files at local dir, named: ipcfl.rc */
        printf("Open default filter rules file:ipcfl.rc...");
        rcfd = fopen(RCFILE, "r");
        if (rcfd == NULL)
        {
            printf("failed!\n");
        }
        else
        {
            printf("ok\n");
        }
    }

    if (NULL != rcfd)
    {
        char *line = NULL;
        int len;
        ssize_t read;

        while ((read = getline(&line, &len, rcfd)) != -1)
        {
            if (line[0] != '#')
            {
                if (parse_filter_rule(line, &dumpreq.filters[index]) < 0)
                {
                    continue;
                }
                else
                {
                    index++;
                }
            }
        }

        if (NULL != line)
        {
            free(line);
        }
    }

    if (node_id == IPC_NODE_ANY)
    {
        printf("You MUST specify which node dump the data!\n");
        usage();
        exit(1);
    }

    if (index == 0)
    {
        printf("No valid filter rules specified!\n");
        exit(1);
    }

    signal(SIGINT, cleanup);
    signal(SIGTERM, cleanup);

    if (index != 0)
    {
        ipcWrite32(&dumpreq.numFilters, index);
    }

    rc = ipcinit(0, 0);
    if (rc < 0)
    {
        printf("Failed to connect to IPC stack!\n");
        return -1;
    }

    sock = ipcsocket(AF_IPC, IPC_CL_SOCK, IPC_PROTO);

    if (sock == -1)
    {
        ipc_perror("create socket failed!");
        goto fail;
    }

    addr.ipc_node = IPC_NODE_ANY;
    addr.ipc_service = local_srv_id;

    rc = ipcbind(sock, &addr, sizeof(addr));
    if (rc < 0)
    {
        ipc_perror("bind failed!");
        goto fail1;
    }

    to.ipc_node = node_id;
    to.ipc_service = IPC_SRV_LOG_CONFIG;

    /* send the request to IPC stack */
    rc = ipcsendto(sock, (const char *)&dumpreq, sizeof(dumpreq), 0, &to, sizeof(to));
    if (rc < 0)
    {
        ipc_perror("ipcsendto failed");
        goto fail1;
    }

    len = sizeof(struct IPCSOCK_ADDR);

    while (1)
    {
        rc = ipczrecvfrom(sock, (char * *)(void *)&dumpdata, 0, &addr, &len);
        if (rc < 0)
        {
            ipc_perror("ipczrecvfrom failed:");
            break;
        }

        rc -= offsetof(ipcLogPacketMsg_t, data);

        sec = ipcRead32(&dumpdata->timestamp_sec);
        usec = ipcRead32(&dumpdata->timestamp_usec);
        originalDataSize = ipcRead32(&dumpdata->originalDataSize);
        if ((0 != sec)
                && (0 != usec))
        {
            printf("%010ld:%06ld ", 
                    sec,
                    usec);
        }
        printf("[%lu:%lu(0x%.4X)] --> [%lu:%lu(0x%.4X)] by (%s) %d of %d bytes", 
                ipcRead32(&dumpdata->srcNode),
                ipcRead32(&dumpdata->srcService),
                (unsigned int)ipcRead32(&dumpdata->srcService),
                ipcRead32(&dumpdata->destNode),
                ipcRead32(&dumpdata->destService),
                (unsigned int)ipcRead32(&dumpdata->destService),
                dumpdata->processName,
                rc, originalDataSize);
        hex_and_ascii_print("\n\t", dumpdata->data, rc, 0);

        printf("\n");
        ipczfree(dumpdata);
    }

fail1:
    ipcclosesocket(sock);
fail:
    ipcclose();

    return 0;
}
