/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcmemdump.c
*   FUNCTION NAME(S): hex_and_ascii_print
*                     usage
*                     main
*                     
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 26Mar2007  Motorola Inc.         Initial Creation 
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include <unistd.h>
#include "ipcsocket.h"

#include "ipc_memory_access.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
extern char *optarg;
int sock = 0;
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#define ASCII_LINELENGTH 300
#define HEXDUMP_BYTES_PER_LINE 16
#define HEXDUMP_HEXSTUFF_PER_SHORT 5 
#define HEXDUMP_SHORTS_PER_LINE    (HEXDUMP_BYTES_PER_LINE / 2)
#define HEXDUMP_HEXSTUFF_PER_LINE  (HEXDUMP_HEXSTUFF_PER_SHORT * HEXDUMP_SHORTS_PER_LINE)
	
#define ULONG_MAX	(~0UL)

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: hex_and_ascii_print
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   print memmory information in hex or ascii format
*   
*   
*   PARAMETER(S):
*   ident: identifier to be printed before each line
*   cp: pointer to memory block to be printed
*   len: size of memmory block
*   oset: offset from cp

*   RETURN: 
*   void 
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Mar2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static void hex_and_ascii_print(register const char *ident,
					register const u_char *cp, register u_int length, register u_int oset)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    register u_int i;
    register int s1, s2;
    register int nshorts;
    char hexstuff[HEXDUMP_SHORTS_PER_LINE*HEXDUMP_HEXSTUFF_PER_SHORT+1], *hsp;
    char asciistuff[ASCII_LINELENGTH+1], *asp;
/*--------------------------------------- CODE -----------------------------------------*/
    nshorts = length / sizeof(u_short);
    i = 0;
    hsp = hexstuff; asp = asciistuff;
    while (--nshorts >= 0)
    {
        s1 = *cp++;
        s2 = *cp++;
        (void)snprintf(hsp, sizeof(hexstuff) - (hsp - hexstuff),
			" %02x%02x", s1, s2);
        hsp += HEXDUMP_HEXSTUFF_PER_SHORT;
        *(asp++) = (isgraph(s1) ? s1 : '.');
        *(asp++) = (isgraph(s2) ? s2 : '.');
        i++;
        if (i >= HEXDUMP_SHORTS_PER_LINE)
        {
            *hsp = *asp = '\0';
            (void)printf("%s0x%08x: %-*s  %s",
				ident, oset, HEXDUMP_HEXSTUFF_PER_LINE,
				hexstuff, asciistuff);
            i = 0; 
	    hsp = hexstuff; 
	    asp = asciistuff;
            oset += HEXDUMP_BYTES_PER_LINE;
        }
    }
	
    if (length & 1)
    {
        s1 = *cp;
        (void)snprintf(hsp, sizeof(hexstuff) - (hsp - hexstuff),
			" %02x", s1);
        hsp += 3;
        *(asp++) = (isgraph(s1) ? s1 : '.');
        ++i;
    }
	
    if (i > 0) 
    {
        *hsp = *asp = '\0';
        (void)printf("%s0x%08x: %-*s  %s",
			ident, oset, HEXDUMP_HEXSTUFF_PER_LINE,
			hexstuff, asciistuff);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: usage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   display  usage information for ipcmemdump
*   
*   
*   PARAMETER(S):
*   void 
*
*   RETURN: 
*   void
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Mar2007  Motorola Inc.         Initial Creation 
* 24Apr2007  Motorola Inc.         Modify usage
* 
*****************************************************************************************/
void usage()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    printf("\n"
            "Usage: ipc_memdump [-m baseAddr] [-o offsetAddr] [-l len] [-hdr] [-h] \n"
            "\n"
            "  -m baseAddr: base address you want to monitor, default base address is 0x80000000\n"
            "  -o offsetAddr: offset address you want to monitor\n"
            "  -l len: total number of bytes you want to dump, default value is 128 bytes\n" 
            "  -hdr: print message header, 32 bytes forward offset for base address\n"          
            "  -h: display usage information\n"
            "\n"
            "Examples:\n"
            "  ipc_memdump -m 0x800000ef -o 0xab -l 0x90 -hdr\n\n");
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   dump sipc memory information
*   
*   
*   PARAMETER(S):
*   argc: an integer specifying how many arguments are passed to program
*   argv: an array of null-terminated strings. The first string(argv[0]) is the program
*         name,and each following string is an argument passed to program
*   
*   RETURN: 
*   0 on success
*   -1 on failure
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Mar2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int main(int argc, char **argv)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc;
    
    unsigned long offset_address=0;
    unsigned long  base_address=0x80000000;
    unsigned long  len = 128;
    char  *ptr = NULL;
    static char optstr[] = "m:o:l:h:";
    int addressFlag=0;
    int c;
/*--------------------------------------- CODE -----------------------------------------*/
    while ((c = getopt(argc, argv, optstr)) != -1)
    {
        switch (c)
        {
	    case 'm':
		base_address = strtoul(optarg,NULL,0);
		if(base_address == ULONG_MAX)
		{
	    	    goto fail;
		}
		addressFlag = 1;
		break;
			
	    case 'o':
		offset_address = strtoul(optarg,NULL,0);
		if(offset_address == ULONG_MAX)
		{
		    goto fail;
		}
		addressFlag = 1;
		break;
			
	    case 'l':
		len = strtoul(optarg,NULL,0);
		if(len == ULONG_MAX)
		{
		    goto fail;
		}
		break;

	    /*-hdr*/
	    case 'h':
		if(strstr(optarg,"dr"))
		{
		    offset_address -= 32;
		}
		else
		{
	    	    goto fail;                
		}
		break;
	    /*-h or unknown option*/
	    case '?':
	    default:
		goto fail;
		break;
	}
    }
	
	
    if(addressFlag == 0)
    {
        goto fail;
    }
	
    ptr = (char *)(base_address+offset_address);   
	
    printf("============== Start From  Address 0X%08x ================= \n", (unsigned int)ptr);

    rc = ipcinit(0, 0);
    if (rc < 0)
    {
        printf("Failed to connect to IPC stack!\n");
        return -1;
    }
	
    hex_and_ascii_print("\n\t", ptr, (u_int)len, (u_int)ptr);
	
    printf("\n");
	
    ipcclose();
	
    return 0;
	
fail:
    usage();
    exit(1);
}


