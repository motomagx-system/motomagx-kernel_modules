/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2006 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipclocsrv.c
*   FUNCTION NAME(S): main
*                     usage
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "ipcsocket.h"

#define MAX 50

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: usage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void usage()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    printf("Usage: ipclocsrv [-n node_id] [-s service_id]\n");
}

extern char *optarg;

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 13Nov2006  Motorola Inc.         Enlarged the buffer size 
*
*****************************************************************************************/
int main(int argc, char **argv)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    struct IPCSERVICE_LOCATION *srv_tab = NULL;
    int table_size = MAX;
    unsigned long len = 0;
    int rc;
    int c;
    unsigned long nodeId = 0;
    unsigned short srvId = 0;
    static char optstr[] = "n:s:h";

    while ((c = getopt(argc, argv, optstr)) != -1)
        switch (c)
        {
            case 'n':
                nodeId = atoi(optarg);
                break;
            case 's':
                srvId = atoi(optarg);
                break;
            case 'h':
                usage();
                return 0;
            case '?':
                printf("Unknown params: %s\n", optarg);
                usage();
                break;
        }

    rc = ipcinit(0, 0);
    if (rc < 0) {
        printf("Failed to connect to IPC stack!\n");
        return -1;
    }

    while (1)
    {
        struct IPCSERVICE_LOCATION* new_ptr = realloc(srv_tab, sizeof(struct IPCSERVICE_LOCATION) * table_size);
        if (new_ptr == NULL)
        {
            fprintf(stderr, "Unable to allocate enough space for service table\n");
            break;
        }
        srv_tab = new_ptr;
        rc = ipclocateservice(nodeId,
                srvId,
                srv_tab,
                &len,
                table_size);

        if (rc < 0) 
        {
            /* the table size is too small */
            if (ipcgetlasterror() == IPCESERVICETABLE)
            {
                table_size *= 2;
            }
            else
            {
                fprintf(stderr, "Failed to locate service\n");
                break;
            }
        }
        else
        {
            printf("+--------------+-----------------------+\n");
            printf("|   Node ID    |        Service ID     |\n");

            for (rc = 0; rc < len; rc++) {
                printf("|--------------|-----------------------|\n");
                printf("|    %-2ld        |    %-5d (0x%.4x %c%c)  |\n", 
                        srv_tab[rc].ipc_node, 
                        srv_tab[rc].ipc_service,
                        srv_tab[rc].ipc_service,
                        (srv_tab[rc].ipc_service & IPC_SRVID_P_BIT) ? 'P':' ',
                        (srv_tab[rc].ipc_service & IPC_SRVID_M_BIT) ? 'M':' ');
            }
            printf("+--------------+--------------------+\n");
            break;
        }
    }

    if (srv_tab != NULL)
    {
        free(srv_tab);
    }
    ipcclose();

    return 0;

}
