/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005, 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : uart_daemon.c
*   FUNCTION NAME(S): OpenDev
*                     device_monitor
*                     main
*                     set_parity
*                     set_speed
*                     uart_daemon
*                     uart_daemon
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation 
* 25Apr2007  Motorola Inc.         Clean up KW warning
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <sys/types.h>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <termios.h>
#define __USE_GNU
#include <string.h>
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#define BUFFER_SIZE 0xF000

static char uart_buffer[BUFFER_SIZE];
static char dev_buffer[BUFFER_SIZE]; 

static int speed_arr[] = { B38400, B19200, B9600, B4800, B2400, B1200, B300,
  B38400, B19200, B9600, B4800, B2400, B1200, B300,
};
static int name_arr[] = { 38400, 19200, 9600, 4800, 2400, 1200, 300, 38400,
  19200, 9600, 4800, 2400, 1200, 300,
};

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: set_speed
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static void set_speed (int fd, int speed)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
  int i;
  int status;
  struct termios Opt;
  tcgetattr (fd, &Opt);
  for (i = 0; i < sizeof (speed_arr) / sizeof (int); i++)
    {
      if (speed == name_arr[i])
	{
	  tcflush (fd, TCIOFLUSH);
	  cfsetispeed (&Opt, speed_arr[i]);
	  cfsetospeed (&Opt, speed_arr[i]);
	  status = tcsetattr (fd, TCSANOW, &Opt);
	  if (status != 0)
	    {
	      perror ("tcsetattr fd1");
	      return;
	    }
	  tcflush (fd, TCIOFLUSH);
	}
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: set_parity
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to set attributes of UART.
*   PARAMETER(S):
*       fd: This is the file description handle of UART
*       databits: This is the data bit attribute of UART
*       stopbits: This is the stop bit arrtibute of UART
*       parity: This is the parity attribute of UART
*   RETURN: FALSE - Failed to set UART attributes
*           TRUE  - Set attribute successfully
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int set_parity (int fd, int databits, int stopbits, int parity)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
  struct termios options;
  if (tcgetattr (fd, &options) != 0)
    {
      perror ("SetupSerial 1");
      return (FALSE);
    }

  options.c_cflag &= ~CSIZE;
  switch (databits)
    {
    case 7:
      options.c_cflag |= CS7;
      break;
    case 8:
      options.c_cflag |= CS8;
      break;
    default:
      fprintf (stderr, "Unsupported data size\n");
      return (FALSE);
    }
  switch (parity)
    {
    case 'n':
    case 'N':
      options.c_cflag &= ~PARENB;	/* Clear parity enable */
      options.c_iflag &= ~INPCK;	/* Enable parity checking */
      break;
    case 'o':
    case 'O':
      options.c_cflag |= (PARODD | PARENB);
      options.c_iflag |= INPCK;	/* Disnable parity checking */
      break;
    case 'e':
    case 'E':
      options.c_cflag |= PARENB;	/* Enable parity */
      options.c_cflag &= ~PARODD;
      options.c_iflag |= INPCK;	/* Disnable parity checking */
      break;
    case 'S':
    case 's':			/*as no parity */
      options.c_cflag &= ~PARENB;
      options.c_cflag &= ~CSTOPB;
      break;
    default:
      fprintf (stderr, "Unsupported parity\n");
      return (FALSE);
    }
  switch (stopbits)
    {
    case 1:
      options.c_cflag &= ~CSTOPB;
      break;
    case 2:
      options.c_cflag |= CSTOPB;
      break;
    default:
      fprintf (stderr, "Unsupported stop bits\n");
      return (FALSE);
    }
/* Set input parity option */
  if ((parity != 'n') && (parity != 'N'))
    options.c_iflag |= INPCK;

  tcflush (fd, TCIFLUSH);
  options.c_cc[VTIME] = 150;	
  options.c_cc[VMIN] = 0;

  /* IMPORTANT:
   * Set UART as raw input & raw output mode, AND ignore some control chars
   */
  options.c_lflag  &= ~(ICANON | ECHO | ECHOE | ISIG | IEXTEN);  
  options.c_oflag  &= ~OPOST;   
  options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | ISTRIP | IXON | INLCR | IGNCR);

  if (tcsetattr (fd, TCSANOW, &options) != 0)
    {
      perror ("SetupSerial 3");
      return (FALSE);
    }
  return (TRUE);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: OpenDev
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to open an UART device.
*   PARAMETER(S):
*       Dev: This is the name of UART, for example "/dev/ttyS0"
*   RETURN: non-negative - The file description handle of UART
*           -1 - Open failed.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int OpenDev (char *Dev)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
  int fd = open (Dev, O_RDWR);	//| O_NOCTTY | O_NDELAY
  if (-1 == fd)
    {
      perror ("Can't Open Serial Port");
      return -1;
    }
  else
    return fd;
}



#ifdef USE_SELECT /* The select operations is quite slow */
#define buffer uart_buffer

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: uart_daemon
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to monitor data from UART or IPC extended device using
*   select mode.
*   PARAMETER(S):
*       uartfd: This is the file description handle of UART
*       devname: This is the name of IPC extended device.
*   RETURN: int
*           0 - Success.
*          -1 - Failed. 
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int uart_daemon(int uartfd,
        const char* devname)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    fd_set fds;
/*--------------------------------------- CODE -----------------------------------------*/
    /* open the device */
    int devfd = open(devname, O_RDWR);
    if (devfd < 0)
    {
        perror("Open device failed");
        return errno;
    }

    for (;;)
    {
        FD_ZERO(&fds);
        FD_SET(uartfd, &fds);
        FD_SET(devfd, &fds);

        int ret = select(devfd + 1,
                &fds,
                NULL, NULL, NULL);
        if (ret < 0)
        {
            perror("Select failed");
            close(devfd);
            return errno;
        }
        /* If we got data from server */
        if (FD_ISSET(uartfd, &fds))
        {
            if ((ret = read(uartfd, uart_buffer, BUFFER_SIZE)) >= 0)
            {
                printf("got %d bytes from uart\n", ret);
                if ((ret = write(devfd, uart_buffer, ret)) < 0)
                {
                    perror("Write to device failed");
                }
            }
            else
            {
                perror("recvfrom failed");
                close(devfd);
                return errno;
            }
        }
        if (FD_ISSET(devfd, &fds))
        {
            if ((ret = read(devfd, dev_buffer, BUFFER_SIZE)) >= 0)
            {
                printf("got %d bytes from device\n", ret);
                if (write(uartfd, dev_buffer, ret) < 0)
                {
                    /* failed? maybe the data is not ready. Reading from the
                     * device is not blocked.
                     */
                    perror("send data to uart failed");
                }
                if (ret == 0)
                {
                    /* it's time to exit */
                    printf("Task finished\n");
                    close(devfd);
                    return 0;
                }
            }
            else
            {
                perror("read from device failed");
                close(devfd);
                return errno;
            }
        } /* if device has data */
    } /* for */
    return 0;
}

#else
struct environment_t
{
    int uartfd;
    int devfd;
};

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: device_monitor
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to monitor data from IPC extended device
*   
*   PARAMETER(S):
*       p: This is the params used to monitor IPC extended device.
*   RETURN: void *
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void* device_monitor(void* p)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int ret = 0;
    register struct environment_t* env = (struct environment_t*)p;

    memset(dev_buffer, 0, BUFFER_SIZE);
    while ((ret = read(env->devfd, dev_buffer, BUFFER_SIZE)) >= 0)
    {
        printf("---->device_monitor: got: %d from pseu-dev\n", ret);
        if (write(env->uartfd, dev_buffer, ret) < 0)
        {
            perror("send data to uart failed");
        }
        if (ret == 0)
        {
            printf("Task finished\n");
            close(env->devfd);
            return 0;
        }
    }
    perror("read from device failed");
    close(env->devfd);
    return (void*)errno;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: uart_daemon
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from UART and then redirect it to the IPC device.
*   PARAMETER(S):
*       uartfd: This is the file description handle of UART
*       devname: This is the name of IPC extended device.
*   RETURN: int
*           0 - Success.
*          -1 - Failed.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation
* 19Aug2005  Motorola Inc.         Increase priority of UDP thread to  make data transmission more  efficient
*
*****************************************************************************************/
static int uart_daemon(int uartfd, const char* devname)
{
    struct environment_t env;
	pthread_attr_t attr;
    pthread_t thread_id;
    int ret;

    /* open the device */
    env.devfd = open(devname, O_RDWR);
    if (env.devfd < 0)
    {
        perror("Open device failed");
        return errno;
    }
    /* assign the environment */
    env.uartfd = uartfd;
    /* create the device monitor thread */
    pthread_attr_init(&attr);
    pthread_attr_setschedpolicy(&attr, SCHED_RR);
    ret = pthread_create(&thread_id, &attr, &device_monitor, &env);
    if (ret != 0)
    {
        perror("Thread creating failed");
        return -1;
    }
    /* monitor the uart */
    memset(uart_buffer, 0, BUFFER_SIZE);
    while ((ret = read(uartfd, uart_buffer, BUFFER_SIZE)) >= 0)
    {
        printf("---->upd_daemon: got %d from uart\n", ret);
        if ((ret = write(env.devfd, uart_buffer, ret)) < 0)
        {
            perror("Write to device failed");
        }
        memset(uart_buffer, 0, BUFFER_SIZE);
    }
    perror("recvfrom failed");
    close(env.devfd);

    return errno;
}
#endif /* USE_SELECT */

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: main
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Main
*   PARAMETER(S):
*       devname: specify the pseudo device name
*       rate: specify the data rate of UART
*       
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation
* 30Aug2005  Motorola Inc.         Initialed some values 
*
*****************************************************************************************/
int main(int argc, char** argv)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int uartfd;
    int i;
    unsigned long br = 115200;

    char *devname = NULL;
    char *rate = NULL;
    
    if (argc != 5)
    {
        fprintf(stderr, "Usage: %s -d devname -s rate\n\n"
         "\t-d devname           --  specify the pseudo device name\n"
         "\t-s rate              --  baud rate\n",
                argv[0]);
        return 1;
    }
    /* parse command line options */
    /*
     * FIXME: it's not very strict, our program is just a demo, isn't it?
     */
    for (i=1; i < argc; i++)
    {
        if (argv[i][0] == '-')
        {
            if (argv[i][1] == 'd')
            {
                devname = argv[++i];
            }
            else if (argv[i][1] == 's')
            {
                rate = argv[++i];
                if (rate == NULL)
                {
                    return 1;
                }
            }
        }
    }
    printf("device name: %s\n"
           "uart rate: %s\n",
           devname, rate);

    if (NULL == devname)
    {
        fprintf(stderr, "ERROR: devname is null\n");
        return 1;
    }    

    /* open /dev/ttyS0 to communicate with outside */
    uartfd = OpenDev("/dev/ttyS0");
    if (NULL != rate)
    {
        br = atol(rate);
    }
    set_speed(uartfd, br);

    if (set_parity(uartfd, 8, 1, 'N') == FALSE)
    {
        printf("Set Parity Error\n");
        exit(0);
    }

    /*
    while (1)
    {
        int i;
        char buf[256];
        memset(buf, 0, 256);
        i = read(uartfd, buf, 256);
        printf("<<< %s <<< %d Bytes\n", buf, i);
    }*/
    return uart_daemon(uartfd, devname);
}

