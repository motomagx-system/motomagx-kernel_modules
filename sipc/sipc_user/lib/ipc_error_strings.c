/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_error_strings.c
*   FUNCTION NAME(S): 
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 29Jan2007  Motorola Inc.         Header files change for GPL
*****************************************************************************************/

#include "ipcsocket.h"

/*------------------------------------- GLOBAL DATA ------------------------------------*/

const ipcErrorItem_t s_errcode_table[] =
{
    {IPCEATTACH        , "IPCEATTACH        : Could not attach to specified shared memory segment"},
    {IPCENOTREADY      , "IPCENOTREADY      : Could not initialize communication with IPC stack"},
    {IPCEDETACH        , "IPCEDETACH        : Could not detach to specified shared memory segment"},
    {IPCENOTINIT       , "IPCENOTINIT       : Need to call ipcinit first"},
    {IPCEBUSY          , "IPCEBUSY          : Stack is unable to take requests at this time"},
    {IPCEAFNOSUPPORT   , "IPCEAFNOSUPPORT   : Specified address family is not supported"},
    {IPCENOBUFS        , "IPCENOBUFS        : No buffer space is available"},
    {IPCEPROTONOSUPPORT, "IPCEPROTONOSUPPORT: Specified protocol is not supported"},
    {IPCEDSM           , "IPCEDSM           : Packet discarded because destination in deep sleep"},
    {IPCEAUTH          , "IPCEAUTH          : Could not authenticate"},
    {IPCEFAULT         , "IPCEFAULT         : Invalid pointer"},
    {IPCEINVAL         , "IPCEINVAL         : Invalid parameter"},
    {IPCENOTSOCK       , "IPCENOTSOCK       : Descriptor is not socket"},
    {IPCEINTR          , "IPCEINTR          : Socket was closed during blocking call"},
    {IPCECHAN          , "IPCECHAN          : Invalid channel descriptor"},
    {IPCENODE          , "IPCENODE          : Invalid Node ID"},
    {IPCEQOS           , "IPCEQOS           : Unable to grant requested Quality of Service"},
    {IPCESERVICE       , "IPCESERVICE       : Invalid Service ID"},
    {IPCEWMARK         , "IPCEWMARK         : Invalid water mark value"},
    {IPCESERVICETABLE  , "IPCESERVICETABLE  : Buffer is small to hold all service table entries"},
    {IPCEWOULDBLOCK    , "IPCEWOULDBLOCK    : Operation would block on non-blocking call"},
    {IPCEMSGSIZE       , "IPCEMSGSIZE       : IPC message is too big to send/receive"},
    {IPCEADDRNOTVAL    , "IPCEADDRNOTVAL    : Invalid address structure"},
    {IPCEADDRINUSE     , "IPCEADDRINUSE     : Address is already in use"},
    {IPCENETUNREACH    , "IPCENETUNREACH    : Network cannot be reached from this node at this time"},
    {IPCENOSHMEM       , "IPCENOSHMEM       : Out of shared memory buffers from specified pool"},
    {IPCEBADBUF        , "IPCEBADBUF        : Invalid buffer passed to ipczfree"},      
    {IPCENOTATTACH     , "IPCENOTATTACH     : Not attached to specified shared memory pool"},
    {IPCENOTIMPLEMENTED, "IPCENOTIMPLEMENTED: Function has not yet been implemented"},
    {IPCESHUTDOWN      , "IPCESHUTDOWN      : Socket has been shut down"},
    {IPCESOCKFULL      , "IPCESOCKFULL      : Destination socket queue is full"},
    {IPCECONNREFUSED   , "IPCECONNREFUSED   : No one listening on the remote address."},
    {IPCEISCONN        , "IPCEISCONN        : The socket is already connected."},
    {IPCETIMEDOUT      , "IPCETIMEDOUT      : Timeout while attempting connection."},
    {IPCESOCKNOSUPPORT , "IPCESOCKNOSUPPORT : Socket specified is not a connection oriented socket."},
    {IPCENOTCONN       , "IPCENOTCONN       : Socket is not connected prior to this call."},
    {IPCEFLAG          , "IPCEFLAG           : Invalid value passed in the flag field."},
    {IPC_LOWEST_ERROR_CODE, NULL}
};

