/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcsocket.c
*   FUNCTION NAME(S): ipcaccept
*                     ipcbind
*                     ipcchan
*                     ipcclose
*                     ipcclosesocket
*                     ipcconnect
*                     ipcgeterrortext
*                     ipcgetlasterror
*                     ipcgetmemtype
*                     ipcgetpeername
*                     ipcgetsockname
*                     ipcgetstatus
*                     ipcgetversion
*                     ipcinit
*                     ipclisten
*                     ipclocateservice
*                     ipcnotify
*                     ipcpsnotify
*                     ipcrecv
*                     ipcrecvfrom
*                     ipcselect
*                     ipcsend
*                     ipcsendto
*                     ipcsetsockqlen
*                     ipcsetwatermark
*                     ipcshutdown
*                     ipcsocket
*                     ipczalloc
*                     ipczcalloc
*                     ipczfree
*                     ipczrecv
*                     ipczrecvfrom
*                     ipczsend
*                     ipczsendto
*                     is_shared_memory
*                     set_ipc_errorcode
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 19Jul2005  Motorola Inc.         Mapping physical shared memory  start address and virtual
*                                   shared memory address to a fixed given one.
* 26Jul2005  Motorola Inc.         Added flag to indicate initialization
* 11Aug2005  Motorola Inc.         Added size verification to ipczalloc
* 19Aug2005  Motorola Inc.         Channel conversion in socket lib not  in kernel space.
* 09Sep2005  Motorola Inc.         Removed printf's and redundant lists
* 01Mar2006  Motorola Inc.         Remove deprecated SIPC definitions
* 20Jun2006  Motorola Inc.         Add the permission controlfor trusted vs. non-trusted 
*                                  Apps for trusted vs. non-trusted Apps
* 12Jul2006  Motorola Inc.         Separated map and unmap functions
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 10May2007  Motorola Inc.         Change error code of wrong msg size in ipczsend
* 05Jul2007  Motorola Inc.         Translated error code for function ipcselect
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <linux/stddef.h>
#include <sys/socket.h>
#include <linux/ipc_ioctl.h>

#include "ipcsocket.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
extern const ipcErrorItem_t s_errcode_table[];
/*------------------------------------ ENUMERATIONS ------------------------------------*/
enum
{
    IPC_NOT_INIT=0,
    IPC_SOCK_INIT,
    IPC_MEM_INIT
};

/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/

/*0: not initialized, can call common socket actions;
1: initialized, but can not access ipc mem;
2: full permission initialized
*/
static int initialized_level = IPC_NOT_INIT;

/*------------------------------------- GLOBAL DATA ------------------------------------*/

/* global ipc open description */
static int ipcfd = -1;
static int ipcmemfd = -1;

static char *virtual_shm_base = NULL;
static char *physical_shm_base = NULL;

static ipcCmd_t shmInfoCmd;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: is_shared_memory
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
*
*****************************************************************************************/
static int is_shared_memory(const char* ptr)
{
    if ((ptr > virtual_shm_base) && 
            (ptr < virtual_shm_base + shmInfoCmd.u.getshminfo.virt_shm_size ))
        return 1;
    if ((ptr > physical_shm_base) &&
            (ptr < physical_shm_base + shmInfoCmd.u.getshminfo.phy_shm_size))
        return 1;
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: set_ipc_errorcode
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
* 
*   This function sets ipc error code according to unix error number
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jan2007  Motorola Inc.         Initial creation 
* 05Jul2007  Motorola Inc.         Translated error code EINTR
*
*****************************************************************************************/
static void set_ipc_errorcode(void)
{
    int new_error = errno;
    switch (errno)
    {
        case EBADF:
        case ENOTSOCK:
            new_error = IPCENOTSOCK;
            break;
        case EACCES:
            new_error = IPCEATTACH;
            break;
        case EBUSY:
            new_error = IPCENOTREADY;
            break;
        case ENOMEM:
            new_error = IPCENOBUFS;
            break;
        case EPERM:
            new_error = IPCEAUTH;
            break;
        case EINVAL:
            new_error = IPCEINVAL;
            break;
        case EINTR:
            new_error = IPCEINTR;
            break;
        case ENETUNREACH:
            new_error = IPCENETUNREACH;
            break;
        case EAGAIN:
            new_error = IPCEWOULDBLOCK;
            break;
        case EFAULT:
            new_error = IPCEFAULT;
            break;
        case EPIPE:
            new_error = IPCESHUTDOWN;
            break;
        case ENOSPC:
            new_error = IPCESOCKFULL;
            break;
        case EISCONN:
            new_error = IPCEISCONN;
            break;
        case ECONNREFUSED:
            new_error = IPCECONNREFUSED;
            break;
        case ETIMEDOUT:
            new_error = IPCETIMEDOUT;
            break;
        case EAFNOSUPPORT:
            new_error = IPCEAFNOSUPPORT;
            break;
        case ENOTCONN:
            new_error = IPCENOTCONN;
            break;
        case EPROTONOSUPPORT:
            new_error = IPCEPROTONOSUPPORT;
            break;
        default:
            new_error = IPCEINVAL;
    }
    errno = new_error;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcinit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*
*   This function initializes the connection to the IPC stack.
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.     Initial Creation
* 16Jun2005  Motorola Inc.     Physical Shm & Virtural Shm Integration
* 19Jul2005  Motorola Inc.     Mapping physical shared memory start
*                              address and virtual shared memory start
*                              address to a fixed one.
* 26Jul2005  Motorola Inc.     Added flag to indicate initialization
* 01Jul2005  Motorola Inc.     Changed return value for user manual
* 20Jun2006  Motorola Inc.     Add the permission control
*                              for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcinit(unsigned int flag,
        struct auth* pauth)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_NOT_INIT)
    {
        return 0;
    }

    if (pauth != NULL)
    {
        return IPCEATTACH;
    }
    if (0 != (flag & ~(IPC_PROCSHM | IPC_PHYSSHM)))
    {
        return IPCEATTACH;
    }

    ipcfd = open("/dev/ipc", O_RDWR);
    if (ipcfd < 0)
    {
        perror("Open IPC stack failed");
        return IPCENOTREADY;
    }

    ipcmemfd = open("/dev/ipcmem", O_RDWR);    
    if (ipcmemfd < 0)
    {
        perror("Warning:You have no access to access ipcmem device.");
        initialized_level = IPC_SOCK_INIT;
    }
    else
    {
        memset(&shmInfoCmd, 0, sizeof(ipcCmd_t));

        /* get physical & virtual shared memory info */
        if (ioctl(ipcmemfd, CMD_IPCGETSHMINFO, &shmInfoCmd) < 0)
        {
            goto initfail;
        }

        if (shmInfoCmd.u.getshminfo.virt_shm_size > 0)
        {
            virtual_shm_base = mmap(shmInfoCmd.u.getshminfo.virt_shm_start_addr, 
                    shmInfoCmd.u.getshminfo.virt_shm_size, 
                    PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_FIXED, 
                    ipcmemfd, 
                    0);
            
            if (virtual_shm_base == MAP_FAILED)
            {
                goto initfail;
            }
        }
        physical_shm_base = virtual_shm_base;

        if ((shmInfoCmd.u.getshminfo.phy_shm_start_addr != NULL)
                && (shmInfoCmd.u.getshminfo.phy_shm_size > 0))
        {
            physical_shm_base = mmap(shmInfoCmd.u.getshminfo.phy_shm_start_addr, 
                    shmInfoCmd.u.getshminfo.phy_shm_size, 
                    PROT_READ | PROT_WRITE, 
                    MAP_SHARED | MAP_FIXED, 
                    ipcmemfd, 
                    0);
            if (physical_shm_base == MAP_FAILED)
            {
                goto initfail1;
            }
        }
        initialized_level = IPC_MEM_INIT;
    }

    return 0;

initfail1:
    if (NULL != virtual_shm_base)
    {
        munmap(virtual_shm_base, shmInfoCmd.u.getshminfo.virt_shm_size);
    }

initfail:
    initialized_level = IPC_NOT_INIT;
    close(ipcfd);
    close(ipcmemfd);
    ipcfd = -1;
    ipcmemfd = -1;
    return IPCEATTACH;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcclose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*
*   This function will close the task's connection to the IPC stack. 
*   
*   PARAMETER(S):
*
*   RETURN: On failure - returns a value of IPCSOCKET_ERROR, and a specific error code can
*                        be retrieved by calling Ipcgetlasterror.
*           On success - returns 0.
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.     Initial Creation
* 16Jun2005  Motorola Inc.     Physical Shm & Virtural Shm Integration
* 26Jul2005  Motorola Inc.     Added flag to indicate initialization
* 01Jul2005  Motorola Inc.     Changed return value for user manual
* 20Jun2006  Motorola Inc.     Add the permission control
*                              for trusted vs. non-trusted Apps
*****************************************************************************************/
int ipcclose()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        return IPCENOTINIT;
    }

    if (ipcfd > 0) 
    {
        ret = close(ipcfd);
        ipcfd = -1;
    }

    if (ipcmemfd > 0)
    {
        if (NULL != virtual_shm_base)
        {
            munmap(virtual_shm_base, shmInfoCmd.u.getshminfo.virt_shm_size);
            if (physical_shm_base != virtual_shm_base)
            {
                munmap(physical_shm_base, shmInfoCmd.u.getshminfo.phy_shm_size);
            }
            virtual_shm_base = NULL;
            physical_shm_base = NULL;
        }
        ret = close(ipcmemfd);
        ipcmemfd = -1;
    }

    if (ret < 0)
    {
        return IPCEDETACH;
    }
    else
    {
        initialized_level = IPC_NOT_INIT;
    }

    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetstatus
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function returns the general status of the IPC stack.  
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.     Initial Creation
* 26Jul2005  Motorola Inc.     Added flag to indicate initialization
* 01Jul2005  Motorola Inc.     Changed return value for user manual
* 20Jun2006  Motorola Inc.     Add the permission control
*                              for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcgetstatus()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    cmd.lasterror = IPCENOTINIT;
    if (initialized_level != IPC_NOT_INIT)
    {
        ret = ioctl(ipcfd, CMD_IPCGETSTATUS, &cmd);
    }

    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetversion
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function returns the version of the IPC stack.  
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcgetversion(unsigned long* version)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    cmd.lasterror = IPCENOTINIT;
    if (initialized_level != IPC_NOT_INIT)
    {
        if (version == NULL)
        {
            ipcSetLastError(IPCEFAULT);
            return ret;
        }
        ret = ioctl(ipcfd, CMD_IPCGETVERSION, &cmd);
        *version = cmd.u.getversion.version;
    }
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsocket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates an ipc socket.  
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 01Jul2005  Motorola Inc.      Changed error code for user manual
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
* 22Jan2007  Motorola Inc.      Use syscall socket to create
*
*****************************************************************************************/
int ipcsocket(int af,
        int type,
        int protocol)
{
    int fd = IPC_ERROR;

    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR;
    }

    /* little workaround to satisfy old applications */
    if (af == 8)
    {
        af = AF_IPC;
    }
    if (af != AF_IPC)
    {
        ipcSetLastError(IPCEAFNOSUPPORT);
        return IPC_ERROR;
    }

    if((type & ~(IPC_CL_SOCK | IPC_CO_SOCK | IPC_DSM_DISCARD | IPC_DSM_WAKE_UP)) != 0)
    {
        ipcSetLastError(IPCESOCKNOSUPPORT);
        return IPC_ERROR;
    }
    if (protocol != IPC_PROTO)
    {
        ipcSetLastError(IPCEPROTONOSUPPORT);
        return IPC_ERROR;
    }

    fd = socket(af, type, protocol);

    if (fd < 0)
    {
        set_ipc_errorcode();
    }
    return fd;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcbind
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function associates a local ipc address with a socket. 
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 01Jul2005  Motorola Inc.      Checked param namelen
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcbind(int sock,
        const struct IPCSOCK_ADDR* name,
        int namelen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    if ((name == NULL) || namelen < (int)sizeof(*name))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    cmd.u.bind.addr = name;
    cmd.lasterror = IPCENOTSOCK;


    ret = ioctl((int)sock, CMD_IPCBIND, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetsockname
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function retrieves the local name for a socket. 
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 01Aug2005  Motorola Inc.      Check for input param namelen
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcgetsockname(int sock,
        struct IPCSOCK_ADDR* name,
        int * namelen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    if ((name == NULL) || (namelen == NULL) || (*namelen < (int)sizeof(*name)))
    {
        ipcSetLastError(IPCEFAULT);
        return ret;
    }
    cmd.lasterror = IPCENOTSOCK;
    cmd.u.getsockname.name = name;

    ret = ioctl((int)sock, CMD_IPCGETSOCKNAME, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    *namelen = sizeof(*name);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetpeername
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function retrieves the local name for a socket. 
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation                
* 26Jul2005  Motorola Inc.      Added flag to indicate initializ
* 01Aug2005  Motorola Inc.      Check for input param namelen   
* 20Jun2006  Motorola Inc.      Add the permission control      
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcgetpeername(int sock,
        struct IPCSOCK_ADDR* name,
        int * namelen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    if ((name == NULL) || (namelen == NULL) || (*namelen < (int)sizeof(*name)))
    {
        ipcSetLastError(IPCEFAULT);
        return ret;
    }
    cmd.lasterror = IPCENOTSOCK;
    cmd.u.getpeername.name = name;

    ret = ioctl((int)sock, CMD_IPCGETPEERNAME, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    *namelen = sizeof(*name);
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcshutdown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function disables sends or receives on a socket.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcshutdown(int sock,
        int how)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    cmd.u.shutdown.how = how;
    cmd.lasterror = IPCENOTSOCK;

    ret = ioctl((int)sock, CMD_IPCSHUTDOWN, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsetsockqlen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sets the maximum number of message that can be queued to an existing ipc 
*       socket.  In the case of CO sockets the queue length is used to define the flow control window.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcsetsockqlen(int sock,
        int queueMaxLen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    cmd.u.setsockqlen.length = queueMaxLen;
    cmd.lasterror = IPCENOTSOCK;

    ret = ioctl((int)sock, CMD_IPCSETSOCKQLEN, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcclosesocket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function closes an existing ipc socket.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.     Initial Creation
* 20Jun2006  Motorola Inc.     Add the permission control
*                              for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcclosesocket(int sock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    ret = close((int)sock);
    if (ret < 0)
    {
        ipcSetLastError(IPCENOTSOCK);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcchan
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
*
*****************************************************************************************/
int ipcchan(int sock, int qos, int flag)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    cmd.lasterror = IPCENOTSOCK;
    cmd.u.chan.qos = qos;
    cmd.u.chan.flag = flag;

    ret = ioctl((int)sock, CMD_IPCCHAN, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcnotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Register a notification
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Oct2005  Motorola Inc.         Initial Creation
* 01Apr2006  Motorola Inc.         Checked message max len in ipcnotify()
* 20Jun2006  Motorola Inc.         Add the permission control for trusted vs. non-trusted Apps
* 30Oct2006  Motorola Inc.         modify wrong returned errorcode   
*
*****************************************************************************************/
int ipcnotify(int sock, 
        const struct IPCSOCK_ADDR* name, 
        int namelen, 
        const char * msg, 
        int msglen, 
        int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    if (name == NULL)
    {
        ipcSetLastError(IPCEADDRNOTVAL);
        return IPC_ERROR;
    }
    if (namelen < sizeof(*name) || (msg == NULL)) 
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    if ((msglen < 0) || (msglen > IPC_NOTIFICATION_MAX_SIZE))
    {
        ipcSetLastError(IPCEINVAL);
        return IPC_ERROR;
    }

    cmd.lasterror = IPCENOTSOCK;
    cmd.u.notify.name = name;
    memcpy(cmd.u.notify.msg, msg, msglen);
    cmd.u.notify.msglen = msglen;
    cmd.u.notify.flags = flags;

    ret = ioctl((int)sock, CMD_IPCNOTIFY, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcpsnotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
*
*****************************************************************************************/
int ipcpsnotify(void* q, int qflag,
        const struct IPCSOCK_ADDR *name, int namelen,
        const char* msg, int msglen, int flags)
{
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    ipcSetLastError(IPCENOTIMPLEMENTED);
    return IPC_ERROR;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsetwatermark
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
int ipcsetwatermark(int sock,
        unsigned long ipc_lowwmark,
        unsigned long ipc_hiwmark)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    cmd.lasterror = IPCENOTSOCK;
    cmd.u.setwatermark.lowwmark = ipc_lowwmark;
    cmd.u.setwatermark.highwmark = ipc_hiwmark;

    ret = ioctl((int)sock, CMD_IPCSETWATERMARK, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetmemtype
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.        Initial Creation
* 25Apr2007  Motorola Inc.        Clean up KW warning
*
*****************************************************************************************/
int ipcgetmemtype(const struct IPCSOCK_ADDR* ipc_address,
        unsigned long *ipc_memtype)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    
    if ((ipc_address == NULL) || (ipc_memtype == NULL))
    {
        ipcSetLastError(IPCEADDRNOTVAL);
        return ret;
    }
    
    cmd.lasterror = IPCENOTINIT;
    cmd.u.getmemtype.dest_node = ipc_address->ipc_node;
    cmd.u.getmemtype.memtype = ipc_memtype;

    if (initialized_level == IPC_MEM_INIT)
    {
        ret = ioctl(ipcmemfd, CMD_IPCGETMEMTYPE, &cmd);
    }

    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipclocateservice
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will locate services available in the IPC network.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.       Initial Creation
* 26Jul2005  Motorola Inc.       Added flag to indicate initialization
* 20Jun2006  Motorola Inc.       Add the permission control
*                                for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipclocateservice(unsigned long ipc_node,
        unsigned short ipc_service,
        struct IPCSERVICE_LOCATION* ipc_servicetable,
        unsigned long* ipc_sizeofservicetable,
        unsigned long ipc_maxsizeofservicetable)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    if ((NULL == ipc_sizeofservicetable) || (NULL == ipc_servicetable))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }
    cmd.u.locateservice.nodeId = ipc_node;
    cmd.u.locateservice.srvId = ipc_service;
    cmd.u.locateservice.serviceTable = ipc_servicetable;
    cmd.u.locateservice.maxsizeofservicetable = ipc_maxsizeofservicetable;
    cmd.lasterror = IPCENOTINIT;

    ret = ioctl(ipcfd, CMD_IPCLOCATESERVICE, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    *ipc_sizeofservicetable = cmd.u.locateservice.sizeofservicetable;
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczsendto
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination without copying the data buffer.  
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.      Initial Creation
* 16Jun2005  Motorola Inc.      Physical Shm & Virtural Shm Integration
* 26Jul2005  Motorola Inc.      Added flag to indicate initialization
* 01Jul2005  Motorola Inc.      Added stricter param checking
* 19Aug2005  Motorola Inc.      Channel convertion
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.      Refactored ipc folder structure
*                               for open source
*
*****************************************************************************************/
int ipczsendto(int sock,
        const char* ptr,
        int leng,
        int flags,
        const struct IPCSOCK_ADDR* to,
        int tolen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int ret = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR;
    }

    if ((ptr == NULL) || !is_shared_memory(ptr) 
            || (to == NULL) || (tolen < (int)sizeof(*to)))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    
    if (leng < 0)
    {
        ipcSetLastError(IPCEMSGSIZE);
        return IPC_ERROR;
    }
    /* get channel from channel handle */
    cmd.u.zsendto.zbuffer = ptr;
    cmd.u.zsendto.flag = flags;
    cmd.u.zsendto.dataLen = leng;
    cmd.u.zsendto.addr = to;
    cmd.lasterror = IPCENOTSOCK;

    ret = ioctl(sock, CMD_IPCZSENDTO, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }

    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczrecvfrom
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives data using zero copy.  
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.       Initial Creation
* 16Jun2005  Motorola Inc.       Physical Shm & Virtural Shm Integration
* 26Jul2005  Motorola Inc.       Added flag to indicate initialization
* 31May2006  Motorola Inc.       Added debug messages for buffer check
* 20Jun2006  Motorola Inc.       Add the permission control
*                                for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.       Refactored ipc folder structure
*                                for open source
*
*****************************************************************************************/
int ipczrecvfrom(int sock,
        char ** zbuf,
        int flags,
        struct IPCSOCK_ADDR* from,
        int * fromlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int len;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR;
    }

    if ((zbuf == NULL) 
            || (from == NULL) 
            || (fromlen == NULL) 
            || (*fromlen < (int)sizeof(*from)))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    cmd.u.zrecvfrom.flag = flags;
    cmd.u.zrecvfrom.addr = from;
    cmd.lasterror = IPCENOTSOCK;

    if ((len = ioctl(sock, CMD_IPCZRECVFROM, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
        return IPC_ERROR;
    }

    *zbuf = cmd.u.zrecvfrom.zbuffer;

    if (fromlen != NULL)
    {
        *fromlen = sizeof(*from);
    }
    return len;
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcselect
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   This function determines the status of one or more ipc sockets, waiting if necessary,
*   to perform synchronous I/O.
*
*   PARAMETER(S):
*
*   RETURN:
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation                 
* 26Jul2005  Motorola Inc.         Added flag to indicate initializa
* 01Jul2005  Motorola Inc.         Changed error codes for user manu
* 20Jun2006  Motorola Inc.         Add the permission control       
*                                  for trusted vs. non-trusted Apps
* 10Nov2006  Motorola Inc.         fix writable implementation issue
* 05Jul2007  Motorola Inc.         Translated error code for function ipcselect
*
*****************************************************************************************/

int ipcselect(int ipcnfds,              /* IGNORED */
        ipcfd_set * readfds,
        ipcfd_set * writefds,     /* NOT IMPLEMENTED */
        ipcfd_set * excepfds,     /* NOT IMPLEMENTED */
        struct ipctimeval * timeout)
{
/*----------------------------- LOCAL VARIABLES ------------------------------*/
    int ret = 0;
/*---------------------------------- CODE ------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    ret = select(ipcnfds, readfds, writefds, excepfds, (struct timeval *)timeout);
    if (ret < 0)
    {
        set_ipc_errorcode();
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczalloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function allocate IPC related zero copy buffers used with the 
*         ipczsendto and ipczrecvfrom api calls. 
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.        Initial Creation
* 16Jun2005  Motorola Inc.        Physical Shm & Virtural Shm Integration
* 15Jul2005  Motorola Inc.        check value of buf_leng
* 26Jul2005  Motorola Inc.        Added flag to indicate initialization
* 18Aug2005  Motorola Inc.        enlarge zero-length to 1 byte
* 16Feb2005  Motorola Inc.        Bug fix. ipczalloc() will never
*                                 return if it tries to allocate a block of
*                                 physical shared memory with size more than max
*                                 buffer size.
* 20Jun2006  Motorola Inc.        Add the permission control
*                                 for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.        Refactored ipc folder structure
*                                 for open source
* 22Mar2007  Motorola Inc.        Added default lasterror
*
*****************************************************************************************/
void *ipczalloc(long buf_leng,
        int flag,
        int wait_time)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return NULL;
    }

    if (buf_leng < 0)
    {
        ipcSetLastError(IPCEINVAL);
        return NULL;
    }

    if (buf_leng == 0)
    {
        buf_leng = 1;
    }

    cmd.lasterror = IPCEINVAL; 
    cmd.u.shmalloc.size = buf_leng;

    /* If not allocating physical shared memory, default to virtual shared */
    if (!(flag & IPC_PHYSSHM))
    {
        flag |= IPC_PROCSHM;
    }

    cmd.u.shmalloc.flags = flag;
    rc = ioctl(ipcmemfd, CMD_IPCSHMALLOC, &cmd);
    if (rc < 0)
    {
        ipcSetLastError(cmd.lasterror);
        return NULL;
    }

    return cmd.u.shmalloc.zbuffer;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczfree
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination without copying the data buffer.  
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.       Initial Creation
* 16Jun2005  Motorola Inc.       Physical Shm & Virtural Shm Integration
* 15Jul2005  Motorola Inc.       check ipc_buf_ptr != NULL
* 26Jul2005  Motorola Inc.       Added flag to indicate initialization
* 28Jul2005  Motorola Inc.       Checked pointer validity before
* 20Jun2006  Motorola Inc.       Add the permission control
*                                for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.       Refactored ipc folder structure
*                                for open source
* 22Mar2007  Motorola Inc.       Added default lasterror
*
*****************************************************************************************/
int ipczfree(void *ipc_buf_ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR;
    }

    if (!is_shared_memory((char*)ipc_buf_ptr))
    {
        ipcSetLastError(IPCEBADBUF);
        return IPC_ERROR;
    }

    cmd.lasterror = IPCEBADBUF;
    cmd.u.shmfree.zbuffer = ipc_buf_ptr;
    if (ioctl(ipcmemfd, CMD_IPCSHMFREE, &cmd) < 0)
    {
        ipcSetLastError(cmd.lasterror);
        rc = IPC_ERROR;
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczcalloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocate nelem blocks of shared memory, each has the length sz.
*   The allocated memory is set to 0.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation
* 27Jul2005  Motorola Inc.         Check for minus parameters
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source   
* 22Mar2007  Motorola Inc.         Added default lasterror
*
*****************************************************************************************/
void *ipczcalloc(long nelem,
                 long sz,
                 int flag,
                 int wait_time)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc;
    ipcCmd_t cmd;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return NULL;
    }
    
    if ((nelem < 0l) || (sz < 0l) || (wait_time < 0l))
    {
        ipcSetLastError(IPCEINVAL);
        return NULL;
    }

    cmd.lasterror = IPCEINVAL;
    cmd.u.zcalloc.nelem = nelem;
    cmd.u.zcalloc.size = sz;
    cmd.u.zcalloc.flags = flag;
    
    rc = ioctl(ipcmemfd, CMD_IPCZCALLOC, &cmd);
    if (rc < 0)
    {
        ipcSetLastError(cmd.lasterror);
        return NULL;
    }
    
    return cmd.u.zcalloc.zbuffer;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipclisten
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.        Add the permission control
*                                 for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipclisten(int socket, int backlog)
{
    ipcCmd_t cmd;
    int rc = 0;

    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }

    cmd.lasterror = IPCENOTSOCK;
    cmd.u.listen.backlog = backlog;

    if (ioctl(socket, CMD_IPCLISTEN, &cmd) < 0)
    {
        ipcSetLastError(cmd.lasterror);
        rc = IPC_ERROR;
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcaccept
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function accepts a connection on a socket. 
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
* 22Jan2007  Motorola Inc.      Use syscall accept to accept
* 02Mar2007  Motorola Inc.      merge code change 
*
*****************************************************************************************/
int ipcaccept(int socket, struct IPCSOCK_ADDR *addr, int *addrlen)
{
    int rc = 0;

    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    rc = accept(socket, (struct sockaddr*)addr, addrlen);
    if (rc < 0)
    {
        /* handle direct connect case */
        if (rc == IPC_LOWEST_ERROR_CODE - 1)
        {
            ipcgetpeername(socket, addr, addrlen);
            return socket;
        }
        set_ipc_errorcode();
        rc = IPC_ERROR;
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcconnect
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function establishes a connection to a specified socket. 
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.      Add the permission control
*                               for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcconnect(int socket, struct IPCSOCK_ADDR *addr, int addrlen)
{
    ipcCmd_t cmd;
    int rc = 0;

    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    if (addr == NULL)
    {
        ipcSetLastError(IPCEADDRNOTVAL);
        return IPC_ERROR;
    }
    if (addrlen < sizeof(struct IPCSOCK_ADDR))
    {
        ipcSetLastError(IPCEINVAL);
        return IPC_ERROR;
    }

    cmd.lasterror = IPCENOTSOCK;
    cmd.u.connect.addr = addr;

    if ((rc = ioctl(socket, CMD_IPCCONNECT, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczsend
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.     Add the permission control
*                              for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.     Refactored ipc folder structure
*                              for open source
*
*****************************************************************************************/
int ipczsend(int socket, const char* ptr, int len, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR;
    }

    cmd.lasterror = IPCENOTSOCK;
    if ((ptr == NULL) || !is_shared_memory(ptr))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }
    
    if (len < 0)
    {
        ipcSetLastError(IPCEMSGSIZE);
        return IPC_ERROR;
    }

    cmd.u.zsend.zbuffer = ptr;
    cmd.u.zsend.len = len;
    cmd.u.zsend.flag = flags;

    if ((rc = ioctl(socket, CMD_IPCZSEND, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczrecv
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.     Add the permission control
*                              for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.     Refactored ipc folder structure
*                              for open source
*
*****************************************************************************************/
int ipczrecv(int sock, char** zbuf, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int len;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level != IPC_MEM_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR;
    }

    if (zbuf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    cmd.u.zrecv.flag = flags;
    cmd.lasterror = IPCENOTSOCK;

    if ((len = ioctl(sock, CMD_IPCZRECV, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
        return IPC_ERROR;
    }

    *zbuf = cmd.u.zrecv.zbuffer;

    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsendto
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination.
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.           Initial Creation
* 31Dec2005  Motorola Inc.           Error check
* 18Jan2006  Motorola Inc.           change invalid leng error code to IPCEMSGSIZE
* 20Jun2006  Motorola Inc.           Add the permission control
*                                    for trusted vs. non-trusted Apps
* 08Dec2006  Motorola Inc.           Return directly when buffer is null
*
*****************************************************************************************/
int ipcsendto(int sock,
        const char* buf,
        int leng,
        int flags,
        const struct IPCSOCK_ADDR* to,
        int tolen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int ret = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    if (NULL == to)
    {
        ipcSetLastError(IPCEADDRNOTVAL);
        return IPC_ERROR;
    }

    if (tolen < (int)sizeof(struct IPCSOCK_ADDR))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    if (leng < 0)
    {
        ipcSetLastError(IPCEMSGSIZE);
        return IPC_ERROR;
    }

    if (flags & ~(IPCNONBLOCK | IPCPRIO | IPCROSPRIO | IPCPAYLOADCRC))
    {
        /* An unknown flag was set */
        ipcSetLastError(IPCEINVAL);
        return IPC_ERROR;
    }

    if (NULL == buf)
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    cmd.u.sendto.buffer = buf;
    cmd.u.sendto.flags = flags;
    cmd.u.sendto.datalen = leng;
    cmd.u.sendto.addr = to;
    cmd.lasterror = IPCENOTSOCK;

    ret = ioctl(sock, CMD_IPCSENDTO, &cmd);
    if (ret < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }

    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcrecvfrom
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives ipc data.
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005   Motorola Inc.            Initial Creation
* 20Jun2006  Motorola Inc.            Add the permission control
*                                      for trusted vs. non-trusted Apps
*
*****************************************************************************************/
int ipcrecvfrom(int sock,
        char *buf,
        int len,
        int flags,
        struct IPCSOCK_ADDR* from,
        int * fromlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int length;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    if ((buf == NULL) 
            || (from == NULL) 
            || (fromlen == NULL) 
            || (*fromlen < (int)sizeof(*from)))
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }
    if (len <= 0)
    {
        ipcSetLastError(IPCEINVAL);
        return IPC_ERROR;
    }

    cmd.u.recvfrom.buffer = buf;
    cmd.u.recvfrom.datalen = len;
    cmd.u.recvfrom.flags = flags;
    cmd.u.recvfrom.addr = from;
    cmd.lasterror = IPCENOTSOCK;

    if ((length = ioctl(sock, CMD_IPCRECVFROM, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
        return IPC_ERROR;
    }

    *fromlen = sizeof(*from);
    return length;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsend
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination on a connected socket.  
*        Both CL and  CO sockets may be used as long as connect is called first.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jan2006  Motorola Inc.              change invalid leng error code to IPCEMSGSIZE
* 20Jun2006  Motorola Inc.              Add the permission control
*                                       for trusted vs. non-trusted Apps 
*
*****************************************************************************************/
int ipcsend(int sock,
        const char * buf, 
        int len,
        int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    if (buf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    if (len < 0)
    {
        ipcSetLastError(IPCEMSGSIZE);
        return IPC_ERROR;
    }

    if(flags & ~(IPCNONBLOCK | IPCRELIABLE | IPCPRIO | IPCROSPRIO | IPCPAYLOADCRC))
    {
        ipcSetLastError(IPCEINVAL);
        return IPC_ERROR;
    }

    cmd.u.send.buffer = buf;
    cmd.u.send.datalen = len;
    cmd.u.send.flags = flags;
    cmd.lasterror = IPCENOTSOCK;

    if ((rc = ioctl(sock, CMD_IPCSEND, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcrecv
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives ipc data.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.            Add the permission control
*                                      for trusted vs. non-trusted Apps
* 24Nov2006  Motorola Inc.              Add flag IPCROSCHKMSG        
*
*****************************************************************************************/
int ipcrecv(int sock,
        char * buf,
        int len,
        int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcCmd_t cmd;
    int length;
/*--------------------------------------- CODE -----------------------------------------*/
    if (initialized_level == IPC_NOT_INIT)
    {
        ipcSetLastError(IPCENOTINIT);
        return IPC_ERROR; 
    }
    if (buf == NULL) 
    {
        ipcSetLastError(IPCEFAULT);
        return IPC_ERROR;
    }

    if(len <= 0 || 
            (flags & ~(IPCNONBLOCK | IPCRELIABLE | IPCPRIO | IPCROSPRIO | IPCROSCHKMSG)))
    {
        ipcSetLastError(IPCEINVAL);
        return IPC_ERROR;
    }

    cmd.u.recv.buffer = buf;
    cmd.u.recv.datalen = len;
    cmd.u.recv.flags = flags;
    cmd.lasterror = IPCENOTSOCK;

    if ((length = ioctl(sock, CMD_IPCRECV, &cmd)) < 0)
    {
        ipcSetLastError(cmd.lasterror);
        return IPC_ERROR;
    }
    return length;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetlasterror
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function gets the error status for the last operation that failed.
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
*16Feb2005  Motorola Inc.         Initial Creation
*****************************************************************************************/
int ipcgetlasterror()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return ipcGetLastErrorImp();
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgeterrortext
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function gets the error text given the error code obtained from ipcgetlasterror.
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation        
* 07Mar2006  Motorola Inc.         Changed to linear search
*
*****************************************************************************************/
const char *ipcgeterrortext(int ipc_error)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    const char *errorText = NULL;
    const ipcErrorItem_t* item = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((ipc_error >= IPC_LOWEST_ERROR_CODE) && (ipc_error <= IPC_HIGHEST_ERROR_CODE))
    {
        item = &s_errcode_table[0];
        while (item->text != NULL)
        {
            if (ipc_error == item->number)
            {
                errorText = item->text;
                break;
            }
            item++;
        }
    }

    return errorText;
}


