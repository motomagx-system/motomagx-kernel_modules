/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2006 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_memory_access.c
*   FUNCTION NAME(S): ipcRead16
*                     ipcRead32
*                     ipcRead64
*                     ipcReadDouble
*                     ipcReadFloat
*                     ipcWrite16
*                     ipcWrite32
*                     ipcWrite64
*                     ipcWriteDouble
*                     ipcWriteFloat
*                     memcpyFromBE16
*                     memcpyToBE16
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements all common memory access functions which do not change
*   based on platform endianness.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation 
* 19Jan2006  Motorola Inc.         Split into two separate memcpy functions for unaligned access
* 28Sep2006  Motorola Inc.         Added floating point access functions
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_memory_access.h"
#include "ipc_memory_helper.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRead16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 16-bit value from memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 16-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned short ipcRead16(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned short val;
/*--------------------------------------- CODE -----------------------------------------*/
    if (_ipcu32(addr) & 0x01)
    {
        /* The address is not aligned to a 16 bit boundary so we will handle
         * the read as a series of 1-byte reads, shifted and ORed together */
        val = _ipcu16(  _ipcshift0(ipcRead8(_ipcp8(addr)    )) |
                        _ipcshift1(ipcRead8(_ipcp8(addr) + 1)) );
    }
    else
    {
        val = ipcReadAligned16(addr);
    }

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRead32
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 32-bit value from memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 32-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned long ipcRead32(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long val;
/*--------------------------------------- CODE -----------------------------------------*/
    if (_ipcu32(addr) & 0x03)
    {
        /* The address is not aligned to a 32 bit boundary so we will handle
         * the read as a series of 1-byte reads, shifted and ORed together */
        val = _ipcu32(  _ipcshift0(ipcRead8(_ipcp8(addr)    )) |
                        _ipcshift1(ipcRead8(_ipcp8(addr) + 1)) |
                        _ipcshift2(ipcRead8(_ipcp8(addr) + 2)) |
                        _ipcshift3(ipcRead8(_ipcp8(addr) + 3)) );
    }
    else
    {
        val = ipcReadAligned32(addr);
    }

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRead64
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 64-bit value from memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 64-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned __longlong ipcRead64(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned __longlong val;
/*--------------------------------------- CODE -----------------------------------------*/
    if (_ipcu32(addr) & 0x07)
    {
        /* The address is not aligned to a 64 bit boundary so we will handle
         * the read as a series of 1-byte reads, shifted and ORed together */
        val = _ipcu64(  _ipcllshift0(ipcRead8(_ipcp8(addr)    )) |
                        _ipcllshift1(ipcRead8(_ipcp8(addr) + 1)) |
                        _ipcllshift2(ipcRead8(_ipcp8(addr) + 2)) |
                        _ipcllshift3(ipcRead8(_ipcp8(addr) + 3)) |
                        _ipcllshift4(ipcRead8(_ipcp8(addr) + 4)) |
                        _ipcllshift5(ipcRead8(_ipcp8(addr) + 5)) |
                        _ipcllshift6(ipcRead8(_ipcp8(addr) + 6)) |
                        _ipcllshift7(ipcRead8(_ipcp8(addr) + 7)) );
    }
    else
    {
        val = ipcReadAligned64(addr);
    }

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcReadFloat
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 32-bit floating point value from memory.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The float read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
float ipcReadFloat(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long val;
/*--------------------------------------- CODE -----------------------------------------*/
    val = ipcRead32(addr);

    return *((float *) ((void *)&val) );
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcReadDouble
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 64-bit double precision floating point value from memory.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The double read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
double ipcReadDouble(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned __longlong val;
/*--------------------------------------- CODE -----------------------------------------*/
    val = ipcRead64(addr);

    return *((double *) ((void *)&val) );
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWrite16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 16-bit value to memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 16-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWrite16(void *addr, unsigned short val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (_ipcu32(addr) & 0x01)
    {
        /* The address is not aligned to a 16 bit boundary so we will handle
         * the write as a series of 1-byte writes */
        ipcWrite8(_ipcp8(addr)  , _ipcbyte0(val));
        ipcWrite8(_ipcp8(addr)+1, _ipcbyte1(val));
    }
    else
    {
       ipcWriteAligned16(addr, val);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWrite32
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 32-bit value to memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 32-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWrite32(void *addr, unsigned long val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (_ipcu32(addr) & 0x03)
    {
        /* The address is not aligned to a 32 bit boundary so we will handle
         * the write as a series of 1-byte writes */
        ipcWrite8(_ipcp8(addr)  , _ipcbyte0(val));
        ipcWrite8(_ipcp8(addr)+1, _ipcbyte1(val));
        ipcWrite8(_ipcp8(addr)+2, _ipcbyte2(val));
        ipcWrite8(_ipcp8(addr)+3, _ipcbyte3(val));
    }
    else
    {
        ipcWriteAligned32(addr, val);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWrite64
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 64-bit value to memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 64-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWrite64(void *addr, unsigned __longlong val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (_ipcu32(addr) & 0x07)
    {
        /* The address is not aligned to a 64 bit boundary so we will handle
         * the write as a series of 1-byte writes */
        ipcWrite8(_ipcp8(addr)  , _ipcllbyte0(val));
        ipcWrite8(_ipcp8(addr)+1, _ipcllbyte1(val));
        ipcWrite8(_ipcp8(addr)+2, _ipcllbyte2(val));
        ipcWrite8(_ipcp8(addr)+3, _ipcllbyte3(val));
        ipcWrite8(_ipcp8(addr)+4, _ipcllbyte4(val));
        ipcWrite8(_ipcp8(addr)+5, _ipcllbyte5(val));
        ipcWrite8(_ipcp8(addr)+6, _ipcllbyte6(val));
        ipcWrite8(_ipcp8(addr)+7, _ipcllbyte7(val));
    }
    else
    {
        ipcWriteAligned64(addr, val);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWriteFloat
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 32-bit floating point value to memory.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       floatVal: The float to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWriteFloat(void *addr, float floatVal)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long val = *((unsigned long *) ((void *)&floatVal) );
/*--------------------------------------- CODE -----------------------------------------*/
    ipcWrite32(addr, val);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWriteDouble
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 64-bit double precision floating point value to memory.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       doubleVal: The double to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWriteDouble(void *addr, double doubleVal)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned __longlong val = *((unsigned __longlong *) ((void *)&doubleVal) );
/*--------------------------------------- CODE -----------------------------------------*/
    ipcWrite64(addr, val);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: memcpyToBE16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function converts an array of bytes from Little Endian (or Big Endian)
*   to Big Endian 16.  If both the source and destination addresses are on 16-bit
*   boundaries, this can be accomplished by sipmly swapping every other byte.
*   Otherwise, the byte access macros will be used.  The conversion can be done
*   in place by providing the same address for source and destination, but this is
*   only allowed if the address is 16-bit aligned.
*   
*   NOTE: When copying to a destination address that is not on a 16-bit boundary,
*        memory one byte BEFORE the start address will be modified.  Also, memory
*        one byte AFTER the final destination byte address will be written to if
*        if the final byte ends up on a 16-bit boundary address.
*   
*   PARAMETER(S):
*       dest: address to write array to
*       src: address to read array from
*       count: number of bytes in array to copy
*                  
*   RETURN: returns the value of dest
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
* 19Jan2006  Motorola Inc.         Modified for unaligned access
*
*****************************************************************************************/
void *memcpyToBE16(void *dest, const void *src, unsigned int count)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ( ((_ipcu32(src) & 0x01) == 0) && ((_ipcu32(dest) & 0x01) == 0))
    {
        /* Both the source and destination addresses are 16-bit aligned so we
         * can do the conversion by swapping every other byte */
        unsigned short *src16 = _ipcp16(src);
        unsigned short *dest16 = _ipcp16(dest);
        unsigned short data16;
         
        /* Divide by 2 and round up to get count of 2-byte words */
        count = (count + 1) >> 1;

        while (count--)
        {
            data16 = *src16++;
            *dest16++ = ipcRev16(data16); /* Swap the two bytes in each word */
        }
    }
    else
    {
        /* The addresses are not aligned so we will copy each byte individually
         * using the memory access macro */
        unsigned char *src8 = _ipcp8(src);
        unsigned char *dest8 = _ipcp8(dest);
        unsigned char data8;
        
        while (count--)
        {
            data8 = ipcRead8(src8);
            ipcWrite8((void *)(_ipcu32(dest8) ^ 0x00000001U), data8);
            src8++;
            dest8++;
        }
    }
    
    return dest;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: memcpyFromBE16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function converts an array of bytes from Big Endian 16 to Little Endian
*   (or Big Endian).  If both the source and destination addresses are on 16-bit
*   boundaries, this can be accomplished by sipmly swapping every other byte.
*   Otherwise, the byte access macros will be used.  The conversion can be done
*   in place by providing the same address for source and destination but this is
*   only allowed if this address is 16-bit aligned.
*   
*   PARAMETER(S):
*       dest: address to write array to
*       src: address to read array from
*       count: number of bytes in array to copy
*                  
*   RETURN: returns the value of dest
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
* 19Jan2006  Motorola Inc.         Modified for unaligned access
*
*****************************************************************************************/
void *memcpyFromBE16(void *dest, const void *src, unsigned int count)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ( ((_ipcu32(src) & 0x01) == 0) && ((_ipcu32(dest) & 0x01) == 0))
    {
        /* Both the source and destination addresses are 16-bit aligned so we
         * can do the conversion by swapping every other byte */
        unsigned short *src16 = _ipcp16(src);
        unsigned short *dest16 = _ipcp16(dest);
        unsigned short data16;
         
        /* Divide by 2 and round up to get count of 2-byte words */
        count = (count + 1) >> 1;

        while (count--)
        {
            data16 = *src16++;
            *dest16++ = ipcRev16(data16); /* Swap the two bytes in each word */
        }
    }
    else
    {
        /* The addresses are not aligned so we will copy each byte individually
         * using the memory access macro */
        unsigned char *src8 = _ipcp8(src);
        unsigned char *dest8 = _ipcp8(dest);
        unsigned char data8;
        
        while (count--)
        {
            data8 = ipcRead8((void *)(_ipcu32(src8) ^ 0x00000001U));
            ipcWrite8(dest8, data8);
            src8++;
            dest8++;
        }
    }
    
    return dest;
}
