/*****************************************************************************************
*                Template No. SWF0113   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2006 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU Lesser General Public License as published by the Free
*   Software Foundation; version 2.1 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the Lesser GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_memory_access_le.c
*   FUNCTION NAME(S): ipcRead8
*                     ipcReadAligned16
*                     ipcReadAligned32
*                     ipcReadAligned64
*                     ipcWrite8
*                     ipcWriteAligned16
*                     ipcWriteAligned32
*                     ipcWriteAligned64
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements all memory access functions for little-endian platforms
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation 
* 19Jan2006  Motorola Inc.         Split into two separate memcpy functions for unaligned access
* 28Sep2006  Motorola Inc.         Convert all macros to functions
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_memory_access.h"
#include "ipc_memory_helper.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRead8
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads an 8-bit value from memory.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 8-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned char ipcRead8(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned char val;
/*--------------------------------------- CODE -----------------------------------------*/
    val = *_ipcp8(addr);

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcReadAligned16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 16-bit value from memory in little endian format.  The address
*   passed in MUST be a multpile of 2.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 16-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned short ipcReadAligned16(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned short val;
/*--------------------------------------- CODE -----------------------------------------*/
    val = *_ipcp16(addr);

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcReadAligned32
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 32-bit value from memory in little endian format.  The address
*   passed in MUST be a multpile of 4.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 32-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned long ipcReadAligned32(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long val;
/*--------------------------------------- CODE -----------------------------------------*/
    val = *_ipcp32(addr);

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcReadAligned64
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 64-bit value from memory in little endian format.  The address
*   passed in MUST be a multpile of 8.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 64-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned __longlong ipcReadAligned64(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned __longlong val;
/*--------------------------------------- CODE -----------------------------------------*/
    val = *_ipcp64(addr);

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWrite8
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes an 8-bit value to memory.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 8-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWrite8(void *addr, unsigned char val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    *_ipcp8(addr) = val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWriteAligned16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 16-bit value to memory in little endian format.  The address
*   passed in MUST be a multpile of 2.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 16-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWriteAligned16(void *addr, unsigned short val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    *_ipcp16(addr) = val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWriteAligned32
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 32-bit value to memory in little endian format.  The address
*   passed in MUST be a multpile of 4.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 32-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWriteAligned32(void *addr, unsigned long val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    *_ipcp32(addr) = val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWriteAligned64
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 64-bit value to memory in little endian format.  The address
*   passed in MUST be a multpile of 8.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 64-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcWriteAligned64(void *addr, unsigned __longlong val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    *_ipcp64(addr) = val;
}
