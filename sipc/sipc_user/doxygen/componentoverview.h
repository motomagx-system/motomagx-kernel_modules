/*  Component Programming Interface Overview Header Template for Doxygen
    Version 1.2
*/

/** 
\mainpage Smart IPC API 
 
\if MOTOROLA_CONFIDENTIAL_PROPRIETARY 

    \author  John Q. Public (johnqpublic@email.mot.com)
    
\endif

\section References 1.0 References
     /vob/iden_doc/cpi/Integral_Processes/documentation_standard/Documentation_Format_Standard_and_Guidelines.doc
     /vob/iden_doc/cpi/Integral_Processes/documentation_standard/Standard_Terminology.doc
     /vob/iden_doc/cpi/Process_Management/iDEN_Standard_Software_Process.doc
     /vob/iden_doc/phoenix/ipc/SmartIPCUserGuide.doc

\section Overview 2.0 Overview
   The Smart IPC provides the support needed for different processors to communicate. In general, a dual-core architecture represents a division of labor between an Application and a BaseBand processor. The BaseBand processes real-time and over-the-air data while the Application packages the data and presents it to the user. The two depends on one another.For services and do so through the exchange of IPC messages. Since the type of services between them varies, the length, speed and point-to-point exchanges of data will also vary. The goal of IPC is not to define what these services are nor impose constraints on the design of the AP and BP. Instead, it must allow any AP or BP that adopts the IPC as its inter-processor communication stack to co-exist together and operates as if the two were actually running on the same core sharing common OS and memory. 

   Following are Key Architecture Issues facilitated by SmartIPC

    * Consistent/standards based communication API (sockets API)\n
    * Services can be easily relocated between cores\n
    * Services can be advertised dynamically\n
    * Applications can target a service dynamically\n
    * DSP acceleration/Multimedia services are enabled across cores\n
    * QoS is available\n
    * Service discovery is provided\n
    * Inherent support for multi-core systems (the Network within the handset)\n
    * Enhanced communication between accessories and the handset\n
    * Consistent connectivity between handset and debug/development and factory tools \n

\section IndustryStandard 3.0 Industry Standard
    For linux platform, sipc is support kernel 2.6.9.

\section ExternalApiDocuments 4.0 External API Documents
    None.

\section ClassAndStructureDefinitions 5.0 C++ Class and Structure Definitions
    None.
       
\section CFunctionsAndDataTypes 6.0 "C" Functions and Data Types
   

\section HeaderFilesAndLibraries 7.0 Header Files And Libraries
         -# ipcsocket.h
         

\section PluginExtensionPointInterface 8.0 Plug-in Extension Point Interface
       None.

\section OtherInterfaces 9.0 Other Interfaces
       None.

\section OtherNotes 10.0 Other Notes
       None.

\warning Optional warning 
     None.
*/

