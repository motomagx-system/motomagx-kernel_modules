/*****************************************************************************************
*                Template No. SWF0114   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA
*
*
*   The GNU C Library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; version 2.1 of 
*   the License.
*
*   This Library is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_memory_access.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   The macros and prototypes defined in this header can be used to access memory 
*   in little-endian format regarldess of the endianness of the platform in which
*   the code is running.  Little-endian was chosen as the format to store all
*   values in shared memory because the ARM11 and x86 are true little-endian
*   processors and the StarCore is not true big-endian (it runs as BE-16).
*
*   To port these memory access macros to a new platform, a new platform-
*   specific implementation file (like ipc_memory_access_be.c) may need to be 
*   created.  The following functions must be defined in this file:
*
*   ipcRead8
*   ipcReadAligned16
*   ipcReadAligned32
*   ipcReadAligned64
*   ipcWrite8
*   ipcWriteAligned16
*   ipcWriteAligned32
*   ipcWriteAligned64
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 12Jan2005  Motorola Inc.         Initial Creation
* 22Nov2005  Motorola Inc.         Rewritten for shared memory
* 19Dec2005  Motorola Inc.         Converted some macros to functions
* 28Sep2006  Motorola Inc.         Convert all macros to functions
* 19Jul2007  Motorola Inc.         Added checking for __cplusplus
*****************************************************************************************/

#ifndef IPC_MEMORY_ACCESS_H
#define IPC_MEMORY_ACCESS_H

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

#define __longlong long long

/* General Memory Access
 * ---------------------
 * These functions work for both aligned and unaligned memory access.  They will
 * first check to see if the address is aligned and call the more efficient
 * aligned access functions when possible.  If it is already known that the address
 * will be aligned, the aligned access functions can be called directly to avoid
 * this check and the overhead of an additional function call.
 */
unsigned char       ipcRead8(const void *addr);
unsigned short      ipcRead16(const void *addr);
unsigned long       ipcRead32(const void *addr);
unsigned __longlong ipcRead64(const void *addr);
float               ipcReadFloat(const void *addr);
double              ipcReadDouble(const void *addr);

void    ipcWrite8(void *addr, unsigned char val);
void    ipcWrite16(void *addr, unsigned short val);           
void    ipcWrite32(void *addr, unsigned long val);      
void    ipcWrite64(void *addr, unsigned __longlong val);
void    ipcWriteFloat(void *addr, float val);
void    ipcWriteDouble(void *addr, double val);


/* Aligned Memory Access
 * ---------------------
 * CAUTION:  No checks are made for valid address alignment so use these functions
 * only when it is certain that the address is properly aligned (16-bit boundary
 * for 16-bit access, 32-bit boundary for 32-bit access, etc.)
 */ 
unsigned short      ipcReadAligned16(const void *addr);
unsigned long       ipcReadAligned32(const void *addr);
unsigned __longlong ipcReadAligned64(const void *addr);

void    ipcWriteAligned16(void *addr, unsigned short val);
void    ipcWriteAligned32(void *addr, unsigned long val);
void    ipcWriteAligned64(void *addr, unsigned __longlong val);


/* Utility function to convert an array of bytes from LE or BE to BE-16 */
void *memcpyToBE16(void *dest, const void *src, unsigned int count);

/* Utility function to convert an array of bytes from BE-16 to LE or BE */
void *memcpyFromBE16(void *dest, const void *src, unsigned int count);

/* NOTE: The two functions above can be used to do conversion in place by
 * providing the same address for src and dest.  However, this is ONLY allowed
 * if that address is 16-bit aligned */

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcRev16(val)  ( ( ((unsigned short)(val)) >> 8 ) | \
                         ( ((unsigned short)(val)) << 8 ) )
                        
#define ipcRev32(val)  ( (  ((unsigned long)(val)) >> 24              ) | \
                         ( (((unsigned long)(val)) & 0x00ff0000) >> 8 ) | \
                         ( (((unsigned long)(val)) & 0x0000ff00) << 8 ) | \
                         (  ((unsigned long)(val)) << 24              ) )

#ifdef __cplusplus
}
#endif

#endif /* IPC_MEMORY_ACCESS_H */
