/*****************************************************************************************
*                Template No. SWF0114   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA
*
*
*   The GNU C Library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; version 2.1 of 
*   the License.
*
*   This Library is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipcsocket.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   Prototype of ipc socket APIs.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 27Oct2004  Motorola Inc.         First Creation
* 19Jan2005  Motorola Inc.         Moved node, service, and channel defines into separate headers
* 22Feb2005  Motorola Inc.         Moved error codes out to separate header
* 11Aug2005  Motorola Inc.         Add preprocessor macro for socket  APIs used in kernel space
* 01Mar2006  Motorola Inc.         Remove deprecated definitions 
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 21Nov2006  Motorola Inc.         Added DSM message discard
* 22Jan2007  Motorola Inc.         Added support for linux protocol family
* 29Jan2007  Motorola Inc.         Header files change for GPL 
*****************************************************************************************/
/**
    \file ipcsocket.h
*/

#ifndef IPC_SOCKET_H
#define IPC_SOCKET_H

#include <linux/ipc_defs.h>
#include <errno.h>

/*-------------------------------------- CONSTANTS -------------------------------------*/
#ifndef NULL
#define NULL (void*)0
#endif

/* set and get error implement in library */
#define ipcGetLastErrorImp()                errno
#define ipcSetLastError(err)                (errno = err)


#define IPC_SHMALLOC_MIN_WAIT_TIME      20  /* milliseconds */

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

#ifdef __cplusplus
extern "C"{
#endif
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

/**
 * \brief
 * This function determines the status of one or more ipc sockets, waiting if necessary, to perform synchronous I/O.
 *
 *   \param [in]  ipcnfds  int, Ignored. The nfds parameter is included only for compatibility with standard sockets.
 *   \param [in,out]  readfds  pointer to ipcfd_set, Optional pointer to a set of sockets to be checked for readability.
 *   \param [in,out]  writefds  pointer to ipcfd_set, Optional pointer to a set of sockets to be checked for writability.
 *   \param [in,out] excepfds  pointer to ipcfd_set, Not currently implemented, Optional pointer to a set of sockets to be checked for errors.
 *   \param [in,out]  timeout  pointer to struct ipctimeval, Maximum time for select to wait, provided in the form of
 *                                    a timeval structure. Set the timeout parameter to NULL for blocking operation.
 *
 *   \return On failure - returns a value of IPC_ERROR, and a specific error code can be retrieved by calling Ipcgetlasterror.
 *           On success - This function returns the total number of socket handles that are ready and contained in the
 *                                ipcfd_set structures, zero if the time limit expired
 *
 *\par Remark
 * This function is used to determine the status of one or more IPC sockets. For each
 * IPC socket, the caller can request information on read, write, or error status. The set
 * of IPC sockets for which a given status is requested is indicated by an ipcfd_set structure.
 * The sockets contained within the ipcfd_set structures must be associated with only IPC
 * protocol sockets. A set of macros is provided for manipulating an ipcfd_set structure.
 *The ipcreadfds parameter identifies the sockets that are to be checked for readability.
 *Readability means that queued data is available for reading such that a call to  ipcrecvfrom,
 *is guaranteed not to block.
 *The ipcwritefds parameter identifies the sockets that are to be checked for writeability.
 *Writeability means that the peer socket's queue has space and you may perform a write operation
 * without blocking, that a call to  ipcsend or ipczsend, is guaranteed not to block.
 *Note that the ipcwritefds provides does not support socket of type IPC_CL_SOCK. That is a full
 *connection must exist between the specifiec socket and the peer socket.
 *Four macros are defined in the header file ipcsocket.h for manipulating and checking the
 *descriptor sets. Use of these macros will maintain software portability between different
 *socket environments. The following macros manipulate and check ipcfd_set contents:
 *IPCFD_CLR(s, *set)
 *Removes descriptor s from set.
 *IPCFD_ISSET(s, *set)
 *Nonzero if s is a member of the set. Otherwise, zero.
 *IPCFD_SET(s, *set)
 *Adds descriptor s to set.
 *IPCFD_ZERO(*set)
 *Initializes the set to the NULL set.
*The timeout parameter controls how long the ipcselect can take to complete. If timeout is
 *a NULL pointer, ipcselect will block indefinitely until at least one descriptor meets the
 *specified criteria. Otherwise, timeout points to a timeval structure that specifies the maximum
 *time that ipcelect should wait before returning.  If timeval is initialized to {0, 0}, ipcselect
 * will return immediately; this is used to poll the state of the selected sockets. If ipcselect
 * returns immediately, then the ipcselect call is considered nonblocking and the standard
 * assumptions for nonblocking calls apply.
 *


 * \par Error Code:
 * - \b  IPCEFAULT      The IPC implementation was unable to allocate needed resources for its internal operations or the readfds, writefds, ex
ceptfds, or timeval parameters are not part of the user address space.
 * - \b  IPCENOTINIT    Ipcinit was not called before making this call
 * - \b  IPCEINVAL      The time-out value is not valid or socket specified in descriptor set is not bound.
 * - \b  IPCENOTSOCK    One of the descriptor sets contains an entry that is not an IPC socket.
 */

int ipcselect(int ipcnfds,              /* IGNORED */
              ipcfd_set * readfds,
              ipcfd_set * writefds,     /* NOT IMPLEMENTED */
              ipcfd_set * execpfds,     /* NOT IMPLEMENTED */
              struct ipctimeval * timeout);
              
/*  Error handling  */

/**
 * \brief
 * This function gets the error status for the last operation that failed.
 *
 *   \param :None
 *   \return The return value indicates the error code for this thread's last IPC Sockets operation that failed.
 *               See individual calls for specific possible error codes.
 *
 *\par Remark
 * This function returns the last network error that occurred. A successful function call, or a call to
 *           ipcgetlasterror, does not reset the error code.
 *
 * \par Error Code:
 * None
 */

int ipcgetlasterror(void);
/**
 * \brief
 * This function gets the error text given the error code obtained from ipcgetlasterror.
 *
 *   \param [in]  ipc_error: int, Ipc error number returned by ipcgetlasterror.
 *
 *   \return The return is the string corresponding to the text for the input ipc_error obtained by calling ipcgetlasterror.
 *
 *
 \par Remark:
 * This function translates the error number obtained by calling ipcgetlasterror to the corresponding text string.
 *
 * \par Error Code:
 * None
 *
 */

const char *ipcgeterrortext(int ipc_error);

/*--------------------------------------- MACROS ---------------------------------------*/
#define IPCFD_ZERO FD_ZERO
#define IPCFD_SET FD_SET
#define IPCFD_ISSET FD_ISSET
#define IPCFD_CLR FD_CLR

#ifdef __cplusplus
}
#endif

#endif /* IPC_SOCKET_H */
