/*****************************************************************************************
*                Template No. SWF0114   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA
*
*
*   The GNU C Library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; version 2.1 of 
*   the License.
*
*   This Library is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_memory_helper.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains helper macros that are used to implement the
*   platform-specific memory access macros.  These macros are not meant
*   to be called directly.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 12Jan2005  Motorola Inc.         Initial Creation
* 19Dec2005  Motorola Inc.         Converted some macros to functions
*****************************************************************************************/

#ifndef IPC_MEMORY_HELPER_H
#define IPC_MEMORY_HELPER_H

#include "ipc_memory_access.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
  
#define _ipcu8(val)         ((unsigned char)        (val))
#define _ipcu16(val)        ((unsigned short)       (val))
#define _ipcu32(val)        ((unsigned long)        (val))
#define _ipcu64(val)        ((unsigned __longlong)  (val))

#define _ipcp8(addr)        ((unsigned char *)      (addr))
#define _ipcp16(addr)       ((unsigned short *)     (addr))
#define _ipcp32(addr)       ((unsigned long *)      (addr))
#define _ipcp64(addr)       ((unsigned __longlong *)(addr))


#define _ipcbyte0(val)      _ipcu8(_ipcu32(val)      )
#define _ipcbyte1(val)      _ipcu8(_ipcu32(val) >> 8 )
#define _ipcbyte2(val)      _ipcu8(_ipcu32(val) >> 16)
#define _ipcbyte3(val)      _ipcu8(_ipcu32(val) >> 24)

#define _ipcshort0(val)     _ipcu16(_ipcu32(val)      )
#define _ipcshort2(val)     _ipcu16(_ipcu32(val) >> 16)

#define _ipcshift0(val)     (_ipcu32(val)      )
#define _ipcshift1(val)     (_ipcu32(val) << 8 )
#define _ipcshift2(val)     (_ipcu32(val) << 16)
#define _ipcshift3(val)     (_ipcu32(val) << 24)


#define _ipcllbyte0(val)    _ipcu8(_ipcu64(val)      )
#define _ipcllbyte1(val)    _ipcu8(_ipcu64(val) >> 8 )
#define _ipcllbyte2(val)    _ipcu8(_ipcu64(val) >> 16)
#define _ipcllbyte3(val)    _ipcu8(_ipcu64(val) >> 24)
#define _ipcllbyte4(val)    _ipcu8(_ipcu64(val) >> 32)
#define _ipcllbyte5(val)    _ipcu8(_ipcu64(val) >> 40)
#define _ipcllbyte6(val)    _ipcu8(_ipcu64(val) >> 48)
#define _ipcllbyte7(val)    _ipcu8(_ipcu64(val) >> 56)

#define _ipcllshort0(val)   _ipcu16(_ipcu64(val)      )
#define _ipcllshort2(val)   _ipcu16(_ipcu64(val) >> 16)
#define _ipcllshort4(val)   _ipcu16(_ipcu64(val) >> 32)
#define _ipcllshort6(val)   _ipcu16(_ipcu64(val) >> 48)

#define _ipcllshift0(val)   (_ipcu64(val)      )
#define _ipcllshift1(val)   (_ipcu64(val) << 8 )
#define _ipcllshift2(val)   (_ipcu64(val) << 16)
#define _ipcllshift3(val)   (_ipcu64(val) << 24)
#define _ipcllshift4(val)   (_ipcu64(val) << 32)
#define _ipcllshift5(val)   (_ipcu64(val) << 40)
#define _ipcllshift6(val)   (_ipcu64(val) << 48)
#define _ipcllshift7(val)   (_ipcu64(val) << 56)


#endif /* IPC_MEMORY_HELPER_H */
