/*****************************************************************************************
*                Template No. SWF0114   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2006 MOTOROLA
*
*
*   The GNU C Library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; version 2.1 of 
*   the License.
*
*   This Library is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_memory_access_le.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   No longer needed.  This header file is being kept to avoid build breakages.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 22Nov2005  Motorola Inc.         Initial Creation
* 28Sep2006  Motorola Inc.         Cleared out
*****************************************************************************************/

#include "ipc_memory_access.h"

