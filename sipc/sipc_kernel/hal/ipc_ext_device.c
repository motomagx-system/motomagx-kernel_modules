/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2006 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_ext_device.c
*   FUNCTION NAME(S): ipcExtDevClose
*                     ipcExtDevRead
*                     ipcExtDevStop
*                     ipcExtDevWrite
*                     ipcExtOpen
*                     ipc_ext_dev_open
*                     ipc_ext_dev_poll
*                     ipc_ext_dev_read
*                     ipc_ext_dev_release
*                     ipc_ext_dev_write
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 28Apr2005  Motorola Inc.         Added data buffering
* 13Mar2006  Motorola Inc.         Change kernel_thread to kthread_run
* 17Nov2006  Motorola Inc.         Created char device when opening
* 29Nov2006  Motorola Inc.         Removed udev support 
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/device.h>
#include <linux/kthread.h>

#include "ipc_sock_impl.h"
#include "ipc_device_layer.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
#define IPCEXTCHRNAME "ipc_ext_dev"

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
typedef struct _inData_t {
    void *data;
    int len;
    struct list_head cont;
} inData_t;

typedef inData_t outData_t;

typedef struct tag_ipcExtDevDriver_t
{
    wait_queue_head_t wq;       /* daemon process sleeps on this queue while selecting */
    wait_queue_head_t outq_empty;
    struct task_struct* output_pid;
    struct task_struct* input_pid;
    int has_daemon: 1;          /* whether there's a daemon process getting data */
    int in_use: 1;              /* whether this device has been opened */
    int stopped: 1;             /* if marked, daemon process returns 0 while reading */
    int bandwidth;
    int framerate;
    ipcPortId_t portId;
    struct list_head inq;
    struct list_head outq;
    wait_queue_head_t inpack_wq; /* device input thread wait on this queue */
}ipcExtDevDriver_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
static int ipc_ext_dev_open(struct inode *inode, struct file *filp);
static ssize_t ipc_ext_dev_read(struct file *filp, char *buf, size_t count, loff_t *f_ops);
static ssize_t ipc_ext_dev_write(struct file *filp, const char *buf, size_t count, loff_t *f_ops);
static int ipc_ext_dev_release(struct inode *inode, struct file *filp);
static unsigned int ipc_ext_dev_poll(struct file *filp, struct poll_table_struct *);

/*------------------------------------- STATIC DATA ------------------------------------*/
static struct file_operations ipc_ext_dev_ops = {
    .open = ipc_ext_dev_open,
    .release = ipc_ext_dev_release,
    .read = ipc_ext_dev_read,
    .write = ipc_ext_dev_write,
    .poll = ipc_ext_dev_poll
};


static ipcExtDevDriver_t ext_drivers[IPC_MAX_PORTS];
static int drivers_in_use = 0;
static int ext_dev_major = 0;

/*--------------------------------------- MACROS ---------------------------------------*/
/*
 * Some function prototypes for the device.
 * Here "ExtDev" stands for "external device"
 */
static int ipcExtDevRead(void *drv_handle, UINT8 *data, int len);
static int ipcExtDevWrite(void *drv_handle, const UINT8 *data, int len);
static void ipcExtDevStop(void *drv_handle);
static void ipcExtDevClose(void *drv_handle);
BOOL ipcExtOpen(UINT8 portId, INT32 bandwidth, INT32 framerate);



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcExtOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Open an external device. This is called when initializaing the ipc stack.
*   
*   PARAMETER(S):
*       portId: Port id of the device
*       bandwidth: The bandwidth of the device
*       framerate: The frame rate of the device
*          
*   RETURN: TRUE on success, FALSE on failure.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 30Aug2005  Motorola Inc.         Checked where band of ext_drivers
* 13Mar2006  Motorola Inc.         Change kernel_thread to kthread_run
* 17Nov2006  Motorola Inc.         Created char device when opening
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 29Nov2006  Motorola Inc.         Removed udev support
*
*****************************************************************************************/
BOOL ipcExtOpen(UINT8 portId, INT32 bandwidth, INT32 framerate)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcExtDevDriver_t* driver = NULL;
    ipcDeviceDriver_t ext_driver;

/*--------------------------------------- CODE -----------------------------------------*/
    /* register the pseudo device if needed, because we may have more than one device */
    if (drivers_in_use == 0)
    {
        memset(&ext_drivers, 0, sizeof(ext_drivers));
        if ((ext_dev_major = register_chrdev(ext_dev_major,
                        IPCEXTCHRNAME, &ipc_ext_dev_ops)) < 0)
        {
            ipcError(IPCLOG_GENERAL, ("Create char device failed\n"));
            return FALSE;
        }
    }

    if (drivers_in_use >= IPC_MAX_PORTS)
    {
        return FALSE;
    }

    driver = &ext_drivers[portId];
    /* create an ipc device */
    ext_driver.handle = driver;
    ext_driver.read = ipcExtDevRead;
    ext_driver.write = ipcExtDevWrite;
    ext_driver.stop = ipcExtDevStop;
    ext_driver.close = ipcExtDevClose;

    if (!ipcCreateDevice(portId, IPC_DEVICE_EXT, bandwidth, framerate, &ext_driver))
    {
        return FALSE;
    }
    driver->portId = portId;
    driver->framerate = framerate;
    driver->bandwidth = bandwidth;
    driver->has_daemon = 0;
    driver->stopped = 0;
    init_waitqueue_head(&driver->wq);
    init_waitqueue_head(&driver->outq_empty);
    init_waitqueue_head(&driver->inpack_wq);
    driver->in_use = 1;
    drivers_in_use++;
    INIT_LIST_HEAD(&driver->inq);
    INIT_LIST_HEAD(&driver->outq);

    /* the function ipcExtDevStart is no longer needed */
    driver->output_pid = kthread_run((int(*)(void*))&ipcDeviceOutputThread, 
            (void*)(int)driver->portId, "ipc_ext_dev%d_out", driver->portId);
    if (IS_ERR(driver->output_pid))
    {
        ipcError(IPCLOG_GENERAL, ("kthread_run failed to create ipc_ext_dev input task\n"));
        ipcCloseDevice(portId);
        drivers_in_use --;
        return FALSE;
    }
    driver->input_pid = kthread_run((int(*)(void*))&ipcDeviceInputThread, 
            (void*)(int)driver->portId, "ipc_ext_dev%d_in", driver->portId);
    if (IS_ERR(driver->input_pid))
    {
        ipcError(IPCLOG_GENERAL, ("kthread_run failed to create ipc_ext_dev output task\n"));
        ipcCloseDevice(portId);
        drivers_in_use --;
        return FALSE;
    }

    devfs_mk_cdev(MKDEV(ext_dev_major, driver->portId),S_IFCHR | S_IRUGO | S_IWUGO, "ipc_ext_dev%d", driver->portId);
    
    /* Disable the device by default.
     * The device is enabled when the daemon task opens it
     */
    ipcDeviceDisable(portId);
    return TRUE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcExtDevStop
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Stop an external device.
*   
*   PARAMETER(S):
*       drv_handle: The handle to the device
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 13Mar2006  Motorola Inc.         Change kill_proc to force_sig 
*
*****************************************************************************************/
void ipcExtDevStop(void* drv_handle)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    /* kill the output thread */
    ipcExtDevDriver_t *driver = (ipcExtDevDriver_t*)drv_handle;
    ipcDevice_t* device;
/*--------------------------------------- CODE -----------------------------------------*/
    if (driver == NULL)
    {
        ipcError(IPCLOG_GENERAL, ("NULL parameter drv_handle\n"));
        return;
    }
    /* just wake up the thread, any signal will work,
     * here we don't use kthread_stop to wait for the thread to exit*/
    if (! IS_ERR(driver->input_pid))
    {
        force_sig(SIGTERM, driver->input_pid);
    }
    /* inform the daemon process for the device */
    driver->stopped = 1;
    wake_up_all(&driver->wq);
    /* set the read and write function pointer to NULL */
    device = ipcGetDevice(driver->portId);
    if (device != NULL)
    {
        device->driver.read = NULL;
        device->driver.write = NULL;
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcExtDevRead
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from an external device, called by device layer.
*   
*   PARAMETER(S):
*       drv_handle: the handle of the device
*       data: the buffer to store the incoming data
*       len: the length of the buffer
*       
*   RETURN: The actual length we can get now. 
*           -EINTR if the reader is interrupted.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcExtDevRead(void* drv_handle, UINT8* data, int len)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    /* we don't implement this function, ignore it */
    ipcExtDevDriver_t *driver = (ipcExtDevDriver_t *)drv_handle;
    inData_t *in = NULL;
    struct list_head *p = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    if (driver->stopped)
    {
        return 0;
    }
    if (list_empty(&driver->inq)) 
    {
        //interruptible_sleep_on(&driver->inpack_wq);
        //if (signal_pending(current))
        if (-ERESTARTSYS == wait_event_interruptible(driver->inpack_wq, 
                    !list_empty(&driver->inq)))
        {
            return -EINTR;
        }
    }

    if (!list_empty(&driver->inq))
    {
        p = driver->inq.next;
        in = (inData_t *)list_entry(p, inData_t, cont);
        list_del(p);
        memcpy(data, in->data, in->len);
        len = in->len;
        kfree(in->data);
        kfree(in);
    }

    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcExtDevWrite
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Write data to the external device.
*   
*   PARAMETER(S):
*       drv_handle: the handle of the device
*       data: the pointer to data to be written out
*       len: the length of data
*       
*   RETURN: The length of data actually written out.
*           -1 on failure.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcExtDevWrite(void* drv_handle, const UINT8* data, int len)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    /* check for the daemon process, if none, return */
    outData_t *out = NULL;
    ipcExtDevDriver_t *driver = (ipcExtDevDriver_t*)drv_handle;

/*--------------------------------------- CODE -----------------------------------------*/
    if (driver == NULL)
    {
        ipcError(IPCLOG_GENERAL, ("NULL parameter drv_handle\n"));
        return -1;
    }
    if (!driver->has_daemon)
    {
        //ipcError(IPCLOG_GENERAL, ("There's no daemon process reading data out\n"));
        return -1;
    }
    out = (outData_t *)kmalloc(sizeof(outData_t), GFP_KERNEL);
    if (NULL == out) return -1;
    memset(out, 0, sizeof(outData_t));
    out->data = kmalloc(len, GFP_KERNEL);
    if (out->data == NULL)
    {
        kfree(out);
        return -1;
    }    
    memset(out->data, 0, len);
    memcpy(out->data, data, len);
    out->len = len;
    list_add_tail(&out->cont, &driver->outq);

    wake_up_all(&driver->wq);
    if (-ERESTARTSYS == wait_event_interruptible(driver->outq_empty,
                list_empty(&driver->outq)))
    {
        return -EINTR;
    }

    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcExtDevClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Close an external device.
*   
*   PARAMETER(S):
*       drv_handle: the handle of the device
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation
* 17Nov2006  Motorola Inc.         Deleted char device when closing
* 29Nov2006  Motorola Inc.         Removed udev support 
*
*****************************************************************************************/
void ipcExtDevClose(void* drv_handle)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    inData_t *in = NULL;
    outData_t* out = NULL;
    struct list_head *p = NULL;
    ipcExtDevDriver_t* driver = (ipcExtDevDriver_t*)drv_handle;

/*--------------------------------------- CODE -----------------------------------------*/
    if (driver == NULL)
    {
        ipcError(IPCLOG_GENERAL, ("Null parameter drv_handle\n"));
        return;
    }

    devfs_remove("ipc_ext_dev%d", driver->portId);

    if (!driver->stopped)
    {
        ipcExtDevStop(driver);
    }
    while (!list_empty(&driver->inq))
    {
        p = driver->inq.next;
        in = (inData_t *)list_entry(p, inData_t, cont);
        list_del(p);
        kfree(in->data);
        kfree(in);
    }

    while (!list_empty(&driver->outq))
    {
        p = driver->outq.next;
        out = (outData_t *)list_entry(p, outData_t, cont);
        list_del(p);
        kfree(out->data);
        kfree(out);
    }

    driver->in_use = 0;
    /* unregister the pseudo device, if needed */
    ipcDebugLog(IPCLOG_GENERAL,
        ("Now there're %d devices in use, before closing one\n", drivers_in_use));
    if (--drivers_in_use == 0)
    {
        ipcDebugLog(IPCLOG_GENERAL, ("Now no device is in use, closing chrdev and proc entry\n"));
        unregister_chrdev(ext_dev_major, IPCEXTCHRNAME);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_ext_dev_open
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Open the device. This function is invoked when the user-space application
*   opens the device file, e.g. /dev/ipc_ext_dev0
*   
*   PARAMETER(S):
*       inode: inode
*       filp: pointer to file. These two parameters are defined by linux.
*       
*   RETURN: 0 on success, 
*           -ENODEV if this is not a valid device
*           -EEXIST if the device is already open
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
int ipc_ext_dev_open(struct inode *inode, struct file *filp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcExtDevDriver_t* driver;

/*--------------------------------------- CODE -----------------------------------------*/
    /* get the minor device number */
    int minor = MINOR(inode->i_rdev);
    /* check whether the device is valid or used */
    if (minor >= IPC_MAX_PORTS)
    {
        ipcError(IPCLOG_GENERAL, ("The minor device number is invalid\n"));
        return -ENODEV;
    }
    driver = &ext_drivers[minor];
    if (driver->has_daemon)
    {
        ipcError(IPCLOG_GENERAL, ("The device already has a daemon\n"));
        return -EEXIST;
    }
    /* fill the filp->f_op */
    filp->f_op = &ipc_ext_dev_ops; 
    /* fill the filp->private_data */
    filp->private_data = driver;
    driver->has_daemon = 1;
    ipcDeviceEnable(driver->portId);
    /* increment the reference count of the module */
    get_ipc_module();
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_ext_dev_read
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from the device. This is invoked by user-space applications.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ssize_t ipc_ext_dev_read(struct file *filp, char *buf, size_t count, loff_t *f_ops)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int len = 0;
    struct list_head *p = NULL;
    outData_t *out = NULL;
    unsigned long bytes;
    ipcExtDevDriver_t* driver = filp->private_data;

/*--------------------------------------- CODE -----------------------------------------*/
    if ((buf == NULL) || (count < IPC_DEV_BUFFER_SIZE))
    {
        ipcError(IPCLOG_GENERAL, ("Invalid buffer.(too small)\n"));
        return -EFAULT;
    }
    /* check if the device is stopped */
    if (driver->stopped)
        return 0;

    /* sleep while there is no data in the buffer */
    if (list_empty(&driver->outq)) {
        //interruptible_sleep_on(&driver->wq);
        //if (signal_pending(current))
        if (-ERESTARTSYS == wait_event_interruptible(driver->wq,
                    !list_empty(&driver->outq)))
        {
            return -EINTR;
        }
    }

    /* copy the frame buffer information to buf */
    if (!list_empty(&driver->outq))
    {
        p = driver->outq.next;
        out = (outData_t *)list_entry(p, outData_t, cont);
        list_del(p);
        bytes = copy_to_user(buf, out->data, out->len);
        if (bytes != 0)
        {
            ipcError(IPCLOG_GENERAL, ("copy_to_user failed!\n"));
        }
        len = out->len;
        kfree(out->data);
        kfree(out);
    }
    if (list_empty(&driver->outq))
    {
        wake_up_all(&driver->outq_empty);
    }

    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_ext_dev_write
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Write data to the device. Called by user-space applications.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ssize_t ipc_ext_dev_write(struct file *filp, const char *buf, size_t count, loff_t *f_ops)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcExtDevDriver_t* driver = filp->private_data;
    unsigned char * tmpbuf;
    inData_t *in;
    unsigned long bytes;

/*--------------------------------------- CODE -----------------------------------------*/
    if (buf == NULL)
    {
        ipcError(IPCLOG_GENERAL, ("NULL pointer argument buf\n"));
        return 0;
    }
    tmpbuf = (unsigned char *)kmalloc(count, GFP_KERNEL);
    if (NULL == tmpbuf) return -1;

    memset(tmpbuf, 0, count);
    bytes = copy_from_user(tmpbuf, buf, count);
    if (bytes != 0)
    {
        ipcError(IPCLOG_GENERAL, ("copy_from_user failed!\n"));
        kfree(tmpbuf);
        return -1;
    }
    ipcDebugLog(IPCLOG_GENERAL, ("user call write:%d bytes\n", count));

    in = (inData_t *)kmalloc(sizeof(inData_t), GFP_KERNEL);
    if (NULL == in) {
        kfree(tmpbuf);
        return -1;
    }
    memset(in, 0, sizeof(inData_t));
    in->data = tmpbuf;
    in->len = count;
    list_add_tail(&in->cont, &driver->inq);
    wake_up_all(&driver->inpack_wq);
    return count;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_ext_dev_release
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Close the device. This is called when the user-space applications close
*   the device file.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipc_ext_dev_release(struct inode *inode, struct file *filp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcExtDevDriver_t* driver = filp->private_data;
/*--------------------------------------- CODE -----------------------------------------*/
    put_ipc_module();
    driver->has_daemon = 0;
    ipcDeviceDisable(driver->portId);
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_ext_dev_poll
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Poll the status of the device. This is used by the user-space application
*   when it tries to call select.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned int ipc_ext_dev_poll(struct file *filp, struct poll_table_struct *polltab)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int mask = 0;
    ipcExtDevDriver_t* driver = filp->private_data;

/*--------------------------------------- CODE -----------------------------------------*/
    poll_wait(filp, &driver->wq, polltab);
    if (!list_empty(&driver->outq))
        mask |= POLLIN | POLLRDNORM;
    return mask;
}

// vim: set ts=4 sw=4 et:
