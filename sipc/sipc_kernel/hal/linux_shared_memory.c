/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : linux_shared_memory.c
*   FUNCTION NAME(S): initPlatformSharedMemory
*                     ipcHandleSMInitMessage
*                     ipcPhyShmPoolWalk
*                     ipcSMParseConfigString
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   The file contains the initialization routine for the shared memory driver
*   on Linux.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 09Jun2005  Motorola Inc.         Initial Version
* 07Jul2005  Motorola Inc.         Support configurable creating pool
* 19Jan2006  Motorola Inc.         Change default pool config
* 15Mar2006  Motorola Inc.         AP will intialize ALL of shared memory (BP pools too)
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 13Mar2007  Motorola Inc.         Updated for non-SIPC shared memory section
*****************************************************************************************/

#ifdef HAVE_PHYSICAL_SHM

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "mu_drv.h"
#include "ipc_shared_memory.h"
#include "ipc_mu_shm.h"
#include "ipc_osal.h"
#include "ipc_osal_util.h"
#include "ipclog.h"
#include "ipc_defs.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

/* There is a small section at the end of shared memory reserved for other users.
 * This must match BP_BOOT_INFO_SIZE in data_logger_public.h (which we cannot include
 * from within the kernel) */
#define AP_BP_NON_SIPC_SHM_SIZE (1024 * 4)

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern ulong paddr;
extern ulong psize;

char *pshm_pool = NULL;
char *pshm_pool_bp = NULL;

extern int debug_bp;

static void *multicoreSemaAddr;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

void ipcSMParseConfigString(char *config, ipcSMPoolConfig_t *poolConfig);

/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMParseConfigString
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function parses a shared memory pool configuration string and populates
*   the configuration structure with the specified pool buffer sizes and counts.
*   
*   PARAMETER(S):
*       config: Shared memory pool configuration string
*       poolConfig: Shared memory config structure to populate
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Mar2006  Motorola Inc.         Initial Version
*
*****************************************************************************************/
void ipcSMParseConfigString(char *config, ipcSMPoolConfig_t *poolConfig)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char buf[20];
    char *p = config;
    char *q = config;
    char *t = NULL;
    int numPools = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (config != NULL)
    {
        while ((*p != 0) && (*q != 0))
        {
            if (numPools >= MAX_SM_POOLS)
            {
                ipcError(IPCLOG_GENERAL, ("Shared memory config string error: too many pools\n"));
                break;
            }
           
            t = p;
            while ((*t != 'x') && (*t != ';') && (*t != 0))
            {
                t++;
            }
            if (*t != 'x')
            {
                ipcError(IPCLOG_GENERAL, ("Shared memory config string error: expected 'x'\n"));
                break;
            }
            memcpy(buf, p, t-p);
            buf[t-p] = 0;
            if ((poolConfig[numPools].bufSize = atoi(buf)) <= 0)
            {
                ipcError(IPCLOG_GENERAL, ("Shared memory config string error: illegal bufSize\n"));
                break;
            }            

            q = ++t;
            while ((*q != 'x') && (*q != ';') && (*q != 0))
            {
                q++;
            }
            if ((*q != ';') && (*q != 0))
            {
                ipcError(IPCLOG_GENERAL, ("Shared memory config string error: expected ';'\n"));
                break;
            }

            p = q + 1;
            memcpy(buf, t, q-t);
            buf[q-t] = 0;
            if ((poolConfig[numPools].numBuffers = atoi(buf)) <= 0)
            {
                ipcError(IPCLOG_GENERAL, ("Shared memory config string: illegal numBuffers\n"));
                break;
            }
            
            ++numPools;
        }
    }

    /* Zero-terminate the config array */
    poolConfig[numPools].bufSize = 0;
    poolConfig[numPools].numBuffers = 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: initPlatformSharedMemory
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function initializes shared memory pools on Linux
*   
*   PARAMETER(S):
*       shmOffset: offset between physical and virtual addresses
*       shmAddr: location of shared memory region
*       shmSize: size of shared memory region
*       poolConfig: not used (only for BP standalone builds)
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Jun2005  Motorola Inc.         Initial Version
* 07Jul2005  Motorola Inc.         Support configurable creating pool
* 18Jul2006  Motorola Inc.         round up pooltable start addr to 8-bytes boundry
* 11Oct2006  Motorola Inc.         rollback to 4-bytes boundry
* 13Mar2007  Motorola Inc.         Updated for non-SIPC shared memory section
* 25Apr2007  Motorola Inc.         Clean up KW warning
*****************************************************************************************/
void initPlatformSharedMemory(int shmOffset, void *shmAddr, int shmSize,
                              ipcSMPoolConfig_t *poolConfig)
{                                
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ulong addrLocal;
    ulong addrRemote;
    void *bpInitAddr;
    
    int sizeLocal;
    int sizeRemote;
    int bytesLeft;
    
    ipcSMPoolConfig_t localSMPoolConfig[MAX_SM_POOLS + 1]; 
    ipcSMPoolConfig_t remoteSMPoolConfig[MAX_SM_POOLS + 1];
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSMOffset = shmOffset;
    
    if ((paddr == 0) || (psize == 0))
    {
        ipcError(IPCLOG_GENERAL, ("Invalid physical shared memory address or size, paddr = %x, psize = %d\n",
            (unsigned int)paddr, (int)psize));
        return;
    }    
    
    ipcSMParseConfigString(pshm_pool, localSMPoolConfig);
    ipcSMParseConfigString(pshm_pool_bp, remoteSMPoolConfig);
    
    sizeLocal = ipcSMCalculatePoolTableSize(localSMPoolConfig);
    sizeRemote = ipcSMCalculatePoolTableSize(remoteSMPoolConfig);
    multicoreSemaAddr = (void *)ipcSMPhysicalToVirtual(paddr);
    
    bytesLeft = shmSize - (sizeof(ipcMulticoreSema_t) + AP_BP_NON_SIPC_SHM_SIZE + sizeLocal + sizeRemote);
    
    if (bytesLeft > 0)
    {
        ipcPrint(IPCLOG_GENERAL, ("Unused bytes in physical shared memory: %d\n", bytesLeft));
        ipcPrint(IPCLOG_GENERAL, ("sizeLocal = %d, sizeRemote = %d, pshm_pool_bp = %s\n", 
            sizeLocal, sizeRemote, pshm_pool_bp));
    }
    else
    {
        ipcError(IPCLOG_GENERAL, ("Insufficient bytes in physical shared memory: need %d more\n", -bytesLeft));
        return;
    }
    
    ipcMulticoreSemaOpen(multicoreSemaAddr);

    addrLocal = (ulong)shmAddr + sizeof(ipcMulticoreSema_t);
    addrLocal = (addrLocal + 3) & (~3);
    addrRemote = addrLocal + sizeLocal;
    addrRemote = (addrRemote + 3) & (~3);
    
    localSMPoolTblProp = (SMPoolTblProp_t *)ipcSMPhysicalToVirtual(addrLocal);
    remoteSMPoolTblProp = (SMPoolTblProp_t *)ipcSMPhysicalToVirtual(addrRemote);
    
    if (NULL != localSMPoolTblProp)
    {
        ipcSMPoolTableInit(localSMPoolTblProp, LOCAL_CORE_ID, sizeLocal, localSMPoolConfig);
    }
    if(NULL != remoteSMPoolTblProp)
    {
        ipcSMPoolTableInit(remoteSMPoolTblProp, REMOTE_CORE_ID, sizeRemote, remoteSMPoolConfig);
    }
    
    /* Store BP Init information at end of shared memory region (right before start of BP image) */
    bpInitAddr = ipcSMPhysicalToVirtual((ulong)shmAddr + shmSize - sizeof(BPInitFlags_t));
    if (NULL == bpInitAddr)
    {
        return;
    }
    
    if (debug_bp)
    {
        configBpDebug(bpInitAddr);
    }
    else
    {
        configBpNoDebug(bpInitAddr);
    }
    
    ipcIsShmReady = 1;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleSMInitMessage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when the AP receives a shared memory init message
*   from the BP.  This message is a request for the AP to send all shared memory
*   configuration information to the BP during initialization.
*   
*   PARAMETER(S):
*       channel: MU Channel ID where data was received
*       data: pointer to the initialization message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 31Aug2005  Motorola Inc.         Initial Version
*
*****************************************************************************************/
void ipcHandleSMInitMessage(int channel, int data)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMInitMessage_t *initMessage;
    int physicalAddr;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsShmReady)
    {
        ipcError(IPCLOG_GENERAL, ("Got SM Init request from BP before shared memory was ready!\n"));
        return;
    }
    
    initMessage = (ipcSMInitMessage_t *)ipcAlloc(sizeof(ipcSMInitMessage_t), IPC_PHYSSHM);
    if (initMessage != NULL)
    {
        /* Populate BP init message */
        physicalAddr = (int)ipcSMVirtualToPhysical(multicoreSemaAddr);
        ipcOsalWriteAligned32(&initMessage->multicoreSema, physicalAddr);
        
        physicalAddr = (int)ipcSMVirtualToPhysical(localSMPoolTblProp);
        ipcOsalWriteAligned32(&initMessage->masterPoolTable, physicalAddr);
        
        physicalAddr = (int)ipcSMVirtualToPhysical(remoteSMPoolTblProp);
        ipcOsalWriteAligned32(&initMessage->slavePoolTable, physicalAddr);

        /* Send the reply message to the BP via the MU */
        muWrite(MU_CHANNEL_SHM_INIT, (int)ipcSMVirtualToPhysical(initMessage));
    }
    else
    {
        ipcError(IPCLOG_GENERAL, ("No memory for SM Init Message!\n"));
    }

    shmConnected = 1;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPhyShmPoolWalk
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get physical shared memory pool info
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Jun2005  Motorola Inc.         Initial Version
*
*****************************************************************************************/
int ipcPhyShmPoolWalk(void)
{
    int rc = 0;

    muShmLock();    
    rc = ipcSMReport(localSMPoolTblProp, ipcPhyShmReport);
    rc += ipcSMReport(remoteSMPoolTblProp, ipcPhyShmReport);    
    muShmUnlock();

    return rc;
}

ipc_string_param(pshm_pool);
ipc_string_param(pshm_pool_bp);

#endif /* HAVE_PHYSICAL_SHM */
