/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_iden_usb.c
*   FUNCTION NAME(S): append_input_data
*                     ipcUsbClose
*                     ipcUsbOpen
*                     ipcUsbRead
*                     ipcUsbStop
*                     ipcUsbWrite
*                     ipc_usb_delete
*                     ipc_usb_disconnect
*                     ipc_usb_probe
*                     ipc_usb_recv_complete
*                     ipc_usb_send_complete
*                     on_data_arrival
*                     submit_read_urb
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file provides the USB driver for IPC stack.
*
*   It works on linux PC/board to communicate with LPK board.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation
* 13Mar2006  Motorola Inc.         Change kernel_thread to kthread_run 
* 13Jul2007  Motorola Inc.         Removed debug code
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_osal.h"
#include "ipclog.h"
#include "ipc_device_layer.h"
#include <linux/usb.h>
#include <linux/spinlock.h>
#include <linux/kthread.h>

/*-------------------------------------- CONSTANTS -------------------------------------*/
#define IDEN_IPC_VID  0x0c44
#define IDEN_IPC_PID  0x0020
#define INPUT_QUEUE_LIMIT 4096 /* the size limit of input queue */
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
typedef struct tag_ipcUsbDevice_t ipcUsbDevice_t;

struct ipc_usb_data_t
{
    struct kref                 kref;
    struct usb_device *         device;
    struct usb_interface *      interface;

    u32                         bulk_in_endpoint_addr;
    u32                         bulk_in_size;
    char *                      bulk_in_buffer;
    struct urb *                bulk_in_urb;

    u32                         bulk_out_endpoint_addr;
    u32                         bulk_out_size;

    ipcUsbDevice_t *            ipc_dev;
};

typedef struct tag_ipcDataBlock_t
{
    char* buf;
    char* cur_ptr;
    u32 buflen;
    struct list_head list;
}ipcDataBlock_t;

struct tag_ipcUsbDevice_t
{
    struct list_head            input_queue;
    ipcDataBlock_t*             cur_block;
    unsigned                    nr_bytes_pending;

    wait_queue_head_t           wq;
    struct ipc_usb_data_t *     usb_data;
    spinlock_t                  lock;
    UINT8                       inUse; 
    ipcPortId_t                 portId;

    struct task_struct*         output_pid;
    struct task_struct*         input_pid;
};

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
static int ipc_usb_probe(struct usb_interface* intf, 
        const struct usb_device_id* id);
static void ipc_usb_disconnect(struct usb_interface* intf);
static void ipc_usb_send_complete(struct urb* urb, struct pt_regs* regs);
static void ipc_usb_recv_complete(struct urb* urb, struct pt_regs* regs);

static void ipc_usb_delete(struct kref* kref);
static int append_input_data(struct ipc_usb_data_t* usb_data, char* data, 
        u32 length);
static int submit_read_urb(struct ipc_usb_data_t* usb_data);
static void on_data_arrival(void* arg);

static INT32 ipcUsbRead(void* handle, UINT8* data, INT32 length);
static INT32 ipcUsbWrite(void* handle, const UINT8* data, INT32 length);
static void ipcUsbStop(void* handle);
static void ipcUsbClose(void* handle);
BOOL ipcUsbOpen(ipcPortId_t portId, UINT32 bandwidth, UINT32 framerate);
/*------------------------------------- STATIC DATA ------------------------------------*/
static ipcUsbDevice_t usbDevices[IPC_MAX_PORTS];
static unsigned numDevicesInUse = 0;

static struct usb_device_id ipc_dev_table[] = 
{
    {USB_DEVICE(IDEN_IPC_VID, IDEN_IPC_PID)},
    {},
};

static struct usb_driver ipc_usb_driver = 
{
    .owner = THIS_MODULE,
    .name = "ipc_usb",
    .id_table = ipc_dev_table,
    .probe = ipc_usb_probe,
    .disconnect = ipc_usb_disconnect,
};
/*--------------------------------------- MACROS ---------------------------------------*/
#define IPC_USB_LOCK(ipc_dev)   spin_lock_irqsave(&(ipc_dev)->lock, flags)
#define IPC_USB_UNLOCK(ipc_dev) spin_unlock_irqrestore(&(ipc_dev)->lock, flags)

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_usb_probe
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Probe the USB device.
*   
*   This function is needed by the USB subsystem, when the USB core detects a
*   new device plugged into the system, it queries all the registered drivers
*   to get a driver for that device. This function probes the LPK usb device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int ipc_usb_probe(struct usb_interface *interface, 
        const struct usb_device_id *id)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    struct ipc_usb_data_t *data = NULL;
    struct usb_host_interface *iface_desc;
    struct usb_endpoint_descriptor *endpoint;
    size_t buffer_size;
    ipcUsbDevice_t* ipc_dev = NULL;
    int i;
    int retval = -ENOMEM;

    /* allocate memory for our device state and initialize it */
    data = kmalloc(sizeof(struct ipc_usb_data_t), GFP_KERNEL);
    if (data == NULL) 
    {
        err("Out of memory");
        goto error;
    }
    memset(data, 0x00, sizeof (*data));
    kref_init(&data->kref);
    data->device = usb_get_dev(interface_to_usbdev(interface));
    data->interface = interface;

    /* find an IPC device to use */
    for (i = 0; i < IPC_MAX_PORTS; i++)
    {
        if ((usbDevices[i].usb_data == NULL) && usbDevices[i].inUse)
        {
            ipc_dev = &usbDevices[i];
            usbDevices[i].usb_data = data;
            data->ipc_dev = ipc_dev;
            break;
        }
    }
    if (ipc_dev == NULL)
    {
        goto error;
    }

    /* set up the endpoint information */
    /* use only the first bulk-in and bulk-out endpoints */
    iface_desc = interface->cur_altsetting;
    printk("iden: iface_desc->desc.bNumEndpoints = %d\n",
            iface_desc->desc.bNumEndpoints);
    for (i = 0; i < iface_desc->desc.bNumEndpoints; ++i) 
    {
        endpoint = &iface_desc->endpoint[i].desc;

        if (!data->bulk_in_endpoint_addr &&
            (endpoint->bEndpointAddress & USB_DIR_IN) &&
            ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK)
                    == USB_ENDPOINT_XFER_BULK)) 
        {
            /* we found a bulk in endpoint */
            buffer_size = endpoint->wMaxPacketSize;
            data->bulk_in_size = buffer_size;
            data->bulk_in_endpoint_addr = endpoint->bEndpointAddress;
            data->bulk_in_buffer = kmalloc(buffer_size, GFP_KERNEL);
            if (NULL == data->bulk_in_buffer) 
            {
                err("Could not allocate bulk_in_buffer");
                goto error;
            }
        }

        if (!data->bulk_out_endpoint_addr &&
            !(endpoint->bEndpointAddress & USB_DIR_IN) &&
            ((endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK)
                    == USB_ENDPOINT_XFER_BULK)) 
        {
            /* we found a bulk out endpoint */
            data->bulk_out_endpoint_addr = endpoint->bEndpointAddress;
        }
    }
    if (!(data->bulk_in_endpoint_addr && data->bulk_out_endpoint_addr)) 
    {
        err("Could not find both bulk-in and bulk-out endpoints");
        goto error;
    }

    /* save our data pointer in this interface device */
    usb_set_intfdata(interface, data);

    /* submit a read request */
    submit_read_urb(data);

    return 0;

error:
    if (data)
    {
        kref_put(&data->kref, ipc_usb_delete);
    }
    if (ipc_dev != NULL)
    {
        ipc_dev->usb_data = NULL;
    }
    return retval;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_usb_disconnect
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Called when the USB core discovers our device being pulled out.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipc_usb_disconnect(struct usb_interface* interface)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    struct ipc_usb_data_t* usb_data = NULL;
    ipcUsbDevice_t* ipc_dev = NULL;
    u32 flags;

    usb_data = usb_get_intfdata(interface);
    usb_set_intfdata(interface, NULL);

    ipc_dev = usb_data->ipc_dev;

    IPC_USB_LOCK(ipc_dev);
    ipc_dev->usb_data = NULL;
    IPC_USB_UNLOCK(ipc_dev);

    kref_put(&usb_data->kref, ipc_usb_delete);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_usb_send_complete
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This callback function is executed by USB core when a URB is sent out or 
*   canceled.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipc_usb_send_complete(struct urb* urb, struct pt_regs* regs)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (urb->status && 
        !(urb->status == -ENOENT || 
          urb->status == -ECONNRESET ||
          urb->status == -ESHUTDOWN)) {
        dbg("%s - nonzero write bulk status received: %d",
            __FUNCTION__, urb->status);
    }

    /* free up our allocated buffer */
    usb_buffer_free(urb->dev, urb->transfer_buffer_length, 
            urb->transfer_buffer, urb->transfer_dma);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_usb_recv_complete
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This callback function is executed when a URB is received by USB core.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipc_usb_recv_complete(struct urb* urb, struct pt_regs* regs)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    static struct work_struct work;
    INIT_WORK(&work, on_data_arrival, urb);
    /* defer the task to a work queue, in order to run in process context */
    schedule_work(&work);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_usb_delete
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Delete the local data for a device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipc_usb_delete(struct kref* kref)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    struct ipc_usb_data_t* data = 
        container_of(kref, struct ipc_usb_data_t, kref);
    usb_put_dev(data->device);
    if (data->bulk_in_buffer != NULL)
    {
        kfree(data->bulk_in_buffer);
    }
    kfree(data);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: append_input_data
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Append a data block into the input queue of a usb device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int append_input_data(struct ipc_usb_data_t* usb_data, char* data, u32 length)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDataBlock_t *block = NULL;
    u32 flags;

    if (usb_data->ipc_dev->nr_bytes_pending + length >= INPUT_QUEUE_LIMIT)
    {
        err("Too much data pending in port %u\n",
                usb_data->ipc_dev->portId);
        return -ENOMEM;
    }

    block = kmalloc(sizeof(ipcDataBlock_t), GFP_KERNEL);
    if (block == NULL)
    {
        err("Can't allocate data block for incoming data\n");
        return -ENOMEM;
    }
    block->buf = kmalloc(length, GFP_KERNEL);
    if (block->buf == NULL)
    {
        err("Can't reserve space for incoming data\n");
        kfree(block);
        return -ENOMEM;
    }
    memcpy(block->buf, usb_data->bulk_in_buffer, length);
    block->cur_ptr = block->buf;
    block->buflen = length;
    
    IPC_USB_LOCK(usb_data->ipc_dev);
    list_add_tail(&block->list, &usb_data->ipc_dev->input_queue);
    usb_data->ipc_dev->nr_bytes_pending += length;
    IPC_USB_UNLOCK(usb_data->ipc_dev);
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: submit_read_urb
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Submit a read request.
*   
*   NOTE: This function is assumed to be executing in process-context, i.e., it
*   can be blocked, which is necessary by flag GFP_KERNEL.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int submit_read_urb(struct ipc_usb_data_t* usb_data)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (usb_data->bulk_in_urb == NULL)
    {
        usb_data->bulk_in_urb = usb_alloc_urb(0, GFP_KERNEL);
    }
    usb_fill_bulk_urb(usb_data->bulk_in_urb,
            usb_data->device,
            usb_rcvbulkpipe(usb_data->device, usb_data->bulk_in_endpoint_addr),
            usb_data->bulk_in_buffer,
            usb_data->bulk_in_size,
            &ipc_usb_recv_complete,
            usb_data);
    return usb_submit_urb(usb_data->bulk_in_urb, GFP_KERNEL);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: on_data_arrival
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   A callback function which is called when data arrives.
*   NOTE: The flag GFP_KERNEL requires the function to be executing in
*   process-context.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void on_data_arrival(void* arg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    struct urb* urb = (struct urb*)arg;
    struct ipc_usb_data_t* usb_data = (struct ipc_usb_data_t*)urb->context;
    append_input_data(usb_data, urb->transfer_buffer, urb->actual_length);
    /* resubmit the read request */
    usb_submit_urb(urb, GFP_KERNEL);
    wake_up_all(&usb_data->ipc_dev->wq);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUsbRead
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from the USB device.
*   This is an interface to the device layer.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
* 13Jul2007  Motorola Inc.         Removed debug code
*
*****************************************************************************************/
INT32 ipcUsbRead(void* handle, UINT8* data, INT32 length)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDataBlock_t* block = NULL;
    ipcUsbDevice_t* ipc_dev = (ipcUsbDevice_t*)handle;
    struct list_head* list;
    u32 flags;
    INT32 ret = 0, bytes_left = 0;

    if (-ERESTARTSYS == wait_event_interruptible(ipc_dev->wq,
                !list_empty(&ipc_dev->input_queue)))
    {
        return 0;
    }

    /* maybe the device gets closed */
    if (list_empty(&ipc_dev->input_queue))
    {
        return 0;
    }

    IPC_USB_LOCK(ipc_dev);
    if (ipc_dev->cur_block == NULL)
    {
        list = ipc_dev->input_queue.next;
        if (list != NULL)
        {
            list_del(list);
            ipc_dev->cur_block = list_entry(list, ipcDataBlock_t, list);
        }
    }
    if (ipc_dev->cur_block == NULL)
    {
        err("What heppend while I try to get a block?\n");
        IPC_USB_UNLOCK(ipc_dev);
        return ret;
    }
    IPC_USB_UNLOCK(ipc_dev);

    block = ipc_dev->cur_block;

    bytes_left = block->buflen - (block->cur_ptr - block->buf);
    ret = min(bytes_left, length);

    memcpy(data, block->cur_ptr, ret);

    bytes_left -= ret;

    if (bytes_left == 0)
    {
        IPC_USB_LOCK(ipc_dev);
        ipc_dev->nr_bytes_pending -= block->buflen;
        ipc_dev->cur_block = NULL;
        IPC_USB_UNLOCK(ipc_dev);

        kfree(block->buf);
        kfree(block);
    }
    else
    {
        IPC_USB_LOCK(ipc_dev);
        block->cur_ptr += ret;
        IPC_USB_UNLOCK(ipc_dev);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUsbWrite
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Write data to the USB device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
INT32 ipcUsbWrite(void* handle, const UINT8* data, INT32 length)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    struct urb* urb = NULL;
    void* buf = NULL;
    ipcUsbDevice_t* ipc_dev = (ipcUsbDevice_t*)handle;
    struct ipc_usb_data_t* usb_data = ipc_dev->usb_data;

    if (usb_data == NULL)
    {
        ipcWarning(IPCLOG_DEVICE, ("USB device not plugged in\n"));
        return 0;
    }
    urb = usb_alloc_urb(0, GFP_KERNEL);
    if (urb == NULL)
    {
        err("Can't allocate URB\n");
        return 0;
    }

    buf = usb_buffer_alloc(usb_data->device, length, GFP_KERNEL, 
            &urb->transfer_dma);
    if (buf == NULL)
    {
        err("Can't allocate memory for urb buffer\n");
        return 0;
    }

    memcpy(buf, data, length);

    usb_fill_bulk_urb(urb, usb_data->device,
            usb_sndbulkpipe(usb_data->device, usb_data->bulk_out_endpoint_addr),
            buf, length, &ipc_usb_send_complete, ipc_dev->usb_data);
    usb_submit_urb(urb, GFP_KERNEL);

    return length;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUsbStop
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Stop the USB device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation
* 13Mar2006  Motorola Inc.         Change kill_proc to force_sig 
*
*****************************************************************************************/
void ipcUsbStop(void* handle)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcUsbDevice_t* ipc_dev = (ipcUsbDevice_t*)handle;
    ipcDevice_t* device = NULL;

    device = ipcGetDevice(ipc_dev->portId);
    if (device != NULL)
    {
        device->driver.read = NULL;
        device->driver.write = NULL;
    }
    if (ipc_dev->usb_data != NULL)
    {
        usb_kill_urb(ipc_dev->usb_data->bulk_in_urb);
    }

    if (! IS_ERR(ipc_dev->input_pid))
    {
        force_sig(SIGTERM, ipc_dev->input_pid);
    }
    wake_up_all(&ipc_dev->wq);
    ipc_dev->inUse = FALSE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUsbClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Close a USB device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcUsbClose(void* handle)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcUsbDevice_t* ipc_dev = (ipcUsbDevice_t*)handle;

    if (ipc_dev->inUse)
    {
        ipcUsbStop(handle);
    }
#if 0
    if (ipc_dev->usb_data != NULL)
    {
        ipc_usb_disconnect(ipc_dev->usb_data->interface);
    }
#endif
    if (--numDevicesInUse == 0)
    {
        ipcDebugLog(IPCLOG_DEVICE, ("Calling usb_deregister(&ipc_usb_driver)\n"));
        usb_deregister(&ipc_usb_driver);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUsbOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Open a USB device.
*   NOTE: This function should be called before the USB device being plugged
*   into the system.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jun2005  Motorola Inc.         Initial Creation
* 13Mar2006  Motorola Inc.         Change kernel_thread to kthread_run 
*
*****************************************************************************************/
BOOL ipcUsbOpen(ipcPortId_t portId, UINT32 bandwidth, UINT32 framerate)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcUsbDevice_t* ipc_dev;
    int i = 0;
    ipcDeviceDriver_t usb_driver;

    if (numDevicesInUse == 0)
    {
    /* initialize the array of usb devices */
        for (i = 0; i < IPC_MAX_PORTS; i++)
        {
            ipc_dev = &usbDevices[i];
            memset(ipc_dev, 0, sizeof(*ipc_dev));
            INIT_LIST_HEAD(&ipc_dev->input_queue);
            init_waitqueue_head(&ipc_dev->wq);
            spin_lock_init(&ipc_dev->lock);
        }
    /* register the usb driver */
        usb_register(&ipc_usb_driver);
    }
    /* for the specified usb driver, mark it as in use */
    ipc_dev = &usbDevices[(int)portId];

    usb_driver.handle = ipc_dev;
    usb_driver.read = ipcUsbRead;
    usb_driver.write = ipcUsbWrite;
    usb_driver.stop = ipcUsbStop;
    usb_driver.close = ipcUsbClose;

    if (!ipcCreateDevice(portId, IPC_DEVICE_USB, bandwidth, framerate, &usb_driver))
    {
        return FALSE;
    }

    ipc_dev->portId = portId;
    ipc_dev->inUse = 1;
    ++numDevicesInUse;

    ipc_dev->output_pid = kthread_run((int(*)(void*))&ipcDeviceOutputThread, 
            (void*)(int)ipc_dev->portId, "ipc_usb%d_out", ipc_dev->portId);
    if (ipc_dev->output_pid < 0)
    {
        ipcError(IPCLOG_DEVICE, ("kthread_run failed to create ipc_usb output task\n"));
        ipcCloseDevice(portId);
        return FALSE;
    }
    ipc_dev->input_pid = kthread_run((int(*)(void*))&ipcDeviceInputThread, 
            (void*)(int)ipc_dev->portId, "ipc_usb%d_in", ipc_dev->portId);
    if (ipc_dev->input_pid < 0)
    {
        ipcError(IPCLOG_DEVICE, ("kthread_run failed to create ipc_usb input task\n"));
        ipcCloseDevice(portId);
        return FALSE;
    }
    return TRUE;
}

