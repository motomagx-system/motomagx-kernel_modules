/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_session.c
*   FUNCTION NAME(S): RTSSDataIndication
*                     RTSSLinkDown
*                     RTSSReady
*                     SSSSAuthReply
*                     SSSSAuthRequest
*                     SSSSConfigRequest
*                     SSSSConfigUpdIndication
*                     SSSSLinkDown
*                     ipcEnterLocalReadyState
*                     ipcEnterNetworkReadyState
*                     ipcEnterRouterReadyState
*                     ipcHandleClientAuthReq
*                     ipcHandleClientConfigReq
*                     ipcHandleClientConfigReqTimeout
*                     ipcHandleClientLinkDown
*                     ipcHandleServerAuthReply
*                     ipcHandleServerConfigReqTimeout
*                     ipcHandleServerConfigUpdInd
*                     ipcHandleServerDrtBroadcastTimeout
*                     ipcHandleSessionControlMessage
*                     ipcHandleSessionDataMessage
*                     ipcIsMsgValidInCurrState
*                     ipcSendPacketToLocalSockets
*                     ipcSessionBroadcast
*                     ipcSessionDestroy
*                     ipcSessionInit
*                     ipcSessionRemoveAddressRange
*                     ipcSetUserInterfaceCallback
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 30Jun2004  Motorola Inc.         Add handle_cpss_status_req, SSCPStatusReply, handle_cpss_sw_ver_req, SSCPSWVersionReply,
* 11Jul2004  Motorola Inc.         IPC integreation restructure,
* 28Dec2004  Motorola Inc.         Simplified Session state machine
* 17Feb2005  Motorola Inc.         Updated states so IPC Stack will work locally before RTSSReady
* 25Feb2005  Motorola Inc.         Removed sanity check and added RTSSLinkDown and SSSSLinkDown
* 30Aug2005  Motorola Inc.         Bugfix deliver packets to multicast  sockets' queue
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 21Nov2006  Motorola Inc.         Added DSM message discard
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 12Apr2007  Motorola Inc.         fix checksum issue
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 13Jul2007  Motorola Inc.         Removed debug code
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_common.h"
#include "ipc_session.h"
#include "ipc_router_interface.h"
#include "ipc_defs.h"
#include "ipc_packet_log_internal.h"
#include "ipc_macro_defs.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/
#define SESSION_MAX_MSG_NUMBERS    1024

/*------------------------------------ ENUMERATIONS ------------------------------------*/

/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
ipcSession_t ipcSession_glb;
#ifdef START_UP_TIME
    extern unsigned long ulStartUpTime;
#endif
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
int SSSSAuthRequest(void); 
int SSSSAuthReply(ipcAddress_t ipc_addr, int auth_status);
int SSSSConfigRequest(void); 
int SSSSConfigUpdIndication(void);
int SSSSLinkDown(ipcAddress_t lowAddr, ipcAddress_t highAddr);   
void ipcSessionRemoveAddressRange(ipcAddress_t lowAddr, ipcAddress_t highAddr);

/* handler functions for session messages */
void ipcHandleClientAuthReq(SSSSAuthRequestMsg_t *packet);
void ipcHandleServerAuthReply(SSSSAuthReplyMsg_t *packet);
void ipcHandleClientConfigReq(SSSSConfigRequestMsg_t *packet);
void ipcHandleServerConfigUpdInd(SSSSConfigUpdIndicationMsg_t *packet);
void ipcHandleClientLinkDown(SSSSLinkDownMsg_t *packet);

/* handler functions for session timers */
void ipcHandleClientConfigReqTimeout(void *obj);
void ipcHandleServerConfigReqTimeout(void *obj);
void ipcHandleServerDrtBroadcastTimeout(void *obj);

/* enter state functions */
void ipcEnterLocalReadyState(void);
void ipcEnterRouterReadyState(void);
void ipcEnterNetworkReadyState(void);

/* IPC packet handler functions */
void ipcHandleSessionControlMessage(ipcPacket_t *packet);
int ipcHandleSessionDataMessage(ipcPacket_t *packet);


/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSessionInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to initialize IPC stack including create message queue,
*   launch session daemon thread, initialize internal data struct etc.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN:  void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Oct2004  Motorola Inc.         Initial creation
* 18Nov2004  Motorola Inc.         Data structure change
* 28Dec2004  Motorola Inc.         State machine simplified
* 17Feb2005  Motorola Inc.         Updated states so IPC Stack will work locally before RTSSReady
* 22Aug2005  Motorola Inc.         Checked fault condition
* 16Aug2005  Motorola Inc.         Only sleep if new DRT item fails 
* 19Mar2007  Motorola Inc.         Initialize ipcSession_glb.serviceNotifications
* 13Jul2007  Motorola Inc.         Removed debug code
*
*****************************************************************************************/
BOOL ipcSessionInit(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSession_glb.state = IPC_SESSION_STATE_NULL;
    ipcSession_glb.update_user_interface = NULL;
    ipcSession_glb.serviceNotifications = NULL;

    while (TRUE)
    {
        /* init DRT */
        ipcListInit(&ipcSession_glb.drt);

        /* init unbound socket list */
        ipcListInit(&ipcSession_glb.unboundSockList);

        /* init IPC Address to DRT Item and IPC Address to Node ID tables */
        memset(ipcSession_glb.ipcAddrToDrtItemLookup, 0, sizeof(ipcSession_glb.ipcAddrToDrtItemLookup));
        memset(ipcSession_glb.ipcAddrToNodeId, 0, sizeof(ipcSession_glb.ipcAddrToNodeId));

        /* init node ID to IPC Address table */
        if (!ipcHashInit(&ipcSession_glb.nodeIdToIpcAddrTable, 
                    HASH_SIZE_FOR_NODE_ID_TABLE, NULL))
        {
            break;
        }

        /* init local_srvid_tab */
        if (!ipcHashInit(&ipcSession_glb.local_srvid_tab, 
                    HASH_SIZE_FOR_SRVID_TABLE, NULL))
        {
            break;
        }

        /* init locks */
        if (ipcOsalMutexOpen(&ipcSession_glb.lockDRTTable) != IPCE_OK)
        {
            break;
        }

        if (ipcOsalMutexOpen(&ipcSession_glb.lockSrvIDTable) != IPCE_OK)
        {
            break;
        }
        /* init timers */
        if (ipcStack_glb.isServer)
        {
            /* server session */
            if (ipcOsalTimerOpen(&ipcSession_glb.configReqTimer) != IPCE_OK)
            {
                break;
            }

            if (ipcOsalTimerInit(ipcSession_glb.configReqTimer,
                        TIMEOUT_SERVER_CONFIG_REQ,
                        ipcHandleServerConfigReqTimeout, NULL) != IPCE_OK)
            {
                break;
            }

            if (ipcOsalTimerOpen(&ipcSession_glb.drtBroadcastTimer) != IPCE_OK)
            {
                break;
            }

            if (ipcOsalTimerInit(ipcSession_glb.drtBroadcastTimer,
                        TIMEOUT_SERVER_DRT_BROADCAST,
                        ipcHandleServerDrtBroadcastTimeout, NULL) != IPCE_OK)
            {
                break;
            }
        }
        else
        {
            /* client session */
            if (ipcOsalTimerOpen(&ipcSession_glb.configReqTimer) != IPCE_OK)
            {
                break;
            }
            if (ipcOsalTimerInit(ipcSession_glb.configReqTimer, 
                        TIMEOUT_CLIENT_CONFIG_REQ,
                        ipcHandleClientConfigReqTimeout, NULL) != IPCE_OK)
            {
                break;
            }
        }

        /* init local DRT Item */
        while ((ipcSession_glb.localDrtItem = ipcNewDRTItem()) == NULL)
        {
            if (ipcOsalSleep(50) < 0)
            {
                ipcError(IPCLOG_SESSION, 
                        ("ipcSessionInit: interrupted!\n"));
                break;
            }
        }
        if (ipcSession_glb.localDrtItem == NULL)
        {
            break;
        }

        ipcSession_glb.localDrtItem->datatype = ipcStack_glb.datatype;
        ipcSession_glb.localDrtItem->ipcaddr = ipcStack_glb.ipcAddr;
        ipcSession_glb.localDrtItem->node_id = ipcStack_glb.nodeId;
        
        /* add local config to DRT (it will always be the first item in the DRT) */
        if (LOCK(DRTTable) != IPCE_OK)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSessionInit: Failed to lock DRT table\n"));
            break;
        }
        ipcListAddHead(&ipcSession_glb.drt, (ipcListLink_t)ipcSession_glb.localDrtItem);
        ipcAddDrtItemToNodeIdTable(ipcSession_glb.localDrtItem);
        ipcSession_glb.ipcAddrToDrtItemLookup[ipcStack_glb.ipcAddr] = ipcSession_glb.localDrtItem;
        ipcSession_glb.ipcAddrToNodeId[ipcStack_glb.ipcAddr] = ipcStack_glb.nodeId;
        UNLOCK(DRTTable);
        
        /* set session's state */
        if (ipcStack_glb.isServer)
        {
            ipcEnterNetworkReadyState();
        }
        else
        {
            ipcEnterLocalReadyState();
        }
        ipcSession_glb.connOps.deliverData = ipcCOSocketDataDeliver;
        ipcSession_glb.connOps.lock = ipcCOSocketLock;
        ipcSession_glb.connOps.onDataArrival = ipcCOSocketDataArrival;
        ipcSession_glb.connOps.onMaxRetries = ipcCOSocketMaxRetries;
        ipcSession_glb.connOps.onSentListEmpty = ipcCOSocketSentListEmpty;
        ipcSession_glb.connOps.onTxWindowAvailable = ipcCOSocketTxWindowAvailable;
        ipcSession_glb.connOps.onTxWindowFull = ipcCOSocketTxWindowFull;
        ipcSession_glb.connOps.unlock = ipcCOSocketUnlock;

        return TRUE;
    }
    /* Error condition, close all the data structures */
    /* No locks held now, we don't have to lock them */
    ipcSessionDestroy();
    return FALSE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSessionDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to clean up IPC session layer environment.
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: Void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 05Jan2005  Motorola Inc.         State machine simplified
* 14Jan2005  Motorola Inc.         Added code to destroy mutex
* 24Jan2005  Motorola Inc.         Added code to free comptable
* 13Oct2005  Motorola Inc.         When closing whole stack, the sockets not closed should be destroyed.
* 13Jul2007  Motorola Inc.         Removed debug code
*
*****************************************************************************************/
void ipcSessionDestroy(void) 
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* sock = NULL;
    ipcSocket_t* tmpSock = NULL;
    ipcPacket_t* packet = NULL;
    ipcServiceNotification_t* srvNot = NULL;
    ipcNotification_t* notification = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcDebugLog(IPCLOG_SESSION, ("IPC SESSION DESTROY\n"));
    
    /*
     * XXX: Here we may fail to lock these tables, don't handle these situations! 
     */
    LOCK(SrvIDTable);
    LOCK(DRTTable);

    /* set state */
    ipcSession_glb.state = IPC_SESSION_STATE_NULL;

    /* close session tiemrs */
    if (ipcStack_glb.isServer)
    {
        /* server session */
        ipcOsalTimerClose(ipcSession_glb.configReqTimer);
        ipcOsalTimerClose(ipcSession_glb.drtBroadcastTimer);
    }
    else
    {
        /* client session */
        ipcOsalTimerClose(ipcSession_glb.configReqTimer);
    }

    /* release all unbound sockets */
    ipcListFreeAll(&ipcSession_glb.unboundSockList);

    /* release all bound sockets as well as local_srvid_tab */
    ipcHashResetIterator(&ipcSession_glb.local_srvid_tab);
    while (NULL != (sock = (ipcSocket_t*)ipcHashGetNext(&ipcSession_glb.local_srvid_tab)))
    {
        while (sock != NULL)
        {
            /* if the socket is CO, we need to free all resources */
            if (sock->isCOSocket)
            {
                ipcConnUndelegate(&sock->conn);

                ipcConnDestroy(&sock->conn, TRUE, TRUE, FALSE);
                ipcOsalSemaClose(sock->coSema);
            }
            tmpSock = sock->next;
            
            /* free all packet left in this socket */
            if (sock->packetQueue.currentSize > 0)
            {
                if (ipcIsMulticastServiceId(sock->srvId))
                {
                    ipcListLink_t listLink = NULL;

                    while (NULL != (listLink = ipcListRemoveHead(&sock->packetQueue.highPriorityList)))
                    {
                        packet = (ipcPacket_t*)listLink->data;
                        ipcFree(packet);
                        ipcFree(listLink);
                    }
                    while (NULL != (listLink = ipcListRemoveHead(&sock->packetQueue.lowPriorityList)))
                    {
                        packet = (ipcPacket_t*)listLink->data;
                        ipcFree(packet);
                        ipcFree(listLink);
                    }
                }
                else
                {
                    ipcListFreeAll(&sock->packetQueue.highPriorityList);
                    ipcListFreeAll(&sock->packetQueue.lowPriorityList);
                }
                sock->packetQueue.currentSize = 0;
            }
            /* close resources in the socket recv queue */
            ipcOsalSemaClose(sock->packetQueue.notEmptySema);
            ipcOsalMutexClose(sock->packetQueue.mutex);
            /* remove notifications in the socket */
            while (sock->notifications != NULL)
            {
                notification = sock->notifications;
                sock->notifications = notification->next;
                ipcFree(notification);
            }

            ipcFree(sock);
            sock = tmpSock;
        }
    }
    ipcHashDestroy(&ipcSession_glb.local_srvid_tab);

    /* free DRT */
    ipcFreeDRT(&ipcSession_glb.drt);
    memset(ipcSession_glb.ipcAddrToDrtItemLookup, 0, sizeof(ipcSession_glb.ipcAddrToDrtItemLookup));
    memset(ipcSession_glb.ipcAddrToNodeId, 0, sizeof(ipcSession_glb.ipcAddrToNodeId));

    /* free service notifications */
    while (ipcSession_glb.serviceNotifications != NULL)
    {
        srvNot = ipcSession_glb.serviceNotifications;
        ipcSession_glb.serviceNotifications = (ipcServiceNotification_t*)srvNot->notification.next;
        ipcFree(srvNot);
    }

    /* free Node ID Table */
    ipcHashDestroy(&ipcSession_glb.nodeIdToIpcAddrTable);

    /* close mutexes */
    ipcOsalMutexClose(ipcSession_glb.lockDRTTable);
    ipcOsalMutexClose(ipcSession_glb.lockSrvIDTable);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSetUserInterfaceCallback
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to set the callback function used to update the
*   user interface
*   
*   PARAMETER(S):
*       fn: function to call when user interface needs to be updated
*       
*   RETURN:  void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jul2005  Motorola Inc.         Initial creation
*
*****************************************************************************************/
void ipcSetUserInterfaceCallback(void (*fn)(void))
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSession_glb.update_user_interface = fn;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleSessionControlMessage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 25Jan2005  Motorola Inc.         Shared memory debug
* 15Mar2005  Motorola Inc.         Unify handling of opcodes for  CO sockets and state machine
* 16Jan2007  Motorola Inc.         Added debug messages 
*
*****************************************************************************************/
void ipcHandleSessionControlMessage(ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT8 opcode;
    BOOL needFree = TRUE;
/*--------------------------------------- CODE -----------------------------------------*/

    opcode = ipcGetMsgOpcode(packet);
    if (!ipcIsMsgValidInCurrState(opcode))
    {
        /* Not a valid message in the current state */
        ipcError(IPCLOG_SESSION, ("recv invalid msg=%02d in current state=%02d\n",
            ipcGetMsgOpcode(packet), 
            ipcSession_glb.state));
        ipcFree(packet);
        return;
    }

    switch (opcode)
    {
    /* Connection-oriented socket messages */
    case IPC_MSG_COCO_CONN_REQ:
        ipcDebugLog(IPCLOG_SESSION,
                    ("ipcHandleSessionControlMessage: Got IPC_MSG_COCO_CONN_REQ\n"));
        ipcHandleCOCOConnReq((COCOConnRequestMsg_t*)packet);
        /* XXX: Don't free here, ipcHandleCOCOConnReq takes care of the packet */
        needFree = FALSE;
        break;

    case IPC_MSG_COCO_CONN_REP:
        ipcDebugLog(IPCLOG_SESSION,
                    ("ipcHandleSessionControlMessage: Got IPC_MSG_COCO_CONN_REP\n"));
        ipcHandleCOCOConnRep((COCOConnReplyMsg_t*)packet);
        break;

    case IPC_MSG_COCO_CONN_COMP:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_COCO_CONN_COMP\n"));
        ipcHandleCOCOConnComp((COCOConnCompleteMsg_t*)packet);
        /* XXX: Don't free here, ipcHandleCOCOConnComp takes care of the packet */
        needFree = FALSE;
        break;

    case IPC_MSG_COCO_CONN_DOWN:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_COCO_CONN_DOWN\n"));
        ipcHandleCOCOConnDown((COCOConnDownMsg_t*)packet);
        break;

    case IPC_MSG_SSSS_NOTIFY_REG:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_NOTIFY_REG\n"));
        ipcHandleNotifyRegister((SSSSNotifyRegisterMsg_t*)packet);
        break;
        
    case IPC_MSG_SSSS_NOTIFY_SIGNAL:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_NOTIFY_SIGNAL\n"));
        ipcHandleNotifySignal((SSSSNotifySignalMsg_t*)packet);
        break;

    /* state machine code */
    case IPC_MSG_SSSS_CLIENT_AUTH_REQ:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_CLIENT_AUTH_REQ\n"));
        ipcHandleClientAuthReq((SSSSAuthRequestMsg_t *)packet);
        break;

    case IPC_MSG_SSSS_CLIENT_CONFIG_REQ:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_CLIENT_CONFIG_REQ\n"));
        ipcHandleClientConfigReq((SSSSConfigRequestMsg_t *)packet);
        break; 
        
    case IPC_MSG_SSSS_CLIENT_LINK_DOWN:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_CLIENT_LINK_DOWN\n"));
        ipcHandleClientLinkDown((SSSSLinkDownMsg_t *)packet);
        break;

    case IPC_MSG_SSSS_SERVER_AUTH_REPLY:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_SERVER_AUTH_REPLY\n"));
        ipcHandleServerAuthReply((SSSSAuthReplyMsg_t *)packet);
        break;

    case IPC_MSG_SSSS_SERVER_CONFIG_UPD_INDICATION:
        ipcDebugLog(IPCLOG_SESSION,
                ("ipcHandleSessionControlMessage: Got IPC_MSG_SSSS_SERVER_CONFIG_UPD_INDICATION\n"));
        ipcHandleServerConfigUpdInd((SSSSConfigUpdIndicationMsg_t *)packet);
        break;

    default:
        ipcError(IPCLOG_SESSION, ("ipcHandleSessionControlMessage -> invalid message code!\n"));
        break;
    }

    if (needFree)
    {
        ipcFree(packet);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleSessionDataMessage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle data message from device layer.
*   
*   PARAMETER(S):
*       packet: pointer to the data packet received from router layer
*       
*   RETURN: Void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 15Nov2004  Motorola Inc.         Rewrite
* 14Jan2005  Motorola Inc.         Use ipcPacketQueue
* 25Jan2005  Motorola Inc.         Shared memory debug
* 19Oct2005  Motorola Inc.         Don't free memory for local msg
* 28Oct2005  Motorola Inc.         Add parameter check
* 03Nov2005  Motorola Inc.         Return value type changed to int 
*
*****************************************************************************************/
int ipcHandleSessionDataMessage(ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (packet == NULL)
    {
        ipcError(IPCLOG_SESSION, ("ipcHandleSessionDataMessage: null packet!\n"));
        return IPCEFAULT;
    }
    return ipcSendPacketToLocalSockets(packet);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSSSAuthRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to send SSSSAuthRequest to server session.
*   
*   PARAMETER(S):
*       NONE
*       
*   RETURN: int
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jan2005  Motorola Inc.         Rewrite
* 26Jan2006  Motorola Inc.         Add datatype into SSSSAuthReq msg
*
*****************************************************************************************/
int SSSSAuthRequest(void)
{ 
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSAuthRequestMsg_t *packet = NULL;
    int ret = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    /* create ipc packet 
    * +---------------+------------+-----------+----------+
    * | IPC header    | msg opcode | node id   | datatype |
    * +---------------+------------+-----------+----------+
    *                      1 byte      4 bytes     1 byte
    */
    packet = (SSSSAuthRequestMsg_t *)ipcCreateSessionControlPacket(
        SSSS_AUTH_REQUEST_DATA_SIZE,
        IPC_MSG_SSSS_CLIENT_AUTH_REQ,
        IPC_SERVER_ADDR);

    if (packet == NULL)
    {
        ret =  IPCENOBUFS;
    }
    else
    {
        /* write node id to ipc packet */
        ipcOsalWrite32(packet->nodeId, ipcStack_glb.nodeId);

        ipcOsalWrite8(packet->datatype, ipcStack_glb.datatype);
        
        ret = SSRTDataRequest((ipcPacket_t*)packet, IPC_CONTROL_CHANNEL_ID);
        
        if (ret != IPC_OK)
        {
            ipcFree(packet);
            ipcError(IPCLOG_SESSION, ("SSSSAuthRequest -> send SSSSAuthRequest failed! %d\n", ret));
        }
        else
        {
            ipcDebugLog(IPCLOG_SESSION, ("Client send SSSSAuthRequest() to server\n"));
        }
    }

    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSSSAuthReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to send SSSSAuthReply to client session.
*   
*   PARAMETER(S):
*       ipc_addr: The dest address this reply will be sent to
*       auth_status: the status sent to client.
*       
*   RETURN: int
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Rewrite 
*
*****************************************************************************************/
int SSSSAuthReply(ipcAddress_t ipc_addr, int auth_status)
{ 
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSAuthReplyMsg_t *packet = NULL;
    int ret = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    /* create a new ipc packet */
    packet = (SSSSAuthReplyMsg_t *)ipcCreateSessionControlPacket(
        SSSS_AUTH_REPLY_DATA_SIZE,
        IPC_MSG_SSSS_SERVER_AUTH_REPLY,
        ipc_addr);

    if (packet == NULL)
    {
        ret = IPCENOBUFS;
        ipcError(IPCLOG_SESSION, ("SSSSAuthReply -> out of memory!!!\n"));
    }
    else
    {
        /* SSSSAuthReply status */
        ipcOsalWrite8(packet->status, auth_status);
        
        /* send SSSSAuthReply */
        ret = SSRTDataRequest((ipcPacket_t *)packet, IPC_CONTROL_CHANNEL_ID);
        if (ret != IPC_OK)
        {
            ipcFree(packet);
            ipcError(IPCLOG_SESSION, ("ipcHandleClientAuthReq -> send SSSSAuthReply failed! %d\n", ret));
        }
        else
        {
            ipcDebugLog(IPCLOG_SESSION, ("Server send SSSSAuthReply() to client: %d\n", auth_status));
        }
    }
    
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSSSConfigRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to send SSSSConfigRequest to server session.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: int
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jan2005  Motorola Inc.         Rewrite
* 21Nov2006  Motorola Inc.         Updated SSSSConfigRequestMsg_t for DSM message discard
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int SSSSConfigRequest(void)
{   
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSConfigRequestMsg_t *packet;
    int ret = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("SSSSConfigRequest: Failed to lock DRT table!\n"));
        return IPCEBUSY;
    }
    packet = (SSSSConfigRequestMsg_t *)ipcCreateSessionControlPacket(
        (UINT16)(SSSS_CONFIG_REQUEST_MIN_SIZE + ipcDRTItemSize(ipcSession_glb.localDrtItem)),
        IPC_MSG_SSSS_CLIENT_CONFIG_REQ,
        IPC_SERVER_ADDR);

    if (packet == NULL)
    {
        UNLOCK(DRTTable);
        ret = IPCENOBUFS;
        ipcError(IPCLOG_SESSION, ("SSSSConfigRequest -> out of memory!!!\n"));
    }
    else
    {
        /* pack local DRT item to packet */
        ipcDRTItemToBuffer(ipcSession_glb.localDrtItem, &packet->drtItem);
        UNLOCK(DRTTable);
        
        /* send packet */
        ret = SSRTDataRequest((ipcPacket_t *)packet, IPC_CONTROL_CHANNEL_ID);
        if (ret != IPC_OK)
        {
            ipcFree(packet);
            ipcError(IPCLOG_SESSION, ("SSSSConfigRequest -> send failed! %d\n", ret));
        }
        else
        {
            ipcDebugLog(IPCLOG_SESSION, ("Client call SSSSConfigRequest() to send config request\n"));
        }
    }
    
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSSSConfigUpdIndication
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to send SSSSConfigUpdIndication to client session.
*   
*   PARAMETER(S):
*       NONE
*       
*   RETURN: int
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jan2005  Motorola Inc.         Rewrite
* 21Nov2006  Motorola Inc.         Updated SSSSConfigUpdIndicationMsg_t for DSM message discard
* 22Jan2007  Motorola Inc.         Unlock DRT before broadcast 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int SSSSConfigUpdIndication(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_OK;
    SSSSConfigUpdIndicationMsg_t *packet = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcSession_glb.drt.size == 1)
    {
        //ipcDebugLog(IPCLOG_SESSION, ("SSSSConfigUpdIndication -> DRT empty!\n"));
        return IPC_OK;
    }
    
    /* create the ipc packet */
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("SSSSConfigUpdIndication: Failed to lock DRT table!\n"));
        return IPCEBUSY;
    }

    packet = (SSSSConfigUpdIndicationMsg_t *)ipcCreateSessionControlPacket(
        (UINT16)(SSSS_CONFIG_UPDATE_INDICATION_MIN_SIZE + ipcCalDRTSize(&ipcSession_glb.drt)),
        IPC_MSG_SSSS_SERVER_CONFIG_UPD_INDICATION,
        IPC_INVALID_ADDR);
    /* We are broadcasting this packet so we can't fill in the address yet */

    if (packet == NULL)
    {
        UNLOCK(DRTTable);
        ipcError(IPCLOG_SESSION, ("SSSSConfigUpdIndication -> out of memory!!!\n"));
        return IPCENOBUFS;
    }
    
    ipcOsalWriteAligned16(packet->numDrtItems, ipcSession_glb.drt.size);
    
    /* encoding DRT to packet */
    ipcDRTToBuffer(&ipcSession_glb.drt, packet->drtData);

    UNLOCK(DRTTable);
    /* broadcast packet */
    ret = ipcSessionBroadcast((ipcPacket_t *)packet, 0);
    
    if (ret == IPC_OK)
    {
        ipcDebugLog(IPCLOG_SESSION, ("Server calls SSSSConfigUpdIndication() to broadcast DRT\n"));
    }
    else
    {
        ipcError(IPCLOG_SESSION, ("Server sends SSSSConfigUpdIndication failed! %d\n", ret));
    }

    ipcFree(packet);

    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSSSLinkDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to inform the server that a client link is down so
*   it can remove that range of addresses
*   
*   PARAMETER(S):
*       lowAddr: low address of range to remove from DRT
*       highAddr: high address of range to remove from DRT
*       
*   RETURN: IPC_OK - message was sent successfuly
*           IPCENOBUFS - out of memory
*           IPCENETUNREACH - link to server is down
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Feb2005  Motorola Inc.         Initial Creation
* 17Aug2005  Motorola Inc.         Check the pack value before using it 
*
*****************************************************************************************/
int SSSSLinkDown(ipcAddress_t lowAddr, ipcAddress_t highAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSLinkDownMsg_t *pack = NULL;
    int ret = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    pack = (SSSSLinkDownMsg_t *)ipcCreateSessionControlPacket(
        SSSS_LINK_DOWN_DATA_SIZE,
        IPC_MSG_SSSS_CLIENT_LINK_DOWN,
        IPC_SERVER_ADDR);
    
    if (pack == NULL)
    {
        ret = IPCENOBUFS;
        ipcError(IPCLOG_SESSION, ("SSSSLinkDown -> out of memory!!!\n"));
    }
    else
    {
        ipcOsalWrite8(pack->ipcAddressRangeLow, lowAddr);
        ipcOsalWrite8(pack->ipcAddressRangeHigh, highAddr);
        ret = SSRTDataRequest((ipcPacket_t *)pack, IPC_CONTROL_CHANNEL_ID);
        if (ret != IPC_OK)
        {
            ipcFree(pack);
            ipcError(IPCLOG_SESSION, ("SSSSLinkDown -> send failed! %d\n", ret));
        }
        else
        {
            ipcDebugLog(IPCLOG_SESSION, ("Call SSSSLinkDown() for range %d -> %d\n", lowAddr, highAddr));
        }
    }
    
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSendPacketToLocalSockets
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to send data packets to local sockets.
*   
*   PARAMETER(S):
*       packet: IPC packet in shared memory
*       
*   RETURN: IPC_OK - message was sent successfuly to at least one socket
*           IPCESOCKFULL - local socket was full (non-multicast only)
*           IPCESERVICE - service not found
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Nov2004  Motorola Inc.         Initial Version
* 14Jan2005  Motorola Inc.         Message queues changed
* 25Jan2005  Motorola Inc.         Shared memory debug
* 30Aug2005  Motorola Inc.         Bugfix delivering packets to  multicast sockets' queue
* 10Mar2006  Motorola Inc.         Return error code on failure
* 22Jan2007  Motorola Inc.         Added cache for putting data 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcSendPacketToLocalSockets(ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
    ipcSocket_t *socket = NULL;
    ipcSocket_t *nextSocket;
    ipcServiceId_t destService = 0;
    ipcList_t sockList;
/*--------------------------------------- CODE -----------------------------------------*/
    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcSendPacketToLocalSockets: Failed to lock SrvIDTable\n"));
        return IPCEBUSY;
    }

    destService = ipcGetDestServiceId(packet);
    socket = ipcFindSocketBySrvId(destService);
    if (socket == NULL)
    {
        UNLOCK(SrvIDTable);
        /* this service id is not supported in this node. */
        ipcWarning(IPCLOG_SESSION, ("ipcSendPacketToLocalSockets:srvId %d not supported\n", 
            destService));
        rc = IPCESERVICE;
    }
    else if (ipcIsMulticastServiceId(destService))
    {
        ipcListInit(&sockList);
        /* First loop through and backup multicast sockets */
        for (nextSocket = socket; nextSocket != NULL; nextSocket = nextSocket->next)
        {
            if (NULL == ipcListAddObjectToTail(&sockList, nextSocket))
            {
                ipcError(IPCLOG_SESSION,
                        ("ipcSendPacketToLocalSockets: Failed to alloc space for socket %p\n", socket));
            }
        }
        
        UNLOCK(SrvIDTable);
        ipcIncRefCount(packet, sockList.size);
          
        /* Now loop through and send the packet to each multicast socket */
        while (sockList.size > 0)
        {
            socket = ipcListRemoveObjectFromHead(&sockList);
            /* We must insert packets into multicast socket queues using a new list link since
             * they can be in more than one queue (built-in next pointer CANNOT be used) */
            rc = IPCEINVAL;
            if (ipcIsSocketValid(socket) && 
                (ipcLockSocket(socket) == IPC_OK))
            {
                ipcRefSocket(socket);
                ipcUnlockSocket(socket);
                rc = ipcPutPacketToSocket(socket, packet);
                if (ipcLockSocket(socket) == IPCE_OK)
                {
                    ipcUnrefSocket(socket);
                    ipcUnlockSocket(socket);
                }
                else
                {
                    ipcError(IPCLOG_SESSION,
                            ("ipcSendPacketToLocalSockets: Failed to lock socket %p\n", socket));
                    ipcUnrefSocket(socket);
                }
            }
            if (rc < 0)
            {
                ipcWarning(IPCLOG_SESSION,
                    ("Failed to put packet to multicast socket, srvId=%d, curlen=%d, numbytes=%lu\n",
                    socket->srvId, socket->packetQueue.currentSize, ipcGetMsgLength(packet)));
                ipcFree(packet);
            }
        }
        if (rc == 0)
        {
            ipcFree(packet);
        }
    }
    else /* not multicast socket */
    {
        UNLOCK(SrvIDTable);

        rc = IPCEBUSY;
        if (ipcIsSocketValid(socket) && (ipcLockSocket(socket) == IPC_OK))
        {
            ipcRefSocket(socket);
            ipcUnlockSocket(socket);
            rc = ipcPutPacketToSocket(socket, packet);
            if (ipcLockSocket(socket) == IPCE_OK)
            {
                ipcUnrefSocket(socket);
                ipcUnlockSocket(socket);
            }
            else
            {
                ipcError(IPCLOG_SESSION,
                        ("ipcSendPacketToLocalSockets: Failed to lock socket %p\n", socket));
                ipcUnrefSocket(socket);
            }
        }
        /* We can insert packets directly into non-multicast socket queues since they will
         * only be in one queue (built-in next pointer can be used) */
        if (rc < 0)
        {
            ipcWarning(IPCLOG_SESSION,
                ("Failed to put packet to socket, srvId=%d, curlen=%d, numbytes=%lu\n",
                 socket->srvId, socket->packetQueue.currentSize, ipcGetMsgLength(packet)));
        }
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleClientConfigReq
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle IPC_CLIENT_CONFIG_REQ from client session.
*   
*   PARAMETER(S):
*       packet: pointer to the session packet
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         State machine simplified
* 15Apr2005  Motorola Inc.         Check return value of ipcAlloc()
* 26Jul2005  Motorola Inc.         Check return value of ipcNewDRTItem
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 15Jan2007  Motorola Inc.         Cached all DRT changes 
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
*
*****************************************************************************************/
void ipcHandleClientConfigReq(SSSSConfigRequestMsg_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t *drt_item = NULL;
    ipcDRTItem_t *old_drt_item = NULL;
    ipcListLink_t prev;
    ipcDrtChangeSet_t changes;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDrtChangeSetInit(&changes);
    /* unpack the packet */
    drt_item = ipcNewDRTItem();
    if (drt_item == NULL)
    {
        ipcError(IPCLOG_SESSION, ("Can't allocate new DRT item\n"));
        return;
    }
    ipcBufferToDRTItem(&packet->drtItem, drt_item);
    
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleClientConfigReq: Failed to lock DRT table\n"));
        ipcFreeDRTItem(drt_item);
        return;
    }
    old_drt_item = findDrtItemByIpcAddr(&ipcSession_glb.drt, ipcGetSrcAddr(packet), &prev);
    
    if (NULL == old_drt_item)
    {
        UNLOCK(DRTTable);
        ipcError(IPCLOG_SESSION, ("ipcHandleClientConfigReq -> NO corresponding DRT item!\n"));
        ipcFreeDRTItem(drt_item);
        return;
    }
    
    if (drt_item->node_id != old_drt_item->node_id)
    {
        UNLOCK(DRTTable);
        ipcError(IPCLOG_SESSION, 
            ("ipcHandleClientConfigReq -> same ipc addr %d but diff node id %d? %d?!\n",
            ipcGetSrcAddr(packet), old_drt_item->node_id, drt_item->node_id));
        ipcFreeDRTItem(drt_item);
        return;
    }
    
    /* The item returned by ipcDrtUpdateItem is the obsolete one */
    old_drt_item = ipcDrtUpdateItem(old_drt_item, drt_item, &changes);
    if (old_drt_item != NULL)
    {
        ipcFreeDRTItem(old_drt_item);
    }
    UNLOCK(DRTTable);
    
    ipcDrtChangeSignal(&changes, TRUE);
    ipcDrtChangeFreeAll(&changes);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleServerConfigUpdInd
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle IPC_SERVER_CONFIG_UPD_INDICATION from server session.
*   
*   PARAMETER(S):
*       packet: pointer to the session packet
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Dec2004  Motorola Inc.         State machine simplified
* 15Jan2005  Motorola Inc.         Optimization of DRT
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 15Jan2007  Motorola Inc.         Cached all DRT changes 
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
*
*****************************************************************************************/
void ipcHandleServerConfigUpdInd(SSSSConfigUpdIndicationMsg_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcList_t drt_in_conf_upd_ind;
    ipcDRTItem_t *drt_item = NULL;
    ipcDRTItem_t* foreign_item = NULL;
    ipcDRTItem_t* my_item = NULL;
    ipcListLink_t prev;
    ipcListLink_t curr;
    ipcListLink_t temp_prev;
    ipcDrtChangeSet_t changes;
    BOOL localDrtSame = FALSE;
    ipcDRTListItem_t* oldOne = NULL;
    ipcDRTListItem_t* newOne = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDrtChangeSetInit(&changes);
    if (ipcSession_glb.state != IPC_SESSION_STATE_NETWORK_READY)
    {
        ipcError(IPCLOG_SESSION, ("ipcHandleServerConfigUpdInd -> recv ConfUpdInd not in N_READY\n"));
        return;
    }
    
    ipcListInit(&drt_in_conf_upd_ind);
    
    if (ipcSession_glb.drt.head == NULL)
    {
        //ipcError(IPCLOG_SESSION, ("ipcHandleServerConfigUpdInd -> local DRT empty!!!\n"));
        return;
    }
    
    ipcDebugLog(IPCLOG_SESSION, ("Client got config_upd_indication\n"));
    
    /* unpack SSSSConfigUpdateIndication */
    ipcBufferToDRT(packet->drtData, &drt_in_conf_upd_ind, ipcOsalReadAligned16(packet->numDrtItems));
    
    if (LOCK(DRTTable) < 0)
    {
        ipcFreeDRT(&drt_in_conf_upd_ind);
        ipcError(IPCLOG_SESSION,
                ("ipcHandleServerConfigUpdInd: Failed to lock DRT table\n"));
        return;
    }
    /* Find local DRT item in server broadcast */
    drt_item = findDrtItemByIpcAddr(&drt_in_conf_upd_ind, ipcStack_glb.ipcAddr, &prev);

    if (drt_item == NULL)
    {
        /* local DRT item not found in server's DRT broadcast */
        ipcError(IPCLOG_SESSION, ("ipcHandleServerConfigUpdInd -> NO self!!!\n"));

        /* free this SSSSConfigUpdateIndication */
        ipcFreeDRT(&drt_in_conf_upd_ind);

        /* enter ROUTER_READY state now */

        /* clear DRT (except for own DRT item) */
        ipcRemoveRemoteDRTItems(&changes);

        ipcEnterRouterReadyState();
    }
    else
    {
        /* remove local DRT item from SSSSConfigUpdateIndication */
        ipcListRemoveNextLink(&drt_in_conf_upd_ind, prev);
        ipcListRemoveHead(&ipcSession_glb.drt);

        /* for all the items in old drt, 
         * update them according to the new drt */
        for (ipcListFirstPair(ipcSession_glb.drt, prev, curr);
            curr != NULL;)
        {
            my_item = (ipcDRTItem_t*)curr;
            foreign_item = findDrtItemByIpcAddr(&drt_in_conf_upd_ind, 
                                    my_item->ipcaddr, &temp_prev);
            /* If the item is NOT found in server's DRT broadcast */
            if (foreign_item == NULL)
            {
                ipcDrtRemoveItem(&ipcSession_glb.drt, (ipcDRTItem_t*)prev, &changes);
                if (prev == NULL)
                {
                    curr = ipcSession_glb.drt.head;
                }
                else
                {
                    curr = prev->next;
                }
            }
            else
            {
                my_item = ipcDrtUpdateItem(my_item, foreign_item, &changes);
                ipcListRemoveNextLink(&drt_in_conf_upd_ind, temp_prev);
                if (my_item != NULL)
                {
                    /* The DRT item being freed is the one removed from
                     * drt_in_conf_upd_ind, so don't dual-free it */
                    ipcFreeDRTItem(my_item);
                }
                ipcListNextPair(prev, curr);
            }
        }
        /* add items which were not in local DRT */
        while ((foreign_item = 
                 (ipcDRTItem_t*)ipcListRemoveHead(&drt_in_conf_upd_ind)) != NULL)
        {
            ipcDrtUpdateItem(NULL, foreign_item, &changes);
        }
        ipcListAddHead(&ipcSession_glb.drt, (ipcListLink_t)ipcSession_glb.localDrtItem);

        /* update DRT lookup tables */
        ipcUpdateDrtLookupTables(&ipcSession_glb.drt);

        if (drt_item->srvId_list.size != ipcSession_glb.localDrtItem->srvId_list.size)
        {
            localDrtSame = FALSE;
        }
        else
        {
            newOne = (ipcDRTListItem_t*)drt_item->srvId_list.head;
            oldOne = (ipcDRTListItem_t*)ipcSession_glb.localDrtItem->srvId_list.head;
            while (newOne != NULL)
            {
                if ((newOne->data.srvId == oldOne->data.srvId) && (newOne->data.dsmAction == oldOne->data.dsmAction))
                {
                    newOne = newOne->next;
                    oldOne = oldOne->next;
                }
                else
                {
                    break;
                }
            }
            if (newOne == NULL)
            {
                localDrtSame = TRUE;
            }
            else
            {
                localDrtSame = FALSE;
            }
        }
    
        /* free the local drt item we got from SSSSConfigUpdInd */
        ipcFreeDRTItem(drt_item);
    }
    UNLOCK(DRTTable);

    ipcDrtChangeSignal(&changes, !localDrtSame);
    ipcDrtChangeFreeAll(&changes);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleClientAuthReq
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle IPC_CLIENT_AUTH_REQ from client session.
*   
*   PARAMETER(S):
*       packet: pointer to the data packet received from router layer
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         State machine simplified
* 15Jan2005  Motorola Inc.         DRT optimization
* 19Jul2005  Motorola Inc.         Delete the duplicate UNLOCK(DRTTable)
* 26Jul2005  Motorola Inc.         Check return value of ipcNewDRTItem
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 15Jan2007  Motorola Inc.         Cached all DRT changes 
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
*
*****************************************************************************************/
void ipcHandleClientAuthReq(SSSSAuthRequestMsg_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t *drt_item = NULL;
    ipcDRTItem_t *new_item = NULL;
    UINT8 status = IPC_STATUS_OK;
    ipcNodeId_t node_id;
    ipcDrtChangeSet_t changes;
/*--------------------------------------- CODE -----------------------------------------*/
    node_id = ipcOsalRead32(packet->nodeId);
    
    ipcDrtChangeSetInit(&changes);
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleClientAuthReq: Failed to lock DRT table\n"));
        return;
    }

    /* use ipc address to try to get corresponding DRT item if any */
    drt_item = ipcAddrToDrtItem(ipcGetSrcAddr((ipcPacket_t *)packet));
    if (NULL == drt_item)
    {
        /* 
         * NO ipc address in DRT table,
         * use node_id to try to get corresponding DRT item if any */
        drt_item = ipcNodeIdToDrtItemWithoutLock(node_id);
        
        if (NULL == drt_item)
        {
            /* new node here */
            new_item = ipcNewDRTItem();
            if (new_item == NULL)
            {
                UNLOCK(DRTTable);
                ipcError(IPCLOG_SESSION, ("Unable to create new DRT item\n"));
                return;
            }
            new_item->ipcaddr = ipcGetSrcAddr(packet);
            new_item->node_id = node_id;
            /* data type */
            new_item->datatype = ipcOsalRead8(packet->datatype);
            
            /* add it to DRT */
            if ((drt_item = ipcDrtUpdateItem(NULL, new_item, &changes)) != NULL)
            {
                ipcFreeDRTItem(drt_item);
            }
            ipcSession_glb.ipcAddrToDrtItemLookup[new_item->ipcaddr] = new_item;
            ipcSession_glb.ipcAddrToNodeId[new_item->ipcaddr] = new_item->node_id;
            
            ipcAddDrtItemToNodeIdTable(new_item);
        }
        else
        {
            /* it means that { Node_id = XXX, Addr = YYY } has been in DRT,
             * but a new node will register to network with
             * { Node_id = XXX, Addr = ZZZ }, we can conclude that
             * this is a DUPLICATED NODE! */
            status = IPC_STATUS_DUPLICATE_NODE_TYPE;

            ipcError(IPCLOG_SESSION, ("{Node = %d, Addr = %d} has been in DRT, \n"
                "{Node = %d, Addr = %d} in SSSSAuthReq, Discard! \n"
                "Own node = {Node = %d, Addr = %d} \n",
                drt_item->node_id, drt_item->ipcaddr,
                node_id, ipcGetSrcAddr((ipcPacket_t *)packet),
                ipcStack_glb.nodeId, ipcStack_glb.ipcAddr));
        }
    }
    else
    {
        /* check the ipc addr */
        if (drt_item->node_id == node_id)
        {
            /* { node_id, ipc_addr } has been in current DRT */
            /* Do nothing here */
        }
        else
        {
            /* it means that { Node_id = XXX, Addr = YYY } has been in DRT,
             * while { Node_id = ZZZ, Addr = YYY } in SSSSAuthReq, this can
             * not happen!!! */
            ipcError(IPCLOG_SESSION, ("{Node = %d, Addr = %d} has been in DRT, \n"
                "{Node = %d, Addr = %d} in SSSSAuthReq, Discard!",
                drt_item->node_id, drt_item->ipcaddr,
                node_id, ipcGetSrcAddr((ipcPacket_t *)packet)));

            status = IPC_STATUS_DUPLICATE_IPC_ADDR;
        }
    }
    UNLOCK(DRTTable);

    ipcDrtChangeSignal(&changes, TRUE);
    ipcDrtChangeFreeAll(&changes);
    
    /* send SSSSAuthReply */
    SSSSAuthReply(ipcGetSrcAddr(packet), status);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleServerAuthReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle IPC_SERVER_AUTH_REPLY from server session.
*   
*   PARAMETER(S):
*       packet: pointer to the session packet
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         State machine simplified 
*
*****************************************************************************************/
void ipcHandleServerAuthReply(SSSSAuthReplyMsg_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL startTimer = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalRead8(packet->status) == IPC_STATUS_OK)
    {
        ipcDebugLog(IPCLOG_SESSION, ("ipcHandleServerAuthReply -> got SSSSAuthReply OK from Server!\n"));

        /* session enter NETWORK_READY state */
        ipcEnterNetworkReadyState();
        
        /* SSSSAuthReply == OK */
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("Failed to lock DRT table\n"));
            return;
        }
        /* we should check local DRT item empty? */
        if (ipcSession_glb.localDrtItem->srvId_list.size != 0)
        {
            startTimer = TRUE;
        }
        UNLOCK(DRTTable);        
        if (startTimer)
        {
            /* start timer to send SSSSConfigReq */
            ipcDrtHasChanged();
        }
    }
    else
    {
        /* Otherwise we are not welcome here so stay in ROUTER_READY state */
        ipcError(IPCLOG_SESSION,
            ("ipcHandleServerAuthReply -> got error response from server! %d\n", ipcOsalRead8(packet->status)));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleClientLinkDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle a message from a client saying that a link
*   is down.  It will remove the specified range of addresses from the DRT.
*   
*   PARAMETER(S):
*       packet: pointer to the SSSSLinkDown message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Feb2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcHandleClientLinkDown(SSSSLinkDownMsg_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSessionRemoveAddressRange(ipcOsalRead8(packet->ipcAddressRangeLow),
                                 ipcOsalRead8(packet->ipcAddressRangeHigh));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcIsMsgValidInCurrState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is used to check whether received msg can be handled in current state.
*   
*   PARAMETER(S):
*       msg_id: message ID
*       
*   RETURN: BOOL
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
BOOL ipcIsMsgValidInCurrState(ipcMsgOpcode_t msg_id)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL ret = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.isServer)
    {
        switch (msg_id)
        {
        case IPC_MSG_SSSS_CLIENT_AUTH_REQ:
        case IPC_MSG_SSSS_CLIENT_CONFIG_REQ:
        case IPC_MSG_SSSS_CLIENT_LINK_DOWN:
            if (ipcSession_glb.state == IPC_SESSION_STATE_NETWORK_READY)
            {
                ret = TRUE;
            }
        default:
            break;
        }
    }
    else
    {
        switch (msg_id)
        {
        case IPC_MSG_SSSS_SERVER_AUTH_REPLY:
            if (ipcSession_glb.state == IPC_SESSION_STATE_ROUTER_READY)
            {
                ret = TRUE;
            }
            break;
        case IPC_MSG_SSSS_SERVER_CONFIG_UPD_INDICATION:
            if (ipcSession_glb.state == IPC_SESSION_STATE_NETWORK_READY)
            {
                ret = TRUE;
            }
        default:
            break;
        }
    }
    /* Messages for CO sockets and for notification */
    switch (msg_id)
    {
        case IPC_MSG_COCO_CONN_DOWN:
        case IPC_MSG_COCO_CONN_REQ:
        case IPC_MSG_COCO_CONN_REP:
        case IPC_MSG_COCO_CONN_COMP:
        case IPC_MSG_SSSS_NOTIFY_SIGNAL:
        case IPC_MSG_SSSS_NOTIFY_REG:
            ret = TRUE;
    }
    
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcEnterLocalReadyState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to change state to local ready (this means we can use
*   sockets locally but we have not yet been assigned an IPC address).
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Feb2005  Motorola Inc.         Changed state to local ready 
*
*****************************************************************************************/
void ipcEnterLocalReadyState(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSession_glb.state = IPC_SESSION_STATE_LOCAL_READY;
#ifdef START_UP_TIME
    printk(KERN_ALERT "ipcStack_glb.state = IPC_SESSION_STATE_LOCAL_READY\n");
    printk(KERN_ALERT "time = %u msecs\n",jiffies_to_msecs(jiffies-ulStartUpTime));
#endif
    /* all timers should be stopped in this state */
    ipcOsalTimerStop(ipcSession_glb.configReqTimer);

    ipcDebugLog(IPCLOG_SESSION, ("Session enter LOCAL_READY state!\n"));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcEnterRouterReadyState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to change state to router ready (this means we have
*   been assigned an IPC address but we have not yet been authenticated by the
*   IPC server node).
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2005  Motorola Inc.         Timers are changed here
* 17Feb2005  Motorola Inc.         Changed state to router ready 
*
*****************************************************************************************/
void ipcEnterRouterReadyState(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSession_glb.state = IPC_SESSION_STATE_ROUTER_READY;

    /* all timers should be stopped in this state */
    ipcOsalTimerStop(ipcSession_glb.configReqTimer);

    /* send SSSSAuthReq */
    SSSSAuthRequest();

    ipcDebugLog(IPCLOG_SESSION, ("Session enter ROUTER_READY state! \n"));
} 


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcEnterNetworkReadyState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to change state to network ready.
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2005  Motorola Inc.         Timers are changed here 
*
*****************************************************************************************/
void ipcEnterNetworkReadyState(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSession_glb.state = IPC_SESSION_STATE_NETWORK_READY;
#ifdef START_UP_TIME
    printk(KERN_ALERT "ipcStack_glb.state = IPC_SESSION_STATE_NETWORK_READY\n");
    printk(KERN_ALERT "time = %u msecs\n",jiffies_to_msecs(jiffies-ulStartUpTime));
#endif
    if (ipcStack_glb.isServer)
    {
        /* start periodic DRT broadcast timer for server node */
        ipcOsalTimerStart(ipcSession_glb.drtBroadcastTimer);
    }

    ipcDebugLog(IPCLOG_SESSION, ("Session enter NETWORK_READY state!\n"));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleClientConfigReqTimeout
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle expiration of client config request timer.
*   This timer is used to delay the SSSSConfigRequest message to the server
*   since there may be several sockets being opened at the same time during
*   component initialization and we do not want a separate SSSSConfigRequest
*   message for each one.
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         Statemachine simplified 
*
*****************************************************************************************/
void ipcHandleClientConfigReqTimeout(void *obj)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcSession_glb.state != IPC_SESSION_STATE_NETWORK_READY)
    {
        ipcError(IPCLOG_SESSION, ("ipcHandleClientConfigReqTimeout -> fires on NON N_READY state!\n"));
        return;
    }
    
    /* send SSSSConfigReq to server */
    SSSSConfigRequest();
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleServerDrtBroadcastTimeout
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle expiration of the server DRT broadcast timer.
*   The server will periodically broadcast the entire DRT table to keep all nodes
*   in sync even if they missed a previous DRT broadcast.
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         Statemachine simplified 
*
*****************************************************************************************/
void ipcHandleServerDrtBroadcastTimeout(void *obj)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDebugLog(IPCLOG_SESSION, ("ipcHandleServerDrtBroadcastTimeout is called\n"));
    if (ipcSession_glb.state != IPC_SESSION_STATE_NETWORK_READY)
    {
        ipcError(IPCLOG_SESSION, ("ipcHandleServerDrtBroadcastTimeout -> fires on NON N_READY state!\n"));
        return;
    }

    /* kill server config request timer first */
    ipcOsalTimerStop(ipcSession_glb.configReqTimer);

    /* broadcast DRT to all nodes to synchronize DRT */
    SSSSConfigUpdIndication();

    /* restart DRT Broadcast timer */
    ipcOsalTimerStart(ipcSession_glb.drtBroadcastTimer);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleServerConfigReqTimeout
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle expiration of the server config req timer.
*   
*   PARAMETER(S):
*       None
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         Statemachine simplified 
*
*****************************************************************************************/
void ipcHandleServerConfigReqTimeout(void *obj)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcSession_glb.state != IPC_SESSION_STATE_NETWORK_READY)
    {
        ipcError(IPCLOG_SESSION, ("ipcHandleServerConfigReqTimeout -> fires on NON N_READY!\n"));
        return;
    }
    
    /* Restart DRT Broadcast timer */
    ipcOsalTimerStart(ipcSession_glb.drtBroadcastTimer);
    
    /* broadcast SSSSConfigUpdateIndication */
    SSSSConfigUpdIndication();
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSessionBroadcast
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to broadcast messages to all nodes. It just makes a 
*   copy for every single node and the copy will be sent. The original packet
*   is untouched.
*   
*   PARAMETER(S):
*       ipcPacket_t *packet
*       
*   RETURN: IPC_OK - Sending to all nodes is OK
*           IPCESERVICE - No node supports the service specified in the packet
*           IPCENOBUFS - No enough memory to copy the packet
*           other - the return value of SSRTDataRequest
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Sep2004  Motorola Inc.         Initial creation
* 16Jan2005  Motorola Inc.         Zero copy change
* 15Apr2005  Motorola Inc.         Check return value of ipcCopyPacket()
* 10Mar2006  Motorola Inc.         Don't break on each failure
* 01Dec2006  Motorola Inc.         Support DSM message discard               
* 17Jan2006  Motorola Inc.         DRT is not locked when calling 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcSessionBroadcast(ipcPacket_t *pPacket, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int error = IPCESERVICE; /* return this for empty DRT */
    int rc = IPC_OK;
    ipcPacket_t *packet_copy = NULL;
    ipcDRTItem_t *drt_item = NULL;
    ipcList_t duplicatedPackets;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Start with second DRT Item to skip the local Node */
    ipcListInit(&duplicatedPackets);
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcSessionBroadcast: Failed to lock DRT table\n"));
        return IPCEBUSY;
    }
    for (drt_item = ((ipcDRTItem_t *)ipcSession_glb.drt.head)->next;
         drt_item != NULL;
         drt_item = drt_item->next)
    {
        if (ipcDiscardForDsm(ipcGetDestServiceId(pPacket), drt_item, flags))
        {
            /* The destination is in deep sleep mode and this packet should
             * be discarded.  Continue to next node without sending. */
            continue;
        }
    
        packet_copy = ipcCopyPacket(pPacket, ipcRouterGetMemType(drt_item->ipcaddr));
        if (NULL == packet_copy)
        {
            rc = IPCENOBUFS;
            ipcError(IPCLOG_SESSION, 
                    ("ipcSessionBroadcast: failed to duplicate packet, size=%lu\n",
                     ipcGetMsgLength(pPacket)));
            continue;
        }
        
        ipcSetDestAddr(packet_copy, drt_item->ipcaddr);
        ipcListAddTail(&duplicatedPackets, (ipcListLink_t)packet_copy);
    }
    UNLOCK(DRTTable);

    while (duplicatedPackets.size > 0)
    {
        packet_copy = (ipcPacket_t*)ipcListRemoveHead(&duplicatedPackets);
        if (packet_copy == NULL)
        {
            break;
        }
        error = SSRTDataRequest(packet_copy, IPC_CONTROL_CHANNEL_ID);
        if (error != IPC_OK)
        {
            ipcFree(packet_copy);
        }
        if (rc == IPC_OK)
        {
            rc = error;
        }
    }
    
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTSSDataIndication
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by Router to send Packet Data to Session.
*   
*   PARAMETER(S):
*       packet: This is a pointer to the data buffer that has been received
*       
*   RETURN: int
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Oct2004  Motorola Inc.         Initial creation
* 02Nov2005  Motorola Inc.         Return value changed to int
* 18Jan2007  Motorola Inc.         Added debug information
* 26Jan2007  Motorola Inc.         add more checks on the shared memory buffer headers
*
*****************************************************************************************/
int RTSSDataIndication(ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    #ifndef IPC_RELEASE
    ipcSMCheckBuffer((char *)packet, "RTSSDataIndication");
    #endif /* IPC_RELEASE */
	
    switch (ipcGetDestServiceId(packet))
    {
    case IPC_SRV_SESSIONCONTROL:
        ipcDebugLog(IPCLOG_SESSION,
                ("RTSSDataIndication: got control message\n"));
        ipcHandleSessionControlMessage(packet);
        break;

    case IPC_SRV_LOG_CONFIG:
        rc = ipcHandleLogConfigRequest((ipcLogConfigPacket_t *)packet);
        break;

    default:
        ipcDebugLog(IPCLOG_SESSION,
                ("RTSSDataIndication: got data message\n"));
        rc = ipcHandleSessionDataMessage(packet);
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTSSReady
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by router manager to let the session know that it is
*   ready to communicate with the nodes on the network
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: IPC_OK or IPCENOTREADY (if state is NULL)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Oct2004  Motorola Inc.         Initial creation
* 17Jan2007  Motorola Inc.         Cached DRT changes 
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int RTSSReady(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDrtChangeSet_t changes;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Server node is already in NETWORK_READY state so do nothing for server */

    ipcDrtChangeSetInit(&changes);
    if (!ipcStack_glb.isServer)
    {
        /* Client node should transition from LOCAL_READY to ROUTER_READY state */

        if (ipcSession_glb.state == IPC_SESSION_STATE_NULL)
        {
            ipcError(IPCLOG_SESSION, ("RTSSReady : recv rt ready in NULL state!\n"));
            return IPCENOTREADY;
        }

        /* The router layer has already stored the local ipc addr in ipcStack_glb */
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION, ("RTSSReady: Failed to lock DRT table!\n"));
            return IPCEBUSY;
        }
        /* Update the local DRT Item */
        ipcSession_glb.localDrtItem->ipcaddr = ipcStack_glb.ipcAddr;

        /* Remove all drt items except self */
        ipcRemoveRemoteDRTItems(&changes);
        UNLOCK(DRTTable);

        /* set session's state */
        if (ipcStack_glb.ipcAddr == IPC_INVALID_ADDR)
        {
            /* We just lost the connection to our higher peer node so we must
             * go back to the local ready state. */
            ipcEnterLocalReadyState();
        }
        else
        {
            /* We just received an IPC Address so enter router ready state */
            ipcEnterRouterReadyState();
        }

        ipcDrtChangeSignal(&changes, TRUE);
        ipcDrtChangeFreeAll(&changes);
    }
    
    return IPC_OK;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTSSLinkDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by router manager to let the session know that a
*   link has gone down so it should remove a range of addresses from the DRT.
*   If this is the server it can do this immediately.  Otherwise we have to
*   send a message to the server node.
*   
*   PARAMETER(S):
*       lowAddr: low address in range to remove
*       highAddr: high address in range to remove
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Feb2005  Motorola Inc.         Initial creation 
*
*****************************************************************************************/
void RTSSLinkDown(ipcAddress_t lowAddr, ipcAddress_t highAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.isServer)
    {
        ipcSessionRemoveAddressRange(lowAddr, highAddr);
    }
    else
    {
        /* Send message to server to tell it to remove this range */
        SSSSLinkDown(lowAddr, highAddr);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSessionRemoveAddressRange
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to remove a range of addresses from the DRT because 
*   a link has gond down
*   
*   PARAMETER(S):
*       lowAddr: low address in range to remove
*       highAddr: high address in range to remove
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Feb2005  Motorola Inc.         Initial creation
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 17Jan2007  Motorola Inc.         Cached DRT changes 
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
*
*****************************************************************************************/
void ipcSessionRemoveAddressRange(ipcAddress_t lowAddr, ipcAddress_t highAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t *drt_item = NULL;
    ipcListLink_t prev, curr;
    ipcDrtChangeSet_t changes;
/*--------------------------------------- CODE -----------------------------------------*/

    ipcDrtChangeSetInit(&changes);
    if ((lowAddr == IPC_INVALID_ADDR) || (highAddr == IPC_INVALID_ADDR))
    {
        ipcError(IPCLOG_SESSION, ("Invalid RTSSLinkDown Message: low=%d, high=%d\n", lowAddr, highAddr));
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, ("Removing range of addresses: %d -> %d\n", lowAddr, highAddr));

        /* Remove all addresses in this range form the DRT table */
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSessionRemoveAddressRange: Failed to lock DRT table\n"));
            return;
        }

        for (ipcListFirstPair(ipcSession_glb.drt, prev, curr);
            curr != NULL;)
        {
            drt_item = (ipcDRTItem_t*)curr;

            if ((drt_item->ipcaddr >= lowAddr) && (drt_item->ipcaddr <= highAddr))
            {
                /* The address is in the range that we lost when this link went down
                 * so remove it from the DRT */
                ipcWarning(IPCLOG_SESSION,
                    ("ipcSessionRemoveAddressRange: Remove DRT item of addr %d\n",
                     drt_item->ipcaddr));
                ipcSession_glb.ipcAddrToDrtItemLookup[drt_item->ipcaddr] = NULL;
                ipcSession_glb.ipcAddrToNodeId[drt_item->ipcaddr] = IPC_NODE_ANY;
                ipcRemoveFromNodeIdTable(drt_item->node_id);
                ipcDrtRemoveItem(&ipcSession_glb.drt, (ipcDRTItem_t*)prev, &changes);

                if (prev == NULL)
                {
                    curr = ipcSession_glb.drt.head;
                }
                else
                {
                    curr = prev->next;
                }
            }
            else
            {
                ipcListNextPair(prev, curr);
            }
        }

        UNLOCK(DRTTable);

        ipcDrtChangeSignal(&changes, TRUE);
        ipcDrtChangeFreeAll(&changes);
    }
}

