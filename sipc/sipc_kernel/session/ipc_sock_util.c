/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_sock_util.c
*   FUNCTION NAME(S): ipcCOSocketDataArrival
*                     ipcCOSocketDataDeliver
*                     ipcCOSocketLock
*                     ipcCOSocketMaxRetries
*                     ipcCOSocketReceiveEnd
*                     ipcCOSocketSentListEmpty
*                     ipcCOSocketTxWindowAvailable
*                     ipcCOSocketTxWindowFull
*                     ipcCOSocketUnlock
*                     ipcCreateConnComplete
*                     ipcCreateConnDown
*                     ipcCreateConnReply
*                     ipcCreateConnRequest
*                     ipcDisconnectSocket
*                     ipcFindSocketBySrvId
*                     ipcHandleCOCOConnComp
*                     ipcHandleCOCOConnDown
*                     ipcHandleCOCOConnRep
*                     ipcHandleCOCOConnReq
*                     ipcHandleNotifyRegister
*                     ipcHandleNotifySignal
*                     ipcInsertPacketIntoQueue
*                     ipcNeedSignalServiceNotification
*                     ipcNotify
*                     ipcPutPacketToSocket
*                     ipcRegisterNotification
*                     ipcRegisterPsNotification
*                     ipcRegisterServiceNotify
*                     ipcRegisterServicePsNotify
*                     ipcRemoveNotification
*                     ipcRemoveObsoleteConnReq
*                     ipcServiceInDRTItem
*                     ipcSignalNotification
*                     ipcSignalPsNotification
*                     ipcSignalServiceNotification
*                     ipcSocketDestroy
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains some utility functions for manipulating ipc sockets.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 13Sep2005  Motorola Inc.         Initial Creation
* 23May2006  Motorola Inc.         Use IPC_INVALID_ADDR for local notifications
* 30Jun2006  Motorola Inc.         Simplified CO socket closing
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
* 22Jun2007  Motorola Inc.         Align messages in notifications
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 21Aug2007  Motorola Inc.         Fixed OSS compliance issue
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_session.h"
#include "ipc_osal.h"
#include "ipc_list.h"
#include "ipc_router_interface.h"
#include "ipc_messages.h"
#include "ipc_sock_impl.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

int ipcInsertPacketIntoQueue(ipcSocket_t *socket, ipcPacket_t *packet);

void ipcNotify(ipcSocket_t *socket, int notificationType);

/*--------------------------------------- MACROS ---------------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateConnRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates a connection request packet.
*   
*   PARAMETER(S):
*       destAddr: dest address to be connected to
*                  destSrv  - dest service Id to be connected to
*       mySrv: the service id of local socket
*       cookie: a 32 bit integer to identify the connection
*       windowSize: the initial size of sliding window
*       startSeqNum: the seqnum of first expected packet
*       
*   RETURN: the connection request message, NULL on failure;
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Sep2005  Motorola Inc.         Initial Creation
* 08Dec2005  Motorola Inc.         Added windowSize and startSeqNum 
*
*****************************************************************************************/
ipcPacket_t* ipcCreateConnRequest(ipcAddress_t destAddr,
                                  ipcServiceId_t destSrv,
                                  ipcServiceId_t mySrv,
                                  UINT32 cookie,
                                  UINT8 windowSize,
                                  UINT8 startSeqNum)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    COCOConnRequestMsg_t* request = 
        (COCOConnRequestMsg_t*)ipcCreateSessionControlPacket(
            COCO_CONN_REQUEST_DATA_SIZE,
            IPC_MSG_COCO_CONN_REQ,
            destAddr);
    if (request != NULL)
    {
        ipcOsalWrite32(request->cookie, cookie);
        ipcOsalWrite16(request->mySrvId, mySrv);
        ipcOsalWrite16(request->destSrvId, destSrv);
        ipcOsalWrite8(request->windowSize, windowSize);
        ipcOsalWrite8(request->startSeqNum, startSeqNum);
    }
    return (ipcPacket_t*)request;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateConnReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates a connection reply packet.
*   
*   PARAMETER(S):
*       destAddr: the dest address to which this packet will be sent
*       destSrv: the desti service id
*       mySrv: service id of local socket
*       cookie: the identification of the connection
*       willAccept: whether this link is allowed
*       windowSize: the max Rx size of our socket
*       startSeqNum: the sequence number of first packet expected
*       
*   RETURN: the connection reply message, NULL on failure;
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcPacket_t* ipcCreateConnReply(ipcAddress_t destAddr,
                                  ipcServiceId_t destSrv,
                                  ipcServiceId_t mySrv,
                                  UINT32 cookie,
                                  BOOL willAccept,
                                  UINT8 windowSize,
                                  UINT8 startSeqNum)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    COCOConnReplyMsg_t* reply = 
        (COCOConnReplyMsg_t*)ipcCreateSessionControlPacket(
            COCO_CONN_REPLY_DATA_SIZE, 
            IPC_MSG_COCO_CONN_REP,
            destAddr);
    if (reply != NULL)
    {
        ipcOsalWrite16(reply->newSrvId, mySrv);
        ipcOsalWrite32(reply->cookie, cookie);
        ipcOsalWrite16(reply->peerSrvId, destSrv);
        ipcOsalWrite8(reply->acceptConnect, willAccept ? 1 : 0);
        ipcOsalWrite8(reply->windowSize, windowSize);
        ipcOsalWrite8(reply->startSeqNum, startSeqNum);
    }
    return (ipcPacket_t*)reply;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateConnComplete
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates a connection complete message.
*   
*   PARAMETER(S):
*       destAddr: dest address where the packet goes
*       destSrv: dest service id
*       mySrv: service id of local socket
*       cookie: the identifier of the connection
*       
*   RETURN: the connection complete message, NULl on failure;
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Sep2005  Motorola Inc.         Initial Creation
* 08Dec2005  Motorola Inc.         This will be transmitted using the  connection rather than control sock 
*
*****************************************************************************************/
ipcPacket_t* ipcCreateConnComplete(ipcAddress_t destAddr,
                                 ipcServiceId_t destSrv,
                                 ipcServiceId_t mySrv,
                                 UINT32 cookie)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT8 msgFlags = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    COCOConnCompleteMsg_t* msg = 
        (COCOConnCompleteMsg_t*)ipcCreateSessionControlPacket(
            COCO_CONN_COMPLETE_DATA_SIZE, 
            IPC_MSG_COCO_CONN_COMP,
            destAddr);
    if (msg != NULL)
    {
        ipcSetSrcServiceId(msg, mySrv);
        msgFlags = ipcGetMsgFlags(msg);
        msgFlags |= IPC_PACKET_HIGH_PRIORITY;

        /* This is no longer a control packet, it is now the first packet going
         * through the new connection */
        ipcSetMsgFlags(msg, msgFlags & (~IPC_PACKET_CONTROL));

        ipcOsalWrite32(msg->cookie, cookie);
        ipcOsalWrite16(msg->destSrvId, destSrv); 
    }
    return (ipcPacket_t*)msg;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateConnDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates a connection down message.
*   
*   PARAMETER(S):
*       destAddr: dest address
*       destSrv: dest service id
*       mySrv: local service id
*       cookie: identifier of the connection
*       how: how the connection will be shutdown, one of
*                        IPCSD_RECEIVE, IPCSD_SEND, IPCSD_BOTH
*       
*   RETURN: the connection down packet, NULL on failure
*   
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcPacket_t* ipcCreateConnDown(ipcAddress_t destAddr,
                                 ipcServiceId_t destSrv,
                                 ipcServiceId_t mySrv,
                                 UINT32 cookie,
                                 UINT8 how)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    COCOConnDownMsg_t* msg = 
        (COCOConnDownMsg_t*)ipcCreateSessionControlPacket(
            COCO_CONN_DOWN_DATA_SIZE, 
            IPC_MSG_COCO_CONN_DOWN,
            destAddr);
/*--------------------------------------- CODE -----------------------------------------*/
    if (msg != NULL)
    {
        ipcOsalWrite32(msg->cookie, cookie);
        ipcOsalWrite16(msg->mySrvId, mySrv);
        ipcOsalWrite16(msg->destSrvId, destSrv); 
        ipcOsalWrite8(msg->how, how);
    }

    return (ipcPacket_t*)msg;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketDataArrival
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Callback function to handle data arrival event for socket.
*   
*   PARAMETER(S):
*       conn: the connection of the socket
*       pack: the incoming packet
*       
*   RETURN: IPC_OK - success
*           IPCEFAULT - Null pointer
*           IPCEINVAL - the socket of the conn is in an invalid state
*           IPCEBUSY - the mutex of the socket queue is closed
*           IPCESOCKFULL - the socket is full
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 08Dec2005  Motorola Inc.         Handles CONN_COMP
* 23Feb2005  Motorola Inc.         Change state to CONNECTED before  waken up
* 13Mar2006  Motorola Inc.         Consume the CONN_COMP message as  normal packets 
* 22Mar2007  Motorola Inc.         Bugfix: consume CONN_COMP before signal sema 
*                                  to avoid wrong window size
* 24Apr2007  Motorola Inc.         Bugfix: for CONN_COMP, send ack before signal sema
*
*****************************************************************************************/
int ipcCOSocketDataArrival(ipcConn_t* conn, ipcPacket_t* pack)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((conn == NULL) || (pack == NULL))
    {
        return IPCEFAULT;
    }
    socket = (ipcSocket_t*)conn->privateData;
    if (socket == NULL)
    {
        return IPCEFAULT;
    }
    /* If this is CONN_COMP msg */
    if ((ipcGetDestServiceId(pack) == IPC_SRV_SESSIONCONTROL) &&
        (ipcGetMsgOpcode(pack) == IPC_MSG_COCO_CONN_COMP) &&
        (socket->state == IPC_SOCK_ACCEPT_PENDING))
    {
        /* transit the state of socket */
        socket->state = IPC_SOCK_CONNECTED;
        ipcConnConsumeData(&socket->conn, 
                ipcGetSrcAddr(pack), 
                ipcGetSrcServiceId(pack));
        ipcFree(pack);
        
        return IPC_OK;
    }

    return ipcInsertPacketIntoQueue(socket, pack);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketTxWindowFull
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   The callback function to handle Tx window full event of a socket.
*   
*   PARAMETER(S):
*       conn: the connection of the socket
*       mayBlock: whether this function is allowed to block
*       
*   RETURN: IPC_OK - success
*           IPCEFAULT - null pointer
*           IPCEINTR - failed to wait on a semaphore
*           IPCEWOULDBLOCK - this operation might block
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 07Mar2006  Motorola Inc.         Check connection state
* 30Jun2006  Motorola Inc.         Removed checking for isClosed
* 15Jan2007  Motorola Inc.         fix many threads are blocked but only one can be waked up 
*
*****************************************************************************************/
int ipcCOSocketTxWindowFull(ipcConn_t* conn, BOOL mayBlock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSocket_t* socket = NULL;

    if ((conn == NULL) || (conn->privateData == NULL))
    {
        return IPCEFAULT;
    }
    socket = (ipcSocket_t*)conn->privateData;

    if (mayBlock)
    {
        rc = ipcOsalSemaWait(socket->coSema, 100);
        if (rc == IPCE_TIMEOUT)
        {
            rc = IPC_OK;
        }
        if (rc != IPC_OK)
        {
            rc = IPCEINTR;
        }
    }
    else
    {
        rc = IPCEWOULDBLOCK;
    }
    if (!socket->peerCanReceive ||
        (socket->state != IPC_SOCK_CONNECTED))
    {
        rc = IPCENOTCONN;
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketTxWindowAvailable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Callback function to handle Tx window available event.
*   
*   PARAMETER(S):
*       conn: the connection of socket
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Added select support for linux
* 18Jan2007  Motorola Inc.         Don't lock socket on ipcNotify 
*
*****************************************************************************************/
void ipcCOSocketTxWindowAvailable(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((conn == NULL) || (conn->privateData == NULL))
    {
        return;
    }
    socket = (ipcSocket_t*)conn->privateData;
    ipcOsalSemaSignal(socket->coSema);

    wake_up_interruptible(&socket->writewq);
    
    /* do the notification stuff */
    ipcNotify(socket, IPCWRITE);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketMaxRetries
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when a packet in a CO socket can't be sent out.
*   
*   PARAMETER(S):
*       conn: the connection for the socket
*       packet: the packet which can't be sent out
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 30Jun2006  Motorola Inc.         Removed checking for isClosed 
*
*****************************************************************************************/
void ipcCOSocketMaxRetries(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((conn == NULL) || (conn->privateData == NULL))
    {
        return;
    }
    /* free all the packets pending in socket */
    ipcConnDestroy(conn, TRUE, TRUE, TRUE);
    /* set the state of the socket to NOT_CONNECTED */
    ipcCOSocketTxWindowAvailable(conn);
    socket = (ipcSocket_t*)conn->privateData;
    ipcDisconnectSocket(socket);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketSentListEmpty
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when all packets in the list sentPackets of a socket
*   has been sent out.
*   
*   PARAMETER(S):
*       the connection of the socket
*       
*   RETURN: Always IPC_OK
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 30Jun2006  Motorola Inc.         Wakeup tasks calling ipcshutdown
* 18Jan2007  Motorola Inc.         Don't lock connMutex 
*
*****************************************************************************************/
int ipcCOSocketSentListEmpty(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((conn == NULL) || (conn->privateData == NULL))
    {
        return IPCEFAULT;
    }
    socket = (ipcSocket_t*)conn->privateData;

    /* XXX: This is not protected by a lock! */
    if ((socket->state == IPC_SOCK_CONNECTED) && (!socket->canSend))
    {
        ipcCOSocketTxWindowAvailable(conn);
    }

    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketDataDeliver
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to deliver a packet for CO socket.
*   
*   PARAMETER(S):
*       conn: the connection of the socket.
*       packet: the packet to be sent
*       
*   RETURN: The return value of SSRTDataRequest
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCOSocketDataDeliver(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = conn->privateData;    
/*--------------------------------------- CODE -----------------------------------------*/
    return SSRTDataRequest(packet, socket->channelId);        
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketLock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Lock a connection of CO socket.
*   
*   PARAMETER(S):
*       conn: the connection to be locked
*       
*   RETURN: IPCEFAULT on null pointer, else return value of ipcOsalMutexLock
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCOSocketLock(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((conn == NULL) ||
        (NULL == (socket = conn->privateData)))
    {
        return IPCEFAULT;
    }

    return ipcLockSocket(socket);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketUnlock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Unlock the connection of a CO socket.
*   
*   PARAMETER(S):
*       conn: the connection of the socket
*       
*   RETURN: IPCEFAULT on Null pointer, else return value of ipcOsalMutexRelease
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCOSocketUnlock(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((conn == NULL) ||
        (NULL == (socket = conn->privateData)))
    {
        return IPCEFAULT;
    }

    return ipcUnlockSocket(socket);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSocketDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to destroy a socket. The socket will be removed from 
*   local service id table, and all packets delieved to this socket will be 
*   freed.
*   NOTE: Don't lock the socket before invoking this function, this function
*   will lock it.
*   
*   PARAMETER(S):
*       socket: This is the socket which will be destroied.
*       
*   RETURN: TRUE - Destroy this socket will cause local DRT item change.
*           FASLE - Destroy this socket will not cause local DRT item change, or error
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Jan2005  Motorola Inc.         Initial Creation
* 02Mar2005  Motorola Inc.         Removed isSocketValid check (this is done before this function call)
* 22Aug2005  Motorola Inc.         Added lock for parralle condition
* 13Sep2005  Motorola Inc.         Added CO socket
* 14Apr2006  Motorola Inc.         Bug fix for vshm leak
* 15Dec2006  Motorola Inc.         Indicated DRT change when needed.
* 18Jan2007  Motorola Inc.         Cache DRT changes 
* 20Mar2007  Motorola Inc.         Abort if failed locking
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
*
*****************************************************************************************/
BOOL ipcSocketDestroy(ipcSocket_t* socket)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL ret = FALSE;
    ipcPacket_t *packet = NULL;
    ipcNotification_t* notification = NULL;
    ipcServiceNotification_t* srvNot = NULL;
    ipcListLink_t prev, curr;
    ipcList_t tempList;
    ipcDrtChangeSet_t changes;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDrtChangeSetInit(&changes);
    if (socket == NULL)
    {
        return FALSE;
    }
    if ((socket->isCOSocket))
    {
        if (ipcOsalMutexLock(ipcStack_glb.connMutex) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSocketDestroy: Failed to lock connMutex\n"));
            return FALSE;
        }
        else
        {
            ipcConnUndelegate(&socket->conn);
            ipcOsalMutexRelease(ipcStack_glb.connMutex);
        }
        ipcConnDestroy(&socket->conn, TRUE, TRUE, FALSE);
        ipcOsalSemaClose(socket->coSema);
    }

    socket->magic = 0;

    if (socket->isBound)
    {
        /* first we remove this socket from local service id table */
        if (!ipcRemoveSocketFromLocalSrvIdTab(socket, &ret))
        {
            return FALSE;
        }
        /* Remove service notifications whose dest is this socket */
        if (LOCK(DRTTable) == IPCE_OK)
        {
            if (ret == TRUE)
            {
                ipcDrtRemoveService(ipcSession_glb.localDrtItem, socket->srvId, &changes);
            }
            ipcListInit(&tempList);
            tempList.head = (ipcListLink_t)ipcSession_glb.serviceNotifications;
            for (ipcListFirstPair(tempList, prev, curr);
                    curr != NULL;)
            {
                srvNot = (ipcServiceNotification_t*)curr;
                if (srvNot->notification.destination.u.serviceId == socket->srvId)
                {
                    ipcListRemoveNextLink(&tempList, prev);
                    ipcFree(srvNot);

                    if (prev == NULL)
                    {
                        curr = tempList.head;
                    }
                    else
                    {
                        curr = prev->next;
                    }
                }
                else
                {
                    ipcListNextPair(prev, curr);
                }
            }
            ipcSession_glb.serviceNotifications = (ipcServiceNotification_t*)tempList.head;
            UNLOCK(DRTTable);
        }
        else
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSocketDestroy: Failed to lock DRT table\n"));
            return FALSE;
        }

        ipcDrtChangeSignal(&changes, TRUE);
        ipcDrtChangeFreeAll(&changes);

        if (ipcLockSocket(socket) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSocketDestroy: Failed to lock socket %p!\n", socket));
            return FALSE;
        }
        /* free all connection requests in newConnList */
        ipcListFreeAll(&socket->newConnList);
        /* free all packet left in this socket */
        if (socket->packetQueue.currentSize > 0)
        {
            if (ipcIsMulticastServiceId(socket->srvId))
            {
                ipcListLink_t listLink = NULL;

                while (NULL != (listLink = ipcListRemoveHead(&socket->packetQueue.highPriorityList)))
                {
                    packet = (ipcPacket_t*)listLink->data;
                    ipcFree(packet);
                    ipcFree(listLink);
                }
                while (NULL != (listLink = ipcListRemoveHead(&socket->packetQueue.lowPriorityList)))
                {
                    packet = (ipcPacket_t*)listLink->data;
                    ipcFree(packet);
                    ipcFree(listLink);
                }
            }
            else
            {
                ipcListFreeAll(&socket->packetQueue.highPriorityList);
                ipcListFreeAll(&socket->packetQueue.lowPriorityList);
            }
            socket->packetQueue.currentSize = 0;
        }
    }
    else
    {
        if (LOCK(SrvIDTable) == IPCE_OK)
        {
            ipcListRemoveLink(&ipcSession_glb.unboundSockList, socket);
            UNLOCK(SrvIDTable);
        }
        else
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSocketDestroy: Failed to lock SrvIDTable, Operation aborted!\n"));
            //ipcListRemoveLink(&ipcSession_glb.unboundSockList, socket);
            return FALSE;
        }
        if (ipcLockSocket(socket) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSocketDestroy: Failed to lock socket! Operation aborted!\n"));
            return FALSE;
        }
    }
    
    /* close the channel reserved by this socket */
    if (socket->channelId != IPC_NONDEDICATED_CHANNEL_ID)
    {
        SSRTChannelRelRequest(socket->channelId,
            ipcNodeIdToIpcAddr(socket->destNode),
            socket->srvId);
    }
    /* close all the notifications on this socket */
    while (socket->notifications != NULL)
    {
        notification = socket->notifications;
        socket->notifications = socket->notifications->next;

        ipcFree(notification);
    }

    /* close the socket recv queue */
    ipcOsalSemaClose(socket->packetQueue.notEmptySema);
    ipcOsalMutexClose(socket->packetQueue.mutex);

    /* free this socket */
    ipcFree(socket);
    
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcInsertPacketIntoQueue
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Insert a packet to the receive queue of a socket
*   
*   PARAMETER(S):
*       socket: the pointer to the socket
*       packet: the packet to be added
*       
*   RETURN: IPC_OK - success
*           IPCESOCKFULL - the socket is full
*           IPCEINTR - failed to lock or wait semaphore
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Oct2005  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcInsertPacketIntoQueue(ipcSocket_t *socket, ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int flags = 0;
    int queueFlags = 0;
    int rc = 0;
    ipcServiceId_t srvId = IPC_SERVICE_ANY;
    int highWatermark = 0;
    int originalNumPackets = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    flags = ipcGetMsgFlags(packet);

    if (flags & IPC_PACKET_HIGH_PRIORITY)
    {
        /* This is a higih priority packet */
        if (flags & IPC_PACKET_LIFO_PRIORITY)
        {
            /* It is a ROS priority packet (last in, first out) */
            queueFlags |= IPC_QUEUE_LIFO;
        }
        else
        {
            /* It is a regular priority packet so maintain correct order
             * among other priority packets */
            queueFlags |= IPC_QUEUE_HIGH_PRIORITY;
        }
    }

    if (ipcLockSocket(socket) < 0)
    {
        return IPCEBUSY;
    }
    srvId = socket->srvId;
    highWatermark = socket->highWatermark;
    originalNumPackets = ipcQueueNumElements(&socket->packetQueue);

    ipcUnlockSocket(socket);

    if (srvId & IPC_SRVID_M_BIT)
    {
        rc = ipcQueuePutObject(&socket->packetQueue, (ipcListLink_t)packet, queueFlags);
    }
    else
    {
        rc = ipcQueuePutLink(&socket->packetQueue, (ipcListLink_t)packet, queueFlags);
    }
    
    if (rc == IPC_OK)
    {
        wake_up_interruptible(&socket->readwq);
    
        if ((highWatermark != 0) && 
            (originalNumPackets < highWatermark) &&
            (ipcQueueNumElements(&socket->packetQueue) >= highWatermark))
        {
            ipcNotify(socket, IPCHIGHMARK);
        }
        ipcNotify(socket, IPCREAD);
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPutPacketToSocket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Put a packet to a socket.
*   
*   PARAMETER(S):
*       socket: pointer to the socket
*       packet: the packet to put
*       
*   RETURN: IPC_OK - success
*           IPCESOCKFULL - the socket is full
*           IPCEINTR - failed to lock or wait semaphore
*           IPCEFAULT - Null pointer
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 30Jun2006  Motorola Inc.         Removed checking for isClosed 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcPutPacketToSocket(ipcSocket_t* socket, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
    BOOL socketCanReceive = FALSE;
    BOOL peerCanSend = FALSE;
    int socketState = IPC_SOCK_CONNECTED;
    ipcNodeId_t socketDestNode = IPC_NODE_ANY;
    ipcServiceId_t socketDestSrv = IPC_SERVICE_ANY;
    ipcServiceId_t socketSrvId = IPC_SERVICE_ANY;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((socket == NULL) || (packet == NULL))
    {
        return IPCEFAULT;
    }

    if (ipcLockSocket(socket) < 0)
    {
        ipcError(IPCLOG_SESSION,
                 ("ipcPutPacketToSocket: Failed to lock socket\n"));
        return IPCEBUSY;
    }

    socketCanReceive = socket->canReceive;
    peerCanSend = socket->peerCanSend;
    socketState = socket->state;
    socketDestNode = socket->destNode;
    socketDestSrv = socket->destSrv;
    socketSrvId = socket->srvId;
    ipcUnlockSocket(socket);

    if (socket->isCOSocket)
    {
        /* handle ack messages first */
        if (ipcGetMsgFlags(packet) == IPC_PACKET_ACK)
        {
            COCOAckMsg_t* ack = (COCOAckMsg_t*)packet;

            ipcConnGotAck(&socket->conn,
                ipcOsalRead8(ack->wasReliable),
                ipcOsalRead8(ack->seqNum),
                ipcOsalRead8(ack->maxSeqNum));

            ipcFree(packet);

            /* XXX: Here rc must be IPC_OK, otherwise we'll face double-free */
            rc = IPC_OK;
        }
        else
        {       
            if (!socketCanReceive || !peerCanSend)
            {
                rc = IPCESHUTDOWN;
            }
            else if ( ((socketState == IPC_SOCK_CONNECTED) || 
                      (socketState == IPC_SOCK_ACCEPT_PENDING)) &&
                     (socketDestNode == ipcAddrToNodeId(ipcGetSrcAddr(packet))) &&
                     (socketDestSrv == ipcGetSrcServiceId(packet)))
            {
                rc = ipcConnGotPacket(&socket->conn, packet);
            }
            else
            {
                ipcWarning(IPCLOG_SESSION,
                    ("ipcPutPacketToSocket: Wrong packet! "
                     "Got (%d, %d), expecting(%d, %d), my state: %d\n",
                    ipcAddrToNodeId(ipcGetSrcAddr(packet)), ipcGetSrcServiceId(packet),
                    socket->destNode, socket->destSrv, socket->state));
                
                rc = IPCEINVAL;
            }
        }
    }
    else
    {
        if (!socketCanReceive)
        {
            rc = IPCESHUTDOWN;
        }
        else
        {
            rc = ipcInsertPacketIntoQueue(socket, packet);
        }
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFindSocketBySrvId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Find a socket by service id.
*   
*   PARAMETER(S):
*       srvId: the service used to find sockets
*       
*   RETURN: pointer to the socket, NULL if not found
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcSocket_t* ipcFindSocketBySrvId(ipcServiceId_t srvId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (ipcSocket_t*)ipcHashFind(&ipcSession_glb.local_srvid_tab,
            ipcCastPtr(srvId, void*));
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcServiceInDRTItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Whether a service is in a DRT item.
*   
*   PARAMETER(S):
*       drtItem: the DRT item to look up
*       service: the desired service
*       dsmAction: optional output parameter used to return the default 
*                              action to take when sending message to this service while
*                              the node is in deep sleep. Pass in NULL if not needed.
*                              Can be IPC_DSM_WAKE_UP or IPC_DSM_DISCARD
*                              
*       
*   RETURN: TRUE if found, otherwise FALSE
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Nov2005  Motorola Inc.         Initial Creation
* 01Dec2006  Motorola Inc.         Support DSM message discard
*
*****************************************************************************************/
BOOL ipcServiceInDRTItem(ipcDRTItem_t* drtItem, ipcServiceId_t service,
                         UINT16 *dsmAction)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTListLink_t link;
/*--------------------------------------- CODE -----------------------------------------*/
    for (link = (ipcDRTListLink_t)drtItem->srvId_list.head;
         link != NULL;
         link = link->next)
    {
        if (link->data.srvId == service)
        {
            if (dsmAction != NULL)
            {
                *dsmAction = link->data.dsmAction;
            }
            return TRUE;
        }
    }
    return FALSE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveObsoleteConnReq
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to remove obsolete connection requests on a socket.
*   NOTE: Assume the socket is locked.
*   
*   PARAMETER(S):
*       socket: pointer to the socket
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
* 06Sep2006  Motorola Inc.         Optimize the loop 
*
*****************************************************************************************/
void ipcRemoveObsoleteConnReq(ipcSocket_t* socket)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev = NULL;
    ipcListLink_t curr = NULL;
    UINT32 currentTick = ipcOsalGetTickCount();
    COCOConnRequestMsg_t* connReq = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    for (ipcListFirstPair(socket->newConnList, prev, curr);
        curr != NULL; )
    {
        connReq = (COCOConnRequestMsg_t*)curr;
        
        if (currentTick - connReq->tick > IPC_MAX_WAIT_TIME)
        {
            ipcListRemoveNextLink(&socket->newConnList, prev);
            ipcFree(connReq);
            if (prev == NULL)
            {
                curr = socket->newConnList.head;
            }
            else
            {
                curr = prev->next;
            }
        }
        else
        {
            /* current connReq is too young to remove, latter ones in the list 
             * are younger, so needn't more checking to them
             */
            break;
        }
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleCOCOConnReq
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle a connection request packet coming from
*   router layer.
*   
*   PARAMETER(S):
*       connReq: the incoming connection request packet
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Added error checking
* 15Nov2005  Motorola Inc.         Refuse connection if sock not found
* 13Dec2005  Motorola Inc.         Wake up select, and send notify
* 18Jan2007  Motorola Inc.         Don't lock socket on ipcNotify 
* 24Apr2007  Motorola Inc.         Added lock when find socket by serviceID
* 
*****************************************************************************************/
void ipcHandleCOCOConnReq(COCOConnRequestMsg_t* connReq)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
    COCOConnReplyMsg_t* reply = NULL;
    BOOL reqAccepted = FALSE;

/*--------------------------------------- CODE -----------------------------------------*/

    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleCOCOConnReq: Failed to lock SrvIDTable\n"));
        ipcFree(connReq);
        return;
    }
    socket = ipcFindSocketBySrvId(ipcOsalRead16(connReq->destSrvId));
    UNLOCK(SrvIDTable);

    if (ipcIsSocketValid(socket))
    {
        if (ipcLockSocket(socket) == IPCE_OK)
        {
            if (socket->state == IPC_SOCK_LISTENING)
            {
                /* XXX: remove all obsolete connRequests from the list */
                ipcRemoveObsoleteConnReq(socket);

                if ( ((UINT32)socket->newConnList.size < socket->backlog) ||
                        ( (0 == (UINT32)socket->newConnList.size) && 
                          (IPC_DIRECT_CONNECT == socket->backlog)))
                {
                    reqAccepted = TRUE;
                    connReq->tick = ipcOsalGetTickCount();
                    /* put the packet into newConnList 
                     * MUST add to the tail to consist with ipcRemoveObsoleteConnReq
                     */
                    ipcListAddTail(&socket->newConnList, (ipcListLink_t)connReq);

                    ipcOsalSemaSignal(socket->coSema);

                    /* the socket is ready for accepting */
                    wake_up_interruptible(&socket->readwq);
                }
            }
            ipcUnlockSocket(socket);
        }
        else
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcHandleCOCOConnReq: Failed to lock socket\n"));
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION,
                ("ipcHandleCOCOConnReq: Failed to find a valid socket\n"));
    }

    if (reqAccepted)
    {
        /* Send notification out */
        ipcNotify(socket, IPCREAD);
    }
    else
    {
        /* refuse the connection */
        reply = (void*)ipcCreateConnReply(ipcGetSrcAddr(connReq),
                ipcOsalRead16(connReq->mySrvId),
                0,
                ipcOsalRead32(connReq->cookie), 0, 0, 0);
        if (reply != NULL)
        {
            if (SSRTDataRequest((ipcPacket_t*)reply, IPC_CONTROL_CHANNEL_ID) < 0)
            {
                ipcWarning(IPCLOG_SESSION, ("Failed to send ConnReply msg\n"));
                ipcFree(reply);
            }
        }
        else
        {
            ipcWarning(IPCLOG_SESSION, ("Failed to allocate ConnReply msg\n"));
        }
        ipcFree(connReq);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleCOCOConnRep
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle connection reply message from router layer
*   
*   PARAMETER(S):
*       connRep: the connection reply message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
* 24Apr2007  Motorola Inc.         Added lock when find socket by serviceID
* 
*****************************************************************************************/
void ipcHandleCOCOConnRep(COCOConnReplyMsg_t* connRep)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
    UINT8 startSeqNum;
    UINT8 windowSize;
    UINT8 acceptConnect;
/*--------------------------------------- CODE -----------------------------------------*/

    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleCOCOConnRep: Failed to lock SrvIDTable\n"));
        return;
    }
        
    socket = ipcFindSocketBySrvId(ipcOsalRead16(connRep->peerSrvId));
    UNLOCK(SrvIDTable);
	
    if (ipcIsSocketValid(socket))
    {
        if (ipcLockSocket(socket) == IPCE_OK)
        {
            /* a valid message in this stage */
            if ((socket->state == IPC_SOCK_CONN_PENDING) &&
                    (socket->cookie == ipcOsalRead32(connRep->cookie)))
            {
                acceptConnect = ipcOsalRead8(connRep->acceptConnect);
                startSeqNum = ipcOsalRead8(connRep->startSeqNum);
                windowSize = ipcOsalRead8(connRep->windowSize);

                /* put some data into the socket struct */
                if (acceptConnect)
                {
                    socket->connAccepted = TRUE;
                    socket->destSrv = ipcOsalRead16(connRep->newSrvId);
                    socket->destNode = ipcAddrToNodeId(ipcGetSrcAddr(connRep));
                    socket->conn.nextTxSeqNum = startSeqNum;
                    socket->conn.maxTxSeqNum = startSeqNum + windowSize - 1;
                }
                else
                {
                    socket->connAccepted = FALSE;
                }

                ipcOsalSemaSignal(socket->coSema);
            }
            else
            {
                ipcWarning(IPCLOG_SESSION, ("Socket state not OK\n"));
            }
            ipcUnlockSocket(socket);
        }
        else
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcHandleCOCOConnRep: Failed to lock socket\n"));
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, ("ipcHandleCOCOConnRep: Can't find a valid socket\n"));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleCOCOConnComp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle connection complete msg from router layer
*   
*   PARAMETER(S):
*       connComp: the connection message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 03Dec2005  Motorola Inc.         Put the packet to socket
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
* 24Apr2007  Motorola Inc.         Added lock when find socket by serviceID and 
*                                  send ack before signal sema
* 
*****************************************************************************************/
void ipcHandleCOCOConnComp(COCOConnCompleteMsg_t* connComp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
    int rc = IPCEINVAL;
    BOOL paramsOK = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/

    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleCOCOConnComp: Failed to lock SrvIDTable\n"));
        ipcFree(connComp);
        return;
    }
    socket = ipcFindSocketBySrvId(ipcOsalRead16(connComp->destSrvId));
    UNLOCK(SrvIDTable);
	
    if (ipcIsSocketValid(socket))
    {
        if (ipcLockSocket(socket) == IPCE_OK)
        {
            if (!socket->isCOSocket || (socket->state != IPC_SOCK_ACCEPT_PENDING))
            {
                ipcWarning(IPCLOG_SESSION, 
                        ("ipcHandleCOCOConnComp: State not ok\n"));
            }
            else if (socket->cookie != ipcOsalRead32(connComp->cookie))
            {
                ipcWarning(IPCLOG_SESSION, 
                        ("ipcHandleCOCOConnComp: cookie not ok\n"));
            }
            else
            {
                paramsOK = TRUE;
            }
            ipcUnlockSocket(socket);
        }
        else
        {
            ipcWarning(IPCLOG_SESSION,
                    ("ipcHandleCOCOConnComp: Failed to find a valid socket\n"));
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION,
                ("ipcHandleCOCOConnComp: Failed to find a valid socket\n"));
    }
    if (paramsOK)
    {
        rc = ipcConnGotPacket(&socket->conn, (ipcPacket_t*)connComp);
        ipcOsalSemaSignal(socket->coSema);

    }
    if (rc != IPC_OK)
    {
        ipcFree(connComp);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleCOCOConnDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle connectio down message from router layer
*   
*   PARAMETER(S):
*       connDown: the connection down message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Sep2005  Motorola Inc.         Initial Creation
* 03Dec2005  Motorola Inc.         Change sock state to NOT_CONNECTED on this message
* 07Mar2006  Motorola Inc.         Wakeup senders on this message
* 30Jun2006  Motorola Inc.         Removed checking for isClosed
* 15Jan2007  Motorola Inc.         Added checking for cookie
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
* 24Apr2007  Motorola Inc.         Added lock when find socket br serviceID
* 
*****************************************************************************************/
void ipcHandleCOCOConnDown(COCOConnDownMsg_t* connDown)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
    BOOL clearSentPackets = FALSE;
    BOOL clearOutofOrderPackets = FALSE;
    BOOL shutdownReceive = FALSE;
    BOOL shutdownSend = FALSE;
    BOOL needRef = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/

    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleCOCOConnDown: Failed to lock SrvIDTable\n"));
        return;
    }
	
    socket = ipcFindSocketBySrvId(ipcOsalRead16(connDown->destSrvId));
    UNLOCK(SrvIDTable);
	
    if (ipcIsSocketValid(socket))
    {
        if (ipcLockSocket(socket) == IPCE_OK)
        {
            if (socket->cookie == ipcOsalRead32(connDown->cookie))
            {
                ipcRefSocket(socket);
                needRef = TRUE;
                if (socket->state == IPC_SOCK_CONNECTED)
                {
                    if ((ipcOsalRead8(connDown->how) & IPCSD_SEND) &&
                            (socket->peerCanSend))
                    {
                        clearOutofOrderPackets = TRUE;
                        socket->peerCanSend = FALSE;
                        shutdownSend = TRUE;
                    }
                    if ((ipcOsalRead8(connDown->how) & IPCSD_RECEIVE) &&
                            (socket->peerCanReceive))
                    {
                        clearSentPackets = TRUE;
                        socket->peerCanReceive = FALSE;
                        shutdownReceive = TRUE;
                    }
                    ipcConnDestroy(&socket->conn, clearSentPackets, clearOutofOrderPackets, FALSE);
                }
            }
            ipcUnlockSocket(socket);
            if (shutdownSend)
            {
                ipcCOSocketReceiveEnd(socket);
            }
            if (shutdownReceive)
            {
                ipcCOSocketTxWindowAvailable(&socket->conn);
            }

            if (needRef)
            {
                if (ipcLockSocket(socket) < 0)
                {
                    ipcError(IPCLOG_SESSION, 
                            ("ipcHandleCOCOConnDown: Failed to lock socket again\n"));
                    ipcUnrefSocket(socket);
                }
                else
                {
                    ipcUnrefSocket(socket);
                    ipcUnlockSocket(socket);
                }
            }
        }
        else
        {
            ipcWarning(IPCLOG_SESSION,
                    ("ipcHandleCOCOConnDown: Failed to lock socket\n"));
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION,
                ("ipcHandleCOCOConnDown: Failed to find a valid socket\n"));
    }

}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRegisterNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to register a notification on a socket.
*   
*   PARAMETER(S):
*       destAddr: Address of the node where the interested event happens
*       destService: The service id of the interested socket
*       flags: indicator specifying what type of notifications we care
*       msg: notification message
*       msglen: length of msg
*       localService: the one who receives the notification message
*       
*   RETURN: IPC_OK - success
*           IPCENOBUFS - failed to allocate packet
*           others - return value of SSRTDataRequest
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Mar2006  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         last param changed to service id 
* 22Jun2007  Motorola Inc.         Align messages in notifications
*
*****************************************************************************************/
int ipcRegisterNotification(ipcAddress_t destAddr,
                            ipcServiceId_t destService,
                            int flags,
                            const char* msg,
                            int msglen,
                            ipcServiceId_t localService)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSNotifyRegisterMsg_t* packet = NULL;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (NULL != (packet = (SSSSNotifyRegisterMsg_t*)ipcCreateSessionControlPacket(
                                    SSSS_NOTIFY_REGISTER_DATA_SIZE,
                                    IPC_MSG_SSSS_NOTIFY_REG,
                                    destAddr)))
    {
        ipcOsalWrite32(packet->flags, flags);
        memcpy(packet->msg, msg, (msglen + 1) & ~1);
        ipcOsalWrite32(packet->msglen, msglen);
        ipcOsalWrite16(packet->srcService, localService);
        ipcOsalWrite32(packet->psFlags, 0);
        ipcOsalWrite32(packet->q, 0);
        ipcOsalWrite16(packet->destService, destService);

        rc = SSRTDataRequest((ipcPacket_t*)packet, IPC_CONTROL_CHANNEL_ID);
        if (rc < 0)
        {
            ipcWarning(IPCLOG_SESSION, 
                    ("ipcRegisterNotification: SSRTDataRequest failed: %d\n", rc));
            ipcFree(packet);
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, 
                ("ipcRegisterNotification: Failed to create packet\n"));
        rc = IPCENOBUFS;
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRegisterPsNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to register a platform-specific notification on a socket.
*   
*   PARAMETER(S):
*       destAddr: Address of the node where the interested event happens
*       destService: The service id of the interested socket
*       flags: indicator specifying what type of notifications we care
*       msg: notification message
*       msglen: length of msg
*       psFlag: information of notificatin object/queue
*       q: platform-specific object/queue
*       
*   RETURN: IPC_OK - success
*           IPCENOBUFS - failed to allocate packet
*           others - return value of SSRTDataRequest
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Mar2006  Motorola Inc.         Initial Creation 
* 22Jun2007  Motorola Inc.         Align messages in notifications
*
*****************************************************************************************/
int ipcRegisterPsNotification(ipcAddress_t destAddr,
                              ipcServiceId_t destService,
                              int flags,
                              const char* msg,
                              int msglen,
                              int psFlags,
                              void* q)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSNotifyRegisterMsg_t* packet = NULL;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (NULL != (packet = (SSSSNotifyRegisterMsg_t*)ipcCreateSessionControlPacket(
                                    SSSS_NOTIFY_REGISTER_DATA_SIZE,
                                    IPC_MSG_SSSS_NOTIFY_REG,
                                    destAddr)))
    {
        ipcOsalWrite32(packet->flags, flags);
        memcpy(packet->msg, msg, (msglen + 1) & ~1);
        ipcOsalWrite32(packet->msglen, msglen);
        ipcOsalWrite16(packet->srcService, 0);
        ipcOsalWrite32(packet->psFlags, psFlags);
        ipcOsalWrite32(packet->q, ipcCastPtr(q, UINT32));
        ipcOsalWrite16(packet->destService, destService);

        rc = SSRTDataRequest((ipcPacket_t*)packet, IPC_CONTROL_CHANNEL_ID);
        if (rc < 0)
        {
            ipcWarning(IPCLOG_SESSION, 
                    ("ipcRegisterPsNotification: SSRTDataRequest failed: %d\n", rc));
            ipcFree(packet);
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, 
                ("ipcRegisterPsNotification: Failed to create packet\n"));
        rc = IPCENOBUFS;
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function removes notification of a specific type for a given socket.
*   
*   PARAMETER(S):
*       socket: pointer to the socket
*       notificationType: type of notification to be removed
*       
*   RETURN: void
*   
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Oct2005  Motorola Inc.         Initial Creation.
* 06Dec2005  Motorola Inc.         fixed misusage of ipcListNextPair
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
*
*****************************************************************************************/
void ipcRemoveNotification(ipcSocket_t* socket, int notificationType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcList_t tmpList;
    ipcNotification_t* notification;
    ipcListLink_t prev = NULL;
    ipcListLink_t curr = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcListInit(&tmpList);

    if (ipcLockSocket(socket) < 0)
    {
        return;
    }

    tmpList.head = (ipcListLink_t)socket->notifications;

    for (ipcListFirstPair(tmpList, prev, curr); 
         curr != NULL; )
    {
        notification = (ipcNotification_t*)curr;

        if (notification->flags & notificationType)
        {
            notification = (ipcNotification_t*)ipcListRemoveNextLink(&tmpList, prev);

            ipcFree(notification);
            
            if (prev == NULL)
            {
                curr = tmpList.head;
            }
            else
            {
                curr = prev->next;
            }
        }
        else
        {
            ipcListNextPair(prev, curr);
        }
    }
    socket->notifications = (ipcNotification_t*)tmpList.head;

    ipcUnlockSocket(socket);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSignalNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This functions signals a notification.
*   
*   PARAMETER(S):
*       notification: the notification to be signaled
*       socket: the notifying socket
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Dec2005  Motorola Inc.         Initial Creation
* 14Mar2006  Motorola Inc.         Only handle non-ps notification
* 23May2006  Motorola Inc.         Use IPC_INVALID_ADDR for local notifications
* 22Jun2007  Motorola Inc.         Align messages in notifications
*
*****************************************************************************************/
void ipcSignalNotification(ipcNotification_t* notification,
                           ipcServiceId_t srvId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int memtype = 0;
    ipcPacket_t* packet = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    memtype = ipcRouterGetMemType(notification->destination.ipcAddr);

    packet = (ipcPacket_t *)ipcAlloc(IPC_PACKET_DATA_OFFSET + 
        notification->msglen, memtype);

    if (packet != NULL)
    {
        if (notification->destination.ipcAddr == IPC_INVALID_ADDR)
        {
            /* IPC_INVALID_ADDR is stored for local notifications because
             * the local ipc addr can change (link up/link down) */
            ipcSetDestAddr(packet, ipcStack_glb.ipcAddr);
        }
        else
        {
            ipcSetDestAddr(packet, notification->destination.ipcAddr);
        }

        ipcSetSrcAddr(packet, ipcStack_glb.ipcAddr);
        ipcSetSrcServiceId(packet, srvId);
        ipcSetDestServiceId(packet, notification->destination.u.serviceId);
        ipcSetMsgLength(packet, notification->msglen);

        memcpy(ipcGetMsgData(packet), notification->msg, (notification->msglen + 1) & ~1);
        if (notification->flags & IPCPRIONOTIFY)
        {
            ipcSetMsgFlags(packet, IPC_PACKET_HIGH_PRIORITY);
        }
        else
        {
            ipcSetMsgFlags(packet, 0);
        }

        if (SSRTDataRequest(packet, IPC_NONDEDICATED_CHANNEL_ID) != IPC_OK)
        {
            ipcWarning(IPCLOG_SESSION, 
                    ("ipcSignalNotification: Failed to send notification\n"));
            ipcFree(packet);
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, 
                ("ipcSignalNotification: Failed to allocate packet\n"));
    } /* packet == NULL */
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSignalPsNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function signals a platform-specific notification. It constructs
*   a message tell the session layer to put the notification
*   to a platform-specific queue.
*   
*   PARAMETER(S):
*
*   RETURN: 
*       IPC_OK - success
*       IPCENOBUFS - failed to allocate packet
*       others - return value of SSRTDataRequest
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Oct2005  Motorola Inc.         Initial Creation.
* 14Mar2006  Motorola Inc.         Removed srcServiceId
* 23May2006  Motorola Inc.         Use IPC_INVALID_ADDR for local notifications
* 22Jun2007  Motorola Inc.         Align messages in notifications
*
*****************************************************************************************/
void ipcSignalPsNotification(ipcNotification_t *notification)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    SSSSNotifySignalMsg_t *packet = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    /* NOTE: A little optimization for local notification */
    if (notification->destination.ipcAddr == IPC_INVALID_ADDR)
    {
        /* IPC_INVALID_ADDR is stored for local notifications because
         * the local ipc addr can change (link up/link down) */
        ipcPlatformNotify(notification);
    }
    else if ((NULL != (packet = (SSSSNotifySignalMsg_t*)ipcCreateSessionControlPacket
                    (SSSS_NOTIFY_SIGNAL_DATA_SIZE,
                    IPC_MSG_SSSS_NOTIFY_SIGNAL,
                    notification->destination.ipcAddr))))
    {
        ipcOsalWrite32(packet->flags, notification->flags);
        memcpy(packet->msg, notification->msg, IPC_NOTIFICATION_MAX_SIZE);
        ipcOsalWrite32(packet->msglen, notification->msglen);
        ipcOsalWrite32(packet->psFlags, notification->psFlags);
        ipcOsalWrite32(packet->q, ipcCastPtr(notification->destination.u.ps, UINT32));

        if (SSRTDataRequest((ipcPacket_t*)packet, IPC_CONTROL_CHANNEL_ID) != IPC_OK)
        {
            ipcWarning(IPCLOG_SESSION, ("Failed to send NotifySignal msg\n"));
            ipcFree(packet);
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, 
                ("ipcSignalPsNotification: Failed to create packet\n"));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcNotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to send out any requested notifications when an event
*   occurs.  It will call a platform specific function to handle notifications
*   requested through ipcpsnotify.
*   
*   PARAMETER(S):
*       socket: socket to check for required notifications
*       notificationType: type of notification required
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Oct2005  Motorola Inc.         Initial Creation.
* 24Oct2005  Motorola Inc.         Added remote notification
* 14Mar2006  Motorola Inc.         Splitted ps notification
* 18Jan2007  Motorola Inc.         Cached notifications before signaling 
* 22Jun2007  Motorola Inc.         Align messages in notifications
*
*****************************************************************************************/
void ipcNotify(ipcSocket_t *socket, int notificationType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcNotification_t *notification;
    ipcList_t backupNotifications;
    ipcNotification_t *tempNotification;
    ipcServiceId_t srvId;
/*--------------------------------------- CODE -----------------------------------------*/

    ipcListInit(&backupNotifications);

    /* If no notification is registered on this socket */
    if (socket->notifications == NULL)
    {
        return;
    }

    /* duplicate notifications to a temp list */
    if (ipcLockSocket(socket) < 0)
    {
        return;
    }
    srvId = socket->srvId;
    for (notification = socket->notifications;
         notification != NULL;
         notification = notification->next)
    {
        if (notification->flags & notificationType)
        {
            tempNotification = ipcAlloc(sizeof(ipcNotification_t), 0);
            if (tempNotification != NULL)
            {
                memcpy(tempNotification, notification, sizeof(ipcNotification_t));
                ipcListAddTail(&backupNotifications, (ipcListLink_t)tempNotification);
            }
            else
            {
                ipcWarning(IPCLOG_SESSION, ("Failed to duplicate notification\n"));
                break;
            }
        }
    } /*  for */
    ipcUnlockSocket(socket);

    while (NULL != (notification = (ipcNotification_t*)ipcListRemoveHead(&backupNotifications)))
    {
        if (notification->psFlags == 0)
        {
            ipcSignalNotification(notification, srvId);
        }
        else
        {
            ipcSignalPsNotification(notification);
        }
        ipcFree(notification);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleNotifySignal
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This functions handles a notification signal message from network.
*   
*   PARAMETER(S):
*       msg: the notification signal message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Dec2005  Motorola Inc.         Initial Creation
* 14Mar2006  Motorola Inc.         Always truct remote notifications 
* 22Jun2007  Motorola Inc.         Align messages in notifications
*
*****************************************************************************************/
void ipcHandleNotifySignal(SSSSNotifySignalMsg_t* msg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcNotification_t notification;
/*--------------------------------------- CODE -----------------------------------------*/

    notification.flags = ipcOsalRead32(msg->flags);
    memcpy(notification.msg, msg->msg, IPC_NOTIFICATION_MAX_SIZE);
    notification.msglen = ipcOsalRead32(msg->msglen);
    notification.psFlags = ipcOsalRead32(msg->psFlags);
    notification.destination.u.ps = (void*)ipcOsalRead32(msg->q);

    ipcPlatformNotify(&notification);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleNotifyRegister
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This functions handles a notification register message from network.
*   
*   PARAMETER(S):
*       msg: the notification register message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Dec2005  Motorola Inc.         Initial Creation
* 14Mar2006  Motorola Inc.         Simplified ps notifications
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
* 24Apr2007  Motorola Inc.         Added lock when find socket by serviceID
* 22Jun2007  Motorola Inc.         Align messages in notifications
* 
*****************************************************************************************/
void ipcHandleNotifyRegister(SSSSNotifyRegisterMsg_t* msg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socketToRegister = NULL;
    ipcNotification_t* notification = NULL;
    int flags = ipcOsalRead32(msg->flags);
    int msglen = ipcOsalRead32(msg->msglen);
    int psFlags = ipcOsalRead32(msg->psFlags);
/*--------------------------------------- CODE -----------------------------------------*/

    /* find the specific socket */
    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
            ("ipcHandleNotifyRegister: Failed to lock SrvIDTable\n"));
        return;
    }

    socketToRegister = ipcFindSocketBySrvId(ipcOsalRead16(msg->destService));
    UNLOCK(SrvIDTable);
	
    if (socketToRegister != NULL)
    {
        if (msglen == 0)
        {
            ipcWarning(IPCLOG_SESSION, ("Removing notification on socket %p\n", 
                socketToRegister));
            ipcRemoveNotification(socketToRegister, flags);
        }
        else
        {
            notification = ipcAlloc(sizeof(ipcNotification_t), 0);
            if (notification != NULL)
            {
                notification->flags = flags;
                notification->psFlags = psFlags;
                notification->destination.ipcAddr = ipcGetSrcAddr(msg);
                if (notification->psFlags == 0)
                {
                    notification->destination.u.serviceId = ipcOsalRead16(msg->srcService);
                }
                else
                {
                    notification->destination.u.ps = (void*)ipcOsalRead32(msg->q);
                }
                notification->msglen = msglen;
                memcpy(notification->msg, msg->msg, IPC_NOTIFICATION_MAX_SIZE);

                /* Insert new notification into socket */
                if (ipcLockSocket(socketToRegister) < 0)
                {
                    ipcFree(notification);
                    return;
                }
                notification->next = socketToRegister->notifications;
                socketToRegister->notifications = notification;
                ipcUnlockSocket(socketToRegister);
            }
            else
            {
                ipcWarning(IPCLOG_SESSION,
                    ("Failed to allocate notification struct\n"));
            }
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, ("ipcHandleNotifyRegister: no socket found\n"));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSignalServiceNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to signal a "service notification", a "service 
*   notification" indicates a node or service is noline or offline. The "service
*   notification" will be removed after being signaled. If flags is set to 0, 
*   then the notification will be removed without signaling a notification.
*   
*   PARAMETER(S):
*       node: the node ID where the event happens
*       service: the service ID of the event
*       flags: IPCONLINE or IPCOFFLINE or (IPCONLINE | IPCOFFLINE) or 0
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         Cached notifications before sending
* 02Mar2007  Motorola Inc.         Merge bug fix from old vob 
* 21Aug2007  Motorola Inc.         Fixed OSS compliance issue
*
*****************************************************************************************/
void ipcSignalServiceNotification(ipcNodeId_t node,
                                  ipcServiceId_t service,
                                  int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcServiceNotification_t* notification;
    ipcListLink_t prev;
    ipcListLink_t curr;
    ipcList_t tempList;
    ipcList_t backupNotifications;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcListInit(&tempList);
    ipcListInit(&backupNotifications);

    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcSignalServiceNotification: Failed to lock DRT table!\n"));
        return;
    }
    tempList.head = (ipcListLink_t)ipcSession_glb.serviceNotifications;
    for (ipcListFirstPair(tempList, prev, curr);
        curr != NULL;)
    {
        notification = (ipcServiceNotification_t*) curr;

        if (( (notification->node == node) ||
              (notification->node == IPC_NODE_ANY)) && /* Check node */
            (notification->service == service) && /* check service */
            ( (notification->notification.flags & flags) || /* check flags */
              (flags == 0))) /* remove notification */
        {
            /* NOTE: The address of the node may be changed...
             *
             * For example, while a standalone client gets connected to an IPC
             * network, the address is changed from IPC_INVALID_ADDR to a valid
             * one, so we have to re-assign the dest address of notification */
            notification->notification.destination.ipcAddr = ipcStack_glb.ipcAddr;
            ipcListRemoveNextLink(&tempList, prev);
            if (flags != 0)
            {
                ipcListAddTail(&backupNotifications, (ipcListLink_t)notification);
            }
            else
            {
                ipcFree(notification);
            }

            if (prev == NULL)
            {
                curr = tempList.head;
            }
            else
            {
                curr = prev->next;
            }
        }
        else
        {
            ipcListNextPair(prev, curr);
        }
    }
    ipcSession_glb.serviceNotifications = (ipcServiceNotification_t*)tempList.head;
    UNLOCK(DRTTable);

    while (backupNotifications.size > 0)
    {
        notification = (ipcServiceNotification_t*)ipcListRemoveHead(&backupNotifications);
        if (notification == NULL)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcSignalServiceNotification: Failed to remove notification from list\n"));
            break;
        }

        if (notification->notification.psFlags == 0)
        {
            ipcSignalNotification(&notification->notification, 0);
        }
        else
        {
            ipcSignalPsNotification(&notification->notification);
        }
        ipcFree(notification);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRegisterServiceNotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Register a "service notification". If the condition is already satisfied,
*   then a notification will be sent immediately, otherwise, it will add the
*   notification to a global list, and the notification will be sent after the
*   interested event happened. 
*   
*   PARAMETER(S):
*       localSrvId: socket that will receive asynchronous notifications
*       nodeId: node id of socket that will cause notifications
*       srvId: service id of socket that will cause notifcations
*       msg: data to be transmitted of the notification
*       msglen: length of data to be transmitted of the notification
*       flags: type of notification the socket requires
*       lasterror: [out] error code
*       
*   RETURN: IPC_OK if no error happens, IPC_ERROR on error conditions, the error
*           code will be stored in *lasterror
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         First param changed to srv id 
* 22Jun2007  Motorola Inc.         Align messages in notifications
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcRegisterServiceNotify(ipcServiceId_t localSrvId,
                             ipcNodeId_t nodeId,
                             ipcServiceId_t srvId,
                             const char* msg,
                             int msglen,
                             int flags,
                             int* lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
    ipcServiceNotification_t* notification = NULL;
    BOOL needSignal = FALSE;

/*--------------------------------------- CODE -----------------------------------------*/
    if (((srvId != IPC_SERVICE_ANY) && !ipcIsPublishedServiceId(srvId)) || 
        ((nodeId == IPC_NODE_ANY) && (srvId == IPC_SERVICE_ANY)))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
    if (msglen == 0)
    {
        /* if msglen is 0, then remove the notification */
        ipcSignalServiceNotification(nodeId, srvId, 0);
        return IPC_OK;
    }

    notification = ipcAlloc(sizeof(ipcServiceNotification_t), 0);

    if (notification != NULL)
    {
        notification->node = nodeId;
        notification->service = srvId;
        notification->notification.destination.ipcAddr = ipcStack_glb.ipcAddr;
        notification->notification.destination.u.serviceId = localSrvId;
        notification->notification.flags = flags;
        memcpy(notification->notification.msg, msg, (msglen + 1) & ~1);
        notification->notification.msglen = msglen;
        notification->notification.next = NULL;
        notification->notification.psFlags = 0;

        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION, 
                    ("ipcRegisterServiceNotify: Failed to lock DRT table\n"));
            ipcFree(notification);
            rc = IPCEBUSY;
        }
        else
        {
            if (ipcNeedSignalServiceNotification(notification))
            {
                needSignal = TRUE;
            }
            else
            {
                notification->notification.next = 
                    (ipcNotification_t*)ipcSession_glb.serviceNotifications;
                ipcSession_glb.serviceNotifications = notification;
            }
            UNLOCK(DRTTable);
            if (needSignal)
            {
                ipcSignalNotification(&notification->notification, 0);
                ipcFree(notification);
            }
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, 
            ("Failed to allocate notification struct\n"));
        rc = IPCENOBUFS;
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRegisterServicePsNotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Register a "service notification". If the condition is already satisfied,
*   then a notification will be sent immediately, otherwise, it will add the
*   notification to a global list, and the notification will be sent after the
*   interested event happened. 
*   
*   PARAMETER(S):
*       q: platform specific queue that receives the notification
*       qflags: flag indication type of the queue
*       nodeId: node id of socket that will cause notifications
*       srvId: service id of socket that will cause notifcations
*       msg: data to be transmitted of the notification
*       msglen: length of data to be transmitted of the notification
*       flags: type of notification the socket requires
*       lasterror: [out] error code
*       
*   RETURN: IPC_OK if no error happens, IPC_ERROR on error conditions, the error
*           code will be stored in *lasterror
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
* 22Jun2007  Motorola Inc.         Align messages in notifications
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcRegisterServicePsNotify(void* q,
                               int qflags,
                               ipcNodeId_t nodeId,
                               ipcServiceId_t srvId,
                               const char* msg,
                               int msglen,
                               int flags,
                               int* lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
    ipcServiceNotification_t* notification = NULL;
    BOOL needSignal = FALSE;

/*--------------------------------------- CODE -----------------------------------------*/
    if (((srvId != IPC_SERVICE_ANY) && !ipcIsPublishedServiceId(srvId)) || 
        ((nodeId == IPC_NODE_ANY) && (srvId == IPC_SERVICE_ANY)))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
    if (msglen == 0)
    {
        /* if msglen is 0, then remove the notification */
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcRegisterServicePsNotify: Failed to lock DRT table\n"));
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        ipcSignalServiceNotification(nodeId, srvId, 0);
        UNLOCK(DRTTable);
        return IPC_OK;
    }

    notification = ipcAlloc(sizeof(ipcServiceNotification_t), 0);

    if (notification != NULL)
    {
        notification->node = nodeId;
        notification->service = srvId;
        notification->notification.destination.ipcAddr = ipcStack_glb.ipcAddr;
        notification->notification.destination.u.ps = q;
        notification->notification.flags = flags;
        memcpy(notification->notification.msg, msg, (msglen + 1) & ~1);
        notification->notification.msglen = msglen;
        notification->notification.next = NULL;
        notification->notification.psFlags = qflags;

        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION, 
                    ("ipcRegisterServiceNotify: Failed to lock DRT table\n"));
            ipcFree(notification);
            rc = IPCEBUSY;
        }
        else
        {
            if (ipcNeedSignalServiceNotification(notification))
            {
                needSignal = TRUE;
            }
            else
            {
                notification->notification.next = 
                    (ipcNotification_t*)ipcSession_glb.serviceNotifications;
                ipcSession_glb.serviceNotifications = notification;
            }
            UNLOCK(DRTTable);
            if (needSignal)
            {
                ipcSignalPsNotification(&notification->notification);
                ipcFree(notification);
            }
        }
    }
    else
    {
        ipcWarning(IPCLOG_SESSION, 
            ("Failed to allocate notification struct\n"));
        rc = IPCENOBUFS;
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcNeedSignalServiceNotification
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Try to signal the "service notification", if the condition of the 
*   notification already holds, it will return TRUE, otherwise it returns FALSE;
*   
*   NOTE: This function assumes DRT table is locked. You should call 
*   LOCK(DRTTable) before invoking this function.
*   
*   PARAMETER(S):
*       notification: pointer of the "service notification"
*       
*   RETURN: TRUE if notification is signaled, otherwise FALSE
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
*
*****************************************************************************************/
BOOL ipcNeedSignalServiceNotification(ipcServiceNotification_t* notification)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL haveFound = FALSE;
    BOOL shouldNotify = FALSE;
    ipcDRTItem_t* item = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    if (notification->node == IPC_NODE_ANY)
    {
        for (item = (ipcDRTItem_t *)ipcSession_glb.drt.head;
            item != NULL;
            item = item->next)
        {
            if (ipcListFind(&item->srvId_list, ipcCastPtr(notification->service, void*),
                            ipcListCompareDrtSrvId, NULL))
            {
                /* We found the service ID so set the address and break */
                haveFound = TRUE;
                break;
            }
        }
    }
    else if (notification->service == IPC_SERVICE_ANY)
    {
        item = ipcNodeIdToDrtItemWithoutLock(notification->node);
        if (item != NULL)
        {
            haveFound = TRUE;
        }
    }
    else
    {
        /* normal case */
        item = ipcNodeIdToDrtItemWithoutLock(notification->node);
        if (item != NULL)
        {
            haveFound = ipcServiceInDRTItem(item, notification->service, NULL);
        }
    }

    /* NOTE: The following if/else statements can be simplified as:
     *
     *   if (notification->notification.flag & (haveFound? IPCONLINE: IPCOFFLINE))
     *   {
     *       send out notification;
     *       return TRUE;
     *   }
     *   return FALSE;
     *
     *   However, K.I.S.S. (Keep It Simple & Stupid!) */
    if (haveFound)
    {
        if (notification->notification.flags & IPCONLINE)
        {
            shouldNotify = TRUE;
        }
    }
    else
    {
        if (notification->notification.flags & IPCOFFLINE)
        {
            shouldNotify = TRUE;
        }
    }

    return shouldNotify;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDisconnectSocket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This functions disconnects a socket
*   
*   PARAMETER(S):
*       socket: the socket to be disconnected
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Dec2005  Motorola Inc.         Initial Creation
* 18Jan2007  Motorola Inc.         Added checking for lock failure 
*
*****************************************************************************************/
void ipcDisconnectSocket(ipcSocket_t* socket)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    ipcchan_imp(socket, 0, 0, &rc);
    if (ipcLockSocket(socket) < 0)
    {
        return;
    }
    socket->destNode = 0;
    socket->destSrv = 0;
    socket->canReceive = TRUE;
    socket->peerCanReceive = TRUE;
    socket->canSend = TRUE;
    socket->peerCanSend = TRUE;
    socket->state = IPC_SOCK_NOT_CONNECTED;
    ipcUnlockSocket(socket);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCOSocketReceiveEnd
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This functions puts an invalid packet to the end of a CO socket queue
*   
*   PARAMETER(S):
*       socket: pointer to the socket
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2005  Motorola Inc.         Initial Creation
* 01Nov2006  Motorola Inc.         Added rxWindowSize sync
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 02Mar2007  Motorola Inc.         merge bug fix from old vob
* 21Aug2007  Motorola Inc.         Fixed OSS compliance issue
*
*****************************************************************************************/
int ipcCOSocketReceiveEnd(ipcSocket_t* socket)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t* packet = NULL;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    packet = ipcAlloc(IPC_PACKET_DATA_OFFSET + 1, IPC_PROCSHM);
    if (packet == NULL)
    {
        ipcWarning(IPCLOG_SESSION, 
                ("ipcCOSocketReceiveEnd: Failed to allocate packet\n"));
        return IPCENOBUFS;
    }

    ipcSetSrcServiceId(packet, IPC_SERVICE_ANY);
    ipcSetDestServiceId(packet, IPC_SERVICE_ANY);

    ipcSetMsgFlags(packet, IPC_PACKET_HIGH_PRIORITY);

    /* NOTE: The packet is inserted in the high-priority queue */
    if (ipcLockSocket(socket) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcCOSocketReceiveEnd: Failed to lock socket\n"));
        return IPCEBUSY;
    }
    /* XXX: Always decrease the rxWindowSize. If the original rxWindowSize is 0,
     * this will make it -1, it harms nothing if the socket is no longer used.
     *
     * However, if the socket is to be used in another connection, the rxWindowSize
     * will be increased in the function ipczrecvfrom_imp, it gets out the receive
     * end packet, calls ipcConnConsumeData to enlarge the window size, after that
     * the window size is still correct.
     */
    socket->conn.rxWindowSize--;
    ipcUnlockSocket(socket);

    rc = ipcInsertPacketIntoQueue(socket, packet);
    
    if (rc < 0)
    {
        ipcConnConsumeData(&socket->conn, 
                ipcNodeIdToIpcAddr(socket->destNode), 
                socket->destSrv);
        ipcFree(packet);
    }
    return rc;
}
