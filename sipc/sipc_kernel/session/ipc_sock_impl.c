/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_sock_impl.c
*   FUNCTION NAME(S): ipcaccept_imp
*                     ipcbind_imp
*                     ipcchan_imp
*                     ipcclosesocket_imp
*                     ipcconnect_imp
*                     ipcgetmemtype_imp
*                     ipcgetpeername_imp
*                     ipcgetsockname_imp
*                     ipcgetstatus_imp
*                     ipcgetversion_imp
*                     ipclisten_imp
*                     ipclocateservice_imp
*                     ipcnotify_imp
*                     ipcpsnotify_imp
*                     ipcsetsockqlen_imp
*                     ipcsetwatermark_imp
*                     ipcshutdown_imp
*                     ipcsocket_imp
*                     ipczrecv_imp
*                     ipczrecvfrom_imp
*                     ipczsend_imp
*                     ipczsendto_imp
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements a series of functions used to handle user requests.
*   User side APIs are directed to these functions.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 10Jan2004  Motorola Inc.         Initial Creation
* 16Jun2005  Motorola Inc.         Physical Shm & Virtural Shm Integration
* 26Jul2005  Motorola Inc.         Changed ipcremchan_imp so it always frees session channelInfo data
* 26Jul2005  Motorola Inc.         Call ipcLogPacket from ipczsendto_imp
* 19Aug2005  Motorola Inc.         Make other modules also can create Channels in kernel space.
* 23Sep2005  Motorola Inc.         Added CO socket support
* 23Nov2005  Motorola Inc.         Add delay in BP send func to make  AP-BP connection more stable
* 23Dec2005  Motorola Inc.         Delete delay when send msg and correct some error code
* 29Mar2006  Motorola Inc.         Add check in locate service
* 04May2006  Motorola Inc.         Make addrlen check in ipcconnect backwards compatible
* 05May2006  Motorola Inc.         Set ipcAddr field in ipcpsnotify
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 01Dec2006  Motorola Inc.         Support DSM message discard               
* 15Jan2007  Motorola Inc.         Include ipc_macro_defs.h
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
* 05Jul2007  Motorola Inc.         Ignore waiting failure for complete message in ipcaccept
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 21Aug2007  Motorola Inc.         Fixed OSS compliance issue
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_macro_defs.h"
#include "ipc_sock_impl.h"
#include "ipc_session.h"
#include "ipc_defs.h"
#include "ipc_shared_memory.h"
#include "ipc_router_interface.h"
#include "ipclog.h"
#include "ipcsocket_osal.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetstatus_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get the status of the ipc stack. If the state of stack is not OK, it returns
*   IPCERROR, the return value is stored in handle->lastError
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: If state is OK, returns 0, else IPCENOTREADY
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jan2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcgetstatus_imp(int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    switch (ipcSession_glb.state)
    {
    case IPC_SESSION_STATE_NULL:
        *lasterror = IPCENOTREADY;
        ret = IPC_ERROR;
        break;
    }

    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetversion_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get the version of IPC stack. The value is stored in *version.
*   
*   PARAMETER(S):
*       version: location to store the version value
*       
*   RETURN: If OK, returns 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jan2005  Motorola Inc.         Initial Creation
* 24Jan2005  Motorola Inc.         Check for input argument strictly 
*
*****************************************************************************************/
int ipcgetversion_imp(unsigned long *version, int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (version != NULL)
    {
        *version = IPC_VERSION;
    }
    else
    {
        *lasterror = IPCEFAULT;
        ret = IPC_ERROR;
    }

    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcchan_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates a QoS channel associated with a socket.
*   
*   PARAMETER(S):
*       socket: the pointer to a socket (either CL or CO)
*       qos: Quality of service requested in bytes/sec.
*       flag: Set of possible bit wise flags (only 0 is allowed now)
*       
*   RETURN: If an error occurs, a value of IPC_ERROR is returned, and specific
*           error code can be retrieved by calling ipcgetlasterror.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Oct2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Added support for removing chan
* 06Dec2006  Motorola Inc.         For physical share memory connection needn't setup channel.
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcchan_imp(ipcSocket_t* socket, 
            int qos,
            int flag,
            int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcAddress_t ipcAddr;
    ipcPortId_t portId;
    int rc = 0;
    ipcNodeId_t destNode;
    ipcServiceId_t destService;
    ipcServiceId_t srvId;
    ipcChannelId_t channelId;
    BOOL finallyOK = FALSE;

/*--------------------------------------- CODE -----------------------------------------*/
    
    if (!ipcIsSocketValid(socket))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    /* NOTE: In current implementation, flag MUST be set to 0! */
    if (flag != 0)
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    if (qos < 0)
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    if (ipcLockSocket(socket) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }
    destNode = socket->destNode;
    destService = socket->destSrv;
    channelId = socket->channelId;
    srvId = socket->srvId;
    ipcUnlockSocket(socket);

    /* check whether the socket has a dest address */
    if (destNode == IPC_NODE_ANY)
    {
        *lasterror = IPCENOTCONN;
        return IPC_ERROR;
    }

    /* check whether the dest address of a socket is the self node */
    if (destNode == ipcStack_glb.nodeId)
    {
        /* connect to ourself? return OK */
        return IPC_OK;
    }

    ipcAddr = ipcNodeIdToIpcAddr(destNode);

    if (ipcAddr == IPC_INVALID_ADDR)
    {
        ipcError(IPCLOG_SESSION, 
                ("ipcchan_imp: no addr for node %d\n", socket->destNode));
        *lasterror = IPCENETUNREACH;
        return IPC_ERROR;
    }
    portId = ipcStack_glb.ipcAddrToPortTable[ipcAddr];
    /*To physical share memory connection, just return OK*/
    if (ipcStack_glb.ports[portId].deviceType == IPC_DEVICE_SHARED_MEMORY)
    {
        return IPC_OK;
    }

    if (qos == 0)
    {
        /* if qos == 0, then the previously defined channel will be removed */
        if (channelId != IPC_NONDEDICATED_CHANNEL_ID)
        {
            if (ipcAddr == IPC_INVALID_ADDR)
            {
                ipcError(IPCLOG_SESSION, 
                        ("ipcchan_imp: no addr for node %d\n", destNode));
                *lasterror = IPCENETUNREACH;
                return IPC_ERROR;
            }
            rc = SSRTChannelRelRequest(channelId,
                ipcAddr, 
                srvId);
            if (rc < 0)
            {
                *lasterror = rc;
                return IPC_ERROR;
            }
        }
        if (ipcLockSocket(socket) < 0)
        {
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }

        /* only set the channelId to initial value */
        if ((socket->state == IPC_SOCK_CONNECTED) &&
            (socket->destNode == destNode) &&
            (socket->channelId == channelId))
        {
            socket->channelId = IPC_NONDEDICATED_CHANNEL_ID;
        }
        ipcUnlockSocket(socket);

        return IPC_OK;
    }

    if (channelId != IPC_NONDEDICATED_CHANNEL_ID)
    {
        /* close the original channel, here looks like a recursive call */
        if (ipcchan_imp(socket, 0, 0, lasterror) < 0)
        {
            ipcError(IPCLOG_SESSION, ("Can't close the original channel\n"));
            return IPC_ERROR;
        }
    }

    /* Send request to router */
    rc = SSRTChannelRequest(ipcAddr, qos, srvId);

    if (rc == IPCENOTREADY)
    {    
        ipcDebugLog(IPCLOG_SESSION, ("Waiting for multihop channel\n"));
        /* This is a multihop channel request so we need to wait for the router's channel reply.
         * The router already locked the multihop channel mutex so no other multihop channel
         * requests can be processed until we unlock it here */
        if (ipcOsalSemaWait(ipcStack_glb.multihopChannelSema,
                            TIMEOUT_MULTIHOP_CHANNEL_REQ) == IPCE_TIMEOUT)
        {
            ipcError(IPCLOG_SESSION, ("Multihop channel request timed out\n"));

            SSRTChannelRelRequest(ipcRouterGetPendingChannelId(), 
                ipcAddr, 
                srvId);
            rc = IPCENETUNREACH;
        }
        else
        {
            rc = ipcStack_glb.multihopChannelReply;
            ipcDebugLog(IPCLOG_SESSION, ("Multihop channel request reply = %d\n", rc));
        }
        ipcOsalMutexRelease(ipcStack_glb.multihopChannelMutex);
    }

    if (rc < 0)
    {
        *lasterror = rc;
        return IPC_ERROR;
    }
    else
    {
        if (ipcIsSocketValid(socket))
        {
            if (ipcLockSocket(socket) == IPCE_OK)
            {
                if ((socket->state == IPC_SOCK_CONNECTED) &&
                        (socket->destNode == destNode) &&
                        (socket->channelId == IPC_NONDEDICATED_CHANNEL_ID))
                {
                    socket->channelId = rc;
                    finallyOK = TRUE;
                }
                else
                {
                    *lasterror = IPCEINVAL;
                }
                ipcUnlockSocket(socket);
            }
            else
            {
                ipcError(IPCLOG_SESSION,
                        ("ipcchan_imp: Failed to lock socket\n"));
                *lasterror = IPCEBUSY;
            }
        }

        if (!finallyOK)
        {
            SSRTChannelRelRequest(rc, ipcAddr, srvId);
        }
    }
    return finallyOK? IPC_OK: IPC_ERROR;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipclocateservice_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get one or some services at the IPC network, each service is represented in
*   struct IPCSERVICE_LOCATION, which is just a pair of <nodeId, serviceId>, and
*   the returned services are filled in ipc_servicetable.
*   if ipc_node is equal to IPC_NODE_ANY, it queries services for all nodes
*   if ipc_service is equal to IPC_SERVICE_ANY, it gets all the PSIDs in the
*   node(or nodes, if ipc_node is IPC_NODE_ANY).
*   
*   PARAMETER(S):
*       ipc_node: specific node id or IPC_NODE_ANY
*       ipc_service: specific PSID or IPC_SERVICE_ANY
*       ipc_servicetable: this is an array of node id and service id pairs
*       ipc_sizeof_servicetable: pointer to return number of service pairs
*       ipc_maxsizeofservicetable: max size of entries in the table
*       
*   RETURN: If OK returns 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2004  Motorola Inc.         Initial Creation
* 13Jan2004  Motorola Inc.         Changed definition of IPC_NODE_ANY and IPC_SERVICE_ANY
* 27Jan2005  Motorola Inc.         Check whether ipc_maxsizeofservicetable is 0
* 27Jul2005  Motorola Inc.         Fixed return value bug
* 29Mar2006  Motorola Inc.         Add check in locate service
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipclocateservice_imp(unsigned long ipc_node,
                         unsigned short ipc_service,
                         struct IPCSERVICE_LOCATION *ipc_servicetable,
                         unsigned long *ipc_sizeofservicetable,
                         unsigned long ipc_maxsizeofservicetable,
                         int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long cnt = 0;
    int ret = 0;
    ipcDRTItem_t *drt_item;
    ipcDRTListLink_t srvIdLink;
/*--------------------------------------- CODE -----------------------------------------*/
    /*  argument checking   */
    if ((ipc_servicetable == NULL) || (ipc_sizeofservicetable == NULL) ||
        (ipc_maxsizeofservicetable == 0))
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }
        
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipclocateservice_imp: Failed to lock DRT table\n"));
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }
    switch (ipcLocateServiceType(ipc_node, ipc_service))
    {
    case QUERY_NETWORK:
        for (drt_item = (ipcDRTItem_t *)ipcSession_glb.drt.head;
             (drt_item != NULL) && (ret != IPC_ERROR);
             drt_item = drt_item->next)
        {
            for (srvIdLink = (ipcDRTListLink_t)drt_item->srvId_list.head;
                 (srvIdLink != NULL) && (ret != IPC_ERROR);
                 srvIdLink = srvIdLink->next)
            {
                if (cnt < ipc_maxsizeofservicetable)
                {
                    ipc_servicetable[cnt].ipc_node = drt_item->node_id;
                    ipc_servicetable[cnt].ipc_service = srvIdLink->data.srvId;
                    cnt++;
                }
                else
                {
                    *lasterror = IPCESERVICETABLE;
                    ret = IPC_ERROR;
                }
            }
        }
        break;

    case QUERY_ALL_SRV_IN_ONE_NODE:
        drt_item = ipcNodeIdToDrtItemWithoutLock(ipc_node);
        if (NULL == drt_item)
        {
            *lasterror = IPCENODE;
            ret = IPC_ERROR;
            break;
        }
        else
        {
            for (srvIdLink = (ipcDRTListLink_t)drt_item->srvId_list.head;
                 (srvIdLink != NULL) && (ret != IPC_ERROR);
                 srvIdLink = srvIdLink->next)
            {
                if (cnt < ipc_maxsizeofservicetable)
                {
                    ipc_servicetable[cnt].ipc_node = drt_item->node_id;
                    ipc_servicetable[cnt].ipc_service = srvIdLink->data.srvId;
                    cnt++;
                }
                else
                {
                    *lasterror = IPCESERVICETABLE;
                    ret = IPC_ERROR;
                }
            }
        }
        break;

    case QUERY_NODE_LIST:
        for (drt_item = (ipcDRTItem_t *)ipcSession_glb.drt.head;
             (drt_item != NULL) && (ret != IPC_ERROR);
             drt_item = drt_item->next)
        {
            if (ipcListFind(&drt_item->srvId_list, ipcCastPtr(ipc_service, void*),
                            ipcListCompareDrtSrvId, NULL))
            {
                if (cnt < ipc_maxsizeofservicetable)
                {
                    ipc_servicetable[cnt].ipc_node = drt_item->node_id;
                    ipc_servicetable[cnt].ipc_service = ipc_service;
                    cnt++;
                }
                else
                {
                    *lasterror = IPCESERVICETABLE;
                    ret = IPC_ERROR;
                }
            }
        }
        break;

    case QUERY_SUPPORT_STATUS:
        drt_item = ipcNodeIdToDrtItemWithoutLock(ipc_node);
        /* check the return drt_item to see whether it is same with local node */
        if (NULL == drt_item)
        {
            *lasterror = IPCENODE;
            ret = IPC_ERROR;
            break;
        }
        else if (ipcListFind(&drt_item->srvId_list, ipcCastPtr(ipc_service, void*),
                             ipcListCompareDrtSrvId, NULL))
        {
            ipc_servicetable[cnt].ipc_node = drt_item->node_id;
            ipc_servicetable[cnt].ipc_service = ipc_service;
            cnt++;
        }
        break;
    }
    
    UNLOCK(DRTTable);
    
    *ipc_sizeofservicetable = cnt;
    
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsetwatermark_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will set the water mark values for a socket.
*   
*   PARAMETER(S):
*       sock: the socket to be set
*       ipc_lowwmark: the lower water mark value
*       ipc_highwmark: the higher water mark value
*       
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Oct2005  Motorola Inc.         Initial Creation
* 16Mar2006  Motorola Inc.         Allow watermark to be 0
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcsetwatermark_imp(ipcSocket_t* sock,
                        unsigned char ipc_lowwmark,
                        unsigned char ipc_highwmark,
                        int* lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if ((ipc_lowwmark >= ipc_highwmark) &&
        (ipc_lowwmark != 0))
    {
        *lasterror = IPCEWMARK;
        return IPC_ERROR;
    }

    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }
    sock->lowWatermark = ipc_lowwmark;
    sock->highWatermark = ipc_highwmark;
    ipcUnlockSocket(sock);

    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsocket_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Create a socket. The socket won't have a service ID until it's bound.
*   
*   PARAMETER(S):
*       af: address family of the socket
*       type: type of the socket
*       protocol: protocol of the socket
*       sock: pointer to socket handle (out param)
*       
*   RETURN: If successfully created, return 0
*           Other return error value IPCEAFNOSUPPORT, IPCEPROTOTYPE, or IPCENOBUFS
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 16Jan2005  Motorola Inc.         Changed msg queue impl
* 24Sep2005  Motorola Inc.         Added CO socket
* 17Oct2006  Motorola Inc.         Set queue length
* 01Dec2006  Motorola Inc.         Support DSM message discard               
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 06Apr2007  Motorola Inc.         Print socket owner in /proc/sipc/ipc_stack
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 
*****************************************************************************************/
ipcSocket_t *ipcsocket_imp(int af,
                           int type,
                           int protocol,
                           int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t *socket = NULL;
    int rc;
    int dsmDiscard = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    /*  Argument checking   */
    if (af != AF_IPC)
    {
        *lasterror = IPCEAFNOSUPPORT;
        return (ipcSocket_t *)IPC_ERROR;
    }

    if (type & IPC_DSM_DISCARD)
    {
        dsmDiscard = 1;
    }
    type &= ~(IPC_DSM_DISCARD | IPC_DSM_WAKE_UP);

    if ((type != IPC_CL_SOCK)
        && (type != IPC_CO_SOCK))
    {
        *lasterror = IPCESOCKNOSUPPORT;
        return (ipcSocket_t *)IPC_ERROR;
    }

    if (protocol != IPC_PROTO)
    {
        *lasterror = IPCEPROTONOSUPPORT;
        return (ipcSocket_t *)IPC_ERROR;
    }

    /*  Parameter check done, now do the real job.  */
    /*  Allocate a slot */
    socket = (ipcSocket_t *)ipcAlloc(sizeof(ipcSocket_t), 0);

    if (socket == NULL)
    {
        *lasterror = IPCENOBUFS;
        ipcError(IPCLOG_SESSION, ("Out of memory in ipcsocket_imp!\n"));
        return (ipcSocket_t *)IPC_ERROR;
    }

    memset(socket, 0, sizeof(ipcSocket_t));

    if (ipcQueueOpen(&socket->packetQueue) != IPCE_OK)
    {
        ipcFree(socket);
        *lasterror = IPCENOBUFS;
        ipcError(IPCLOG_SESSION, ("ipcQueueOpen failed in ipcsocket_imp!\n"));
        return (ipcSocket_t *)IPC_ERROR;
    }

    memcpy( socket->owner, current->comm, 15);
    socket->srvId = IPC_SERVICE_ANY;
    socket->isBound = FALSE;
    socket->isCOSocket = FALSE;
    socket->magic = IPC_SOCKET_MAGIC;
    socket->state = IPC_SOCK_NOT_CONNECTED;
    socket->peerCanSend = TRUE;
    socket->peerCanReceive = TRUE;
    socket->dsmDiscard = dsmDiscard;
    socket->channelId = IPC_NONDEDICATED_CHANNEL_ID;
    socket->notifications = NULL;

    /* for CO socket, register call back functions */
    if (IPC_CO_SOCK == type)
    {
        /* init conn */
        ipcConnInit(&socket->conn, IPC_MAX_WINDOW_SIZE, 
            socket, &ipcSession_glb.connOps);
        ipcQueueSetMaxSize(&socket->packetQueue, IPC_MAX_WINDOW_SIZE);
        socket->state = IPC_SOCK_NOT_CONNECTED;
        ipcListInit(&socket->newConnList);

        socket->isCOSocket = TRUE;
        /* open a semaphore */
        rc = ipcOsalSemaOpen(&socket->coSema, 0);
        if (rc < 0)
        {
            ipcQueueClose(&socket->packetQueue);
            ipcFree(socket);
            ipcError(IPCLOG_SESSION, ("open coSema semaphore failed\n"));
            *lasterror = IPCENOBUFS;
            return (ipcSocket_t *)IPC_ERROR;
        }
        /* delegate the socket */
        if (ipcOsalMutexLock(ipcStack_glb.connMutex) < 0)
        {
            ipcOsalSemaClose(socket->coSema);
            ipcQueueClose(&socket->packetQueue);
            ipcFree(socket);
            *lasterror = IPCEBUSY;
            return (ipcSocket_t*)IPC_ERROR;
        }
        ipcConnDelegate(&socket->conn);
        ipcOsalMutexRelease(ipcStack_glb.connMutex);
    }
    else
    {
        ipcQueueSetMaxSize(&socket->packetQueue, IPC_MAX_QUEUE_LEN);
    }

    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcsocket_imp: Failed to lock SrvIDTable\n"));
        if (IPC_CO_SOCK == type)
        {
            ipcOsalSemaClose(socket->coSema);
            if (ipcOsalMutexLock(ipcStack_glb.connMutex) < 0)
            {
                ipcError(IPCLOG_SESSION, 
                         ("ipcsocket_imp: Failed to lock ipcStack_glb.connMutex\n"));
                ipcConnUndelegate(&socket->conn);
            }
            else
            {
                ipcConnUndelegate(&socket->conn);
                ipcOsalMutexRelease(ipcStack_glb.connMutex);
            }
        }
        ipcQueueClose(&socket->packetQueue);
        ipcFree(socket);
        *lasterror = IPCEBUSY;
        return (ipcSocket_t*)IPC_ERROR;
    }
    ipcListAddTail(&ipcSession_glb.unboundSockList, (ipcListLink_t)socket);
    UNLOCK(SrvIDTable);
    init_waitqueue_head(&socket->readwq);
    init_waitqueue_head(&socket->writewq);

    return socket;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcbind_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Associate a local ipc address to a socket.
*   
*   PARAMETER(S):
*       s: the socket handle
*       name: the ipc address need to be associated to the socket
*       namelen: the length of the name
*       
*   RETURN: If success, return 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 16Jan2005  Motorola Inc.         Changed the tables for sockets
* 15Apr2005  Motorola Inc.         Bug fix for disordered serviceid
* 25Jul2005  Motorola Inc.         parameter namelen checking error
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcbind_imp(ipcSocket_t *sock,
    const struct IPCSOCK_ADDR *name,
    int namelen,
    int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT16 dsmAction = IPC_DSM_WAKE_UP;
    ipcServiceId_t srvId;
    ipcDrtChangeSet_t changes;
    int rc = IPC_OK;
    BOOL needDRTChange = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDrtChangeSetInit(&changes);
    /*  argument checking   */
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if ((name == NULL) || (namelen < (int)sizeof(struct IPCSOCK_ADDR)))
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }

    if ((name->ipc_node != ipcStack_glb.nodeId) && 
        (name->ipc_node != IPC_NODE_ANY))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
    
    /* CO socket can't be bound to multicast srvId's */
    if (sock->isCOSocket && (name->ipc_service & IPC_SRVID_M_BIT))
    {
        *lasterror = IPCEADDRNOTVAL;
        return IPC_ERROR;
    }

    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (sock->isBound)
    {
        *lasterror = IPCEINVAL;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }

    if (sock->dsmDiscard)
    {
        dsmAction = IPC_DSM_DISCARD;
    }
    
    /* check whether the service id is valid */
    srvId = name->ipc_service;

    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("Failed to lock SrvIDTable\n"));
        *lasterror = IPCEBUSY;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }
    
    if (srvId == IPC_SERVICE_ANY)
    {
        /* session assign a service id */
        srvId = ipcAllocateServiceId();

        if (srvId == IPC_SERVICE_ANY)
        {
            rc = IPCENOBUFS;
        }
    }
    else if (!ipcIsPublishedServiceId(srvId))
    {
        /* Client cannot specify an unpublished service ID (session assigns these) */
        rc = IPCEADDRNOTVAL;
    }

    if (rc == IPC_OK)
    {
        if (!ipcIsServiceIdAvailable(srvId))
        {
            rc = IPCEADDRINUSE;
        }
        else
        {
            sock->isBound = TRUE;
            sock->canReceive = TRUE;
            sock->canSend = TRUE;
            sock->srvId = srvId;
            needDRTChange = ipcAddSocketToLocalSrvIdTab(sock);
        }
    }
    UNLOCK(SrvIDTable);
    ipcUnlockSocket(sock);

    if (rc == IPC_OK)
    {
        if (needDRTChange)
        {
            /* We added a published service ID so we need to update the DRT */
            if (LOCK(DRTTable) < 0)
            {
                /*
                 * FIXME: Failed to lock DRT table? Maybe this task is killed
                 *
                 * We can hardly handle it correctly...
                 */
                ipcError(IPCLOG_SESSION,
                        ("ipcbind_imp: Failed to lock DRT!\n"));
            }
            else
            {
                ipcDrtAddService(ipcSession_glb.localDrtItem, srvId, dsmAction, &changes);
                UNLOCK(DRTTable);

                ipcDrtChangeSignal(&changes, TRUE);
                ipcDrtChangeFreeAll(&changes);
            }
        }
    }
    else
    {
        *lasterror = rc;
        return IPC_ERROR;
    }

    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetsockname_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Retrieves the local name for a socket.
*   
*   PARAMETER(S):
*       s: the socket handle
*       name: the pointer to return the socket's name
*       namelen: the pointer to return the length of name
*       
*   RETURN: If parameters are all valid, 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2004  Motorola Inc.         Initial Creation
* 25Jul2005  Motorola Inc.         parameter namelen checking error
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcgetsockname_imp(ipcSocket_t *sock,
    struct IPCSOCK_ADDR *name,
    int *namelen,
    int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    /*  argument checking */
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if ((name == NULL) || (namelen == NULL) || (*namelen < (int)sizeof(struct IPCSOCK_ADDR)))
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }

    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (!sock->isBound)
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
    else
    {
        /* if the socket is bound, return its name */
        name->ipc_node = ipcStack_glb.nodeId;
        name->ipc_service = sock->srvId;
        ipcUnlockSocket(sock);
    }
    
    *namelen = sizeof(struct IPCSOCK_ADDR);
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetpeername_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Retrieves the peer name for a socket.
*   
*   PARAMETER(S):
*       s: the socket handle
*       name: the pointer to return the socket's name
*       namelen: the pointer to return the length of name
*       
*   RETURN: If parameters are all valid, 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Nov2005  Motorola Inc.         Initial Creation
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 06Apr2007  Motorola Inc.         Added checking for namelen
* 24Apr2007  Motorola Inc.         Deleted checking for socket connection
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 
*****************************************************************************************/
int ipcgetpeername_imp(ipcSocket_t *sock,
                       struct IPCSOCK_ADDR *name,
                       int *namelen,
                       int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if ((name == NULL) || (namelen == NULL) || (*namelen < (int)sizeof(struct IPCSOCK_ADDR))) 
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }
    
    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (!sock->isBound)
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
    else if (sock->state != IPC_SOCK_CONNECTED)
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCENOTCONN;
        return IPC_ERROR;
    }
    else
    {
        name->ipc_node = sock->destNode;
        name->ipc_service = sock->destSrv;
        ipcUnlockSocket(sock);
    }

    *namelen = sizeof(*name);

    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcshutdown_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Disable sends or receives on a socket.
*   
*   PARAMETER(S):
*       s: the socket handle
*       how: flag that describes what types of operation would not be allowed
*       
*   RETURN: If success, return 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 16Jan2005  Motorola Inc.         Keep socket bound on shutdown
* 12Oct2005  Motorola Inc.         Added CO socket
* 20Feb2005  Motorola Inc.         Do not wakeup receivers on shutdown
* 30Jun2006  Motorola Inc.         Add timeout for shutdown
* 04Sep2006  Motorola Inc.         Only work on IPC_SOCK_CONNECTED
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcshutdown_imp(ipcSocket_t *sock,
    int how,
    int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
    ipcPacket_t* packet = NULL;
    ipcNodeId_t destNode;
    ipcServiceId_t srvId;
    ipcServiceId_t destSrvId;
    ipcAddress_t destAddr;
    UINT32 cookie = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    /*  check argument */
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if ((how != IPCSD_SEND) && (how != IPCSD_RECEIVE) && (how != IPCSD_BOTH))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
   
    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (sock->isCOSocket &&
        (sock->state != IPC_SOCK_CONNECTED))
    {
        ipcUnlockSocket(sock);
        return IPC_OK;
    }

    destNode = sock->destNode;
    destSrvId = sock->destSrv;
    srvId = sock->srvId;
    if (sock->isCOSocket)
    {
        cookie = sock->cookie;
    }

    if ((how & IPCSD_RECEIVE) && (sock->canReceive))
    {
        sock->canReceive = 0;
        if (sock->isCOSocket)
        {
            ipcConnDestroy(&sock->conn, FALSE, TRUE, FALSE);
        }
    }
    if ((how & IPCSD_SEND) && (sock->canSend))
    {
        sock->canSend = 0;

        if (sock->isCOSocket)
        {
            if (ipcIsSocketValid(sock) &&
                (sock->conn.sentPackets.size > 0) &&
                (sock->state == IPC_SOCK_CONNECTED))
            {
                ipcOsalSemaReset(sock->coSema);
                ipcUnlockSocket(sock);
                if (ipcOsalSemaWait(sock->coSema, IPC_SHUTDOWN_TIMEOUT) < 0)
                {
                    ipcError(IPCLOG_SESSION, 
                             ("ipcshutdown_imp: Failed to wait on sock->coSema\n"));
                }
                if (ipcLockSocket(sock) < 0)
                {
                    *lasterror = IPCEBUSY;
                    return IPC_ERROR;
                }
                ipcConnDestroy(&sock->conn, TRUE, FALSE, FALSE);
            }
        }
    }
    ipcUnlockSocket(sock);

    destAddr = ipcNodeIdToIpcAddr(destNode);
    if (sock->isCOSocket)
    {
        packet = ipcCreateConnDown(destAddr,
            destSrvId, 
            srvId, cookie, (UINT8)how);
        if (packet != NULL)
        {
            /* tell the peer node a bout the shutdown event */
            if ((rc = SSRTDataRequest(packet, IPC_CONTROL_CHANNEL_ID)) < 0)
            {
                ipcFree(packet);
                *lasterror = rc;
                rc = IPC_ERROR;
            }
        }
    }
    ipcCheckSocketConnection(sock);

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsetsockqlen_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Set the max number of messages that can be queued in an existing socket.
*   
*   PARAMETER(S):
*       s: the socket handle
*       queueMaxlen: the new capacity of the socket receive queue.
*       
*   RETURN: 0 on success, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 21Oct2005  Motorola Inc.         Added support for CO socket
* 07Mar2006  Motorola Inc.         Send ack to tell window size to peer
* 17Oct2006  Motorola Inc.         Also set qlen for CO sockets
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcsetsockqlen_imp(ipcSocket_t *sock,
    int queueMaxLen,
    int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if ((queueMaxLen < IPC_MIN_QUEUE_LEN) || (queueMaxLen > IPC_MAX_QUEUE_LEN))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    if (sock->isCOSocket)
    {
        /* the function ipcsetsockqlen sets window size for CO sockets */
        if (ipcLockSocket(sock) < 0)
        {
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        if (((int)queueMaxLen < (int)ipcQueueNumElements(&sock->packetQueue)) ||
            ((int)queueMaxLen > (UINT8)(-1)))
        {
            *lasterror = IPCEINVAL;
            ipcUnlockSocket(sock);
            return IPC_ERROR;
        }
        sock->conn.rxWindowSize = queueMaxLen - ipcQueueNumElements(&sock->packetQueue);
        ipcQueueSetMaxSize(&sock->packetQueue, queueMaxLen);

        if (sock->state == IPC_SOCK_CONNECTED)
        {
            ipcUnlockSocket(sock);
            ipcSendFlow(&sock->conn,
                    ipcNodeIdToIpcAddr(sock->destNode),
                    sock->destSrv,
                    (UINT8)(sock->conn.nextRxSeqNum + sock->conn.rxWindowSize - 1));
        }
        else
        {
            ipcUnlockSocket(sock);
        }
    }
    else
    {
        if (ipcLockSocket(sock) < 0)
        {
            *lasterror = IPCEBUSY;      
            return IPC_ERROR;
        }
        ipcQueueSetMaxSize(&sock->packetQueue, queueMaxLen);
        ipcUnlockSocket(sock);
    }

    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcclosesocket_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Close an existing socket.
*   
*   PARAMETER(S):
*       s: the socket to be closed
*       
*   RETURN: 0 on success, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 16Jan2005  Motorola Inc.         Changed tables for sockets
* 12Oct2005  Motorola Inc.         Added CO socket support
* 14Mar2006  Motorola Inc.         Wakeup senders and receivers
* 14Apr2006  Motorola Inc.         Bug fix for vshm leak
* 30Jun2006  Motorola Inc.         Simplified for CO socket
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
*
*****************************************************************************************/
int ipcclosesocket_imp(ipcSocket_t *sock,
                       int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int originalState = IPC_SOCK_NOT_CONNECTED;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    originalState = sock->state;
    ipcshutdown_imp(sock, IPCSD_BOTH, lasterror);

    if (sock->isCOSocket )
    {
        if (originalState == IPC_SOCK_CONNECTED)
        {
            /* Wake up senders and receivers if they are blocked */
            ipcCOSocketReceiveEnd(sock);
            ipcCOSocketTxWindowAvailable(&sock->conn);
        }
    }

    if (ipcLockSocket(sock) == IPC_OK)
    {
        sock->isClosed = TRUE;
        while (sock->refCount > 0)
        {
            ipcUnlockSocket(sock);
            if (ipcOsalSleep(100) < 0)
            {
                break;
            }
            if (ipcLockSocket(sock) < 0)
            {
                break;
            }
        }
        ipcUnlockSocket(sock);
    }
    else
    {
        ipcError(IPCLOG_SESSION,
                 ("ipcclosesocket_imp: Failed to lock socket before destroying\n"));
    }

    ipcSocketDestroy(sock);

    return IPC_OK;

}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczsendto_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send data to a specific IPC address.
*   
*   PARAMETER(S):
*       s: socket handle 
*       bufferOffset: offset to buffer in shared memory to be sent out
*       leng: the length of data packet
*       flags: indicator specifying the way in which the call is made
*       to: destination IPC address
*       tolen: size of address in the to parameter
*       error: pointer to error code (out param)
*       
*   RETURN: If no error occurs, returns the total number of bytes sent, if an
*           error occurs, IPC_ERROR is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 16Jan2005  Motorola Inc.         Channel validity check is done by SSRTDataRequest, not this one. Data goes by shared memory. Added logic for IPC_NODE_ANY.
* 25Jan2005  Motorola Inc.         Shared memory debug
* 27Jan2005  Motorola Inc.         Check whether comp and channle and peerNode are matched
* 01Mar2005  Motorola Inc.         Channel validity check is ignored if sending data to self. Changed return value to leng on succ.
* 23May2005  Motorola Inc.         Changed ipc address judgment for communication on same client node
* 16Jun2005  Motorola Inc.         Physical Shm & Virtural Shm Integration
* 01Jul2005  Motorola Inc.         Changed pointer to buffer offset
* 26Jul2005  Motorola Inc.         parameter tolen checking error
* 26Jul2005  Motorola Inc.         check whether parameter leng < 0
* 19Aug2005  Motorola Inc.         Make other module can also create channel in kernel space
* 23Nov2005  Motorola Inc.         Add delay in BP send func to make  AP-BP connection more stable
* 23Dec2005  Motorola Inc.         Delete delay in ipczsendto_imp
* 18Jan2006  Motorola Inc.         change invalid leng error code to IPCEMSGSIZE
* 10Mar2006  Motorola Inc.         Set channel only when dest node matches
* 18Jul2006  Motorola Inc.         shared memory overrun detection 
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source 
* 18Aug2006  Motorola Inc.         Return if ipcOsalSleep fails
* 01Dec2006  Motorola Inc.         Support DSM message discard               
* 11Dec2006  Motorola Inc.         Return error to user once IPCENOBUF occurs
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipczsendto_imp(ipcSocket_t *sock,
    const char *zbuffer,
    int leng,
    int flags,
    const struct IPCSOCK_ADDR *to,
    int tolen,
    int *lasterror)
{    
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *pack = NULL;
    ipcAddress_t ipcAddr;
    UINT8 packetFlags = 0;
    ipcChannelId_t channelId = IPC_NONDEDICATED_CHANNEL_ID;
    ipcNodeId_t sockDestNode = IPC_NODE_ANY;
    ipcChannelId_t sockChannel = IPC_NONDEDICATED_CHANNEL_ID;
    int rc;
    BOOL discardForDsm = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (sock->isCOSocket || !sock->isBound)
    {
        *lasterror = IPCEINVAL;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }

    if ((leng > IPC_MAX_PACKET_SIZE) || (leng < 0))
    {
        *lasterror = IPCEMSGSIZE;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }
  
    pack = ipcDataToPacket(zbuffer);
    if ((pack == NULL) || (!ipcSMCheckBuffer((char *)pack, "ipczsendto_imp")) )
    {
        *lasterror = IPCEFAULT;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }
    
    if (to == NULL)
    {
        *lasterror = IPCEADDRNOTVAL;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }
    
    if (tolen < (int)sizeof(struct IPCSOCK_ADDR))
    {
        *lasterror = IPCEFAULT;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }
    
    if (!sock->canSend)
    {
        *lasterror = IPCESHUTDOWN;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }

    if (flags & ~(IPCNONBLOCK | IPCPRIO | IPCROSPRIO | IPCPAYLOADCRC | 
                  IPC_DSM_WAKE_UP | IPC_DSM_DISCARD))
    {
        /* An unknown flag was set */
        *lasterror = IPCEINVAL;
        ipcUnlockSocket(sock);
        return IPC_ERROR;
    }

    if (flags & (IPCPRIO | IPCROSPRIO))
    {
        packetFlags |= IPC_PACKET_HIGH_PRIORITY;

        if (flags & IPCROSPRIO)
        {
            packetFlags |= IPC_PACKET_LIFO_PRIORITY;
        }
    }
    if (flags & IPCPAYLOADCRC)
    {
        packetFlags |= IPC_PACKET_CRC;
    }

    /* Packet header has already been allocated before the data */
    pack->next = NULL;

    ipcSetMsgLength(pack, leng);
    ipcSetSrcAddr(pack, ipcStack_glb.ipcAddr);
    ipcSetDestAddr(pack, ipcStack_glb.ipcAddr);
    ipcSetSrcServiceId(pack, sock->srvId);
    ipcSetDestServiceId(pack, to->ipc_service);
    ipcSetMsgFlags(pack, packetFlags);

    sockDestNode = sock->destNode;
    sockChannel = sock->channelId;

    ipcUnlockSocket(sock);

    if (to->ipc_node == IPC_NODE_BROADCAST)
    {
        /* broadcast */
        ipcSessionBroadcast(pack, flags);

        ipcSetDestAddr(pack, ipcStack_glb.ipcAddr);
        if (SSRTDataRequest(pack, IPC_NONDEDICATED_CHANNEL_ID) < 0)
        {
            /* no local destination found so free packet */
            ipcFree(pack);
        }
    }
    else /* not a broadcast */
    { 
        if (to->ipc_node == IPC_NODE_ANY)
        {
            /* No node was specified so we must find a node that provides
             * the requested service */
            if (!ipcIsPublishedServiceId(to->ipc_service) ||
                !ipcFindAddrForService(to->ipc_service, &ipcAddr))
            {
                *lasterror = IPCESERVICE;
                return IPC_ERROR;
            }
        }
        else
        {
            /* The node was specified so convert it to an IPC Address */
            ipcAddr = ipcNodeIdToIpcAddr(to->ipc_node);

            if ((ipcAddr == IPC_INVALID_ADDR) && (to->ipc_node != ipcStack_glb.nodeId))
            {
                *lasterror = IPCEADDRNOTVAL;
                return IPC_ERROR;
            }
        }
        
        /* Set destination */
        ipcSetDestAddr(pack, ipcAddr);

        if (sockDestNode == to->ipc_node)
        {
            channelId = sockChannel;
        }
        
        /* Check if packet should be discarded for deep sleep mode */
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipczsendto_imp: Failed to lock DRTTable\n"));
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        discardForDsm = ipcDiscardForDsm(to->ipc_service, ipcAddrToDrtItem(ipcAddr), flags);
        UNLOCK(DRTTable);
        
        if (discardForDsm)
        {
            /* This packet should be discarded because the destination is in DSM */
            *lasterror = IPCEDSM;
            return IPC_ERROR;
        }
        
        /* Send packet */
        rc = SSRTDataRequest(pack, channelId);
        if (rc < 0)
        {
            if ( !(flags & IPCNONBLOCK) )
            {
                /* This is a blocking call so keep trying to write the data as long as
                 * the error is IPCENOBUFS (device layer has too much data pending) or
                 * IPCESOCKFULL (The target socket is full) */
                while ((rc == IPCESOCKFULL) && (to->ipc_node == ipcStack_glb.nodeId))
                {
                    if (ipcOsalSleep(IPC_SENDTO_RETRY_TIME) < 0)
                    {
                        rc = IPCEINTR;
                        break;
                    }
                    rc = SSRTDataRequest(pack, channelId);
                }
            }

            if (rc < 0)
            {
                if (rc == IPCESOCKFULL) 
                {
                    rc = IPCEWOULDBLOCK;
                }
                *lasterror = rc;
                return IPC_ERROR;
            }
        }
    }
    
    return leng;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczrecvfrom_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives a data packet using zero copy.
*   
*   PARAMETER(S):
*       s: Descriptor identifying a bound socket
*       bufferOffset: pointer to shared memory offset for the incoming data
*       len: the upperbound size of the buffer, ignored if len equals to 0
*       flags: indicator specifying the way in which the call is made
*       from: pointer to return the source address of the data packet
*       fromlen: pointer to return the size of from buffer
*       error: pointer to error code (out param)
*       
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2005  Motorola Inc.         Initial Creation
* 16Jan2005  Motorola Inc.         Data are transferred in shm.
* 16Jun2005  Motorola Inc.         Physical Shm & Virtural Shm Integration
* 01Jul2005  Motorola Inc.         Changed pointer to buffer offset
* 27Dec2005  Motorola Inc.         Socket close event is notified
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source 
* 18Jul2006  Motorola Inc.         shared memory overrun detection
* 18Aug2006  Motorola Inc.         Adjusted for IPC_RELEASE build
* 01Nov2006  Motorola Inc.         Consumed data when receiving shutdown message
* 17Jan2007  Motorola Inc.         Added checking for lock failure
* 02Mar2007  Motorola Inc.         Merge bug fix from old vob 
* 31May2007  Motorola Inc.         Check if memory is corrupted when met IPCEWOULDBLOCK
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 21Aug2007  Motorola Inc.         Fixed OSS compliance issue
*
*****************************************************************************************/
int ipczrecvfrom_imp(ipcSocket_t *sock,
                     char **zbuffer,
                     int flags,
                     struct IPCSOCK_ADDR *from,
                     int *fromlen,
                     int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packet;
    int numBytes;
    BOOL isCOSocket;
    int lowWatermark = 0;
    ipcNodeId_t srcNodeId = IPC_NODE_ANY;
    ipcAddress_t srcAddr = IPC_INVALID_ADDR;
    ipcServiceId_t srcServiceId = IPC_SERVICE_ANY;
    ipcServiceId_t destService = IPC_SERVICE_ANY;
    ipcNodeId_t sockDestNode = IPC_NODE_ANY;
    ipcAddress_t sockDestAddr = IPC_INVALID_ADDR;
    ipcServiceId_t sockDestService = IPC_SERVICE_ANY;
    int queueFlags = 0; /* for default blocking call */
    int originalNumPackcets = 0;
    int newNumPackets = 0;
    int error;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((from == NULL) || (fromlen == NULL) ||
        (*fromlen < sizeof(struct IPCSOCK_ADDR)))
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }

    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (!sock->isBound || (flags & ~(IPCNONBLOCK | IPCPRIO | IPCROSPRIO | IPCROSCHKMSG)))
    {
        /* socket is not bound or unknown flag passed in */
        ipcUnlockSocket(sock);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    originalNumPackcets = ipcQueueNumElements(&sock->packetQueue);
    isCOSocket = sock->isCOSocket;
    lowWatermark = sock->lowWatermark;
    sockDestNode = sock->destNode;
    sockDestService = sock->destSrv;

    /* if the socket is not connected, but there're packets in the queue,
     * we can still read data from it */
    if (((flags & IPCPRIO) && (sock->packetQueue.highPriorityList.size == 0)) ||
            (sock->packetQueue.currentSize == 0))
    {
        if (!sock->canReceive)
        {
            *lasterror = IPCESHUTDOWN;
            ipcUnlockSocket(sock);
            return IPC_ERROR;
        }
        if ( isCOSocket && 
                (!sock->peerCanSend || (sock->state == IPC_SOCK_NOT_CONNECTED)))
        {
            *lasterror = IPCENOTCONN;
            ipcUnlockSocket(sock);
            return IPC_ERROR;
        }
    }

    if (flags & IPCNONBLOCK)
    {
        /* this is a non-blocking call */
        queueFlags |= IPC_QUEUE_NON_BLOCKING;
    }

    if (flags & (IPCPRIO | IPCROSPRIO))
    {
        /* only return high priority messages */
        queueFlags |= IPC_QUEUE_HIGH_PRIORITY;
    }

    if (flags & IPCROSCHKMSG)
    {
        /* just peek at the next message without removing from the queue */
        queueFlags |= (IPC_QUEUE_PEEK | IPC_QUEUE_NON_BLOCKING);
    }

    ipcUnlockSocket(sock);
    if (ipcIsMulticastServiceId(sock->srvId))
    {
        /* Packets are queued inside list links for multicast sockets */
        packet = (ipcPacket_t *)ipcQueueGetObject(&sock->packetQueue, queueFlags, &error);
    }
    else
    {
        /* Packets are queued directly for non-multicast sockets */
        packet = (ipcPacket_t *)ipcQueueGetLink(&sock->packetQueue, queueFlags, &error);
    }

    if (packet == NULL)
    {
        /* Should NEVER be NULL for blocking call */
        *lasterror = error;

        if ((error == IPCEWOULDBLOCK) && ((queueFlags & IPC_QUEUE_NON_BLOCKING) == 0))
        {
            ipcError(IPCLOG_MEMTRACK,
                    ("ipc socket queue length=%u, but ipczrecvfrom will return IPCEWOULDBLOCK, perhaps memory corrupted",
                     sock->packetQueue.currentSize));
        }

        return IPC_ERROR;
    }
#ifndef IPC_RELEASE
    ipcSMCheckBuffer((char *)packet, "ipczrecvfrom_imp");
#endif /* IPC_RELEASE */
    
    if (ipcLockSocket(sock) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipczrecvfrom_imp: Failed to lock socket\n"));
        ipcFree(packet);
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }
    newNumPackets = ipcQueueNumElements(&sock->packetQueue);
    ipcUnlockSocket(sock);
    /* Find the source Node ID from the source IPC Address */
    srcAddr = ipcGetSrcAddr(packet);
    srcServiceId = ipcGetSrcServiceId(packet);
    destService = ipcGetDestServiceId(packet);
    sockDestAddr = ipcNodeIdToIpcAddr(sockDestNode);

    while ((srcServiceId == IPC_SERVICE_ANY) && 
           (destService == IPC_SERVICE_ANY))
    {
        /* XXX: This is a shutdown msg */
        if (isCOSocket)
        {
            /* Get this msg out */
            if (queueFlags & IPC_QUEUE_PEEK)
            {
                packet = (ipcPacket_t*)ipcQueueGetLink(&sock->packetQueue, 
                                        queueFlags & ~IPC_QUEUE_PEEK, NULL);
            }

            if (ipcLockSocket(sock) < 0)
            {
                ipcError(IPCLOG_SESSION,
                        ("ipczrecvfrom_imp: Failed to lock socket\n"));
                ipcFree(packet);
                *lasterror = IPCEBUSY;
                return IPC_ERROR;
            }
            newNumPackets = ipcQueueNumElements(&sock->packetQueue);
            ipcUnlockSocket(sock);

            /* consume packet */
            ipcConnConsumeData(&sock->conn, sockDestAddr, sockDestService);

            /* do the notification stuff */
            if ((lowWatermark != 0) && 
                (originalNumPackcets > lowWatermark) && 
                (newNumPackets <= lowWatermark))
            {
                ipcNotify(sock, IPCLOWMARK);
            }

            if (flags & IPCPRIO)
            {
                if (newNumPackets > 0)
                {
                    ipcError(IPCLOG_SESSION, ("There're still data in the socket\n"));
                }
                ipcFree(packet);
                *lasterror = IPCENOTCONN;
                return IPC_ERROR;
            }
            else
            {
                if (newNumPackets > 0)
                {
                    /* There're packets in the queue, get it */
                    ipcFree(packet);
                    packet = (ipcPacket_t*)ipcQueueGetLink(&sock->packetQueue, queueFlags, NULL);
                    if (packet != NULL)
                    {
#ifndef IPC_RELEASE
                        ipcSMCheckBuffer((char *)packet, "ipczrecvfrom_imp");
#endif /* IPC_RELEASE */
                        srcServiceId = ipcGetSrcServiceId(packet);
                        destService = ipcGetDestServiceId(packet);
                        srcAddr = ipcGetSrcAddr(packet);
                        continue;
                    }
                    else
                    {
                        ipcError(IPCLOG_SESSION, ("The socket is empty?\n"));
                    }
                }
                ipcFree(packet);
                *lasterror = IPCENOTCONN;
                return IPC_ERROR;
            }
        } /* co socket */
        else
        {
            ipcError(IPCLOG_SESSION, ("The srcAddr of packet is IPC_INVALID_ADDR\n"));
            *lasterror = IPCEINVAL;
            return IPC_ERROR;
        }
    } /* while */
    
    /* some assignments */
    srcNodeId = ipcAddrToNodeId(srcAddr);
    numBytes = ipcGetMsgLength(packet);
    *fromlen = sizeof(struct IPCSOCK_ADDR);
    from->ipc_service = srcServiceId;
    from->ipc_node = srcNodeId;

    /* return the data pointer */
    *zbuffer = ipcGetMsgData(packet);

    if (isCOSocket && (numBytes >= 0))
    {
        /* consume packet */
        ipcConnConsumeData(&sock->conn, sockDestAddr, sockDestService);
    }

    /* do the notification stuff */
    if ((lowWatermark != 0) && 
        (originalNumPackcets > lowWatermark) && 
        (newNumPackets <= lowWatermark))
    {
        ipcNotify(sock, IPCLOWMARK);
    }

    return numBytes;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipclisten_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function allows a socket to listen or allow incoming connections.
*   
*   PARAMETER(S):
*       socket: pointer to a bound, unconnected socket
*       backlog: maximum number of pending connections before ipcaccept
*       
*   RETURN: If an error occurs, a value of IPC_ERROR is returned, and a specified
*           error code can be retrieve by calling ipcgetlasterror. 0 is returned
*           after a successful connection.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation
* 15Jan2007  Motorola Inc.         check connect status for reconnection
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 30Mar2007  Motorola Inc.         fix relisten issue in ipc direct connect mode
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*****************************************************************************************/
int ipclisten_imp(ipcSocket_t* socket, 
                  int backlog, 
                  int* lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(socket))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    ipcCheckSocketConnection(socket);

    if (ipcLockSocket(socket) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (!socket->isCOSocket)
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCESOCKNOSUPPORT;
        return IPC_ERROR;
    }

    if (!socket->isBound)
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    if(!ipcIsPublishedServiceId(socket->srvId) ||
        ipcIsMulticastServiceId(socket->srvId))
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCESERVICE;
        return IPC_ERROR;
    }

    if (socket->state != IPC_SOCK_NOT_CONNECTED)
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    } 

    if ((backlog < 0) || (backlog > IPCMAXBACKLOG))
    {
        backlog = IPCMAXBACKLOG;
    }

    socket->backlog = backlog;

    socket->state = IPC_SOCK_LISTENING;
    ipcUnlockSocket(socket);

    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcaccept_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function accepts a connection on a socket.
*   
*   PARAMETER(S):
*       socket: the pointer to a socket that has been placed in a listening
*                           state with the ipclisten function. the connection is made
*                           with the socket that is returned by this function.
*       addr: [out] Optional pointer to a buffer that receives the address
*                         of the connecting entity.
*       addrlen: [out] Optional pointer to an integer that contains the
*                            length of addr.
*       
*   RETURN: If an error occurs, a value of IPC_ERROR is returned, and a specified
*           error code can be retrieve by calling ipcgetlasterror. 0 is returned
*           after a successful connection.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation
* 21Oct2005  Motorola Inc.         Added support for direct connect
* 10Nov2005  Motorola Inc.         Check packet validation 
* 09Dec2005  Motorola Inc.         Wait the complete msg in socket
* 20Feb2005  Motorola Inc.         Set addr to connecting entity
* 23Feb2005  Motorola Inc.         Change state to CONNECTED before  waken up
* 17Oct2006  Motorola Inc.         new socket inherits queue length
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Ignore failure when waiting for complete message
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
ipcSocket_t* ipcaccept_imp(ipcSocket_t* socket, 
               struct IPCSOCK_ADDR *addr, 
               int *addrlen, 
               int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t* packet = NULL;
    COCOConnRequestMsg_t* request = NULL;
    ipcSocket_t* newSocket = NULL;
    UINT32 cookie;
    int errorcode = 0;
    struct IPCSOCK_ADDR tmp_addr;
    ipcAddress_t srcAddr;
    ipcServiceId_t srcServiceId;
    UINT8 startSeqNum;
    UINT8 windowSize;
    int queueLength = 0 ;
/*--------------------------------------- CODE -----------------------------------------*/

    if (!ipcIsSocketValid(socket))
    {
        *lasterror = IPCENOTSOCK;
        return (ipcSocket_t *)IPC_ERROR;
    }

    if (ipcLockSocket(socket) < 0)
    {
        *lasterror = IPCEBUSY;
        return (ipcSocket_t*)IPC_ERROR;
    }
    /* parameter checking */
    if (!socket->isBound)
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCEINVAL;
        return (ipcSocket_t *)IPC_ERROR;
    }
    if (!socket->isCOSocket)
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCEFAULT;
        return (ipcSocket_t *)IPC_ERROR;
    }
    if (socket->state != IPC_SOCK_LISTENING)
    {
        ipcUnlockSocket(socket);
        *lasterror = IPCEINVAL;
        ipcError(IPCLOG_SESSION,
                 ("ipcaccept_imp: Socket state is not listening\n"));
        return (ipcSocket_t *)IPC_ERROR;
    }

    /* NOTE: set the semaphore to 0 */
    ipcOsalSemaReset(socket->coSema);

    queueLength = socket->packetQueue.maxQueueSize;
    ipcRemoveObsoleteConnReq(socket);

    while (socket->newConnList.size == 0)
    {
        ipcUnlockSocket(socket);
        errorcode = ipcOsalSemaWait(socket->coSema, IPC_OSAL_INFINITE);
        if (ipcLockSocket(socket) < 0)
        {
            *lasterror = IPCEBUSY;
            ipcError(IPCLOG_SESSION,
                     ("ipcaccept_imp: Failed to lock socket\n"));
            return (ipcSocket_t*)IPC_ERROR;
        }

        if (errorcode < 0)
        {
            *lasterror = IPCEINTR;
            ipcUnlockSocket(socket);
            ipcError(IPCLOG_SESSION,
                     ("ipcaccept_imp: ipcOsalSemaWait failed\n"));
            return (ipcSocket_t *)IPC_ERROR;
        }
    }

    packet = (ipcPacket_t *)ipcListRemoveHead(&socket->newConnList);
    ipcUnlockSocket(socket);

    /* get necessary fields */
    srcAddr = ipcGetSrcAddr(packet);
    request = (COCOConnRequestMsg_t*)packet;
    srcServiceId = ipcOsalRead16(request->mySrvId);
    cookie = ipcOsalRead32(request->cookie);
    startSeqNum = ipcOsalRead8(request->startSeqNum);
    windowSize = ipcOsalRead8(request->windowSize);

    /* free the CONN_REQ */
    ipcFree(packet);

    if (socket->backlog != IPC_DIRECT_CONNECT)
    {
        newSocket = ipcsocket_imp(AF_IPC, IPC_CO_SOCK, IPC_PROTO, &errorcode);
        if ((int)newSocket == IPC_ERROR)
        {
            *lasterror = IPCENOBUFS;
            ipcError(IPCLOG_SESSION,
                     ("ipcaccept_imp: Failed to create new socket\n"));
            return (ipcSocket_t *)IPC_ERROR;
        }
        
        memset(&tmp_addr, 0, sizeof(struct IPCSOCK_ADDR));

        if (IPC_ERROR == ipcbind_imp(newSocket, 
                    &tmp_addr, 
                    sizeof(struct IPCSOCK_ADDR), 
                    &errorcode))
        {
            ipcSocketDestroy(newSocket);
            *lasterror = errorcode;
            ipcError(IPCLOG_SESSION,
                     ("ipcaccept_imp: Failed to bind new socket\n"));
            return (ipcSocket_t *)IPC_ERROR;
        }
        /* set queue length for the new socket */
        ipcQueueSetMaxSize(&newSocket->packetQueue, queueLength);
        newSocket->conn.rxWindowSize = queueLength;

    }
    else
    {
        /* if backlog == IPC_DIRECT_CONNECT, then we just use socket */
        newSocket = socket;
    }
    if (ipcLockSocket(newSocket) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcaccept_imp: Failed to lock new socket\n"));
        *lasterror = IPCEBUSY;
        if (newSocket != socket)
        {
            ipcSocketDestroy(newSocket);
        }
        return (ipcSocket_t*)IPC_ERROR;
    }
    newSocket->state = IPC_SOCK_ACCEPT_PENDING;
    newSocket->cookie = cookie;
    ipcListFreeAll(&newSocket->newConnList);

    /* set peer's address */
    newSocket->destNode = ipcAddrToNodeId(srcAddr);
    newSocket->destSrv = srcServiceId;
    tmp_addr.ipc_node = newSocket->destNode;
    tmp_addr.ipc_service = newSocket->destSrv;

    newSocket->conn.nextTxSeqNum = startSeqNum;
    newSocket->conn.maxTxSeqNum = startSeqNum + windowSize - 1;

    packet = ipcCreateConnReply(srcAddr,
        srcServiceId,
        newSocket->srvId,
        cookie,
        TRUE,   /* Here we'll accept it anyway */
        newSocket->conn.rxWindowSize,
        newSocket->conn.nextRxSeqNum);
    if( packet == NULL ) 
    {
        ipcError(IPCLOG_SESSION, ("Fail to create CONN_REP message\n"));
        *lasterror = IPCENOBUFS;
        if (newSocket != socket)
        {
            newSocket->state = IPC_SOCK_NOT_CONNECTED;
            ipcUnlockSocket(newSocket);
            ipcSocketDestroy(newSocket);
        }
        else
        {
            newSocket->state = IPC_SOCK_LISTENING;
            ipcUnlockSocket(newSocket);
        }
        return (ipcSocket_t *)IPC_ERROR;
    }

    ipcOsalSemaReset(newSocket->coSema);
    ipcUnlockSocket(newSocket);
    
    /* send the packet */
    errorcode = SSRTDataRequest(packet, IPC_CONTROL_CHANNEL_ID);
    if (errorcode < 0)
    {
        ipcFree(packet);
        *lasterror = errorcode;
        ipcError(IPCLOG_SESSION,
                 ("ipcaccept_imp: Failed to send reply out\n"));
        if (newSocket != socket)
        {
            newSocket->state = IPC_SOCK_NOT_CONNECTED;
            ipcSocketDestroy(newSocket);
        }
        else
        {
            /* XXX: This is not protected by a lock! */
            newSocket->state = IPC_SOCK_LISTENING;
        }
        return (ipcSocket_t *)IPC_ERROR;
    }

    /* wait for connection complete message */
    errorcode = ipcOsalSemaWait(newSocket->coSema, IPC_MAX_WAIT_TIME);
    if ((errorcode == IPCE_TIMEOUT) || (errorcode == IPCE_OBJECT_NOT_FOUND))
    {
        *lasterror = IPCETIMEDOUT;
        ipcError(IPCLOG_SESSION,
                 ("Failed to wait for complete message\n"));
        if (newSocket != socket)
        {
            newSocket->state = IPC_SOCK_NOT_CONNECTED;
            ipcSocketDestroy(newSocket);
        }
        else
        {
            /* XXX: This is not protected by a lock! */
            newSocket->state = IPC_SOCK_LISTENING;
        }
        return (ipcSocket_t *)IPC_ERROR;
    }
    else if (errorcode != IPCE_OK)
    {
        ipcError(IPCLOG_SESSION, 
                ("Failed to wait for complete message. Program interrupted? Ignore the error\n"));
    }

    if (addr != NULL)
    {
        memcpy(addr, &tmp_addr, sizeof(tmp_addr));
        if (addrlen != NULL)
        {
            *addrlen = sizeof(struct IPCSOCK_ADDR);
        }
    }
    
    return newSocket;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcconnect_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function establishes a connection to a specified socket.
*   
*   PARAMETER(S):
*       socket: pointer to an unconnected socket
*       addr: address of socket to which the connection will be established
*       addrlen: length of addr
*       
*   RETURN: If an error occurs, a value of IPC_ERROR is returned, and a specified
*           error code can be retrieve by calling ipcgetlasterror. 0 is returned
*           after a successful connection.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation
* 23Dec2005  Motorola Inc.         Correct return error code
* 04May2006  Motorola Inc.         Make addrlen check backwards compatible
* 11Dec2006  Motorola Inc.         Taos: self check fail: f8000 RB.2.00.00 during power up
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcconnect_imp(ipcSocket_t* socket, 
               struct IPCSOCK_ADDR *addr, 
               int addrlen,
               int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    COCOConnRequestMsg_t* request = NULL;
    COCOConnCompleteMsg_t* complete = NULL;
    ipcDRTItem_t* drtItem = NULL;
    ipcNodeId_t destNode = IPC_NODE_ANY;
    ipcAddress_t destAddr = IPC_INVALID_ADDR;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    if (!ipcIsSocketValid(socket))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }

    if (NULL == addr) 
    {
        *lasterror = IPCEADDRNOTVAL;
        return IPC_ERROR;
    }

    if ((!socket->isBound)||(addrlen < sizeof(struct IPCSOCK_ADDR)))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    if (addr->ipc_service == IPC_SERVICE_DISCONNECT)
    {
        if (socket->isCOSocket)
        {
            return ipcshutdown_imp(socket, IPCSD_BOTH, lasterror);
        }
        else
        {
            if (ipcLockSocket(socket) < 0)
            {
                *lasterror = IPCEBUSY;
                return IPC_ERROR;
            }
            socket->destNode = IPC_NODE_ANY;
            socket->destSrv = IPC_SERVICE_ANY;
            socket->state = IPC_SOCK_NOT_CONNECTED;
            ipcUnlockSocket(socket);
            return IPC_OK;
        }
    }

    /* for CL socket, just store default address */
    if (!socket->isCOSocket)
    {
        /*
         * FIXME: What happens if addr->ipc_node is IPC_NODE_ANY? Then the dest
         * address will be recalculated everytime this function is invoked.
         */
        if (ipcLockSocket(socket) < 0)
        {
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        socket->destNode = addr->ipc_node;
        socket->destSrv = addr->ipc_service;
        socket->state = IPC_SOCK_CONNECTED;
        ipcUnlockSocket(socket);

        return IPC_OK;
    }

    /* seek the dest address */
    destNode = addr->ipc_node;
    if (destNode == IPC_NODE_ANY)
    {
        if (ipcFindAddrForService(addr->ipc_service, &destAddr))
        {
            destNode = ipcAddrToNodeId(destAddr);
        }
        else
        {
            *lasterror = IPCEADDRNOTVAL;
            return IPC_ERROR;
        }
    }
    else
    {
        /* if dest node is specified, then check whether the service exists */
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("Failed to lock DRT table\n"));
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        rc = IPCEADDRNOTVAL;
        if (ipcIsPublishedServiceId(addr->ipc_service) &&
            (NULL != (drtItem = ipcNodeIdToDrtItemWithoutLock(destNode))))
        {
            destAddr = drtItem->ipcaddr;
	    /* just sure existence of node, no check on service */ 
            rc = IPC_OK;
        }
        UNLOCK(DRTTable);

        if (rc != IPC_OK)
        {
            *lasterror = rc;
            return IPC_ERROR;
        }
    }

    ipcCheckSocketConnection(socket);
    if (ipcLockSocket(socket) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    /* the socket is connected or connecting? */
    if (socket->state == IPC_SOCK_CONNECTED)
    {
        *lasterror = IPCEISCONN;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }

    ipcOsalSemaReset(socket->coSema);

    if (socket->state != IPC_SOCK_NOT_CONNECTED)
    {
        *lasterror = IPCEINVAL;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }

    /* set state to IPC_SOCK_CONN_PENDING */
    socket->state = IPC_SOCK_CONN_PENDING;
    /* send conn request */
    request = (COCOConnRequestMsg_t*)ipcCreateConnRequest(
        destAddr,
        addr->ipc_service,
        socket->srvId,
        socket->cookie = ipcOsalGetTickCount(),
        socket->conn.rxWindowSize,
        socket->conn.nextRxSeqNum);

    ipcUnlockSocket(socket);
    if (request != NULL)
    {
        /* send CONNECT_REQ to peer */
        rc = SSRTDataRequest((ipcPacket_t *)request, IPC_CONTROL_CHANNEL_ID);
    }
    else
    {
        rc = IPCENOBUFS;
    }

    if(IPC_OK != rc)
    {
        *lasterror = rc;
        if (request != NULL)
        {
            ipcFree(request);
        }

        if (ipcLockSocket(socket) < 0)
        {
            *lasterror = IPCEBUSY;
            socket->state = IPC_SOCK_NOT_CONNECTED;
            return IPC_ERROR;
        }
        socket->state = IPC_SOCK_NOT_CONNECTED;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }
    
    rc = ipcOsalSemaWait(socket->coSema, IPC_MAX_WAIT_TIME);
    if (ipcLockSocket(socket) < 0)
    {
        /*
         * FIXME: I know we shouldn't just return now, what shall we do?
         */
        *lasterror = IPCEBUSY;
        socket->state = IPC_SOCK_NOT_CONNECTED;
        return IPC_ERROR;
    }

    if (rc < 0)
    {
        *lasterror = IPCETIMEDOUT;
        socket->state = IPC_SOCK_NOT_CONNECTED;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }

    if (!socket->connAccepted)
    {
        *lasterror = IPCECONNREFUSED;
        socket->state = IPC_SOCK_NOT_CONNECTED;
        ipcUnlockSocket(socket);

        return IPC_ERROR;
    }
    /* all the necessary fields are set by main thread... */

    /* send conn complete */
    complete = (COCOConnCompleteMsg_t*)ipcCreateConnComplete(
        destAddr,
        socket->destSrv,
        socket->srvId,
        socket->cookie);

    if (NULL == complete)
    {
        *lasterror = IPCENOBUFS;
        socket->state = IPC_SOCK_NOT_CONNECTED;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }

    /* PENDING -> CONNECTED */
    socket->state = IPC_SOCK_CONNECTED;
    ipcUnlockSocket(socket);

    /* FIXME: The last ConnComp msg is sent via the newly created sock */
    rc = ipcConnDeliverData(&socket->conn, (ipcPacket_t*)complete,
                            TRUE, TRUE);
    if (rc < 0)
    {
        /* TODO: here need a reasonable error code, ex. IPCENETUNREACH,
           it doesn't comply with user manual 0916 version */
        *lasterror = IPCENETUNREACH;
        if (ipcLockSocket(socket) == IPCE_OK)
        {
            socket->state = IPC_SOCK_NOT_CONNECTED;
            ipcUnlockSocket(socket);
        }
        else
        {
            socket->state = IPC_SOCK_NOT_CONNECTED;
        }
        ipcFree(complete);
        return IPC_ERROR;
    }

    return IPC_OK;

}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczsend_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination on a connected socket.
*   Both CL and CO sockets may be used as long as ipcconnect is called first.
*   
*   PARAMETER(S):
*       socket: socket handle 
*       bufferOffset: offset to buffer in shared memory to be sent out
*       len: the length of data packet
*       flags: indicator specifying the way in which the call is made
*       lasterror: [out] pointer to error code
*       
*   RETURN: If no error occurs, returns the total number of bytes sent, if an
*           error occurs, IPC_ERROR is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation
* 23Nov2005  Motorola Inc.         Add delay in BP send func to make  AP-BP connection more stable
* 23Dec2005  Motorola Inc.         Delete Delay and correct some error  code
* 18Jul2006  Motorola Inc.         shared memory overrun detection
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source 
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipczsend_imp(ipcSocket_t* socket, 
             const char *zbuffer,
             int len, 
             int flags, 
             int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *pack = NULL;
    UINT8 packetFlags = 0;
    struct IPCSOCK_ADDR to;
    BOOL isReliable = FALSE;
    BOOL mayBlock = FALSE;
    int rc;
    ipcAddress_t destAddr;
    BOOL discardForDsm = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSocketValid(socket))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }
    
    if ((len > IPC_MAX_PACKET_SIZE) || (len < 0))
    {
        *lasterror = IPCEMSGSIZE;
        return IPC_ERROR;
    }
  
    pack = (ipcPacket_t *)ipcDataToPacket(zbuffer);
    if ((pack == NULL) || (!ipcSMCheckBuffer((char *)pack, "ipczsend_imp")))        
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }

    if (flags & ~(IPCRELIABLE | IPCNONBLOCK | IPCPRIO | IPCROSPRIO | IPCPAYLOADCRC | 
                  IPC_DSM_WAKE_UP | IPC_DSM_DISCARD))
    {
        /* An unknown flag was set */
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    ipcCheckSocketConnection(socket);
    
    if (ipcLockSocket(socket) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if(!socket->isBound)
    {
    	*lasterror = IPCEINVAL;
        ipcUnlockSocket(socket);
    	return IPC_ERROR;
    }
    
    if (!socket->canSend)
    {
        *lasterror = IPCESHUTDOWN;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }

    if ((IPC_SOCK_CONNECTED != socket->state) ||
        (!socket->peerCanReceive))
    {
        *lasterror = IPCENOTCONN;
        ipcUnlockSocket(socket);
        return IPC_ERROR;
    }
    /* if it is CL socket, sending data using
     * previous logic */
    if (!(socket->isCOSocket))
    {
        to.ipc_node = socket->destNode;
        to.ipc_service = socket->destSrv;
        
        ipcUnlockSocket(socket);
        return ipczsendto_imp(socket, 
            zbuffer, 
            len,
            flags, 
            &to, 
            sizeof(to), 
            lasterror);
    }
    if (flags & (IPCPRIO | IPCROSPRIO))
    {
        packetFlags |= IPC_PACKET_HIGH_PRIORITY;

        if (flags & IPCROSPRIO)
        {
            packetFlags |= IPC_PACKET_LIFO_PRIORITY;
        }
    }

    if (flags & IPCPAYLOADCRC)
    {
        packetFlags |= IPC_PACKET_CRC;
    }

    if (flags & IPCRELIABLE)
    {
        isReliable = TRUE;
        packetFlags |= IPC_PACKET_RELIABLE;

        if (socket->conn.sentPackets.size == 0)
        {
            packetFlags |= IPC_PACKET_FIRST_ACKED;
        }
    }

    if (!(flags & IPCNONBLOCK))
    {
        mayBlock = TRUE;
    }

    /* Packet header has already been allocated before the data */
    pack->next = NULL;

    destAddr = ipcNodeIdToIpcAddr(socket->destNode);
    
    ipcSetMsgLength(pack, len);
    ipcSetSrcAddr(pack, ipcStack_glb.ipcAddr);
    ipcSetDestAddr(pack, destAddr);
    ipcSetSrcServiceId(pack, socket->srvId);
    ipcSetDestServiceId(pack, socket->destSrv);
    ipcSetMsgFlags(pack, packetFlags);

    ipcUnlockSocket(socket);
    /* Check if packet should be discarded for deep sleep mode */
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipczsend_imp: Failed to lock DRT table\n"));
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    discardForDsm = ipcDiscardForDsm(socket->destSrv, ipcAddrToDrtItem(destAddr), flags);
    UNLOCK(DRTTable);
    
    if (discardForDsm)
    {
        /* This packet should be discarded because the destination is in DSM */
        *lasterror = IPCEDSM;
        return IPC_ERROR;
    }

    rc = ipcConnDeliverData(&socket->conn, 
        pack,
        isReliable, /* RELIABLE */
        mayBlock);

    if (rc < 0)
    {
        *lasterror = rc;
        return IPC_ERROR;
    }

    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczrecv_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives ipc data.
*   
*   PARAMETER(S):
*       sock: Descriptor identifying a bound socket
*       bufferOffset: pointer to shared memory offset for the incoming data
*       flags: indicator specifying the way in which the call is made
*       lasterror: pointer to error code (out param)
*       
*   RETURN: If no error occurs, this function returns the number of bytes 
*           received. If the connection has been gracefully closed, the return
*           value is 0. If an error occurs, a value of IPC_ERROR is returned,
*           and a specific error code can be retrieved by calling ipcgetlasterror.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation
* 18Jan2006  Motorola Inc.         check the bound state and the flags
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source 
* 17Jan2007  Motorola Inc.         Simplified to call ipczrecvfrom_imp 
*
*****************************************************************************************/
int ipczrecv_imp(ipcSocket_t *sock,
                 char **zbuffer,
                 int flags,
                 int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct IPCSOCK_ADDR from;
    int fromlen;
    int rc = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    memset(&from, 0, sizeof(from));
    fromlen = sizeof(from);
    
    rc = ipczrecvfrom_imp(sock,
        zbuffer,
        flags,
        &from,
        &fromlen,
        lasterror);
    
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcnotify_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function defines an asynchronous notification to be delivered to the
*   SmartIPC socket sock.
*   
*   PARAMETER(S):
*       sock: socket that will receive asynchronous notifications
*       name: address of socket that will cause the asynchronous notification. 
*       namelen: length of the value in the name parameter. 
*       msg: buffer containing the data to be transmitted for notification. 
*       len: length of the data in the msg parameter. 
*       flags: specifies what type of notification the socket requires. 
*       
*   RETURN: IPC_OK or IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2004  Motorola Inc.         Initial Creation
* 25Jul2005  Motorola Inc.         parameter namelen checking error
* 24Oct2005  Motorola Inc.         Added remote notification
* 31Dec2005  Motorola Inc.         checking flags ,whether bound
* 12Apr2006  Motorola Inc.         Added support for service discovery
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcnotify_imp(ipcSocket_t *sock, const struct IPCSOCK_ADDR* name, int namelen,
                  const char * msg, int msglen, int flags, int *lasterror)

{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t *socketToRegister;
    ipcNotification_t *notification;
    ipcNodeId_t destNode = IPC_NODE_ANY;
    ipcAddress_t destAddr = IPC_INVALID_ADDR;
    ipcServiceId_t destService = IPC_SERVICE_ANY;
    ipcServiceId_t localService = IPC_SERVICE_ANY;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    /*  argument checking */
    if (!ipcIsSocketValid(sock))
    {
        *lasterror = IPCENOTSOCK;
        return IPC_ERROR;
    }
    if (ipcLockSocket(sock) < 0)
    {
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }

    if (sock->isCOSocket ||
        !sock->isBound || 
        (msglen < 0) ||
        (msglen > IPC_NOTIFICATION_MAX_SIZE))
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }
    if (!sock->canReceive)
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCESHUTDOWN;
        return IPC_ERROR;
    }
    if (NULL == name)
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCEADDRNOTVAL;
        return IPC_ERROR;
    }
    if ((namelen < (int)sizeof(struct IPCSOCK_ADDR)) || 
        (msg == NULL))
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }
    
    if ((flags == 0) || 
        (flags & ~(IPCLOWMARK|
                   IPCHIGHMARK|
                   IPCREAD|
                   IPCWRITE|
                   IPCPRIONOTIFY|
                   IPCONLINE|
                   IPCOFFLINE)))
    {
        ipcUnlockSocket(sock);
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    localService = sock->srvId;
    ipcUnlockSocket(sock);
    if (flags & (IPCONLINE | IPCOFFLINE))
    {
        rc = ipcRegisterServiceNotify(localService,
                                      name->ipc_node,
                                      name->ipc_service,
                                      msg,
                                      msglen,
                                      flags & (IPCONLINE | IPCOFFLINE),
                                      lasterror);
        if (rc < 0)
        {
            return IPC_ERROR;
        }
    }

    flags &= ~(IPCONLINE | IPCOFFLINE);
    if (flags == 0)
    {
        return IPC_OK;
    }

    destNode = name->ipc_node;
    destService = name->ipc_service;
    if (destNode == IPC_NODE_ANY)
    {
        if (!ipcIsPublishedServiceId(destService) ||
            !ipcFindAddrForService(destService, &destAddr))
        {
            *lasterror = IPCEADDRNOTVAL;
            return IPC_ERROR;
        }
        destNode = ipcAddrToNodeId(destAddr);
    }
    else if (destNode != ipcStack_glb.nodeId)
    {
        destAddr = ipcNodeIdToIpcAddr(destNode);
        if (destAddr == IPC_INVALID_ADDR)
        {
            *lasterror = IPCEADDRNOTVAL;
            return IPC_ERROR;
        }
    }
    
    if (destNode == ipcStack_glb.nodeId)
    {
        if (LOCK(SrvIDTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("Failed to lock SrvIDTable\n"));
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        socketToRegister = ipcFindSocketBySrvId(destService);
        UNLOCK(SrvIDTable);
        if (socketToRegister == NULL)
        {
            *lasterror = IPCESERVICE;
            return IPC_ERROR;
        }
        /* The monitored socket can't be the same as monitoring one */
        if (socketToRegister == sock)
        {
            *lasterror = IPCEINVAL;
            return IPC_ERROR;
        }

        if (msglen == 0)
        {
            ipcRemoveNotification(socketToRegister, flags);
        }
        else
        {
            notification = (ipcNotification_t *)ipcAlloc(sizeof(ipcNotification_t), 0);
            if (notification == NULL)
            {
                *lasterror = IPCENOBUFS;
                return IPC_ERROR;
            }

            notification->flags = flags;
            notification->msglen = msglen;
            memcpy(notification->msg, msg, msglen);
            notification->destination.u.serviceId = localService;
            notification->psFlags = 0;
            /* Store IPC_INVALID_ADDR for local notifications because the
             * local address could change (link up/link down) */
            notification->destination.ipcAddr = IPC_INVALID_ADDR;

            /* Insert new notification into socket */
            if (ipcLockSocket(socketToRegister) < 0)
            {
                ipcFree(notification);
                *lasterror = IPCEBUSY;
                return IPC_ERROR;
            }
            notification->next = socketToRegister->notifications;
            socketToRegister->notifications = notification;
            ipcUnlockSocket(socketToRegister);
        }
    }
    else
    {
        rc = ipcRegisterNotification(
                destAddr,
                name->ipc_service,
                flags,
                msg,
                msglen,
                localService);

        if (rc < 0)
        {
            *lasterror = rc;
            return IPC_ERROR;
        }
    }
    return IPC_OK;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcpsnotify_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function defines an asynchronous notification to be delivered to a
*   platform-specific queue.
*   
*   PARAMETER(S):
*       q: Descriptor identifying a system specific "queue" that will receive
*                      asynchronous notifications.
*       qflag: Indicator providing additional information about the type of
*                          notification object/queue specified in q.
*       name: address of socket that will cause the asynchronous notification. 
*       namelen: length of the value in the name parameter. 
*       msg: buffer containing the data to be transmitted for notification. 
*       len: length of the data in the msg parameter. 
*       flags: specifies what type of notification the socket requires. 
*       
*   RETURN: IPC_OK or IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jan2004  Motorola Inc.         Initial Creation
* 25Jul2005  Motorola Inc.         parameter namelen checking error
* 24Oct2005  Motorola Inc.         Added remote notification
* 06Dec2005  Motorola Inc.         fixed misusage of ipcListNextPair
* 14Mar2006  Motorola Inc.         Simplified remote notification
* 12Apr2006  Motorola Inc.         Added support for service discovery
* 05May2006  Motorola Inc.         Set ipcAddr field
* 17Jan2007  Motorola Inc.         Added checking for lock failure 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcpsnotify_imp(void *q, int qflag, const struct IPCSOCK_ADDR* name, int namelen,
                    const char * msg, int msglen, int flags, int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t *socketToRegister;
    ipcNotification_t *notification;
    ipcNodeId_t destNode = IPC_NODE_ANY;
    ipcAddress_t destAddr = IPC_INVALID_ADDR;
    ipcServiceId_t destService = IPC_SERVICE_ANY;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (NULL == name)
    {
        *lasterror = IPCEADDRNOTVAL;
        return IPC_ERROR;
    }
    if ((namelen < (int)sizeof(struct IPCSOCK_ADDR)) || 
        (msg == NULL) || (msglen < 0))
    {
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }
    
    if ((flags == 0) || 
        (flags & ~(IPCLOWMARK|
                   IPCHIGHMARK|
                   IPCREAD|
                   IPCWRITE|
                   IPCPRIONOTIFY|
                   IPCONLINE|
                   IPCOFFLINE)))
    {
        *lasterror = IPCEINVAL;
        return IPC_ERROR;
    }

    if (flags & (IPCONLINE | IPCOFFLINE))
    {
        rc = ipcRegisterServicePsNotify(q,
                                      qflag,
                                      name->ipc_node,
                                      name->ipc_service,
                                      msg,
                                      msglen,
                                      flags & (IPCONLINE | IPCOFFLINE),
                                      lasterror);
        if (rc < 0)
        {
            return IPC_ERROR;
        }
    }

    flags &= ~(IPCONLINE | IPCOFFLINE);

    if (flags == 0)
    {
        return IPC_OK;
    }

    destNode = name->ipc_node;
    destService = name->ipc_service;
    if (destNode == IPC_NODE_ANY)
    {
        if (!ipcIsPublishedServiceId(destService) ||
            !ipcFindAddrForService(destService, &destAddr))
        {
            *lasterror = IPCEADDRNOTVAL;
            return IPC_ERROR;
        }
        destNode = ipcAddrToNodeId(destAddr);
    }
    else if (destNode != ipcStack_glb.nodeId)
    {
        destAddr = ipcNodeIdToIpcAddr(destNode);
        if (destAddr == IPC_INVALID_ADDR)
        {
            *lasterror = IPCEADDRNOTVAL;
            return IPC_ERROR;
        }
    }

    if (destNode == ipcStack_glb.nodeId)
    {
        if (LOCK(SrvIDTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcpsnotify_imp: Failed to locl SrvIDTable\n"));
            *lasterror = IPCEBUSY;
            return IPC_ERROR;
        }
        socketToRegister = ipcFindSocketBySrvId(destService);
        UNLOCK(SrvIDTable);
        if (socketToRegister == NULL)
        {
            *lasterror = IPCESERVICE;
            return IPC_ERROR;
        }

        if (msglen == 0)
        {
            ipcRemoveNotification(socketToRegister, flags);
        }
        else if (msglen > IPC_NOTIFICATION_MAX_SIZE)
        {
            *lasterror = IPCEINVAL;
            return IPC_ERROR;
        }
        else
        {
            notification = (ipcNotification_t *)ipcAlloc(sizeof(ipcNotification_t), 0);
            if (notification == NULL)
            {
                *lasterror = IPCENOBUFS;
                return IPC_ERROR;
            }

            notification->psFlags = qflag;
            notification->flags = flags;
            notification->msglen = msglen;
            memcpy(notification->msg, msg, msglen);
            notification->destination.u.ps = q;
            /* Store IPC_INVALID_ADDR for local notifications because the
             * local address could change (link up/link down) */
            notification->destination.ipcAddr = IPC_INVALID_ADDR;

            /* Insert new notification into socket */
            if (ipcLockSocket(socketToRegister) < 0)
            {
                ipcFree(notification);
                *lasterror = IPCEBUSY;
                return IPC_ERROR;
            }
            notification->next = socketToRegister->notifications;
            socketToRegister->notifications = notification;
            ipcUnlockSocket(socketToRegister);
        }
    }
    else
    {
        /* to a remote node */
        rc = ipcRegisterPsNotification(
                destAddr,
                name->ipc_service,
                flags,
                msg,
                msglen,
                qflag,
                q);

        if (rc < 0)
        {
            *lasterror = rc;
            return IPC_ERROR;
        }
    }
    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetmemtype_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function gets the type of memory that may be used to reach a node.
*   
*   PARAMETER(S):
*       address: [in] address of desired service
*       memtype: [out] type of shared memory available for zero-copy purpose.
*       
*   RETURN: if an error occurs, an error of IPC_ERROR will be returned, and a
*           specified error code can be retrieved by calling ipcgetlasterror.
*   
*           IPCEADDRNOTVAL - the specified ipc address is not valid
*           IPCENOTINIT - ipcinit was not called before making this call
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Oct2005  Motorola Inc.         Initial Creation
* 17Jan2007  Motorola Inc.         Added checking for lock failure
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcgetmemtype_imp(const struct IPCSOCK_ADDR* address,
                  unsigned long* memtype,
                  int* lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t* drtItem = NULL;
    ipcAddress_t ipcAddr = IPC_INVALID_ADDR;
    int rc = IPC_OK;

/*--------------------------------------- CODE -----------------------------------------*/
    if ((address == NULL) || (memtype == NULL))
    {
        *lasterror = IPCEADDRNOTVAL;
        return IPC_ERROR;
    }

    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcgetmemtype_imp: Failed to lock DRT table\n"));
        *lasterror = IPCEBUSY;
        return IPC_ERROR;
    }
    drtItem = ipcNodeIdToDrtItemWithoutLock(address->ipc_node);
    if (drtItem != NULL)
    {
        ipcAddr = drtItem->ipcaddr;
    }
    else
    {
        rc = IPCEADDRNOTVAL;
    }
    UNLOCK(DRTTable);

    if (rc < 0)
    {
        *lasterror = rc;
        return IPC_ERROR;
    }
    
    *memtype = ipcRouterGetMemType(ipcAddr);
    
    return IPC_OK;
}
