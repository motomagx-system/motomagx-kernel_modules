/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_session_util.c
*   FUNCTION NAME(S): ipcAddSocketToLocalSrvIdTab
*                     ipcAllocateServiceId
*                     ipcBufferToDRT
*                     ipcBufferToDRTItem
*                     ipcCalDRTSize
*                     ipcDRTItemSize
*                     ipcDRTItemToBuffer
*                     ipcDRTToBuffer
*                     ipcDiscardForDsm
*                     ipcDrtAddService
*                     ipcDrtChangeAddItem
*                     ipcDrtChangeFreeAll
*                     ipcDrtChangeItemNew
*                     ipcDrtChangeSetInit
*                     ipcDrtChangeSignal
*                     ipcDrtHasChanged
*                     ipcDrtRemoveItem
*                     ipcDrtRemoveService
*                     ipcDrtUpdateItem
*                     ipcFindAddrForService
*                     ipcFreeDRT
*                     ipcFreeDRTItem
*                     ipcHandleNodeDown
*                     ipcIsServiceIdAvailable
*                     ipcListCompareDrtIpcAddr
*                     ipcListCompareDrtSrvId
*                     ipcLocateServiceType
*                     ipcNewDRTItem
*                     ipcNodeIdToDrtItemWithoutLock
*                     ipcNodeIdToIpcAddr
*                     ipcNodeIdToIpcAddrWithoutLock
*                     ipcRemoveRemoteDRTItems
*                     ipcRemoveSocketFromLocalSrvIdTab
*                     ipcSrvIdListCompare
*                     ipcUpdateDrtLookupTables
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jun2004  Motorola Inc.         Porting to c version
* 11Jul2004  Motorola Inc.         IPC integreation restructure
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 21Nov2006  Motorola Inc.         Support DSM message discard
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 12Apr2007  Motorola Inc.         fix checksum issue
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
* 19Jul2007  Motorola Inc.         Cleanup unused macros
*****************************************************************************************/
/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_session.h"
#include "ipc_defs.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

/*------------------------------------ ENUMERATIONS ------------------------------------*/

/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

/*------------------------------------- STATIC DATA ------------------------------------*/

/*------------------------------------- GLOBAL DATA ------------------------------------*/
extern ipcSession_t ipcSession_glb;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcNewDRTItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Create a new drt item
*   
*   PARAMETER(S):
*       void
*                  
*   RETURN: the new drt item
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Jul2005  Motorola Inc.         Initial Version 
*
*****************************************************************************************/
ipcDRTItem_t* ipcNewDRTItem(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDRTItem_t* item = ipcAlloc(sizeof(ipcDRTItem_t), 0);
    if (item != NULL)
    {
        ipcListInit(&item->srvId_list); 
    }
    return item;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcNodeIdToIpcAddrWithoutLock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert a Node ID to an IPC Address.
*   
*   NOTE: This function will not lock DRT table, so we assume DRT table is
*   locked before this function is called.
*   
*   PARAMETER(S):
*       nodeId
*                  
*   RETURN: corresponding IPC Address (or IPC_INVALID_ADDRESS)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Dec2005  Motorola Inc.         Initial Version 
*
*****************************************************************************************/
ipcAddress_t ipcNodeIdToIpcAddrWithoutLock(ipcNodeId_t nodeId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    void *ipcAddrInHash;
    ipcAddress_t ipcAddr = IPC_INVALID_ADDR;
/*--------------------------------------- CODE -----------------------------------------*/
    /* First check if this is our own node ID */
    if (nodeId == ipcStack_glb.nodeId)
    {
        ipcAddr = ipcStack_glb.ipcAddr;
    }
    else
    {
        /* It's not our own node so check the hash table */
        ipcAddrInHash = ipcHashFind(&ipcSession_glb.nodeIdToIpcAddrTable, (void *)nodeId);

        if (ipcAddrInHash != NULL)
        {
            /* stored with offset of 1 because zero is a valid IPC Address (not to be
             * confused with NULL which means it was not found in the hash table) */
            ipcAddr = (ipcAddress_t)((int)ipcAddrInHash - 1);
        }
    }
    
    return ipcAddr;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcNodeIdToDrtItemWithoutLock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert a Node ID to an DRT item.
*   
*   NOTE: This function will not lock DRT table, so we assume DRT table is
*   locked before this function is called.
*   
*   PARAMETER(S):
*       nodeId
*                  
*   RETURN: corresponding IPC Address (or IPC_INVALID_ADDRESS)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Apr2006  Motorola Inc.         Initial Version 
*
*****************************************************************************************/
ipcDRTItem_t* ipcNodeIdToDrtItemWithoutLock(ipcNodeId_t nodeId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t* drtItem = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    drtItem = ipcAddrToDrtItem(ipcNodeIdToIpcAddrWithoutLock(nodeId));
    if (drtItem != NULL)
    {
        if (drtItem->node_id != nodeId)
        {
            drtItem = NULL;
        }
    }
    return drtItem;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcNodeIdToIpcAddr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert a Node ID to an IPC Address.
*   
*   NOTE: This function will lock DRT table, so we assume DRT table is NOT
*   locked before this function is called.
*   
*   PARAMETER(S):
*       nodeId
*                  
*   RETURN: corresponding IPC Address (or IPC_INVALID_ADDRESS)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jan2004  Motorola Inc.         Initial Version
* 13Dec2005  Motorola Inc.         Directly call function ipcNodeIdToIpcAddrWithoutLock
* 23May2006  Motorola Inc.         Do not lock DRT for local node
* 11Jan2007  Motorola Inc.         Checked for failure of locking 
*
*****************************************************************************************/
ipcAddress_t ipcNodeIdToIpcAddr(ipcNodeId_t nodeId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcAddress_t ipcAddr = IPC_INVALID_ADDR;
/*--------------------------------------- CODE -----------------------------------------*/
    /* First check if this is our own node ID */
    if (nodeId == ipcStack_glb.nodeId)
    {
        ipcAddr = ipcStack_glb.ipcAddr;
    }
    else
    {        
        if (LOCK(DRTTable) < 0)
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcNodeIdToIpcAddr: Failed to lock DRT table\n"));
            return IPC_INVALID_ADDR;
        }
        ipcAddr = ipcNodeIdToIpcAddrWithoutLock(nodeId);
        UNLOCK(DRTTable);
    }
    return ipcAddr;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUpdateDrtLookupTables
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function updates the IPC Addr to DRT Item and Node ID to IPC Addr
*   lookup tables based on the current DRT Table
*   
*   PARAMETER(S):
*       drtTable: pointer to DRT Table (an IPC list)
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jan2004  Motorola Inc.         Initial Version
* 19Dec2005  Motorola Inc.         Trace node down event
* 11Jan2007  Motorola Inc.         Removed node down handling 
*
*****************************************************************************************/
void ipcUpdateDrtLookupTables(ipcList_t *drtTable)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t *drtItem;
    int i = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    memset(ipcSession_glb.ipcAddrToDrtItemLookup, 0, sizeof(ipcSession_glb.ipcAddrToDrtItemLookup));

    /*  Clear nodeIdToIpcAddrTable   */
    ipcHashRemoveAll(&ipcSession_glb.nodeIdToIpcAddrTable);

    for (drtItem = (ipcDRTItem_t *)drtTable->head;
         drtItem != NULL;
         drtItem = drtItem->next)
    {
        ipcSession_glb.ipcAddrToDrtItemLookup[drtItem->ipcaddr] = drtItem;
        ipcAddDrtItemToNodeIdTable(drtItem);
    }
    for (i = 0; i < IPC_INVALID_ADDR + 1; i++)
    {
        if (ipcSession_glb.ipcAddrToDrtItemLookup[i] != NULL)
        {
            ipcSession_glb.ipcAddrToNodeId[i] = ipcSession_glb.ipcAddrToDrtItemLookup[i]->node_id;
            
        }
        else
        {
            ipcSession_glb.ipcAddrToNodeId[i] = 0;
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDRTItemToBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert DRT item to data buffer.
*   See ipc_messages.h for DRT item data buffer structure.
*   
*   PARAMETER(S):
*       item: pointer to DRT item
*       buffer: pointer to data buffer
*                  
*   RETURN: buffer length
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jun2004  Motorola Inc.         Porting to c version
* 26Jul2004  Motorola Inc.         change the usage of ListMgr
* 21Nov2006  Motorola Inc.         Updated DRT item encoding to  include DSM action 
*
*****************************************************************************************/
int ipcDRTItemToBuffer(const ipcDRTItem_t *item, drtItemData_t *buffer)
{/* push the drt item into buffer */
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i = 0;
    int num_services;
    ipcDRTListLink_t link;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((item == NULL) || (buffer == NULL))
    {
        return 0;
    }

    num_services = item->srvId_list.size;

    ipcOsalWriteAligned32(&buffer->node_id, item->node_id);     /* 1. ipc node ID (32 bits) */
    ipcOsalWrite8(&buffer->ipcaddr, item->ipcaddr);             /* 2. ipc addr (8 bits) */
    ipcOsalWrite8(&buffer->datatype, item->datatype);           /* 3. date type (8 bits) */
    ipcOsalWriteAligned16(&buffer->num_services, num_services); /* 4. num services the node supports (16 bits) */

    for (link = (ipcDRTListLink_t)item->srvId_list.head; link != NULL; link = link->next)
    {
        ipcOsalWriteAligned16(&buffer->services[i].srvId, link->data.srvId);
        ipcOsalWriteAligned16(&buffer->services[i].dsmAction, link->data.dsmAction);
        i++;
    }

    return calcDrtItemSize(num_services);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcBufferToDRTItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert data buffer to DRT item.
*   See ipc_messages.h for DRT item data buffer structure.
*   
*   PARAMETER(S):
*       buffer: pointer to data buffer
*       item: pointer to DRT item
*                  
*   RETURN: buffer length
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jun2004  Motorola Inc.         porting to c version
* 21Nov2006  Motorola Inc.         Updated DRT item encoding to  include DSM action 
*
*****************************************************************************************/
int ipcBufferToDRTItem(const drtItemData_t *buffer, ipcDRTItem_t *item)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i = 0;
    int num_services;
    ipcDRTListLink_t drtLink;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((buffer == NULL) || (item == NULL))
    {
        return 0;
    }
    
    ipcListFreeAll(&item->srvId_list);

    item->node_id = ipcOsalReadAligned32(&buffer->node_id);     /* 1. ipc node ID (32 bits) */
    item->ipcaddr = ipcOsalRead8(&buffer->ipcaddr);             /* 2. ipc addr (8 bits) */
    item->datatype = ipcOsalRead8(&buffer->datatype);           /* 3. date type (8 bits) */
    num_services = ipcOsalReadAligned16(&buffer->num_services); /* 4. num services the node supports (16 bits) */

    for (i = 0; i < num_services; i++)
    {
        drtLink = (ipcDRTListLink_t)ipcAlloc(sizeof(ipcDRTListItem_t), 0);
        if (drtLink == NULL)
        {
            ipcListFreeAll(&item->srvId_list);
            ipcError(IPCLOG_SESSION, ("Failed to alloc new DRT list item\n"));
            return 0;
        }
            
        drtLink->data.srvId = ipcOsalReadAligned16(&buffer->services[i].srvId);
        drtLink->data.dsmAction = ipcOsalReadAligned16(&buffer->services[i].dsmAction);
        ipcListAddTail(&item->srvId_list, (ipcListLink_t)drtLink);
    }

    return calcDrtItemSize(num_services);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDRTToBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert DRT list to data buffer.
*   
*   PARAMETER(S):
*       drt_table: pointer to DRT list
*       buffer: pointer to data buffer
*                  
*   RETURN: buffer length
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jun2004  Motorola Inc.         Porting to c version
* 21Nov2006  Motorola Inc.         Updated DRT encoding for DSM message discard 
*
*****************************************************************************************/
int ipcDRTToBuffer(ipcList_t *drt_table, UINT8 *buffer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int offset = 0;
    ipcDRTItem_t *drt_item;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((drt_table == NULL) || (buffer == NULL)) 
    {
        return 0;
    }

    for (drt_item = (ipcDRTItem_t *)drt_table->head;
         drt_item != NULL;
         drt_item = drt_item->next)
    {
        offset += ipcDRTItemToBuffer(drt_item, (drtItemData_t *)(buffer + offset));
    }

    return offset;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcBufferToDRT
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert data buffer to DRT list.
*   
*   PARAMETER(S):
*       buffer: pointer to data buffer
*       drt_table: pointer to DRT list
*                  
*   RETURN: buffer length
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 14Jun2004  Motorola Inc.         Porting to c version
* 21Nov2006  Motorola Inc.         Updated DRT encoding for DSM message discard 
*
*****************************************************************************************/
int ipcBufferToDRT(const UINT8 *buffer, ipcList_t *drt_table, int numDrtItems)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i, length;
    int offset = 0;
    ipcDRTItem_t *drt_item;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((buffer == NULL) || (drt_table == NULL))
    {
        return 0;
    }

    ipcFreeDRT(drt_table);
    for (i = 0; i < numDrtItems; i++)
    {
        drt_item = ipcNewDRTItem();
        if (drt_item == NULL)
        {
            ipcError(IPCLOG_SESSION, ("Failed to create new DRT item\n"));
            ipcFreeDRT(drt_table);
            return 0;
        }

        length = ipcBufferToDRTItem((const drtItemData_t *)(buffer + offset), drt_item);
        if (length == 0)
        {
            ipcError(IPCLOG_SESSION, ("ipcBufferToDRTItem failed\n"));
            ipcFree(drt_item);
            return 0;
        }
        else
        {
            offset += length; 
            ipcListAddTail(drt_table, (ipcListLink_t)drt_item);
        }
    }

    return offset;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFreeDRTItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to remove all DRT items in list.
*   
*   PARAMETER(S):
*       item: pointer to DRT item
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jun2004  Motorola Inc.         Initial Version 
*
*****************************************************************************************/
void ipcFreeDRTItem(ipcDRTItem_t *item)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/

/*--------------------------------------- CODE -----------------------------------------*/
    ipcListFreeAll(&item->srvId_list);
    ipcFree(item);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFreeDRT
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to clean up DRT table.
*   
*   PARAMETER(S):
*       drt_table: pointer to DRT table
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jun2004  Motorola Inc.         Initial Version 
*
*****************************************************************************************/
void ipcFreeDRT(ipcList_t *drt_table)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t *drt_item;
/*--------------------------------------- CODE -----------------------------------------*/
    if (drt_table == NULL) 
    {
        return;
    }

    while (drt_table->head != NULL)
    {
        drt_item = (ipcDRTItem_t *)ipcListRemoveHead(drt_table);
        ipcFreeDRTItem(drt_item);
    }
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtHasChanged
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called whenever the local DRT changes (for clients) or when
*   any DRT item changes (for the server).  It will start the client or server
*   config request timer if it is not already active to make sure that the
*   SSSSConfigReq or SSSSConfigUpdInd is sent out.
*   
*   PARAMETER(S):
*       void
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Jan2005  Motorola Inc.         Initial Version 
*
*****************************************************************************************/
void ipcDrtHasChanged()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ((!ipcStack_glb.isServer) 
        && (ipcSession_glb.state != IPC_SESSION_STATE_NETWORK_READY))
    {
        /* If we are a client node and we are not in the network ready state, do nothing */
        return;
    }
    
    if (!ipcOsalTimerIsActive(ipcSession_glb.configReqTimer))
    {
        if (LOCK(DRTTable) == IPCE_OK)
        {
            /* Timer is not active so start it now */
            ipcOsalTimerStart(ipcSession_glb.configReqTimer);
            UNLOCK(DRTTable);
        }
        else
        {
            ipcError(IPCLOG_SESSION,
                    ("ipcDrtHasChanged: Failed to lock DRT table\n"));
        }
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDRTItemSize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to get length of msg data in SSSSConfigRequest
*   
*   PARAMETER(S):
*       ipcDRTItem_t *item -- drt_item
*                  
*   RETURN: length of DRT item
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Aug2004  Motorola Inc.         Initial Version
* 29Dec2004  Motorola Inc.         Optimize
* 21Nov2006  Motorola Inc.         Updated DRT encoding for DSM message discard 
*
*****************************************************************************************/
UINT16 ipcDRTItemSize(ipcDRTItem_t *item)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT16 size = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (item != NULL)
    { 
        size = calcDrtItemSize(item->srvId_list.size);
    }
    
    return size;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCalDRTSize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to get length of msg data in SSSSConfigRequest.
*   
*   PARAMETER(S):
*       ipcList_t *drt -- DRT table
*                  
*   RETURN: length of DRT table
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Nov2006  Motorola Inc.         Updated DRT encoding for DSM message discard 
*
*****************************************************************************************/
UINT16 ipcCalDRTSize(ipcList_t *drt)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT16 size = 0;
    ipcDRTItem_t *drtItem;
/*--------------------------------------- CODE -----------------------------------------*/
    if (NULL == drt)
    {
        return 0;
    }

    for (drtItem = (ipcDRTItem_t *)drt->head; drtItem != NULL; drtItem = drtItem->next)
    {
        size += ipcDRTItemSize(drtItem);
    }

    return size;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcIsServiceIdAvailable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to check whether the specified service ID is valid for
*   a new socket to bind to.  The rules are as follows:
*   
*   It must be a published service ID since all unpublished service IDs are
*   automatically assigned by the IPC Stack (socket binds to IPC_SERVICE_ANY).
*   This should be checked before this function is called!
*   
*   It must not be in use unless it is a multicast service ID
*   
*   All multicast service IDs are valid (as long as the published bit is set)
*   
*   PARAMETER(S):
*       srvId: the service id to check
*                  
*   RETURN: TRUE  - the service id is valid
*           FALSE - the service id is invalid
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Sep2004  Motorola Inc.         Initial Version
* 14Jan2005  Motorola Inc.         Updated to match spec
*
*****************************************************************************************/
BOOL ipcIsServiceIdAvailable(ipcServiceId_t srvId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL valid = TRUE;
/*--------------------------------------- CODE -----------------------------------------*/

    if (!ipcIsMulticastServiceId(srvId))
    {
        /* Any multicast service ID is valid so we only need to check if the
         * service ID is already in use for non-multicast service IDs */
        if (ipcHashFind(&ipcSession_glb.local_srvid_tab, ipcCastPtr(srvId, void*)) != NULL)
        {
            /* Service ID is in use so we cannot bind another socket to it */
            valid = FALSE;
        }
    }

    return valid;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAllocateServiceId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to generate a new service ID.
*   
*   PARAMETER(S):
*       none.
*                  
*   RETURN: service id.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jan2007  Motorola Inc.         Removed locking of srvIdTable 
*
*****************************************************************************************/
ipcServiceId_t ipcAllocateServiceId()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    static ipcServiceId_t srvId = 0x0001;

    int iterationsLeft = IPC_LAST_UNPUB_SRV_ID - IPC_FIRST_UNPUB_SRV_ID + 1;
    ipcServiceId_t srvIdToReturn;
/*--------------------------------------- CODE -----------------------------------------*/

    /* Increment Service ID until we find one that isn't taken or we have
     * tried every single one */
    while ((iterationsLeft-- > 0) && 
           (ipcHashFind(&ipcSession_glb.local_srvid_tab, 
                        ipcCastPtr(srvId, void*)) != NULL))
    {
        if (srvId++ == IPC_LAST_UNPUB_SRV_ID)
        {
            srvId = IPC_FIRST_UNPUB_SRV_ID;
        }
    }

    /* If we tried every service ID and they were all taken, return IPC_SERVICE_ANY */
    if (iterationsLeft == -1)
    {
        srvIdToReturn = IPC_SERVICE_ANY;
    }
    else 
    {
        srvIdToReturn = srvId;

        if (srvId++ == IPC_LAST_UNPUB_SRV_ID)
        {
            srvId = IPC_FIRST_UNPUB_SRV_ID;
        }
    }

    return(srvIdToReturn);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSrvIdListCompare
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Compare two service ID lists, assuming they are SORTED.
*   
*   PARAMETER(S):
*       srvIdlist1: a pointer to a service id list
*       srvIdlist2: another pointer to a service id list
*       
*   RETURN: TRUE if the two lists contain the same service IDs
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 21Nov2006  Motorola Inc.         Use new ipcDRTListLink_t struct 
*
*****************************************************************************************/
BOOL ipcSrvIdListCompare(ipcList_t *srvIdlist1, ipcList_t *srvIdlist2)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTListLink_t link1, link2;
/*--------------------------------------- CODE -----------------------------------------*/
    /* if the size is not the same, return false. */
    if (srvIdlist1->size != srvIdlist2->size) 
    {
        return FALSE;
    }
    
    /* the srvIds are sorted so we can compare them in order */
    for (link1 = (ipcDRTListLink_t)srvIdlist1->head, link2 = (ipcDRTListLink_t)srvIdlist2->head;
         link1 != NULL;
         link1 = link1->next, link2 = link2->next)
    {
        if (link1->data.srvId != link2->data.srvId)
        {
            return FALSE;
        }
    }
    
    return TRUE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAddSocketToLocalSrvIdTab
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Add a socket into the local service id table. 
*   
*   PARAMETER(S):
*       socket: pointer to the socket to be added
*       
*   RETURN: TRUE if DRT has been changed after the socket is added
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         Initial Creation
* 13Jan2005  Motorola Inc.         Data structure of the table changed
* 03Nov2005  Motorola Inc.         Added error checking
* 11Jan2007  Motorola Inc.         Removed locking srvIdTable 
*
*****************************************************************************************/
BOOL ipcAddSocketToLocalSrvIdTab(ipcSocket_t *socket)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL drtChanged = FALSE;
    ipcSocket_t *sockFound;
/*--------------------------------------- CODE -----------------------------------------*/
    /* remove the socket from unbound list */
    ipcListRemoveLink(&ipcSession_glb.unboundSockList, socket);

    /* temporarily remove socket from hash table if it exists */
    sockFound = ipcHashRemove(&ipcSession_glb.local_srvid_tab, 
            ipcCastPtr(socket->srvId, void*));

    /* link up existing sockets to the new one we are putting in (for multicast) */
    socket->next = sockFound;
    if (!ipcHashPut(&ipcSession_glb.local_srvid_tab, 
            ipcCastPtr(socket->srvId, void*), (void *)socket))
    {
        ipcError(IPCLOG_SESSION, ("Failed to add socket to local_srvid_tab"));
    }

    if ((sockFound == NULL) && ipcIsPublishedServiceId(socket->srvId))
    {
        /* We added a new published service ID so we should update the DRT */
        drtChanged = TRUE;
    }

    return(drtChanged);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveSocketFromLocalSrvIdTab
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Remove a socket from local service id table.
*   
*   PARAMETER(S):
*       socket: the socket to be removed
*       needChange: [out] whether DRT need to be updated after this function call
*       
*   RETURN: TRUE if no error happened.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Dec2004  Motorola Inc.         Initial Creation
* 13Jan2005  Motorola Inc.         Data structure of the table changed
* 03Nov2005  Motorola Inc.         Added error checking
* 24Jan2007  Motorola Inc.         Added checking for lock failure 
* 20Mar2007  Motorola Inc.         Added output parameter needChanges
*
*****************************************************************************************/
BOOL ipcRemoveSocketFromLocalSrvIdTab(ipcSocket_t *socket, BOOL* needChange)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL drtChanged = FALSE;
    ipcSocket_t *sockFound;
    ipcList_t tempList;
/*--------------------------------------- CODE -----------------------------------------*/
    if (LOCK(SrvIDTable) < 0)
    {
        /*
         * XXX: Here it should always be OK!
         */
        ipcError(IPCLOG_SESSION, 
                ("ipcRemoveSocketFromLocalSrvIdTab: Failed to lock SrvIDTable\n"));
        return FALSE;
    }
    /* temporarily remove socket from hash table if it exists */
    sockFound = ipcHashRemove(&ipcSession_glb.local_srvid_tab, 
            ipcCastPtr(socket->srvId, void*));

    if (ipcIsMulticastServiceId(socket->srvId))
    {
        ipcListInit(&tempList);
        /* This is a multicast service ID so there may be multiple sockets.
           We will use a temporary list structure to search for our socket */
        tempList.head = (ipcListLink_t)sockFound;

        /* Remove the socket from the list */
        ipcListRemoveLink(&tempList, (ipcListLink_t)socket);

        if (tempList.head != NULL)
        {
            /* There are still sockets left with this multicast ID so put it 
             * back in the local service ID hash table */
            if (!ipcHashPut(&ipcSession_glb.local_srvid_tab, 
                ipcCastPtr(socket->srvId, void*),
                ipcCastPtr(tempList.head, void*)))
            {
                ipcError(IPCLOG_SESSION,
                    ("Failed to add socket to local_srvid_tab"));
            }
        }
        else
        {
            /* We removed the last multicast socket with this service ID so we
             * should update the DRT */
            drtChanged = TRUE;
        }
    }
    else if (sockFound != NULL)
    {
        if (ipcIsPublishedServiceId(socket->srvId))
        {
            /* We removed a published service ID so we should update the DRT */
            drtChanged = TRUE;
        }
    }
    else
    {
        /* The service ID was not found */
        ipcError(IPCLOG_SESSION, ("ipcRemoveSocketFromLocalSrvIdTab -> service id not found!\n"));
    }
    UNLOCK(SrvIDTable);

    *needChange = drtChanged;
    return TRUE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFindAddrForService
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Find ANY IPC address of a node which supports a specified srvId. It searches
*   the DRT table and returns the first qualified item.
*   
*   PARAMETER(S):
*       srvId: the service id to be searched
*       ipcAddr: the ipc address to be found 
*       
*   RETURN: If found, return TRUE,
*           else return FALSE
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jan2005  Motorola Inc.         Initial Creation
* 23May2005  Motorola Inc.         Changed function params and return
* 23Jan2007  Motorola Inc.         Try to avoid nodes in DSM
* 24Jan2007  Motorola Inc.         Added checking for lock failure 
*
*****************************************************************************************/
BOOL ipcFindAddrForService(ipcServiceId_t srvId, ipcAddress_t * ipcAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTItem_t *drt_item;
    BOOL bSrvFound = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    
    if (LOCK(DRTTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcFindAddrForService: Failed to lock DRT table\n"));
        return FALSE;
    }

    /* Search through the entire DRT Table for the requested service ID.
     * The first item in the DRT Table is the local node so we will always
     * return the local IPC address if the service is provided locally. */
    for (drt_item = (ipcDRTItem_t *)ipcSession_glb.drt.head;
         drt_item != NULL;
         drt_item = drt_item->next)
    {
        if (ipcListFind(&drt_item->srvId_list, ipcCastPtr(srvId, void*),
                        ipcListCompareDrtSrvId, NULL))
        {
            /* We found the service ID so set the address */
            *ipcAddr = drt_item->ipcaddr;
            bSrvFound = TRUE;
            
            /* Check if the node we found this service ID on is in deep sleep mode.
             * If it is, we will keep looking to see if we can find the same service
             * on a node that is not in DSM (otherwise break out of loop) */
            if (ipcGetNodeDsmStatus(drt_item->ipcaddr) != IPC_DSM_ASLEEP)
            {
                break;
            }
        }
    }
    UNLOCK(DRTTable);
    
    return bSrvFound;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcLocateServiceType
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get the request type for a locate_service request.
*   
*   PARAMETER(S):
*       node_id: node id to query
*       srvId: service id to query
*       
*   RETURN: The request type
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Sep2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcLocateServiceReqType_t ipcLocateServiceType(UINT32 node_id, 
                                                  ipcServiceId_t srvId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/

/*--------------------------------------- CODE -----------------------------------------*/
    if ((node_id == IPC_NODE_ANY) && (srvId == IPC_SERVICE_ANY))
    {
        return QUERY_NETWORK;
    }
    else if (srvId == IPC_SERVICE_ANY)
    {
        return QUERY_ALL_SRV_IN_ONE_NODE;
    }
    else if (node_id == IPC_NODE_ANY)
    {
        return QUERY_NODE_LIST;
    }
    else
    {
        return QUERY_SUPPORT_STATUS;
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveRemoteDRTItems
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to clean up remote nodes information and update :
*   i) DRT
*   ii) Node id hash table
*   iii) ipcAddrToDrtItemLookup table.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Jan2005  Motorola Inc.         Initial Creation
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 17Jan2007  Motorola Inc.         Cached DRT changes 
*
*****************************************************************************************/
void ipcRemoveRemoteDRTItems(ipcDrtChangeSet_t* changes)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcListRemoveHead(&ipcSession_glb.drt);

    while (ipcSession_glb.drt.head != NULL)
    {
        ipcDrtRemoveItem(&ipcSession_glb.drt, NULL, changes);
    }
    ipcListAddHead(&ipcSession_glb.drt, (void *)ipcSession_glb.localDrtItem);

    ipcUpdateDrtLookupTables(&ipcSession_glb.drt);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListCompareDrtIpcAddr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Comparison function used to find a DRT item using an IPC address.
*   
*   PARAMETER(S):
*       link: list link to compare to
*       ipcAddr: IPC address we are searching for
*       
*   RETURN: TRUE if ipcAddr is identical to the ipcaddr for that item
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcListCompareDrtIpcAddr(ipcListLink_t link, void *ipcAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return ((ipcDRTItem_t *)link)->ipcaddr == ipcCastPtr(ipcAddr, ipcAddress_t);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListCompareDrtSrvId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Comparison function used to find a DRT list item using a service ID.
*   
*   PARAMETER(S):
*       link: list link to compare to
*       serviceId: service ID we are searching for
*       
*   RETURN: TRUE if serviceId is identical to the serviceID for that list link
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Nov2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcListCompareDrtSrvId(ipcListLink_t link, void *serviceId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return ((ipcDRTListLink_t)link)->data.srvId == ipcCastPtr(serviceId, ipcServiceId_t);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleNodeDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when a node is removed from DRT table. It removes 
*   all relevant data of that node.
*   
*   PARAMETER(S):
*       nodeId: the node id removed from DRT
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2005  Motorola Inc.         Initial Creation
* 25Jan2006  Motorola Inc.         Early return for local node
* 17Jan2007  Motorola Inc.         Cached down sockets 
*
*****************************************************************************************/
void ipcHandleNodeDown(ipcNodeId_t nodeId)
{
    /*
     * XXX: This function is always executed by main thread,
     * so it can't fail with locking
     */
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* socket = NULL;
    ipcList_t brokenSockets;
/*--------------------------------------- CODE -----------------------------------------*/

    if (nodeId == ipcStack_glb.nodeId)
    {
        return;
    }

    ipcListInit(&brokenSockets);
    /* FIXME: Maybe the locking here will cause dead lock, CHECK!! */
    if (LOCK(SrvIDTable) < 0)
    {
        ipcError(IPCLOG_SESSION,
                ("ipcHandleNodeDown: Failed to lock SrvIDTable\n"));
        return;
    }

    /* for all sockets, see if the destination is that node, if so, place the
     * socket to NOT_CONNECTED state */    
    for (ipcHashResetIterator(&ipcSession_glb.local_srvid_tab);
         NULL != (socket = (ipcSocket_t*)ipcHashGetNext(&ipcSession_glb.local_srvid_tab));)
    {
        while (socket != NULL)
        {
            if (socket->destNode == nodeId)
            {
                /* the destination of this socket is no longer available */
                ipcWarning(IPCLOG_SESSION, ("Peernode %d of socket %p is down\n",
                    nodeId, socket));
                
                if (ipcLockSocket(socket) == IPCE_OK)
                {
                    ipcRefSocket(socket);
                    ipcUnlockSocket(socket);
                }
                else
                {
                    ipcError(IPCLOG_SESSION, 
                            ("ipcHandleNodeDown: Failed to lock socket %p\n", socket));
                    ipcRefSocket(socket);
                }

                ipcListAddObjectToTail(&brokenSockets, socket);
            }
            socket = socket->next;
        }
    }
    UNLOCK(SrvIDTable);

    while (brokenSockets.size > 0)
    {
        socket = ipcListRemoveObjectFromHead(&brokenSockets);
        if (ipcIsSocketValid(socket) && (socket->destNode == nodeId))
        {
            ipcCOSocketReceiveEnd(socket);
            ipcDisconnectSocket(socket);

            if (ipcLockSocket(socket) == IPCE_OK)
            {
                ipcUnrefSocket(socket);
                ipcUnlockSocket(socket);
            }
            else
            {
                ipcError(IPCLOG_SESSION,
                        ("ipcHandleNodeDown: Failed to lock socket %p\n", socket));
                ipcUnrefSocket(socket);
            }
        }
    }

    ipcListFreeAll(&brokenSockets);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtChangeItemNew
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates a new DRT change item
*   
*   PARAMETER(S):
*       nodeId: The node id of the change
*       srvId: The service id of the change
*       change: IPCONLINE or IPCOFFLINE
*       
*   RETURN: the newly created change item
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcDrtChangeItem_t* ipcDrtChangeItemNew(ipcNodeId_t nodeId, ipcServiceId_t srvId, int change)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDrtChangeItem_t* item = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    item = (ipcDrtChangeItem_t*)ipcAlloc(sizeof(ipcDrtChangeItem_t), 0);
    if (item != NULL)
    {
        item->nodeId = nodeId;
        item->srvId = srvId;
        item->change = change;
    }
    else
    {
        ipcError(IPCLOG_SESSION,
                ("ipcDrtChangeItemNew: Failed to alloc DRT change item\n"));
    }
    return item;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtChangeSetInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function initializes a DRT change set
*   
*   PARAMETER(S):
*       nodeId: The change set to be initialized
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcDrtChangeSetInit(ipcDrtChangeSet_t* changeSet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcListInit(&changeSet->changes);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtChangeAddItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function adds an item to a DRT change set
*   
*   PARAMETER(S):
*       changeSet: the set of changes
*       item: the drt change item
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcDrtChangeAddItem(ipcDrtChangeSet_t* changeSet, ipcDrtChangeItem_t* item)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (item != NULL)
    {
        ipcListAddTail(&changeSet->changes, (ipcListLink_t)item);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtChangeFreeAll
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function frees all items in a DRT change set
*   
*   PARAMETER(S):
*       changeSet: the set of changes
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcDrtChangeFreeAll(ipcDrtChangeSet_t* changeSet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcListFreeAll(&changeSet->changes);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtChangeSignal
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function takes action for a giving DRT change set. For every change, it
*   signals a service online/offline notification, and if a node gets offline
*   in the change set, this function will disconnect all CO sockets whose dest
*   node is that node.
*   
*   PARAMETER(S):
*       changeSet: the set of changes
*       needSendDrt: whether the DRT need to be sent
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Dec2006  Motorola Inc.         Initial Creation 
* 31May2007  Motorola Inc.         Added parameter needSendDrt
*
*****************************************************************************************/
void ipcDrtChangeSignal(ipcDrtChangeSet_t* changeSet, BOOL needSendDrt)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDrtChangeItem_t* item = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    if (needSendDrt && (changeSet->changes.size > 0))
    {
        ipcDrtHasChanged();
    }

    for (item = (ipcDrtChangeItem_t*)changeSet->changes.head;
         item != NULL;
         item = item->next)
    {
        ipcSignalServiceNotification(item->nodeId, item->srvId, item->change);
        /* If this change indicates a node is down, handle it */
        if ((item->change == IPCOFFLINE) &&
            (item->srvId == IPC_SERVICE_ANY))
        {
            ipcHandleNodeDown(item->nodeId);
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtRemoveItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Remove an item from DRT, the item to be removed is the next link of prev, if
*   prev is NULL, then the first item will be removed.
*   Removing a DRT item will send out node down notifications.
*   
*   PARAMETER(S):
*       drt: pointer to DRT table
*       prev: the previous link to the item to be removed
*       changes: [out] the DRT change set
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 21Nov2006  Motorola Inc.         Use new ipcDRTListLink_t struct
* 17Jan2007  Motorola Inc.         Cached DRT changes 
*
*****************************************************************************************/
void ipcDrtRemoveItem(ipcList_t* drt, ipcDRTItem_t* prev, ipcDrtChangeSet_t* changes)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTListLink_t link = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    ipcDRTItem_t* item = (ipcDRTItem_t*)ipcListRemoveNextLink(drt, 
                                                (ipcListLink_t)prev);

    if (item != NULL)
    {
        for (link = (ipcDRTListLink_t)item->srvId_list.head;
            link != NULL;
            link = link->next)
        {
            ipcDrtChangeAddItem(changes, 
                    ipcDrtChangeItemNew(item->node_id,
                                         link->data.srvId,
                                         IPCOFFLINE));
        }
        /* The node down event */
        ipcDrtChangeAddItem(changes,
                ipcDrtChangeItemNew(item->node_id,
                                    IPC_SERVICE_ANY,
                                    IPCOFFLINE));
        ipcFreeDRTItem(item);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtUpdateItem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Update a DRT item from old_item to new_item, after calling this function,
*   the service ids in old_item are updated, and a DRT item containing the old
*   service id list is returned, which should be freed later.
*   
*   PARAMETER(S):
*       old_item: the DRT item to be updated
*       new_item: the new item    
*       changes: [out] the DRT change set
*       
*   RETURN: If old_item is NULL, it returns NULL; otherwise it returns new_item,
*           but the service id list in new_item is changed to the original list
*           in old_item.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 08May2006  Motorola Inc.         Fixed bug of binding to 0xFFFF
* 21Nov2006  Motorola Inc.         Use new ipcDRTListLink_t struct
* 17Jan2007  Motorola Inc.         Cached DRT changes 
*
*****************************************************************************************/
ipcDRTItem_t* ipcDrtUpdateItem(ipcDRTItem_t* old_item, 
                               ipcDRTItem_t* new_item,
                               ipcDrtChangeSet_t* changes)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDRTListLink_t curr_old;
    ipcDRTListLink_t curr_new;
    ipcList_t obsolete_list;
    ipcDRTItem_t* returnedItem;
    UINT32 old_srvId;
    UINT32 new_srvId;
/*--------------------------------------- CODE -----------------------------------------*/

    /* If old_item is NULL, a new node is added to the DRT */
    if (old_item == NULL)
    {
        ipcListAddTail(&ipcSession_glb.drt, (ipcListLink_t)new_item);
        returnedItem = old_item;

        for (curr_new = (ipcDRTListLink_t)new_item->srvId_list.head;
                curr_new != NULL;
                curr_new = curr_new->next)
        {
            new_srvId = curr_new->data.srvId;
            ipcDrtChangeAddItem(changes,
                    ipcDrtChangeItemNew(new_item->node_id, 
                                        (UINT16)new_srvId,
                                        IPCONLINE));
        }
        /* The node up event */
        ipcDrtChangeAddItem(changes,
                ipcDrtChangeItemNew(new_item->node_id, 
                    (UINT16)IPC_SERVICE_ANY,
                    IPCONLINE));
    }
    else
    {
        /* swap old_item->srvId_list and new_item->srvId_list */
        obsolete_list = old_item->srvId_list;
        old_item->srvId_list = new_item->srvId_list;

        /* Compare obsolete_list and new_item->srvId_list, 
         * both the lists are sorted */
        curr_old = (ipcDRTListLink_t)obsolete_list.head;
        curr_new = (ipcDRTListLink_t)new_item->srvId_list.head;
        while ((curr_old != NULL) || (curr_new != NULL))
        {
            old_srvId = (curr_old == NULL)? 
                0x10000: /* the max service id is 0xFFFF */
                curr_old->data.srvId;
            new_srvId = (curr_new == NULL)?
                0x10000:
                curr_new->data.srvId;

            if (old_srvId < new_srvId)
            {
                ipcDrtChangeAddItem(changes,
                        ipcDrtChangeItemNew(old_item->node_id, 
                            (ipcServiceId_t)old_srvId,
                            IPCOFFLINE));
                if (curr_old != NULL)
                { 
                    curr_old = curr_old->next;
                }
            }
            else if (old_srvId > new_srvId)
            {
                ipcDrtChangeAddItem(changes,
                        ipcDrtChangeItemNew(new_item->node_id, 
                            (ipcServiceId_t)new_srvId,
                            IPCONLINE));
                if (curr_new != NULL)
                {
                    curr_new = curr_new->next;
                }
            }
            else
            {
                if ((curr_new != NULL) && (curr_old != NULL))
                {
                    curr_old = curr_old->next;
                    curr_new = curr_new->next;
                }
            }
        }
        new_item->srvId_list = obsolete_list;
        returnedItem = new_item;
    }
    return returnedItem;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtAddService
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Add a service id into item. All the service ids in the DRT item are sorted.
*   
*   PARAMETER(S):
*       item: the target DRT item
*       service: the service id to be added
*       dsmAction: IPC_DSM_WAKE_UP or IPC_DSM_DISCARD (whether or not
*                              this node should wake up from deep sleep mode when a 
*                              message is sent to this service)
*       changes: [out] the DRT change set
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 21Nov2006  Motorola Inc.         Use new ipcDRTListLink_t struct
* 17Jan2007  Motorola Inc.         Cached DRT changes 
*
*****************************************************************************************/
void ipcDrtAddService(ipcDRTItem_t* item, ipcServiceId_t service, UINT16 dsmAction,
                      ipcDrtChangeSet_t* changes)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev;
    ipcListLink_t curr;
    ipcServiceId_t tempService = IPC_SERVICE_ANY;
    ipcDRTListLink_t drtListLink;
/*--------------------------------------- CODE -----------------------------------------*/
    drtListLink = (ipcDRTListLink_t)ipcAlloc(sizeof(ipcDRTListItem_t), 0);
    if (drtListLink == NULL)
    {
        ipcError(IPCLOG_SESSION, ("Failed to insert service id to DRT"));
        return;
    }
    drtListLink->data.srvId = service;
    drtListLink->data.dsmAction = dsmAction;

    for (ipcListFirstPair(item->srvId_list, prev, curr);
        curr != NULL;
        ipcListNextPair(prev, curr))
    {
        tempService = ((ipcDRTListLink_t)curr)->data.srvId;
        if (tempService > service)
        {
            ipcListInsertLink(&item->srvId_list, (ipcListLink_t)drtListLink, prev);
            break;
        }
        else if (tempService == service)
        {
            ipcError(IPCLOG_SESSION,
                ("ipcDrtAddServcie: duplicated service %d\n", service));
            ipcFree(drtListLink);
            return;
        }
    }
    if (curr == NULL)
    {
        ipcListAddTail(&item->srvId_list, (ipcListLink_t)drtListLink);
    }
    ipcDrtChangeAddItem(changes,
            ipcDrtChangeItemNew(item->node_id, service, IPCONLINE));
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDrtRemoveService
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Remove a service ID from item.
*   
*   PARAMETER(S):
*       item: pointer to the DRT item
*       service: the service ID to be removed
*       changes: [out] the DRT change set
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Apr2006  Motorola Inc.         Initial Creation
* 21Nov2006  Motorola Inc.         Use ipcListCompareDrtSrvId function to compare DRT items
* 17Jan2007  Motorola Inc.         Cached DRT changes 
*
*****************************************************************************************/
void ipcDrtRemoveService(ipcDRTItem_t* item, ipcServiceId_t service, 
                         ipcDrtChangeSet_t* changes)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    link = ipcListRemove(&item->srvId_list, ipcCastPtr(service, void*), ipcListCompareDrtSrvId);
    if (link != NULL)
    {
        ipcFree(link);
        ipcDrtChangeAddItem(changes, 
                ipcDrtChangeItemNew(item->node_id, service, IPCOFFLINE));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDiscardForDsm
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   The function returns TRUE if a packet should be discarded because the
*   destination is in deep sleep mode.
*   
*   PARAMETER(S):
*       service: the destination service ID
*       drt_item: the corresponding item in the DRT
*       flags: sendto flags parameter (can override default DSM action)
*       
*   RETURN: FALSE - the destination node is already awake or we should wake it up
*                   for this message
*           TRUE - the destination node is in deep sleep mode and this message
*                  should be discarded
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Dec2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcDiscardForDsm(ipcServiceId_t service, ipcDRTItem_t* drtItem, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL discardMessage = FALSE;
    UINT16 dsmAction;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcGetNodeDsmStatus(drtItem->ipcaddr) == IPC_DSM_ASLEEP)
    {
        /* The node is asleep so first check for the override flag */
        if (flags & IPC_DSM_DISCARD)
        {
            /* The sender wants to discard the message when destination is asleep */
            discardMessage = TRUE;
        }
        else if (!(flags & IPC_DSM_WAKE_UP))
        {
            /* No override flag was provided so check the default
             * behavior for this service in the DRT */
             if (ipcServiceInDRTItem(drtItem, service, &dsmAction) && 
                 (dsmAction == IPC_DSM_DISCARD))
             {
                 /* The default behavior is to discard the message */
                 discardMessage = TRUE;
             }
        }
    }
    
    return discardMessage;
}

