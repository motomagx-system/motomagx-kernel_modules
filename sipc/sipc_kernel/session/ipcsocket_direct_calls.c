/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcsocket_direct_calls.c
*   FUNCTION NAME(S): ipcaccept
*                     ipcbind
*                     ipcchan
*                     ipcclosesocket
*                     ipcconnect
*                     ipcgetmemtype
*                     ipcgetpeername
*                     ipcgetsockname
*                     ipcgetstatus
*                     ipcgetversion
*                     ipclisten
*                     ipclocateservice
*                     ipcnotify
*                     ipcpsnotify
*                     ipcrecv
*                     ipcrecvfrom
*                     ipcsend
*                     ipcsendto
*                     ipcsetsockqlen
*                     ipcsetwatermark
*                     ipcshutdown
*                     ipcsocket
*                     ipczalloc
*                     ipczcalloc
*                     ipczfree
*                     ipczrecv
*                     ipczrecvfrom
*                     ipczsend
*                     ipczsendto
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 11Aug2005  Motorola Inc.         Add preprocessors for other modules can access SmartIPC socket APIs in linux kernel space.
* 01Mar2006  Motorola Inc.         Remove deprecated definitions 
* 20Jun2006  Motorola Inc.         Add the permission control for trusted vs. non-trusted Apps
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source        
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 17Feb2007  Motorola Inc.         Update ipczalloc for common allocator
* 22Jun2007  Motorola Inc.         Align size to even value when copying
* 19Jul2007  Motorola Inc.         Added ipcTrace
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
#include "ipc_defs.h"
#include "ipc_platform_defs.h"
#include "ipc_packet.h"
#include "ipc_sock_impl.h"
#include "ipc_last_error_hash.h"
#include "ipclog.h"

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetstatus
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to retrieve the status of IPC stack.
*   PARAMETER(S):
*       void
*   RETURN: status of IPC stack.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Init variable ret 
*
*****************************************************************************************/
int ipcgetstatus()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    ret = ipcgetstatus_imp(&lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetversion
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to retrieve version of IPC stack.
*   PARAMETER(S):
*       version: This is the address to store IPC stack's status.
*   RETURN: 0 - Successfully retrieve IPC stack's status.
*          -1 - Failed to get status.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Init variable ret 
*
*****************************************************************************************/
int ipcgetversion(unsigned long* version)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    ret = ipcgetversion_imp(version, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsocket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Create a socket. The socket won't have a service ID until it's bound.
*   
*   PARAMETER(S):
*       af: address family of the socket
*       type: type of the socket
*       protocol: protocol of the socket
*       sock: pointer to socket handle (out param)
*       
*   RETURN: If successfully created, return 0
*           Other return error value IPCEAFNOSUPPORT, IPCEPROTOTYPE, or IPCENOBUFS
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Aug2005  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcsocket(int af,
              int type,
              int protocol)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int sock;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SOCKET_ID);
    sock = (int)ipcsocket_imp(af, type, protocol, &lasterror);
    
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_SOCKET_ID);
    return sock;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcbind
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Associate a local ipc address to a socket.
*   
*   PARAMETER(S):
*       sock: the socket handle
*       name: the ipc address need to be associated to the socket
*       namelen: the length of the name
*       
*   RETURN: If success, return 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Init variable ret and delete parameter checking 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcbind(int sock,
            const struct IPCSOCK_ADDR* name,
            int namelen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_BIND_ID);
    ret = ipcbind_imp((ipcSocket_t *)sock, name, namelen, &lasterror);

    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_BIND_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetsockname
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Retrieves the local name for a socket.
*   
*   PARAMETER(S):
*       sock: the socket handle
*       name: the pointer to return the socket's name
*       namelen: the pointer to return the length of name
*       
*   RETURN: If parameters are all valid, 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Delete parameter checking
* 29Dec2006  Motorola Inc.         Return error right after func failed 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcgetsockname(int sock,
                   struct IPCSOCK_ADDR* name,
                   int * namelen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_GETSOCKETNAME_ID);
    ret = ipcgetsockname_imp((ipcSocket_t *)sock, name, namelen, &lasterror);
    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    else
    {
        *namelen = sizeof(*name);
    }
    ipcTrace(SIPCEXITLOG, IPC_GETSOCKETNAME_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetpeername
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Retrieves the peer name for a socket.
*   
*   PARAMETER(S):
*       sock: the socket handle
*       name: the pointer to return the socket's name
*       namelen: the pointer to return the length of name
*       
*   RETURN: If parameters are all valid, 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Nov2005  Motorola Inc.         Initial Creation 
* 06Apr2007  Motorola Inc.         Return error right after func failed       
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*****************************************************************************************/
int ipcgetpeername(int sock,
                   struct IPCSOCK_ADDR* name,
                   int * namelen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_GETPEERNAME_ID);
    ret = ipcgetpeername_imp((ipcSocket_t *)sock, name, namelen, &lasterror);
    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    else
    {
        *namelen = sizeof(*name);
    }
    ipcTrace(SIPCEXITLOG, IPC_GETPEERNAME_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcshutdown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Disable sends or receives on a socket.
*   
*   PARAMETER(S):
*       sock: the socket handle
*       how: flag that describes what types of operation would not be allowed
*       
*   RETURN: If success, return 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 26Jul2005  Motorola Inc.         Added flag to indicate initialization 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcshutdown(int sock,
                int how)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SHUTDOWN_ID);
    ret = ipcshutdown_imp((ipcSocket_t *)sock, how, &lasterror);

    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_SHUTDOWN_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsetsockqlen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Set the max number of messages that can be queued in an existing socket.
*   
*   PARAMETER(S):
*       sock: the socket handle
*       queueMaxlen: the new capacity of the socket receive queue.
*       
*   RETURN: 0 on success, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24May2005  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcsetsockqlen(int sock,
                   int queueMaxLen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SETSOCKQLEN_ID);
    ret = ipcsetsockqlen_imp((ipcSocket_t *)sock, queueMaxLen, &lasterror);

    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_SETSOCKQLEN_ID);
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcclosesocket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Close an existing socket.
*   
*   PARAMETER(S):
*       sock: the socket to be closed
*       
*   RETURN: 0 on success, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Init variable ret 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcclosesocket(int sock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_CLOSESOCKET_ID);
    ret = ipcclosesocket_imp((ipcSocket_t *)sock, &lasterror);
    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_CLOSESOCKET_ID);
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcchan
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcchan(int sock, int qos, int flag)
{
    int ret = IPC_ERROR;
    int lasterror;

    ipcTrace(SIPCENTRYLOG, IPC_CHAN_ID);
    ret = ipcchan_imp((ipcSocket_t *)sock, qos, flag, &lasterror);
    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_CHAN_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipclocateservice
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get one or some services at the IPC network, each service is represented in
*   struct IPCSERVICE_LOCATION, which is just a pair of <nodeId, serviceId>, and
*   the returned services are filled in ipc_servicetable.
*   if ipc_node is equal to IPC_NODE_ANY, it queries services for all nodes
*   if ipc_service is equal to IPC_SERVICE_ANY, it gets all the PSIDs in the
*   node(or nodes, if ipc_node is IPC_NODE_ANY).
*   
*   PARAMETER(S):
*       ipc_node: specific node id or IPC_NODE_ANY
*       ipc_service: specific PSID or IPC_SERVICE_ANY
*       ipc_servicetable: this is an array of node id and service id pairs
*       ipc_sizeof_servicetable: pointer to return number of service pairs
*       ipc_maxsizeofservicetable: max size of entries in the table
*       
*   RETURN: If OK returns 0, else IPC_ERROR
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Delete parameter checking 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipclocateservice(unsigned long ipc_node,
                     unsigned short ipc_service,
                     struct IPCSERVICE_LOCATION* ipc_servicetable,
                     unsigned long* ipc_sizeofservicetable,
                     unsigned long ipc_maxsizeofservicetable)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_LOCATESERVICE_ID);
    ret = ipclocateservice_imp(ipc_node,
            ipc_service,
            ipc_servicetable,
            ipc_sizeofservicetable,
            ipc_maxsizeofservicetable,
            &lasterror);

    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_LOCATESERVICE_ID);
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczsendto
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send data to a specific IPC address.
*   
*   PARAMETER(S):
*       sock: socket handle 
*       buf: the buffer in shared memory to be sent out
*       leng: the length of data packet
*       flags: indicator specifying the way in which the call is made
*       to: destination IPC address
*       tolen: size of address in the to parameter
*       
*   RETURN: If no error occurs, returns the total number of bytes sent, if an
*           error occurs, IPC_ERROR is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Init variable ret
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipczsendto(int sock,
               const char* ptr,
               int leng,
               int flags,
               const struct IPCSOCK_ADDR* to,
               int tolen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int ret = IPC_ERROR;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZSENDTO_ID);
    if ((ptr == NULL) || (to == NULL) || (tolen < (int)sizeof(*to)))
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_ZSENDTO_ID);
        return IPC_ERROR;
    }
    
    ret = ipczsendto_imp((ipcSocket_t *)sock, 
            ptr, 
            leng,
            flags,
            to,
            tolen,
            &lasterror);

    if (ret < 0)
    {
        ipcSetLastError(lasterror);
    }

    ipcTrace(SIPCEXITLOG, IPC_ZSENDTO_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczrecvfrom
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives a data packet using zero copy.
*   
*   PARAMETER(S):
*       sock: Descriptor identifying a bound socket
*       zbuf: buffer pointer for the incoming data
*       len: the upperbound size of the buffer, ignored if len equals to 0
*       flags: indicator specifying the way in which the call is made
*       from: pointer to return the source address of the data packet
*       fromlen: pointer to return the size of from buffer
*   RETURN: Number of bytes received.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Delete parameter checking and set error code
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipczrecvfrom(int sock,
                 char ** zbuf,
                 int flags,
                 struct IPCSOCK_ADDR* from,
                 int * fromlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int len;
    int lasterror;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZRECVFROM_ID);
    if (zbuf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_ZRECVFROM_ID);
        return IPC_ERROR;
    }
    
    len = ipczrecvfrom_imp((ipcSocket_t *)sock, 
            zbuf, 
            flags,
            from,
            fromlen,
            &lasterror);

    if (len < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_ZRECVFROM_ID);
    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsendto
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation
* 31Dec2005  Motorola Inc.         Error check
* 18Jan2006  Motorola Inc.         change invalid leng error code to IPCEMSGSIZE
* 08Dec2006  Motorola Inc.         Allocate PROCSHM when failed to allocate PHYSSHM
* 15Jan2007  Motorola Inc.         Change ipcsendto() implement
* 22Jun2007  Motorola Inc.         Align size to even value when copying
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcsendto(int sock,
              const char* buf,
              int leng,
              int flags,
              const struct IPCSOCK_ADDR* to,
              int tolen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* ptr = NULL;
    int ret = IPC_ERROR;
#ifdef HAVE_PHYSICAL_SHM
    int flag = IPC_PHYSSHM;
#else
    int flag = IPC_PROCSHM;
#endif
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SENDTO_ID);
    if (to == NULL)
    {
        ipcSetLastError(IPCEADDRNOTVAL);
        ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
        return IPC_ERROR;
    }
    if (tolen < (int)sizeof(struct IPCSOCK_ADDR))
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
        return IPC_ERROR;
    }

    if (leng < 0)
    {
        ipcSetLastError(IPCEMSGSIZE);
        ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
        return IPC_ERROR;
    }

    if (flags & ~(IPCNONBLOCK | IPCPRIO | IPCROSPRIO | IPCPAYLOADCRC))
    {
        /* An unknown flag was set */
        ipcSetLastError(IPCEINVAL);
        ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
        return IPC_ERROR;
    }
    if (buf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
        return IPC_ERROR;
    }
    ptr = ipczalloc(leng, flag, 0);    
    if (NULL != ptr)
    {
        memcpy(ptr, buf, (leng + 1) & ~1);
        ret = ipczsendto(sock, ptr, leng, flags, to, tolen);
        if (ret < 0)
        {
            /* packet was not sent so we must free it */
            ipczfree(ptr);
        }
    }
    else if (flag == IPC_PHYSSHM)
    {
        ptr = ipczalloc(leng, IPC_PROCSHM, 0);
        if (NULL != ptr)
        {
            memcpy(ptr, buf, (leng + 1) & ~1);
            ret = ipczsendto(sock, ptr, leng, flags, to, tolen);
            if (ret < 0)
            {
                /* packet was not sent so we must free it */
                ipczfree(ptr);
            }   
        }
    }
    else
    { 
        switch(ipcGetLastErrorImp())
        {
        case IPCENOTINIT:
            ipcSetLastError(IPCENOTINIT);
            break;
        case IPCEINVAL:
            ipcSetLastError(IPCEINVAL);
            break;
        default:
            ipcSetLastError(IPCENOBUFS);
        }
    }
    ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
    return ret;
}



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcrecvfrom
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives ipc data.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation
* 22Jun2007  Motorola Inc.         Align size to even value when copying
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcrecvfrom(int sock,
                char *buf,
                int len,
                int flags,
                struct IPCSOCK_ADDR* from,
                int * fromlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char *zbuf = NULL;
    int ret;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_RECVFROM_ID);
    if (len > 0)
    {
        ret = ipczrecvfrom(sock, &zbuf, flags, from, fromlen);

        if (ret >= 0)
        {
            if (ret > len)
            {
                /*  Buffer is too small */
                ipcSetLastError(IPCEMSGSIZE);
                ret = IPC_ERROR;
            }
            else
            {
                if ((ret & 1) && (len > ret))
                {
                    len = ret + 1;
                }
                else
                {
                    len = ret;
                }
            }

            memcpy(buf, zbuf, len);
        }

        if ((zbuf != NULL) && !(flags & IPCROSCHKMSG))
        {
            ipczfree(zbuf);
        }
    }
    else
    {
        ipcSetLastError(IPCEINVAL);
        ret = IPC_ERROR;
    }

    ipcTrace(SIPCEXITLOG, IPC_RECVFROM_ID);
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczalloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 22Aug2005  Motorola Inc.         ipcAllocImp changed to ipcAlloc
* 25Aug2005  Motorola Inc.         Set error code
* 17Jan2006  Motorola Inc.         The message length which could be sent is not same in  ipcsendto()/ipcsend() and ipczsendto()/ipczsend()
* 16Feb2005  Motorola Inc.         Bug fix. ipczalloc() will never return if it tries to allocate a block of physical shared memory with size more than max buffer size.
* 23Feb2007  Motorola Inc.         Update for common allocator
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
void *ipczalloc(long buf_leng,
                int flag,
                int wait_time)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    void *data;
    int memType = flag & MEMTYPE_FLAG_MASK;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZALLOC_ID);
    if (buf_leng < 0)
    {
        ipcSetLastError(IPCEINVAL);
        ipcTrace(SIPCEXITLOG, IPC_ZALLOC_ID);
        return NULL;
    }
    else if (0 == buf_leng)
    {
        buf_leng = 1;
    }
    	
    buf_leng += IPC_PACKET_DATA_OFFSET;

    if (memType == IPC_PHYSSHM)
    {
#ifdef HAVE_PHYSICAL_SHM
        if (buf_leng > ipcGetMaxPSMBufferSize())
#endif
        {
            /* User tried to allocate physical shared memory but there is none */
            ipcSetLastError(IPCENOSHMEM);
            ipcTrace(SIPCEXITLOG, IPC_ZALLOC_ID);
            return NULL;
        }
    }
    else if (memType == 0)
    {
        /* Default to virtual shared memory (instead of unshared) */
        flag |= IPC_PROCSHM;
    }
    
    data = ipcAlloc(buf_leng, flag);

    if (NULL == data)
    {
        ipcSetLastError(IPCENOSHMEM);
        ipcTrace(SIPCEXITLOG, IPC_ZALLOC_ID);
        return NULL;
    }
    memset(data, BUFHEAD_MARKER, IPC_PACKET_DATA_OFFSET);

    ipcTrace(SIPCEXITLOG, IPC_ZALLOC_ID);
    return ipcGetMsgData(data);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczfree
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to free a block of shared memory.
*   PARAMETER(S):
*       ipc_buf_ptr: This is the address of memory.
*   RETURN: 0 - Sucess.
*          -1 - Failed.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Aug2005  Motorola Inc.         Initial Creation
* 25Aug2005  Motorola Inc.         Check return value and set error code
* 18May2006  Motorola Inc.         check the invalid pointer 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipczfree(void *ipc_buf_ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZFREE_ID);
    if (NULL == ipc_buf_ptr)
    {
        ipcSetLastError(IPCEBADBUF);
        ipcTrace(SIPCEXITLOG, IPC_ZFREE_ID);
        return IPC_ERROR;
    }
    rc = ipcFree(ipcDataToPacket(ipc_buf_ptr));
    if (rc != IPC_OK)
    {
        ipcSetLastError(rc);
        rc = IPC_ERROR;
    }
    ipcTrace(SIPCEXITLOG, IPC_ZFREE_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipclisten
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipclisten(int sock, int backlog)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_LISTEN_ID);
    ret = ipclisten_imp((ipcSocket_t *)sock, backlog, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_LISTEN_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcaccept
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcaccept(int sock,
              struct IPCSOCK_ADDR *addr,
              int *addrlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int newSock;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ACCEPT_ID);
    newSock = (int)ipcaccept_imp((ipcSocket_t *)sock, addr, addrlen, &lasterror);
    if (newSock == IPC_ERROR)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_ACCEPT_ID);
    return newSock;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcconnect
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcconnect(int sock, 
               struct IPCSOCK_ADDR *addr, 
               int addrlen)  
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_CONNECT_ID);
    ret = ipcconnect_imp((ipcSocket_t *)sock, addr, addrlen, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
        ret = IPC_ERROR;
    }
    ipcTrace(SIPCEXITLOG, IPC_CONNECT_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczsend
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send data to a specific IPC address.
*   
*   PARAMETER(S):
*       sock: socket handle 
*       buf: the buffer in shared memory to be sent out
*       len: the length of data packet
*       flags: indicator specifying the way in which the call is made
*       
*   RETURN: If no error occurs, returns the total number of bytes sent, if an
*           error occurs, IPC_ERROR is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source           
* 04Jun2007  Motorola Inc.         Return error if buf is NULL
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipczsend(int sock,
             const char * buf, 
             int len,
             int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZSEND_ID);
    if (!buf)
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_ZSEND_ID);
        return IPC_ERROR;
    }

    ret = ipczsend_imp((ipcSocket_t *)sock, buf, len, flags, &lasterror);
    if(ret<0) 
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_ZSEND_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczrecv
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives a data packet using zero copy.
*   
*   PARAMETER(S):
*       sock: Descriptor identifying a bound socket
*       zbuf: buffer pointer for the incoming data
*       flags: indicator specifying the way in which the call is made
*   RETURN: Number of bytes received.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipczrecv(int sock,
             char ** zbuf,
             int flags)

{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int len = IPC_ERROR;
    
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZRECV_ID);
    if (zbuf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_ZRECV_ID);
        return len;
    }
    
    len = ipczrecv_imp((ipcSocket_t *)sock, zbuf, flags, &lasterror);
    if (len < 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_ZRECV_ID);
    return len;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsend
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination on a connected socket.  
*   Both CL and  CO sockets may be used as long as connect is called first.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jan2006  Motorola Inc.         change invalid leng error code to IPCEMSGSIZE
* 22Jun2007  Motorola Inc.         Align size to even value when copying
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcsend(int sock,
            const char * buf, 
            int len,
            int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* ptr = NULL;
    int ret = IPC_ERROR;
    int allocFlags = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SEND_ID);
#ifdef HAVE_PHYSICAL_SHM
    allocFlags |= IPC_PHYSSHM;
#else
    allocFlags |= IPC_PROCSHM;
#endif

    if(buf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
        return IPC_ERROR;
    }

    if (len < 0)
    {
        ipcSetLastError(IPCEMSGSIZE);
        ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
        return IPC_ERROR;
    }

    if(flags & ~(IPCNONBLOCK | IPCRELIABLE | IPCPRIO | IPCROSPRIO | IPCPAYLOADCRC))
    {
        ipcSetLastError(IPCEINVAL);
        ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
        return IPC_ERROR;
    }
    
    ptr = (char*)ipczalloc(len, allocFlags, 0);
    if(ptr != NULL) 
    {
        /* Maybe replaced by the strncpy */
        memcpy(ptr, buf, (len + 1) & ~1);
        ret = ipczsend(sock, ptr, len, flags);
        if(ret < 0)
        {
            /* packet was not sent so we must free it */
            ipczfree(ptr);
        }
    }
    else
    {
        if (ipcGetLastErrorImp() != IPCENOTINIT)
        {
            ipcSetLastError(IPCENOBUFS);
        }
    }
    ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcrecv
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives ipc data.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Nov2006  Mororola Inc.         add flag IPCROSCHKMSG
* 26Apr2007  Motorola Inc.         Donot free buffer for flag IPCROSCHKMSG
* 22Jun2007  Motorola Inc.         Align size to even value when copying
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcrecv(int sock,
  			char * buf,
 			int len,
 			int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char *zbuf = NULL;
    int ret = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_RECV_ID);
    if (buf == NULL)
    {
        ipcSetLastError(IPCEFAULT);
        ipcTrace(SIPCEXITLOG, IPC_RECV_ID);
        return IPC_ERROR;
    }
    if(len <= 0 || 
        (flags & ~(IPCNONBLOCK | IPCRELIABLE | IPCPRIO | IPCROSPRIO | IPCROSCHKMSG)))
    {
        ipcSetLastError(IPCEINVAL);
        ipcTrace(SIPCEXITLOG, IPC_RECV_ID);
        return IPC_ERROR;
    }
    ret = ipczrecv(sock, &zbuf, flags);
    if (ret >= 0)
    {
        if (ret > len)
        {
            /*  Buffer is too small */
            ipcSetLastError(IPCEMSGSIZE);
            ret = IPC_ERROR;
        }
        else
        {
            if ((ret & 1) && (len > ret))
            {
                len = ret + 1;
            }
            else
            {
                len = ret;
            }
        }

        memcpy(buf, zbuf, len);
    }

    if ((zbuf != NULL) && !(flags & IPCROSCHKMSG))
    {
        ipczfree(zbuf);
    }
    ipcTrace(SIPCEXITLOG, IPC_RECV_ID);
    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcnotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 07Oct2005  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcnotify(int sock, const struct IPCSOCK_ADDR* name, int namelen,
              const char * msg, int msglen, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_NOTIFY_ID);
    ret = ipcnotify_imp((ipcSocket_t *)sock, name, namelen, msg, msglen, flags, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_NOTIFY_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcpsnotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcpsnotify(void *q, int qflag, const struct IPCSOCK_ADDR* name, int namelen,
                const char * msg, int msglen, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_PSNOTIFY_ID);
    ret = ipcpsnotify_imp(q, qflag, name, namelen, msg, msglen, flags, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_PSNOTIFY_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcgetmemtype
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function gets the type of memory that may be used to reach a node.
*   
*   PARAMETER(S):
*       ipc_address: address of desired service
*       ipc_memtype: type of shared memory available for zero-copy purpose.
*       
*   RETURN: if an error occurs, an error of IPC_ERROR will be returned, and a
*           specified error code can be retrieved by calling ipcgetlasterror.
*   
*           IPCEADDRNOTVAL - the specified ipc address is not valid
*           IPCENOTINIT - ipcinit was not called before making this call
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Oct2005  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcgetmemtype(const struct IPCSOCK_ADDR* ipc_address,
                  unsigned long *ipc_memtype)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_GETMEMTYPE_ID);
    ret = ipcgetmemtype_imp(ipc_address, ipc_memtype, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_GETMEMTYPE_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsetwatermark
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 04Nov2005  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int ipcsetwatermark(int sock,
                    unsigned long ipc_lowwmark,
                    unsigned long ipc_hiwmark)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int lasterror = 0;
    int ret;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SETWATERMARK_ID);
    ret = ipcsetwatermark_imp((ipcSocket_t*)sock, 
        (unsigned char)ipc_lowwmark, 
        (unsigned char)ipc_hiwmark, &lasterror);
    if (lasterror != 0)
    {
        ipcSetLastError(lasterror);
    }
    ipcTrace(SIPCEXITLOG, IPC_SETWATERMARK_ID);
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipczcalloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocate nelem blocks of shared memory, each has the length sz.
*   The allocated memory is set to 0.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation
* 27Jul2005  Motorola Inc.         Check for minus parameters 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
void *ipczcalloc(long nelem,
                 long sz,
                 int flag,
                 int wait_time)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    void *pointer;
    long length;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ZCALLOC_ID);
    if ((nelem < 0l) || (sz < 0l) || (wait_time < 0l))
    {
        ipcSetLastError(IPCEINVAL);
        ipcTrace(SIPCEXITLOG, IPC_ZCALLOC_ID);
        return NULL;
    }
    length = nelem * sz;

    pointer = ipczalloc(length, flag, wait_time);
    if ((pointer != NULL) && (length > 0))
    {
        memset(pointer, 0, length);
    }
    ipcTrace(SIPCEXITLOG, IPC_ZCALLOC_ID);
    return pointer;
}




#if defined(__KERNEL__)
EXPORT_SYMBOL(ipcgetstatus);
EXPORT_SYMBOL(ipcgetversion);
EXPORT_SYMBOL(ipcsocket);
EXPORT_SYMBOL(ipcbind);
EXPORT_SYMBOL(ipcgetsockname);
EXPORT_SYMBOL(ipcshutdown);
EXPORT_SYMBOL(ipcsetsockqlen);
EXPORT_SYMBOL(ipcclosesocket);
EXPORT_SYMBOL(ipcchan);
EXPORT_SYMBOL(ipclocateservice);
EXPORT_SYMBOL(ipczsendto);
EXPORT_SYMBOL(ipczrecvfrom);
EXPORT_SYMBOL(ipcsend);
EXPORT_SYMBOL(ipcrecv);
EXPORT_SYMBOL(ipcsendto);
EXPORT_SYMBOL(ipcrecvfrom);
EXPORT_SYMBOL(ipczalloc);
EXPORT_SYMBOL(ipczfree);
EXPORT_SYMBOL(ipcgetmemtype);
EXPORT_SYMBOL(ipclisten);
EXPORT_SYMBOL(ipcaccept);
EXPORT_SYMBOL(ipcconnect);
EXPORT_SYMBOL(ipczsend);
EXPORT_SYMBOL(ipczrecv);
EXPORT_SYMBOL(ipcnotify);
EXPORT_SYMBOL(ipcpsnotify);
EXPORT_SYMBOL(ipcsetwatermark);
EXPORT_SYMBOL(ipczcalloc);
#endif
