/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2000, 2005 - 2006 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_config.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines IPC run time parameters which maybe different for different
*   devices.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 20Apr2000  Motorola Inc.         Initial Creation 
* 14May2005  Motorola Inc.         LPK: Ros config.
* 26Dec2005  Motorola Inc.         Update to unix format and add macro IPC_DEV_BUFFER_SIZE  IPC_MAX_QUEUE_LEN IPC_MAX_BYTES_PENDING for ROS_ON
* 15Mar2006  Motorola Inc.         Allow larger notification messages (required for SPL notification)
* 30Jun2006  Motorola Inc.         Added macro IPC_SHUTDOWN_TIMEOUT
* 17Jul2006  Motorola Inc.         Moved macro IPC_NOTIFICATION_MAX_SIZE to ipcsocket.h 
* 16Nov2006  Motorola Inc.         Optimize SIPC timer       
* 11Dec2006  Motorola Inc.         Taos: self check fail: f8000 RB.2.00.00 during power up   							  
*****************************************************************************************/

#ifndef IPC_CONFIG_H
#define IPC_CONFIG_H

/*-------------------------------------- CONSTANTS -------------------------------------*/

/* session layer parameters */

/* This is the time to sleep before retrying a blocking write 
 * (when the device layer has too much data pending) */
#define IPC_SENDTO_RETRY_TIME           100

/* Client session in NETWORK_READY, hold a period to collect comp's config reqs */
#define TIMEOUT_CLIENT_CONFIG_REQ       10

/* Client session in ROUTER_READY, resend auth req after failure reply */
#define TIMEOUT_CLIENT_AUTH_REQ         2000

/* Server session in NETWORK_READY, hold a period to collect config reqs
 * The reqs come from component or client sessions */
#define TIMEOUT_SERVER_CONFIG_REQ       10

/* Server session in NETWORK_READY, send UpdateInd periodically */
#define TIMEOUT_SERVER_DRT_BROADCAST    300000

/* Wait for reply to multihop channel request */
#define TIMEOUT_MULTIHOP_CHANNEL_REQ    1000

#define HASH_SIZE_FOR_NODE_ID_TABLE     11
#define HASH_SIZE_FOR_SRVID_TABLE       11

/* max number of packets can be pending in a socket */
#define IPC_MAX_QUEUE_LEN               500000

/* min queue length used in set queue length */
#define IPC_MIN_QUEUE_LEN               1

/* max wait time for ipcshutdown */
#define IPC_SHUTDOWN_TIMEOUT            5000


/* router layer parameters */

#define TIMEOUT_ROUTER_ADDR_REQ         1000
#define TIMEOUT_ROUTER_ADDR_REPLY       1000
#define TIMEOUT_ROUTER_LINK_UP          10000

/* Router layer timers:
 *
 * IPC_LINK_REQ_TIMEOUT: the interval used to send LINK_REQUEST message.
 * IPC_LINK_PENDING_TIMEOUT: timeout to wait LINK_COMP messager in PENDING state
 * IPC_LINK_COMP_TIMEOUT: the interval to send LINK_COMP message.
 */
#define IPC_LINK_REQ_TIMEOUT                   1000 /* milli-seconds */
#define IPC_LINK_PENDING_TIMEOUT               5000
#define IPC_LINK_COMP_TIMEOUT                  60000 /* every minute */

#define IPC_CONN_TIMEOUT_VAL                   50

/* device layer parameters */
#define IPC_DEV_BUFFER_SIZE       0xF000

#define IPC_MAX_BYTES_PENDING           500000
#define IPC_DEFAULT_FRAME_RATE          10
#define IPC_FRAME_ACK_TIMEOUT           1000
#define IPC_MAX_RETRANSMISSIONS         10

#define IPC_CONTROL_CHANNEL_BANDWIDTH   1000

/* Eventually all IPC Node Types should be defined here */

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


#endif /* IPC_CONFIG_H */
