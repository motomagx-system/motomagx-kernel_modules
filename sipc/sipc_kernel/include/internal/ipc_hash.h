/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2005 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_hash.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file contains data structure, function prototypes for IPC hash table.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial creation
* 13Jan2005  Motorola Inc.         Changes for new IPC list impl
*****************************************************************************************/
#ifndef IPC_HASH_H
#define IPC_HASH_H

#include "ipc_platform_defs.h"
#include "ipc_list.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

typedef struct tag_ipcHashItem_t
{
    struct tag_ipcHashItem_t *next;
    void *key;
    void *object;
} ipcHashItem_t, *ipcHashLink_t;

typedef struct tag_ipcHashTable_t
{
//public:
    unsigned int nItems;
    ipcHashLink_t *chunks;
    unsigned int (*hash_func)(void *key);
//private:
    unsigned int nChunks;
    unsigned int currentChunk;
    ipcHashLink_t currentLink;
}ipcHashTable_t;

typedef unsigned int (*ipcHashFunc_t)(void*);

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

BOOL ipcHashInit(ipcHashTable_t* table,
                 unsigned int nChunks,
                 ipcHashFunc_t hash_func);
void ipcHashDestroy(ipcHashTable_t* table);
BOOL ipcHashPut(ipcHashTable_t* table, void* key, void* object);
void* ipcHashFind(ipcHashTable_t* table, void* key);
void* ipcHashRemove(ipcHashTable_t* table, void* key);
void ipcHashResetIterator(ipcHashTable_t* table);
void* ipcHashGetNext(ipcHashTable_t* table);
unsigned int ipcHashGetSize(ipcHashTable_t* table);
void ipcHashRemoveAll(ipcHashTable_t* table);

unsigned int ipcHashDefaultHashFunc(void *key);
unsigned int ipcHashPointerHashFunc(void *key);
BOOL ipcListHashKeyCompare(ipcListLink_t link, void *key);

/*--------------------------------------- MACROS ---------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif
