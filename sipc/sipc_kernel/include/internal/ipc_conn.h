/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_conn.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines the sliding-window mechanism of IPC stack, which will be
*   used in both router layer and session sockets, providing reliable data
*   communication between endpoints (also mentioned as a connection), flow 
*   control, as well as interface to router and session layer sockets.
*
*   An endpoint of a connection is defined as a struct ipcConn_t, it maintains
*   the information of a connection, a series of function are also defined to
*   operate on the struct.
*
*   Each packet is assigned a sequence number, so does every ack packets,
*   while sending a packet out via a connection, it will be chained in a list, 
*   in case our peer doesn't get the packet correctly, if we don't get an 
*   acknowledgment of the packet from the peer in some certain time, we will
*   think the packet gets lost, so we will resend the packet. If we still fail
*   after resending the packet for some times, we'll think the connection is
*   broken, and then shut it down. And the packet will be removed from the list
*   if the ack arrives as expected.
*
*   In the receiver side, if it got a packet, it will send an acknowledgment 
*   back, and put the packet to a temporary list if the sequence number of the
*   packet is not the one it's expected to be; otherwise the packet will be
*   handled by router layer or session layer socket.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.        Initial Creation

*****************************************************************************************/

#ifndef IPC_CONN_H
#define IPC_CONN_H

#include "ipc_list.h"
#include "ipc_packet.h"
#include "ipc_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/
#define IPC_MAX_WINDOW_SIZE     12
#define IPC_MAX_WAIT_TIME       5000
#define IPC_MAX_COUNTDOWN       20
#define IPC_MAX_RETRY_TIMES     5


/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
typedef int (*ipcConnDataArrival_t)(ipcConn_t* conn, ipcPacket_t* pack);
typedef int (*ipcConnTxWindowFull_t)(ipcConn_t* conn, BOOL mayBlock);
typedef void (*ipcConnTxWindowAvailable_t)(ipcConn_t* conn);
typedef void (*ipcConnMaxRetries_t)(ipcConn_t* conn, ipcPacket_t* pack);
typedef int (*ipcConnSentListEmpty_t)(ipcConn_t* conn);
typedef int (*ipcConnDataDeliver_t)(ipcConn_t* conn, ipcPacket_t* pack);
typedef int (*ipcConnLock_t)(ipcConn_t* conn);
typedef int (*ipcConnUnlock_t)(ipcConn_t* conn);

struct tag_ipcConnOperations_t
{
    /* event triggers */
    ipcConnDataArrival_t onDataArrival;
    ipcConnTxWindowFull_t  onTxWindowFull;
    ipcConnTxWindowAvailable_t onTxWindowAvailable;
    ipcConnMaxRetries_t  onMaxRetries;
    ipcConnSentListEmpty_t onSentListEmpty;
    
    /* operations */
    ipcConnDataDeliver_t deliverData;
    ipcConnLock_t        lock;
    ipcConnUnlock_t      unlock;

};

/* Define a Conn structure for acked-transmission/flow-control */
struct tag_ipcConn_t
{
    struct tag_ipcConn_t* next;

    /* NOTE:
     *    packets in sentPackets list ared chained using next2 field
     */
    ipcList_t sentPackets;        /* ipcList_t<ipcPacket_t> */
    ipcList_t outOfOrderPackets;  /* ipcList_t<ipcPacket_t> */

    /* Rx window */
    UINT8     nextRxSeqNum;       /* The expected sequence number */
    UINT8     rxWindowSize;       /* Receive window size */

    /* Tx window */
    UINT8     nextTxSeqNum;
    UINT8     maxTxSeqNum;

    ipcConnOperations_t *ops;
    /* private data */
    void *    privateData;
};

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/* functions for the Conn */
/* initialize a connection */
void ipcConnInit(ipcConn_t* conn,
                 UINT8 defaultWindowSize, 
                 void* privateData,
                 ipcConnOperations_t *ops);

/* remove all packets in outoforder list and sent list */
void ipcConnDestroy(ipcConn_t* conn,
                    BOOL clearSentPackets,
                    BOOL clearOutOfOrderPackets,
                    BOOL locked);

/* on each timer tick, decrement the countdown in each packet */
int ipcConnTimerTick(ipcConn_t* conn);


/* send out data.
 * only handle reliable data transmission, the caller should handle
 * unreliable data itself.
 */
int ipcConnDeliverData(ipcConn_t* conn, 
                       ipcPacket_t* packet,
                       BOOL isReliable,
                       BOOL mayBlock);

/* retransmit data */
int ipcConnRetransmitData(ipcConn_t* conn, ipcPacket_t* packet);

/* put a packet to the out of order list */
void ipcConnAddOutofOrderPacket(ipcConn_t* conn, ipcPacket_t* packet);

/* called while got a data packet from network */
int ipcConnGotPacket(ipcConn_t* conn, ipcPacket_t* packet);

/* called while got an ack packet from network */
int ipcConnGotAck(ipcConn_t* conn,
                  BOOL wasReliable,
                  UINT8 seqNum,
                  UINT8 maxTxSeqNum);

/* send an ack to inform the window */
int ipcSendAck(ipcConn_t* conn,
               ipcAddress_t destAddr,
               ipcServiceId_t destSrvId,
               UINT8 seqNum,
               UINT8 maxTxSeqNum);

int ipcSendFlow(ipcConn_t* conn,
                ipcAddress_t destAddr,
                ipcServiceId_t destSrvId,
                UINT8 maxTxSeqNum);

/* consume a data in the Conn */
void ipcConnConsumeData(ipcConn_t* conn, 
                        ipcAddress_t destAddr, 
                        ipcServiceId_t destSrv);

/*--------------------------------------- MACROS ---------------------------------------*/
#define ipcConnDelegate(conn) ipcListAddTail(&ipcStack_glb.connList, (ipcListLink_t)conn)
#define ipcConnUndelegate(conn) ipcListRemoveLink(&ipcStack_glb.connList, conn)

#define SEQ_LT(a, b)    ((signed char)((a) - (b)) < 0)
#define SEQ_LEQ(a, b)   ((signed char)((a) - (b)) <= 0)
#define SEQ_GT(a, b)    ((signed char)((a) - (b)) > 0)
#define SEQ_GEQ(a, b)   ((signed char)((a) - (b)) >= 0)
#define SEQ_EQ(a, b)    ((signed char)((a) - (b)) == 0)

/* Get the address of a struct, given the address of one of its fields */
#define FIELD_TO_STRUCT(s,f,p) \
    (s*)((char*)(p) - (offsetof(s, f)))

/* Get the address of the field next2 in a packet */
#define IPC_PACKET_TO_NEXT2(packet) (ipcListLink_t)(&(packet)->next2)
/* Get the address of a packet using the address of next2 in it */
#define IPC_NEXT2_TO_PACKET(n)      FIELD_TO_STRUCT(ipcPacket_t, next2, n)

#ifdef __cplusplus
}
#endif

#endif
