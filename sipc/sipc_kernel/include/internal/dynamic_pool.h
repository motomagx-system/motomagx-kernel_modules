/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2006 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: dynamic_pool.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header defines interfaces of dynamic pool functionality.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 26May2006  Motorola Inc.         Initial Creation
* 26Dec2006  Motorola Inc.         Remove last_alloc_tick
* 30Dec2006  Motorola Inc.         Moved pool size definition to header
* 20Feb2007  Motorola Inc.         Updated for common memory alloc
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*****************************************************************************************/

#ifndef DYNAMIC_POOL_H
#define DYNAMIC_POOL_H

#include "dpccshm.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
#define MIN_POOL_SIZE (1 << 18)  /* min pool size is 256K */
#define MAX_POOL_SIZE (1 << 23)  /* max pool size is 8M */
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

typedef struct ipcShmPool_t
{
    struct ipcShmPool_t* next;      /* sorted by offset */
    struct ipcShmPool_t* next2;     /* sorted by block size */
    int num_active_blocks;          /* number of allocated blocks */
    unsigned long offset;           /* offset in the address range */
    unsigned long pool_size;        /* size */

    dpcc_shm_info_t dpccshminfo[1];
} ipcShmPool_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

int ipcShmInit(unsigned long size_limit);
void ipcShmFinalize(void);
void ipcShmDump(void);

void* ipcShmAlloc(unsigned int size, unsigned int flags);
int ipcShmFree(void* ptr);

ipcShmPool_t* ipcShmCreatePool(unsigned long offset, unsigned long size);
void ipcShmDestroyPool(ipcShmPool_t* pool);
ipcShmPool_t* ipcShmFindPoolFromPtr(char* ptr);
unsigned long ipcShmPtrToOffset(char* ptr);
char* ipcShmOffsetToPtr(unsigned long offset);

#ifdef __cplusplus
}
#endif

/*--------------------------------------- MACROS ---------------------------------------*/
#define NEXT2_TO_POOL(ptr) \
    (ipcShmPool_t*)((char*)ptr - offsetof(ipcShmPool_t, next2))

#define POOL_TO_NEXT2(pool) \
    (ipcListLink_t)&(pool)->next2

#endif /* DYNAMIC_POOL_H */
