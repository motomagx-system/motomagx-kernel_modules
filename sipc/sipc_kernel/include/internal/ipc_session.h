/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_session.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains the main data structures and function prototypes related
*   with the session layer.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 20Feb2004  Motorola Inc.         IPC phase 0 initial cpp Version
* 30Jun2004  Motorola Inc.         Add handle_cpss_status_req, SSCPStatusReply, 
*                                  handle_cpss_sw_ver_req, SSCPSWVersionReply
* 11Jul2004  Motorola Inc.         IPC integreation restructure
* 26Jul2004  Motorola Inc.         Chang the usage of listMgr
* 03Dec2004  Motorola Inc.         Updated for RTXC port
* 17Feb2005  Motorola Inc.         Updated states so IPC Stack will work locally before RTSSReady
* 19Jul2005  Motorola Inc.         Enhance macro ipcIsChannelValid()
* 13Dec2005  Motorola Inc.         Add new function ipcNodeIdToIpcAddrWithoutLock
* 12Apr2006  Motorola Inc.         Added notification for service  discovery
* 30Jun2006  Motorola Inc.         Removed flag ipcSocket_t::isClosed
* 21Nov2006  Motorola Inc.         Added DSM message discard
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 10Jan2007  Motorola Inc.         added ipcDrtChangeSet_t added refCount for socket 
* 06Apr2007  Motorola Inc.         Print socket owner in /proc/sipc/ipc_stack
* 31May2007  Motorola Inc.         Added a parameter in function ipcDrtChangeSignal
* 13Jul2007  Motorola Inc.         Removed debug code
*****************************************************************************************/
#ifndef IPC_SESSION_H
#define IPC_SESSION_H

#ifdef __cplusplus  /* Allow header file to be included in a C++ file */
extern "C" {
#endif

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_common.h"
#include "ipclog.h"
#include "ipc_config.h"
#include "ipc_defs.h"
#include "ipc_messages.h"
#include "ipc_conn.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/


#define IPC_SOCKET_MAGIC    0x33DEAD44

/* socket states */
#define IPC_SOCK_NOT_CONNECTED  1
#define IPC_SOCK_LISTENING      2
#define IPC_SOCK_CONN_PENDING   3
#define IPC_SOCK_ACCEPT_PENDING 4
#define IPC_SOCK_CONNECTED      5

/*------------------------------------ ENUMERATIONS ------------------------------------*/

typedef enum tag_ipcSessionStatus_t
{
     IPC_SESSION_STATE_NULL = 0x00,              /* 00000000 */
     IPC_SESSION_STATE_LOCAL_READY = 0x01,       /* 00000001 */
     IPC_SESSION_STATE_ROUTER_READY = 0x02,      /* 00000010 */
     IPC_SESSION_STATE_NETWORK_READY = 0x04      /* 00000100 */
} ipcSessionStatus_t;

typedef enum tag_ipcLocateServiceReqType_t
{
    QUERY_NETWORK,
    QUERY_ALL_SRV_IN_ONE_NODE,
    QUERY_NODE_LIST,
    QUERY_SUPPORT_STATUS
} ipcLocateServiceReqType_t;


/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
typedef struct tag_ipcDRTListItem_t
{
    struct tag_ipcDRTListItem_t *next;
    drtServiceData_t data;
} ipcDRTListItem_t, *ipcDRTListLink_t;

typedef struct tag_ipcDRTItem_t
{
    struct tag_ipcDRTItem_t *next; /* Link to next item in list */
    ipcNodeId_t node_id; 
    ipcList_t srvId_list;
    ipcAddress_t ipcaddr;
    ipcDataType_t datatype;

} ipcDRTItem_t;

typedef struct tag_ipcDrtChangeItem_t
{
    struct tag_ipcDrtChangeItem_t* next;
    ipcNodeId_t nodeId;
    ipcServiceId_t srvId;
    int change; /* IPCONLINE or IPCOFFLINE */

} ipcDrtChangeItem_t;

typedef struct tag_ipcDrtChangeSet_t
{
    ipcList_t changes;

} ipcDrtChangeSet_t;

typedef struct tag_ipcNotification_t
{
    struct tag_ipcNotification_t *next;
    char msg[IPC_NOTIFICATION_MAX_SIZE];
    int msglen;
    int flags; /* event to be notified of + high-priority flag */
    int psFlags; /* platform-specific notification type */
    
    /* The destination of the notification can either be an IPC Socket or
     * a platform specific object stored as a void pointer. If psFlag == 0,
     * then serviceId is used, otherwise we use ps as the target */
        struct
        {
            ipcAddress_t ipcAddr;
        union 
        {
            ipcServiceId_t serviceId;
        void *ps;
        } u;

    } destination;

} ipcNotification_t;

typedef struct tag_ipcServiceNotification_t
{
    ipcNotification_t notification;
    ipcNodeId_t node;
    ipcServiceId_t service;

} ipcServiceNotification_t;

typedef struct tag_ipcSocket_t
{
    /* link to next socket for multicast service Ids
     * Yequan 08Mar2005:
     * A socket is either bound or unbound, all unbound sockets are chained in
     * a separate list, when the socket gets bound, it's removed from the list
     * and added to the local_srvid_tab.
     */
    struct tag_ipcSocket_t *next;
    ipcQueue_t packetQueue;
    char owner[16];
    UINT32  state;  /* state for socket */
    ipcServiceId_t srvId;
    
    /* status fields */
    INT16   isClosed        :1;
    INT16   isBound         :1;
    INT16   canSend         :1;
    INT16   canReceive      :1;
    INT16   isCOSocket      :1;
    INT16   connAccepted    :1; /* this is used by ipcconnect */

    /* peer states */
    INT16   peerCanSend     :1;
    INT16   peerCanReceive  :1;
    
    /* discard messages to this socket if node is in DSM? */
    INT16   dsmDiscard      :1;

    /* destination address */
    ipcNodeId_t destNode;
    ipcServiceId_t destSrv;

    UINT32 magic; /* used to validate socket handles */ 

    /* Connection cookie */
    UINT32 cookie;

    /* connection struct */
    ipcConn_t conn;
    /* semaphore used to handle
     * connection setup messages
     */
    ipcSemaHandle_t coSema;
    /* all Conns come between listen and accept */
    ipcList_t newConnList; 

    UINT32 backlog;   /* the max number of allowed connection before accept */

    ipcNotification_t* notifications;

    wait_queue_head_t readwq;
    wait_queue_head_t writewq;

    /* watermark values for flow control */
    UINT8 lowWatermark;
    UINT8 highWatermark;

    /* the channel id attached to this socket */
    ipcChannelId_t channelId;
    /* window notification count down */
    UINT8 windowCountDown;

    /* Reference count */
    UINT8 refCount;

} ipcSocket_t;


typedef struct tag_ipcSession_t
{
    ipcList_t drt;     /* DRT Table */

    ipcList_t unboundSockList;

    ipcDRTItem_t *localDrtItem;
    ipcDRTItem_t *ipcAddrToDrtItemLookup[IPC_INVALID_ADDR + 1];
    ipcNodeId_t ipcAddrToNodeId[IPC_INVALID_ADDR + 1];

    ipcHashTable_t nodeIdToIpcAddrTable;    /* node ID to IPC Addr table */
    ipcHashTable_t local_srvid_tab;         /* local service ID table */

    /* Timers used by session */
    ipcTimerHandle_t configReqTimer;
    ipcTimerHandle_t drtBroadcastTimer;  /* server only */

    /* Locks for multi-threaded access */
    ipcMutexHandle_t lockDRTTable;
    ipcMutexHandle_t lockSrvIDTable;

    /* session's state */
    UINT8 state;

    /* service online/offline notifications */
    ipcServiceNotification_t* serviceNotifications;

    /* socket connection callbacks */
    ipcConnOperations_t connOps;
    /* callback function to update user interface */
    void (*update_user_interface)(void);

} ipcSession_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
extern ipcSession_t ipcSession_glb;

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

/*----------------------------------------------------------------------------*
 *                Session init functions & message handler                    *
 *----------------------------------------------------------------------------*/
BOOL ipcSessionInit(void);

void ipcSessionDestroy(void);

BOOL ipcIsMsgValidInCurrState(ipcMsgOpcode_t msg_id);

void ipcDrtHasChanged(void);

ipcPacket_t* ipcCreateConnRequest(ipcAddress_t destAddr,
                                  ipcServiceId_t destSrv,
                                  ipcServiceId_t mySrv,
                                  UINT32 cookie,
                                  UINT8 windowSize,
                                  UINT8 startSeqNum);

ipcPacket_t* ipcCreateConnReply(ipcAddress_t destAddr,
                                      ipcServiceId_t destSrv,
                                      ipcServiceId_t mySrv,
                                      UINT32 cookie,
                                      BOOL willAccept,
                                      UINT8 windowSize,
                                      UINT8 startSeqNum);

ipcPacket_t* ipcCreateConnComplete(ipcAddress_t destAddr,
                                 ipcServiceId_t destSrv,
                                 ipcServiceId_t mySrv,
                                 UINT32 cookie);

ipcPacket_t* ipcCreateConnDown(ipcAddress_t destAddr,
                               ipcServiceId_t destSrv,
                               ipcServiceId_t mySrv,
                               UINT32 cookie,
                               UINT8 how);
/*----------------------------------------------------------------------------*
 *                   inferface to rounter layer                               *
 *----------------------------------------------------------------------------*/

int RTSSDataIndication(ipcPacket_t* pPacket);

int RTSSReady(void);

void RTSSLinkDown(ipcAddress_t lowAddr, ipcAddress_t highAddr);

/*----------------------------------------------------------------------------*
 *                   Functions used to check params                           *
 *----------------------------------------------------------------------------*/

BOOL ipcIsServiceIdAvailable(ipcServiceId_t srvId);

/*----------------------------------------------------------------------------*
 *                   DRT operation func & others                              *
 *----------------------------------------------------------------------------*/

ipcAddress_t ipcNodeIdToIpcAddrWithoutLock(ipcNodeId_t nodeId);

ipcAddress_t ipcNodeIdToIpcAddr(ipcNodeId_t nodeId);

ipcDRTItem_t* ipcNodeIdToDrtItemWithoutLock(ipcNodeId_t nodeId);

void ipcUpdateDrtLookupTables(ipcList_t *drtTable);

ipcServiceId_t ipcAllocateServiceId(void);

BOOL ipcSrvIdListCompare(ipcList_t *srvIdlist1, ipcList_t *srvIdlist2);

ipcLocateServiceReqType_t ipcLocateServiceType(UINT32 node_id,
                                                  ipcServiceId_t srvId);

UINT16 ipcDRTItemSize(ipcDRTItem_t *item);

int ipcDRTItemToBuffer(const ipcDRTItem_t *item, drtItemData_t *buffer);

UINT16 ipcCalDRTSize(ipcList_t *drt);

int ipcDRTToBuffer(ipcList_t *drt_table, UINT8 *buffer);

BOOL ipcAddSocketToLocalSrvIdTab(ipcSocket_t *socket);

BOOL ipcRemoveSocketFromLocalSrvIdTab(ipcSocket_t *socket, BOOL* needChange);

int ipcBufferToDRTItem(const drtItemData_t *buffer, ipcDRTItem_t *item);

int ipcBufferToDRT(const UINT8 *buffer, ipcList_t *drt_table, int numDrtItems);

void ipcFreeDRT(ipcList_t *drt_table);

void ipcFreeDRTItem(ipcDRTItem_t *item);

void ipcRemoveRemoteDRTItems(ipcDrtChangeSet_t* changes);

BOOL ipcFindAddrForService(ipcServiceId_t srvId, ipcAddress_t * ipcAddr);

BOOL ipcServiceInDRTItem(ipcDRTItem_t* drtItem, ipcServiceId_t service,
                         UINT16 *dsmAction);

BOOL ipcDiscardForDsm(ipcServiceId_t service, ipcDRTItem_t* drtItem, int flags);
                         
void ipcHandleNodeDown(ipcNodeId_t nodeId);

/* DRT change operations */
ipcDrtChangeItem_t* ipcDrtChangeItemNew(ipcNodeId_t nodeId, ipcServiceId_t srvId, int change);
void ipcDrtChangeSetInit(ipcDrtChangeSet_t* changeSet);
void ipcDrtChangeAddItem(ipcDrtChangeSet_t* changeSet, ipcDrtChangeItem_t* item);
void ipcDrtChangeFreeAll(ipcDrtChangeSet_t* changeSet);
void ipcDrtChangeSignal(ipcDrtChangeSet_t* changeSet, BOOL needSendDrt);

void ipcDrtRemoveItem(ipcList_t* drt, ipcDRTItem_t* prev, ipcDrtChangeSet_t* changeSet);
ipcDRTItem_t* ipcDrtUpdateItem(ipcDRTItem_t* old_item, ipcDRTItem_t* new_item, ipcDrtChangeSet_t* changeSet);
void ipcDrtAddService(ipcDRTItem_t* item, ipcServiceId_t service, UINT16 dsmAction,
                      ipcDrtChangeSet_t* changeSet);
void ipcDrtRemoveService(ipcDRTItem_t* item, ipcServiceId_t service, ipcDrtChangeSet_t* changeSet);
/*----------------------------------------------------------------------------*
 *      IPC List comparison functions                                         *
 *----------------------------------------------------------------------------*/
BOOL ipcListCompareDrtIpcAddr(ipcListLink_t link, void *ipcAddr);

BOOL ipcListCompareDrtSrvId(ipcListLink_t link, void *serviceId);

/*----------------------------------------------------------------------------*
 * session send | recv util functions                                         *
 *----------------------------------------------------------------------------*/
int ipcSessionBroadcast(ipcPacket_t *pPacket, int flags);

int ipcSendPacketToLocalSockets(ipcPacket_t *shmPacket);

ipcDRTItem_t* ipcNewDRTItem(void);

/*----------------------------------------------------------------------------*
 *      Socket functions
 *----------------------------------------------------------------------------*/
BOOL ipcSocketDestroy(ipcSocket_t *s);

int ipcSocketBind(ipcSocket_t* socket, ipcServiceId_t srvId);

int ipcPutPacketToSocket(ipcSocket_t* socket, ipcPacket_t* packet);

ipcSocket_t* ipcFindSocketBySrvId(ipcServiceId_t srvId);

/* CO socket connection utilities */
void ipcHandleCOCOConnReq(COCOConnRequestMsg_t* connReq);
void ipcHandleCOCOConnRep(COCOConnReplyMsg_t* connRep);
void ipcHandleCOCOConnComp(COCOConnCompleteMsg_t* connComp);
void ipcHandleCOCOConnDown(COCOConnDownMsg_t* connDown);
int ipcCOSocketReceiveEnd(ipcSocket_t* socket);
void ipcDisconnectSocket(ipcSocket_t* socket);
void ipcRemoveObsoleteConnReq(ipcSocket_t* socket);

/* CO socket callbacks */
int ipcCOSocketDataArrival(ipcConn_t* conn, ipcPacket_t* packet);
int ipcCOSocketTxWindowFull(ipcConn_t* conn, BOOL mayBlock);
void ipcCOSocketTxWindowAvailable(ipcConn_t* conn);
void ipcCOSocketMaxRetries(ipcConn_t* conn, ipcPacket_t* pack);
int ipcCOSocketSentListEmpty(ipcConn_t* conn);
int ipcCOSocketDataDeliver(ipcConn_t* conn, ipcPacket_t* pack);
int ipcCOSocketLock(ipcConn_t* conn);
int ipcCOSocketUnlock(ipcConn_t* conn);

/* Notification utilities */
void ipcPlatformNotify(ipcNotification_t* notification);
void ipcNotify(ipcSocket_t* socket, int notificationType);
void ipcSignalNotification(ipcNotification_t* notification, ipcServiceId_t srvId);
void ipcSignalPsNotification(ipcNotification_t *notification);
int ipcRegisterNotification(ipcAddress_t destAddr,
                            ipcServiceId_t destService,
                            int flags,
                            const char* msg,
                            int msglen,
                            ipcServiceId_t localService);
int ipcRegisterPsNotification(ipcAddress_t destAddr,
                              ipcServiceId_t destService,
                              int flags,
                              const char* msg,
                              int msglen,
                              int psFlag,
                              void* q);
void ipcRemoveNotification(ipcSocket_t* socket, int notificationType); 

void ipcHandleNotifySignal(SSSSNotifySignalMsg_t* msg);
void ipcHandleNotifyRegister(SSSSNotifyRegisterMsg_t* msg);
void ipcSignalServiceNotification(ipcNodeId_t node,
                                  ipcServiceId_t service,
                                  int flags);

int ipcRegisterServiceNotify(ipcServiceId_t localService,
                             ipcNodeId_t nodeId,
                             ipcServiceId_t srvId,
                             const char* msg,
                             int msglen,
                             int flags,
                             int* lasterror);
int ipcRegisterServicePsNotify(void* q,
                               int qflags,
                               ipcNodeId_t nodeId,
                               ipcServiceId_t srvId,
                               const char* msg,
                               int msglen,
                               int flags,
                               int* lasterror);
BOOL ipcNeedSignalServiceNotification(ipcServiceNotification_t* notification);

/*--------------------------------------- MACROS ---------------------------------------*/
#define ipcIsSocketValid(socket)    ( (socket != NULL) && \
    (socket != (ipcSocket_t*)IPC_ERROR) && \
    (((ipcSocket_t*)(socket))->isClosed == 0) && \
    (((ipcSocket_t*)(socket))->magic == IPC_SOCKET_MAGIC))

#define ipcLockSocket(socket) ipcOsalMutexLock((socket)->packetQueue.mutex)
#define ipcUnlockSocket(socket) ipcOsalMutexRelease((socket)->packetQueue.mutex)
#define ipcRefSocket(socket) (++(socket)->refCount)
#define ipcUnrefSocket(socket) (--(socket)->refCount)

#undef LOCK
#undef UNLOCK
#define LOCK(x)      ipcOsalMutexLock(ipcSession_glb.lock##x)
#define UNLOCK(x)    ipcOsalMutexRelease(ipcSession_glb.lock##x)

/* This macro should only be used when we need the previous list link in the DRT.
 * The macro ipcAddrToDrtItem is a more efficient way to find a DRT Item otherwise. */
#define findDrtItemByIpcAddr(drtTable, ipcAddr, prevPtr) \
    ( (ipcDRTItem_t *)ipcListFind(drtTable, ipcCastPtr(ipcAddr, void*), ipcListCompareDrtIpcAddr, prevPtr) )

/* IPC Addr is stored with offset of 1 because zero is a valid IPC Address (not to be
 * confused with NULL which means it was not found in the hash table) */
#define ipcAddDrtItemToNodeIdTable(drtItem) \
    ipcHashPut(&ipcSession_glb.nodeIdToIpcAddrTable, ipcCastPtr((drtItem)->node_id, void*), \
               (void *)((int)((drtItem)->ipcaddr) + 1))

#define ipcRemoveFromNodeIdTable(nodeId) \
    ipcHashRemove(&ipcSession_glb.nodeIdToIpcAddrTable, (void *)(nodeId))

#define ipcAddrToDrtItem(ipcAddr)   ipcSession_glb.ipcAddrToDrtItemLookup[ipcAddr]

#define ipcAddrToNodeId(ipcAddr)    ipcSession_glb.ipcAddrToNodeId[ipcAddr]

#define ipcCheckSocketConnection(socket) \
    do { \
        if ((socket)->isCOSocket && \
            !((socket)->canSend && (socket)->peerCanReceive) && \
            !((socket)->canReceive && (socket)->peerCanSend)) \
        { \
            ipcDisconnectSocket(socket); \
        } \
    } while (0)


#ifdef __cplusplus  /* Allow header file to be included in a C++ file */
}
#endif /* __cplusplus */

#endif /* IPC_SESSION_H */
