/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_channel_id.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines all IPC Node IDs
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 14Jan2004  Motorola Inc.         Initial Creation 
*****************************************************************************************/

#ifndef IPC_CHANNEL_ID_H
#define IPC_CHANNEL_ID_H

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define IPC_INVALID_CHANNEL_ID              0xFF
#define IPC_CONTROL_CHANNEL_ID              0x00
#define IPC_NONDEDICATED_CHANNEL_ID         0x01
#define IPC_FIRST_CHANNEL_ID                0x02
#define IPC_LAST_CHANNEL_ID                 0xFE

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


#endif /* IPC_CHANNEL_ID_H */
