/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 1998, 2006 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: dpccshm.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 03/09/1998 Motorola Inc.        Modified for thread safety.
* 05/22/2006 Motorola Inc.        modified for dynamic pool support.
* 20Feb2007  Motorola Inc.        Updated for common memory alloc - removed
                                  usage field, reduced buffer eyecatch size,
                                  and removed unused definitions
* 22Jun2007  Motorola Inc.        Removed unused macros
* 13Jul2007  Motorola Inc.        Added checking for __cplusplus
*****************************************************************************************/

#ifndef DPCCSHM_H
#define DPCCSHM_H

#include "ipc_shared_memory.h"


#define DPCC_SHM_POOL_EYE_CATCH 0x55555555  /* "DDDD" pool header marker */
#define DPCC_SHM_EYE_CATCH      0x7777      /* "ww" shared block marker */

/*
 * A set of checks are performed on pointers passed to the allocator routines
 * to validate their values.
 */
#define DPCC_VAL_ISNOTNULL(m) assert((int)(m))  /* NULL POINTER */
#define DPCC_VAL_ISLONGALIGN(m) assert((((int)(m)) & 3) == 0)   /* BAD ALIGN */
#define DPCC_VAL_HEADNOTNULL(m) (((struct dpcc_memblk *)m - 1)) /* BAD MEM TYPE */
#define DPCC_MSG_STD_CHECKS(msg_ptr) \
            DPCC_VAL_ISNOTNULL(msg_ptr); \
            DPCC_VAL_ISLONGALIGN(msg_ptr); \
            DPCC_VAL_HEADNOTNULL(msg_ptr);

#define DPCC_SHM_MIN_SEG_SZ         32
/*
 * Number of retries to perform if it can not allocate a shared memory
 * buffer.
 */
#define DPCC_SHMALLOC_RETRIES       3

/*
 * Values for self and neighbor (they're boolean, but these values provide
 * more redundancy to make sure they haven't been stepped on):
 */
#define DPCC_FREE       'F'
#define DPCC_ALLOCed    'A'

#ifdef __cplusplus
extern "C" {
#endif
/*
 * shared memory buffer pool link structure
 */
typedef struct dpcc_memblk
{
    unsigned short          eyecatch;
    char                    self;
    char                    neighbor;
    struct dpcc_shm_info*   shm_start_ptr;
    struct dpcc_memblk*     flink;
    struct dpcc_memblk*     blink;
    unsigned long           size;
} dpcc_memblk_t;


/*
 * shared memory segment looks like this
 */
typedef struct dpcc_shm_info
{
    unsigned long       eyecatch;
    int                 status;     /* 0=uninitialized  1=initialized */
    void*               mutex;  
    unsigned long       size;       /*in Bytes*/
    unsigned long       size_used;  /*in Bytes*/
    char*               vir_addr;
    struct dpcc_memblk* freelist;
} dpcc_shm_info_t;


extern int          dpccshmfree(char *);
extern char*        dpccshmalloc(dpcc_shm_info_t *, unsigned long);
extern dpcc_shm_info_t* dpccshmcreate(char *, unsigned long);
extern int          dpccshmdestroy(char *shmname);
extern int          dpccshminit(dpcc_shm_info_t *);
extern int          dpccpoolwalk(struct dpcc_shm_info * shm_info);

/******************************************************************************
 * Place-holders of dpcc mutex stuff, in current implementation, locks are held
 * outside invocation of dpcc functions.
 *****************************************************************************/

typedef void* dpcc_mutex_t;
#   define dpcc_mutex_initialize(aMutexPtr, aMutexName, isInterProc, deadlockDetectTimeout)
#   define dpcc_mutex_destroy(aMutexPtr)
#   define dpcc_mutex_lock(aMutexPtr)
#   define dpcc_mutex_unlock(aMutexPtr)

#ifdef __cplusplus
}
#endif

#endif              /* DPCCSHM_H */
