/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004, 2006 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_device_interface.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header defines the device layer interface to other IPC layers
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 02Jun2004  Motorola Inc.         Initial Creation  
* 01Sep2004  Motorola Inc.         Updated for acknowledged channels                 
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*****************************************************************************************/

#ifndef IPC_DEVICE_INTERFACE_H
#define IPC_DEVICE_INTERFACE_H

#include "ipc_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/

/* CRC Polynomials: 32-bit for frame data, 16-bit for frame header and packet header
   Note: the functions for verifying and calculating the crc assume
   that x^n is always '1' for an nth order polynomial, therefore it does
   not need to be present in the definition of the polynomial */

/* x32 + x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 + x1 + x0 */
#define IPC_CRC32_POLYNOMIAL    0x04c11db7

/* x16 + x15 + x2 + x0 */
#define IPC_CRC16_POLYNOMIAL    0x00018005

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef INT32 (*ipcReadFunction_t)  (void *driverHandle, UINT8 *data, INT32 length);
typedef INT32 (*ipcWriteFunction_t) (void *driverHandle, const UINT8 *data, INT32 length);
typedef void (*ipcStopFunction_t) (void *driverHandle);
typedef void (*ipcCloseFunction_t) (void *driverHandle);

typedef int (*ipcShmWriteFunction_t) (void *data);


typedef struct tag_ipcDeviceDriver_t
{
    void *handle;
    ipcReadFunction_t read;
    ipcWriteFunction_t write;
    ipcStopFunction_t stop;
    ipcCloseFunction_t close;

} ipcDeviceDriver_t;


/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern ipcPortId_t ipcShmPortId;
extern ipcShmWriteFunction_t ipcDeviceShmWrite;

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

BOOL ipcDeviceInit(void);

void ipcDeviceDestroy(void);

int RTDVChannelRequest(ipcPortId_t portId,
                       ipcQoS_t QoS);

int RTDVChannelRelRequest(ipcChannelId_t channelId,
                          ipcPortId_t portId);

int RTDVDataRequest(ipcChannelId_t channelId,
                    ipcPacket_t *packet,
                    ipcPortId_t portId);

void SSDVBaseFrameRequest(UINT8 frameTime);

UINT32 ipcCalculateCrc(const UINT8 *inputBufferPtr, INT32 bufferSize, INT16 crcSize,
                       UINT32 polynomial, UINT32 initialValue);

void ipcDeviceDisable(ipcPortId_t portId);
void ipcDeviceEnable(ipcPortId_t portId);


/* Shared Memory Device Functions */

#ifdef HAVE_PHYSICAL_SHM

BOOL ipcCreateShmDevice(ipcPortId_t portId, ipcShmWriteFunction_t write);

void ipcCloseShmDevice(ipcPortId_t portId);

int ipcShmTransmitPacket(ipcPortId_t portId, ipcChannelId_t channelId, ipcPacket_t *packet);

#else

/* Stub all shared memory device functions for builds with no physical shared memory*/

#define ipcCreateShmDevice(portId, write)                   FALSE
#define ipcCloseShmDevice(portId)
#define ipcShmTransmitPacket(portId, channelId, packet)     IPCENOTIMPLEMENTED

#endif /* HAVE_PHYSICAL_SHM */



/* Multiplexed Device Functions */

#ifndef IPC_SIMPLE_DEVICE

BOOL ipcCreateDevice(ipcPortId_t portId,
                     ipcDeviceType_t deviceType,
                     INT32 bandwidth,
                     INT32 framesPerSecond,
                     void *driver);

void ipcCloseDevice(ipcPortId_t portId);

int ipcCreateTxChannel(ipcPortId_t portId, INT32 bandwidth, ipcChannelId_t id);

int ipcCloseTxChannel(ipcPortId_t portId, ipcChannelId_t id);

int ipcTransmitPacket(ipcPortId_t portId, ipcChannelId_t channelId, ipcPacket_t *packet);


void ipcWakeUpOutputThread(ipcPortId_t portId);

void ipcResetFrameTime(ipcPortId_t portId, UINT8 frameTime);

void ipcDeviceInputThread(ipcPortId_t portId);

void ipcDeviceOutputThread(ipcPortId_t portId);

void ipcDeviceFreePendingPackets(ipcPortId_t portId);

#else /* IPC_SIMPLE_DEVICE */

/* Stub all multiplexed device functions for shared memory only builds */

#define ipcCreateDevice(portId, deviceType, bandwidth, framesPerSecond, driver)     FALSE
#define ipcCloseDevice(portId)
#define ipcCreateTxChannel(portId, bandwidth, id)                                   IPCENOTIMPLEMENTED
#define ipcCloseTxChannel(portId, id)                                               IPCENOTIMPLEMENTED
#define ipcTransmitPacket(portId, channelId, packet)                                IPCENOTIMPLEMENTED

#define ipcWakeUpOutputThread(device)
#define ipcResetFrameTime(portId, frameTime)
#define ipcDeviceInputThread(portId)
#define ipcDeviceOutputThread(portId)
#define ipcDeviceFreePendingPackets(portId)

#endif /* IPC_SIMPLE_DEVICE */


/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcIsSharedMemoryDevice(portId) \
    (ipcStack_glb.ports[portId].deviceType == IPC_DEVICE_SHARED_MEMORY)

#define ipcGetShmDevice(portId)     ( (portId) < IPC_MAX_PORTS ? (ipcShmDevice_t *)ipcStack_glb.ports[portId].device : NULL );

#define ipcCrc16(runningCrc, data, size) \
    ((UINT16)ipcCalculateCrc(data, size, 16, IPC_CRC16_POLYNOMIAL, runningCrc))

#define ipcCrc32(runningCrc, data, size) \
    ipcCalculateCrc(data, size, 32, IPC_CRC32_POLYNOMIAL, runningCrc)


#ifdef __cplusplus  /* Allow header file to be included in a C++ file */
}
#endif /* __cplusplus */

#endif /* IPC_DEVICE_INTERFACE_H */
