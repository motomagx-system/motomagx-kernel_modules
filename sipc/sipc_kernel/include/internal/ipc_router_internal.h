/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_router_internal.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines router only #defines, globals, and function prototypes.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation
* 09Feb2005  Motorola Inc.         Updated for sockets
* 19Sep2005  Motorola Inc.         Updated for Conn-oriented API
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*
*****************************************************************************************/

#ifndef IPC_ROUTER_INTERNAL_H
#define IPC_ROUTER_INTERNAL_H

#include "ipc_messages.h"
#include "ipc_conn.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/
/* Control socket (pseudo-socket) states */
#define IPC_CONTROL_SOCKET_LINK_DOWN           0
#define IPC_CONTROL_SOCKET_LINK_PENDING        1
#define IPC_CONTROL_SOCKET_LINK_UP             2

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void ipcRouterForceLinkDown(ipcPortId_t portId);

void ipcRouterSetAddressRange(ipcAddress_t lowAddr, ipcAddress_t highAddr,
                              ipcPortId_t highPeerPortId);

BOOL ipcIsPeerNode(ipcAddress_t ipcAddr);

ipcMultihopChannelId_t ipcGetMultihopChannelId(ipcAddress_t srcAddr,
                                               ipcAddress_t destAddr,
                                               ipcServiceId_t srcService);

void ipcAddMultihopChannel(ipcMultihopChannelId_t multihopChannelId,
                           ipcChannelId_t localChannelId);

void ipcRemoveMultihopChannel(ipcMultihopChannelId_t multihopChannelId);

ipcChannelId_t ipcMultihopToLocalChannel(ipcMultihopChannelId_t multihopChannelId);

ipcAddress_t ipcMultihopToSrcIpcAddr(ipcMultihopChannelId_t multihopChannelId);

ipcAddress_t ipcMultihopToDestIpcAddr(ipcMultihopChannelId_t multihopChannelId);

void ipcRouterDispatchPacket(ipcPacket_t* packet, ipcPortId_t portId);


int RTRTIpcAddressAssign(ipcPortId_t destPortId);

int RTRTLinkDown(ipcPortId_t portId);

int RTRTLinkComplete(ipcPortId_t portId, BOOL resetSeqNum);

int RTRTChannelRequest(ipcMultihopChannelId_t multihopChannelId, ipcQoS_t QoS);

int RTRTChannelRelRequest(ipcMultihopChannelId_t multihopChannelId);

int RTRTChannelReply(ipcMultihopChannelId_t multihopChannelId, INT8 status);



void handleRTRTLinkDown(RTRTLinkDownMsg_t *packet, ipcPortId_t portId);

void handleRTRTLinkRequest(RTRTLinkRequestMsg_t* packet, ipcPortId_t portId);

void handleRTRTLinkReply(RTRTLinkReplyMsg_t* packet, ipcPortId_t portId);

void handleRTRTLinkComplete(RTRTLinkCompleteMsg_t* packet, ipcPortId_t portId);

void handleRTRTChannelRequest(RTRTChannelRequestMsg_t *packet, ipcPortId_t portId);

void handleRTRTChannelReply(RTRTChannelReplyMsg_t *packet, ipcPortId_t portId);

void handleRTRTChannelRelRequest(RTRTChannelRelRequestMsg_t *packet, ipcPortId_t portId);

void handleRTRTAddressAssign(RTRTAddressAssignMsg_t* packet, ipcPortId_t portId);

void ipcRouterEnterLinkDownState(ipcPortId_t portId);

void ipcRouterEnterLinkPendingState(ipcPortId_t portId);

void ipcRouterEnterLinkUpState(ipcPortId_t portId);

int ipcSendCtrlSockPacket(ipcPortId_t portId, ipcPacket_t* packet);

/* pseudo socket callbacks */
int ipcCtrlSockTxWindowFull(ipcConn_t* conn, BOOL mayBlock);
void ipcCtrlSockTxWindowAvailable(ipcConn_t* conn);
int ipcCtrlSockSentListEmpty(ipcConn_t* conn);
int ipcCtrlSockLock(ipcConn_t* conn);
int ipcCtrlSockUnlock(ipcConn_t* conn);
int ipcCtrlSockDataDeliver(ipcConn_t* conn, ipcPacket_t* packet);
int ipcCtrlSockDataArrival(ipcConn_t* conn, ipcPacket_t* packet);
void ipcCtrlSockMaxRetries(ipcConn_t* conn, ipcPacket_t* pack);

void ipcDoRouterTimerJob(void*);
void ipcDoConnTimerJob(void*);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcAddrToPort(ipcAddr)      ipcStack_glb.ipcAddrToPortTable[ipcAddr]

#define ipcIsCtrlSocketPacket(packet) \
    ((ipcGetMsgFlags(packet) != IPC_PACKET_ACK) && \
        ((ipcGetMsgFlags(packet) & (IPC_PACKET_RELIABLE | IPC_PACKET_CONTROL)) == \
     (IPC_PACKET_RELIABLE | IPC_PACKET_CONTROL)))

#ifdef __cplusplus
}
#endif

#endif /* IPC_ROUTER_INTERNAL_H */
