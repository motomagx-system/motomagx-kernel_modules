/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_memory.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines the interface to interal SIPC memory allocation functions
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 19Feb2007  Motorola Inc.         Initial Creation
* 12Apr2007  Motorola Inc.         fix checksum issue
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_DEBUG
*****************************************************************************************/

#ifndef IPC_MEMORY_H
#define IPC_MEMORY_H

#include "ipc_shared_memory.h"
#include "ipc_osal_impl.h"

#ifdef __cplusplus 
extern "C" {
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/

#define IPC_DEFAULT_MEM_TYPE    0x0000

/* Standard shared memory types below are defined in ipc_defs.h:
#define IPC_PHYSSHM             0x0001 - memory shared between processor cores
#define IPC_PROCSHM             0x0002 - memory shared between processes on same core
*/

/* Platform-specific memory types can range from 0x0003 -> 0x000F */

#define IPC_MAX_MEM_TYPES       (MEMTYPE_FLAG_MASK + 1)

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef void * (*ipcAllocFunc_t) (unsigned int size, unsigned int flags);
typedef int (*ipcFreeFunc_t) (void *ptr);

typedef struct tag_ipcAllocatorTableEntry_t
{
    ipcAllocFunc_t alloc;
    ipcFreeFunc_t free;
    ipcMutexHandle_t mutex;
} ipcAllocatorTableEntry_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/

/* This table holds function pointers to the platform-specific alloc and free
 * functions for every memory type SIPC manages on each platform.  It is 
 * statically intialized to use default function stubs that return NULL on
 * every call (or IPCEBADBUF for free).  Each platform should replace these
 * default function pointers with the appropriate function for all supported
 * memory types */
extern ipcAllocatorTableEntry_t ipcAllocatorTable[IPC_MAX_MEM_TYPES];

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void *ipcAllocImp(unsigned int size, unsigned int flags);
int ipcFreeImp(void *ptr);
int ipcIncRefCount(void *ptr, int amount);

void ipcLockMem(unsigned int memType);
void ipcUnlockMem(unsigned int memType);

void ipcAddAllocator(unsigned int memType, ipcAllocFunc_t alloc, ipcFreeFunc_t free);
void ipcAddAllocatorAlias(unsigned int newMemType, unsigned int existingMemType);
void ipcRemoveAllocator(unsigned int memType);
void ipcRemoveAllocatorAlias(unsigned int memType);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcAlloc(size, flags)   ipcAllocImp(size, flags)
#define ipcFree(ptr)            ipcFreeImp(ptr)

#ifdef __cplusplus 
}
#endif

#endif /* IPC_MEMORY_H */
