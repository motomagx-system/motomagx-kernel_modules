/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2005, 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipclog.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains the common defines and macros used for logging and
*   displaying messages in the Smart IPC Stack.  All the macros defined here
*   have the following format:
*
*   macroName(cond, printf_exp)
*
*   cond - condition that must be true for the message to be logged/displayed
*
*   printf_exp - a printf expression that must be enclosed in parentheses.
*                For example:  ("x = %d", x)
*
*   The cond parameter should be of the form IPC_LOG_XXX which will be defined
*   in this file as (ipcLogEnableMask_glb & IPC_LOG_TYPE_XXX).  This allows
*   entire categories of messages to be turned on and off dynamically by changing
*   the value of ipcLogEnableMask_glb.  The macros ipcLogEnable and ipcLogDisable
*   are defined for this purpose.
*
*   The following macros should be used for logging data:
*
*   ipcPrint - displays message when condition is true (all builds)
*   ipcLog - logs message when condition is true (all builds)
*   ipcLogWithTime - same as above but also logs timestamp (all builds)
*
*   ipcDebugPrint, ipcDebugLog, ipcDebugLogWithTime -
*       same as macros above but only active for debug builds
*
*   ipcWarning - ignored for release builds.  For debug builds this function
*                will log the message with a timestamp and the prefix
*                "WARNING: <FILE> line <LINE>".  It will also display the 
*                message on the screen.
*
*   ipcError - for all builds this function will log the message with a timestamp
*              and the prefix "ERROR: <FILE> line <LINE>".  For debug builds, the
*              message will also be displayed on the screen.
*
*
*
*   Platform Implementation:
*   ========================
*
*   The following functions (or macros) must be declared or defined for each
*   platform in ipclog_impl.h:
*
*       void ipcLogFlush(void); //for buffered file streams
*
*       void ipcLogBegin(int type, int category); //for EventViewer only
* 
*       void ipcPrintFunc(const char *format, ...);
*
*       void ipcLogFunc(const char *format, ...);
*
*       void ipcTimestamp(ipcLogFunction_t print);
*
* 
*   The following global variable must be defined:
* 
*       unsigned int ipcLogEnableMask_glb
*
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 28May2004  Motorola Inc.         Initial creation
* 20Aug2004  Motorola Inc.         
* 31Aug2004  Motorola Inc.         Add GetIpcLogVersion
* 23Nov2004  Motorola Inc.         Moved Windows code to ipclog_impl.h
* 02Feb2005  Motorola Inc.         Simplifed log function call
* 12Jul2005  Motorola Inc.         Added conditional logging
* 26Jan2007  Motorola Inc.         add more checks on the shared memory buffer headers
* 12Apr2007  Motorola Inc.         fix checksum issue
* 25May2007  Motorola Inc.         Dump memory pools on memory error
* 13Jul2007  Motorola Inc.         Changed definition of ipcLogIsEnabled when IPC_NO_LOG is defined
* 18Jul2007  Motorola Inc.         Added ipcTrace funcition
*****************************************************************************************/

#ifndef IPCLOG_H
#define IPCLOG_H

#include "ipclog_impl.h"


#ifdef __cplusplus 
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/

/* log message types */
#define IPCLOG_TYPE_GENERAL             0x00000001
#define IPCLOG_TYPE_SESSION             0x00000002
#define IPCLOG_TYPE_ROUTER              0x00000004
#define IPCLOG_TYPE_DEVICE              0x00000008
#define IPCLOG_TYPE_USER_APP            0x00000010
#define IPCLOG_TYPE_DATA                0x00000020  /* log data (RX and TX) */
#define IPCLOG_TYPE_CHANNELS            0x00000040  /* detailed channel info */
#define IPCLOG_TYPE_DEVICE_DECODE       0x00000080  /* detailed device decoder status */
#define IPCLOG_TYPE_RX_FRAMES           0x00000100  /* RX device frame status */
#define IPCLOG_TYPE_TX_FRAMES           0x00000200  /* TX device frame status */
#define IPCLOG_TYPE_PACKET_LOGGER       0x00000400
#define IPCLOG_TYPE_MEMTRACK            0x00000800  /* tracking shared mem alloc/incref/free */

#define IPCLOG_LEVEL_DEBUG              0x00010000
#define IPCLOG_LEVEL_WARNING            0x00020000
#define IPCLOG_LEVEL_ERROR              0x00040000
#define IPCLOG_LEVEL_PANIC              0x00080000

#define IPCLOG_TYPE_ALL                 0xFFFFFFFF


/* log conditions - for turning message types on and off */
#define IPCLOG_GENERAL                  ipcLogIsEnabled(IPCLOG_TYPE_GENERAL)
#define IPCLOG_SESSION                  ipcLogIsEnabled(IPCLOG_TYPE_SESSION)
#define IPCLOG_ROUTER                   ipcLogIsEnabled(IPCLOG_TYPE_ROUTER)
#define IPCLOG_DEVICE                   ipcLogIsEnabled(IPCLOG_TYPE_DEVICE)
#define IPCLOG_USER_APP                 ipcLogIsEnabled(IPCLOG_TYPE_USER_APP)
#define IPCLOG_DATA                     ipcLogIsEnabled(IPCLOG_TYPE_DATA)
#define IPCLOG_CHANNELS                 ipcLogIsEnabled(IPCLOG_TYPE_CHANNELS)
#define IPCLOG_DEVICE_DECODE            ipcLogIsEnabled(IPCLOG_TYPE_DEVICE_DECODE)
#define IPCLOG_RX_FRAMES                ipcLogIsEnabled(IPCLOG_TYPE_RX_FRAMES)
#define IPCLOG_TX_FRAMES                ipcLogIsEnabled(IPCLOG_TYPE_TX_FRAMES)
#define IPCLOG_PACKET_LOGGER            ipcLogIsEnabled(IPCLOG_TYPE_PACKET_LOGGER)
#define IPCLOG_MEMTRACK                 ipcLogIsEnabled(IPCLOG_TYPE_MEMTRACK)

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern unsigned int ipcLogEnableMask_glb;

/*--------------------------------------- MACROS ---------------------------------------*/

#ifdef IPC_NO_LOG /* Turn off ALL logging functions */

    #define ipcPanicPrint()
    #define ipcPrint(cond, printf_exp)
    #define ipcLog(cond, printf_exp)
    #define ipcLogWithTime(cond, printf_exp)
    #define ipcDebugPrint(cond, printf_exp)
    #define ipcDebugLog(cond, printf_exp)
    #define ipcDebugLogWithTime(cond, printf_exp)
    #define ipcWarning(cond, printf_exp)
    #define ipcError(cond, printf_exp)
    #define ipcLogEnable(messageType)
    #define ipcLogDisable(messageType)
    #define ipcLogIsEnabled(messageType) 0
    #define ipcTrace(status,type)
    

#else /* !IPC_NO_LOG */

    #define ipcLogEnable(messageType)       (ipcLogEnableMask_glb |= (messageType))

    #define ipcLogDisable(messageType)      (ipcLogEnableMask_glb &= ~(messageType))

    #define ipcLogIsEnabled(messageType)    (ipcLogEnableMask_glb & (messageType))

    /* 16 is the length limit of current->comm */
    #define ipcPanicPrint() ipcLogFunc("Critical ERROR happened:%-*s(PID:%d)\n", 16, current->comm, current->pid)

    /* 16 is the length limit of current->comm */
    #define ipcPanicJob()  panic("Panic happened:%-*s(PID:%d) %s Line %d\n", 16, current->comm, current->pid, __FUNCTION__, __LINE__)

    extern void ipcHandleMemoryError(void);
    
    #define ipcPrint(cond, printf_exp)                                      \
        if(cond)                                                            \
        {                                                                   \
            ipcLogBegin(IPCLOG_INFO, cond);                                 \
            ipcPrintFunc printf_exp;                                        \
        }

    #define ipcLog(cond, printf_exp)                                        \
        if(cond)                                                            \
        {                                                                   \
            ipcLogBegin(IPCLOG_INFO, cond);                                 \
            ipcLogFunc printf_exp;                                          \
        }

    #define ipcLogWithTime(cond, printf_exp)                                \
        if(cond)                                                            \
        {                                                                   \
            ipcLogBegin(IPCLOG_INFO, cond);                                 \
            ipcTimestamp(ipcLogFunc);                                       \
            ipcLogFunc printf_exp;                                          \
        }


    #define __ipcPrintAlert(alert_str, printf_exp)                          \
        ipcPrintFunc(alert_str "%s line %d: ", __FUNCTION__, __LINE__);         \
        ipcPrintFunc printf_exp

    #define __ipcLogAlert(alert_str, printf_exp)                            \
        ipcTimestamp(ipcLogFunc);                                           \
        ipcLogFunc(alert_str "%s line %d: ", __FUNCTION__, __LINE__);           \
        ipcLogFunc printf_exp
    

    /* For debug builds, we will display errors and warnings on the screen
     * in addition to logging them */
    #define ipcDebugPrint(cond, printf_exp)                                 \
        if(cond && ipcLogIsEnabled(IPCLOG_LEVEL_DEBUG))                     \
        {                                                                   \
            ipcLogBegin(IPCLOG_INFO, cond);                                 \
            ipcPrintFunc printf_exp;                                        \
        }

    #define ipcDebugLog(cond, printf_exp)                                   \
        if(cond && ipcLogIsEnabled(IPCLOG_LEVEL_DEBUG))                     \
        {                                                                   \
            ipcLogBegin(IPCLOG_INFO, cond);                                 \
            ipcLogFunc printf_exp;                                          \
        }

    #define ipcDebugLogWithTime(cond, printf_exp)                           \
        if(cond && ipcLogIsEnabled(IPCLOG_LEVEL_DEBUG))                     \
        {                                                                   \
            ipcLogBegin(IPCLOG_INFO, cond);                                 \
            ipcTimestamp(ipcLogFunc);                                       \
            ipcLogFunc printf_exp;                                          \
        }


    #define ipcWarning(cond, printf_exp)                                \
        if(cond && ipcLogIsEnabled(IPCLOG_LEVEL_WARNING))               \
        {                                                               \
            ipcLogBegin(IPCLOG_WARNING, cond);                          \
            __ipcLogAlert("WARNING: ", printf_exp);                     \
        }
        
    #define ipcError(cond, printf_exp)                                  \
        if(cond && ipcLogIsEnabled(IPCLOG_LEVEL_ERROR))                 \
        {                                                               \
            ipcLogBegin(IPCLOG_ERROR, cond);                            \
            __ipcLogAlert("ERROR: ", printf_exp);                       \
            ipcPanicPrint();                                            \
            if ((cond) & IPCLOG_TYPE_MEMTRACK)                          \
            {                                                           \
                ipcHandleMemoryError();                                \
            }                                                           \
            if (ipcLogIsEnabled(IPCLOG_LEVEL_PANIC))                    \
            {                                                           \
                ipcPanicJob();                                          \
            }                                                           \
        }
    #define ipcTrace(status,type)    ipcTraceFunc(status,type)

#endif /* !IPC_NO_LOG */

/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

typedef int (*ipcLogFunction_t)(const char *, ...);


#ifdef __cplusplus 
}
#endif

#endif /* IPCLOG_H */
