/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_device_debug.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Jan2007 Motorola Inc.         Initial Creation
*****************************************************************************************/

#ifndef IPC_DEVICE_DEBUG_H
#define IPC_DEVICE_DEBUG_H

/* Debug functions for IPC Device Layer */

#include "ipc_common.h"
#include "ipc_list.h"
#include "ipc_osal.h"

#ifdef __cplusplus
extern "C" {
#endif


#define TESTDATA    0x00
#define BEGIN       0x01
#define DATA        0x02
#define ACK         0x03
#define END         0x04

    
#ifdef IPC_DEBUG
    
    typedef struct tag_ipcDeviceDebug_t
    {
        ipcSemaHandle_t deviceAckSema;
        int numDatagramsSent;
        int numPacketsSent;
        int numPacketsReceived;
        char filename[50];

    } ipcDeviceDebug_t;

    extern ipcDeviceDebug_t ipcDeviceDebug_glb;


    ipcPacket_t * createDataPacket(UINT8 id, UINT16 length);
    BOOL verifyDataPacket(ipcPacket_t *packet);
    BOOL processTestPacket(ipcPacket_t *packet);

    void sprintDeviceStructSizes(char *buffer);
    void sprintSequenceNumbers(char *buffer, void *devicePtr);
    void sprintFrame(char *buffer, UINT8 *frame);

    void printDataImp(const char *description, const UINT8 *data, INT32 length);
    void printPacketListImp(ipcList_t *list);
    void printTxChannelsImp(void *devicePtr);
    void printTxFrameImp(void *devicePtr);
    void printRxFrameImp(void *devicePtr);
    void printPacketImp(const char *description, ipcPacket_t *packet);

    #define printData               if (IPCLOG_DATA)        printDataImp
    #define printPacket             if (IPCLOG_DATA)        printPacketImp
    #define printPacketList         if (IPCLOG_DATA)        printPacketListImp
    #define printTxChannels         if (IPCLOG_CHANNELS)    printTxChannelsImp
    #define printRxFrame            if (IPCLOG_RX_FRAMES)   printRxFrameImp
    #define printTxFrame            if (IPCLOG_TX_FRAMES)   printTxFrameImp

#else /* !IPC_DEBUG */
    
    #define printData(a,b,c)
    #define printPacket(a,b)
    #define printPacketList(a)
    #define printTxChannels(a)
    #define printRxFrame(a)
    #define printTxFrame(a)

#endif



#ifdef TIME_BENCHMARK

    extern void *pTimeBench1;
    extern void *pTimeBench2;
    extern void *pTimeBench3;
    extern void *pTimeBench4;
    extern void *pTimeBench5;
    extern void *pTimeBench6;
    extern void *pTimeBench7;
    extern void *pTimeBench8;
    extern void *pTimeBench9;

    void timeBenchStartImp(void *timeBenchmark);
    void timeBenchStopImp(void *timeBenchmark);
    double timeBenchReportImp(void *timeBenchmark, const char *msg);

    #define timeBenchStart   timeBenchStartImp
    #define timeBenchStop    timeBenchStopImp
    #define timeBenchReport  timeBenchReportImp

#else /* NO TIME_BENCHMARK */

    #define timeBenchStart(a)
    #define timeBenchStop(a)
    #define timeBenchReport(a, b)

#endif



#ifdef __cplusplus  /* Allow header file to be included in a C++ file */
}
#endif /* __cplusplus */

#endif /* IPC_DEVICE_DEBUG_H */
