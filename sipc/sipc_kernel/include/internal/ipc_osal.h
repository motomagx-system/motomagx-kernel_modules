/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004, 2006 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_osal.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines the common interface to the Operating System Abstraction
*   Layer (OSAL)
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 03Dec2004  Motorola Inc.         Initial Creation
* 21Nov2006  Motorola Inc.         Added DSM message discard
* 23Feb2007  Motorola Inc.         Unify memory allocation functions
* 29Jun2007  Motorola Inc.         Make function ipcOsalGetProcessName available in release
* 19Jul2007  Motorola Inc.         Cleanup unused macros
*****************************************************************************************/

#ifndef IPC_OSAL_H
#define IPC_OSAL_H

#include "ipc_platform_defs.h"
#include "ipc_memory.h"
#include "ipc_osal_impl.h"

#ifdef __cplusplus
extern "C" {
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/

/* Osal Error Codes */
#define IPCE_OK                        0
#define IPCE_OUT_OF_MEMORY            -1
#define IPCE_OBJECT_NOT_AVAILABLE     -2
#define IPCE_OBJECT_NOT_FOUND         -3
#define IPCE_OBJECT_IN_USE            -4
#define IPCE_ILLEGAL_USE              -5
#define IPCE_TIMEOUT                  -6

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/


typedef void (*ipcOsalTimerCallback_t) (void *param);


UINT32 ipcOsalGetThreadId(void);

UINT32 ipcOsalGetProcessId(void);

const char *ipcOsalGetProcessName(UINT32 pid);


int ipcOsalSemaOpen(ipcSemaHandle_t *semaHandle, UINT32 initialCount);

int ipcOsalSemaClose(ipcSemaHandle_t sema);

int ipcOsalSemaSignal(ipcSemaHandle_t sema);

int ipcOsalSemaWait(ipcSemaHandle_t sema, UINT32 timeout);

#define ipcOsalSemaReset(sema)\
    do ; while (IPCE_OK == ipcOsalSemaWait((sema), 0))

#define ipcOsalSemaWakeupAll(sema)\
    do ipcOsalSemaSignal((sema)); while (ipcOsalSemaWait((sema), 0) == IPCE_TIMEOUT)

int ipcOsalMutexOpen(ipcMutexHandle_t *mutexHandle);

int ipcOsalMutexClose(ipcMutexHandle_t mutex);

int ipcOsalMutexLock(ipcMutexHandle_t mutex);

int ipcOsalMutexRelease(ipcMutexHandle_t mutex);

UINT32 ipcOsalGetTickCount(void);

int ipcOsalTimerOpen(ipcTimerHandle_t *timerHandle);

int ipcOsalTimerInit(ipcTimerHandle_t timer, UINT32 milliseconds,
                     ipcOsalTimerCallback_t callback, void *param);

int ipcOsalTimerClose(ipcTimerHandle_t timer);

int ipcOsalTimerStart(ipcTimerHandle_t timer);

int ipcOsalTimerStop(ipcTimerHandle_t timer);

BOOL ipcOsalTimerIsActive(ipcTimerHandle_t timer);

int ipcOsalSleep(UINT32 milliseconds);

void ipcOsalGetTimestamp(long *sec, long *usec);



unsigned short      ipcOsalRead16(const void *addr);

unsigned long       ipcOsalRead32(const void *addr);

void    ipcOsalWrite16(void *addr, unsigned short val); 
          
void    ipcOsalWrite32(void *addr, unsigned long val);      

#if 0 /* Additional memory access prototypes or macros defined in ipc_osal_impl.h */

unsigned char       ipcOsalRead8(const void *addr);

unsigned short      ipcOsalReadAligned16(const void *addr);

unsigned long       ipcOsalReadAligned32(const void *addr);

void    ipcOsalWrite8(void *addr, unsigned char val);    
       
void    ipcOsalWriteAligned16(void *addr, unsigned short val); 
          
void    ipcOsalWriteAligned32(void *addr, unsigned long val);      

#endif /* 0 */



BOOL ipcPlatformInit(void);

BOOL ipcPlatformDeviceInit(void);

void ipcPlatformDestroy(void);

void ipcPlatformDataIndication(void);

unsigned short ipcGetNodeDsmStatus(unsigned char ipcAddr);


/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcGetRefCount(ptr)     ipcIncRefCount(ptr, 0)


#define ipcOsalRev16(val)  ( ( ((unsigned short)(val)) >> 8 ) | \
                             ( ((unsigned short)(val)) << 8 ) )
                        
#define ipcOsalRev32(val)  ( (  ((unsigned long)(val)) >> 24              ) | \
                             ( (((unsigned long)(val)) & 0x00ff0000) >> 8 ) | \
                             ( (((unsigned long)(val)) & 0x0000ff00) << 8 ) | \
                             (  ((unsigned long)(val)) << 24              ) )


#ifdef __cplusplus
}
#endif

#endif /* IPC_OSAL_H */
