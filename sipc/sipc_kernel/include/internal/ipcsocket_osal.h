/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipcsocket_osal.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file contains platform specific types and macros for the Windows
*   direct function call implementation of the IPC stack.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Mar2005  Motorola Inc.         Initial Creation
* 01Jul2005  Motorola Inc.         Updated for linux
* 19Aug2005  Motorola Inc.         Make other modules in kernel  space can also create channel
*****************************************************************************************/
#ifndef IPCSOCKET_IMPL_H
#define IPCSOCKET_IMPL_H

#include "ipc_session.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef ipcSocket_t         ipcSocketHandle_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcGetSocketFromHandle(hSock)       ((ipcSocket_t *)hSock)

#endif /* IPCSOCKET_IMPL_H */
