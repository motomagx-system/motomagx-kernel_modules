/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004, 2006 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_common.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines IPC constants and data structures used by all layers
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 02Jun2004  Motorola Inc.         Initial Creation
* 14Jul2004  Motorola Inc.         Add enum MOT_RETURN_T
* 14Dec2004  Motorola Inc.         Add function prototype ipcStackInit
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 22May2007  Motorola Inc.         Update sipc version to 01.38.00
* 10Jul2007  Motorola Inc.         Update sipc version to 01.38.04
* 13Jul2007  Motorola Inc.         Removed unused field pendingPackets in ipcPort_t
*****************************************************************************************/

#ifndef IPC_COMMON_H
#define IPC_COMMON_H

#include "ipc_platform_defs.h"
#include "ipc_channel_id.h"
#include "ipc_packet.h"
#include "ipc_queue.h"
#include "ipc_hash.h"
#include "ipc_defs.h"

#ifdef __cplusplus 
extern "C" {
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/
/* NOTE: The version number has to be updated at every release */
#define IPC_VERSION                     0x013804

#define IPC_MAX_CHANNELS                (IPC_LAST_CHANNEL_ID + 1)
#define IPC_MAX_PORTS                   10
#define IPC_MAX_PACKET_SIZE             0x3FF000  /* 4M - 4K */

/* IPC Address defines */
#define IPC_INVALID_ADDR                0xFF
#define IPC_SERVER_ADDR                 (0xFF-1)

#define IPC_INVALID_PORT                0xFF

/* IPC Internal control IDs */
#define IPC_SRV_SESSIONCONTROL          IPC_CONTROL_SERVICE(0x0000)
#define IPC_SRV_ROUTERCONTROL           IPC_CONTROL_SERVICE(0x0001)
#define IPC_SRV_DEVICECONTROL           IPC_CONTROL_SERVICE(0x0002)

/*------------------------------------ ENUMERATIONS ------------------------------------*/


typedef enum tag_ipcStackState_t
{
    IPC_STACK_STATE_NULL,
    IPC_STACK_STATE_RUNNING,
    IPC_STACK_STATE_TERMINATED,
    IPC_STACK_STATE_ERROR

} ipcStackState_t;

typedef enum tag_ipcDeviceType_t
{
    IPC_DEVICE_SHARED_MEMORY,
    IPC_DEVICE_USB,
    IPC_DEVICE_RS232,

    /* The rest are for test purposes only */
    IPC_DEVICE_UDP,
    IPC_DEVICE_TCP,
    IPC_DEVICE_EXT

} ipcDeviceType_t;


/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/


typedef UINT32  ipcNodeId_t;
typedef UINT16  ipcServiceId_t;
typedef UINT8   ipcChannelId_t;
typedef UINT32  ipcMultihopChannelId_t;
typedef UINT8   ipcPortId_t;
typedef UINT8   ipcAddress_t;
typedef UINT8   ipcDataType_t;
typedef INT32   ipcQoS_t;

typedef struct tag_ipcConn_t ipcConn_t;
typedef struct tag_ipcConnOperations_t ipcConnOperations_t;

typedef struct tag_ipcPort_t
{
    ipcDeviceType_t deviceType;
    UINT16 state;
    ipcTimerHandle_t timer;
    BOOL enabled;
    void *device;
    ipcConn_t* conn;

    ipcMutexHandle_t mutex;
} ipcPort_t;


/* Port to IPC Address Range Table */
typedef struct tag_ipcPortToAddrRangeEntry_t
{
    UINT8 lowAddr;
    UINT8 highAddr;

} ipcPortToAddrRangeEntry_t;


/* Contains common global variables used by all layers of the IPC stack */
typedef struct tag_ipcStack_t
{
    /* flag used to indicate server or client node */
    BOOL isServer;

    /* the local node ID */
    ipcNodeId_t nodeId;

    /* the local IPC address */
    ipcAddress_t ipcAddr;

    /* endianness for this node */
    ipcDataType_t datatype;

    /* queue for received packets */
    ipcQueue_t receivedPacketQueue;

    /* Conn data structures */
    ipcList_t connList; /* ipcList_t<ipcConn_t> */
    ipcTimerHandle_t connTimer;
    ipcMutexHandle_t connMutex;

    /* device layer ports */
    int numOpenPorts;
    ipcPort_t ports[IPC_MAX_PORTS];

    /* router structures */
    ipcPortToAddrRangeEntry_t portToIpcAddrRangeTable[IPC_MAX_PORTS];
    ipcPortId_t ipcAddrToPortTable[IPC_INVALID_ADDR + 1];
    ipcAddress_t higherPeerAddr;

    ipcConnOperations_t* routerOps;

    /* for multihop channel requests */
    ipcHashTable_t multihopChannelTable;
    ipcMutexHandle_t multihopChannelMutex;
    ipcSemaHandle_t multihopChannelSema;
    ipcMultihopChannelId_t multihopChannelPending;
    int multihopChannelReply;

    /* indicates the current state of the IPC stack */
    volatile ipcStackState_t state;

} ipcStack_t;


/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern ipcStack_t ipcStack_glb;

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void ipcStackConfig(BOOL runAsServer, UINT32 nodeId, UINT8 dataType);
BOOL ipcStackInit(void);
void ipcStackTerminate(void);
void ipcStackDestroy(void);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcMax(x,y)  (((x) > (y)) ? (x) : (y))
#define ipcMin(x,y)  (((x) < (y)) ? (x) : (y))

#define ipcIsPublishedServiceId(srvId)      ((srvId) & IPC_SRVID_P_BIT)
#define ipcIsMulticastServiceId(srvId)      ((srvId) & IPC_SRVID_M_BIT)

#define IS_BIG_ENDIAN (*(int*)"abcd" == 0x61626364)

/* Use this macro to avoid warnings for casting pointer to smaller integer */
#define ipcCastPtr(ptr, type)   ((type)(int)(ptr))

#define ipcIsControlPacket(packet)  (ipcGetMsgFlags(packet) & IPC_PACKET_CONTROL)


#ifdef __cplusplus 
}
#endif

#endif /* IPC_COMMON_H */
