/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004, 2006 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_messages.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines all internal IPC message types
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 03Dec2004  Motorola Inc.         Initial Creation
* 21Nov2006  Motorola Inc.         Support DSM message discard
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 22Jun2007  Motorola Inc.         Make all IPC messages 4 bytes aligned
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*****************************************************************************************/

#ifndef IPC_MESSAGES_H
#define IPC_MESSAGES_H

#include "ipc_defs.h"
#include "ipc_common.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

/* Status for SSSSAuthReply and RTRTChannelReply */
#define IPC_STATUS_OK                           0
#define IPC_STATUS_NO_CHANNEL                   1
#define IPC_STATUS_DUPLICATE_NODE_TYPE          2
#define IPC_STATUS_DUPLICATE_IPC_ADDR           3


/*------------------------------------ ENUMERATIONS ------------------------------------*/

/*
 * These messages are so-called control messages, they go through control 
 * channels, and they are delivered reliable.
 */
enum
{
    /* Session Messages */
    IPC_MSG_SSSS_CLIENT_AUTH_REQ,
    IPC_MSG_SSSS_CLIENT_CONFIG_REQ,
    IPC_MSG_SSSS_CLIENT_LINK_DOWN,
    IPC_MSG_SSSS_SERVER_AUTH_REPLY,
    IPC_MSG_SSSS_SERVER_CONFIG_UPD_INDICATION,

    /* Notification Messages */
    IPC_MSG_SSSS_NOTIFY_REG,
    IPC_MSG_SSSS_NOTIFY_SIGNAL,

    /* Socket Messages */
    IPC_MSG_COCO_CONN_REQ,
    IPC_MSG_COCO_CONN_REP,
    IPC_MSG_COCO_CONN_COMP,
    IPC_MSG_COCO_CONN_DOWN,
    
    /* Router Messages */
    IPC_MSG_RTRT_LINK_REQUEST,
    IPC_MSG_RTRT_LINK_REPLY,
    IPC_MSG_RTRT_LINK_COMPLETE,
    IPC_MSG_RTRT_LINK_DOWN,
    IPC_MSG_RTRT_IPC_ADDRESS_ASSIGN,

    IPC_MSG_RTRT_CHANNEL_REQUEST,
    IPC_MSG_RTRT_CHANNEL_REPLY,
    IPC_MSG_RTRT_CHANNEL_REL_REQUEST,
    IPC_MSG_RTRT_CHANNEL_REL_REPLY,

    IPC_MSG_DESTROY
};


/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef UINT8 ipcMsgOpcode_t;


typedef struct tag_SSSSAuthRequestMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 nodeId[4];
    UINT8 datatype[1];
    UINT8 pad[2];
    UINT8 end;

} SSSSAuthRequestMsg_t;

#define SSSS_AUTH_REQUEST_DATA_SIZE \
    (offsetof(SSSSAuthRequestMsg_t, end) - IPC_PACKET_DATA_OFFSET)


typedef struct tag_SSSSAuthReplyMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 status[1];
    UINT8 pad[2];
    UINT8 end;

} SSSSAuthReplyMsg_t;

#define SSSS_AUTH_REPLY_DATA_SIZE \
    (offsetof(SSSSAuthReplyMsg_t, end) - IPC_PACKET_DATA_OFFSET)



/* 
 * DRT item structure:  (4 bytes wide)
 *
 * +-------------------------------------------+
 * | node_id                                   |  4
 * +----------+----------+---------------------+
 * | ipcaddr  | datatype | num_services        |  8
 * +----------+----------+---------------------+
 * | srvId 1             | dsmAction 1         |  12
 * +---------------------+---------------------+
 * | ...                 | ...                 | 
 * +---------------------+---------------------+
 * | srvId n             | dsmAction n         |  8 + (4 * num_services)
 * +---------------------+---------------------+
 *
 */

typedef struct tag_drtServiceData_t
{
    ipcServiceId_t  srvId;
    UINT16          dsmAction;  /* IPC_DSM_WAKE_UP or IPC_DSM_NO_WAKE_UP */
    
} drtServiceData_t;

typedef struct tag_drtItemData_t
{
    ipcNodeId_t         node_id;
    ipcAddress_t        ipcaddr;
    UINT8               datatype;
    UINT16              num_services;
    drtServiceData_t    services[1];
    
} drtItemData_t;

#define calcDrtItemSize(numSrvIds) \
    (offsetof(drtItemData_t, services) + ((numSrvIds) * sizeof(drtServiceData_t)))


typedef struct tag_SSSSConfigRequestMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[3];           /* For 32-bit alignment */
    drtItemData_t drtItem;  /* start of DRT data (variable length message) */

} SSSSConfigRequestMsg_t;

#define SSSS_CONFIG_REQUEST_MIN_SIZE \
    (offsetof(SSSSConfigRequestMsg_t, drtItem) - IPC_PACKET_DATA_OFFSET)


typedef struct tag_SSSSConfigUpdIndicationMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[1];           /* For 16-bit alignment */
    UINT8 numDrtItems[2];   /* Same as number of nodes in the network */
    UINT8 drtData[1];       /* start of DRT data (variable length message) */

} SSSSConfigUpdIndicationMsg_t;

#define SSSS_CONFIG_UPDATE_INDICATION_MIN_SIZE \
    (offsetof(SSSSConfigUpdIndicationMsg_t, drtData) - IPC_PACKET_DATA_OFFSET)


typedef struct tag_SSSSLinkDownMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 ipcAddressRangeLow[1];
    UINT8 ipcAddressRangeHigh[1];
    UINT8 pad[1];
    UINT8 end;

} SSSSLinkDownMsg_t;

#define SSSS_LINK_DOWN_DATA_SIZE \
    (offsetof(SSSSLinkDownMsg_t, end) - IPC_PACKET_DATA_OFFSET)



typedef struct tag_RTRTLinkRequestMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 myAddr[1];   /* Tell peer node its IPC address */
    UINT8 pad[2];
    UINT8 end;

} RTRTLinkRequestMsg_t;

#define RTRT_LINK_REQUEST_DATA_SIZE \
    (offsetof(RTRTLinkRequestMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_RTRTLinkReplyMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[3];
    UINT8 end;

} RTRTLinkReplyMsg_t;

#define RTRT_LINK_REPLY_DATA_SIZE \
    (offsetof(RTRTLinkReplyMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_RTRTLinkCompleteMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[3];
    UINT8 end;

} RTRTLinkCompleteMsg_t;

#define RTRT_LINK_COMPLETE_DATA_SIZE \
    (offsetof(RTRTLinkCompleteMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_RTRTAddressAssignMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 ipcAddressRangeLow[1];
    UINT8 ipcAddressRangeHigh[1];
    UINT8 pad[1];
    UINT8 end;

} RTRTAddressAssignMsg_t;

#define RTRT_ADDRESS_ASSIGN_DATA_SIZE \
    (offsetof(RTRTAddressAssignMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_RTRTLinkDownMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[3];
    UINT8 end;

} RTRTLinkDownMsg_t;

#define RTRT_LINK_DOWN_DATA_SIZE \
    (offsetof(RTRTLinkDownMsg_t, end) - IPC_PACKET_DATA_OFFSET)


typedef struct tag_RTRTChannelRequestMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 multihopChannelId[4];
    UINT8 QoS[4];
    UINT8 pad[3];
    UINT8 end;

} RTRTChannelRequestMsg_t;

#define RTRT_CHANNEL_REQUEST_DATA_SIZE \
    (offsetof(RTRTChannelRequestMsg_t, end) - IPC_PACKET_DATA_OFFSET)


typedef struct tag_RTRTChannelReplyMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 multihopChannelId[4];
    UINT8 status[1];
    UINT8 pad[2];
    UINT8 end;

} RTRTChannelReplyMsg_t;

#define RTRT_CHANNEL_REPLY_DATA_SIZE \
    (offsetof(RTRTChannelReplyMsg_t, end) - IPC_PACKET_DATA_OFFSET)


typedef struct tag_RTRTChannelRelRequestMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 multihopChannelId[4];
    UINT8 pad[3];
    UINT8 end;

} RTRTChannelRelRequestMsg_t;

#define RTRT_CHANNEL_REL_REQUEST_DATA_SIZE \
    (offsetof(RTRTChannelRelRequestMsg_t, end) - IPC_PACKET_DATA_OFFSET)

/* Notification data structures */
/* All these data structrues are session control messages */
typedef struct tag_SSSSNotifyRegisterMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[3]; /* Needed for memcpy to work correctly on msg data below
                   * (because of BE-16 munging on StarCore) */

    char msg[IPC_NOTIFICATION_MAX_SIZE];
    UINT8 msglen[4];
    UINT8 flags[4];
    UINT8 psFlags[4];
    UINT8 q[4];             /* The platform specific queue */

    UINT8 srcService[2];    /* The service of the socket waiting for the notification */
    UINT8 destService[2];

    UINT8 end;

} SSSSNotifyRegisterMsg_t;

#define SSSS_NOTIFY_REGISTER_DATA_SIZE \
    (offsetof(SSSSNotifyRegisterMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_SSSSNotifySignalMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 pad[3]; /* Needed for memcpy to work correctly on msg data below
                   * (because of BE-16 munging on StarCore) */

    char msg[IPC_NOTIFICATION_MAX_SIZE];
    UINT8 msglen[4];
    UINT8 flags[4];
    UINT8 psFlags[4];
    UINT8 q[4];

    UINT8 end;

} SSSSNotifySignalMsg_t;

#define SSSS_NOTIFY_SIGNAL_DATA_SIZE \
    (offsetof(SSSSNotifySignalMsg_t, end) - IPC_PACKET_DATA_OFFSET)

/* connection oriented sockets data structs */
/* All these data structures are session control messages */
typedef struct tag_COCOConnRequestMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 cookie[4];
    UINT8 destSrvId[2];
    UINT8 mySrvId[2];
    UINT8 windowSize[1];
    UINT8 startSeqNum[1];
    UINT32 tick;        /* This is used by ipcaccept */
    UINT8 pad[1];
    UINT8 end;

} COCOConnRequestMsg_t;

#define COCO_CONN_REQUEST_DATA_SIZE \
    (offsetof(COCOConnRequestMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_COCOConnReplyMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 cookie[4];
    UINT8 peerSrvId[2];
    UINT8 newSrvId[2];
    UINT8 acceptConnect[1];    /* 1 for OK, 0 for refuse */
    UINT8 windowSize[1];
    UINT8 startSeqNum[1];
    UINT8 end;

} COCOConnReplyMsg_t;

#define COCO_CONN_REPLY_DATA_SIZE \
    (offsetof(COCOConnReplyMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_COCOConnCompleteMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 cookie[4];
    UINT8 destSrvId[2];
    UINT8 pad[1];
    UINT8 end;

} COCOConnCompleteMsg_t;

#define COCO_CONN_COMPLETE_DATA_SIZE \
    (offsetof(COCOConnCompleteMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_COCOConnDownMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 opcode[1];
    UINT8 cookie[4];
    UINT8 mySrvId[2];
    UINT8 destSrvId[2];
    UINT8 how[1]; /* one of IPCSD_READ, IPCSD_WRITE, IPCSD_BOTH */
    UINT8 pad[2];
    UINT8 end;

} COCOConnDownMsg_t;

#define COCO_CONN_DOWN_DATA_SIZE \
    (offsetof(COCOConnDownMsg_t, end) - IPC_PACKET_DATA_OFFSET)

typedef struct tag_COCOAckMsg_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    UINT8 wasReliable[1];
    UINT8 seqNum[1];
    UINT8 maxSeqNum[1];
    UINT8 pad[1];
    UINT8 end;

} COCOAckMsg_t;

#define COCO_ACK_DATA_SIZE \
    (offsetof(COCOAckMsg_t, end) - IPC_PACKET_DATA_OFFSET)

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

ipcPacket_t * ipcCreateControlPacket(UINT32 length, UINT8 opcode, ipcAddress_t destAddr,
                                     ipcServiceId_t destServiceId);

ipcPacket_t * ipcCopyPacket(ipcPacket_t *packet, int flags);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcCreateSessionControlPacket(length, opcode, destAddr) \
    ipcCreateControlPacket(length, opcode, destAddr, IPC_SRV_SESSIONCONTROL)

#define ipcCreateRouterControlPacket(length, opcode, destAddr) \
    ipcCreateControlPacket(length, opcode, destAddr, IPC_SRV_ROUTERCONTROL)

#define ipcCreateDeviceControlPacket(length, opcode, destAddr) \
    ipcCreateControlPacket(length, opcode, destAddr, IPC_SRV_DEVICECONTROL)

#ifdef __cplusplus
}
#endif

#endif /* IPC_MESSAGES_H */
