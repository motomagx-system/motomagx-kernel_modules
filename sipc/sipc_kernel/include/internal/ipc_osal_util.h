/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_osal_util.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.        Initial Creation
* 25May2007  Motorola Inc.        Added functions to write files
* 05Jul2007  Motorola Inc.        Changed ipc proc read function mechanism
* 13Jul2007  Motorola Inc.        Added checking for __cplusplus
*
*****************************************************************************************/
#ifndef _IPC_OSAL_UTIL_H_
#define _IPC_OSAL_UTIL_H_

#include "ipc_platform_defs.h"
#include <linux/kernel.h>

#define DFTPROCSIZE 8050

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tag_ipcProcElem_t
{
    char * procdata;
    /* the position where we reach */
    unsigned int pos;
    /* the data length in this element */
    unsigned int len;
    /* the total length in this element */
    unsigned int total;
    /* to avoid race condition */
    unsigned long mutex;
} ipcProcElem_t;


extern ipcProcElem_t ipc_virt_shm_map;
extern ipcProcElem_t ipc_phys_shm_map;
extern ipcProcElem_t ipc_stack_info;
extern char* dump_path;



int atoi(const char* str);

BOOL logInfo(ipcProcElem_t * procElem, const char* format, va_list args);

int getProcData(ipcProcElem_t *procElem, char *buf, size_t len, unsigned int offset);

int ipcPhyShmReport(const char * format, ...);

int ipcVirtShmReport(const char * format, ...);

int ipcStackReport(const char * format, ...);

void write_to_file(const char* filename, const char* data, unsigned int datalen);

int file_exists(const char* filename);

#ifdef __cplusplus
}
#endif


#define initProcElem(elem) do { \
(elem)->procdata = NULL; \
(elem)->len = (elem)->pos = (elem)->total = 0; \
} while(0)

#endif
