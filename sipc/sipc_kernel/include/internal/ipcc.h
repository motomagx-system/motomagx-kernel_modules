/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipcc.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Jan2007 Motorola Inc.         Initial Creation
* 13Jul2007  Motorola Inc.        Added checking for __cplusplus
*
*****************************************************************************************/
#include <linux/ioctl.h>

#define IPC_MAJOR 0   /* dynamic major by default */

#ifdef __cplusplus
extern "C" {
#endif

extern struct file_operations ipc_fops;
extern struct file_operations ipcsock_dev_fops;
extern struct file_operations ipcmem_dev_fops;

/*
 * The different configurable parameters
 */
extern int ipc_major;     /* main.c */

#ifdef __cplusplus
}
#endif

#ifndef min
#  define min(a,b) ((a)<(b) ? (a) : (b))
#endif

#define TYPE(dev) (MINOR(dev) >> 4)
#define NUM(dev) (MINOR(dev) & 0xf)


