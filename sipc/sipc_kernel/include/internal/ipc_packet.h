/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_packet.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   Defined the struct of packet as well as some operations.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 20Jan2005  Motorola Inc.         Initial Creation
* 16Feb2006  Motorola Inc.         Optionally calculate CRC for data
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation
* 15Jan2007  Motorola Inc.         Include ipc_macro_defs.h 
* 10Apr2007  Motorola Inc.         Remove the definition of ipcPrintMuPacket_t
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*
*****************************************************************************************/
#ifndef IPC_PACKET_H
#define IPC_PACKET_H

#include "ipc_macro_defs.h"
#include "ipc_platform_defs.h"
#include "ipc_osal.h"


#ifdef __cplusplus
extern "C" {
#endif
/*-------------------------------------- CONSTANTS -------------------------------------*/

#define IPC_PACKET_HEADER_CRC_BITS      16
#define IPC_PACKET_HEADER_CRC_BYTES     (IPC_PACKET_HEADER_CRC_BITS / 8)

/* IPC Packet Flags */
#define IPC_PACKET_HIGH_PRIORITY        0x01
#define IPC_PACKET_LIFO_PRIORITY        0x02
#define IPC_PACKET_RELIABLE             0x04
#define IPC_PACKET_CRC                  0x08
#define IPC_PACKET_CONTROL              0x10    /* control message */
#define IPC_PACKET_FIRST_ACKED          0x20

#define IPC_PACKET_ACK                  0xFF    /* ACK message */

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef struct tag_ipcHeader_t
{
    UINT32      msgLength;

    UINT16      srcServiceId;
    UINT16      destServiceId;

    UINT8       srcAddr;
    UINT8       destAddr;

    UINT8       seqNum;
    UINT8       flags;

    UINT16      dataChecksum;
    UINT16      checksum;

} ipcHeader_t;


typedef struct tag_ipcPacket_t
{
    struct tag_ipcPacket_t *next;
    /* NOTE: the field next2 is used to chain the packet in a list for
     * retransmission, we don't use the field "next" because the packet
     * may be already chained in another list */
    struct tag_ipcPacket_t *next2;

    UINT8           portId;
    UINT8           countDown;
    UINT8           retryTimes;
    UINT8           pad;

    ipcHeader_t     header;
    UINT8           data[1];
} ipcPacket_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#define IPC_PACKET_HEADER_OFFSET            offsetof(ipcPacket_t, header)
#define IPC_PACKET_DATA_OFFSET              offsetof(ipcPacket_t, data)
#define IPC_HEADER_SIZE                     (IPC_PACKET_DATA_OFFSET - IPC_PACKET_HEADER_OFFSET)

#define ipcGetPortId(packet)                ipcOsalRead8(&((ipcPacket_t*)(packet))->portId)
#define ipcGetCountDown(packet)             ipcOsalRead8(&((ipcPacket_t*)(packet))->countDown)
#define ipcGetRetryTimes(packet)            ipcOsalRead8(&((ipcPacket_t*)(packet))->retryTimes)
#define ipcGetMsgLength(packet)             ipcOsalRead32(&((ipcPacket_t *)(packet))->header.msgLength)
#define ipcGetSrcAddr(packet)               ipcOsalRead8(&((ipcPacket_t *)(packet))->header.srcAddr)
#define ipcGetDestAddr(packet)              ipcOsalRead8(&((ipcPacket_t *)(packet))->header.destAddr)
#define ipcGetSrcServiceId(packet)          ipcOsalReadAligned16(&((ipcPacket_t *)(packet))->header.srcServiceId)
#define ipcGetDestServiceId(packet)         ipcOsalReadAligned16(&((ipcPacket_t *)(packet))->header.destServiceId)
#define ipcGetSeqNum(packet)                ipcOsalRead8(&((ipcPacket_t *)(packet))->header.seqNum)
#define ipcGetMsgFlags(packet)              ipcOsalRead8(&((ipcPacket_t *)(packet))->header.flags)
#define ipcGetDataChecksum(packet)          ipcOsalRev16(ipcOsalReadAligned16(&((ipcPacket_t *)(packet))->header.dataChecksum))
#define ipcGetChecksum(packet)              ipcOsalRev16(ipcOsalReadAligned16(&((ipcPacket_t *)(packet))->header.checksum))
#define ipcGetMsgOpcode(packet)             ipcOsalRead8(&((ipcPacket_t *)(packet))->data[0] ) /* control message type */

#define ipcSetPortId(packet, val)           ipcOsalWrite8(&((ipcPacket_t*)(packet))->portId, val)
#define ipcSetCountDown(packet, val)        ipcOsalWrite8(&((ipcPacket_t*)(packet))->countDown, val)
#define ipcSetRetryTimes(packet, val)       ipcOsalWrite8(&((ipcPacket_t*)(packet))->retryTimes, val)
#define ipcSetMsgLength(packet, val)        ipcOsalWrite32(&((ipcPacket_t *)(packet))->header.msgLength, val)
#define ipcSetSrcAddr(packet, val)          ipcOsalWrite8(&((ipcPacket_t *)(packet))->header.srcAddr, val)
#define ipcSetDestAddr(packet, val)         ipcOsalWrite8(&((ipcPacket_t *)(packet))->header.destAddr, val)
#define ipcSetSrcServiceId(packet, val)     ipcOsalWriteAligned16(&((ipcPacket_t *)(packet))->header.srcServiceId, val)
#define ipcSetDestServiceId(packet, val)    ipcOsalWriteAligned16(&((ipcPacket_t *)(packet))->header.destServiceId, val)
#define ipcSetSeqNum(packet, val)           ipcOsalWrite8(&((ipcPacket_t *)(packet))->header.seqNum, val)
#define ipcSetMsgFlags(packet, val)         ipcOsalWrite8(&((ipcPacket_t *)(packet))->header.flags, val)
#define ipcSetDataChecksum(packet, val)     ipcOsalWriteAligned16(&((ipcPacket_t *)(packet))->header.dataChecksum, ipcOsalRev16(val))
#define ipcSetChecksum(packet, val)         ipcOsalWriteAligned16(&((ipcPacket_t *)(packet))->header.checksum, ipcOsalRev16(val))
#define ipcSetMsgOpcode(packet, val)        ipcOsalWrite8(&((ipcPacket_t *)(packet))->data[0], val) /* control message type */

#define ipcGetHeaderStart(packet)           ( (UINT8 *) &((ipcPacket_t *)(packet))->header )
#define ipcGetMsgData(packet)               ( ((ipcPacket_t *)(packet))->data )
#define ipcGetPacketTransmitSize(packet)    (ipcGetMsgLength(packet) + IPC_HEADER_SIZE)
#define ipcGetPacketStorageSize(packet)     (ipcGetMsgLength(packet) + IPC_PACKET_DATA_OFFSET)
#define ipcDataToPacket(data)               (ipcPacket_t *)((UINT8 *)(data) - IPC_PACKET_DATA_OFFSET)

#ifdef __cplusplus
}
#endif

#endif /* IPC_PACKET_H */
