/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2006 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: mu_channels.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header defines all MU Channel IDs.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 11Oct2005  Motorola Inc.         Initial Version
* 17Apr2006  Motorola Inc.         Enable runtime log config
*****************************************************************************************/
#ifndef MU_CHANNELS_H
#define MU_CHANNELS_H

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define MU_CHANNEL_BOOT0        0
#define MU_CHANNEL_BOOT1        1
#define MU_CHANNEL_SHM_INIT     2
#define MU_CHANNEL_SHM_CONTROL  3
#define MU_CHANNEL_SHM_PRINT    4
#define MU_CHANNEL_IPC_PACKET   5
#define MU_CHANNEL_LOGFLAG      6

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#endif /* MU_CHANNELS_H */
