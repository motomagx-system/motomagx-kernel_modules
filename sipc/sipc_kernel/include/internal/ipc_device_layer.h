/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2003, 2004 - 2006 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_device_layer.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains the main data structures and function prototypes used
*   internally by the device layer.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 02Jun2004  Motorola Inc.         Update to match IPC spec
* 01Sep2004  Motorola Inc.         Changes for acknowledged channels                      
* 13Jan2005  Motorola Inc.         Device Layer Optimization
* 18Sep2005  Motorola Inc.         Removed acknowledged frames
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
*****************************************************************************************/

#ifndef IPC_DEVICE_LAYER_H
#define IPC_DEVICE_LAYER_H

#include "ipc_common.h"
#include "ipc_list.h"
#include "ipc_device_interface.h"
#include "ipc_config.h"
#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define IPC_DEV_ESCAPE_BYTE             0x6b
#define IPC_DEV_FRAME_START_BYTE        0xb4
#define IPC_DEV_PACKET_START_BYTE       0x59

#define IPC_DEV_ESCAPE_LENGTH           2

#define IPC_FRAME_HEADER_CRC_BITS       16
#define IPC_FRAME_HEADER_CRC_BYTES      (IPC_FRAME_HEADER_CRC_BITS / 8)

/*------------------------------------ ENUMERATIONS ------------------------------------*/

typedef enum tag_ipcRxStreamState_t
{
    IPC_RX_FRAME_START,
    IPC_RX_NUM_CHANNELS,
    IPC_RX_FRAME_HEADER,
    IPC_RX_DATA
} ipcRxStreamState_t;

typedef enum tag_ipcRxPacketState_t
{
    IPC_RX_PACKET_START,
    IPC_RX_PACKET_LENGTH_BYTE0,
    IPC_RX_PACKET_LENGTH_BYTE1,
    IPC_RX_PACKET_LENGTH_BYTE2,
    IPC_RX_PACKET_LENGTH_BYTE3,
    IPC_RX_PACKET_HEADER,
    IPC_RX_PACKET_DATA
} ipcRxPacketState_t;


/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef struct tag_ipcFrameHeaderEntry_t
{
    ipcChannelId_t channelId;
    UINT8 length[2];
    UINT8 nextEntryData[1];

} ipcFrameHeaderEntry_t;


typedef struct tag_ipcFrameHeader_t
{
    UINT8 numChannels;
    UINT8 channelEntryData[1];

} ipcFrameHeader_t;

#define IPC_FRAME_HEADER_ENTRY_SIZE  ( offsetof(ipcFrameHeaderEntry_t, nextEntryData) )
#define IPC_MIN_FRAME_HEADER_SIZE  ( offsetof(ipcFrameHeader_t, channelEntryData) + IPC_FRAME_HEADER_CRC_BYTES )
#define IPC_MAX_FRAME_HEADER_SIZE  ( IPC_MIN_FRAME_HEADER_SIZE + (IPC_MAX_CHANNELS * IPC_FRAME_HEADER_ENTRY_SIZE) )


typedef struct tag_ipcRxChannel_t
{
    INT8 state;
    UINT16 frameBytesRemaining;
    UINT32 rxIndex;
    UINT32 packetLength;
    UINT8 *packet;

} ipcRxChannel_t;


typedef struct tag_ipcTxChannel_t
{
    /* include 'next' pointer so this struct can be added directly to txChannelList */
    struct tag_ipcTxChannel_t *next;

    ipcChannelId_t id;
    INT8 terminate;
    UINT32 txIndex;

    UINT16 bytesPerFrame;
    UINT16 frameBytesRemaining;
    
    UINT32 numBytesPending;
    UINT32 numBytesToSend;
    ipcList_t highPriorityPackets;
    ipcList_t lowPriorityPackets;
    char *currentPacket;
    
    ipcAddress_t finalAddr; /* FIXME: TEMP SOLUTION */
    ipcServiceId_t sourceService; /* FIXME: TEMP SOLUTION */
} ipcTxChannel_t;


typedef struct tag_ipcDeviceRxData_t
{
    ipcRxStreamState_t state;
    INT8 isEscapeSequence;
    UINT32 runningCrc;
    INT32 frameIndex;
    INT32 channelIndex;
    ipcChannelId_t currentChannelId;
    ipcRxChannel_t channels[IPC_MAX_CHANNELS];
    UINT8 frameHeader[IPC_MAX_FRAME_HEADER_SIZE];
    INT32 frameLength;

} ipcDeviceRxData_t;
    

typedef struct tag_ipcDeviceTxData_t
{
    ipcChannelId_t nextChannelId;
    INT32 totalBytesPerFrame;
    INT32 availableBytesPerFrame;
    ipcList_t channelList;
    ipcTxChannel_t *channelPointers[IPC_MAX_CHANNELS];
    ipcChannelId_t frameChannelIds[IPC_MAX_CHANNELS + 1];
    UINT8 frameHeader[IPC_MAX_FRAME_HEADER_SIZE];
    INT32 frameHeaderLength;
    UINT32 runningCrc;
    
#ifdef IPC_BUFFER_TX     
    UINT8 buffer[IPC_DEV_BUFFER_SIZE];
    INT32 index;
#endif

} ipcDeviceTxData_t;


typedef struct tag_ipcDevice_t
{
    ipcPortId_t portId;       
    INT32 totalBandwidth;
    INT32 framesPerSecond;
    ipcDeviceDriver_t driver;
    BOOL terminate;
    BOOL freePackets;

    ipcDeviceRxData_t rx;
    ipcDeviceTxData_t tx;

    ipcMutexHandle_t mutex;
    ipcSemaHandle_t waitForOutputSema;

} ipcDevice_t;


/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern const UINT8 IPC_DEV_ESCAPE_DATA[IPC_DEV_ESCAPE_LENGTH];
extern const UINT8 IPC_DEV_ESCAPE_START_FRAME[IPC_DEV_ESCAPE_LENGTH];
extern const UINT8 IPC_DEV_ESCAPE_START_PACKET[IPC_DEV_ESCAPE_LENGTH];

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void ipcCloseTxChannelImmediately(ipcDevice_t *device, ipcTxChannel_t *channel);
void ipcFreeTxChannelPackets(ipcTxChannel_t *channel);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcGetDevice(portId)            ( (portId) < IPC_MAX_PORTS ? \
                (ipcDevice_t *)ipcStack_glb.ports[(int)portId].device : (ipcDevice_t *)NULL )

#define ipcLockDevice(device)           ipcOsalMutexLock(device->mutex)

#define ipcUnlockDevice(device)         ipcOsalMutexRelease(device->mutex)


#define ipcGetFrameHeaderEntry(frame, index)  ((ipcFrameHeaderEntry_t *) \
    ( ((ipcFrameHeader_t *)(frame))->channelEntryData + (index * IPC_FRAME_HEADER_ENTRY_SIZE) ) )

#define ipcGetFrameHeaderLength(frame)  (IPC_MIN_FRAME_HEADER_SIZE + \
    (ipcGetFrameNumChannels(frame) * IPC_FRAME_HEADER_ENTRY_SIZE))

#define ipcGetFrameNumChannels(frame)  ( ((ipcFrameHeader_t *)(frame))->numChannels )

#define ipcGetFrameChannelId(frame, index)  ( ipcGetFrameHeaderEntry(frame, index)->channelId )

#define ipcGetFrameChannelLength(frame, index) \
    ipcOsalRead16( ipcGetFrameHeaderEntry(frame, index)->length )


#define ipcSetFrameNumChannels(frame, num)  ((ipcFrameHeader_t *)(frame))->numChannels = (num)

#define ipcSetFrameChannelId(frame, index, id)  ipcGetFrameHeaderEntry(frame, index)->channelId = (id)

#define ipcSetFrameChannelLength(frame, index, len) \
    ipcOsalWrite16( ipcGetFrameHeaderEntry(frame, index)->length, len )


#ifdef __cplusplus  /* Allow header file to be included in a C++ file */
}
#endif /* __cplusplus */

#endif /* IPC_DEVICE_LAYER_H */
