/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_platform_shared_memory.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains platform specific constants for shared memory on Linux.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 16Aug2005  Motorola Inc.        Initial Version
* 11Jan2006  Motorola Inc.        Add function ipcOpenMemLock()
* 17Jul2006  Motorola Inc.        Refactored ipc folder structure  for open source        
* 20Feb2007  Motorola Inc.        Updated for common memory alloc
* 13Jul2007  Motorola Inc.        Added checking for __cplusplus
*
*****************************************************************************************/
#ifndef IPC_PLATFORM_SHARED_MEMORY_H
#define IPC_PLATFORM_SHARED_MEMORY_H
#include "ipc_platform_defs.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/

#define LOCAL_CORE_ID           LINUX_CORE_ID
#define REMOTE_CORE_ID          RTXC_CORE_ID

#ifdef __cplusplus
extern "C"
#endif
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void ipcInitMemory(void);

unsigned long ipcSMVirtualToUser(unsigned long vaddr);
unsigned long ipcSMUserToVirtual(unsigned long ubuffer);
BOOL ipcShmPtrUserValidate(unsigned long uaddr);

#ifdef __cplusplus
}
#endif

/*--------------------------------------- MACROS ---------------------------------------*/

/* The following macros convert between physical and virtual addresses on Linux */
#define ipcSMPhysicalToVirtual(ptr)     ((void *)((ptr) ? ((char *)(ptr) + ipcSMOffset) : 0))
#define ipcSMVirtualToPhysical(ptr)     ((void *)((ptr) ? ((char *)(ptr) - ipcSMOffset) : 0))


#endif /* IPC_PLATFORM_SHARED_MEMORY_H */
