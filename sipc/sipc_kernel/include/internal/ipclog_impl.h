/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipclog_impl.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
* 18Jul2007  Motorola Inc.         Added ipcTrace funcition
*****************************************************************************************/
#ifndef IPCLOG_IMPL_H
#define IPCLOG_IMPL_H
#include "ipc_macro_defs.h"

#if !defined(IPC_RELEASE) && defined(CONFIG_MOT_FEAT_LTT_LITE)
#include <linux/ltt-events.h>

#define SIZE_SIPCLTT_STRING   30

typedef enum
{
    SIPCENTRYLOG = 0,
    SIPCEXITLOG,
    SIPCLTTNUM
}SIPC_LTT_LOGTYPE;

typedef enum
{    
    SIPC_FIRST_TYPE_ID = 0,
    IPC_MAIN_ID,

    // Add function ID
    IPC_SOCKET_ID, 
    IPC_BIND_ID, 
    IPC_GETSOCKETNAME_ID,
    IPC_GETPEERNAME_ID,
    IPC_SHUTDOWN_ID, 
    IPC_SETSOCKQLEN_ID,
    IPC_CLOSESOCKET_ID,
    IPC_CHAN_ID,
    IPC_SETWATERMARK_ID,
    IPC_LOCATESERVICE_ID,
    IPC_NOTIFY_ID,
    IPC_PSNOTIFY_ID,
    IPC_SENDTO_ID,
    IPC_ZSENDTO_ID,
    IPC_RECVFROM_ID,
    IPC_ZRECVFROM_ID,
    IPC_LISTEN_ID,
    IPC_ACCEPT_ID,
    IPC_CONNECT_ID,
    IPC_ZSEND_ID,
    IPC_ZRECV_ID,
    IPC_SEND_ID,
    IPC_RECV_ID,
    IPC_ZALLOC_ID,
    IPC_ZFREE_ID,
    IPC_ZCALLOC_ID,
    IPC_GETMEMTYPE_ID,

    SIPC_LAST_TYPE_ID
}SIPC_LOG_ID_T;


extern char sipc_logID[SIPC_LAST_TYPE_ID+1][SIZE_SIPCLTT_STRING];
#endif

#define ipcLogFlush()
#define ipcLogBegin(type, cat)
#define ipcTimestamp(print)


#ifdef __KERNEL__

#define ipcPrintFunc(fmt, arg...)   printk(KERN_ALERT "IPC: " fmt, ##arg)
#define ipcLogFunc(fmt, arg...)     printk(KERN_DEBUG "IPC: " fmt, ##arg)

#if !defined(IPC_RELEASE) && defined(CONFIG_MOT_FEAT_LTT_LITE)
void ipcTraceFunc(int status, int type);
#else
#define ipcTraceFunc(status,type)
#endif

#else

#define ipcPrintFunc                printf
#define ipcLogFunc(fmt, arg...)

#endif

#endif /* IPCLOG_IMPL_H */
