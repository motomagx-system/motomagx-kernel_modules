/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2005 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_router_interface.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file defines structures used by the router.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation
* 09Feb2005  Motorola Inc.         Updated for sockets 
*****************************************************************************************/

#ifndef IPC_ROUTER_INTERFACE_H
#define IPC_ROUTER_INTERFACE_H

#include "ipc_common.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

BOOL ipcRouterInit(void);

void ipcRouterDestroy(void);

ipcChannelId_t ipcRouterGetPendingChannelId(void);

int SSRTDataRequest(ipcPacket_t *packet, ipcChannelId_t channelId);

int SSRTChannelRequest(ipcAddress_t ipcAddr, ipcQoS_t QoS, 
                       ipcServiceId_t srcService);

int SSRTChannelRelRequest(ipcChannelId_t channelId, ipcAddress_t ipcAddr,
                          ipcServiceId_t srcService);

void DVRTDataIndication(ipcPacket_t *packet, ipcPortId_t portId);

void handleDVRTDataIndication(ipcPacket_t *packet);

int ipcRouterGetMemType(ipcAddress_t destAddr);

BOOL ipcRouterOpenConnection(ipcPortId_t portId, void *device, ipcDeviceType_t deviceType);

void ipcRouterCloseConnection(ipcPortId_t portId);

/*--------------------------------------- MACROS ---------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif /* IPC_ROUTER_INTERFACE_H */
