/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005, 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_packet_log_internal.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains structures and function prototypes used internally for
*   logging IPC Packets.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 26Jul2005  Motorola Inc.         Initial Creation
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*
*****************************************************************************************/

#ifndef IPC_PACKET_LOG_INTERNAL_H
#define IPC_PACKET_LOG_INTERNAL_H

#include "ipc_common.h"
#include "ipc_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define IPC_MIN_LOG_PACKET_LEN      offsetof(ipcLogPacketMsg_t, data)

/* Mask off the timestamp flag */
#define IPC_LOG_ACTION_MASK         0x0000FFFF

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
typedef struct tag_ipcLogFilterEntry_t
{
    ipcAddress_t srcAddr;
    ipcAddress_t destAddr;
    ipcServiceId_t srcService;
    ipcServiceId_t destService;
    long    action;
} ipcLogFilterEntry_t;


typedef struct tag_ipcPacketLogConfig_t
{
    int numEntries;
    ipcAddress_t loggerAddr;
    ipcServiceId_t loggerServiceId;
    int memtype;

    ipcLogFilterEntry_t filters[IPC_LOG_MAX_FILTERS];

} ipcPacketLogConfig_t;


typedef struct tag_ipcLogPacket_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    ipcLogPacketMsg_t data;

} ipcLogPacket_t;


typedef struct tag_ipcLogConfigPacket_t
{
    UINT8 header[IPC_PACKET_DATA_OFFSET];
    ipcLogConfigMsg_t data;

} ipcLogConfigPacket_t;


/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern ipcPacketLogConfig_t ipcPacketLog_glb;

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void ipcLogPacketImp(ipcPacket_t *packet);
int ipcHandleLogConfigRequest(ipcLogConfigPacket_t *packet);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcLogPacket(packet)    if (ipcPacketLog_glb.numEntries) ipcLogPacketImp(packet) 

#ifdef __cplusplus
}
#endif

#endif /* IPC_PACKET_LOG_INTERNAL_H */
