/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005, 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_sock_impl.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains the main data structures and function prototypes related
*   with the socket API implementation in session layer.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 10Jan2005  Motorola Inc.        Initial Creation
* 04Jun2007  Motorola Inc.        Removed redundant #ifndef linux
*
*****************************************************************************************/
#ifndef IPC_SESSION_SOCK_IMPL_H
#define IPC_SESSION_SOCK_IMPL_H

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_defs.h"
#include "ipc_session.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

int ipcgetstatus_imp(int* lasterror);

int ipcgetversion_imp(unsigned long *version, int* lasterror);


int ipclocateservice_imp(unsigned long ipc_node,
                         unsigned short ipc_service,
                         struct IPCSERVICE_LOCATION *ipc_servicetable,
                         unsigned long *ipc_sizeofservicetable,
                         unsigned long ipc_maxsizeofservicetable,
                         int *lasterror);
                     
int ipcsetwatermark_imp(ipcSocket_t* sock,
                        unsigned char ipc_lowwmark,
                        unsigned char ipc_highwmark,
                        int *lasterror);

/*  Socket and channel management   */
ipcSocket_t *ipcsocket_imp(int af,
                           int type,
                           int protocol,
                           int *lasterror);

int ipcbind_imp(ipcSocket_t *sock,
                const struct IPCSOCK_ADDR *name,
                int namelen,
                int *lasterror);

int ipcgetsockname_imp(ipcSocket_t *sock,
                       struct IPCSOCK_ADDR *name,
                       int *namelen,
                       int *lasterror);

int ipcgetpeername_imp(ipcSocket_t *sock,
                       struct IPCSOCK_ADDR *name,
                       int *namelen,
                       int *lasterror);

int ipcshutdown_imp(ipcSocket_t *sock,
                    int how,
                    int* lasterror);

int ipcsetsockqlen_imp(ipcSocket_t *sock,
                       int queueMaxLen,
                       int *lasterror);

int ipcchan_imp(ipcSocket_t* socket, 
            int qos,
            int flag,
            int *lasterror);

int ipcclosesocket_imp(ipcSocket_t *sock,
                       int *lasterror);

int ipczsendto_imp(ipcSocket_t *sock,
                   const char *zbuffer,
                   int leng,
                   int flags,
                   const struct IPCSOCK_ADDR *to,
                   int tolen,
                   int *lasterror);

int ipczrecvfrom_imp(ipcSocket_t *sock,
                     char **zbuffer,
                     int flags,
                     struct IPCSOCK_ADDR *from,
                     int *fromlen,
                     int *lasterror);

/* Attach an attribute to the socket */
int ipcregnotifydata_imp(ipcSocket_t* sock,
                         void* data,
                         int *lasterror);

int ipclisten_imp(ipcSocket_t* socket, 
                  int backlog, 
                  int* lasterror);

ipcSocket_t* ipcaccept_imp(ipcSocket_t* socket, 
                           struct IPCSOCK_ADDR *addr, 
                           int *addrlen, 
                           int *lasterror);

int ipcconnect_imp(ipcSocket_t* socket, 
               struct IPCSOCK_ADDR *addr, 
               int addrlen,
               int *lasterror);

int ipczsend_imp(ipcSocket_t* socket, 
             const char *zbuffer, 
             int len, 
             int flags, 
             int *lasterror);

int ipczrecv_imp(ipcSocket_t *sock,
                 char **zbuffer,
                 int flags,
                 int *lasterror);

int ipcnotify_imp(ipcSocket_t *sock,
                  const struct IPCSOCK_ADDR* name,
                  int namelen,
                  const char * msg,
                  int msglen,
                  int flags,
                  int *lasterror);

int ipcpsnotify_imp(void *q,
                    int qflag,
                    const struct IPCSOCK_ADDR* name,
                    int namelen,
                    const char * msg,
                    int msglen,
                    int flags,
                    int *lasterror);

int ipcgetmemtype_imp(const struct IPCSOCK_ADDR* ipc_address,
                  unsigned long* ipc_memtype,
                  int *lasterror);
/*--------------------------------------- MACROS ---------------------------------------*/

#ifdef __cplusplus
}
#endif

#endif /* IPC_SESSION_SOCK_IMPL_H */
