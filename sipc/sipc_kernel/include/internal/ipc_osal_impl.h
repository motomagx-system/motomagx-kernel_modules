/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_osal_impl.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Jan2007 Motorola Inc.         Initial Creation
* 09Jul2007 Motorola Inc.         Removed legacy support
* 13Jul2007  Motorola Inc.        Added checking for __cplusplus
*****************************************************************************************/
#ifndef IPC_OSAL_IMPL_H
#define IPC_OSAL_IMPL_H

#include <linux/module.h>
#include <linux/types.h>
#include <linux/stddef.h>
#include <linux/mm.h>
#include <linux/version.h>
#include <linux/vmalloc.h>
#include <linux/time.h>
#define MAX_SEMA_NUMBER     32767
#define IPC_OSAL_INFINITE   -1

#ifdef __cplusplus
extern "C" {
#endif

extern struct proc_dir_entry *ipc_root;

typedef struct tag_ipcSemaphore_t
{
    volatile int status;
    struct semaphore sem;
}ipcSemaphore_t, *ipcSemaHandle_t, *ipcMutexHandle_t;

typedef struct ipcTimer_t
{
    void (*callback)(void*);
    void* param;
    unsigned long expires;
    struct timer_list sys_timer;
    struct work_struct work; /* In 2.6.x, timers are executed in a workqueue */
} ipcTimer_t, *ipcTimerHandle_t;

#define MS_TO_JIFFIES(x) msecs_to_jiffies(x)
#define JIFFIES_TO_MS(x) jiffies_to_msecs(x)

/* virtual shared memory size
 * default virtual shared memory size is 4096K */
#define POOL_SIZE 4096

/* In 2.6 kernel, the traditional MODULE_INC_USE_COUNT is deprecated */
#define get_ipc_module() try_module_get(THIS_MODULE)
#define put_ipc_module() module_put(THIS_MODULE)
#define ipc_string_param(name) module_param(name, charp, S_IRUGO)
#define ipc_int_param(name) module_param(name, int, S_IRUGO)
#define ipc_short_param(name) module_param(name, short, S_IRUGO)
#define ipc_ulong_param(name) module_param(name, ulong, S_IRUGO)

#define SHM_ALLOC_BYTES(bytes) __vmalloc(bytes, GFP_KERNEL, PAGE_SHARED)
#define SHM_ADDR_TO_PAGE(addr) vmalloc_to_page(addr)
#define SHM_FREE_MEMPOOL(pool,bytes) vfree(pool)

/* Memory access macros (little endian to little endian - no conversion needed) */
#define ipcOsalRead8(addr)                      ( *((unsigned char  *)(addr)) )
#define ipcOsalReadAligned16(addr)              ( *((unsigned short *)(addr)) )
#define ipcOsalReadAligned32(addr)              ( *((unsigned long  *)(addr)) )

#define ipcOsalWrite8(addr, val)                ( *((unsigned char  *)(addr)) = ((unsigned char )(val)) )
#define ipcOsalWriteAligned16(addr, val)        ( *((unsigned short *)(addr)) = ((unsigned short)(val)) )
#define ipcOsalWriteAligned32(addr, val)        ( *((unsigned long  *)(addr)) = ((unsigned long )(val)) )

#ifdef __cplusplus
}
#endif

#endif /* !defined IPC_OSAL_IMPL_H */

