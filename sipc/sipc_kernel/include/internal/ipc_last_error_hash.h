/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005, 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_last_error_hash.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file contains prototypes for a platform independent method of storing
*   and retreiving an error code in a thread-safe manner.  It should only be
*   used if the platform does not already support this functionality.  For example,
*   Windows already has equivalent SetLastError and GetLastError functions.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation
* 29Jan2007  Motorola Inc.         Header files change for GPL 
*****************************************************************************************/
#ifndef IPC_LAST_ERROR_HASH_H
#define IPC_LAST_ERROR_HASH_H

#include "ipc_platform_defs.h"
#include "ipc_list.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif


/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

#define ipcOpenErrorTable()     TRUE
#define ipcCloseErrorTable()
#define ipcSetLastError(x)
#define ipcClearLastError()
#define ipcGetLastErrorImp()  0

/*--------------------------------------- MACROS ---------------------------------------*/


#ifdef __cplusplus
}
#endif

#endif /* IPC_LAST_ERROR_HASH */
