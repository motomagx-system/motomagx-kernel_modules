/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_list.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file contains definitions, macros, and function prototypes used by
*   the IPC list.  For details on the usage of the IPC list, see ipc_list.c
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 02Jun2004  Motorola Inc.         Initial Creation
* 26Jul2004  Motorola Inc.         Add ipcListGetAtIndex
* 14Sep2004  Motorola Inc.         Added ipcListGet/SetPosition
* 22Sep2004  Motorola Inc.         Removed ipcListGetAtIndex
* 23Dec2004  Motorola Inc.         Removed list iterator 
*****************************************************************************************/

#ifndef IPC_LIST_H
#define IPC_LIST_H

#include "ipc_platform_defs.h"


#ifdef __cplusplus
extern "C" {
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef struct tag_ipcListItem_t
{
    struct tag_ipcListItem_t *next;
    void *data;
} ipcListItem_t, *ipcListLink_t;


typedef struct tag_ipcList_t
{
    ipcListLink_t head;
    ipcListLink_t tail;
    INT32 size;
} ipcList_t;


/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

typedef BOOL (*ipcListCompareFunction_t)(ipcListLink_t link, void *key);

void ipcListInsertLink(ipcList_t *list, ipcListLink_t link, ipcListLink_t prev);
void ipcListAddHead(ipcList_t *list, ipcListLink_t newHead);
void ipcListAddTail(ipcList_t *list, ipcListLink_t newTail);
ipcListLink_t ipcListRemoveNextLink(ipcList_t *list, ipcListLink_t prev);
ipcListLink_t ipcListRemoveHead(ipcList_t *list);

ipcListLink_t ipcListInsertObject(ipcList_t *list, void *obj, ipcListLink_t prev);
ipcListLink_t ipcListAddObjectToHead(ipcList_t *list, void *obj);
ipcListLink_t ipcListAddObjectToTail(ipcList_t *list, void *obj);
void *ipcListRemoveNextObject(ipcList_t *list, ipcListLink_t prev);
void *ipcListRemoveObjectFromHead(ipcList_t *list);

ipcListLink_t ipcListFind(ipcList_t *list, void *key, ipcListCompareFunction_t compare,
                          ipcListLink_t *prev);

ipcListLink_t ipcListRemove(ipcList_t *list, void *key, ipcListCompareFunction_t compare);
BOOL ipcListRemoveObject(ipcList_t *list, void *object);

void ipcListFreeAll(ipcList_t *list);
void ipcListAppendList(ipcList_t *toList, ipcList_t *fromList);

/* comparison functions used by the find macros below */
BOOL ipcListCompareLink(ipcListLink_t link, void *key);
BOOL ipcListCompareObject(ipcListLink_t link, void *key);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcListInit(list)                       (list)->head = (list)->tail = NULL,  (list)->size = 0

#define ipcListFirst(list, curr)             (curr) = (list).head
#define ipcListNext(curr)                    (curr) = (curr)->next

#define ipcListFirstPair(list, prev, curr)   (prev) = NULL,  (curr) = (list).head
#define ipcListNextPair(prev, curr)          (prev) = (curr),  (curr) = (curr)->next

#define ipcListFindLink(list, link, prevPtr)    ipcListFind(list, (void *)link, ipcListCompareLink, prevPtr)
#define ipcListFindObject(list, obj, prevPtr)   ipcListFind(list, (void *)obj, ipcListCompareObject, prevPtr)

#define ipcListRemoveLink(list, link)           ipcListRemove(list, (void *)link, ipcListCompareLink)

#ifdef __cplusplus
}
#endif

#endif /* IPC_LIST_H */
