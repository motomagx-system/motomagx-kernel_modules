/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_mu_shm.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains function prototypes and definitions for using the MU
*   to transfer SmartIPC packets and other data through physical shared memory.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 11Oct2005  Motorola Inc.         Initial Version
* 23Nov2005  Motorola Inc.         Change struct ipcMulticoreSema_t for BIG-16 endianness
* 05Jan2006  Motorola Inc.         Bug fix for MulticoreSema race condition
* 06Feb2006  Motorola Inc.         Add function muShmExit()
* 15Mar2006  Motorola Inc.         Pass in multicore sema address
* 10Apr2007  Motorola Inc.         Add circular buffer for BP log in multicore sema
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*****************************************************************************************/
#ifndef IPC_MU_SHM_H
#define IPC_MU_SHM_H

#include "mu_channels.h"
#include "ipc_shared_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define MU_SHM_SEMA_SIGNAL          0
#define MU_SHM_LOCK                 1
#define MU_SHM_UNLOCK               2
#define MU_SHM_LOCK_ACK             3

#ifndef IPC_RELEASE
#define MU_LOG_BUF_SIZE             256
#define MU_LOG_BUF_NUM              20
#endif

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

/* Multi-core Semaphore */
typedef struct tag_ipcMulticoreSema_t
{
    volatile short coreWantsEntry[SM_NUM_CORES];
    volatile short turn;
    
    volatile short semaCount;
    volatile short notificationRequest;

    /* round up to 4-bytes boundry */
    volatile short filler;

#ifndef IPC_RELEASE
    char muLogBuf[MU_LOG_BUF_NUM][MU_LOG_BUF_SIZE];   
#endif
} ipcMulticoreSema_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern ipcMulticoreSema_t *ipcMulticoreSema;

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

void muShmInit(void);
void muShmExit(void);
int muWriteIpcPacket(void *packet);
void muShmLock(void);
void muShmUnlock(void);
void ipcMulticoreSemaOpen(void *multicoreSemaAddr);
void ipcMulticoreSemaClose(void);
void ipcMulticoreSemaWait(void);
void ipcMulticoreSemaSignal(void);

/*--------------------------------------- MACROS ---------------------------------------*/

/* Peterson's Algorith for mutual exclusion across cores */
#define petersonEntry() \
    ipcMulticoreSema->coreWantsEntry[LOCAL_CORE_ID] = 1; \
    ipcMulticoreSema->turn = REMOTE_CORE_ID; \
    while (ipcMulticoreSema->coreWantsEntry[REMOTE_CORE_ID] && \
           (ipcMulticoreSema->turn == REMOTE_CORE_ID)) { /* Spin for lock */ }
    
#define petersonExit() \
    ipcMulticoreSema->coreWantsEntry[LOCAL_CORE_ID] = 0;

#ifdef __cplusplus
}
#endif

#endif /* IPC_MU_SHM_H */
