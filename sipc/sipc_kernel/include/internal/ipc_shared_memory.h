/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_shared_memory.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains the main data structures and function prototypes used
*   internally by the shared memory driver.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Sep2004  Motorola Inc.         Initial Version
* 23May2005  Motorola Inc.         Removed IPC includes
* 09Jun2005  Motorola Inc.         Modified data structures for Linux
* 02Nov2005  Motorola Inc.         Add function ipcSMFinalizePools()
* 20Dec2005  Motorola Inc.         Add mem config structure
* 19Jan2006  Motorola Inc.         Change shared memory addr & size
* 03Mar2006  Motorola Inc.         Change shared memory addr
* 16Feb2005  Motorola Inc.         Bug fix. ipczalloc() will never return if it tries to allocate a block of physical shared memory with size more than max buffer size.
* 15Mar2006  Motorola Inc.         Allow single core to initialize  ALL shared memory (both cores)
* 12Apr2006  Motorola Inc.         Bug fix for wrong maxPSMBufSize
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 19Sep2006  Motorola Inc.         ipc_share_memory.h include  ipc_platform_defs.h will cause  layer1 build error
* 08Oct2006  Motorola Inc.         chang TRUE to 1
* 11Oct2006  Motorola Inc.         change buf header size to 4 bytes
* 15Jan2007  Motorola Inc.         Include ipc_macro_defs.h
* 23Feb2007  Motorola Inc.         Unify memory allocation functions
* 05Jul2007  Motorola Inc.         Removed function ipcSMGetTailMark
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*****************************************************************************************/
#ifndef IPC_SHARED_MEMORY_H
#define IPC_SHARED_MEMORY_H

#include "ipc_macro_defs.h"
#include "ipc_platform_shared_memory.h"

#ifdef __cplusplus
extern "C" {
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/

/*------------ Buffer header structure -----------------------------------------
  byte 1:  Marker
  byte 2:  Reserved (7), Multicore (6), Pool Owner (5:4), Memory Type (3:0)
  byte 3:  Pool ID
  byte 4:  Reference Count
Total size is 4 bytes if IPC_RELEASE is defined, otherwise 16 bytes with fields:
  byte  5-6 : process ID
  byte  7-10: tick
  byte 11-14: size requested by user
  byte 15-16: header marker again
  note: the size of header is 8-byte aligned as bp spl requires 8-bytes aligned addr
------------------------------------------------------------------------------*/

#ifdef IPC_RELEASE
#define BUFHEAD_NUM_WORDS           2
#define BUFTAIL_NUM_BYTES           0
#else
#define BUFHEAD_NUM_WORDS           8
#define BUFHEAD_PID_INDEX           2
#define BUFHEAD_TK1_INDEX           3
#define BUFHEAD_TK2_INDEX           4
#define BUFHEAD_SZ1_INDEX           5
#define BUFHEAD_SZ2_INDEX           6
#define BUFHEAD_MARKER2_INDEX       7
#define BUFTAIL_NUM_BYTES           2
#endif

#define BUFHEAD_MARKER_INDEX        0
#define BUFHEAD_MULTICORE_INDEX     0
#define BUFHEAD_OWNER_INDEX         0
#define BUFHEAD_MEMTYPE_INDEX       0
#define BUFHEAD_POOLID_INDEX        1
#define BUFHEAD_REFCOUNT_INDEX      1


#define BUFHEAD_MARKER_OFFSET       8
#define BUFHEAD_MULTICORE_OFFSET    6
#define BUFHEAD_OWNER_OFFSET        4
#define BUFHEAD_MEMTYPE_OFFSET      0
#define BUFHEAD_POOLID_OFFSET       8
#define BUFHEAD_REFCOUNT_OFFSET     0

#define BUFHEAD_MARKER_MASK         0xFF00
#define BUFHEAD_MULTICORE_MASK      0x0040
#define BUFHEAD_OWNER_MASK          0x0030
#define BUFHEAD_MEMTYPE_MASK        0x000F
#define BUFHEAD_POOLID_MASK         0xFF00
#define BUFHEAD_REFCOUNT_MASK       0x00FF

/* Pool Owner IDs */
#define LINUX_CORE_ID               0
#define RTXC_CORE_ID                1

#define SM_NUM_CORES                2
#define MAX_SM_POOLS                25
#define BUFHEAD_MARKER              0xAA  /* binary 10101010 */
#define BUFRED_MARKER               0xA5A5

/* Special value written to shared memory by AP to indicate to BP that it is present */
#define AP_BP_MAGIC_FLAG            0xBE16

/* Used to mask off memtype from ipcAlloc flags param */
#define MEMTYPE_FLAG_MASK           0x0000000F

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

/* buffer head structure */
typedef struct tag_ipcSMBufHead_t
{
    unsigned short words[BUFHEAD_NUM_WORDS];
} ipcSMBufHead_t;
 
typedef struct tag_ipcSMBufTail_t
{
    unsigned char mark[2];
} ipcSMBufTail_t;


/* Pool description Table Structure */
typedef struct tag_ipcSMDesTbl_t
{
    unsigned long       bufSize;
    unsigned long       numBuffers;
    ipcSMBufHead_t **   freeBufWPtr;
    ipcSMBufHead_t **   freeBufRPtr;
    void *              pointerStartAddress;
    void *              bufferStartAddress;
} ipcSMDesTbl_t;

/* define SM pool table property */
typedef struct tag_SMPoolTblProp_t
{
    unsigned long   ownerID;            /* local owner ID for this pool table */
    unsigned long   SMBytesTotal;       /* total number of SM bytes for this core */
    unsigned long   SMBytesRemaining;   /* the number of bytes remaining in SM */
    unsigned long   totalNumOfPools;    /* total number of pools open */
    void *          nextPoolStart;      /* pointer to next pool starting point */
    unsigned long   maxPSMBufSize;      /* max physical shared memory buffer size */
    ipcSMDesTbl_t   poolDesTables[MAX_SM_POOLS];
} SMPoolTblProp_t;

/* Structure used to configure shared memory pools */
typedef struct tag_ipcSMPoolConfig_t
{
    unsigned long   bufSize;
    unsigned long   numBuffers;
} ipcSMPoolConfig_t;

/* Initialization message from AP to BP */
typedef struct tag_ipcSMInitMessage_t
{
    void *multicoreSema;
    void *masterPoolTable;
    void *slavePoolTable;
} ipcSMInitMessage_t;

/* Flags set by AP to tell BP how to initialize */
typedef struct tag_BPInitFlags_t
{
    unsigned short  apIsPresent;
    unsigned short  bpShouldHalt;
} BPInitFlags_t;    

typedef int (*ipcSMPrintFunction_t)( const char*, ...);

/*------------------------------------- GLOBAL DATA ------------------------------------*/

extern SMPoolTblProp_t *localSMPoolTblProp;
extern SMPoolTblProp_t *remoteSMPoolTblProp;
extern long ipcSMOffset;
extern int ipcIsShmReady;
extern int shmConnected;

/* For BP-standalone builds */
extern ipcSMPoolConfig_t localSMPoolConfig[];

/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/

#ifdef HAVE_PHYSICAL_SHM

void ipcSMPoolTableInit(SMPoolTblProp_t *p, unsigned long ownerID,
                        unsigned long SMBytesTotal, ipcSMPoolConfig_t *poolConfig);

int ipcSMCalculatePoolTableSize(ipcSMPoolConfig_t *poolConfig);

int ipcCreateSMPool(SMPoolTblProp_t *p, unsigned long bufSize,
                    unsigned long numBuffers, unsigned long ownerID);
                              
void ipcSMFinalizePools(SMPoolTblProp_t *p, unsigned long ownerId);

void *ipcGetSMBuffer(unsigned int bufSize, unsigned int flags);

int ipcFreeSMBuffer(void *pBufHead);

int ipcSMReport(SMPoolTblProp_t *p, ipcSMPrintFunction_t printFn);
unsigned long ipcGetMaxPSMBufferSize(void);

/* platform-specific functions */

void initPlatformSharedMemory(int shmOffset, void *shmAddr, int shmSize,
                              ipcSMPoolConfig_t *poolConfig);

void ipcHandleSMInitMessage(int channel, int data);

#endif /* HAVE_PHYSICAL_SHM */


int ipcIsPhysicalSharedMemory(void *ptr);

/* memory overrun detection functions */
/* debug features will be disabled if IPC_RELEASE is defined */

#if defined(IPC_RELEASE)
#define ipcSMCheckBuffer(ptr,caller)   isBufferValid(ptr)
#else /* enable debug features */
int ipcSMCheckBuffer(char *ptr, const char *caller);
void ipcSMSetDebugInfo(ipcSMBufHead_t *buffer, int usrSize, unsigned short pid);
#endif

/*--------------------------------------- MACROS ---------------------------------------*/

/* Total number of bytes required with buffer header and buffer tail, aligned to 4-byte boundary */
#define ipcSMAllocSize(bytes)   ((sizeof(ipcSMBufHead_t) + (bytes) + BUFTAIL_NUM_BYTES + 3) & (~3))

#define ipcSMReadBufheadField(buffer, index, mask, offset) \
    (( ((ipcSMBufHead_t *)(buffer))->words[index] & mask) >> offset)

#define ipcSMWriteBufheadField(buffer, index, mask, offset, value) \
    ((ipcSMBufHead_t *)(buffer))->words[index] = (unsigned short) \
    ((((ipcSMBufHead_t *)(buffer))->words[index] & ~mask) | ((value) << offset))

#define ipcSMInitBuffer(buffer, memtype, poolId, ownerId, refCount) \
   ((ipcSMBufHead_t *)(buffer))->words[0] = (unsigned short)        \
    ((BUFHEAD_MARKER                << BUFHEAD_MARKER_OFFSET)    |  \
     (ownerId                       << BUFHEAD_OWNER_OFFSET)     |  \
     (memtype                       << BUFHEAD_MEMTYPE_OFFSET)),    \
   ((ipcSMBufHead_t *)(buffer))->words[1] = (unsigned short)        \
    ((poolId                        << BUFHEAD_POOLID_OFFSET)    |  \
     (refCount                      << BUFHEAD_REFCOUNT_OFFSET))

#define ipcSMGetWord(hdr, index) (((ipcSMBufHead_t *)hdr)->words[(index)])
#define ipcSMReadBufLong(buf, index)  \
    (((unsigned long) (ipcSMGetWord(buf, index)) << 16) + \
      (unsigned long) (ipcSMGetWord(buf, index + 1)))
     
#define ipcSMWriteBufLong(buf, index, value)  \
    (ipcSMGetWord(buf, index) = (unsigned short)(((value) >> 16) & 0xffff),  \
     ipcSMGetWord(buf, index+1) = (unsigned short)((value) & 0xffff) )

#define ipcSMSetPoolOwner(buffer, owner)    ipcSMWriteBufheadField(buffer, \
    BUFHEAD_OWNER_INDEX, BUFHEAD_OWNER_MASK, BUFHEAD_OWNER_OFFSET, owner)

#define ipcSMSetMemType(buffer, memtype)    ipcSMWriteBufheadField(buffer, \
    BUFHEAD_MEMTYPE_INDEX, BUFHEAD_MEMTYPE_MASK, BUFHEAD_MEMTYPE_OFFSET, memtype)

#define ipcSMSetPoolId(buffer, poolId)      ipcSMWriteBufheadField(buffer, \
    BUFHEAD_POOLID_INDEX, BUFHEAD_POOLID_MASK, BUFHEAD_POOLID_OFFSET, poolId)

#define ipcSMSetRefCount(buffer, refCount)  ipcSMWriteBufheadField(buffer, \
    BUFHEAD_REFCOUNT_INDEX, BUFHEAD_REFCOUNT_MASK, BUFHEAD_REFCOUNT_OFFSET, refCount)

#define ipcSMSetMarker(buffer, marker)      ipcSMWriteBufheadField(buffer, \
    BUFHEAD_MARKER_INDEX, BUFHEAD_MARKER_MASK, BUFHEAD_MARKER_OFFSET, marker)

#define ipcSMSetMulticore(buffer, multi)    ipcSMWriteBufheadField(buffer, \
    BUFHEAD_MULTICORE_INDEX, BUFHEAD_MULTICORE_MASK, BUFHEAD_MULTICORE_OFFSET, multi)

#define ipcSMGetPoolOwner(buffer)           ipcSMReadBufheadField(buffer, \
    BUFHEAD_OWNER_INDEX, BUFHEAD_OWNER_MASK, BUFHEAD_OWNER_OFFSET)

#define ipcSMGetMemType(buffer)             ipcSMReadBufheadField(buffer, \
    BUFHEAD_MEMTYPE_INDEX, BUFHEAD_MEMTYPE_MASK, BUFHEAD_MEMTYPE_OFFSET)

#define ipcSMGetPoolId(buffer)              ipcSMReadBufheadField(buffer, \
    BUFHEAD_POOLID_INDEX, BUFHEAD_POOLID_MASK, BUFHEAD_POOLID_OFFSET)

#define ipcSMGetRefCount(buffer)            ipcSMReadBufheadField(buffer, \
    BUFHEAD_REFCOUNT_INDEX, BUFHEAD_REFCOUNT_MASK, BUFHEAD_REFCOUNT_OFFSET)

#define ipcSMGetMarker(buffer)              ipcSMReadBufheadField(buffer, \
    BUFHEAD_MARKER_INDEX, BUFHEAD_MARKER_MASK, BUFHEAD_MARKER_OFFSET)

#define ipcSMGetMulticore(buffer)           ipcSMReadBufheadField(buffer, \
    BUFHEAD_MULTICORE_INDEX, BUFHEAD_MULTICORE_MASK, BUFHEAD_MULTICORE_OFFSET)

/* debug features will be disabled if IPC_RELEASE is defined */
#ifndef IPC_RELEASE
#define ipcSMGetPid(buffer)       ((ipcSMBufHead_t *)(buffer))->words[BUFHEAD_PID_INDEX]
#define ipcSMSetPid(buffer, pid)  ((ipcSMBufHead_t *)(buffer))->words[BUFHEAD_PID_INDEX] = pid

#define ipcSMGetUsrSize(buf)      ipcSMReadBufLong(buf, BUFHEAD_SZ1_INDEX)
#define ipcSMSetUsrSize(buf, sz)  ipcSMWriteBufLong(buf, BUFHEAD_SZ1_INDEX, sz)

#define ipcSMGetTick(buf)         ipcSMReadBufLong(buf, BUFHEAD_TK1_INDEX)
#define ipcSMSetTick(buf, tk)     ipcSMWriteBufLong(buf, BUFHEAD_TK1_INDEX, tk)

#define ipcSMGetMark2(buf)        ((ipcSMBufHead_t *)(buf))->words[BUFHEAD_MARKER2_INDEX]
#define ipcSMSetMark2(buf, mk)    ((ipcSMBufHead_t *)(buf))->words[BUFHEAD_MARKER2_INDEX] = mk

#endif

#define ipcSMCheckHdr(buffer)  (ipcSMGetMarker(buffer) == BUFHEAD_MARKER_MASK)

#define ipcSMHeaderToData(bufhead)      ( (void *) (((char *)(bufhead)) + sizeof(ipcSMBufHead_t)) )
#define ipcSMDataToHeader(data)         ( (ipcSMBufHead_t *) (((char *)(data)) - sizeof(ipcSMBufHead_t)) )

#define getMemType(buf)     ipcSMGetMemType(ipcSMDataToHeader(buf))
#define isBufferValid(buf)  ((buf != (void *)0) && \
    (ipcSMGetMarker(ipcSMDataToHeader(buf)) == BUFHEAD_MARKER))

#define isApPresent(addr)               ( ((BPInitFlags_t *)(addr))->apIsPresent == AP_BP_MAGIC_FLAG )
#define shouldBpHalt(addr)              ( ((BPInitFlags_t *)(addr))->bpShouldHalt == AP_BP_MAGIC_FLAG )

#define configBpDebug(addr)             ( ((BPInitFlags_t *)(addr))->apIsPresent = AP_BP_MAGIC_FLAG ), \
                                        ( ((BPInitFlags_t *)(addr))->bpShouldHalt = AP_BP_MAGIC_FLAG )
                                        
#define configBpNoDebug(addr)           ( ((BPInitFlags_t *)(addr))->apIsPresent = AP_BP_MAGIC_FLAG ), \
                                        ( ((BPInitFlags_t *)(addr))->bpShouldHalt = 0 )
#ifdef __cplusplus
}
#endif

#endif /* IPC_SHARED_MEMORY_H */
