/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_macro_defs.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This header contains macro definitions used by SIPC throughout to replace
*   some preprocessor macros of compiler
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 15Jan2007  Motorola Inc.         Initial Version
* 17Apr2007  Motorola Inc.         Switched IPC_RELEASE macro for MS and LAB build
*****************************************************************************************/
#ifndef IPC_MACRO_DEFS_H
#define IPC_MACRO_DEFS_H

#ifdef IPC_DEBUG_ON
    #ifdef IPC_RELEASE
    #undef IPC_RELEASE
    #endif
#else
    #ifndef IPC_RELEASE
    #define IPC_RELEASE
    #endif
#endif

#endif
