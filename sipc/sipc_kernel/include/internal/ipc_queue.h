/*****************************************************************************************
*                Template No. SWF0112   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA
*
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_queue.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file contains definitions, macros, and function prototypes used by
*   the IPC Queue.
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation 
*****************************************************************************************/

#ifndef IPC_QUEUE_H
#define IPC_QUEUE_H

#include "ipc_list.h"
#include "ipc_osal.h"


#ifdef __cplusplus
extern "C" {
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/

/* flags to pass into IPC Queue put and get functions */
#define IPC_QUEUE_NON_BLOCKING      0x01
#define IPC_QUEUE_HIGH_PRIORITY     0x02
#define IPC_QUEUE_LIFO              0x04
#define IPC_QUEUE_PEEK              0x08

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef struct tag_ipcQueue_t
{
    ipcList_t lowPriorityList;
    ipcList_t highPriorityList;

    ipcMutexHandle_t mutex;
    ipcSemaHandle_t notEmptySema;
    INT32 currentSize;
    INT32 maxQueueSize;
} ipcQueue_t;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/


int ipcQueueOpen(ipcQueue_t *queue);
void ipcQueueClose(ipcQueue_t *queue);

int ipcQueuePutLink(ipcQueue_t *queue, ipcListLink_t link, int flags);
ipcListLink_t ipcQueueGetLink(ipcQueue_t *queue, int flags, int *error);

int ipcQueuePutObject(ipcQueue_t *queue, void *object, int flags);
void *ipcQueueGetObject(ipcQueue_t *queue, int flags, int *error);

/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcQueueNumElements(queue)      ((queue)->highPriorityList.size + \
                                         (queue)->lowPriorityList.size)
#define ipcQueueSetMaxSize(queue, value) \
    (queue)->maxQueueSize = (value)


#ifdef __cplusplus
}
#endif

#endif /* IPC_LIST_H */
