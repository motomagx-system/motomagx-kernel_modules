/*****************************************************************************************
*                Template No. SWF0114   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   The GNU C Library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; version 2.1 of 
*   the License.
*
*   This Library is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_defs.h
*
*------------------------------------- PURPOSE -------------------------------------------
*
*   This file contains definitions, macros, and function prototypes used by
*   IPC stack. 
*
*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 29Jan2007  Motorola Inc.         Initial Creation 
* 29Jun2007  Motorola Inc.         Added macro IPC_LOG_PROC_NAME_SIZE
*****************************************************************************************/
/**
 \file ipc_defs.h
**/

#ifndef IPC_DEFS_H
#define IPC_DEFS_H

#if defined(__KERNEL__)
#include "linux/types.h"
#else 
#include <sys/types.h>
#endif
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*  IPC node id */
#define IPC_NODE_ANY            0x00000000
#define IPC_NODE_BROADCAST      0x00000001

/*  IPC service id *
 *   The IPC Service ID is 2 bytes long
 *  
 *  +-+-+-------------------------+
 *  |x|x|    2^14 service id      |
 *  +-+-+-------------------------+
 *  P M
 * P bit - whether or not the service ID is published
 * M bit - whether or not the service is a multicast service or not
 */
#define IPC_SRVID_P_BIT                 0x8000
#define IPC_SRVID_M_BIT                 0x4000

/* NOTE: IPC_CONTROL_SERVICE is for internal use only */
#define IPC_CONTROL_SERVICE(code)       (IPC_SRVID_M_BIT | ((code) & 0x3FFF))
#define IPC_UNICAST_SERVICE(code)       (IPC_SRVID_P_BIT | ((code) & 0x3FFF))
#define IPC_MULTICAST_SERVICE(code)     (IPC_SRVID_P_BIT | IPC_SRVID_M_BIT | ((code) & 0x3FFF))
#define IPC_UNPUBLISHED_SERVICE(code)   ((code) & 0x3FFF)

/* Bind to IPC_SERVICE_ANY to have the SmartIPC stack assign an unpublished service ID */
#define IPC_SERVICE_ANY                 0x0000

/* Service IDs defined internally for packet logger */
#define IPC_SRV_LOG_CONFIG              IPC_UNICAST_SERVICE(0x3FFE)
#define IPC_SRV_MULTI_PACKET_LOG        IPC_MULTICAST_SERVICE(0x3FFE)

/* Valid range for unpublished service IDs (assigned by SmartIPC stack) */
#define IPC_FIRST_UNPUB_SRV_ID          IPC_UNPUBLISHED_SERVICE(0x0001) 
#define IPC_LAST_UNPUB_SRV_ID           IPC_UNPUBLISHED_SERVICE(0x3FFF)

/* special service IDs */
#define IPC_SERVICE_DISCONNECT ((unsigned short)0)

/* Return values for IPC function calls */
#define IPC_OK                   0
#define IPC_ERROR               -1

/* User error codes start from -10000 and go down */
#define IPC_HIGHEST_ERROR_CODE  -10000   
#define IPCEATTACH              -10000      /* Could not attach to specified shared memory segment */
#define IPCENOTREADY            -10001      /* Could not initialize communication with IPC stack */
#define IPCEDETACH              -10002      /* Could not detach to specified shared memory segment */
#define IPCENOTINIT             -10003      /* Need to call ipcinit first */
#define IPCEBUSY                -10004      /* Stack is unable to take requests at this time */
#define IPCEAFNOSUPPORT         -10005      /* Specified address family is not supported */
#define IPCENOBUFS              -10006      /* No buffer space is available */
#define IPCEPROTONOSUPPORT      -10007      /* Specified protocol is not supported */
#define IPCEDSM                 -10008      /* Packet discarded because destination in deep sleep */
#define IPCEAUTH                -10009      /* Could not authenticate */
#define IPCEFAULT               -10010      /* Invalid pointer */
#define IPCEINVAL               -10011      /* Invalid parameter */
#define IPCENOTSOCK             -10012      /* Descriptor is not socket */
#define IPCEINTR                -10013      /* Socket was closed during blocking call */
#define IPCECHAN                -10014      /* Invalid channel descriptor */
#define IPCENODE                -10015      /* Invalid Node ID */
#define IPCEQOS                 -10016      /* Unable to grant requested Quality of Service */
#define IPCESERVICE             -10017      /* Invalid Service ID */
#define IPCEWMARK               -10018      /* Invalid water mark value */
#define IPCESERVICETABLE        -10019      /* Buffer is small to hold all service table entries */
#define IPCEWOULDBLOCK          -10020      /* Operation would block on non-blocking call */
#define IPCEMSGSIZE             -10021      /* IPC message is too big to send/receive */
#define IPCEADDRNOTVAL          -10022      /* Invalid address structure */
#define IPCEADDRINUSE           -10023      /* Address is already in use */
#define IPCENETUNREACH          -10024      /* Network cannot be reached from this node at this time */
/* A HOLE HERE */
#define IPCENOSHMEM             -10026      /* Out of shared memory buffers from specified pool */
#define IPCEBADBUF              -10027      /* Invalid buffer passed to ipczfree */      
#define IPCENOTATTACH           -10028      /* Not attached to specified shared memory pool */
#define IPCENOTIMPLEMENTED      -10029      /* Function has not yet been implemented */
#define IPCESHUTDOWN            -10030      /* Socket has been shut down */
#define IPCESOCKFULL            -10031      /* Destination socket queue is full */
#define IPCECONNREFUSED         -10032      /* No one listening on the remote address. */
#define IPCEISCONN              -10033      /* The socket is already connected. */
#define IPCETIMEDOUT            -10034      /* Timeout while attempting connection. */
/* A HOLE HERE */
#define IPCESOCKNOSUPPORT       -10036      /* Socket specified is not a connection oriented socket. */
/* A HOLE HERE */
#define IPCENOTCONN             -10038      /* Socket is not connected prior to this call. */
#define IPCEFLAG                -10039      /* Invalid value passed in the flag field. */
#define IPC_LOWEST_ERROR_CODE   -10039

/* dsm_status values for IPCSERVICE_LOCATION */
#define IPC_DSM_UNKNOWN         0
#define IPC_DSM_AWAKE           1
#define IPC_DSM_ASLEEP          2


/* ipcinit flags */
#define IPC_PHYSSHM         1
#define IPC_PROCSHM         2

/*  IPC address family  */
#define AF_IPC              28

/*  IPC socket type */
#define IPC_CO_SOCK         4
#define IPC_CL_SOCK         8

/*  IPC protocol    */
#define IPC_PROTO           10

/*  shutdown flags  */
#define IPCSD_RECEIVE       1
#define IPCSD_SEND          2
#define IPCSD_BOTH          3

/*  channel/data transfer flags   */
#define IPC_DSM_WAKE_UP     0x0001
#define IPC_DSM_DISCARD     0x0002

#define IPCPRIO             0x0004
#define IPCROSPRIO          0x0008
#define IPCROSCHKMSG        0x0010
#define IPCRELIABLE         0x0020
#define IPCPAYLOADCRC       0x0040
#define IPCNONBLOCK         0x0080

/*  notification flags  */
#define IPCREAD             0x0001
#define IPCWRITE            0x0002
#define IPCLOWMARK          0x0004
#define IPCHIGHMARK         0x0008
#define IPCONLINE           0x0010
#define IPCOFFLINE          0x0020
#define IPCPRIONOTIFY       0x1000

/* CO socket backlog */
#define IPCMAXBACKLOG       5
#define IPC_DIRECT_CONNECT  0

/* Max message size of ipc notification */
#define IPC_NOTIFICATION_MAX_SIZE 32

/* Max supported number of log filters */
#define IPC_LOG_MAX_FILTERS     10
#define IPC_LOG_PROC_NAME_SIZE  32

/* The following constants indicate what action should be taken if a packet
 * matches the corresponding packet filter.  Any other value between 0 and
 * 0x8000 indicates that we should include packet data in the log but no more
 * than that length (in bytes) */
#define IPC_LOG_IGNORE          0x0000FFFF 
#define IPC_LOG_WITHOUT_DATA    0x00000000
/*          ...                 Any other value in this range is also valid */
#define IPC_LOG_WITH_ALL_DATA   0x00008000

/* The timestamp flag can be ORed with the constants above to turn timestamps
 * on for any message that matches this filter */
#define IPC_LOG_TIMESTAMP       0x00010000

/*
* Select uses arrays of IPC sockets.  These macros manipulate such
* arrays.  IPCFD_SETSIZE may be defined by the user before including
* this file, but the default here should be >= 64.
*/
#ifndef IPCFD_SETSIZE
#   define IPCFD_SETSIZE      64
#endif /* IPCFD_SETSIZE */

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*
* Structure used in select() call, taken from the BSD file sys/time.h.
*/
struct ipctimeval {
    long    tv_sec;         /* seconds */
    long    tv_usec;        /* and microseconds */
};

struct IPCSOCK_ADDR
{
    unsigned long   ipc_node;
    unsigned short  ipc_service;
};

struct IPCSERVICE_LOCATION
{
    unsigned long ipc_node;
    unsigned short ipc_service;
    unsigned short pad; /* for efficient word-aligned access */
};

typedef struct tag_ipcErrorItem_t
{
    int number;
    const char* text;
} ipcErrorItem_t;

typedef struct tag_ipcLogFilter_t
{
    long    srcNode;
    long    srcService;
    long    destNode;
    long    destService;
    long    action;
} ipcLogFilter_t;

/* App -> IPC message to set log filters (or turn off logging if numFilters = 0) */
typedef struct tag_ipcLogConfigMsg_t
{
    long    numFilters;
    ipcLogFilter_t    filters[IPC_LOG_MAX_FILTERS];
} ipcLogConfigMsg_t;

/* IPC -> App message to log received packet */
typedef struct tag_ipcLogPacketMsg_t
{
    long    srcNode;
    long    srcService;
    long    destNode;
    long    destService;
    long    timestamp_sec;
    long    timestamp_usec;
    long    originalDataSize;
    char    processName[IPC_LOG_PROC_NAME_SIZE];
    unsigned char data[1];
} ipcLogPacketMsg_t;


#if !defined(__KERNEL__)
typedef fd_set ipcfd_set;
#endif

/*  Since components don't know the nodeID, so here the "something" is actually
*  the node ID.
*/
struct auth
{
    unsigned int something;
};


#ifdef __cplusplus
extern "C"{
#endif
/*----------------------------- FUNCTION PROTOTYPE(S) ----------------------------------*/
/**
 * \brief
 * This function initializes the connection to the IPC stack.
 * The flag parameter modifies how the task intends to use the IPC stack as specified below.
 *
 * \param[in] flag  Set of possible bit wise flags. Must be set to 0, will be implemented in the future.
 * \param[in] pauth  Authentication structure.  Must be set to null, will be implemented in the future.
 *
 * \return
 * If an error occurs the function will return the specific error code as specified in the table below.
 * On successful completion this function returns 0.
 *
 *\par Remark
 *Use this function to connect the task to the IPC stack.
 *Both the flag and auth parameters are there for future expansion.
 *At this time calls to ipcinit should set flag to 0 and auth to null.
 *
 * \par Error Code:
 * - \b IPCEATTACH Could not attach shared memory segments
 * - \b IPCNOTREADY Could not initialize communication with the IPC stack.
 *
 */

int ipcinit(unsigned int flag,
            struct auth* pauth);

/**
 * \brief
 * This function will close the task's connection to the IPC stack.
 *
 * \param None.
 *
 * \return
 *      If an error occurs this function returns a specific error code as specified in the table follow.
 *      On successful completion this function will return 0 and will disconnect the calling task from the IPC stack.
 *\par Remark
 * Use this function to disconnect a task to the IPC stack. All socket connections belonging to the calling task will be closed.
 *
 * \par Error Code:
 * - \b IPCEDETACH Could not detach to a specified shared memory segment.
 * - \b IPCENOTINIT  Ipcinit was never called
 *
 */

int ipcclose(void);

/**
 * \brief
 *This function returns the general status of the IPC stack.
 *
 * \param None.
 *
 * \return
 * If no error occurs, this function returns zero.
 * If an error occurs this function returns IPC_ERROR, and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark:
 * Successful completion of this function should be taken to mean that the stack is ready for use.
 *
 * \par Error Code:
 * - \b IPCENOTREADY Stack has been initialized but not able to take requests at this time.
 * - \b IPCENOTINIT Stack has not been initialized.
 *
 */

int ipcgetstatus(void);


/**
 * \brief
 * This function returns the version of the IPC stack.
 *
 * \param [out] version pointer to long to be populated with the version of the stack
 *
 * \return
 * If an error occurs this function returns IPC_ERROR, and a specific error code can be retrieved by calling Ipcgetlasterror.
 * On successful completion this function return the IPC stack version number in ipc_version.
 *
 * \par Error Code:
 * - \b IPCENOTINIT  Ipcinit must be called before using this function
 * - \b IPCEFAULT Ipc_version is not valid pointer
 *
 */

int ipcgetversion(unsigned long* version);


/*  Socket and channel management   */
/**
 * \brief
 * This function creates an ipc socket.
 *
 * \param[in] af Address family specification.
 *               AF_IPC :Specifies IPC as the address family (domain) to be used by this ipcsocket.
 * \param[in] type Type specification for the new socket.
 * \param[in]  type: int, Type specification for the new socket.
 *                   IPC_CL_SOCK        :Standard IPC socket (connection less).
 *                   IPC_CO_SOCK        :Connection oriented IPC socket
 * \param[in]  protocol: int, Protocol to be used with the socket that is specific to the indicated address family.
 *                   IPC_PROTO    :Specifies IPC as the protocol to be used by this IPC socket.
 *
 * \return On failure - a value of IPC_ERROR is returned, and a specific error code can be retrieved by
 *                               calling Ipcgetlasterror.
 *         On success -  this function returns a descriptor referencing the new IPC socket.
 *\par Remark
 * This function causes an IPC socket descriptor and any related resources to be allocated and bound to a specific IPC transport-service provider
 *
 * \par Error Code:
 * - \b IPCEATTACH Could not attach shared memory segments
 * - \b IPCNOTREADY Could not initialize communication with the IPC stack.
 * - \b IPCENOTINIT  Ipcinit was not called before making this call
 * - \b IPCEAFNOSUPPORT  The specified address family is not supported.
 * - \b IPCENOBUFS  No buffer space is available. The ipcsocket cannot be created.
 * - \b IPCEPROTONOSUPPORT  The specified protocol is not supported.
 * - \b IPCESOCKNOSUPPORT  The specified ipcsocket type is not supported in this address family.
 */

int ipcsocket(int af,
              int type,
              int protocol);

/**
 * \brief
 * This function associates a local ipc address with a socket
 *
 * \param [in]  sock: int, Descriptor identifying an unbound socket.
 * \param [in]  name: const struct IPCSOCK_ADDR, Address to assign to the socket from the
 *             ipcsock_addr structure.
 * \param [in]  namelen: int, Length of the value in the name parameter.
 *
 * \return On failure - returns a value of IPCSOCKET_ERROR, and a specific error code can
 *                      be retrieved by calling Ipcgetlasterror.
 *           On success - returns 0.
 *\par Remark
 * The ipcbind function is used to bind IPC sockets only. When a socket is created with
 * a call to the ipcsocket function, it exists in the IPC name space (IPC address family)
 * but has no address assigned to it. Use the ipcbind function to establish the local
 * association of the socket by assigning a local name to an unnamed (addressed) socket.
 *
 * \par Error Code:
 * - \b IPCENOTINIT Ipcinit was not called before making this call
 * - \b IPCEADDRINUSE A process on the machine is already bound to the same fully qualified IPC address.
 * - \b IPCEADDRNOTVAL The specified IPC address is not a valid address for this machine.
 * - \b IPCEFAULT The name or namelen parameter is not a valid part of the user address space, the namelen parameter is too small, the name parameter contains an incorrect address format for the associated address family, or the first two bytes of the memory block specified by name donot match the address family associated with the socket descriptor s.
 * - \b IPCEINVAL The socket is already bound to an address.
 * - \b IPCENOBUFS Insufficient resources to complete bind.
 * - \b IPCENOTSOCK The descriptor is not a socket.
 */

int ipcbind(int sock,
            const struct IPCSOCK_ADDR* name,
            int namelen);

/**
 * \brief
 * This function retrieves the local name for a socket.
 *
 *   \param [in]  sock int, Descriptor identifying a socket.
 *   \param [out]  name IPCSOCK_ADDR, Receives the address (name) of the socket.
 *   \param [in,out]  namelen int, Size of the name buffer.
 *
 *   \return On failure - returns a value of IPCSOCKET_ERROR, and a specific error code can
 *                        be retrieved by calling Ipcgetlasterror.
 *           On success - returns 0.
 *\par Remark
 * This function retrieves the current name for the specified socket descriptor in the name parameter.
 * It is used on the bound or connected socket specified by the s parameter. The ipcgetsockname
 * function provides the only way to determine the local association that has been set by the system.
 * On call, the namelen argument contains the size of the name buffer, in bytes.
 * On return, the namelen parameter contains the actual size in bytes of the name parameter.
 *
 * \par Error Code:
 * - \b IPCENOTINIT Ipcinit was not called before making this call.
 * - \b IPCENOTSOCK The descriptor is not a socket.
 * - \b IPCEINVAL The socket has not been bound to an address with bind. Or the address is not initialized.
 * - \b IPCEFAULT name or namelen are invalid pointers, or namelen is too small.
 */

int ipcgetsockname(int sock,
                   struct IPCSOCK_ADDR* name,
                   int * namelen);

/**
 * \brief
 * This function retrieves the peer name for a socket.
 *
 *   \param [in] sock Descriptor identifying a socket
 *   \param [in] name Receives the address (name) of the peer socket.
 *   \param [in, out] namelen Size of the name buffer.
 *
 *   \return If no error occurs, this function returns zero.
 *   If an error occurs, a value of IPC_ERROR is returned, and a specific error code
 *   can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 *
 * This function retrieves the peer name for the specified socket descriptor in the name parameter.
 * It is used on the connected socket specified by the s parameter.
 * The ipcgetpeername function provides the only way to determine the peer association
 * that has been set by the system.
 * On call, the namelen argument contains the size of the name buffer,
 * in bytes. On return, the namelen parameter contains the actual size in bytes of the name parameter.
 *
 *   \par Error Code:
 *
 * - \b IPCENOTINIT     Ipcinit was not called before making this call.
 * - \b IPCENOTSOCK     The descriptor is not a socket.
 * - \b IPCEINVAL       The socket has not been bound to an address with bind. Or the address is not initialized.
 * - \b IPCEFAULT       name or namelen are invalid pointers, or namelen is too small.
 */

int ipcgetpeername(int sock,
                   struct IPCSOCK_ADDR* name,
                   int *namelen);

/**
 * \brief
 * This function disables sends or receives on a socket.
 *
 *   \param [in] sock Descriptor identifying a socket.
 *   \param [in] how Flag that describes what types of operation will no longer be allowed.
 *
 *   \return
 * If no error occurs, this function returns zero.
 * If an error occurs, a value of IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 * This function is used on ipc sockets to disable reception, transmission, or both.
 * If the how parameter is IPCSD_RECEIVE, subsequent calls to the recvfrom function
 * on the IPC socket will be disallowed.
 * If the how parameter is IPCSD_SEND, subsequent calls to the send function are disallowed.
 * Setting how to IPCSD_BOTH disables both sends and receives as described above.
 *
 * The ipcshutdown function does not close the socket.
 * Any resources attached to the socket will not be freed until ipcclosesocket is invoked.
 *
 *   \par Error Code:
 * - \b IPCENOTINIT     Ipcinit was not called before making this call
 * - \b IPCEINVAL       The how parameter is not valid or is not consistent with the socket type.
 * - \b IPCENOTSOCK     The descriptor is not a socket.
 */

int ipcshutdown(int sock,
                int how);

/**
 * \brief
 * This function sets the maximum number of message that can be queued to an existing ipc socket.
 * In the case of CO sockets the queue length is used to define the flow control window.
 *
 *   \param [in] sock Descriptor identifying the ipc socket.
 *   \param [in] queueMaxLen Maximum numbers of messages that can be queued to a socket?s receive queue.
 *
 *   \return
 * If no error occurs, this function returns zero.
 * If an error occurs, a value of IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 * Use this function to set the maximum number of messages that can be queued to an ipcsocket.
 * To CO sockets, the maximum number is 255;To CL socket,
 * the maximum number is 300000 if the platform is ROS and 500000 if the platform is not ROS.
 * The behavior of ipcsend and ipcsendto depends on whether the socket is
 * a connected or a connectionless socket and how the high and low water
 * marks are set by using ipcsetwatermark.
 *
 *   \par Error Code:
 * - \b IPCENOTINIT     Ipcinit was not called before making this call
 * - \b IPCENOTSOCK     The descriptor is not a socket.
 * - \b IPCEINVAL       There is currently data in the queue and the value passed
 *   is smaller than the number of elements in the queue.
 *   Or the queueMaxLen is not positive number.
 *   Or the queueMaxlen is larger than the value defined in Remarks.
 */

int ipcsetsockqlen(int sock,
                   int queueMaxLen);

/**
 * \brief
 * This function closes an existing ipc socket.
 *
 *   \param [in] sock Descriptor identifying the ipc socket to close.
 *
 *   \return
 * If no error occurs, this function returns zero.
 * If an error occurs, a value of IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 * Use this function to release the ipcsocket descriptor s so that further
 * references to s fail with the error IPCENOTSOCK.
 * Any pending overlapped send and receive operations issued by any thread
 * in this process are canceled.  An application should always have a matching
 * call to ipcclosesocket for each successful call to the ipcsocket function
 * to return any socket resources to the system.
 *
 *   \par Error Code:
 * - \b IPCENOTINIT     Ipcinit was not called before making this call
 * - \b IPCENOTSOCK     The descriptor is not a socket.
 */

int ipcclosesocket(int sock);

/*  Service management  */

/**
 * \brief
 * This function creates a QOS channel associated with the socket provided.
 *
 *   \param [in] sock Descriptor identifying a socket (can be either IPC_CL_SOCK or IPC_CO_SOCK).
 *   \param [in] qos Quality of service requested is bytes/sec.
 *   \param [in] flag Set of possible bit wise flags (see below).
 *
 *   \return
 * If an error occurs, a value of IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling ipcgetlasterror.
 *
 *   \par Remark
 * This function will create an IPC channel for the purpose of providing
 * a particular quality of service (qos).
 * The flag parameter modifies the behavior of the channel that will be
 * created according to the table below.
 * \b NOTE: the flag parameter is specified for possible future features
 * and should be set to 0 in this version.
 * Note that both connection oriented and connectionless sockets can be specified.
 * If ipcchan is called with qos is set to 0 the previously defined channel will be removed.
 *
 *   \par Error Code:
 * - \b IPCEQOS Quality of service not available.
 * - \b IPCENOTSOCK     The descriptor is not an IPC socket.
 * - \b IPCENOBUFS      Resources could not be found to create a new channel
 * - \b IPCENETUNREACH  Timeout occurred while creating multihop channel
 * - \b IPCENOTCONN     Socket is not connected prior to this call.
 * - \b IPCENOTINIT     ipcinit was not called before making this call
 * - \b IPCEINVAL       Either ipc_qos or the flags parameter is invalid.
 */

int ipcchan(int sock, int qos, int flag);

/*  Service management  */
/**
 * \brief
 * This function will set the water mark value for a service in the IPC network.
 *
 *   \param [in] sock Descriptor identifying an IPC socket.
 *   \param [in] ipc_lowwmark Low water mark value 0-255.
 *   \param [in] ipc_hiwmark High water mark value 0-255.
 *
 *   \return
 * If an error occurs, a value of IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 * The ipc_highwmark value must be greater than the ipc_lowwmark value.
 * If both values are set to 0, then the watermark values of the socket will be removed.
 *
 *   \par Error Code:
 * - \b IPCEWMARK       Invalid water mark value.
 * - \b IPCENOTSOCK     The socket is an invalid socket.
 * - \b IPCENOTINIT     Ipcinit must be called before calling this function.
 */
int ipcsetwatermark(int sock,
                    unsigned long ipc_lowwmark,
                    unsigned long ipc_hiwmark);

/**
 * \brief
 *  This function will locate services available in the IPC network.
 *
 *   \param [in] ipc_node Specific node id or IPC_NODE_ANY.
 *   \param [in] ipc_service Specific service or IPC_SERVICE_ANY.
 *   \param [out] ipc_servicetable This is a list of ipc_service and ipc_node pairs.
 *   \param [out] ipc_sizeofservicetable number of entries in the ipc_servicetable.
 *   \param [in] ipc_maxsizeofservicetable max number of entries in the ipc_servicetable.
 *
 *   \return
 * If an error occurs, a value of IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 * This function will locate IPC services.
 * The contents of  ipc_servicetable will depend on what values are
 * passed in ipc_node and ipc_service as specified in the following table.
 *
 * <table>
 * <tr> <td>Ipc_node</td>       <td>Ipc_service </td> <td>Ipc_servicetable</td> </tr>
 * <tr> <td>Specific ipc_node</td> <td>Specific ipc_service</td>
 *      <td>A list with only one entry is returned if the service is
 *      found in the specified node.
 *      Otherwise,  an empty list is returned  (NULL)
 *      and ipc_sizeofservicetable will be returned as 0. </td> </tr>
 * <tr> <td>IPC_NODE_ANY</td> <td>Specific ipc_service</td>
 *      <td> List of nodes supporting the specific ipc_service
 *      (possibly more than one if the service is a multicast service). </td> </tr>
 * <tr> <td> Specific ipc_node </td> <td> IPC_SERVICE_ANY </td>
 *      <td> List of services provided by the specified specific node. </td> </tr>
 * <tr> <td> IPC_NODE_ANY </td> <td> IPC_SERVICE_ANY </td>
 *      <td> All nodes and all services available in the IPC network. </td> </tr>
 * </table>
 *
 *   \par Error Code:
 * - \b IPCENODE        Bad ipc_node.
 * - \b IPCENOTINIT     Ipcinit was not called before making this call
 * - \b IPCEFAULT       The ipc_servicetable or the sizeofparameter in ipclocateservice is not in a valid part of the process address space.
 * - \b IPCESERVICETABLE        The ipc_servicetable is too small to hold all the available entries that need to be returned.
 */

int ipclocateservice(unsigned long ipc_node,
                     unsigned short ipc_service,
                     struct IPCSERVICE_LOCATION* ipc_servicetable,
                     unsigned long* ipc_sizeofservicetable,
                     unsigned long ipc_maxsizeofservicetable);


/**
 * \brief
 *  These functions define asynchronous notifications to be delivered as specified.
 *
 *   \param [in] sock Descriptor identifying an IPC socket that will receive asynchronous notifications.
 *   \param [in] name Address of service/socket that will cause the asynchronous notification.
 *   \param [in] namelen Length of the value in the name parameter.
 *   \param [in] msg Buffer containing the data to be transmitted when the notification occurs .
 *   \param [in] msglen Length of the data in the msg parameter.
 *   \param [in] flags Indicator specifying what type of notification the socket s is requesting.
 *
 *   \return
 * If no error occurs, this function returns 0 ,
 * If an error occurs IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 * This function is used to setup asynchronous notification related to
 * events affection the IPC service specified by name.
 *
 * The msg parameter defines the actual message that will be sent to the socket \a sock.
 * The parameter \a msglen is used to specify the length of the message.
 * If \a msglen is set to 0 the specified notification in the flags field will be removed.
 * The maximum message length is 32 bytes.
 *
 * The parameter \a sock must be a CL socket(IPC_CL_SOCKET).
 * If the socket specified by \a name is a connectionless socket and IPCWRITE is set in \a flags,
 * then no write notifications will be arrived on \a sock. i.e. connectionless sockets are always writable.
 *
 * For ipcnotify, the socket \a sock and that specified by \a name must not be the same one.
 * If \a name->ipc_node is set to IPC_NODE_ANY and \a name->ipc_service is a published service ID,
 * the IPC stack will pick up any node in which the service \a name->ipc_service is supported.
 * If none of the nodes supports this service, an error IPCESERVICE will be returned.
 *
 * The \a flags parameter is used to define the type of event/situation that will
 * trigger the asynchronous notification.
 * The following table shows the value that is used with the
 * bitwise OR operator to construct the flags parameter.
 *
 * - \b IPCLOWMARK      Notification will be send if the sockets queue associated with the specified service has reached its low water mark.
 * - \b IPCHIGHMARK     Notification will be send if the sockets queue associated with the specified service has reached its high water mark.
 * - \b IPCREAD Notification will occur when data has been received in the service/socket queue specified by name.
 * - \b IPCWRITE        Notification will occur when data can be written in the service/socket queue specified by name.
 * - \b IPCPRIONOTIFY   The notification message should be sent as a priority message
 * - \b IPCONLINE       Notification will be send if the specified service/node is online
 * - \b IPCOFFLINE      Notification will be sent if the specified service/node is offline
*
 * Notification messages related to \b IPCONLINE and \b IPCOFFLINE are one-shot notifications,
 * that is the notification message will be sent only once for each call to ipcnotify()
 * related to \b IPCONLINE and \b IPCOFFLINE.
 * After the \b IPCONLINE and \b IPCOFFLINE condition is met the notification
 * request will automatically be removed from the stack.
 *
 * If the notification condition has been met before the call to ipcnotify()
 * (with IPCONLINE or IPCOFFLINE) is made then the notification message will be immediately sent.
 * Note that only IPCONLINE and IPCOFFLINE related notification request are one-shot notifications
 * all other types of notifications must be explicitly removed.
 *
 * Generally ipcnotify/ipcpsnotify do \b not generate messages on events that have already occurred,
 * only events that occur after ipcnotify or ipcpsnotify is invoked will cause the related message to be sent.
 * However, notifications related to \b IPCONLINE or \b IPCOFFLINE are the exception to this behavior.
 * That is if ipcnotify() is invoked with \b IPCONLINE or \b IPCOFFLINE the related message
 * will occur immediately if the \b IPCONLINE  or \b IPCOFFLINE condition has already been met.
 * The table below further details this behavior.
 *
 * <table>
 * <tr> <td>Case</td> <td>IPCONLINE</td> <td>IPCOFFLINE</td> </tr>
 * <tr> <td>IPC_NODE_ANY: IPC_SERVICE_ANY</td>
 *      <td>NOT ALLOWED - IPCEINVAL error generated </td>
 *      <td>NOT ALLOWED - IPCEINVAL error generated </td> </tr>
 * <tr> <td>IPC_NODE_ANY: service_id</td>
 *      <td>Notification message occurs if service_id is online at any node or
 *          if service id becomes online at any node</td>
 *      <td>Notification message occurs if no node supports service_id or
 *          if service_id becomes offline at any node</td> </tr>
 * <tr> <td>Node_id: IPC_SERVICE_ANY</td>
 *      <td>Notification message occurs if  node is online or if node goes online</td>
 *      <td>Notification message occurs if node_id offline or if  node_id goes off line  </td> </tr>
 * <tr> <td> Node_id: service_id </td>
 *      <td> Notification message occurs if service_id is online at node_id or
 *          if service_id gets online at node_id</td>
 *      <td>Notification message occurs if service_id is offline at node_id or
 *          if service_id gets offline at node_id</td> </tr>
 * </table>
*
 *   \par Error Code:
 * - \b IPCEINVAL       An unknown flag was specified or invalid flag was specified.
 * - \b The socket has not been bound
 * - \b IPCENOTINIT     Ipcinit was not called before making this call
 * - \b IPCEFAULT       The msg  parameter is not part of the user address space, or the tolen parameter is too small.
 * - \b IPCENOBUFS      No buffer space is available.
 * - \b IPCENOTSOCK     The descriptor is not an IPC socket.
 * - \b IPCEADDRNOTVAL  The remote/destination address is not a valid address.
 * - \b IPCEAFNOSUPPORT         Addresses in the specified family cannot be used with this socket.
 * - \b IPCESHUTDOWN    The IPC socket has been shut down.
 * - \b IPCESERVICE     Service not found
 */

int ipcnotify(int sock, 
              const struct IPCSOCK_ADDR* name, 
              int namelen, 
              const char * msg, 
              int msglen, 
              int flags);

/**
 * \brief
 *  These functions define asynchronous notifications to be delivered as specified.
 *
 *   \param [in] q Descriptor identifying a system specific ?queue? that will receive asynchronous notifications.
 *   \param [in] qflag Indicator providing additional information about the type of notification object/queue specified in q.
 *   \param [in] name Address of service/socket that will cause the asynchronous notification.
 *   \param [in] namelen Length of the value in the name parameter.
 *   \param [in] msg Buffer containing the data to be transmitted when the notification occurs .
 *   \param [in] msglen Length of the data in the msg parameter.
 *   \param [in] flags Indicator specifying what type of notification the socket s is requesting.
 *
 *   \return
 * If no error occurs, this function returns 0 ,
 * If an error occurs IPC_ERROR is returned,
 * and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *   \par Remark
 *   This function serves the similar functionality as ipcnotify(),
 * On systems where ipcpsnotify (platform specific notification) is implemented the
 * parameter \a q and \a qflag together determine the platform specific object that will
 * receive the notification specified in flags.
 *
 */

int ipcpsnotify(void* q, int qflag,
        const struct IPCSOCK_ADDR *name, int namelen,
        const char* msg, int msglen, int flags);

/*  Data transfer   */
/**
 * \brief
 * This function sends data to a specific IPC destination.
 *
 *   \param  [in] sock  Descriptor identifying  an IPC socket.
 *   \param  [in] buf   Buffer containing the data to be transmitted.
 *   \param  [in] leng   Length of the data in the buf parameter.
 *   \param  [in] flags Indicator specifying the way in which the call is made.
 *   \param  [in] to    Pointer to the address of the target IPC socket.
 *   \param  [in] tolen Size of the address in parameter to.
 *
 *   \return If no error occurs, this function returns the total number of bytes sent, which should be equal to the
 *           number indicated by len. It is impossible that partial data is sent successfully. If an error occurs,
 *           a value of IPC_ERROR is returned, and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * This function is used to write outgoing data on an ipc connectionless socket. The to parameter can be any valid
 * address in the IPC socket address family, including a broadcast or any multicast address as long as they refer to
 * a connectionless socket.  The successful completion of an ipcsendto call does not indicate that the data was successfully delivered.
 * If to->ipc_node is set to IPC_NODE_ANY and to->ipc_service is a published service ID, the IPC stack will pick up any
 * node in which the service to->ipc_service is supported. If none of the nodes supports this service, an error IPCESERVICE will be returned.
 * If to->ipc_node is set to IPC_NODE_BROADCAST, the message will be sent to all the nodes in which the service to->ipc_service is supported.
 * If the target sockets queue is full a call to ipcsendto will block unless the call is made in a non-blocking mode
 * (see flags).In non-blocking mode if a queue full condition is detected the error IPCEWOULDBLOCK will be returned.If the target socket is
 * connectionless and resides in a remote node ipcsendto will not block and the packet may be discarded if the target sockets queue is
 * full.Connectionless sockets may register for flow control notification using ipcnotify or ipcpsnotify to prevent packet loss and achieve flow control.
 * Calling ipcsendto will return an error if the socket s or the socket specified by to is a connection oriented one, and ipcgetlasterror willreturn IPCEINVAL.
 * The flags parameter can be used to influence the behavior of the function invocation beyond the options specified for the associated
 * socket. The following table shows the value that is used with the bitwise OR operator to construct the flags parameter.
 *      Value          Description
 * - \b IPCNONBLOCK        Forces non-blocking operation.
 * - \b IPCPRIO        Request that the message be treated with higher priority, priority message order is preserved as
 *                     a separate queue is used for priority messages and messages are inserted in FIFO order.
 * - \b IPCROSPRIO         Request that the message be treated with higher priority but the message will be inserter
 *                     into the priority queue in LIFO order. So new priority messages will be available first.
 * - \b IPCPAYLOADCRC  Add checksum for the entire payload
 * \par Error Code:
 * - \b IPCEINVAL          An unknown flag was specified or invalid flag was specified.The specified socket has not been bound.
 * - \b IPCENOTINIT    Ipcinit was not called before making this call
 * - \b IPCEFAULT          The buf  parameter is not part of the user or IPC address space.
 * - \b IPCENOBUFS     No buffer space is available.
 * - \b IPCENOTSOCK        The descriptor is not an IPC socket.
 * - \b IPCEWOULDBLOCK The call is marked as nonblocking and the requested operation would block.
 * - \b IPCESHUTDOWN   The IPC socket has been shut down.
 * - \b IPCENOTCONN        The connection with peer side has been closed or not set up.
 * - \b IPCEMSGSIZE        The len parameter is invalid
 */

int ipcsendto(int sock,
              const char* buf,
              int leng,
              int flags,
              const struct IPCSOCK_ADDR* to,
              int tolen);

 /**
 * \brief
 * This function sends data to a specific IPC destination without copying the data buffer.
 *
 *   \param  [in] sock  escriptor identifying  an IPC socket.
 *   \param  [in] buf   Buffer containing the data to be transmitted.
 *   \param  [in] leng   Length of the data in the buf parameter.
 *   \param  [in] flags Indicator specifying the way in which the call is made.
 *   \param  [in] to    Pointer to the address of the target IPC socket.
 *   \param  [in] tolen Size of the address in parameter to.
 *
 *   \return If no error occurs, this function returns the total number of bytes sent, which should be equal to the
 *           number indicated by len. It is impossible that partial data is sent successfully. If an error occurs,
 *           a value of IPC_ERROR is returned, and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * This function is used to write outgoing data on an ipc connectionless socket. The to parameter can be any valid
 * address in the IPC socket address family, including a broadcast or any multicast address as long as they refer to
 * a connectionless socket.
 * If to->ipc_node is set to IPC_NODE_ANY and to->ipc_service is a published service ID, the IPC stack will pick up any
 * node in which the service to->ipc_service is supported. If none of the nodes supports this service, an error IPCESERVICE will be returned.
 * If to->ipc_node is set to IPC_NODE_BROADCAST, the message will be sent to all the nodes in which the service to->ipc_service is supported.
 * Calling ipczsendto  will return an error if the socket s or the socket specified by to is a connection oriented one, and ipcgetlasterror will return IPCEINVAL.
 * For successful calls to ipczsendto, the calling task no longer owns this buffer and is not responsible for freeing it.  For failed
 * calls (return IPC_ERROR), the calling task still owns the buffer so it can try again or free it.
 * The flags parameter can be used to influence the behavior of the function invocation beyond the options specified for the associated
 * socket. The following table shows the value that is used with the bitwise OR operator to construct the flags parameter.
 *      Value          Description
 * - \b IPCNONBLOCK        Forces non-blocking operation.
 * - \b IPCPRIO        Request that the message be treated with higher priority, priority message order is preserved as
 *                     a separate queue is used for priority messages and messages are inserted in FIFO order.
 * - \b IPCROSPRIO         Request that the message be treated with higher priority but the message will be inserter
 *                     into the priority queue in LIFO order. So new priority messages will be available first.
 * - \b IPCPAYLOADCRC  Add checksum for the entire payload
* \par Error Code:
 * - \b IPCEINVAL          An unknown flag was specified or invalid flag was specified.The specified socket has not been bound.
 * - \b IPCENOTINIT    Ipcinit was not called before making this call
 * - \b IPCEFAULT          The buf  parameter is not part of the user or IPC address space.
 * - \b IPCENOBUFS     No buffer space is available.
 * - \b IPCENOTSOCK        The descriptor is not an IPC socket.
 * - \b IPCEWOULDBLOCK The call is marked as nonblocking and the requested operation would block.
 * - \b IPCESHUTDOWN   The IPC socket has been shut down.
 * - \b IPCENOTCONN        The connection with peer side has been closed or not set up.
 * - \b IPCEMSGSIZE        The len parameter is invalid
 */

int ipczsendto(int sock,
               const char* buf,
               int leng,
               int flags,
               const struct IPCSOCK_ADDR* to,
               int tolen);

/**
 * \brief
 * This function receives ipc data.
 *
 *   \param  [in]  sock         Descriptor identifying a bound socket.
 *   \param  [out] buf          Buffer for the incoming data.
 *   \param  [in]  len          Length of the buf parameter.
 *   \param  [in]  flags        Indicator specifying the way in which the call is made.
 *   \param  [out] from         Pointer to a buffer that will hold the source address on return.
 *   \param  [in, out] fromlen  Pointer to the size of the from buffer
 *
 *   \return If no error occurs, this function returns the number of bytes received. If the connection has been
 *           gracefully closed, the return value is zero. If an error occurs, a value of IPC_ERROR is returned,
 *           and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * This function reads incoming data on ipc sockets and captures the address from which the data was sent. The local
 * address of the socket must be known.  This is usually done explicitly through ipcbind.  The socket can become bound
 * implicitly to a local address through ipcsendto.
 * Data is extracted from the first enqueued message, up to the size of the buffer supplied. If the datagram or message
 * is larger than the buffer supplied, the buffer is filled with the first part of the datagram and ipcrecvfrom generates
 * the error IPCEMSGSIZE. For unreliable protocols the excess data is lost.
 * If the from parameter is nonzero the network address of the peer that sent the data is copied to the corresponding
 * ipcsock_addr structure. The value pointed to by fromlen is initialized to the size of this structure and is modified,
 * on return, to indicate the actual size of the address stored in the ipcsock_addr structure.
 * If no incoming data is available at the socket, the ipcrecvfrom function blocks and waits for data to arrive unless
 * the socket is nonblocking. In this case, a value of IPC_ERROR is returned with the error code set to IPCEWOULDBLOCK. The
 * ipcselect function can be used to determine when more data arrives.
 * The flags parameter can be used to influence the behavior of the function invocation beyond the options specified for
 * the associated socket. The semantics of the ipcrecvfrom function are determined by the ipc socket options and the flags
 * parameter. The following table shows the values can be used to construct the flags parameter. The bitwise OR operator is
 * applied to these values.
 *      Value          Description
 * - \b IPCNONBLOCK        Forces non-blocking operation.
 * - \b IPCROSPRIO         Block until a priority message is received, queue all other messages.
 * - \b IPCROSCHKMSG   Return pointer to message without de-queuing it.
 *               
* \par Error Code:
 * - \b IPCENOTINIT    Ipcinit was not called before making this call
 * - \b IPCEFAULT          The buf parameters are not part of the user address space
 * - \b IPCEINVAL          The IPC socket has not been bound with ipcbind, an unknown flag was specified, or len was zero or negative.
 * - \b IPCENOTSOCK    The descriptor is not a socket.
 * - \b IPCESHUTDOWN   The IPC socket has been shut down. It is not possible to call ipcrecv or ipczrecv on a socket
 *                     after ipcshutdown has been invoked with how set to IPCSD_RECEIVE or IPCSD_BOTH.
 * - \b IPCEWOULDBLOCK The operation is marked as nonblocking and the ipcrecv or ipczrecv operation would block.
 * - \b IPCEMSGSIZE    The message was too large to fit into the specified buffer and was truncated.
 */

int ipcrecvfrom(int sock,
                char * buf,
                int len,
                int flags,
                struct IPCSOCK_ADDR* from,
                int * fromlen);

/**
 * \brief
 * This function receives data using zero copy.
 *
 *   \param  [in]  sock         Descriptor identifying a bound socket.
 *   \param  [in]  zbuf         Buffer pointer for the incoming data to allow zero copy operation
 *   \param  [in]  flags        Indicator specifying the way in which the call is made.
 *   \param  [out] from         Pointer to a buffer that will hold the source address on return.
 *   \param  [in, out] fromlen  Pointer to the size of the from buffer
 *
 *   \return If no error occurs, this function returns the number of bytes received. If the connection has been
 *           gracefully closed, the return value is zero. If an error occurs, a value of IPC_ERROR is returned,
 *           and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * This function reads incoming data on ipc sockets and captures the address from which the data was sent. The local
 * address of the socket must be known.  This is usually done explicitly through ipcbind.
 * If the from parameter is nonzero the network address of the peer that sent the data is copied to the corresponding
 * ipcsock_addr structure. The value pointed to by fromlen is initialized to the size of this structure and is modified,
 * on return, to indicate the actual size of the address stored in the ipcsock_addr structure.
 * For successful calls to ipczrecvfrom, the calling task now owns the new buffer that was passed to it.It is responsible for
 * freeing this buffer when it is done with it.  For failed calls, no new buffer is received.
 * The flags parameter can be used to influence the behavior of the function invocation beyond the options specified for
 * the associated socket.The following table shows the values can be used to construct the flags parameter. The bitwise OR operator is
 * applied to these values.
 *      Value          Description
 * - \b IPCNONBLOCK        Forces non-blocking operation.
 * - \b IPCROSPRIO         Block until a priority message is received, queue all other messages.
 * - \b IPCROSCHKMSG   Return pointer to message without de-queuing it.
 *
 * \par Error Code:
 * - \b IPCENOTINIT    Ipcinit was not called before making this call
 * - \b IPCEFAULT          The buf parameters are not part of the user address space
 * - \b IPCEINVAL          The IPC socket has not been bound with ipcbind, an unknown flag was specified, or len was zero or negative.
 * - \b IPCENOTSOCK    The descriptor is not a socket.
 * - \b IPCESHUTDOWN   The IPC socket has been shut down. It is not possible to call ipcrecv or ipczrecv on a socket
 *                     after ipcshutdown has been invoked with how set to IPCSD_RECEIVE or IPCSD_BOTH.
 * - \b IPCEWOULDBLOCK The operation is marked as nonblocking and the ipcrecv or ipczrecv operation would block.
 * - \b IPCEMSGSIZE    The message was too large to fit into the specified buffer and was truncated.
 */

int ipczrecvfrom(int sock,
                 char ** zbuf,
                 int flags,
                 struct IPCSOCK_ADDR* from,
                 int * fromlen);

/**
 * \brief
 * This function allows a socket to listen or wait for incoming connections.
 *
 *   \param [in] socket Descriptor identifying a bound, unconnected socket.
 *   \param [in] backlog Maximum length of the queue of pending connections.
 *   \return If an error occurs, a value of IPC_ERROR is returned, and a specific error code can be retrieved by calling ipcgetlasterror.
 *            Zero is returned after a successful connection.
 *
 *\par Remark
 * This function must be called before calling ipcaccept.  It prepares a socket to act as a listening server
 *and establish connections to client sockets.  The number passed in for backlog specifies the number of client
 *connections this socket will accept before ipcaccept is called.  If backlog is set to IPC_DIRECT_CONNECT, a
 *single connection will be accepted and that connection will be to the socket specified in s, that is a new socket
 *will not be created.  It in invalid to call this function on an unbound socket or on a socket bound to an
 *unpublished service ID.  This function can only be used with connection oriented sockets (IPC_CO_SOCK).
 *
 * \par Error Code:
 * - \b  IPCEADDRINUSE Another socket is already listening on the same service ID.
 * - \b  IPCENOTINIT ipcinit was not called before making this call
 * - \b  IPCENOTSOCK The descriptor is not an IPC socket.
 * - \b  IPCEINVAL Socket has not been bound before this call.
 * - \b  IPCESERVICE Socket is bounded to a multicast service id.
 * - \b  IPCESOCKNOSUPPORT Socket specified is not a connection oriented socket.
 */

int ipclisten(int socket, int backlog);

 /**
 * \brief
 * This function accepts a connection on a socket.
 *
 *   \param [in]  socket   Descriptor identifying an IPC socket that has been placed in
 *               a listening state with the ipclisten function. The connection is actually made
 *               with the socket that is returned by this function.
 *   \param [out]  addr IPCSOCK_ADDR, Optional pointer to a buffer that receives the address
 *               of the connecting entity.
 *   \param [out] addrlen int, Optional pointer to an integer that contains the length of addr.
 *   \return On failed- a value of IPC_ERROR is returned, and a specific error code
 *                             can be retrieved by calling ipcgetlasterror.
 *           On Success-return 0
 *
 *\par Remark
 * This function extracts the first connection on the queue of pending connections on socket s. It then
 *creates a new ipcsocket and returns a handle to the new ipcsocket.
 *The accept function can block the caller until a connection is present if no pending connections are on queue.
 *If backlog in ipclisten was set to IPC_DIRECT_CONNECT, a single connection will be accepted and that connection
 *will be to the socket specified in s, that is a new socket will not be created.  Otherwise, after the successful
 *completion, ipcaccept will return a new ipcsocket handle, the accepted ipcsocket cannot be used to accept more
 *connections. The original ipcsocket remains open and listens for new connection requests unless it is operating
 *in IPC_DIRECT_CONNECT mode.
 *The argument s is a socket that has been created with ipcsocket, bound to a local address with ipcbind, and is
 *listening for connections after a call to ipclisten. .  This function can only be used with connection oriented sockets
 *(IPC_CO_SOCK).
 *The socket returned by ipcaccept inherits the queue length of the socket s.  The queue length of s must be set before
 *ipclisten is called.
 * \par Error Code:
 * - \b  IPCEADDRINUSE  Another socket is already listening on the same service ID.
 * - \b  IPCENOTINIT    ipcinit was not called before making this call
 * - \b  IPCENOTSOCK    The descriptor is not an IPC socket.
 * - \b  IPCEINVAL      Socket has not been bound before this call.
 * - \b  IPCESERVICE    Socket is bounded to a unpublished service id.
 * - \b  IPCESOCKNOSUPPORT      Socket specified is not a connection oriented socket.
 */

int ipcaccept(int socket, struct IPCSOCK_ADDR *addr, int *addrlen);

/**
 * \brief
 * This function establishes a connection to a specified socket.
 *
 *   \param [in]  socket int,  Descriptor identifying an unconnected socket.
 *   \param [in]  addr  IPCSOCK_ADDR, Address of the socket to which the connection should be established.
 *   \param [in]  addrlen int, Length of the address.
 *   \return   On failed- a value of IPC_ERROR is returned, and a specific error code can be retrieved by calling ipcgetlasterror.
 *             On success-return 0;
 *
 *\par Remark
 * If the  socket  is of  type IPC_CL_SOCK (a connectionless socket) then the addr is the address to which packets
 *are sent by default.  If the socket is of type IPC_CO_SOCK, this call attempts to make a connection  to  another  socket.
 *The other socket is specified by addr, which is an address (of length addrlen).
 *Generally, connection-based protocol sockets may successfully connect only once; connectionless protocol sockets may
 *use  connect  multiple times  to  change  their association.  Sockets may dis-solve the association/connection by connecting
 *to an address with a service ID (ipc_service ) set to  IPC_SERVICE_DISCONNECT.
 *The specified socket must be bound using ipcbind before connect is called.
 *
 * \par Error Code:
 * - \b  IPCECONNREFUSED        No one listening on the remote address. Or the connection request queue is full.
 * - \b  IPCEISCONN     The socket is already connected.
 * - \b  IPCETIMEDOUT   Timeout while attempting connection. The server may be too busy to accept new connections.
 * - \b  IPCENOTINIT    ipcinit was not called before making this call
 * - \b  IPCENOTSOCK    The descriptor is not an IPC socket.
 * - \b  IPCENOBUFS     Resources could not be found to create new connection
 * - \b  IPCEADDRNOTVAL         The remote/destination address is not a valid address.
 * - \b  IPCEINVAL      The socket was not bounded using bind before connect Or address length is invalid.
 */
int ipcconnect(int socket, struct IPCSOCK_ADDR *addr, int addrlen);

/**
 * \brief
 * This function sends data to a specific IPC destination without copying the data buffer.
 * Both connectionless and connection oriented sockets may be used as long as connect is called first.
 *
 *   \param [in]  socket int,  Descriptor identifying  an IPC socket.
 *   \param [in]  ptr const char,Buffer containing the data to be transmitted.
 *   \param [in]  len int, Length of the data in the buf parameter.
 *   \param [in]  flags int,Indicator specifying the way in which the call is made.
 *   \return If no error occurs, this function returns the total number of bytes sent,
 *           which should be equal to the number indicated by len. It is impossible
 *           that partial data is sent successfully. If an error occurs, a value of
 *           IPC_ERROR is returned, and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * These functions are used to write outgoing data on an ipc socket. If the target sockets queue is
 * full a call to ipcsend will block unless the call is made in a non-blocking mode (see flags).
 * In non-blocking mode if a queue full condition is detected the error IPCEWOULDBLOCK will be returned.
 * If the target socket is connectionless and resides in a remote node ipcsend will not block and the packet
 * may be discarded if the target sockets queue is full.  For connection oriented sockets the ipcselect
 * function can be used to determine when it is possible to send more data.   Connectionless sockets may
 * register for flow control notification using ipcnotify or ipcpsnotify to prevent packet loss and achieve flow control.
 * For successful calls to ipczsend, the calling task no longer owns this buffer and is not responsible
 * for freeing it.  For failed calls (return IPC_ERROR), the calling task still owns the buffer so it can try again or free it.
 *
 * \par Error Code:
 * - \b  IPCNONBLOCK     Forces non-blocking operation.
 * - \b  IPCPRIO         Request that the message be treated with higher priority, priority message order is
 *                 preserved as a separate queue is used for priority messages and messages are inserted in FIFO order.
 * - \b  IPCROSPRIO      Request that the message be treated with higher priority but the message will be inserter into
 *                   the priority queue in LIFO order. So new priority messages will be available first.
 * - \b  IPCRELIABLE    When this flag will ensure that IPC_CO_SOCK type sockets are reliably delivered.
 *                    By default IPC_CO_SOCK type sockets only provide flow control.
 * - \b  IPCPAYLOADCRC   Add checksum for entire payload
 */

int ipczsend(int socket, const char* ptr, int len, int flags);

/**
 * \brief
 * This function receives data using zero copy
 *
 *   \param [in]  sock  int,  Descriptor identifying a bound socket.
 *   \param [out] ptr char,Buffer pointer for the incoming data to allow zero copy operation
 *   \param [in]  flags int,Indicator specifying the way in which the call is made.
 *   \return If no error occurs, this function returns the number of bytes received.
 *           If the connection has been gracefully closed, the return value is zero.
 *           If an error occurs, a value of IPCSOCKET_ERROR is returned, and a specific
 *           error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * These functions read incoming data on ipc sockets and captures the address from which the data was sent.
 * The local address of the socket must be known.  This is usually done explicitly through ipcbind.
 * Data is extracted from the first enqueued message, up to the size of the buffer supplied. If the message/packet
 * is larger than the buffer supplied, the buffer is filled with the first part of the message and ipcrecv generates
 * the error IPCEMSGSIZE.  If no incoming data is available at the socket, the ipcrecv or ipczrecv function block
 * and wait unless a nonblocking flag is specified. In this case, a value of IPC_ERROR is returned with the error code
 * set to IPCEWOULDBLOCK. The ipcselect function can be used to determine when more data arrives.
 * For successful calls to ipczrecv, the calling task now owns the new buffer that was passed to it.
 * It is responsible for freeing this buffer when it is done with it.  For failed calls, no new buffer is received.
 * The flags parameter can be used to influence the behavior of the function invocation beyond the options specified
 * for the associated socket. The semantics of the ipcrecv or ipczrecv functions are determined by the ipc socket
 * type and the flags parameter. The following table shows the values can be used to construct the flags parameter.
 * The bitwise OR operator is applied to these values.
 * - \b IPCNONBLOCK     Forces non-blocking operation.
 * - \b IPCROSPRIO      Block until a priority message is received, queue all other messages.
 * - \b IPCROSCHKMSG    Return pointer to message without de-queuing it.
 *
 * \par Error Code:
 * - \b  IPCENOTINIT    Ipcinit was not called before making this call
 * - \b  IPCEFAULT      The buf parameters are not part of the user address space
 * - \b  IPCEINVAL      The IPC socket has not been bound with ipcbind, an unknown flag was specified, or len was zero or negative.
 * - \b  IPCENOTSOCK    The descriptor is not a socket.
 * - \b  IPCESHUTDOWN   The IPC socket has been shut down. It is not possible to call ipcrecv or ipczrecv on a socket after
 *                       ipcshutdown has been invoked with how set to IPCSD_RECEIVE or IPCSD_BOTH.
 * - \b  IPCEWOULDBLOCK         The operation is marked as nonblocking and the ipcrecv or ipczrecv operation would block.
 * - \b  IPCEMSGSIZE    The message was too large to fit into the specified buffer and was truncated.
 * - \b  IPCENOTCONN    The socket s is not connected or the connection is lost.
 */

int ipczrecv(int sock, char** ptr, int flags);

/**
 * \brief
 * This function sends data to a specific IPC destination on a connected socket.
 * Both CL and  CO sockets may be used as long as connect is called first.
 *
 *   \param [in]  sock  int,  Descriptor identifying  an IPC socket.
 *   \param [in]  buf  const char,Buffer containing the data to be transmitted.
 *   \param [in]  len int, Length of the data in the buf parameter.
 *   \param [in]  flags int,Indicator specifying the way in which the call is made.
 *   \return If no error occurs, this function returns the total number of bytes sent,
 *           which should be equal to the number indicated by len. It is impossible
 *           that partial data is sent successfully. If an error occurs, a value of
 *           IPC_ERROR is returned, and a specific error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * These functions are used to write outgoing data on an ipc socket. If the target sockets queue is
 * full a call to ipcsend will block unless the call is made in a non-blocking mode (see flags).
 * In non-blocking mode if a queue full condition is detected the error IPCEWOULDBLOCK will be returned.
 * If the target socket is connectionless and resides in a remote node ipcsend will not block and the packet
 * may be discarded if the target sockets queue is full.  For connection oriented sockets the ipcselect
 * function can be used to determine when it is possible to send more data.   Connectionless sockets may
 * register for flow control notification using ipcnotify or ipcpsnotify to prevent packet loss and achieve flow control.
 *
 * \par Error Code:
 * - \b  IPCNONBLOCK     Forces non-blocking operation.
 * - \b  IPCPRIO         Request that the message be treated with higher priority, priority message order is
 *                 preserved as a separate queue is used for priority messages and messages are inserted in FIFO order.
 * - \b  IPCROSPRIO      Request that the message be treated with higher priority but the message will be inserter into
 *                   the priority queue in LIFO order. So new priority messages will be available first.
 * - \b  IPCRELIABLE    When this flag will ensure that IPC_CO_SOCK type sockets are reliably delivered.
 *                    By default IPC_CO_SOCK type sockets only provide flow control.
 * - \b  IPCPAYLOADCRC   Add checksum for entire payload
 */

int ipcsend(int sock,
 			const char * buf, 
 			int len,
   			int flags);

/**
 * \brief
 * This function receives ipc data.
 *
 *   \param [in]  sock  int,  Descriptor identifying a bound socket.
 *   \param [out] buf char,Buffer pointer for the incoming data to allow zero copy operation
 *   \param [in]  len int,  Length of the buf parameter.
 *   \param [in]  flags int,Indicator specifying the way in which the call is made.
 *   \return If no error occurs, this function returns the number of bytes received.
 *           If the connection has been gracefully closed, the return value is zero.
 *           If an error occurs, a value of IPCSOCKET_ERROR is returned, and a specific
 *           error code can be retrieved by calling Ipcgetlasterror.
 *
 *\par Remark
 * This function read incoming data on ipc sockets and captures the address from which the data was sent.
 * The local address of the socket must be known.  This is usually done explicitly through ipcbind.
 * Data is extracted from the first enqueued message, up to the size of the buffer supplied. If the message/packet
 * is larger than the buffer supplied, the buffer is filled with the first part of the message and ipcrecv generates
 * the error IPCEMSGSIZE.  If no incoming data is available at the socket, the ipcrecv or ipczrecv function block
 * and wait unless a nonblocking flag is specified. In this case, a value of IPC_ERROR is returned with the error code
 * set to IPCEWOULDBLOCK. The ipcselect function can be used to determine when more data arrives.
 * For successful calls to ipczrecv, the calling task now owns the new buffer that was passed to it.
 * It is responsible for freeing this buffer when it is done with it.  For failed calls, no new buffer is received.
 * The flags parameter can be used to influence the behavior of the function invocation beyond the options specified
 * for the associated socket. The semantics of the ipcrecv or ipczrecv functions are determined by the ipc socket
 * type and the flags parameter. The following table shows the values can be used to construct the flags parameter.
 * The bitwise OR operator is applied to these values.
 * - \b IPCNONBLOCK     Forces non-blocking operation.
 * - \b IPCROSPRIO      Block until a priority message is received, queue all other messages.
 * - \b IPCROSCHKMSG    Return pointer to message without de-queuing it.
 *
 * \par Error Code:
 * - \b  IPCENOTINIT    Ipcinit was not called before making this call
 * - \b  IPCEFAULT      The buf parameters are not part of the user address space
 * - \b  IPCEINVAL      The IPC socket has not been bound with ipcbind, an unknown flag was specified, or len was zero or negative.
 * - \b  IPCENOTSOCK    The descriptor is not a socket.
 * - \b  IPCESHUTDOWN   The IPC socket has been shut down. It is not possible to call ipcrecv or ipczrecv on a socket after
 *                       ipcshutdown has been invoked with how set to IPCSD_RECEIVE or IPCSD_BOTH.
 * - \b  IPCEWOULDBLOCK         The operation is marked as nonblocking and the ipcrecv or ipczrecv operation would block.
 * - \b  IPCEMSGSIZE    The message was too large to fit into the specified buffer and was truncated.
 * - \b  IPCENOTCONN    The socket s is not connected or the connection is lost.
 */

int ipcrecv(int sock,
  			char * buf,
 			int len,
 			int flags);


/*  Memory management   */
/**
 * \brief
 * This function allocate IPC related zero copy buffers used with the
 *          ipczsendto and ipczrecvfrom api calls.
 *
 *   \param [in]  buf_leng  long, Size of buffer in bytes.
 *   \param [in]  flag  int, Modifies operation of allocator (see remarks below).
 *   \param [in]  wait_time  int, wait_time parameter is the time (usecs) between allocation retries.
 *
 *   \return On failed-  IPC_ERROR is returned, and a specific error code can be retrieved
 *           by calling Ipcgetlasterror.
 *
 *\par Remark
 * The API call ipczalloc allocates ipc zero copy memory buffers.  The flag parameter allows allocation of buffers
 * from the specified shared memory pool.  If the flag parameter is set to IPC_PHYSSHM, the buffer will be allocated
 * from physical shared memory.  IPC_PHYSSHM memory is sharable by the calling process any process with in the local
 * machine and possibly other processors connected to physical shared memory (if the destination address used in
 * ipczsendto is to a service located on a system connected through physical shared memory). If the flag parameter
 * is set to IPC_PROCSHM, the buffer will be allocated from shared memory allowing zero copy operation from the calling
 * process to any other task in the local machine including the entire IPC stack, but not physical shared memory (memory
 * shared by two independent systems).
 * The wait_time parameter is not implemented at this time, just pass a value of 0.
 * The ipczcalloc() function allocates space from a contiguous  buffer of nelem elements of size sz.  The space is initialized to zeroes.
 *
 * \par Error Code:
 * - \b  IPCENOSHMEM    Out of shared memory buffers from the specified shared memory pool.
 * - \b  IPCENOTINIT    Ipcinit was not called before making this call
 * - \b  IPCEBADBUF     Error is invalid buffer is passed to IPCZFREE
 * - \b  IPCEINVAL      Invalid buffer length.
 * - \b  IPCEFLAG       Invalid value passed in the flag field. Must specify the correct shared memory flags.
 *
 */

void* ipczalloc(long buf_leng,
                int flag,
                int wait_time);

/**
 * \brief
 * This function sends data to a specific IPC destination without copying the data buffer.
 *
 *   \param [in]  ipc_buf_ptr void, Pointer to ipc zero copy buffer.
 *   \return If an error occurs, a value of  IPC_ERROR is returned, and a specific error code can
 *           be retrieved by calling Ipcgetlasterror.
 *
 * \par Error Code:
 * - \b  IPCENOSHMEM    Out of shared memory buffers from the specified shared memory pool.
 * - \b  IPCENOTINIT    Ipcinit was not called before making this call
 * - \b  IPCEBADBUF     Error is invalid buffer is passed to IPCZFREE
 * - \b  IPCEINVAL      Invalid buffer length.
 * - \b  IPCEFLAG       Invalid value passed in the flag field. Must specify the correct shared memory flags.
 *
 */

int ipczfree(void* ipc_buf_ptr);

/**
 * \brief
 * Allocate nelem blocks of shared memory, each has the length sz.
 *   The allocated memory is set to 0.
 * \par Remark:
 *  Please refer to ipczalloc().
 */

void* ipczcalloc(long nelem,
                 long sz,
                 int flag,
                 int wait_time);

/**
 * \brief
 * This function obtains the type of shared memory that may be used to reach a specified node.
 *
 *   \param [in]  ipc_address Address of desired service.
 *   \param [out]  ipc_memtype ipc_memtype type of shared memory available for zero-copy purposes.
 *
 *   \return If an error occurs, a value of IPC_ERROR is returned, and a specific error code can
 *  be retrieved by calling ipcgetlasterror.
 *
 \par Remark:
 * The parameter ipc_memtype returns the type of memory that the component must use for zero-copy purposes.
 * This is a bit wise or of one of the following:  IPC_PHYSSHM, IPC_PROCSHM.  IPC_PHYSHM is physical shared
 * memory between two nodes. IPC_PROCSHM is shared memory between two tasks in the same node.  If zero is
 * returned in ipc_memtype then there is no shared memory is available to communicate to this node.
 *
 * \par Error Code:
 * - \b  IPCEADDRNOTVAL The specified IPC address is not a valid address for this machine.
 * - \b  IPCENOTINIT    ipcinit was not called before making this call
 *
 */

int ipcgetmemtype(const struct IPCSOCK_ADDR* ipc_address,
                  unsigned long* ipc_memtype);


#ifdef __cplusplus
}
#endif

#endif /*IPC_DEFS_H*/
