/*****************************************************************************************
*                Template No. SWF0114   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA
*
*
*   The GNU C Library is free software; you can redistribute it and/or
*   modify it under the terms of the GNU Lesser General Public
*   License as published by the Free Software Foundation; version 2.1 of 
*   the License.
*
*   This Library is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
*   more details.
*
*   You should have received a copy of the GNU Lesser General Public License along
*   with this Library; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
******************************************************************************************
*
*   FILE NAME: ipc_ioctl.h
*
*------------------------------------- PURPOSE -------------------------------------------

*-------------------------------- HEADER FILE INCLUDES -----------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author               Comments
* ---------  -------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
* 13Jul2007  Motorola Inc.         Added checking for __cplusplus
*****************************************************************************************/
#ifndef _IPC_IOCTL_H_
#define _IPC_IOCTL_H_

#include "ipc_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct tag_ipcgetversionCmd_t
{
    unsigned long version; /* out */
} ipcgetversionCmd_t;

typedef struct tag_ipclocateserviceCmd_t
{
    unsigned long nodeId;
    unsigned short srvId;
    struct IPCSERVICE_LOCATION* serviceTable;
    unsigned long sizeofservicetable; /* out */
    unsigned long maxsizeofservicetable;
} ipclocateserviceCmd_t;

typedef struct tag_ipcsocketCmd_t
{
    int af;
    int type;
    int protocol;
} ipcsocketCmd_t;

typedef struct tag_ipcbindCmd_t
{
    const struct IPCSOCK_ADDR* addr;
} ipcbindCmd_t;

typedef struct tag_ipcgetsocknameCmd_t
{
    struct IPCSOCK_ADDR* name;
} ipcgetsocknameCmd_t;

typedef struct tag_ipcgetpeernameCmd_t
{
    struct IPCSOCK_ADDR* name;
} ipcgetpeernameCmd_t;

typedef struct tag_ipcshutdownCmd_t
{
    int how;
} ipcshutdownCmd_t;

typedef struct tag_ipcsetsockqlenCmd_t
{
    int length;
} ipcsetsockqlenCmd_t;

typedef struct tag_ipcchanCmd_t
{
    int qos;
    int flag;
} ipcchanCmd_t;

typedef  struct tag_ipcsetwatermarkCmd_t
{
    int highwmark;
    int lowwmark;
} ipcsetwatermarkCmd_t;

typedef struct tag_ipczsendtoCmd_t
{
    int flag;
    const char *zbuffer;
    int dataLen;
    const struct IPCSOCK_ADDR* addr;
} ipczsendtoCmd_t;

typedef struct tag_ipczrecvfromCmd_t
{
    char *zbuffer; /* out */
    int flag;
    struct IPCSOCK_ADDR* addr;
} ipczrecvfromCmd_t;

typedef struct tag_ipcgetsockoptCmd_t
{
    int level;
    int optname;
    char *optval;
    int *optlen;
} ipcgetsockoptCmd_t;

typedef struct tag_ipcshmallocCmd_t
{
    size_t size;
    char *zbuffer; /* out */
    int flags;
} ipcshmallocCmd_t;

typedef struct tag_ipcshmfreeCmd_t
{
    char *zbuffer;
} ipcshmfreeCmd_t;

typedef struct tag_ipczcallocCmd_t
{
    size_t nelem;
    size_t size;
    char *zbuffer;  /* out */
    int flags;
} ipczcallocCmd_t;

typedef struct tag_ipcgetstatusCmd_t
{
} ipcgetstatusCmd_t;

typedef struct tag_ipcgetshminfoCmd_t
{
    char *phy_shm_start_addr;
    unsigned long phy_shm_size;
    unsigned long max_phy_shm_blk_size;
    char *virt_shm_start_addr;
    unsigned long virt_shm_size;
} ipcgetshminfoCmd_t;

typedef struct tag_ipcnotifyCmd_t
{
    const struct IPCSOCK_ADDR *name;
    char msg[IPC_NOTIFICATION_MAX_SIZE];
    int msglen;
    int flags;
} ipcnotifyCmd_t;

typedef struct tag_ipcgetmemtypeCmd_t
{
    unsigned long dest_node;
    unsigned long *memtype;
} ipcgetmemtypeCmd_t;

typedef struct tag_ipclistenCmd_t
{
    int backlog;
} ipclistenCmd_t;

typedef struct tag_ipcacceptCmd_t
{
    struct IPCSOCK_ADDR* addr;
    int *needNewfd; /* out, whether we need to create a new fd */
} ipcacceptCmd_t;

typedef struct tag_ipcconnectCmd_t
{
    struct IPCSOCK_ADDR* addr;
} ipcconnectCmd_t;

typedef struct tag_ipczsendCmd_t
{
    const char *zbuffer;
    int len;
    int flag;
} ipczsendCmd_t;

typedef struct tag_ipczrecvCmd_t
{
    char *zbuffer;
    int flag;
} ipczrecvCmd_t;

typedef struct tag_ipcsetprivatedataCmd_t
{
    /* the private data is the socket pointer */
    int privateData; 
} ipcsetprivatedataCmd_t;


typedef struct tag_ipcsendCmd_t
{
    const char *buffer;
    int datalen;
    int flags;
} ipcsendCmd_t;

typedef struct tag_ipcsendtoCmd_t
{
    const char *buffer;
    int datalen;
    int flags;
    const struct IPCSOCK_ADDR *addr;
} ipcsendtoCmd_t;

typedef struct tag_ipcrecvCmd_t
{
    char *buffer;
    int datalen;
    int flags;
} ipcrecvCmd_t;

typedef struct tag_ipcrecvfromCmd_t
{
    char *buffer;
    int datalen;
    int flags;
    struct IPCSOCK_ADDR *addr;
} ipcrecvfromCmd_t;

typedef struct tag_ipcCmd_t
{
    int lasterror;
    union
    {
        /* IPC stack connection initialization */
        ipcgetstatusCmd_t getstatus;
        ipcgetversionCmd_t getversion;

        /* Socket and channel management */
        ipcsocketCmd_t socket;
        ipcbindCmd_t bind;
        ipcgetsocknameCmd_t getsockname;
        ipcgetpeernameCmd_t getpeername;
        ipcshutdownCmd_t shutdown;
        ipcsetsockqlenCmd_t setsockqlen;
        ipcconnectCmd_t connect;
        ipclistenCmd_t listen;
        ipcacceptCmd_t accept;
        ipcchanCmd_t chan;

        /* Service management */
        ipclocateserviceCmd_t locateservice;
        ipcsetwatermarkCmd_t setwatermark;
        ipcnotifyCmd_t notify;

        /* Data transfer */
        ipczsendtoCmd_t zsendto;
        ipczrecvfromCmd_t zrecvfrom;
        ipczsendCmd_t zsend;
        ipczrecvCmd_t zrecv;
        ipcsendCmd_t send;
        ipcsendtoCmd_t sendto;
        ipcrecvCmd_t recv;
        ipcrecvfromCmd_t recvfrom;

        /* Shared memory */
        ipcshmallocCmd_t shmalloc;
        ipcshmfreeCmd_t shmfree;
        ipcgetshminfoCmd_t getshminfo;
        ipcgetmemtypeCmd_t getmemtype;
        ipczcallocCmd_t zcalloc;

        /* internal used commands */
        ipcsetprivatedataCmd_t setprivatedata;
    }u;
}ipcCmd_t;

#ifdef __cplusplus
}
#endif

#define IPC_MAGIC 0X7B

/* IPC stack connection/initialization */
#define CMD_IPCGETSTATUS        _IOR(IPC_MAGIC, 1, ipcCmd_t)
#define CMD_IPCGETVERSION       _IOR(IPC_MAGIC, 2, ipcCmd_t)

/* Socket and channel management */
#define CMD_IPCSOCKET           _IOW(IPC_MAGIC, 6, ipcCmd_t)
#define CMD_IPCBIND             _IOR(IPC_MAGIC, 7, ipcCmd_t)
#define CMD_IPCGETSOCKNAME      _IOW(IPC_MAGIC, 8, ipcCmd_t)
#define CMD_IPCSHUTDOWN         _IOW(IPC_MAGIC, 9, ipcCmd_t)
#define CMD_IPCSETSOCKQLEN      _IOW(IPC_MAGIC, 10, ipcCmd_t)
#define CMD_IPCCONNECT          _IOW(IPC_MAGIC, 11, ipcCmd_t)
#define CMD_IPCLISTEN           _IOW(IPC_MAGIC, 12, ipcCmd_t)
#define CMD_IPCACCEPT           _IOW(IPC_MAGIC, 13, ipcCmd_t)
#define CMD_IPCCHAN             _IOW(IPC_MAGIC, 14, ipcCmd_t)
#define CMD_IPCGETPEERNAME      _IOW(IPC_MAGIC, 15, ipcCmd_t)

/* service management */
#define CMD_IPCLOCATESERVICE    _IOW(IPC_MAGIC, 20, ipcCmd_t)
#define CMD_IPCSETWATERMARK     _IOW(IPC_MAGIC, 21, ipcCmd_t)
#define CMD_IPCNOTIFY           _IOW(IPC_MAGIC, 22, ipcCmd_t)

/* Data Transfer */
#define CMD_IPCZSENDTO          _IOW(IPC_MAGIC, 30, ipcCmd_t)
#define CMD_IPCZRECVFROM        _IOR(IPC_MAGIC, 31, ipcCmd_t)
#define CMD_IPCZSEND            _IOW(IPC_MAGIC, 32, ipcCmd_t)
#define CMD_IPCZRECV            _IOR(IPC_MAGIC, 33, ipcCmd_t)
#define CMD_IPCSENDTO           _IOW(IPC_MAGIC, 34, ipcCmd_t)
#define CMD_IPCRECVFROM         _IOR(IPC_MAGIC, 35, ipcCmd_t)
#define CMD_IPCSEND             _IOW(IPC_MAGIC, 36, ipcCmd_t)
#define CMD_IPCRECV             _IOR(IPC_MAGIC, 37, ipcCmd_t)

/* Memory Management */
#define CMD_IPCSHMALLOC         _IOW(IPC_MAGIC, 40, ipcCmd_t)
#define CMD_IPCSHMFREE          _IOW(IPC_MAGIC, 41, ipcCmd_t)
#define CMD_IPCGETSHMINFO       _IOR(IPC_MAGIC, 42, ipcCmd_t)
#define CMD_IPCGETMEMTYPE       _IOR(IPC_MAGIC, 43, ipcCmd_t)
#define CMD_IPCZCALLOC          _IOR(IPC_MAGIC, 44, ipcCmd_t)


/* Internal */
#define CMD_IPCSETPRIVATEDATA   _IOW(IPC_MAGIC, 50, ipcCmd_t)

#endif /* _IPC_IOCTL_H_ */
