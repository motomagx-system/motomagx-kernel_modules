/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_last_error_hash.c
*   FUNCTION NAME(S): ipcClearLastError
*                     ipcCloseErrorTable
*                     ipcGetLastErrorImp
*                     ipcOpenErrorTable
*                     ipcSetLastError
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements a platform independent method of storing and retreiving
*   an error code in a thread-safe manner.  It should only be used if the 
*   platform does not already support this functionality.  For example, Windows
*   already has equivalent SetLastError and GetLastError functions.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation 
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_osal.h"
#include "ipc_hash.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define HASH_SIZE_FOR_ERROR_TABLE 11

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/

static BOOL ipcErrorTableInitialized = FALSE;
static ipcHashTable_t ipcThreadSpecificErrorTable;
static ipcMutexHandle_t ipcErrorTableMutex;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOpenErrorTable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called to initialize the thread-specific error table that
*   stores the last error code for each thread.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: TRUE if successful
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcOpenErrorTable(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcHashInit(&ipcThreadSpecificErrorTable, 
        HASH_SIZE_FOR_ERROR_TABLE, NULL))
    {
        return FALSE;
    }
    if (ipcOsalMutexOpen(&ipcErrorTableMutex) == IPCE_OK)
    {
        ipcErrorTableInitialized = TRUE;
        return TRUE;
    }

    ipcError(IPCLOG_GENERAL, ("Unable to open thread-specific error table\n"));
    ipcHashDestroy(&ipcThreadSpecificErrorTable);
    return FALSE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCloseErrorTable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called to close the thread-specific error table and free
*   the memory that was allocated to it.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcCloseErrorTable(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcErrorTableInitialized)
    {
        ipcErrorTableInitialized = FALSE;
        ipcHashDestroy(&ipcThreadSpecificErrorTable);
        ipcOsalMutexClose(ipcErrorTableMutex);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSetLastError
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the platform-independent function that can be used to store an
*   error code for the current thread.
*   
*   PARAMETER(S):
*       ipc_error: the error code to store
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcSetLastError(int ipc_error)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcErrorTableInitialized)
    {
        ipcOsalMutexLock(ipcErrorTableMutex);
        ipcHashRemove(&ipcThreadSpecificErrorTable, (void *)ipcOsalGetThreadId());
        ipcHashPut(&ipcThreadSpecificErrorTable, 
                (void *)ipcOsalGetThreadId(), (void *)ipc_error);
        ipcOsalMutexRelease(ipcErrorTableMutex);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcClearLastError
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the platform-independent function that can be used to clear an
*   error code for the curr thread.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcClearLastError(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcErrorTableInitialized)
    {
        ipcOsalMutexLock(ipcErrorTableMutex);
        /* 
         * BUG: What the matter with the following line?
         * The linux system panics on this line, but why????
         */
        //ipcHashRemove(&ipcThreadSpecificErrorTable, (void *)ipcOsalGetThreadId());
        ipcOsalMutexRelease(ipcErrorTableMutex);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcGetLastErrorImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the platform-independent function that can be used to retreive the
*   last error code for the current thread.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: the last error code for current thread
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcGetLastErrorImp(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int error = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcErrorTableInitialized)
    {
        ipcOsalMutexLock(ipcErrorTableMutex);
        error = (int)ipcHashFind(&ipcThreadSpecificErrorTable, (void *)ipcOsalGetThreadId());
        ipcOsalMutexRelease(ipcErrorTableMutex);
    }
    
    return error;
}
