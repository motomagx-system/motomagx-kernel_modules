/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2005 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_hash.c
*   FUNCTION NAME(S): ipcHashDefaultHashFunc
*                     ipcHashDestroy
*                     ipcHashFind
*                     ipcHashGetNext
*                     ipcHashGetSize
*                     ipcHashInit
*                     ipcHashPointerHashFunc
*                     ipcHashPut
*                     ipcHashRemove
*                     ipcHashRemoveAll
*                     ipcHashResetIterator
*                     ipcListHashKeyCompare
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This is the implementation of the hash utility. Below is the data structure
*   of the has table:
*   |-----------------|
*   |    Chunk 0      |--->[object]--->[object]--->[object]
*   |-----------------|
*   |    Chunk 1      |--->[object]--->[object]
*   |-----------------|
*   .                 .
*   .                 .
*   .                 .
*   |-----------------|
*   |    Chunk n      |--->[object]--->[object]--->[object]
*   |-----------------|
*   A hash table is an array of chunks, each is a list of objects.
*
*   The function ipcHashInit initializes a hash table. ipcHashDestroy destroys
*   it.
*   Function ipcHashPut is is used for putting objects into the hash table,
*   both key and object should be specified, a hash_func, which is specified on
*   ipcHashInit, is applied to key to calculate the chunk number, then the object
*   is appended to the end of the list for that chunk.
*   
*   ipcHashFind is used to get an object previously stored in the hash table.
*   This function needs a key to get the object, again, the hash_func is applied
*   to the key to calculate the hash code, it finds the object in the list 
*   corresponding for that hash code. If no object is found, NULL is returned.
*
*   The functions ipcHashResetIterator and ipcHashGetNext are used to traverse
*   the hash table.
*
*   To remove an object in the hashtable, ipcHashRemove is called, it expects
*   a key to find the value.
*   ipcHashGetSize returns the size of the hashtable.
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation
* 13Jan2005  Motorola Inc.         Separated keys from objects
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_hash.h"
#include "ipc_osal.h"
#include "ipc_common.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initialize a hash table.
*   
*   PARAMETER(S):
*       table: The pointer to the hash table to be initialized
*       nChunks: Number of buckets for the hashtable
*       hash_func: The hash function used for putting and finding objects
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial creation
* 13Jan2005  Motorola Inc.         Separated keys from objects
* 15Apr2005  Motorola Inc.         Check return value of ipcAlloc()
* 22Aug2005  Motorola Inc.         Return false if out of memory
*
*****************************************************************************************/
BOOL ipcHashInit(ipcHashTable_t* table,
                 unsigned int nChunks,
                 ipcHashFunc_t hash_func)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    table->nChunks = ipcMax(1, nChunks);
    table->hash_func = (hash_func != NULL) ? hash_func : ipcHashDefaultHashFunc;
    table->nItems = 0;
    table->currentChunk = 0;
    table->chunks = (ipcHashLink_t *)ipcAlloc(nChunks * sizeof(ipcHashLink_t), 0);
    if (NULL == table->chunks)
    {
        ipcError(IPCLOG_GENERAL, ("Out of memory in ipcHashInit\n"));
        return FALSE;
    }

    memset(table->chunks, 0, nChunks * sizeof(ipcHashLink_t));
    return TRUE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Destroy a hash table, release the occupied memory.
*   After this function, the hash table COULD NOT be used unless you call
*   ipcHashInit again.
*   
*   PARAMETER(S):
*       table: the hashtable to destroy.
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation
* 13Jan2005  Motorola Inc.         Separated keys from objects
*
*****************************************************************************************/
void ipcHashDestroy(ipcHashTable_t* table)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcHashRemoveAll(table);

    if (table->chunks != NULL)
    {
        ipcFree(table->chunks);
    }

    memset(table, 0, sizeof(ipcHashTable_t));
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashPut
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Put an object into the hash table.
*   The hash code of the object is calculated using table->hash_func
*   
*   PARAMETER(S):
*       table: The hash table to store the data
*       key: The key of the object
*       object: Pointer to the object to be stored in the hashtable
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation
* 04Jan2005  Motorola Inc.         Removed iterators in ipcList_t
* 13Jan2005  Motorola Inc.         Separated keys from objects
*
*****************************************************************************************/
BOOL ipcHashPut(ipcHashTable_t* table, void *key, void* object)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned int n;
    ipcHashLink_t newLink;
/*--------------------------------------- CODE -----------------------------------------*/
    n = table->hash_func(key) % table->nChunks;
    newLink = (ipcHashLink_t)ipcAlloc(sizeof(ipcHashItem_t), 0);
    if (newLink != NULL)
    {
        newLink->key = key;
        newLink->object = object;
        newLink->next = table->chunks[n];
        table->chunks[n] = newLink;
        table->nItems++;
        return TRUE;
    }
    return FALSE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashFind
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Find An object from the hash table, by the key specified.
*   
*   PARAMETER(S):
*       table: The hashtable to look up
*       key: The key for the object to find
*       
*   RETURN: If found, returns the object corresponding to the key,
*           otherwise NULL.
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation
* 04Jan2005  Motorola Inc.         Removed iterators in ipcList_t
*
*****************************************************************************************/
void* ipcHashFind(ipcHashTable_t* table, void* key)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcList_t tempList;
    ipcListLink_t link;
    void* object = NULL;
    unsigned int n;
/*--------------------------------------- CODE -----------------------------------------*/
    n = table->hash_func(key) % table->nChunks;

    tempList.head = (ipcListLink_t)table->chunks[n];
    link = ipcListFind(&tempList, key, ipcListHashKeyCompare, NULL);
    if (link != NULL)
    {
        /* key was found */
        object = ((ipcHashLink_t)link)->object;
    }

    return object;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashResetIterator
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reset the iterator of the hash table. Call this function before you want to
*   access all objects in the hash table one by one.
*   
*   PARAMETER(S):
*       table: Pointer to the hashtable
*       
*   RETURN: void
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation
* 04Jan2005  Motorola Inc.         Removed iterators in ipcList_t
*
*****************************************************************************************/
void ipcHashResetIterator(ipcHashTable_t* table)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    table->currentChunk = -1;
    table->currentLink = NULL;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashGetNext
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get the next object in hash table.
*   
*   PARAMETER(S):
*       table: the pointer to the hashtable
*       
*   RETURN: The next object in the iteration. Returns NULL if end is reached
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Nov2004  Motorola Inc.         Initial Creation
* 04Jan2005  Motorola Inc.         Removed iterators in ipcList_t
*
*****************************************************************************************/
void* ipcHashGetNext(ipcHashTable_t* table)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    void* obj = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if (table->currentLink != NULL)
    {
        table->currentLink = table->currentLink->next;
    }

    /* If we still have a NULL link we have reached the end of the current
     * chunk and we need to skip over all vacant chunks */
    while (table->currentLink == NULL)
    {
        if (++table->currentChunk >= table->nChunks)
        {
            return NULL;
        }
        table->currentLink = table->chunks[table->currentChunk];
    }

    /* We have found the next link or reached the end of the table so return
     * the object or return NULL */
    if (table->currentLink != NULL)
    {
        obj = table->currentLink->object;
    }
    return obj;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashRemove
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Remove an object from the hash table, identified by key.
*   
*   PARAMETER(S):
*       table: pointer to the hashtable
*       key: The key for the object
*       
*   RETURN: The removed object. If the object is not found, returns NULL.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation
* 04Jan2005  Motorola Inc.         Removed iterators in ipcList_t
* 13Jan2005  Motorola Inc.         Separate keys from objects
*
*****************************************************************************************/
void* ipcHashRemove(ipcHashTable_t* table, void* key)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcList_t tempList;
    ipcListLink_t link;
    void* object = NULL;
    unsigned int n;
/*--------------------------------------- CODE -----------------------------------------*/
    n = table->hash_func(key) % table->nChunks;

    tempList.head = (ipcListLink_t)table->chunks[n];
    link = ipcListRemove(&tempList, key, ipcListHashKeyCompare);
    if (link != NULL)
    {
        /* key was found, free link and return object */
        object = ((ipcHashLink_t)link)->object;
        ipcFree(link);
        --table->nItems;
    }
    table->chunks[n] = (ipcHashLink_t)tempList.head;

    return object;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashRemoveAll
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Remove all the objects from the hash table.
*   
*   PARAMETER(S):
*       table: pointer to the hashtable
*       
*   RETURN: void
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Nov2004  Motorola Inc.         Initial Creation
* 13Jan2004  Motorola Inc.         Separate keys from objects 
*
*****************************************************************************************/
void ipcHashRemoveAll(ipcHashTable_t* table)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned int i = 0;
    ipcList_t tempList;
/*--------------------------------------- CODE -----------------------------------------*/
    for (i = 0; i < table->nChunks;i++)
    {
        tempList.head = (ipcListLink_t)table->chunks[i];
        ipcListFreeAll(&tempList);
        table->chunks[i] = NULL;
    }
    table->nItems = 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashGetSize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get the number of objects stored in the hash table.
*   
*   PARAMETER(S):
*       table: pointer to the hashtable
*       
*   RETURN: Number of objects in the hashtable
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Oct2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned int ipcHashGetSize(ipcHashTable_t* table)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return table->nItems;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashDefaultHashFunc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   The default hash function.
*   This function is used if hash_func is not specified in icpHashInit.
*   
*   PARAMETER(S):
*       key: The hash key.
*       
*   RETURN: return value is identical to the key
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned int ipcHashDefaultHashFunc(void *key)
{
    return (unsigned int)key;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHashPointerHashFunc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   The default hash function for pointers. This function devides the key by 4
*   since most pointers are aligned to 4 byte boundary.
*   
*   PARAMETER(S):
*       key: The hash key.
*       
*   RETURN: hash code for the key
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned int ipcHashPointerHashFunc(void *key)
{
    /* Divide by 4 since most pointers are aligned to 4 byte boundary */
    return (unsigned int)key >> 2;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListHashKeyCompare
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   The comparison function for the IPC lists in hashtable
*   
*   PARAMETER(S):
*       link: hash link to compare to
*       key: key we are searching for
*       
*   RETURN: TRUE if link->key meets the key
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcListHashKeyCompare(ipcListLink_t link, void *key)
{
    return ((ipcHashLink_t)link)->key == key;
}
