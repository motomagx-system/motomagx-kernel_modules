/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_shared_memory.c
*   FUNCTION NAME(S): ipcCreateSMPool
*                     ipcFreeSMBuffer
*                     ipcGetMaxPSMBufferSize
*                     ipcGetSMBuffer
*                     ipcIsPhysicalSharedMemory
*                     ipcSMCalculatePoolTableSize
*                     ipcSMCheckBuffer
*                     ipcSMFinalizePools
*                     ipcSMPoolTableInit
*                     ipcSMReport
*                     ipcSMSetDebugInfo
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 09Jun2005  Motorola Inc.         Updated for Linux
* 02Sep2005  Motorola Inc.         Removed unnecessary NULL pointer checks (need a KW filter instead) Added multicore semaphore
* 15Mar2006  Motorola Inc.         Allow single core to configure ALL of shared memory (both cores)
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure for open source
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 15Jan2007  Motorola Inc.         Include ipc_macro_defs.h
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 23Feb2007  Motorola Inc.         Unify memory allocation functions
* 02Apr2007  Motorola Inc.         Unify memory log format
* 24Apr2007  Motorola Inc.         Dump memory if memory corrupted
* 25May2007  Motorola Inc.         Print error messages with IPCLOG_MEMTRACK
* 05Jul2007  Motorola Inc.         Removed function ipcSMGetTailMark
*
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_macro_defs.h"
#include "ipc_shared_memory.h"
#include "ipc_packet.h"
#include "ipc_defs.h"
#include "ipc_osal.h"
#include "ipclog.h"

#ifdef HAVE_PHYSICAL_SHM /* Do not build if there is no shared memory */
#include "mu_drv.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

#ifndef NULL
#define NULL (void *)0
#endif
/*------------------------------------ ENUMERATIONS ------------------------------------*/

/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

/*------------------------------------- STATIC DATA ------------------------------------*/

/*------------------------------------- GLOBAL DATA ------------------------------------*/

SMPoolTblProp_t *localSMPoolTblProp = 0;
SMPoolTblProp_t *remoteSMPoolTblProp = 0;
long ipcSMOffset = 0;
int ipcIsShmReady = 0;
int shmConnected = 0;
unsigned long maxPSMBufSize = 0;
extern unsigned long  paddr,physical_shm_base;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

/*--------------------------------------- MACROS ---------------------------------------*/
#define findSMPool(table, poolId)   ((ipcSMDesTbl_t *) \
    ( ((poolId) < ipcOsalReadAligned32(&(table)->totalNumOfPools)) ? &(table)->poolDesTables[poolId] : 0 ))

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMPoolTableInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function initializes the shared memory pool table properties structure.
*   It does not create any shared memory pools.  The function ipcCreateSMPool
*   must be called to create each pool afterwards.
*   
*   PARAMETER(S):
*       p: pointer to pool table property structure
*       ownerID: the owner ID for this core
*       SMBytesTotal: the total number of shared memory bytes available to this
*                      core (including memory used by the pool table property structure)
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Jun2005  Motorola Inc.         Updated for Linux
* 12Apr2006  Motorola Inc.         Bug fix for wrong maxPSMBufSize
*
*****************************************************************************************/
void ipcSMPoolTableInit(SMPoolTblProp_t *p, unsigned long ownerID,
                        unsigned long SMBytesTotal, ipcSMPoolConfig_t *poolConfig)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcOsalWriteAligned32(&p->ownerID, ownerID);
    ipcOsalWriteAligned32(&p->SMBytesTotal, SMBytesTotal);
    ipcOsalWriteAligned32(&p->SMBytesRemaining, SMBytesTotal - sizeof(SMPoolTblProp_t));
    ipcOsalWriteAligned32(&p->totalNumOfPools, 0);
    ipcOsalWriteAligned32(&p->nextPoolStart, ipcSMVirtualToPhysical((char *)p + sizeof(SMPoolTblProp_t)));
    ipcOsalWriteAligned32(&p->maxPSMBufSize, 0);

    if (poolConfig != 0)
    {
        for (i = 0; (i < MAX_SM_POOLS) && (poolConfig[i].numBuffers != 0); i++)
        {
            ipcCreateSMPool(p,
                            poolConfig[i].bufSize,
                            poolConfig[i].numBuffers,
                            ownerID);
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMCalculatePoolTableSize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function calculates the total number of bytes required to create all
*   shared memory pools listed in the configuration array.
*   
*   PARAMETER(S):
*       poolConfig: configuration array for shared memory pools
*       
*   RETURN: total number of bytes required for these pools
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Jun2005  Motorola Inc.         Updated for Linux
* 11Oct2006  Motorola Inc.         rollback to 4-bytes boundry
*
*****************************************************************************************/
int ipcSMCalculatePoolTableSize(ipcSMPoolConfig_t *poolConfig)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    int totalSize;
    unsigned long bufSize;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Start with size of the pool table descriptor */
    totalSize = sizeof(SMPoolTblProp_t);

    if (poolConfig != 0)
    {
        for (i = 0; (i < MAX_SM_POOLS) && (poolConfig[i].numBuffers != 0); i++)
        {
            /* round bufSize to the next 32 bit (4 bytes) boundary */
            bufSize = (poolConfig[i].bufSize + 3) & (~3);

            /* each buffer also needs a pointer */
            totalSize += poolConfig[i].numBuffers * (bufSize + sizeof(void *));
        }
    }
    return totalSize;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateSMPool
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function adds a new shared memory pool.  The shared memory mutex must
*   be acquired before calling this function.
*   
*   PARAMETER(S):
*       p: pointer to pool table property structure
*       bufSize: size of pool buffers in bytes
*       numBuffers: number of buffers in the pool
*       ownerID: identifies which core owns this pool
*       
*   RETURN: poolId for success, -1 for failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Jun2005  Motorola Inc.         Updated for Linux
* 30Aug2005  Motorola Inc.         Checked NULL pointer
* 02Sep2005  Motorola Inc.         Removed unnecessary NULL pointer check
* 02Nov2005  Motorola Inc.         Add function ipcSMFinalizePools()
* 23Nov2005  Motorola Inc.         Bug fix for wrong BP physical shared memory pools
* 16Feb2005  Motorola Inc.         Bug fix. ipczalloc() will never return if it tries to allocate a block of physical shared memory with size more than max buffer size.
* 12Apr2006  Motorola Inc.         Bug fix for wrong maxPSMBufSize
* 11Oct2006  Motorola Inc.         rollback to 4-bytes boundry
*
*****************************************************************************************/
int ipcCreateSMPool(SMPoolTblProp_t *p, unsigned long bufSize,
                    unsigned long numBuffers, unsigned long ownerID)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    int poolId;
    unsigned long numPools;
    unsigned long bytesRequired;
    unsigned long bytesRemaining;
    ipcSMDesTbl_t newPool;
    ipcSMBufHead_t *bufferPtr;
    ipcSMBufHead_t **tempPtr;
    void *nextPoolStart;
    unsigned long maxBufSize = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    /* round bufSize to the next 32 bit(4 bytes) boundary */
    bufSize = (bufSize + 3) & (~3);

    /* Calculate number of bytes required */
    bytesRequired = numBuffers * (bufSize + sizeof(void *));

    /* Check if there is room for this new pool */
    numPools = ipcOsalReadAligned32(&p->totalNumOfPools);
    bytesRemaining = ipcOsalReadAligned32(&p->SMBytesRemaining);
    if ((numPools >= MAX_SM_POOLS) || (bytesRemaining < bytesRequired))
    {
        /* error */
        return(-1);
    }

    ++numPools;
    ipcOsalWriteAligned32(&p->totalNumOfPools, numPools);

    /* Point to start of next shared memory pool */
    nextPoolStart = (void *)ipcSMPhysicalToVirtual(ipcOsalReadAligned32(&p->nextPoolStart));
    tempPtr = (ipcSMBufHead_t **)nextPoolStart;
    nextPoolStart = (void *)((char *)nextPoolStart + bytesRequired);
    ipcOsalWriteAligned32(&p->nextPoolStart, ipcSMVirtualToPhysical(nextPoolStart));
    ipcOsalWriteAligned32(&p->SMBytesRemaining, bytesRemaining - bytesRequired);

    /* Initialize pool data */
    bufferPtr = (ipcSMBufHead_t *)(tempPtr + numBuffers);
    ipcOsalWriteAligned32(&newPool.pointerStartAddress, ipcSMVirtualToPhysical(tempPtr));
    ipcOsalWriteAligned32(&newPool.bufferStartAddress, ipcSMVirtualToPhysical(bufferPtr));
    ipcOsalWriteAligned32(&newPool.bufSize, bufSize);
    ipcOsalWriteAligned32(&newPool.numBuffers, numBuffers);
    newPool.freeBufWPtr = newPool.pointerStartAddress;
    newPool.freeBufRPtr = newPool.pointerStartAddress;

    /* Insert this new pool in the correct location (start by finding the index of
     * the first pool with a larger buffer size then the new one). */
    for(poolId = 0; poolId < numPools - 1; poolId++)
    {
        if (ipcOsalReadAligned32(&p->poolDesTables[poolId].bufSize) > bufSize)
        {
            /* We found the correct location for the new pool */
            break;
        }
    }

    /* Loop through remaining pools and shift them over one to make room */
    for (i = numPools - 1; i > poolId; i--)
    {
        p->poolDesTables[i] = p->poolDesTables[i - 1];
    }

    /* Copy new pool data into correct slot */
    p->poolDesTables[poolId] = newPool;

    /* Initialize all buffer headers for all pools */
    ipcSMFinalizePools(p, ownerID);

    maxBufSize = ipcOsalReadAligned32(&p->maxPSMBufSize);
    maxBufSize = (bufSize > maxBufSize) ? bufSize : maxBufSize;
    ipcOsalWriteAligned32(&p->maxPSMBufSize, maxBufSize);

    return(poolId);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMFinalizePools
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function initializes the buffer headers for all pools
*   
*   PARAMETER(S):
*       p: pointer to pool table property structure
*       ownerID: identifies whicih core owns these buffers
*       
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 02Nov2005  Motorola Inc.         Initial Creation
* 23Feb2007  Motorola Inc.         Pass initial refcount to ipcSMInitBuffer
*
*****************************************************************************************/
void ipcSMFinalizePools(SMPoolTblProp_t *p, unsigned long ownerID)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    int poolId;
    unsigned long numPools;
    ipcSMDesTbl_t curPool;
    ipcSMBufHead_t *bufferPtr;
    ipcSMBufHead_t **tempPtr;
    unsigned long bufSize;
    unsigned long numBuffers;
/*--------------------------------------- CODE -----------------------------------------*/
    numPools = ipcOsalReadAligned32(&p->totalNumOfPools);

    for (poolId = 0; poolId < numPools; poolId++)
    {
        curPool = p->poolDesTables[poolId];
        bufSize = ipcOsalReadAligned32(&curPool.bufSize);
        numBuffers = ipcOsalReadAligned32(&curPool.numBuffers);
        tempPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(ipcOsalReadAligned32(&curPool.pointerStartAddress));
        bufferPtr = (ipcSMBufHead_t *)ipcSMPhysicalToVirtual(ipcOsalReadAligned32(&curPool.bufferStartAddress));

        if ((NULL == bufferPtr) || (NULL == tempPtr))
        {
            ipcError(IPCLOG_MEMTRACK, ("ipcSMFinalizePools: null pointer encountered!\n"));
            return;
        }

        for (i = 0; i < numBuffers; i++)
        {
            ipcSMInitBuffer(bufferPtr, IPC_PHYSSHM, poolId, ownerID, 0);
            ipcOsalWriteAligned32(tempPtr, ipcSMVirtualToPhysical(bufferPtr));
            bufferPtr = (ipcSMBufHead_t *)((char *)bufferPtr + bufSize);
            tempPtr++;
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcGetSMBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is to get a poiter to a buffer from physical shared memory
*   
*   PARAMETER(S):
*       bufSize: buffer size (including buffer header bytes)
*       flags: currently ignored
*       
*   RETURN: pointer to a buffer
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Jun2005  Motorola Inc.         Updated for Linux
* 30Aug2005  Motorola Inc.         Checked NULL pointer
* 02Sep2005  Motorola Inc.         Removed unnecessary NULL pointer check
* 02Nov2005  Motorola Inc.         Init the buffer header to set the right pool id and other flags
* 23Feb2007  Motorola Inc.         bufSize passed in now includes header bytes
* 24Apr2007  Motorola Inc.         Dump memory pool on error
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void *ipcGetSMBuffer(unsigned int bufSize, unsigned int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    volatile ipcSMDesTbl_t *desTable;
    ipcSMBufHead_t *buffer = 0;
    ipcSMBufHead_t **bufferPtr;
    unsigned long totalNumOfPools;
    ipcSMBufHead_t **freeBufRPtr;
    void *pointerStartAddress;
    void *bufferStartAddress;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsShmReady)
    {
        return(0);
    }
    
    ipcLockMem(IPC_PHYSSHM);

    totalNumOfPools = ipcOsalReadAligned32(&localSMPoolTblProp->totalNumOfPools);

    /* find a pool with required buf size */
    for(i = 0; i < totalNumOfPools; i++)
    {
        desTable = &localSMPoolTblProp->poolDesTables[i];

        if(bufSize <= ipcOsalReadAligned32(&desTable->bufSize))
        {
            freeBufRPtr = (ipcSMBufHead_t **)ipcOsalReadAligned32(&desTable->freeBufRPtr);
            pointerStartAddress = (void *)ipcOsalReadAligned32(&desTable->pointerStartAddress);
            bufferStartAddress = (void *)ipcOsalReadAligned32(&desTable->bufferStartAddress);

            bufferPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(freeBufRPtr);
            if (NULL == bufferPtr)
            {
                ipcError(IPCLOG_MEMTRACK, ("ipcGetSMBuffer: null pointer encountered!\n"));
                buffer = 0;
                break;
            }

            buffer = (ipcSMBufHead_t *)ipcOsalReadAligned32(bufferPtr);

            /* see if there are any free buffers in the pool */
            if(buffer != 0)
            {
                /* return the pointer to a free bufer */
                buffer = (ipcSMBufHead_t *)ipcSMPhysicalToVirtual(buffer);

                /* check buffer marker */
                if (ipcSMGetMarker(buffer) != BUFHEAD_MARKER)
                {
                    ipcError(IPCLOG_MEMTRACK, ("ipcGetSMBuffer: bad Marker!\n"));
                    buffer = 0;
                    break;
                }

                /* NULL out this buffer */
                ipcOsalWriteAligned32(bufferPtr, 0);

                /* increment pointer and wrap around if needed */
                if (++freeBufRPtr == bufferStartAddress)
                {
                    freeBufRPtr = pointerStartAddress;
                }
                ipcOsalWriteAligned32(&desTable->freeBufRPtr, freeBufRPtr);

                ipcSMSetPoolId(buffer, i);
                break;
            }
        }
    }
    
    ipcUnlockMem(IPC_PHYSSHM);

    return((void *)buffer);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFreeSMBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to free a shared memory buffer
*   
*   PARAMETER(S):
*       buffer
*       
*   RETURN: status (0 for success, error code < 0 otherwise)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Jun2005  Motorola Inc.         Updated for Linux
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 18Aug2006  Motorola Inc.         Adjusted for IPC_RELEASE build
* 23Feb2007  Motorola Inc.         Return SIPC user error codes
* 25Apr2007  Motorola Inc.         Clean up KW warning
* 05Jul2007  Motorola Inc.         Removed function ipcSMGetTailMark
*
*****************************************************************************************/
int ipcFreeSMBuffer(void *buffer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    volatile ipcSMDesTbl_t *desTable;
    ipcSMBufHead_t **bufferPtr;
    int rc = IPC_OK;
    unsigned long poolId;
    unsigned long poolOwner;
    ipcSMBufHead_t **freeBufRPtr;
    ipcSMBufHead_t **freeBufWPtr;
    void *pointerStartAddress;
    void *bufferStartAddress;
/*--------------------------------------- CODE -----------------------------------------*/
    poolId = ipcSMGetPoolId(buffer);
    poolOwner = ipcSMGetPoolOwner(buffer);

    if (poolOwner == ipcOsalReadAligned32(&localSMPoolTblProp->ownerID))
    {
        desTable = findSMPool(localSMPoolTblProp, poolId);
        if (desTable == 0)
        {
            rc = IPCEBADBUF;
        }
        else
        {
            pointerStartAddress = (void *)ipcOsalReadAligned32(&desTable->pointerStartAddress);
            bufferStartAddress = (void *)ipcOsalReadAligned32(&desTable->bufferStartAddress);
            freeBufRPtr = (ipcSMBufHead_t **)ipcOsalReadAligned32(&desTable->freeBufRPtr);

            /* decrement pointer and wrap around if needed*/
            if ((void *)--freeBufRPtr < pointerStartAddress)
            {
                freeBufRPtr = (ipcSMBufHead_t **)bufferStartAddress - 1;
            }
            ipcOsalWriteAligned32(&desTable->freeBufRPtr, freeBufRPtr);

            /* Now add the buffer being freed back into the free list */
            bufferPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(freeBufRPtr);
            if (NULL == bufferPtr)
            {
                ipcError(IPCLOG_MEMTRACK, ("ipcFreeSMBuffer: null pointer encountered!\n"));
                return IPCEBADBUF;
            }

            ipcOsalWriteAligned32(bufferPtr, ipcSMVirtualToPhysical(buffer));
        }

    }
    else if(poolOwner == ipcOsalReadAligned32(&remoteSMPoolTblProp->ownerID))
    {
        desTable = findSMPool(remoteSMPoolTblProp, poolId);
        if (desTable == 0)
        {
            rc = IPCEBADBUF;
        }
        else
        {
            pointerStartAddress = (void *)ipcOsalReadAligned32(&desTable->pointerStartAddress);
            bufferStartAddress = (void *)ipcOsalReadAligned32(&desTable->bufferStartAddress);
            freeBufWPtr = (ipcSMBufHead_t **)ipcOsalReadAligned32(&desTable->freeBufWPtr);

            /* add this buffer back to free list */
            bufferPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(freeBufWPtr);
            if(NULL != bufferPtr)
            {
                ipcOsalWriteAligned32(bufferPtr, ipcSMVirtualToPhysical(buffer));
            }
            
            /* increment pointer and wrap around if needed */
            if (++freeBufWPtr == bufferStartAddress)
            {
                freeBufWPtr = pointerStartAddress;
            }
            ipcOsalWriteAligned32(&desTable->freeBufWPtr, freeBufWPtr);
        }
    }
    else
    {
        /* invalid pool owner */
        rc = IPCEBADBUF;
    }

    return(rc);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMReport
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function outputs the status of all shared memory pools in the specified
*   shared memory pool table.
*   
*   PARAMETER(S):
*       p: pointer to pool table property structure
*       printFn: pointer to function to call to output data
*       
*   RETURN: 0 for success, -1 if an error was found
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 07Jul2005  Motorola Inc.         Initial Creation
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 18Aug2006  Motorola Inc.         Adjusted for IPC_RELEASE build
* 23Feb2007  Motorola Inc.         Renamed ipcSMGetTaskName to ipcOsalGetProcessName
* 02Apr2007  Motorola Inc.         Unify memory log format
* 25Apr2007  Motorola Inc.         Clean up KW warning
* 05Jul2007  Motorola Inc.         Removed function ipcSMGetTailMark
*
*****************************************************************************************/
int ipcSMReport(SMPoolTblProp_t *p, ipcSMPrintFunction_t printFn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int result = 0;
    int i, poolNum;
    int freeBuffers, usedBuffers, numBuffers, bufSize, val;
    unsigned long totalNumOfPools;
    unsigned long freeBufWPtr, freeBufRPtr;
    unsigned long pointerStartAddress, bufferStartAddress;
    ipcSMDesTbl_t *desTable;
    ipcSMBufHead_t *buffer;
    ipcSMBufHead_t **bufferPtr;
    ipcSMBufHead_t **virtualBufferPtr;
#ifndef IPC_RELEASE
    unsigned short pid;
    unsigned long tick, cur_tick, sm_tick;
    int isLocalPool = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    cur_tick = ipcOsalGetTickCount();
#endif /*IPC_RELEASE*/

    if (!ipcIsShmReady)
    {
        printFn("Shared Memory has not been initialized\n");
        return -1;
    }

    printFn("\n------------------- Shared Memory Report --------------------\n");
    printFn("Owner ID = %d\n", ipcOsalReadAligned32(&p->ownerID));

    totalNumOfPools = ipcOsalReadAligned32(&p->totalNumOfPools);
    for (poolNum = 0; poolNum < totalNumOfPools; poolNum++)
    {
        desTable = &p->poolDesTables[poolNum];
        numBuffers = ipcOsalReadAligned32(&desTable->numBuffers);
        bufSize = ipcOsalReadAligned32(&desTable->bufSize);

        printFn("[%d]\tbufSize = %d, numBuffers = %d\n",
            poolNum, bufSize, numBuffers);

        freeBufWPtr = ipcOsalReadAligned32(&desTable->freeBufWPtr);
        freeBufRPtr = ipcOsalReadAligned32(&desTable->freeBufRPtr);
        pointerStartAddress = ipcOsalReadAligned32(&desTable->pointerStartAddress);
        bufferStartAddress = ipcOsalReadAligned32(&desTable->bufferStartAddress);

        if ((freeBufWPtr & 3) ||
            (freeBufRPtr & 3) ||
            (freeBufWPtr < pointerStartAddress) ||
            (freeBufWPtr >= bufferStartAddress) ||
            (freeBufRPtr < pointerStartAddress) ||
            (freeBufRPtr >= bufferStartAddress))
        {
            printFn("\tERROR: freeBufWPtr = %X, freeBufRPtr = %X, valid range = [%X, %X)\n",
                freeBufWPtr, freeBufRPtr, pointerStartAddress, bufferStartAddress);

            result = -1;

            continue;
        }

        freeBuffers = 0;
        usedBuffers = 0;

        bufferPtr = (ipcSMBufHead_t **)freeBufRPtr;
        virtualBufferPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(bufferPtr);
        if (NULL == virtualBufferPtr)
        {
            ipcError(IPCLOG_MEMTRACK, ("ipcSMReport: null pointer encountered!\n"));
            return -1;
        }

        while((NULL != virtualBufferPtr) && ((buffer = (ipcSMBufHead_t *)ipcOsalReadAligned32(virtualBufferPtr)) != 0))
        {
            ++freeBuffers;

            val = (int)buffer;
            val -= bufferStartAddress;
            if ((val % bufSize) != 0)
            {
                printFn("\tERROR: buffer pointer does not point to start:  %X\n",
                    (unsigned int)buffer);
                result = -1;
            }

            if ((val < 0) || ((val / bufSize) >= numBuffers))
            {
                printFn("\tERROR: buffer pointer out of range:  %X\n",
                    (unsigned int)buffer);
                result = -1;
            }

            /* Convert buffer pointer to virtual address */
            buffer = (ipcSMBufHead_t *)ipcSMPhysicalToVirtual(buffer);

            if ((val = ipcSMGetMarker(buffer)) != BUFHEAD_MARKER)
            {
                printFn("\tERROR: Invalid marker %X for free buffer %X\n",
                    val, (unsigned int)ipcOsalReadAligned32(virtualBufferPtr));
                result = -1;
            }

            if ((val = ipcSMGetRefCount(buffer)) != 0)
            {
                printFn("\tERROR: Nonzero reference count %d for free buffer %X\n",
                    val, (unsigned int)ipcOsalReadAligned32(virtualBufferPtr));
                result = -1;
            }

            if (++bufferPtr == (ipcSMBufHead_t **)bufferStartAddress)
            {
                bufferPtr = (ipcSMBufHead_t **)pointerStartAddress;
            }

            virtualBufferPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(bufferPtr);

            if (bufferPtr == (ipcSMBufHead_t **)freeBufRPtr)
            {
                /* looped all the way around so break */
                break;
            }
        }

        if (bufferPtr != (ipcSMBufHead_t **)freeBufWPtr)
        {
            printFn("\tERROR: First Null buffer = %X, freeBufWPtr = %X, freeBufRPtr = %X\n",
                (unsigned int)bufferPtr, freeBufWPtr, freeBufRPtr);
            result = -1;
        }

        printFn("\tNumber of free buffers = %d\n", freeBuffers);

        while ((NULL != virtualBufferPtr) && (*virtualBufferPtr == 0))
        {
            ++usedBuffers;
            if (++bufferPtr == (ipcSMBufHead_t **)bufferStartAddress)
            {
                bufferPtr = (ipcSMBufHead_t **)pointerStartAddress;
            }

            virtualBufferPtr = (ipcSMBufHead_t **)ipcSMPhysicalToVirtual(bufferPtr);

            if (bufferPtr == (ipcSMBufHead_t **)freeBufRPtr)
            {
                /* looped all the way around so break */
                break;
            }
        }

        if (bufferPtr != (ipcSMBufHead_t **)freeBufRPtr)
        {
            printFn("\tERROR: First free buffer = %X, freeBufWPtr = %X, freeBufRPtr = %X\n",
                (unsigned int)bufferPtr, freeBufWPtr, freeBufRPtr);
            result = -1;
        }

        if ((freeBuffers + usedBuffers) != numBuffers)
        {
            printFn("\tERROR: freeBuffers = %d, usedBuffers = %d, total buffers should be %d\n",
                freeBuffers, usedBuffers, numBuffers);
            result = -1;
        }

        buffer = (ipcSMBufHead_t *)ipcSMPhysicalToVirtual(bufferStartAddress);
        if (NULL == buffer)
        {
            ipcError(IPCLOG_MEMTRACK, ("ipcSMReport: null pointer encountered!\n"));
            return -1;
        }

        for (i = 0; i < numBuffers; i++)
        {

            if ((val = ipcSMGetMarker(buffer)) != BUFHEAD_MARKER)
            {
                printFn("\tERROR: Invalid marker %X for buffer %X\n",
                        val, (unsigned int)ipcSMVirtualToPhysical(buffer));
                result = -1;
            }

            if (ipcSMGetRefCount(buffer) == 0)
            {
                --freeBuffers;
            }
            else
            {
                --usedBuffers;
#ifndef IPC_RELEASE
                sm_tick = ipcSMGetTick(buffer);
                tick = cur_tick - sm_tick;
                isLocalPool = (int)(ipcSMGetPoolOwner(buffer) == ipcOsalReadAligned32(&localSMPoolTblProp->ownerID));
                pid =  ipcSMGetPid(buffer);

                printFn("\t usrptr=%p \tref=%d  usrsz=%d \t%s ",
                 paddr+(unsigned long)buffer+sizeof(ipcSMBufHead_t)+IPC_PACKET_DATA_OFFSET-(unsigned long)physical_shm_base,
		 ipcSMGetRefCount(buffer),
                 ipcSMGetUsrSize(buffer)-IPC_PACKET_DATA_OFFSET,
                 ipcSMCheckBuffer(ipcSMHeaderToData(buffer), "ipcSMReport") ? "ok": "corrupted");

                if (isLocalPool)
                {
                    printFn("pid=%d(%s)  age = %ld ticks\n",
                        pid, ipcOsalGetProcessName(pid), tick);
                }
                else
                {
                    printFn("pid=%d(%s)  bp tick stamp = %ld, current ap tick = %ld\n",
                        pid, "bp task", sm_tick, cur_tick);
                }
#endif
            }

            buffer = (ipcSMBufHead_t *)((char *)buffer + bufSize);
        }


        if (freeBuffers || usedBuffers)
        {
            printFn("\tERROR: freeBuffers not found = %d, usedBuffers not found = %d\n",
                freeBuffers, usedBuffers);
            result = -1;
        }
    }

    printFn("-------------------------------------------------------------\n\n");

    return(result);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcGetMaxPSMBufferSize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to get the max buffer size of all physical shared memory.
*   
*   PARAMETER(S):
*       
*       
*   RETURN: the max physical shared memory buffer size.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2005  Motorola Inc.         Bug fix. ipczalloc() will never return if it tries to allocate a block of physical shared memory with size more than max buffer size.
*
*****************************************************************************************/
unsigned long ipcGetMaxPSMBufferSize(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (0 == maxPSMBufSize)
    {
        maxPSMBufSize = ipcOsalReadAligned32(&localSMPoolTblProp->maxPSMBufSize);
    }

    return maxPSMBufSize;
}
#endif /* HAVE_PHYSICAL_SHM */


#ifndef IPC_RELEASE
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMSetDebugInfo
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to set the tail mark of shared memory.
*   
*   PARAMETER(S):
*       
*   
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection 
*
*****************************************************************************************/
void ipcSMSetDebugInfo(ipcSMBufHead_t *buffer, int usrSize, unsigned short pid)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMBufTail_t *pTail;
    unsigned long tick;
/*--------------------------------------- CODE -----------------------------------------*/
    tick = ipcOsalGetTickCount();
    ipcSMSetPid(buffer, pid);
    ipcSMSetUsrSize(buffer, usrSize);
    ipcSMSetTick(buffer, tick);
    ipcSMSetMark2(buffer, BUFRED_MARKER);

    usrSize = (usrSize + 1) & (~1);
    pTail = (ipcSMBufTail_t *)((char *)buffer + sizeof(ipcSMBufHead_t) + usrSize);
    pTail->mark[0] = pTail->mark[1] = BUFRED_MARKER & 0xff;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMCheckBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to check whether tail mark of shared memory buffer is correct
*   
*   PARAMETER(S):
*       
*       
*   RETURN: TRUE means ok, otherwise FALSE
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 26Jan2007  Motorola Inc.         add more checks on the shared memory buffer headers
* 23Feb2007  Motorola Inc.         Check all memory types now
* 06Apr2007  Motorola Inc.         Print pid when under/overrun detected
* 24Apr2007  Motorola Inc.         Dump memory pool on error
* 05Jul2007  Motorola Inc.         Removed function ipcSMGetTailMark
* 
*****************************************************************************************/
BOOL ipcSMCheckBuffer(char *memBlock, const char *caller)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMBufHead_t *blkHdr;
    ipcSMBufTail_t *pTail;
    unsigned short mk_hdr = 0;
    unsigned short mk_tail = 0;
    unsigned short pid;
    unsigned short refCount = 0;
    int userSize = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!isBufferValid(memBlock))
    {
        ipcError(IPCLOG_MEMTRACK, ("%s: buffer invalid [%p]\n", caller, memBlock));
        return FALSE;
    }

    blkHdr = ipcSMDataToHeader(memBlock);
    mk_hdr = ipcSMGetMark2(blkHdr);
    pid = ipcSMGetPid(blkHdr);
    userSize = ipcSMGetUsrSize(blkHdr);
    refCount = ipcSMGetRefCount(blkHdr);
    pTail = (ipcSMBufTail_t *)((char *)blkHdr + sizeof(ipcSMBufHead_t) + ((userSize + 1) & (~1)));
    mk_tail = ((unsigned short) (pTail->mark[0]) << 8) + pTail->mark[1];
    if ((mk_hdr != BUFRED_MARKER) || (mk_tail != BUFRED_MARKER) || (refCount == 0))
    {
        ipcError(IPCLOG_MEMTRACK, 
                ("%s :buffer invalid! ptr %p, headmark %x or tailmark %x != %x, pid = %d(%s), refCount=%d\n",
                 caller, blkHdr, mk_hdr, mk_tail, BUFRED_MARKER, pid, ipcOsalGetProcessName(pid), refCount));
        return FALSE;
    }
    return TRUE;
}
#endif


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcIsPhysicalSharedMemory
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Return TRUE if the pointer passed in points to a physical shared memory
*   buffer.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jul2005  Motorola Inc.         Initial Creation
* 23Feb2007  Motorola Inc.         IPC_PHYSSHM replaces previous memtype def
*
*****************************************************************************************/
BOOL ipcIsPhysicalSharedMemory(void *ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (ipcSMGetMemType(ipcSMDataToHeader(ptr)) == IPC_PHYSSHM);
}
