/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2006 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_osal_memory_access_common.c
*   FUNCTION NAME(S): ipcOsalRead16
*                     ipcOsalRead32
*                     ipcOsalWrite16
*                     ipcOsalWrite32
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements all common OSAL memory access functions which do not
*   change based on platform endianness.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation 
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_osal.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalRead16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 16-bit value from memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 16-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned short ipcOsalRead16(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned short val;
/*--------------------------------------- CODE -----------------------------------------*/
    if (((unsigned long)(addr)) & 0x01)
    {
        /* The address is not aligned to a 16 bit boundary so we will handle
         * the read as a series of 1-byte reads, shifted and ORed together */
        val = (unsigned short)ipcOsalRead8((unsigned char *)(addr));
        val |= ( ((unsigned short)ipcOsalRead8(((unsigned char *)(addr)) + 1)) << 8 );       
    }
    else
    {
        val = ipcOsalReadAligned16(addr);
    }

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalRead32
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Reads a 32-bit value from memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to read from
*                  
*   RETURN: The 32-bit value read from this address
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned long ipcOsalRead32(const void *addr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long val;
/*--------------------------------------- CODE -----------------------------------------*/
    if (((unsigned long)(addr)) & 0x03)
    {
        /* The address is not aligned to a 32 bit boundary so we will handle
         * the read as a series of 1-byte reads, shifted and ORed together */
        val = (unsigned short)ipcOsalRead8((unsigned char *)(addr));
        val |= ( ((unsigned short)ipcOsalRead8(((unsigned char *)(addr)) + 1)) << 8  );       
        val |= ( ((unsigned short)ipcOsalRead8(((unsigned char *)(addr)) + 2)) << 16 );       
        val |= ( ((unsigned short)ipcOsalRead8(((unsigned char *)(addr)) + 3)) << 24 );       
    }
    else
    {
        val = ipcOsalReadAligned32(addr);
    }

    return val;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalWrite16
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 16-bit value to memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 16-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcOsalWrite16(void *addr, unsigned short val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (((unsigned long)(addr)) & 0x01)
    {
        /* The address is not aligned to a 16 bit boundary so we will handle
         * the write as a series of 1-byte writes */
        ipcOsalWrite8( ((unsigned char *)(addr))    , (unsigned char)(val     ));
        ipcOsalWrite8( ((unsigned char *)(addr)) + 1, (unsigned char)(val >> 8));
    }
    else
    {
       ipcOsalWriteAligned16(addr, val);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalWrite32
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Writes a 32-bit value to memory in little endian format.
*   
*   PARAMETER(S):
*       addr: The memory address we want to write to
*       val: The 32-bit value to write to memory
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Sep2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcOsalWrite32(void *addr, unsigned long val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (((unsigned long)(addr)) & 0x03)
    {
        /* The address is not aligned to a 32 bit boundary so we will handle
         * the write as a series of 1-byte writes */
        ipcOsalWrite8( ((unsigned char *)(addr))    , (unsigned char)(val      ));
        ipcOsalWrite8( ((unsigned char *)(addr)) + 1, (unsigned char)(val >> 8 ));
        ipcOsalWrite8( ((unsigned char *)(addr)) + 2, (unsigned char)(val >> 16));
        ipcOsalWrite8( ((unsigned char *)(addr)) + 3, (unsigned char)(val >> 24));
    }
    else
    {
        ipcOsalWriteAligned32(addr, val);
    }
}
