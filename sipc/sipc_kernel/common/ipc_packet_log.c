/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_packet_log.c
*   FUNCTION NAME(S): ipcHandleLogConfigRequest
*                     ipcLogPacketImp
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains functions used for logging IPC Packets for system
*   diagnostics.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 26Jul2005  Motorola Inc.         Initial Creation
* 19Jul2006  Motorola Inc.         Added original size of packets
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 29Jun2007  Motorola Inc.         Added task name in log packet
*
*****************************************************************************************/

#ifndef IPC_NO_PACKET_LOG
/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_packet_log_internal.h"
#include "ipc_common.h"
#include "ipc_osal.h"
#include "ipc_session.h"
#include "ipc_router_interface.h"
#include "ipc_packet.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

ipcPacketLogConfig_t ipcPacketLog_glb =
{
    0,                          /* numEntries */
    IPC_INVALID_ADDR,           /* loggerAddr */
    IPC_SERVICE_ANY,            /* loggerServiceId */
    IPC_PROCSHM                 /* memtype */
}; 

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcLogPacketImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called once for every IPC Packet that goes through this
*   node if packet logging is enabled.  It will check the filter table to see
*   if this packet should be logged and how much of the payload data should be
*   included in the log message if any.
*   
*   PARAMETER(S):
*       packet: IPC Packet to log (if matching filter is found)
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Jul2005  Motorola Inc.         Initial Creation
* 19Jul2006  Motorola Inc.         Added original size of packets 
* 29Jun2007  Motorola Inc.         Added task name in log packet
*
*****************************************************************************************/
void ipcLogPacketImp(ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcAddress_t srcAddr = IPC_INVALID_ADDR;
    ipcAddress_t destAddr = IPC_INVALID_ADDR;
    ipcServiceId_t srcService;
    ipcServiceId_t destService;
    
    int i;
    int dataLengthToLog;
    int packetLen;
    int rc;
    int action;
    long timestamp_sec = 0L;
    long timestamp_usec = 0L;
    BOOL logWithTimestamp;
    ipcLogPacket_t *logPacket;
/*--------------------------------------- CODE -----------------------------------------*/

    ipcDebugLog(IPCLOG_PACKET_LOGGER, ("ipcLogPacketImp called!\n"));

    srcAddr = ipcGetSrcAddr(packet);
    destAddr = ipcGetDestAddr(packet);
    srcService = ipcGetSrcServiceId(packet);
    destService = ipcGetDestServiceId(packet);

    for (i = 0; i < ipcPacketLog_glb.numEntries; i++)
    {
        if ( ((ipcPacketLog_glb.filters[i].srcService == IPC_SERVICE_ANY) ||
              (ipcPacketLog_glb.filters[i].srcService == srcService)) &&

             ((ipcPacketLog_glb.filters[i].destService == IPC_SERVICE_ANY) ||
              (ipcPacketLog_glb.filters[i].destService == destService)) &&

             ((ipcPacketLog_glb.filters[i].srcAddr == IPC_INVALID_ADDR) ||
              (ipcPacketLog_glb.filters[i].srcAddr == srcAddr)) &&

             ((ipcPacketLog_glb.filters[i].destAddr == IPC_INVALID_ADDR) ||
              (ipcPacketLog_glb.filters[i].destAddr == destAddr)) )
        {
            /* The filter was a match for this packet */
            action = ipcPacketLog_glb.filters[i].action & IPC_LOG_ACTION_MASK;
            logWithTimestamp = ipcPacketLog_glb.filters[i].action & IPC_LOG_TIMESTAMP;

            switch (action)
            {
            case IPC_LOG_IGNORE:
                ipcDebugLog(IPCLOG_PACKET_LOGGER, ("ignore packet\n"));
                /* Do nothing for this packet */
                break;

            case IPC_LOG_WITHOUT_DATA:
            case IPC_LOG_WITH_ALL_DATA:
            default:
                ipcDebugLog(IPCLOG_PACKET_LOGGER, 
                    ("log packet, action = %X\n", action));
                dataLengthToLog = ipcGetMsgLength(packet);
                dataLengthToLog = ipcMin(dataLengthToLog, action);
                packetLen = IPC_MIN_LOG_PACKET_LEN + dataLengthToLog;

                logPacket = (ipcLogPacket_t *)ipcAlloc(IPC_PACKET_DATA_OFFSET + packetLen,
                                ipcPacketLog_glb.memtype);

                if (logPacket != NULL)
                {
                    ipcSetMsgLength(logPacket, packetLen);
                    ipcSetSrcAddr(logPacket, ipcStack_glb.ipcAddr);
                    ipcSetSrcServiceId(logPacket, IPC_SRV_LOG_CONFIG);
                    ipcSetDestAddr(logPacket, ipcPacketLog_glb.loggerAddr);
                    ipcSetDestServiceId(logPacket, ipcPacketLog_glb.loggerServiceId);
                    ipcSetMsgFlags(logPacket, 0);

                    ipcOsalWrite32(&logPacket->data.srcService, srcService);
                    ipcOsalWrite32(&logPacket->data.destService, destService);

                    /* Find the Node IDs from the IPC Addresses */
                    ipcOsalWrite32(&logPacket->data.srcNode, ipcAddrToNodeId(srcAddr));
                    ipcOsalWrite32(&logPacket->data.destNode, ipcAddrToNodeId(destAddr));

                    ipcOsalWrite32(&logPacket->data.originalDataSize, ipcGetMsgLength(packet));

                    if (logWithTimestamp)
                    {
                        ipcOsalGetTimestamp(&timestamp_sec,
                                            &timestamp_usec);
                        ipcOsalWrite32(&logPacket->data.timestamp_sec, timestamp_sec);
                        ipcOsalWrite32(&logPacket->data.timestamp_usec, timestamp_usec);
                    }
                    else
                    {
                        ipcOsalWrite32(&logPacket->data.timestamp_sec, 0);
                        ipcOsalWrite32(&logPacket->data.timestamp_usec, 0);
                    }

                    strncpy(logPacket->data.processName, ipcOsalGetProcessName(ipcOsalGetProcessId()), sizeof(logPacket->data.processName)-2);
                    logPacket->data.processName[sizeof(logPacket->data.processName)-2] = 0;
                    logPacket->data.processName[sizeof(logPacket->data.processName)-1] = 0;

                    memcpy(logPacket->data.data, ipcGetMsgData(packet), dataLengthToLog);

                    /* Send to another node */
                    rc = SSRTDataRequest((ipcPacket_t *)logPacket, IPC_NONDEDICATED_CHANNEL_ID);

                    if (rc < 0)
                    {
                        ipcError(IPCLOG_PACKET_LOGGER,
                            ("Error sending log packet to srv id: %d: %d\n", ipcPacketLog_glb.loggerServiceId, rc));
                        ipcFree(logPacket);
                    }
                    else
                    {
                        ipcDebugLog(IPCLOG_PACKET_LOGGER, 
                            ("sending log packet to srv id: %d: %d\n", ipcPacketLog_glb.loggerServiceId, rc));
                    }
                }

                break;
            }

            break;
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleLogConfigRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   PARAMETER(S):
*       packet: ipc log config packet
*       
*   RETURN: IPC_OK for success, IPCEFAULT for invalid parameter
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 26Jul2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Free packet before return
* 23Feb2007  Motorola Inc.         Updated memory type defs
*
*****************************************************************************************/
int ipcHandleLogConfigRequest(ipcLogConfigPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcPacketLog_glb.numEntries = ipcOsalRead32(&packet->data.numFilters);
    ipcPacketLog_glb.loggerAddr = ipcGetSrcAddr(packet);
    ipcPacketLog_glb.loggerServiceId = ipcGetSrcServiceId(packet);
    ipcPacketLog_glb.memtype = ipcRouterGetMemType(ipcPacketLog_glb.loggerAddr);
    
    ipcDebugLog(IPCLOG_PACKET_LOGGER,
        ("config packet log: numEntries = %d, loggerAddr = %d, loggerSrv = %d\n",
         ipcPacketLog_glb.numEntries,
         ipcPacketLog_glb.loggerAddr,
         ipcPacketLog_glb.loggerServiceId));

    for (i = 0; i < ipcPacketLog_glb.numEntries; i++)
    {
        ipcPacketLog_glb.filters[i].action = ipcOsalRead32(&packet->data.filters[i].action);

        ipcPacketLog_glb.filters[i].srcService =
            (ipcServiceId_t)ipcOsalRead32(&packet->data.filters[i].srcService);

        ipcPacketLog_glb.filters[i].destService =
            (ipcServiceId_t)ipcOsalRead32(&packet->data.filters[i].destService);

        ipcPacketLog_glb.filters[i].srcAddr =
            ipcNodeIdToIpcAddr(ipcOsalRead32(&packet->data.filters[i].srcNode));

        ipcPacketLog_glb.filters[i].destAddr =
            ipcNodeIdToIpcAddr(ipcOsalRead32(&packet->data.filters[i].destNode));
    }
    
    ipcFree(packet);
    return rc;
}

#endif /* !IPC_NO_PACKET_LOG */
