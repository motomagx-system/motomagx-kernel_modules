/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2006 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_alignment_check.c
*   FUNCTION NAME(S): 
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 29Apr2006  Motorola Inc.         Added template 
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_DEBUG
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_device_layer.h"
#include "ipc_common.h"
#include "ipc_platform_defs.h"
#include "ipc_messages.h"

#define verify(name, const_expression, value) \
    char name[((const_expression) == value) * 2 - 1]


/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/* The following union is here to verify correct offsets and sizes.
 * If they are incorrect, the compiler will give an error for a negative-sized array */
union
{
    verify(a, IPC_HEADER_SIZE, 16);
    verify(b, IPC_FRAME_HEADER_ENTRY_SIZE, 3);
    verify(c, IPC_MIN_FRAME_HEADER_SIZE, 3);
    verify(d, IPC_PACKET_HEADER_OFFSET, 12);
    verify(e, IPC_PACKET_DATA_OFFSET, 28);
    verify(f, SSSS_AUTH_REQUEST_DATA_SIZE, 8);
    verify(g, SSSS_AUTH_REPLY_DATA_SIZE, 4);
    
} ipc_alignment_check; 


/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
