/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_queue.c
*   FUNCTION NAME(S): ipcQueueClose
*                     ipcQueueGetLink
*                     ipcQueueGetLinkBlock
*                     ipcQueueGetLinkNonBlock
*                     ipcQueueGetObject
*                     ipcQueueOpen
*                     ipcQueuePutLink
*                     ipcQueuePutObject
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   The IPC queue is a simple queue implementation based on the IPC list.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 17Feb2006  Motorola Inc.         Change ipcQueueGetMaxSize to macro
* 29Jan2007  Motorola Inc.         Header files change for GPL 
* 26Jun2007  Motorola Inc.         Fix semaphore problem
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_queue.h"
#include "ipclog.h"
#include "ipc_defs.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#define ipcQueueGet(list, peek)     ( (peek) ? (list).head : ipcListRemoveHead(&(list)) )
#define set_err_code(err, err_code) { if (err != NULL) { *err = err_code; } }
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueueOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initializes an IPC queue
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*                  
*   RETURN: IPCE_OK or an OSAL error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 28Oct2005  Motorola Inc.         Add parameter check 
*
*****************************************************************************************/
int ipcQueueOpen(ipcQueue_t *queue)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPCE_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if(queue == NULL)
    {
        ipcError(IPCLOG_GENERAL, ("ipcQueueOpen a invalid queue\n"));
        return IPC_ERROR;
    }
    
    rc = ipcOsalMutexOpen(&queue->mutex);
    if (rc == IPCE_OK)
    {
        rc = ipcOsalSemaOpen(&queue->notEmptySema, 0);
        if (rc == IPCE_OK)
        {
            ipcListInit(&queue->lowPriorityList);
            ipcListInit(&queue->highPriorityList);
        }
        else
        {
            ipcError(IPCLOG_GENERAL, ("Sema Open failed in ipcQueueOpen: %d\n", rc));
            ipcOsalMutexClose(queue->mutex);
        }
    }
    else
    {
        ipcError(IPCLOG_GENERAL, ("Mutex Open failed in ipcQueueOpen: %d\n", rc));
    }

    queue->currentSize = 0;
    queue->maxQueueSize = 0;

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueueClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Destroys an IPC queue
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcQueueClose(ipcQueue_t *queue)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcOsalMutexLock(queue->mutex);
    
    ipcListFreeAll(&queue->lowPriorityList);
    ipcListFreeAll(&queue->highPriorityList);

    ipcOsalMutexClose(queue->mutex);
    ipcOsalSemaClose(queue->notEmptySema);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueuePutLink
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Inserts an IPC list link into the queue
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*       link: list link to insert
*       flags: IPC_QUEUE_HIGH_PRIORITY: the link should be inserted with high
*                          priority (the order among high priority links will be maintained
*                          so the link will be inserted after any other high priority links
*                          but before all of the low priority links
*                          IPC_QUEUE_LIFO: link is inserted at the head of the queue
*                  
*   RETURN: IPC_OK - the link was added to the QUEUE
*           IPCESOCKFULL - the queue was full (max bytes queued)
*           IPCEINTR - socket was closed (problem with mutex)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 03Nov2005  Motorola Inc.         Added check for invalid param 
* 26Jun2007  Motorola Inc.         Fix semaphore problem
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcQueuePutLink(ipcQueue_t *queue, ipcListLink_t link, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    INT32 newSize;
    BOOL wasEmpty;
    BOOL highPriorityWasEmpty;
    BOOL putHighPriority = (flags & (IPC_QUEUE_HIGH_PRIORITY | IPC_QUEUE_LIFO));
/*--------------------------------------- CODE -----------------------------------------*/
    if ((queue == NULL) || (link == NULL))
    {
        return IPCEFAULT;
    }

    if (ipcOsalMutexLock(queue->mutex) != IPCE_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalMutexLock failed in ipcQueuePutLink\n"));
        return IPCEBUSY;
    }

    wasEmpty = (queue->currentSize == 0);
    highPriorityWasEmpty = (queue->highPriorityList.size == 0);

    newSize = queue->currentSize + 1;
    if ((queue->maxQueueSize > 0) && (newSize > queue->maxQueueSize))
    {
        /* No room in Queue for this link */
        if (ipcOsalMutexRelease(queue->mutex) != IPCE_OK)
        {
            ipcError(IPCLOG_GENERAL, ("ipcOsalMutexRelease failed in ipcQueuePutLink\n"));
        }
        return IPCESOCKFULL;
    }

    queue->currentSize = newSize;
    
    if (flags & IPC_QUEUE_LIFO)
    {
        ipcListAddHead(&queue->highPriorityList, link);
    }
    else if (putHighPriority)
    {
        ipcListAddTail(&queue->highPriorityList, link);
    }
    else
    {
        ipcListAddTail(&queue->lowPriorityList, link);
    }

    if (wasEmpty || (putHighPriority && highPriorityWasEmpty))
    {
        /* The queue was previously empty (or we just put the first high
         * priority message in) so we must signal the sema.
         * First we will test the semaphore to make sure the count is 
         * zero so we only increment it to one. */
        if (ipcOsalSemaWait(queue->notEmptySema, 0) == IPCE_ILLEGAL_USE)
        {
            ipcError(IPCLOG_GENERAL, ("ipcOsalSemaWait failed in ipcQueuePutLink\n"));
        }
        
        if (ipcOsalSemaSignal(queue->notEmptySema) != IPCE_OK)
        {
            ipcError(IPCLOG_GENERAL, ("ipcOsalSemaSignal failed in ipcQueuePutLink\n"));
        }
    }

    if (ipcOsalMutexRelease(queue->mutex) != IPCE_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalMutexRelease failed in ipcQueuePutLink\n"));
    }
    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueueGetLinkBlock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get a normal priority message from queue in block mode
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*       error: pass out the error code
*       peek: whether peek message
*       highPriority: true if we want to wait for high priority messages only
*                  
*   RETURN: pointer to the link
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 22Dec2005  Motorola Inc.         fix and simplify for normal mode
* 31May2006  Motorola Inc.         Changed debug messages for buffer check
* 09Jan2007  Motorola Inc.         Clear notEmpty sema before signal it 
* 26Jun2007  Motorola Inc.         Fix semaphore problem
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
static ipcListLink_t ipcQueueGetLinkBlock(ipcQueue_t *queue, int *error, BOOL peek, BOOL highPriority)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    /* lock */
    if (ipcOsalMutexLock(queue->mutex) != IPCE_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalMutexLock failed in ipcQueueGetLink\n"));
        set_err_code(error, IPCEBUSY);
        return NULL;
    }
    
    while ( highPriority ? (queue->highPriorityList.size == 0) : (queue->currentSize == 0) )
    {
        /* unlock */
        if (IPCE_OK != ipcOsalMutexRelease(queue->mutex))
        {
            ipcError(IPCLOG_GENERAL, ("ipcOsalMutexRelease failed in ipcQueueGetLink\n"));
            set_err_code(error, IPCEBUSY);
            return NULL;
        }
        /* wait */        
        if (ipcOsalSemaWait(queue->notEmptySema, IPC_OSAL_INFINITE) != IPCE_OK)
        {            
            ipcWarning(IPCLOG_GENERAL, ("ipcOsalSemaWait failed in ipcQueueGetLink\n"));
            set_err_code(error,IPCEINTR);
            return NULL;
        }
        /* lock again */        
        if (ipcOsalMutexLock(queue->mutex) != IPCE_OK)
        {            
            ipcError(IPCLOG_GENERAL, ("ipcOsalMutexLock failed in ipcQueueGetLink\n"));
            set_err_code(error, IPCEBUSY);
            return NULL;
        }
    }
    
    /* get the next link from the queue */
    link = (queue->highPriorityList.size > 0) ?
        ipcQueueGet(queue->highPriorityList, peek) : ipcQueueGet(queue->lowPriorityList, peek);
    if(link == NULL)
    {
        set_err_code(error, IPCEFAULT);
        ipcError(IPCLOG_MEMTRACK, ("ipcQueueGet failed, cur size = %d, high size = %d, low size = %d, peek = %d\n", 
            queue->currentSize,
            queue->highPriorityList.size, 
            queue->lowPriorityList.size, peek));
    }
    else if(!peek) /* link is not NULL so decrement size */
    {
        queue->currentSize -= 1;
    }
    
    /* first clear nonEmpty sema for all cases */
    if (ipcOsalSemaWait(queue->notEmptySema, 0) == IPCE_ILLEGAL_USE)
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalSemaWait failed in ipcQueueGetLink\n"));                  
    }
    
    if (queue->currentSize > 0)
    {
        /* then signal notEmpty sema if there are more items in the queue */
        if (ipcOsalSemaSignal(queue->notEmptySema) != IPCE_OK)
        {
            ipcError(IPCLOG_GENERAL, ("ipcOsalSemaSignal failed in ipcQueueGetLink\n"));
        }
    }
    
    /* unlock */
    if (IPCE_OK != ipcOsalMutexRelease(queue->mutex))
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalMutexRelease failed in ipcQueueGetLink\n"));
    }
    return link;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueueGetLinkNonBlock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get a message using non-block mode
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*       error: pass out the error code
*       peek: whether peek message
*       highPriority: high priority flag
*                  
*   RETURN: pointer to the link
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 22Dec2005  Motorola Inc.         Simplify for nonBlock mode
* 10Feb2006  Motorola Inc.         Fix bug. When invoke ipczrecvfrom() it returns an unexpected error.   
* 31May2006  Motorola Inc.         Changed debug messages for buffer check 
* 26Jun2007  Motorola Inc.         Fix semaphore problem
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
static ipcListLink_t ipcQueueGetLinkNonBlock(ipcQueue_t *queue, int *error,BOOL peek, BOOL highPriority)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    /* lock */
    if (ipcOsalMutexLock(queue->mutex) != IPCE_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalMutexLock failed in ipcQueueGetLink\n"));
        set_err_code(error, IPCEBUSY);
        return NULL;
    }
    /* get one message */
    if ( highPriority ? (queue->highPriorityList.size > 0) : (queue->currentSize > 0) )
    {
        link = (queue->highPriorityList.size > 0) ?
            ipcQueueGet(queue->highPriorityList, peek) : ipcQueueGet(queue->lowPriorityList, peek);
        if (link == NULL)
        {
            if (IPCE_OK != ipcOsalMutexRelease(queue->mutex))
            {
                ipcError(IPCLOG_GENERAL, ("ipcOsalMutexRelease failed in ipcQueueGetLink\n"));
            }
            ipcError(IPCLOG_MEMTRACK, ("ipcQueueGet failed, cur size = %d, high size = %d, low size = %d, peek = %d\n", 
                        queue->currentSize,
                        queue->highPriorityList.size, 
                        queue->lowPriorityList.size, peek));
            return NULL;
        }
    }
    if( link != NULL )
    {
        if(!peek)
        {
            queue->currentSize -= 1;
            if (queue->currentSize == 0)
            {
                /* clear the not empty sema */
                if (ipcOsalSemaWait(queue->notEmptySema, 0) == IPCE_ILLEGAL_USE)
                {            
                    ipcError(IPCLOG_GENERAL, ("ipcOsalSemaWait failed in ipcQueueGetLink\n"));
                }
            }
        }        
    }
    else
    {
        set_err_code(error, IPCEWOULDBLOCK);
    }
    /* unlock and return */
    if (IPCE_OK != ipcOsalMutexRelease(queue->mutex))
    {
        ipcError(IPCLOG_GENERAL, ("ipcOsalMutexRelease failed in ipcQueueGetLink\n"));
    }
    return link;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueueGetLink
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes an IPC list link from the queue.  If mayBlock is FALSE, it will return
*   NULL if the queue is empty.  Otherwise, it will block until a link is put in.
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*       flags: IPC_QUEUE_NON_BLOCKING: return NULL instead of blocking when
*                          there is no data in the queue
*                          IPC_QUEUE_HIGH_PRIORITY: only return high priority messages
*       error: optional pointer to error code.  If an error occurs and this
*                          pointer is not NULL, the error code is returned here.
*                  
*   RETURN: pointer to the link removed
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation
* 28Oct2005  Motorola Inc.         Add parameter check
* 22Dec2005  Motorola Inc.         simplify the logic 
*
*****************************************************************************************/
ipcListLink_t ipcQueueGetLink(ipcQueue_t *queue, int flags, int *error)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link = NULL;
    BOOL peek = flags & IPC_QUEUE_PEEK;
    BOOL mayBlock = !(flags & IPC_QUEUE_NON_BLOCKING);
    BOOL hp = (flags & IPC_QUEUE_HIGH_PRIORITY) != 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if(mayBlock)
    {
        link = ipcQueueGetLinkBlock(queue, error, peek, hp);
    }
    else
    {
        link = ipcQueueGetLinkNonBlock(queue, error, peek, hp);
    }
    return link;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueuePutObject
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Inserts an object into the queue
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*       object: pointer to object to insert
*       flags: IPC_QUEUE_HIGH_PRIORITY: the link should be inserted with high
*                          priority (the order among high priority links will be maintained
*                          so the link will be inserted after any other high priority links
*                          but before all of the low priority links
*                  
*   RETURN: IPC_OK - the object was added to the QUEUE successfully
*           IPCENOBUFS - out of memory
*           IPCESOCKFULL - the queue was full (max bytes queued)
*           IPCEINTR - socket was closed (problem with mutex)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcQueuePutObject(ipcQueue_t *queue, void *object, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    link = (ipcListLink_t)ipcAlloc(sizeof(ipcListItem_t), 0);

    if (link == NULL)
    {
        return IPCENOBUFS;
    }

    link->data = object;
    rc = ipcQueuePutLink(queue, link, flags);
    if (rc != IPC_OK)
    {
        ipcFree(link);
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcQueueGetObject
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes an object from the queue.  If mayBlock is FALSE, it will return NULL
*   if the queue is empty.  Otherwise, it will block until an object is put in.
*   
*   PARAMETER(S):
*       queue: pointer to IPC queue structure
*       flags: IPC_QUEUE_NON_BLOCKING: return NULL instead of blocking when
*                          there is no data in the queue
*                          IPC_QUEUE_HIGH_PRIORITY: only return high priority messages
*       error: optional pointer to error code.  If an error occurs and this
*                          pointer is not NULL, the error code is returned here.
*       
*   RETURN: pointer to the object removed
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void *ipcQueueGetObject(ipcQueue_t *queue, int flags, int *error)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link;
    void *object = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    link = ipcQueueGetLink(queue, flags, error);

    if (link != NULL)
    {
        object = link->data;

        if (!(flags & IPC_QUEUE_PEEK))
        {
            ipcFree(link);
        }
    }

    return object;
}

