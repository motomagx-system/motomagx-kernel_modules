/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2005, 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_list.c
*   FUNCTION NAME(S): ipcListAddHead
*                     ipcListAddObjectToHead
*                     ipcListAddObjectToTail
*                     ipcListAddTail
*                     ipcListAppendList
*                     ipcListCompareLink
*                     ipcListCompareObject
*                     ipcListFind
*                     ipcListFreeAll
*                     ipcListInsertLink
*                     ipcListInsertObject
*                     ipcListRemove
*                     ipcListRemoveHead
*                     ipcListRemoveNextLink
*                     ipcListRemoveNextObject
*                     ipcListRemoveObject
*                     ipcListRemoveObjectFromHead
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   The IPC list is a singly-linked list implementation for general use in the
*   IPC protocol stack.  In can be used in two ways:
*
*   1) The objects inserted into the list can be structures containing the pointer
*      to the next link.  Therefore, inserting a new link does not require an
*      allocation of a link object and removing a link does not requre the
*      freeing of a link object.  These objects can only be included in one list
*      at a time.  The functions ipcListInsertLink, ipcListAddHead, ipcListAddTail
*      ipcListRemoveNextLink, and ipcListRemoveHead are used to add and remove
*      links from such a list.  The macros ipcListFindLink and ipcListRemoveLink
*      are used to search for and remove links.
* 
*   2) The objects inserted into the list can be structures which do not contain the
*      pointer to the next link.  Inserting a new link requires the allocation of 
*      a link object that contains a pointer to the object and a pointer to the
*      next link.  The functions ipcListInsertObject, ipcListAddObjectToHead,
*      ipcListAddObjectToTail, ipcListRemoveNextObject, and ipcListRemoveObjectFromHead
*      are used to add and remove links from such a list.  The macro ipcListFindObject
*      and the function ipcListRemoveObject are used to search for and remove objects.
*
*   The function ipcListInit should be called first to initialize the list structure.
*
*   A more general search of an IPC list can be done by calling ipcListFind and passing
*   in a comparison function that takes a list link and a pointer to the key we are
*   searching for and returns TRUE if they match.
*
*   Removing a link from the list requires access to the previous link.  When iterating
*   through a list where links may need to be removed, the macros ipcListFirstPair
*   and ipcListNextPair should be used to update both the current and previous links.
*   Otherwise, the macros ipcListFirst and ipcListNext can be used to update a single link.
*
*   When only removing a single link, the function ipcListRemove handles all the work
*   described above.  It searches from the head of the list to find the link to remove.
*
*   The function ipcListFreeAll will free the memory used by all ipc list link
*   structures, but not by any data they point to.
*
*   All links in one list can be appended to another list by calling ipcListAppendList.
*
*   SAVING STORAGE SPACE:
*   In order to reduce memory usage when using many lists (in a hashtable implementation,
*   for example, where every bucket contains a list) the list's head link can be stored
*   without the tail and size fields.  When using any of the list functions below, the
*   head link can be copied into a temporary list structure and the tail and size fields
*   do not have to be valid.  The only functions that cannot be called with an invalid tail
*   field are ipcListAdd_XXX_ToTail and ipcListAppendList.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 02Jun2004  Motorola Inc.         Initial Creation                       
* 14Sep2004  Motorola Inc.         Added ipcListGet/SetPosition
* 23Dec2004  Motorola Inc.         Removed list iterator
* 22Jun2007  Motorola Inc.         Set next pointer invalid when link is removed
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_LIST_DEBUG
*
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_list.h"
#include "ipc_osal.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
#define LIST_MAGIC_UNUSED (void*)0xFEEEFEEE
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListInsertLink
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Inserts a new link after the previous link passed in.  If the previous link
*   passed in is NULL, the new element will be inserted at the head of the list.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       link: link to insert into list
*       prev: link to insert after
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 23Dec2004  Motorola Inc.         Removed list iterator
* 28Oct2005  Motorola Inc.         Add parameter check
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_LIST_DEBUG
*
*****************************************************************************************/
void ipcListInsertLink(ipcList_t *list, ipcListLink_t link, ipcListLink_t prev)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (link == NULL)
    {
        ipcError(IPCLOG_GENERAL,("DEBUG ipcListInsertLink, link == NULL\n"));
        return;		
    }
    
    if (prev != NULL)
    {
        link->next = prev->next;
        prev->next = link;
    }
    else
    {
        link->next = list->head;
        list->head = link;
    }

    if ((list->tail == prev) || (list->tail == NULL))
    {
        list->tail = link;
    }

    list->size++;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListAddHead
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Adds the specified link as the new head of the list
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       newHead: the IPC list link to add as the head
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
* 28Oct2005  Motorola Inc.         Add parameter check
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_LIST_DEBUG
*
*****************************************************************************************/
void ipcListAddHead(ipcList_t *list, ipcListLink_t newHead)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ( newHead == NULL )
    {
        ipcError(IPCLOG_GENERAL,("DEBUG ipcListAddHead, newHead == NULL\n"));
        return;		
    }
    
    newHead->next = list->head;
    list->head = newHead;
    
    if (list->tail == NULL)
    {
        list->tail = newHead;
    }
    
    list->size++;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListAddTail
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Adds the specified link as the new tail of the list
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       newTail: the IPC list link to add as the tail
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
* 28Oct2005  Motorola Inc.         Add parameter check
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_LIST_DEBUG
*
*****************************************************************************************/
void ipcListAddTail(ipcList_t *list, ipcListLink_t newTail)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (newTail == NULL)
    {
        ipcError(IPCLOG_GENERAL,("DEBUG ipcListAddTail, newTail == NULL\n"));
        return;		
    }
    
    if (list->tail != NULL)
    {
        list->tail->next = newTail;
    }
    else
    {
        list->head = newTail;  
    }
    
    list->tail = newTail;
    newTail->next = NULL;
    
    list->size++;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListRemoveNextLink
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes and returns the next element of an IPC list.  If NULL is passed in 
*   for prev, this function removes the head of the list.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       prev: link before the one we want to remove
*                  
*   RETURN: link that was removed
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 23Dec2004  Motorola Inc.         Removed list iterator
* 22Jun2007  Motorola Inc.         Set next pointer invalid when link is removed
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_LIST_DEBUG
*
*****************************************************************************************/
ipcListLink_t ipcListRemoveNextLink(ipcList_t *list, ipcListLink_t prev)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t curr = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if (prev == NULL)
    {
        return(ipcListRemoveHead(list));
    }
    else if (prev->next != NULL)
    {
        curr = prev->next;

        if (curr == list->tail)
        {
            list->tail = prev;
        }
        
        prev->next = curr->next;
        curr->next = LIST_MAGIC_UNUSED;
        list->size--;
    }

    return(curr);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListRemoveHead
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes and returns the head of an IPC list.  Because this is a singly-linked
*   list, the operation of removing the tail is not efficient and is not included
*   as a seprate function.  If necessary, it must be done by modifying the list
*   structure's tail field directly.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*                  
*   RETURN: pointer to previous head of list (which we just removed)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 23Dec2004  Motorola Inc.         Removed list iterator
* 22Jun2007  Motorola Inc.         Set next pointer invalid when link is removed
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_LIST_DEBUG
*
*****************************************************************************************/
ipcListLink_t ipcListRemoveHead(ipcList_t *list)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t head = list->head;
/*--------------------------------------- CODE -----------------------------------------*/
    if (head != NULL)
    {
        list->head = head->next;
        head->next = LIST_MAGIC_UNUSED;
        list->size--;

        if (list->head == NULL)
        {
            list->tail = NULL;
        }
    }
    
    return(head);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListInsertObject
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocates a new link with a pointer to the specified object and inserts this
*   link into the list after the previous link passed in.  If the previous link
*   passed in is NULL, the new link will be inserted at the head of the list.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       obj: pointer to the object to insert into the list
*       prev: link to insert after
*                  
*   RETURN: pointer to the new link we created
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
ipcListLink_t ipcListInsertObject(ipcList_t *list, void *obj, ipcListLink_t prev)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link;
/*--------------------------------------- CODE -----------------------------------------*/
    link = (ipcListLink_t)ipcAlloc(sizeof(ipcListItem_t), 0);

    if (link != NULL)
    {
        link->data = obj;
        ipcListInsertLink(list, link, prev);
    }

    return(link);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListAddObjectToHead
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocates a new link with a pointer to the specified object and adds this
*   link as the head of the list.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       obj: pointer to the object to insert into the list
*                  
*   RETURN: pointer to the new link we created
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
ipcListLink_t ipcListAddObjectToHead(ipcList_t *list, void *obj)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t head;
/*--------------------------------------- CODE -----------------------------------------*/
    head = (ipcListLink_t)ipcAlloc(sizeof(ipcListItem_t), 0);

    if (head != NULL)
    {
        head->data = obj;
        ipcListAddHead(list, head);
    }

    return(head);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListAddObjectToTail
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocates a new link with a pointer to the specified object and adds this
*   link as the tail of the list.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       obj: pointer to the object to insert into the list
*                  
*   RETURN: pointer to the new link we created
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
ipcListLink_t ipcListAddObjectToTail(ipcList_t *list, void *obj)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t tail;
/*--------------------------------------- CODE -----------------------------------------*/
    tail = (ipcListLink_t)ipcAlloc(sizeof(ipcListItem_t), 0);

    if (tail != NULL)
    {
        tail->data = obj;
        ipcListAddTail(list, tail);
    }

    return(tail);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListRemoveNextObject
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes and frees the next element of an IPC list
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       prev: link before the one we want to remove
*                  
*   RETURN: pointer to the object contained in the link we freed
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void *ipcListRemoveNextObject(ipcList_t *list, ipcListLink_t prev)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link;
    void *obj;
/*--------------------------------------- CODE -----------------------------------------*/
    link = ipcListRemoveNextLink(list, prev);
    obj = link->data;
    ipcFree(link);

    return(obj);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListRemoveObjectFromHead
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes the object stored at the head of on IPC list and frees the link it
*   was stored in
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       prev: link before the one we want to remove
*                  
*   RETURN: pointer to the object contained in link we freed
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void *ipcListRemoveObjectFromHead(ipcList_t *list)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t oldHead;
    void *obj;
/*--------------------------------------- CODE -----------------------------------------*/
    oldHead = ipcListRemoveHead(list);
    obj = oldHead->data;
    ipcFree(oldHead);

    return(obj);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListFind
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Searches through the list and returns the link that matches the key we are
*   searching for (based on the comparison function that was passed in).
*   If the key is found, the variable prev will point to the previous link in
*   the list or it will be NULL of the link found was the head of the list.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       key: pointer to the key we are searching for
*       compare: function used to compare each link to the key
*       prev: pointer used to return the link before the one we are searching for
*                         (NULL can be passed in, if the previous link is not needed)
*       
*   RETURN: the list link with the key we were searching for, or NULL if not found
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Dec2004  Motorola Inc.         Initial Creation
* 13Jan2005  Motorola Inc.         Added comparison function pointer
*
*****************************************************************************************/
ipcListLink_t ipcListFind(ipcList_t *list, void *key, ipcListCompareFunction_t compare,
                          ipcListLink_t *prevPtr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev, curr;
/*--------------------------------------- CODE -----------------------------------------*/
    for (ipcListFirstPair(*list, prev, curr);
         curr != NULL;
         ipcListNextPair(prev, curr))
    {
        if (compare(curr, key))
        {
            /* We found the key, so break out of the loop */
            break;
        }
    }
    
    if (prevPtr != NULL)
    {
        *prevPtr = prev;
    }

    return(curr);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListRemove
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Searches through the list and removes the link that matches the key we are
*   searching for (based on the comparison function that was passed in).
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       key: pointer to the key we are searching for
*       compare: function used to compare each link to the key
*       
*   RETURN: the list link that was removed, or NULL if not found
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jan2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
ipcListLink_t ipcListRemove(ipcList_t *list, void *key, ipcListCompareFunction_t compare)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev, curr;
/*--------------------------------------- CODE -----------------------------------------*/
    curr = ipcListFind(list, key, compare, &prev);
    if (curr != NULL)
    {
        curr = ipcListRemoveNextLink(list, prev);
    }
    return curr;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListRemoveObject
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Searches through the list and removes the object that was passed in if it
*   is found.  It will free the link in the list that contained this object.
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*       object: pointer to the object we are searching for
*       
*   RETURN: TRUE if object was found and removed
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jan2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
BOOL ipcListRemoveObject(ipcList_t *list, void *object)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link;
    BOOL found = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    link = ipcListRemove(list, object, ipcListCompareObject);
    if (link != NULL)
    {
        ipcFree(link);
        found = TRUE;
    }

    return found;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListFreeAll
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Removes all links from an IPC list and frees the memory used by the list
*   link structures (but not by any data they point to).
*   
*   PARAMETER(S):
*       list: pointer to IPC list structure
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 23Dec2004  Motorola Inc.         Removed list iterator
*
*****************************************************************************************/
void ipcListFreeAll(ipcList_t *list)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    while (list->head != NULL)
    {
        /* Remove and free each element of the list */
        ipcFree(ipcListRemoveHead(list));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListAppendList
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Appends all elements in fromList to the end of toList.  After this is complete,
*   fromList will be empty.
*   
*   PARAMETER(S):
*       toList: pointer to IPC list to append to
*       fromList: pointer to IPC list to append from
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcListAppendList(ipcList_t *toList, ipcList_t *fromList)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (fromList->head != NULL)
    {
        if (toList->head != NULL)
        {
            toList->tail->next = fromList->head;
        }
        else
        {
            toList->head = fromList->head;
        }

        toList->tail = fromList->tail;
        toList->size += fromList->size;

        ipcListInit(fromList);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListCompareLink
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Comparison function used to find a link directly
*   
*   PARAMETER(S):
*       link: list link to compare to
*       key: list link we are searching for
*       
*   RETURN: TRUE if key matches the link
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Jan2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
BOOL ipcListCompareLink(ipcListLink_t link, void *key)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (link == key);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcListCompareObject
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Comparison function used to find a link based on the data it points to
*   
*   PARAMETER(S):
*       link: list link to compare to
*       key: pointer to data object we are searching for
*       
*   RETURN: TRUE if link contains pointer to the data object we are searching for
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 13Jan2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
BOOL ipcListCompareObject(ipcListLink_t link, void *key)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (link->data == key);
}
