/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_main.c
*   FUNCTION NAME(S): ipcAddAllocator
*                     ipcAddAllocatorAlias
*                     ipcAllocImp
*                     ipcCopyPacket
*                     ipcCreateControlPacket
*                     ipcFreeImp
*                     ipcIncRefCount
*                     ipcInvalidAlloc
*                     ipcInvalidFree
*                     ipcLockMem
*                     ipcRemoveAllocator
*                     ipcRemoveAllocatorAlias
*                     ipcStackConfig
*                     ipcStackDestroy
*                     ipcStackInit
*                     ipcStackTerminate
*                     ipcUnlockMem
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains the main IPC thread and common utility functions
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 12Jan2005  Motorola Inc.         Initial Creation
* 23Feb2007  Motorola Inc.         Unify memory allocation functions
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
* 22Jun2007  Motorola Inc.         Make size aligned to even value in memcpy
* 13Jul2007  Motorola Inc.         Removed compile condition IPC_DEBUG
* 19Jul2007  Motorola Inc.         Included ipc_maro_defs.h
*
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_common.h"
#include "ipc_osal.h"
#include "ipc_session.h"
#include "ipc_messages.h"
#include "ipc_router_interface.h"
#include "ipc_device_interface.h"
#include "ipc_queue.h"
#include "ipclog.h"
#include "ipc_macro_defs.h"
#ifdef HAVE_PHYSICAL_SHM
#include "ipc_mu_shm.h"
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
void *ipcInvalidAlloc(UINT32 bufSize, UINT32 flags);
int ipcInvalidFree(void *pBuf);

/*------------------------------------- GLOBAL DATA ------------------------------------*/

#ifdef START_UP_TIME
    extern unsigned long ulStartUpTime;
#endif

ipcStack_t ipcStack_glb;

unsigned int ipcLogEnableMask_glb =
    IPCLOG_TYPE_SESSION     |
    IPCLOG_TYPE_ROUTER      |
    IPCLOG_TYPE_DEVICE      |
    IPCLOG_TYPE_GENERAL     |
    IPCLOG_TYPE_USER_APP    |
    IPCLOG_TYPE_MEMTRACK    |
    IPCLOG_LEVEL_ERROR;


ipcAllocatorTableEntry_t ipcAllocatorTable[IPC_MAX_MEM_TYPES] = 
{
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0},
    {ipcInvalidAlloc, ipcInvalidFree, 0}
};


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcStackConfig
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function configures the IPC stack to run as a server or client and sets
*   the node ID and data type
*   
*   PARAMETER(S):
*       BOOL runAsServer
*                  UINT32 nodeId
*                  UINT8 dataType
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcStackConfig(BOOL runAsServer, UINT32 nodeId, UINT8 datatype)
{
#ifdef START_UP_TIME
    printk(KERN_ALERT "ipcStack_glb.state = IPC_STACK_STATE_NULL\n");
    printk(KERN_ALERT "ipcStack_glb.isServer = %d\n",runAsServer);
    printk(KERN_ALERT "time = %u msecs\n",jiffies_to_msecs(jiffies-ulStartUpTime));
#endif
    ipcStack_glb.state = IPC_STACK_STATE_NULL;

    ipcStack_glb.isServer = runAsServer;
    ipcStack_glb.nodeId = nodeId;
    ipcStack_glb.datatype = datatype;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcStackInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function initializes the IPC stack.  It must be called from the main
*   IPC Stack thread.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: TRUE if successful, FALSE for any error
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Dec2004  Motorola Inc.         Initial Creation
* 22Aug2005  Motorola Inc.         Checked fault condition of session
* 29Nov2005  Motorola Inc.         Close connMutex on failure 
*
*****************************************************************************************/
BOOL ipcStackInit()
{
    if (ipcOsalMutexOpen(&ipcStack_glb.connMutex) != IPCE_OK)
    {
        ipcError(IPCLOG_ROUTER, ("Error opening mutex for connList failed!\n"));
        ipcStack_glb.state = IPC_STACK_STATE_ERROR;
        return FALSE;
    }
              
    if (ipcQueueOpen(&ipcStack_glb.receivedPacketQueue) != IPCE_OK)
    {
        ipcError(IPCLOG_ROUTER, ("Error opening ipcRouterPacketQueue\n"));
        ipcStack_glb.state = IPC_STACK_STATE_ERROR;
        ipcOsalMutexClose(ipcStack_glb.connMutex);
        return FALSE;
    }

    if (!ipcDeviceInit())
    {
        ipcError(IPCLOG_DEVICE, ("Error initializing device layer\n"));
        ipcStack_glb.state = IPC_STACK_STATE_ERROR;
        ipcQueueClose(&ipcStack_glb.receivedPacketQueue);
        ipcOsalMutexClose(ipcStack_glb.connMutex);
        return FALSE;
    }

    if (!ipcRouterInit())
    {
        ipcError(IPCLOG_ROUTER, ("Error initializing router layer\n"));
        ipcStack_glb.state = IPC_STACK_STATE_ERROR;
        ipcDeviceDestroy();
        ipcQueueClose(&ipcStack_glb.receivedPacketQueue);
        ipcOsalMutexClose(ipcStack_glb.connMutex);
        return FALSE;
    }

    if (!ipcSessionInit())
    {
        ipcError(IPCLOG_SESSION, ("Error initializing session layer\n"));
        ipcStack_glb.state = IPC_STACK_STATE_ERROR;
        ipcRouterDestroy();
        ipcDeviceDestroy();
        ipcQueueClose(&ipcStack_glb.receivedPacketQueue);
        ipcOsalMutexClose(ipcStack_glb.connMutex);
        return FALSE;
    }

    /* All layers of the IPC Stack have been initialized so its now ok for the
     * device layer to begin communication (which it will do once we set the
     * IPC stack state below */
    ipcStack_glb.state = IPC_STACK_STATE_RUNNING;
#ifdef START_UP_TIME
    printk(KERN_ALERT "ipcStack_glb.state = IPC_STACK_STATE_RUNNING\n");
    printk(KERN_ALERT "time = %u msecs\n",jiffies_to_msecs(jiffies-ulStartUpTime));
#endif
    return TRUE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcStackTerminate
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function tells the main IPC thread to terminate
*   
*   PARAMETER(S):
*       void
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Jan2005  Motorola Inc.         add entry verify,  disable tiemr and close queue
* 09May2005  Motorola Inc.         Do not use semaphore for destroy
* 31Oct2005  Motorola Inc.         Used virtual shared memory  instead of physical shared memory for destroy message packet.
* 13Jan2006  Motorola Inc.         Try different memory types
* 17Jan2007  Motorola Inc.         Don't put packet in  IPC_STACK_STATE_NULL
* 23Feb2007  Motorola Inc.         IPC_PROCSHM replaces previous memtype definition
*
*****************************************************************************************/
void ipcStackTerminate()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packet = NULL;
    int try_count = 0;
    int allocFlags[] = { IPC_PROCSHM, 0 };
    int i = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcDebugLog(IPCLOG_GENERAL, ("ipcStackTerminate is called\n"));

    if (ipcStack_glb.state != IPC_STACK_STATE_NULL)
    {
        /* Change state */
        ipcStack_glb.state = IPC_STACK_STATE_TERMINATED;

        for (packet = NULL, try_count = 0;
                packet == NULL;
                try_count++)
        {
            for (i = 0; 
                    (packet == NULL) && (i < sizeof(allocFlags) / sizeof(*allocFlags));
                    i++)
            {
                packet = (ipcPacket_t *)ipcAlloc(IPC_PACKET_DATA_OFFSET + 1, 
                        allocFlags[i]);
            }
            if (packet == NULL)
            {
                ipcError(IPCLOG_SESSION, 
                        ("Failed to allocate terminate message, time %d\n", try_count));
                if (ipcOsalSleep(50) < 0)
                {
                    break;
                }
            }
        } 
        if (packet == NULL)
        {
            ipcError(IPCLOG_SESSION, 
                    ("Failed to allocate a terminate message, killed by user\n"));
            return;
        }

        packet->next = NULL;
        ipcSetMsgLength(packet, 1);
        ipcSetSrcAddr(packet, ipcStack_glb.ipcAddr);
        ipcSetSrcServiceId(packet, IPC_SRV_SESSIONCONTROL);
        ipcSetDestAddr(packet, IPC_INVALID_ADDR);
        ipcSetDestServiceId(packet, IPC_SRV_SESSIONCONTROL);
        ipcSetMsgFlags(packet, IPC_PACKET_CONTROL);
        ipcSetMsgOpcode(packet, IPC_MSG_DESTROY);    

        /*  Inform the main thread to shut down all other resources */
        DVRTDataIndication(packet, IPC_INVALID_PORT);

        /*  Wait until the main thread is terminated */
        while (ipcStack_glb.state != IPC_STACK_STATE_NULL)
        {
            if (ipcOsalSleep(10) < 0)
            {
                ipcError(IPCLOG_SESSION,
                        ("ipcStackTerminate failed, interrupted!\n"));
                return;
            }
        }
    }

    ipcStackDestroy();
    ipcQueueClose(&ipcStack_glb.receivedPacketQueue);
    ipcOsalMutexClose(ipcStack_glb.connMutex);
    ipcPlatformDestroy();
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcStackDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to close all stack resources
*   
*   PARAMETER(S):
*       void
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09May2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed destroy sequence to session->router->device
*
*****************************************************************************************/
void ipcStackDestroy()
{
    ipcSessionDestroy();
    ipcRouterDestroy();
    ipcDeviceDestroy();
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAddAllocator
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function populates the allocator table entry for the specified type of memory
*   and opens a new mutex for it.
*   
*   PARAMETER(S):
*       memType: memory type index to add allocator for
*       alloc: pointer to platform-specific allocator function
*       free: pointer to platform-specific free function
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jun2007  Motorola Inc.         Initial Creation
*****************************************************************************************/
void ipcAddAllocator(unsigned int memType, ipcAllocFunc_t alloc, ipcFreeFunc_t free)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (memType >= IPC_MAX_MEM_TYPES)
    {
        ipcError(IPCLOG_GENERAL, ("ipcAddAllocator: invalid memType %d\n", memType));
        return;
    }
    
    if (ipcAllocatorTable[memType].mutex != 0)
    {
        ipcError(IPCLOG_GENERAL, ("ipcAddAllocator: memType %d already has allocator\n", memType));
        return;
    }    
    
    rc = ipcOsalMutexOpen(&ipcAllocatorTable[memType].mutex);
    if (rc != IPC_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcAddAllocator: ipcOsalMutexOpen failed! return code = %d\n", rc));
        return;
    }

    ipcAllocatorTable[memType].alloc = alloc;
    ipcAllocatorTable[memType].free = free;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAddAllocatorAlias
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function copies another allocator table entry so that the same allocator is used
*   for a new memory type index as well.  Since a common allocator is used, the same 
*   mutex will be used as well instead of creating a new one.
*   
*   PARAMETER(S):
*       newMemType: memory type index that will share the existing allocator
*       existingMemType: memory type index that already has an allocator assigned
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jun2007  Motorola Inc.         Initial Creation
*****************************************************************************************/
void ipcAddAllocatorAlias(unsigned int newMemType, unsigned int existingMemType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ((newMemType >= IPC_MAX_MEM_TYPES) || (existingMemType >= IPC_MAX_MEM_TYPES))
    {
        ipcError(IPCLOG_GENERAL, ("ipcAddAllocatorAlias: invalid memType [%d <- %d]\n",
            newMemType, existingMemType));
        return;
    }
    
    if (ipcAllocatorTable[newMemType].mutex != 0)
    {
        ipcError(IPCLOG_GENERAL, ("ipcAddAllocator: newMemType %d already has allocator\n", newMemType));
        return;
    }

    if (ipcAllocatorTable[existingMemType].mutex == 0)
    {
        ipcError(IPCLOG_GENERAL, ("ipcAddAllocator: existingMemType %d has no allocator\n", existingMemType));
        return;
    }

    ipcAllocatorTable[newMemType] = ipcAllocatorTable[existingMemType];
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveAllocator
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function clears the allocator table entry for the specified type of memory
*   and close the mutex created for it.
*   
*   PARAMETER(S):
*       memType: memory type index to remove allocator for
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jun2007  Motorola Inc.         Initial Creation
*****************************************************************************************/
void ipcRemoveAllocator(unsigned int memType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (memType >= IPC_MAX_MEM_TYPES)
    {
        ipcError(IPCLOG_GENERAL, ("ipcRemoveAllocator: invalid memType %d\n", memType));
        return;
    }
    
    ipcAllocatorTable[memType].alloc = ipcInvalidAlloc;
    ipcAllocatorTable[memType].free = ipcInvalidFree;
    
    ipcOsalMutexClose(ipcAllocatorTable[memType].mutex);
    ipcAllocatorTable[memType].mutex = 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveAllocatorAlias
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function clears the allocator table entry for the specified type of memory.
*   Since it was just an alias for another memory type with an existing allocator and no 
*   new mutex was created for it, there is no need to close the mutex.
*   
*   PARAMETER(S):
*       memType: memory type index to remove allocator for
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jun2007  Motorola Inc.         Initial Creation
*****************************************************************************************/
void ipcRemoveAllocatorAlias(unsigned int memType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (memType >= IPC_MAX_MEM_TYPES)
    {
        ipcError(IPCLOG_GENERAL, ("ipcRemoveAllocatorAlias: invalid memType %d\n", memType));
        return;
    }
    
    ipcAllocatorTable[memType].alloc = ipcInvalidAlloc;
    ipcAllocatorTable[memType].free = ipcInvalidFree;
    ipcAllocatorTable[memType].mutex = 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcLockMem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to get the memory lock.
*   
*   PARAMETER(S):
*       memType: index of memory type to lock
*   RETURN: none
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Ported to linux
* 16Jun2005  Motorola Inc.         Remove #ifndef linux, use OSAL func
* 09Dec2005  Motorola Inc.         Modify the comments and function header description.
* 11Jan2006  Motorola Inc.         Check the return value of ipcOsalMutexLock()
* 16Feb2007  Motorola Inc.         Move to ipc_main for all SIPC platforms
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void ipcLockMem(unsigned int memType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (memType >= IPC_MAX_MEM_TYPES)
    {
        ipcError(IPCLOG_GENERAL, ("ipcLockMem: invalid memType %d\n", memType));
        return;
    }

    if ((rc = ipcOsalMutexLock(ipcAllocatorTable[memType].mutex)) != IPC_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcLockMem: ipcOsalMutexLock failed! return code = %d\n", rc));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcUnlockMem
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to release the memory lock.
*   
*   PARAMETER(S):
*       memType: index of memory type to unlock
*   Retrun:	none
*   
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Ported to linux
* 16Jun2005  Motorola Inc.         Remove #ifdef linux, use OSAL func
* 09Dec2005  Motorola Inc.         Modify the comments and function header description.
* 11Jan2006  Motorola Inc.         Check the return value of ipcOsalMutexRelease()
* 16Feb2007  Motorola Inc.         Move to ipc_main for all SIPC platforms
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void ipcUnlockMem(unsigned int memType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (memType >= IPC_MAX_MEM_TYPES)
    {
        ipcError(IPCLOG_GENERAL, ("ipcUnlockMem: invalid memType %d\n", memType));
        return;
    }

    if ((rc = ipcOsalMutexRelease(ipcAllocatorTable[memType].mutex)) != IPC_OK)
    {
        ipcError(IPCLOG_GENERAL, ("ipcUnlockMem: ipcOsalMutexRelease failed! return code = %d\n", rc));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAllocImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the common allocator function used to allocate all types of memory
*   on all SIPC platforms.  Based on the memory type requested, it will call
*   the appropriate platform-specific allocator.
*   
*   PARAMETER(S):
*       bufSize: size of buffer to allocate (not including hidden header)
*       flags: bit field that specifies the type of memory being allocated
*                          as well as any other platform-specific flags
*       
*   RETURN: a pointer to buffer or NULL if no memory available
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void *ipcAllocImp(unsigned int bufSize, unsigned int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMBufHead_t *pBufHead;
    unsigned int memType = flags & MEMTYPE_FLAG_MASK;
    int poolId;
    unsigned int totalBufSize = ipcSMAllocSize(bufSize);
/*--------------------------------------- CODE -----------------------------------------*/
#ifndef HAVE_PHYSICAL_SHM
    if (memType == IPC_PHYSSHM)
    {
        /* Substitute virtual shared memory if physical shared memory
         * is not available */
        memType = IPC_PROCSHM;
    }
#endif

    pBufHead = ipcAllocatorTable[memType].alloc(totalBufSize, flags);

    if(pBufHead == NULL)
    {
        ipcError(IPCLOG_GENERAL, ("ipcAlloc out of memory type %d, size = %d\n", memType, bufSize));
        return (NULL);
    }
    else
    {
        /* preserve pool ID from platform allocator */
        poolId = ipcSMGetPoolId(pBufHead);

        /* initialize all other buffer header fields (set reference count to 1) */
        ipcSMInitBuffer(pBufHead, memType, poolId, LOCAL_CORE_ID, 1);

#ifndef IPC_RELEASE
        ipcSMSetDebugInfo(pBufHead, bufSize, (unsigned short)ipcOsalGetProcessId());
        
        ipcDebugLog(IPCLOG_MEMTRACK, 
            ("MEMTRACK: Alloc flags=%d, size=%d, ptr=%.8X, caller=%s, tick=%u\n",
             flags, bufSize, (UINT32)ipcSMHeaderToData(pBufHead),
             ipcOsalGetProcessName(ipcOsalGetProcessId()),
             ipcOsalGetTickCount()));
#endif

        return ipcSMHeaderToData(pBufHead);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFreeImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the common free function used to free all types of memory on all
*   SIPC platforms.  Based on the memory type in the buffer header, it will call
*   the appropriate platform-specific free function.
*   
*   PARAMETER(S):
*       pointer to buffer 
*                  
*   RETURN: IPC_OK or IPCEBADBUF
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2007  Motorola Inc.         Initial Creation 
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
int ipcFreeImp(void *pBuf)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMBufHead_t *pBufHead;
    int refCount;
    int ret;
    unsigned int memType;
#ifdef HAVE_PHYSICAL_SHM
    int multicore;
#endif
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcSMCheckBuffer(pBuf, "ipcFreeImp"))
    {
        return(IPCEBADBUF); /* Invalid buffer */
    }
    
    pBufHead = ipcSMDataToHeader(pBuf);
    memType = ipcSMGetMemType(pBufHead);
    
    ipcLockMem(memType);
    
#ifdef HAVE_PHYSICAL_SHM
    multicore = ipcSMGetMulticore(pBufHead);

    /* The multicore semaphore should be arounded by local mutex */
    if (multicore)
    {
        /* There are references to this buffer on both cores so we need to lock
         * the multicore semaphore */
        ipcMulticoreSemaWait();         
    }
#endif

    refCount = ipcSMGetRefCount(pBufHead) - 1; /* subtract one from reference count */

    /* check if refcount is valid */
    if (refCount < 0)
    {
        /* Error! Invalid buffer */
        ret = IPCEBADBUF;
    }
    else
    {
        ipcSMSetRefCount(pBufHead, refCount);
        
        if (refCount > 0)
        {
            /* there are more references so do not free buffer yet */
            ret = IPC_OK;
        }
        else
        {
            ret = ipcAllocatorTable[memType].free(pBufHead);
        }
    }
    
#ifdef HAVE_PHYSICAL_SHM    
    /* The multicore semaphore should be surrounded by local mutex */
    if (multicore)
    {
        ipcMulticoreSemaSignal();         
    }
#endif /* HAVE_PHYSICAL_SHM */

    ipcUnlockMem(memType);

#ifndef IPC_RELEASE
    if (ret == IPC_OK)
    {
        ipcDebugLog(IPCLOG_MEMTRACK,
            ("MEMTRACK: Free ptr=%.8X, caller=%s, tick=%u\n",
             (UINT32)pBuf, ipcOsalGetProcessName(ipcOsalGetProcessId()),
             ipcOsalGetTickCount()));
    }
    else
    {
        ipcError(IPCLOG_MEMTRACK, ("ipcFreeImp failed"));
    }
#endif /* IPC_RELEASE */

    return ret;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcIncRefCount
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function increments the reference count for a memory buffer.
*   
*   PARAMETER(S):
*       pBuf: pointer to buffer
*       amount: amount to increment reference count by (pass in zero to
*                           leave ref count the same and return current count)
*                  
*   RETURN: new reference count or IPCEBADBUF
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 07Apr2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Move the func imp from Spl_IncRefCount to ipcIncRefCount and add mem overrun check to all kind of mem and move lock to function beginning
* 13Dec2006  Motorola Inc.         Remove RTXC buffer overrun checking
* 16Feb2007  Motorola Inc.         Convert to common function for all allocators 
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
int ipcIncRefCount(void *pBuf, int amount)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMBufHead_t *pBufHead;
    int newCount;
    unsigned int memType;
#ifdef HAVE_PHYSICAL_SHM
    int multicore;
#endif
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcSMCheckBuffer(pBuf, "ipcIncRefCount"))
    {
        return(IPCEBADBUF); /* Invalid buffer */
    }

    pBufHead = ipcSMDataToHeader(pBuf);
    memType = ipcSMGetMemType(pBufHead);

    ipcLockMem(memType);

#ifdef HAVE_PHYSICAL_SHM
    multicore = ipcSMGetMulticore(pBufHead);

    /* The multicore semaphore should be arounded by local mutex */
    if (multicore)
    {
        /* There are references to this buffer on both cores so we need to lock
         * the multicore semaphore */
        ipcMulticoreSemaWait();         
    }
#endif

    newCount = ipcSMGetRefCount(pBufHead);
    
    /* check if reference count and amount to increment are valid */
    if ((newCount < 1) || (amount < 0))
    {
        /* error, invalid ref count or amount param */
        newCount = IPCEBADBUF;
    }
    else
    {
        newCount += amount;
        if (newCount > 255)
        {
            /* We have exceeded the maximum reference count */
            newCount = IPCEINVAL;
        }
        else
        {
            ipcSMSetRefCount(pBufHead, newCount);
        }
    }
        
#ifdef HAVE_PHYSICAL_SHM
    /* The multicore semaphore should be surrounded by local mutex */
    if (multicore)
    {
        ipcMulticoreSemaSignal();         
    }
#endif

    ipcUnlockMem(memType);
  
    ipcDebugLog(IPCLOG_MEMTRACK,
            ("MEMTRACK: IncRefCount ptr=%.8X, amount=%d, newCount=%d, tick=%u\n",
             (UINT32)pBuf, amount, newCount, ipcOsalGetTickCount()));

    return newCount;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcInvalidAlloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the default stub function used for unsupported memory types.
*   The allocator table is statically initialized to use this function for all
*   memory types.  Each platform then install its own allocator functions
*   for all supported memory types.
*   
*   PARAMETER(S):
*       bufSize: size of buffer to allocate (not including hidden header)
*       flags: bit field that specifies the type of memory being allocated
*                          as well as any other platform-specific flags
*                  
*   RETURN: always returns NULL
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void *ipcInvalidAlloc(UINT32 bufSize, UINT32 flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return NULL;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcInvalidFree
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the default stub function used for unsupported memory types.
*   The allocator table is statically initialized to use this function for all
*   memory types.  Each platform then install its own allocator functions
*   for all supported memory types.
*   
*   PARAMETER(S):
*       pointer to buffer 
*                  
*   RETURN: always returns IPCEBADBUF
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Feb2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcInvalidFree(void *pBuf)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return IPCEBADBUF;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateControlPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to allocate and initialize an IPC Control Packet
*   
*   PARAMETER(S):
*       length: length of th emessage data
*       opcode: type of control message
*       destAddr: IPC address we are sending this to
*       destServiceId: Service that should handle this packet (should be
*                                  IPC_SRV_SESSIONCONTROL, IPC_SRV_ROUTERCONTROL, or
*                                  IPC_SRV_DEVICECONTROL for control messages)
*                  
*   RETURN: pointer to packet
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Jan2005  Motorola Inc.         Initial Creation
* 25Jan2005  Motorola Inc.         Shared memory debug
* 12Sep2005  Motorola Inc.         Removed channel
* 16Feb2006  Motorola Inc.         Calculate CRC for data
* 23Feb2007  Motorola Inc.         IPC_PHYSSHM replaces previous memtype definition
* 22Jun2007  Motorola Inc.         Initialize next pointer to invalid value
*
*****************************************************************************************/
ipcPacket_t * ipcCreateControlPacket(UINT32 length, UINT8 opcode, ipcAddress_t destAddr,
                                     ipcServiceId_t destServiceId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packet;
/*--------------------------------------- CODE -----------------------------------------*/
    packet = (ipcPacket_t *)ipcAlloc(IPC_PACKET_DATA_OFFSET + length,
                                     IPC_PHYSSHM);

    if (packet == NULL) 
    {
        return (NULL);
    }

    memset(&packet->next, BUFHEAD_MARKER, sizeof(packet->next));
    ipcSetMsgLength(packet, length);
    ipcSetSrcAddr(packet, ipcStack_glb.ipcAddr);
    ipcSetSrcServiceId(packet, destServiceId); /* src service id is always 0? */
    ipcSetDestAddr(packet, destAddr);
    ipcSetDestServiceId(packet, destServiceId);
    ipcSetMsgFlags(packet, IPC_PACKET_RELIABLE | IPC_PACKET_CONTROL | IPC_PACKET_CRC);
    ipcSetMsgOpcode(packet, opcode);

    return packet;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCopyPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used copy an IPC packet into a new buffer
*   
*   PARAMETER(S):
*       packet: IPC packet to copy
*       flags: flags to pass to allocator (physical shared memory, etc.)
*                  
*   RETURN: pointer to new packet
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Jan2005  Motorola Inc.         Initial Creation
* 25Jan2005  Motorola Inc.         Shared memory debug
* 15Apr2005  Motorola Inc.         Check return value of ipcAlloc()
* 05Jul2005  Motorola Inc.         Added allocator flags
* 22Jun2007  Motorola Inc.         Make size aligned to even value in memcpy
*
*****************************************************************************************/
ipcPacket_t * ipcCopyPacket(ipcPacket_t *packet, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packetCopy;
    int size;
/*--------------------------------------- CODE -----------------------------------------*/
    size = ipcGetPacketStorageSize(packet);
    packetCopy = (ipcPacket_t *)ipcAlloc(size, flags);
    if (NULL == packetCopy)
    {
        return NULL;
    }
    
    memcpy(packetCopy, packet, (size + 1) & ~1);
    packetCopy->next = NULL;

    return packetCopy;
}
