/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_mu_shm.c
*   FUNCTION NAME(S): ipcMulticoreSemaClose
*                     ipcMulticoreSemaOpen
*                     ipcMulticoreSemaSignal
*                     ipcMulticoreSemaWait
*                     muReadIpcPacket
*                     muReadShmControlMessage
*                     muShmExit
*                     muShmInit
*                     muShmLock
*                     muShmUnlock
*                     muWriteIpcPacket
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains functions for using the MU to transfer data through
*   physical shared memory.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 11Oct2005  Motorola Inc.         Initial Creation
* 15Mar2006  Motorola Inc.         Pass in address of multicore sema
* 01Jun2007  Motorola Inc.         Use semaphore to wait other core lock physical memory
*****************************************************************************************/

#ifdef HAVE_PHYSICAL_SHM
/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "mu_drv.h"
#include "ipc_mu_shm.h"
#include "ipc_shared_memory.h"
#include "ipc_osal.h"
#include "ipc_router_interface.h"
#include "ipc_device_interface.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/

/* Semaphore that works across both cores */
static ipcMulticoreSema_t *ipcMulticoreSema = 0;

/* This local semaphore is used for the multicore semaphore */
static ipcSemaHandle_t ipcLocalMulticoreSema = (ipcSemaHandle_t)0;

/* This semaphore is used to indicate when the other core has locked memory */
static ipcSemaHandle_t muLockAckSema = (ipcSemaHandle_t)0;

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

void muReadIpcPacket(int channel, int data);
void muReadShmControlMessage(int channel, int data);

/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muShmInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function registers callbacks for the MU channels needed for IPC and
*   physical shared memory communication.  It will also open the semaphore
*   used by the BP to wait for shared memory initialization to complete.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Oct2005  Motorola Inc.         Initial Version
* 12Dec2006  Motorola Inc.         Moved ipcPrintMu callback out. 
* 01Jun2007  Motorola Inc.         Open semaphore to wait other core lock physical memory
*
*****************************************************************************************/
void muShmInit(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalSemaOpen(&muLockAckSema, 0) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("failed to open semaphore\n"));
    }

    if (muRegisterCallback(MU_CHANNEL_SHM_INIT, ipcHandleSMInitMessage) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not register for MU_CHANNEL_SHM_INIT\n"));
    } 
    
    if (muRegisterCallback(MU_CHANNEL_SHM_CONTROL, muReadShmControlMessage) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not register for MU_CHANNEL_SHM_CONTROL\n"));
    }
    
    if (muRegisterCallback(MU_CHANNEL_IPC_PACKET, muReadIpcPacket) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not register for MU_CHANNEL_IPC_PACKET\n"));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muShmExit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function remove callbacks for the MU channels.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Feb2006  Motorola Inc.         Initial Creation
* 12Dec2006  Motorola Inc.         Moved ipcPrintMu callback out. 
* 01Jun2007  Motorola Inc.         Close semaphore to wait other core lock physical memory
*
*****************************************************************************************/
void muShmExit(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (muRegisterCallback(MU_CHANNEL_SHM_INIT, NULL) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not remove callback for MU_CHANNEL_SHM_INIT\n"));
    } 
    
    if (muRegisterCallback(MU_CHANNEL_SHM_CONTROL, NULL) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not remove callback for MU_CHANNEL_SHM_CONTROL\n"));
    }
    
    if (muRegisterCallback(MU_CHANNEL_IPC_PACKET, NULL) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not remove callback for MU_CHANNEL_IPC_PACKET\n"));
    }

    if (ipcOsalSemaClose(muLockAckSema) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("failed to close semaphore\n"));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muWriteIpcPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends an IPC Packet to the other core
*   
*   PARAMETER(S):
*       packet: pointer to IPC Packet to send
*       
*   RETURN: 0 for success, -1 for failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 29Aug2005  Motorola Inc.         Initial Version
*
*****************************************************************************************/
int muWriteIpcPacket(void *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSMBufHead_t *bufhead;
    int data;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsShmReady || !shmConnected)
    {
        return -1;
    }
    
    bufhead = ipcSMDataToHeader(packet);
    
    /* If we are sending a packet to the other side that has a reference count
     * greater than one, we are keeping a local reference.  This means we must
     * must set the multicore bit to sync the reference count between cores */
    if (ipcSMGetRefCount(bufhead) > 1)
    {
        ipcSMSetMulticore(bufhead, 1);
    }

    /* Translate data pointer to physical memory */
    data = (int)ipcSMVirtualToPhysical(packet);

    return muWrite(MU_CHANNEL_IPC_PACKET, data);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muReadIpcPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the callback function that is called whenever an IPC Packet is 
*   received via the MU
*   
*   PARAMETER(S):
*       channel: MU Channel ID where data was received
*       data: pointer to the IPC Packet
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 29Aug2005  Motorola Inc.         Initial Version
*
*****************************************************************************************/
void muReadIpcPacket(int channel, int data)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packet;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.state == IPC_STACK_STATE_RUNNING)
    {
        packet = (ipcPacket_t *)ipcSMPhysicalToVirtual(data);

        DVRTDataIndication(packet, ipcShmPortId);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muReadShmControlMessage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the callback function that is called whenever a shared memory
*   control message is received via the MU
*   
*   PARAMETER(S):
*       channel: MU Channel ID where data was received
*       data: shared memory control opcode
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 29Aug2005  Motorola Inc.         Initial Version
* 01Jun2007  Motorola Inc.         Use semaphore to wait other core lock physical memory
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void muReadShmControlMessage(int channel, int data)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDebugLog(IPCLOG_GENERAL, ("Shm Control: %d\n", data));
    
    switch(data)
    {
    case MU_SHM_SEMA_SIGNAL:
        ipcOsalSemaSignal(ipcLocalMulticoreSema);
        break;

    case MU_SHM_LOCK:
        ipcLockMem(IPC_PHYSSHM);
        muWrite(MU_CHANNEL_SHM_CONTROL, MU_SHM_LOCK_ACK);
        break;
        
    case MU_SHM_UNLOCK:
        ipcUnlockMem(IPC_PHYSSHM);
        break;
        
    case MU_SHM_LOCK_ACK:
        ipcOsalSemaSignal(muLockAckSema);
        break;
        
    default:
        /* ERROR: Unkown opcode */
        ipcError(IPCLOG_DEVICE, ("Unknown MU Shm Control opcode: %d\n", data));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muShmLock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function locks memory on both cores by sending an MU message to the
*   other core and waiting for acknowledgment.  This function should be called
*   VERY rarely as it will hurt performance considerably.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 07Jul2005  Motorola Inc.         Initial Version
* 20Jul2005  Motorola Inc.         On AP, we should give up CPU when waiting the ack from BP
* 01Jun2007  Motorola Inc.         Use semaphore to wait other core lock physical memory
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void muShmLock(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcLockMem(IPC_PHYSSHM);
    ipcOsalSemaReset(muLockAckSema);
    if (muWrite(MU_CHANNEL_SHM_CONTROL, MU_SHM_LOCK) == 0)
    {
        /* If the write succeeds, wait for ack */
        if (ipcOsalSemaWait(muLockAckSema, 500) < 0)
        {
            /* We've waited long enough (500 ms) */
            ipcError(IPCLOG_DEVICE, ("*** muShmLock failed!\n"));
        }
    }
    /* Otherwise, the other core is not responding */
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muShmUnlock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function unlocks memory on both cores by sending an MU message to the
*   other core.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 07Jul2005  Motorola Inc.         Initial Version
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void muShmUnlock(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    muWrite(MU_CHANNEL_SHM_CONTROL, MU_SHM_UNLOCK);
    ipcUnlockMem(IPC_PHYSSHM);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMulticoreSemaOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will initialize the multicore semaphore
*   
*   PARAMETER(S):
*       multicoreSemaAddr: location of multicore semaphore
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Aug2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcMulticoreSemaOpen(void *multicoreSemaAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcMulticoreSema = (ipcMulticoreSema_t *)multicoreSemaAddr;
    
    rc = ipcOsalSemaOpen(&ipcLocalMulticoreSema, 0);
    if (rc < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Error opening multicore semaphore Sema: %d\n", rc));
    }

    if (LOCAL_CORE_ID == LINUX_CORE_ID)
    {
        ipcMulticoreSema->coreWantsEntry[0] = 0;
        ipcMulticoreSema->coreWantsEntry[1] = 0;
        ipcMulticoreSema->turn = RTXC_CORE_ID;
        ipcMulticoreSema->semaCount = 0;
        ipcMulticoreSema->notificationRequest = 0;
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMulticoreSemaClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will close the multicore semaphore
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Aug2005  Motorola Inc.         Initial Creation
* 05Jan2006  Motorola Inc.         Close the local semaphore
*
*****************************************************************************************/
void ipcMulticoreSemaClose(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcOsalSemaClose(ipcLocalMulticoreSema);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMulticoreSemaWait
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will wait until the multicore semaphore has been signalled.
*   
*   [NOTE] the calls to function pair ipcMulticoreSemaWait/ipcMulticoreSemaSignal 
*         must be surrounded by a local mutex
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Aug2005  Motorola Inc.         Initial Creation
* 05Jan2006  Motorola Inc.         Bug fix for MulticoreSema race condition
*
*****************************************************************************************/
void ipcMulticoreSemaWait(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int success = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    while (!success)
    {
        //TODO: Disable task switches here (use macro in ipc_platform_shared_memory.h
        //                                  so it works for both linux and rtxc)
        petersonEntry(); /* Enter multicore critical section */
        
        if (ipcMulticoreSema->semaCount == 0)
        {
            --ipcMulticoreSema->semaCount;
            success = TRUE;
        }
        else
        {
            /* Tell other core to notify us when sema is signalled */
            ipcMulticoreSema->notificationRequest = 1;
        }
        
        petersonExit(); /* Exit multicore critical section */
        //TODO: Re-enable task switches
        
        if (!success)
        {
            ipcOsalSemaWait(ipcLocalMulticoreSema, IPC_OSAL_INFINITE);
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMulticoreSemaSignal
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will signal the multicore semaphore.
*   
*   [NOTE] the calls to function pair ipcMulticoreSemaWait/ipcMulticoreSemaSignal 
*         must be surrounded by a local mutex
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Aug2005  Motorola Inc.         Initial Creation
* 05Jan2006  Motorola Inc.         Bug fix for MulticoreSema race condition
*
*****************************************************************************************/
void ipcMulticoreSemaSignal(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    //TODO: Disable task switches here
    petersonEntry(); /* Enter multicore critical section */
    
    ++ipcMulticoreSema->semaCount;
    
    if (ipcMulticoreSema->notificationRequest)
    {
        ipcMulticoreSema->notificationRequest = FALSE;
        muWrite(MU_CHANNEL_SHM_CONTROL, MU_SHM_SEMA_SIGNAL);
    }
    
    petersonExit(); /* Exit multicore critical section */
    //TODO: Re-enable task switches
}

#endif /* HAVE_PHYSICAL_SHM */
