/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_conn.c
*   FUNCTION NAME(S): ipcConnAddOutofOrderPacket
*                     ipcConnConsumeData
*                     ipcConnDeliverData
*                     ipcConnDestroy
*                     ipcConnGotAck
*                     ipcConnGotPacket
*                     ipcConnInit
*                     ipcConnRetransmitData
*                     ipcConnTimerTick
*                     ipcSendAck
*                     ipcSendFlow
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements the sliding-window mechanism of IPC stack, which will be
*   used in both router layer and session sockets, providing reliable data
*   communication between endpoints (also mentioned as a connection), flow 
*   control, as well as interface to router and session layer sockets.
*
*   An endpoint of a connection is defined as a struct ipcConn_t, it maintains
*   the information of a connection, a series of function are also defined to
*   operate on the struct.
*
*   Each packet is assigned a sequence number, so does every ack packets,
*   while sending a packet out via a connection, it will be chained in a list, 
*   in case our peer doesn't get the packet correctly, if we don't get an 
*   acknowledgment of the packet from the peer in some certain time, we will
*   think the packet gets lost, so we will resend the packet. If we still fail
*   after resending the packet for some times, we'll think the connection is
*   broken, and then shut it down. And the packet will be removed from the list
*   if the ack arrives as expected.
*
*   In the receiver side, if it got a packet, it will send an acknowledgment 
*   back, and put the packet to a temporary list if the sequence number of the
*   packet is not the one it's expected to be; otherwise the packet will be
*   handled by router layer or session layer socket.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 10Oct2005  Motorola Inc.         Removed potential duplicated locking
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 23Feb2007  Motorola Inc.         Update memory type defs
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_session.h"
#include "ipc_conn.h"
#include "ipc_osal.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

#define LOCK_CONN(conn) conn->ops->lock(conn) 
#define UNLOCK_CONN(conn) conn->ops->unlock(conn)

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initialize a connection.
*   
*   PARAMETER(S):
*       conn: the connection to be initialized
*       defaultWindowSize: the default rx window size of the connection
*       privateData: the private data of the connection, it can be anything,
*                                it may be used by the function pointers in ops
*       ops: some function pointers for the connection, which contains:
*                        onDataArrival: called when a packet comes in the conn
*                        onTxWindowFull: called when the tx window is full
*                        onTxWindowAvailable: called thwn the tx window gets available
*                        onMaxRetries: called when a packet has been resent some times
*                        onSentListEmpty: called when all packets are sent out
*                        deliverData: function to send a packet out
*                        lock: function to lock the connection
*                        unlock: function to unlock the connection
*                        
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcConnInit(ipcConn_t* conn,
                 UINT8 defaultWindowSize, 
                 void* privateData,
                 ipcConnOperations_t *ops)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (conn != NULL)
    {
        ipcListInit(&conn->outOfOrderPackets);
        ipcListInit(&conn->sentPackets);
        conn->privateData = privateData;
        conn->rxWindowSize = defaultWindowSize;
        conn->nextTxSeqNum = ipcOsalGetTickCount();
        conn->maxTxSeqNum = conn->nextTxSeqNum + IPC_MAX_WINDOW_SIZE - 1;
        conn->ops = ops;
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Destroy a connection.
*   
*   PARAMETER(S):
*       conn: the connection to be destroyed
*       clearSentPackets: whether we should clear all sent packets
*       clearOutOfOrderPackets: whether we should clear out of order packets
*       lockNeeded: whether this operation needs to be locked
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Not reset rxWindowSize 
*
*****************************************************************************************/
void ipcConnDestroy(ipcConn_t* conn,
                    BOOL clearSentPackets,
                    BOOL clearOutOfOrderPackets,
                    BOOL lockNeeded)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link = NULL;
    ipcPacket_t* packet = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    if (lockNeeded)
    {
        if (LOCK_CONN(conn) < 0)
        {
            ipcError(IPCLOG_GENERAL, ("Failed to lock conn\n"));
            return;
        }
    }
    if (clearSentPackets)
    {
        while (NULL != (link = ipcListRemoveHead(&conn->sentPackets)))
        {
            packet = IPC_NEXT2_TO_PACKET(link);
            ipcFree(packet);
        }
        conn->maxTxSeqNum = conn->nextTxSeqNum + IPC_MAX_WINDOW_SIZE - 1;
    }
    if (clearOutOfOrderPackets)
    {
        while (NULL != (link = ipcListRemoveHead(&conn->outOfOrderPackets)))
        {
            ipcFree(link);
        }
    }
    if (lockNeeded)
    {
        UNLOCK_CONN(conn);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnTimerTick
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Handle timer tick for a connection.
*   
*   There's only one timer for all packets in a connection, in each time slice,
*   the countDown of every packet in the list sentPackets of conn is decremented,
*   it will be resent if countDown reduces to 0, the connection will be shutdown
*   if max retry times arrives.
*   
*   PARAMETER(S):
*       conn: the conn to operate on
*          
*   RETURN: IPC_OK on success.
*   
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 17Jan2007  Motorola Inc.         Unlock while retransmitting 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcConnTimerTick(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t link = NULL;
    ipcPacket_t* packet = NULL;
    UINT8 countDown;
    UINT8 retryTimes;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((conn != NULL))
    {      
        if (LOCK_CONN(conn) < 0)
        {
            return IPCEBUSY;
        }
        for (link = conn->sentPackets.head;
             link != NULL;
             link = link->next)
        {
            packet = IPC_NEXT2_TO_PACKET(link);

            countDown = ipcGetCountDown(packet) - 1;
            ipcSetCountDown(packet, countDown);
            if (countDown == 0)
            {
                ipcWarning(IPCLOG_GENERAL,
                    ("Packet # %d countDown got to 0\n",
                     ipcGetSeqNum(packet)));

                ipcSetCountDown(packet, IPC_MAX_COUNTDOWN);
                
                retryTimes = ipcGetRetryTimes(packet) + 1;
                ipcSetRetryTimes(packet, retryTimes);
                if (retryTimes >= IPC_MAX_RETRY_TIMES)
                {
                    /* we should return here! */
                    UNLOCK_CONN(conn);
                    conn->ops->onMaxRetries(conn, packet);
                    return IPC_ERROR;
                }
                else
                {
                    UNLOCK_CONN(conn);
                    ipcConnRetransmitData(conn, packet);
                    /* XXX: Here we'll return immediately. We can't go ahead because
                     * now the list of packets may get changed, perhaps the main thread
                     * gets an ack and removes the packet from the list since the lock
                     * is not held */
                    return 0;
                }
            }
        }
        UNLOCK_CONN(conn);
    }
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnDeliverData
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send data out from a connection.
*   
*   The connection is used to provide reliable communication (optional), the 
*   behavior is controled by the parameters. The data is reliable only when the
*   parameter isReliable is TRUE, if isReliable is FALSE, the caller thread will
*   block until all the packets in the list sentPacket have been acked if 
*   mayBlock is set to TRUE;
*   
*   If mayBlock, the function will block until there's space in Tx window.
*   
*   PARAMETER(S):
*       conn: the connection used to send data out
*       packet: the packet to send
*       isReliable: whether the packet is sent reliably
*       mayBlock: do we allow the function call to block?
*       
*   RETURN: IPC_OK if no error occurs; otherwise a negative value;
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Bugfix of flow control on  unreliable delivery
* 01Nov2006  Motorola Inc.         Change conn timer to nonperiodic
* 18Jan2007  Motorola Inc.         Lock connMutex if restart timer 
*
*****************************************************************************************/
int ipcConnDeliverData(ipcConn_t* conn, 
                       ipcPacket_t* packet,
                       BOOL isReliable,
                       BOOL mayBlock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
    
/*--------------------------------------- CODE -----------------------------------------*/
    if ((conn == NULL) || (packet == NULL))
    {
        return IPCEFAULT;
    }
    
    if (!isReliable)
    {
        if ((!mayBlock) && (conn->sentPackets.size != 0))
        {
            return IPCEWOULDBLOCK;
        }

         /* FIXME: We are not going to add a semaphore to mess things up... */
        while (conn->sentPackets.size != 0)
        {
            if (ipcOsalSleep(50) < 0)
            {
                return IPCEINTR;
            }
        }
    }

    /* wait until the window is not full */
    while (SEQ_GEQ(conn->nextTxSeqNum, conn->maxTxSeqNum + 1))
    {
        ipcWarning(IPCLOG_GENERAL,
            ("Waiting for window, nextTx=%d, maxTx=%d\n",
             conn->nextTxSeqNum, conn->maxTxSeqNum));
        rc = conn->ops->onTxWindowFull(conn, mayBlock);
        if (rc < 0)
        {
            return rc;
        }
    }

    if (!isReliable)
    {
        /* clear the RELIABLE flag in the packet */
        ipcSetMsgFlags(packet, ipcGetMsgFlags(packet) & (~IPC_PACKET_RELIABLE));
        
        ipcDebugLog(IPCLOG_GENERAL,
            ("Delivering packet # %d\n", ipcGetSeqNum(packet)));

        /* FIXME: the nextTxSeqNum is not protected by locks, so this will be
         * a problem if two tasks are sending unreliable data simultaneously.
         */
        ipcSetSeqNum(packet, conn->nextTxSeqNum);
        rc = conn->ops->deliverData(conn, packet);

        if (rc == IPC_OK)
        {
            if (LOCK_CONN(conn) == IPCE_OK)
            {
                conn->nextTxSeqNum++;
                UNLOCK_CONN(conn);
            }
            else
            {
                ipcError(IPCLOG_GENERAL,
                        ("ipcConnDeliverData: Failed to lock connection\n"));
            }
        }
        return rc;
    }
    
    /* Set the RELIABLE flag in the packet */
    ipcSetMsgFlags(packet, ipcGetMsgFlags(packet) | IPC_PACKET_RELIABLE);
    
    if (conn->sentPackets.size == 0)
    {
        ipcSetMsgFlags(packet, ipcGetMsgFlags(packet) | IPC_PACKET_FIRST_ACKED);
    }

    if (LOCK_CONN(conn) < 0)
    {
        ipcError(IPCLOG_GENERAL,
                ("ipcConnDeliverData: Failed to lock connection\n"));
        return IPCEBUSY;
    }
    ipcIncRefCount(packet, 1);
    ipcSetSeqNum(packet, conn->nextTxSeqNum);
    ipcSetRetryTimes(packet, 0);
    ipcSetCountDown(packet, IPC_MAX_COUNTDOWN);
    
    conn->nextTxSeqNum++;

    ipcDebugLog(IPCLOG_GENERAL,
            ("Delivering packet # %d\n", ipcGetSeqNum(packet)));

    ipcListAddTail(&conn->sentPackets, IPC_PACKET_TO_NEXT2(packet));

    UNLOCK_CONN(conn);

    /* FIXME: if device layer (maybe) failed to deliver the packet, we will 
     * return an error to users, is it OK? */
    rc =  conn->ops->deliverData(conn, packet);

    if (!ipcOsalTimerIsActive(ipcStack_glb.connTimer))
    {
        if (ipcOsalMutexLock(ipcStack_glb.connMutex) == IPCE_OK)
        {
            ipcOsalTimerStart(ipcStack_glb.connTimer);
            ipcOsalMutexRelease(ipcStack_glb.connMutex);
        }
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnRetransmitData
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Retransmit a packet.
*   
*   PARAMETER(S):
*       conn: the connection from which the packet will be sent
*       packet: the packet to send
*       
*   RETURN: IPC_OK if no error occurs, otherwise a negative value;
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcConnRetransmitData(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcWarning(IPCLOG_GENERAL, 
        ("Retransmitting packet # %d\n", ipcGetSeqNum(packet)));
    /* FIXME: Shall we increase the refcount of the packet?
     * Generally, we have to, but when transmitting using shared memory, 
     * and if the packet is lost in transmission, then there might be
     * memory leak */
    ipcIncRefCount(packet, 1);

    /* Clear the first-acked flag, or mess things up */
    ipcSetMsgFlags(packet, ipcGetMsgFlags(packet) & ~IPC_PACKET_FIRST_ACKED);
    rc = conn->ops->deliverData(conn, packet);
    if (rc < 0)
    {
        /* retransmission failed, free the packet (just decrease refcount) */
        ipcFree(packet);
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnAddOutofOrderPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Add a packet to the list outOfOrderPacket in conn.
*   NOTE: This function is called assuming the connection is locked.
*   
*   PARAMETER(S):
*       conn: the connection which will store the packet
*       packet: the packet to add to the list
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcConnAddOutofOrderPacket(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *p = (ipcPacket_t*)conn->outOfOrderPackets.tail;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcWarning(IPCLOG_GENERAL,
        ("Got unexpected packet # %d\n", ipcGetSeqNum(packet)));

    /* If the last packet in the out of order list has a smaller seqNum,
     * then we can just append this packet to the end of the list */
    if ( (p == NULL) || SEQ_LT(ipcGetSeqNum(p), ipcGetSeqNum(packet)) )
    {
        ipcListAddTail(&conn->outOfOrderPackets, (ipcListLink_t)packet);
    }
    else
    {
        /* else, we have to scan the list to find a place for the packet */
        ipcListLink_t curr, prev;

        for (ipcListFirstPair(conn->outOfOrderPackets, prev, curr);
             curr != NULL;
             ipcListNextPair(prev, curr))
         {
             p = (ipcPacket_t*)curr;

             if (SEQ_GT(ipcGetSeqNum(p), ipcGetSeqNum(packet)))
             {
                 ipcListInsertLink(&conn->outOfOrderPackets,
                     (ipcListLink_t)packet,
                     prev);
             }
             else if (SEQ_EQ(ipcGetSeqNum(p), ipcGetSeqNum(packet)))
             {
                 ipcError(IPCLOG_GENERAL, ("Got duplicated packet # %d\n",
                     ipcGetSeqNum(packet)));
                 ipcFree(packet);
             } /* else */
         } /* for */
    } /* else */
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSendAck
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send out an acknowledgment packet.
*   
*   PARAMETER(S):
*       conn: pointer to the connection struct
*       destAddr: destination address where the ack goes to
*       destSrvId: destination service id
*       seqNum: the sequence number of the packet being acked
*       maxTxSeqNum: the max sequence number our peer can send
*       
*   RETURN: IPC_OK if no error occurs, otherwise a negative value
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 03Nov2005  Motorola Inc.         Free packet when error occurs 
*
*****************************************************************************************/
int ipcSendAck(ipcConn_t* conn,
               ipcAddress_t destAddr,
               ipcServiceId_t destSrvId,
               UINT8 seqNum,
               UINT8 maxTxSeqNum)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t* packet = NULL;
    COCOAckMsg_t* ack = NULL;
    int rc = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    packet = ipcAlloc(IPC_PACKET_DATA_OFFSET + COCO_ACK_DATA_SIZE,
        IPC_PHYSSHM);
    if (packet == NULL)
    {
        return IPCENOBUFS;
    }

    ipcDebugLog(IPCLOG_GENERAL,
            ("Sending ack # %d max %d\n", seqNum, maxTxSeqNum));

    ack = (COCOAckMsg_t*)packet;

    packet->next = NULL;
    ipcSetSrcAddr(ack, ipcStack_glb.ipcAddr);
    ipcSetDestAddr(ack, destAddr);
    ipcSetSrcServiceId(ack, 0);
    ipcSetDestServiceId(ack, destSrvId);

    ipcSetMsgFlags(ack, IPC_PACKET_ACK);
    ipcSetMsgLength(ack, COCO_ACK_DATA_SIZE);

    ipcOsalWrite8(ack->seqNum, seqNum);
    ipcOsalWrite8(ack->maxSeqNum, maxTxSeqNum);
    ipcOsalWrite8(ack->wasReliable, TRUE);

    /* the port Id can be fetched from private data */
    rc =  conn->ops->deliverData(conn, packet);
    if (rc < 0)
    {
        ipcFree(packet);
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSendFlow
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send out an flow packet telling our peer the maxTxSeqNum.
*   
*   PARAMETER(S):
*       conn: pointer to the connection struct
*       destAddr: destination address where the ack goes to
*       destSrvId: destination service id
*       maxTxSeqNum: the max sequence number our peer can send
*       
*   RETURN: IPC_OK if no error occurs, otherwise a negative value
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 03Nov2005  Motorola Inc.         Free packet when error occurs 
*
*****************************************************************************************/
int ipcSendFlow(ipcConn_t* conn,
                ipcAddress_t destAddr,
                ipcServiceId_t destSrvId,
                UINT8 maxTxSeqNum)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t* packet = NULL;
    COCOAckMsg_t* ack = NULL;
    int rc = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    packet = ipcAlloc(IPC_PACKET_DATA_OFFSET + COCO_ACK_DATA_SIZE,
        IPC_PHYSSHM);
    if (packet == NULL)
    {
        return IPCENOBUFS;
    }
    ack = (COCOAckMsg_t*)packet;

    ipcDebugLog(IPCLOG_GENERAL,
            ("Sending flow, maxTx=%d\n", maxTxSeqNum));

    packet->next = NULL;
    ipcSetSrcAddr(ack, ipcStack_glb.ipcAddr);
    ipcSetDestAddr(ack, destAddr);
    ipcSetSrcServiceId(ack, 0);
    ipcSetDestServiceId(ack, destSrvId);

    ipcSetMsgFlags(ack, IPC_PACKET_ACK);
    ipcSetMsgLength(ack, COCO_ACK_DATA_SIZE);

    ipcOsalWrite8(ack->seqNum, 0);
    ipcOsalWrite8(ack->maxSeqNum, maxTxSeqNum);
    ipcOsalWrite8(ack->wasReliable, FALSE); /* DISTINGUISHED WITH ACK MSG */

    /* the port Id can be fetched from private data */
    rc = conn->ops->deliverData(conn, packet);
    if (rc < 0)
    {
        ipcFree(packet);
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnGotPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to process a packet which comes into a connection.
*   
*   PARAMETER(S):
*       conn: the connection where the packet arrives at
*       packet: the incoming packet
*       
*   RETURN: IPC_OK if no error occurs, otherwise a negative value
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 03Nov2005  Motorola Inc.         Bugfix: update window on arrival of unreliable packet
* 16Jan2006  Motorola Inc.         use macro ipcGetMsgFlags to read the packet flags
* 19Jan2007  Motorola Inc.         Removed locking of connMutex 
* 22Mar2007  Motorola Inc.         Bugfix: update window before handling unreliable packet
*                                  to avoid wrong window size
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcConnGotPacket(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL needAck = FALSE;
    int rc = IPC_OK;
    UINT8 seqNum = 0;
    UINT8 maxRxSeqNum = 0;
    ipcAddress_t fromAddr;
    ipcServiceId_t fromSrv;
            
/*--------------------------------------- CODE -----------------------------------------*/
    if ((conn == NULL) || (packet == NULL))
    {
        return IPCEFAULT;
    }
    needAck = (ipcGetMsgFlags(packet) & IPC_PACKET_RELIABLE) ? TRUE: FALSE;
    fromAddr = ipcGetSrcAddr(packet);
    fromSrv = ipcGetSrcServiceId(packet);
    seqNum = ipcGetSeqNum(packet);

    /* if the packet doesn't need ack */
    if (!needAck)
    {
        if (conn->rxWindowSize == 0)
        {
            ipcWarning(IPCLOG_GENERAL,
                ("Got packet # %d when rxWindow is full\n", seqNum));

            /* Do we have to free the packet? */
            ipcSendAck(conn, fromAddr, fromSrv, seqNum, maxRxSeqNum);
            return IPCEINVAL;
        }
        ipcDebugLog(IPCLOG_GENERAL,
            ("Queueing packet # %d\n", ipcGetSeqNum(packet)));

        /* recalculate the window size, then add packet to queue */
        if (LOCK_CONN(conn) < 0)
        {
            ipcError(IPCLOG_GENERAL,
                    ("ipcConnGotPacket: Failed to lock connection\n"));
            return IPCEBUSY;
        }
        conn->nextRxSeqNum = seqNum + 1;
        conn->rxWindowSize--;
        maxRxSeqNum = seqNum + conn->rxWindowSize;
        UNLOCK_CONN(conn);

        rc = conn->ops->onDataArrival(conn, packet);
        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_GENERAL, ("onDataArrival failed?\n"));
            return rc;
        }
    }
    else /* if the packet needs ack */
    {
        if (LOCK_CONN(conn) < 0)
        {
            ipcError(IPCLOG_GENERAL,
                    ("ipcConnGotPacket: Failed to lock connection\n"));
            return IPCEBUSY;
        }
        if (ipcGetMsgFlags(packet) & IPC_PACKET_FIRST_ACKED)
        {
            conn->nextRxSeqNum = seqNum;
            /* don't change rxWindowSize */
        }

        maxRxSeqNum = conn->nextRxSeqNum + conn->rxWindowSize - 1;

        /* the packet is within the window */
        if ((SEQ_LT(seqNum, conn->nextRxSeqNum)) || 
            (SEQ_GT(seqNum, maxRxSeqNum)))
        {
            UNLOCK_CONN(conn);
            ipcSendAck(conn, fromAddr, fromSrv, seqNum, maxRxSeqNum);
            return IPCEINVAL;
        }

        /* if the packet is expected */
        if (ipcGetSeqNum(packet) == conn->nextRxSeqNum)
        {
            conn->nextRxSeqNum++;
            conn->rxWindowSize--;

            UNLOCK_CONN(conn);
            rc = conn->ops->onDataArrival(conn, packet);
            if (LOCK_CONN(conn) < 0)
            {
                ipcError(IPCLOG_GENERAL,
                        ("ipcConnGotPacket: Failed to lock connection\n"));
                return IPCEBUSY;
            }

            while (NULL != (packet = (ipcPacket_t*)conn->outOfOrderPackets.head))
            {
                if (ipcGetSeqNum(packet) == conn->nextRxSeqNum)
                {
                    packet = (ipcPacket_t*)ipcListRemoveHead(&conn->outOfOrderPackets);
                    conn->nextRxSeqNum++;
                    conn->rxWindowSize--;

                    UNLOCK_CONN(conn);
                    if (conn->ops->onDataArrival(conn, packet) < 0)
                    {
                        ipcError(IPCLOG_GENERAL, ("Failed to call onDataArrival\n"));
                        ipcFree(packet);
                    }
                    if (LOCK_CONN(conn) < 0)
                    {
                        ipcError(IPCLOG_GENERAL,
                                ("ipcConnGotPacket: Failed to lock connection\n"));
                        return IPCEBUSY;
                    }
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            /* otherwise, add it to the outofOrderList */
            ipcConnAddOutofOrderPacket(conn, packet);
        }
        maxRxSeqNum = conn->nextRxSeqNum + conn->rxWindowSize - 1;
        
        UNLOCK_CONN(conn);
    }
    ipcSendAck(conn, fromAddr, fromSrv, seqNum, maxRxSeqNum);
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnGotAck
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 17Oct2005  Motorola Inc.         Do not return prematurely
* 24Apr2007  Motorola Inc.         Bugfix for TxWindowAvailable
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 
*****************************************************************************************/
int ipcConnGotAck(ipcConn_t* conn,
                  BOOL wasReliable,
                  UINT8 seqNum,
                  UINT8 maxTxSeqNum)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev = NULL;
    ipcListLink_t curr = NULL;
    ipcPacket_t* packet = NULL;
    UINT8 originalMax = 0;
    UINT8 nextTxSeqNum = 0;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/

    if (LOCK_CONN(conn) < 0)
    {
        ipcError(IPCLOG_GENERAL,
                ("ipcConnGotAck: Failed to lock connection\n"));
        return IPCEBUSY;
    }
    if (wasReliable)
    {
        ipcDebugLog(IPCLOG_GENERAL,
            ("Got ack # %d max %d\n", seqNum, maxTxSeqNum));

        for (ipcListFirstPair(conn->sentPackets, prev, curr);
            curr != NULL;
            ipcListNextPair(prev, curr))
        {
            packet = IPC_NEXT2_TO_PACKET(curr);
            if (ipcGetSeqNum(packet) == seqNum)
            {
                ipcDebugLog(IPCLOG_GENERAL,
                    ("The ack removes packet # %d\n", seqNum));
                ipcListRemoveNextLink(&conn->sentPackets, prev);
                
                ipcFree(packet);

                /* if the list sentPackets gets empty */
                if (conn->sentPackets.size == 0)
                {
                    rc = 1; 
                    /* if rc is not IPC_OK, then we'll have to call
                     * onSentListEmpty */
                }
                break;
            }
            else if (SEQ_GT(ipcGetSeqNum(packet), seqNum))
            {
                /* If the seqNum of the new packet is greater than that of the
                 * ack, then the expected packet is not in the list, because
                 * the sender always sends packets in order. */
                break;
            }
        }
    }
    else
    {
        ipcDebugLog(IPCLOG_GENERAL,
            ("Flow msg maxTx=%d, my maxTx=%d\n", maxTxSeqNum, conn->maxTxSeqNum));
    }

    originalMax = conn->maxTxSeqNum;
    nextTxSeqNum = conn->nextTxSeqNum;

    /* For ack messages, update maxTxSeqNum only when the window is advanced,
     * but for flow messages, we'll always update the maxTxSeqNum.
     * We do this because users will send flow messages if they try to shrink
     * the window using the function ipcsetsockqlen, in this case, the new
     * maxTxSeqNum will get smaller */
    if (SEQ_LEQ(originalMax, maxTxSeqNum) || !wasReliable)
    {
        conn->maxTxSeqNum = maxTxSeqNum;
    }
    else
    {
        ipcWarning(IPCLOG_GENERAL,
            ("The incoming seqnum is less, why?\n"));
    }
    UNLOCK_CONN(conn);

    if (rc != IPC_OK)
    {
        /* any callbacks should be called without protection of locks */
        rc = conn->ops->onSentListEmpty(conn);
    }
    if ((nextTxSeqNum == (UINT8)(originalMax + 1)) && 
        (SEQ_LT(originalMax, maxTxSeqNum)))
    {
        conn->ops->onTxWindowAvailable(conn);
    }

    ipcDebugLog(IPCLOG_GENERAL,
        ("After got ack, nextTx=%d, maxTx=%d\n",
         conn->nextTxSeqNum, conn->maxTxSeqNum));

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcConnConsumeData
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 06Sep2006  Motorola Inc.         Fixed potential dead lock on same node 
*
*****************************************************************************************/
void ipcConnConsumeData(ipcConn_t* conn, 
                        ipcAddress_t destAddr, 
                        ipcServiceId_t destSrv)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT8 rxWindowSize = 0;
    UINT8 nextRxSeqNum = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (LOCK_CONN(conn) < 0)
    {
        ipcError(IPCLOG_GENERAL,
                ("ipcConnConsumeData: Failed to lock connection\n"));
        return;
    }
    conn->rxWindowSize++;
    rxWindowSize = conn->rxWindowSize;
    nextRxSeqNum = conn->nextRxSeqNum;
    ipcDebugLog(IPCLOG_GENERAL,
        ("Consuming data, nextRx=%d, rxWindowSize=%d\n",
            conn->nextRxSeqNum, conn->rxWindowSize));
    UNLOCK_CONN(conn);

    if (rxWindowSize == 1)
    {
        ipcSendFlow(conn, destAddr, destSrv, 
                (UINT8)(nextRxSeqNum + rxWindowSize - 1));
    }
}
