/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_osal_mmu.c
*   FUNCTION NAME(S): ipcAllocUnshared
*                     ipcFreeUnshared
*                     ipcInitMemory
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 25May2004  Motorola Inc.         Initial Version
* 31Aug2004  Motorola Inc.         Add GetIpcMemVersion
* 07Sep2004  Motorola Inc.         Add shared memory facilities
* 24Apr2005  Motorola Inc.         Ported to linux
* 16Jun2005  Motorola Inc.         Physical Shm & Virtural Shm Integration
* 24Aug2005  Motorola Inc.         ipczalloc/ipczfree last error 
* 09Dec2005  Motorola Inc.         Modify the comments and function header description.
* 01Mar2006  Motorola Inc.         print error for out of memory case
* 10Apr2006  Motorola Inc.         Support runtime log level config for mm
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 20Feb2007  Motorola Inc.         Update for common allocator
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_osal.h"
#include "ipclog.h"
#include "ipc_shared_memory.h"
#include "dynamic_pool.h"
#include "ipc_defs.h"
#ifdef HAVE_PHYSICAL_SHM
#include "ipc_mu_shm.h"
#endif

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

char *physical_shm_base = NULL;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
void *ipcAllocUnshared(unsigned int size, unsigned int flags);
int ipcFreeUnshared(void* memBlock);

/*------------------------ Implementation of internal functions --------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcInitMemory
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function intializes the AP memory allocator table and opens the
*   memory allocator mutex.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Sep2004  Motorola Inc.         Initial Creation
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void ipcInitMemory()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    /* Initialize allocator table */
    ipcAddAllocator(IPC_DEFAULT_MEM_TYPE, ipcAllocUnshared, ipcFreeUnshared);
    ipcAddAllocator(IPC_PROCSHM, ipcShmAlloc, ipcShmFree);

#ifdef HAVE_PHYSICAL_SHM

    /* Initialize physical shared memory allocator functions */
    ipcAddAllocator(IPC_PHYSSHM, ipcGetSMBuffer, ipcFreeSMBuffer);
#endif /* HAVE_PHYSICAL_SHM */
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAllocUnshared
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocates memory used internally by the SIPC stack
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Feb2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void *ipcAllocUnshared(unsigned int size, unsigned int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return kmalloc(size, GFP_KERNEL);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFreeUnshared
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Frees memory used internally by the SIPC stack
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Feb2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcFreeUnshared(void* memBlock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    kfree(memBlock);
    return IPC_OK;
}
