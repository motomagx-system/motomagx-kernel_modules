/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcmem_dev.c
*   FUNCTION NAME(S): ipcRemapVirtualSharedMemory
*                     ipcSMUserToVirtual
*                     ipcSMVirtualToUser
*                     ipcShmPtrUserValidate
*                     ipcmem_ioctl
*                     ipcmem_mmap
*                     ipcmem_open
*                     ipcmem_read
*                     ipcmem_release
*                     ipcmem_vm_nopage
*                     ipcmem_write
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 21Jun2005  Motorola Inc.         Initial Creation
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source         
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 24Jul2007  Motorola Inc.         Changed error type to IPCLOG_MEMTRACK
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <linux/types.h>  /* size_t */
#include <linux/mm.h>
#include <linux/fs.h>
#include <asm/mman.h>
#include <asm/uaccess.h>

#include "ipc_ioctl.h"
#include "ipc_sock_impl.h"
#include "ipc_defs.h"
#include "dynamic_pool.h"
#include "ipc_shared_memory.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
extern ulong paddr;
extern ulong psize;
extern ulong vaddr;
extern ulong vsize;

extern void* physical_shm_base;

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_vm_nopage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
static struct page* ipcmem_vm_nopage(struct vm_area_struct* area,
        unsigned long address,
        int* type);

/*-------------------------------- STATIC DATA -------------------------------*/
static struct vm_operations_struct ipcmem_vm_ops = {
    .nopage = ipcmem_vm_nopage
};
/*--------------------------------------- MACROS ---------------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemapVirtualSharedMemory
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Jul2007  Motorola Inc.         Changed error type to IPCLOG_MEMTRACK
*
*****************************************************************************************/
static void ipcRemapVirtualSharedMemory(void)
{
    struct vm_area_struct *vma = NULL;

    down_write(&current->mm->mmap_sem);
    vma = find_vma(current->mm, vaddr);
    if ((vma == NULL) || (vma->vm_file == NULL))
    {
        up_write(&current->mm->mmap_sem);
        ipcError(IPCLOG_MEMTRACK, ("Memory corruption detected.\n"));
        return; 
    }
    if ((do_munmap(current->mm, vaddr, vsize) != 0)
            ||(IS_ERR((void*)do_mmap(vma->vm_file, vaddr, vsize, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_FIXED, 0))))
    {
        up_write(&current->mm->mmap_sem);
        ipcError(IPCLOG_MEMTRACK, ("Memory corruption detected.\n"));
        return;
    }
    up_write(&current->mm->mmap_sem);        
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmPtrUserValidate
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Validate shared memory pointer parsed from or to user space. If needed, remap 
*   virtual shared memory region.
*   
*   PARAMETER(S):
*       uptr: [in] address of pointer in user space
*       
*   RETURN: if an error occurs, return FALSE,otherwise return TRUE
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation
* 22Aug2006  Motorola Inc.         Optimized 
* 24Jul2007  Motorola Inc.         Changed error type to IPCLOG_MEMTRACK
*
*****************************************************************************************/
BOOL ipcShmPtrUserValidate(unsigned long uptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc;
    struct dpcc_memblk memblk;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Only verify virtual shared memory dpcc_memblk header's eyecatch */
    if ((uptr > vaddr) && (uptr < (vaddr + vsize)))
    {
        rc = copy_from_user(&memblk, 
                            (void*)((unsigned long)ipcSMDataToHeader(ipcDataToPacket(uptr)) - sizeof(struct dpcc_memblk)), 
                            sizeof(struct dpcc_memblk));
        if (rc != 0)
        {
            ipcError(IPCLOG_MEMTRACK,("copy_from_user failed:%d\n", rc));
            return FALSE;
        }

        if (memblk.eyecatch != DPCC_SHM_EYE_CATCH)
        {
            ipcError(IPCLOG_MEMTRACK,("eyecatch is not match!\n"));
            return FALSE;
        }
    }
    return TRUE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMVirtualToUser
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Parse a virtual shared memory address in kernel space to user space
*   
*   PARAMETER(S):
*       vptr: [in] virtual shared memory address in kernel space
*       
*   RETURN: virtual shared memory address in user space
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation
* 30Dec2006  Motorola Inc.         Remap for all memory blocks with address higher than 256K in virtual shared memory
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 24Jul2007  Motorola Inc.         Changed error type to IPCLOG_MEMTRACK
*
*****************************************************************************************/
unsigned long ipcSMVirtualToUser(unsigned long vptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int memtype;
    unsigned long uptr = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    memtype = ipcSMGetMemType(ipcSMDataToHeader(ipcDataToPacket(vptr)));
    if (memtype == IPC_PHYSSHM)
    {
        uptr = paddr + (vptr - (unsigned long)physical_shm_base);
    }
    else if (memtype == IPC_PROCSHM)
    {
        uptr = vaddr + ipcShmPtrToOffset((char *)vptr);
        /* FIXME: for all the memory blocks in pools other than the first pool,
         * remap all IPC virtual shared memory region */
        if (uptr - vaddr > MIN_POOL_SIZE)
        {
            ipcRemapVirtualSharedMemory();
        }
    }
    else
    {
        ipcError(IPCLOG_MEMTRACK, ("Memory corruption detected.\n"));
    }
    
    if (FALSE == ipcShmPtrUserValidate(uptr))
    {
        uptr = 0;
    }
    
    return uptr;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSMUserToVirtual
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Parse a virtual shared memory address in user space to kernel space
*   
*   PARAMETER(S):
*       vptr: [in] virtual shared memory address in user space
*       
*   RETURN: virtual shared memory address in kernel space
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation 
* 24Jul2007  Motorola Inc.         Changed error type to IPCLOG_MEMTRACK
*
*****************************************************************************************/
unsigned long ipcSMUserToVirtual(unsigned long uptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long vptr = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (TRUE != ipcShmPtrUserValidate(uptr))
    {
        return vptr;
    }

    /* get memory type by offset range and calculate the virtual address */
    if ((uptr > paddr) && (uptr < (paddr + psize)))
    {
        vptr = (unsigned long)physical_shm_base + (uptr - paddr);
    }
    else if ((uptr > vaddr) && (uptr < (vaddr + vsize)))
    {
        vptr = (unsigned long)ipcShmOffsetToPtr(uptr - vaddr);
    }
    else
    {
        ipcError(IPCLOG_MEMTRACK,("Unknown memory type by address pointer \n"));
    }
    
    return vptr; 
}



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_open
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Open a memory device. This is called by user-space applications.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int ipcmem_open(struct inode *inode, struct file *filp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    filp->private_data = NULL;
    get_ipc_module();
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_read
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from the memory device.
*   This doesn't actually read data, which is implemented using ioctl.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static ssize_t ipcmem_read(struct file *filp, char *buf, size_t count, loff_t *f_ops)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_write
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Write data to the memory device.
*   This doesn't actually write data, see ioctl.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static ssize_t ipcmem_write(struct file *filp, const char *buf, size_t count, loff_t *f_ops)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_release
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Release memory device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int ipcmem_release(struct inode *inode, struct file *filp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    put_ipc_module();
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_ioctl
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   ioctl for the memory device.
*   Most ipc operations are implemented using ioctl.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Jun2006  Motorola Inc.         Initial Creation
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source        
* 24Jul2007  Motorola Inc.         Changed error type to IPCLOG_MEMTRACK
*
*****************************************************************************************/
static int ipcmem_ioctl(struct inode *inode, struct file *filp, unsigned int cmd,
        unsigned long a)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
    ipcCmd_t arg;
    unsigned long bytesleft;
    unsigned long memtype;
    struct IPCSOCK_ADDR addr;
/*--------------------------------------- CODE -----------------------------------------*/
    bytesleft = copy_from_user(&arg, (void*)a, sizeof(ipcCmd_t));
    if (bytesleft != 0)
    {
        ipcError(IPCLOG_MEMTRACK, ("copy_from_user failed\n"));
        return -1;
    }

    switch(cmd)
    {
        case CMD_IPCSHMALLOC:
            arg.u.shmalloc.zbuffer = ipczalloc(arg.u.shmalloc.size, arg.u.shmalloc.flags, 0);
            if (arg.u.shmalloc.zbuffer == NULL)
            {
                arg.lasterror = IPCENOSHMEM;
                rc = IPC_ERROR;
            }
            else
            {
                arg.u.shmalloc.zbuffer = (char *)ipcSMVirtualToUser((unsigned long)arg.u.shmalloc.zbuffer);
                if (arg.u.shmalloc.zbuffer == NULL)
                {
                    ipcError(IPCLOG_MEMTRACK, ("Memory corruption detected.\n"));
                    rc = IPC_ERROR;
                }
            }
            break;
        case CMD_IPCSHMFREE:
            arg.u.shmfree.zbuffer = (char *)ipcSMUserToVirtual((unsigned long)arg.u.shmfree.zbuffer);
            if (NULL == arg.u.shmfree.zbuffer)
            {
                ipcError(IPCLOG_MEMTRACK, ("Memory corruption detected.\n"));
                rc = IPC_ERROR;
            }
            else
            {
                rc = ipczfree(arg.u.shmfree.zbuffer);
            }
            
            if (rc == IPC_ERROR)
            {
                arg.lasterror = IPCEBADBUF;
            }
            break;
        case CMD_IPCGETSHMINFO: 
            arg.u.getshminfo.phy_shm_start_addr = (char *)paddr;
            arg.u.getshminfo.phy_shm_size = psize;
            /* for viewed by APPs in user space, we minus the max block size with IPC_PACKET_DATA_OFFSET */
#ifdef HAVE_PHYSICAL_SHM
            arg.u.getshminfo.max_phy_shm_blk_size = ipcGetMaxPSMBufferSize() - IPC_PACKET_DATA_OFFSET;
#else
            arg.u.getshminfo.max_phy_shm_blk_size = 0;
#endif
            arg.u.getshminfo.virt_shm_start_addr = (char *)vaddr;
            arg.u.getshminfo.virt_shm_size = vsize - IPC_PACKET_DATA_OFFSET;
            break;
        case CMD_IPCGETMEMTYPE:
            memset(&addr, 0, sizeof(addr));
            addr.ipc_node = arg.u.getmemtype.dest_node;

            rc = ipcgetmemtype_imp(&addr, &memtype, &arg.lasterror);
            (void)copy_to_user(arg.u.getmemtype.memtype, &memtype, sizeof(memtype));
	    break;
        case CMD_IPCZCALLOC:
            arg.u.zcalloc.zbuffer = ipczcalloc(arg.u.zcalloc.nelem, arg.u.zcalloc.size, arg.u.zcalloc.flags, 0);
            if (arg.u.zcalloc.zbuffer == NULL)
            {
                arg.lasterror = IPCENOSHMEM;
                rc = IPC_ERROR;
            }
            else
            {
                arg.u.zcalloc.zbuffer = (char *)ipcSMVirtualToUser((unsigned long)arg.u.zcalloc.zbuffer);
                if (arg.u.zcalloc.zbuffer == NULL)
                {
                    ipcError(IPCLOG_MEMTRACK, ("Memory corruption detected.\n"));
                    rc = IPC_ERROR;
                }
            }
            break;
        default:
            break;
    }

    /* copy arg to user space */
    copy_to_user((void*)a, &arg, sizeof(arg));

    /* If the call is broken by a signal, return -ERESTARTSYS */
    if ((rc < 0) && (arg.lasterror == IPCEINTR))
    {
        rc = -ERESTARTSYS;
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_vm_nopage
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Function to handle page fault of IPC.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Jan2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static struct page* ipcmem_vm_nopage(struct vm_area_struct* area,
        unsigned long address,
        int* type)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    unsigned long offset;
    struct page* page = NOPAGE_SIGBUS;
    char* ptr = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    offset = address - area->vm_start;
    /* for noncontinuous logical shared memory, ipc_vm_nopage is used */
    /* for continuous physical shared memory, remap_pfn_range is used */    
    if (offset <= vsize)
    {
        ptr = ipcShmOffsetToPtr(offset);
        if (ptr != NULL)
        {
            page = SHM_ADDR_TO_PAGE(ptr);

            if (NOPAGE_SIGBUS != page)
            {
                get_page(page);
                if (NULL != type)
                {
                    *type = VM_FAULT_MINOR;
                }
            }
            else
            {
                ipcError(IPCLOG_GENERAL, ("ipc_vm_nopage: PAGE == NULL\n"));
            }
        }
    }

    return page;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcmem_mmap
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Map a memory block of ipc memory device into the address space of a process.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Apr2005  Motorola Inc.         Initial Creation
* 24Jan2006  Motorola Inc.         Big memory pool support 
*
*****************************************************************************************/
static int ipcmem_mmap(struct file *file, struct vm_area_struct *vma) 
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    vma->vm_flags |= VM_SHARED | VM_RESERVED ;

#ifdef HAVE_PHYSICAL_SHM
    /* Map physical shared memory */
    if ((vma->vm_start >= paddr) && (vma->vm_end <= paddr + psize))
    {

        vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
        vma->vm_pgoff = ( paddr >> PAGE_SHIFT);
        if (remap_pfn_range(vma,
                    vma->vm_start,
                    vma->vm_pgoff,
                    vma->vm_end - vma->vm_start,
                    vma->vm_page_prot))       
        {            
            return -EAGAIN;
        }      
    }
    else
#endif
    {
        /* can not use remap_pfn_range here since logical memory can be noncontinuous */
        vma->vm_ops = &ipcmem_vm_ops;
    }
    return 0;
}


struct file_operations ipcmem_dev_fops = {
read:ipcmem_read,
     write:ipcmem_write,
     ioctl:ipcmem_ioctl,
     open:ipcmem_open,
     release:ipcmem_release,
     mmap: ipcmem_mmap,
};


