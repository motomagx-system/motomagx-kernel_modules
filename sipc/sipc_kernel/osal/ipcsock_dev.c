/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcsock_dev.c
*   FUNCTION NAME(S): ipcrecv_imp
*                     ipcsend_imp
*                     register_sipc_protocol
*                     sipc_accept
*                     sipc_alloc_sock
*                     sipc_bind
*                     sipc_connect
*                     sipc_create
*                     sipc_getname
*                     sipc_getsockopt
*                     sipc_ioctl
*                     sipc_listen
*                     sipc_poll
*                     sipc_recvmsg
*                     sipc_release
*                     sipc_sendmsg
*                     sipc_setsockopt
*                     sipc_shutdown
*                     sock_set_sipc_sock
*                     sock_to_sipc_sock
*                     socket_to_sipc_sock
*                     translate_errorcode
*                     unregister_sipc_protocol
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 27Apr2005  Motorola Inc.         Initial Creation
* 22Jan2007  Motorola Inc.         Added to linux protocol family
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 22Jun2007  Motorola Inc.         Align size to even value when copying
* 05Jul2007  Motorola Inc.         Return ERESTARTSYS on IPCEINTR in accept
* 19Jul2007  Motorola Inc.         Added ipcTrace
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <linux/types.h>  /* size_t */
#include <asm/uaccess.h>
#include <linux/poll.h>

#include "ipc_session.h"
#include "ipc_ioctl.h"
#include "ipc_sock_impl.h"
#include "ipc_shared_memory.h"
#include "linux/net.h"
#include "linux/netdevice.h"
#include <net/sock.h>
#include "linux/poll.h"
#include "ipc_ioctl.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
static int             sipc_create    (struct socket* sock, int protocol);
static int             sipc_release   (struct socket *sock);
static int             sipc_bind      (struct socket *sock, 
                                       struct sockaddr *myaddr, 
                                       int sockaddr_len);
static int             sipc_connect   (struct socket *sock, 
                                       struct sockaddr *vaddr, 
                                       int sockaddr_len, 
                                       int flags);
static int             sipc_accept    (struct socket *sock, 
                                       struct socket *newsock, 
                                       int flags);
static int             sipc_getname   (struct socket *sock, 
                                       struct sockaddr *addr, 
                                       int *sockaddr_len, 
                                       int peer);
static unsigned int    sipc_poll      (struct file *file, 
                                       struct socket *sock, 
                                       struct poll_table_struct *wait);
static int             sipc_ioctl     (struct socket *sock, 
                                       unsigned int cmd, 
                                       unsigned long arg);
static int             sipc_listen    (struct socket *sock, 
                                       int len);
static int             sipc_shutdown  (struct socket *sock, 
                                       int flags);
static int             sipc_setsockopt(struct socket *sock, 
                                       int level, 
                                       int optname, 
                                       char __user *optval, 
                                       int optlen);
static int             sipc_getsockopt(struct socket *sock, 
                                       int level, 
                                       int optname, 
                                       char __user *optval, 
                                       int __user *optlen);
static int             sipc_sendmsg   (struct kiocb *iocb, 
                                       struct socket *sock, 
                                       struct msghdr *m, 
                                       size_t total_len);
static int             sipc_recvmsg   (struct kiocb *iocb, 
                                       struct socket *sock, 
                                       struct msghdr *m, 
                                       size_t total_len, 
                                       int flags);
static int translate_errorcode(int ec);

/*--------------------------------------- MACROS ---------------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcrecv_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function receives ipc data.
*   
*   PARAMETER(S):
*       sock: Descriptor identifying a bound socket
*       buffer: pointer to memory that is to be sent
*       flags: indicator specifying the way in which the call is made
*       from: pointer to ipc address structure, if NULL, handle as connected
*       fromlen: length of from
*       lasterror: pointer to error code (out param)
*       
*   RETURN: If no error occurs, this function returns the number of bytes 
*           received. If the connection has been gracefully closed, the return
*           value is 0. If an error occurs, a value of IPC_ERROR is returned,
*           and a specific error code can be retrieved by calling ipcgetlasterror.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.         Initial Creation
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source        
* 22Jun2007  Motorola Inc.         Align size to even value when copying
*
*****************************************************************************************/
static int ipcrecv_imp(ipcSocket_t *sock,
                 char * buffer,
                 int len,
                 int flags,
                 struct IPCSOCK_ADDR *from,
                 int *fromlen,
                 int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
    char *zbuffer = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    /*  TBD: is it reasonable to drop packet 
        if user's buffer is not big enough? */
    if (NULL == from)
    {
        rc = ipczrecv_imp(sock, &zbuffer, flags, lasterror);
    }
    else
    {
        rc = ipczrecvfrom_imp(sock, &zbuffer, flags, from, fromlen, lasterror);
    }
    
    if (rc < 0)
    {   
        return IPC_ERROR;
    }
    
    if (rc > len)
    {
        *lasterror = IPCEMSGSIZE;
        rc = IPC_ERROR;
    }
    else
    {
        if ((rc & 1) && (rc < len))
        {
            len = rc + 1;
        }
        else
        {
            len = rc;
        }
        if (0 != copy_to_user(buffer, zbuffer, len))
        {
            *lasterror = IPCEFAULT;
            rc = IPC_ERROR;
        }
    }

    if ((zbuffer != NULL) && !(flags & IPCROSCHKMSG))
    {
        ipczfree(zbuffer);
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcsend_imp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function sends data to a specific IPC destination on a connected socket.
*   Both CL and CO sockets may be used as long as ipcconnect is called first.
*   
*   PARAMETER(S):
*       socket: socket handle 
*       buffer: pointer to memory that is to be sent
*       len: the length of data packet
*       flags: indicator specifying the way in which the call is made
*       to: indicator destination, if NULL, handle as connected
*       tolen: length of to
*       lasterror: [out] pointer to error code
*       
*   RETURN: If no error occurs, returns the total number of bytes sent, if an
*           error occurs, IPC_ERROR is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Jun2006  Motorola Inc.         Initial Creation
* 17Jul2006  Motorola Inc.         Refactored ipc folder structure  for open source
* 08Dec2006  Motorola Inc.         Allocate PROCSHM when failed to allocate PHYSSHM
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 22Jun2007  Motorola Inc.         Align size to even value when copying
*
*****************************************************************************************/
static int ipcsend_imp(ipcSocket_t *sock,
    const char *buffer,
    int len,
    int flags,
    const struct IPCSOCK_ADDR *to,
    int tolen,
    int *lasterror)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
    int buflen;
    char *zbuffer = NULL;
    char *zdata = NULL;
    unsigned long memtype = IPC_PROCSHM;
/*--------------------------------------- CODE -----------------------------------------*/
#ifdef HAVE_PHYSICAL_SHM
    memtype = IPC_PHYSSHM;
#endif
    /* Allocate enough memory for the data AND the packet header */
    buflen = len + IPC_PACKET_DATA_OFFSET;
    zbuffer = ipcAlloc(buflen, memtype);
    if (NULL == zbuffer)
    {
    #ifndef HAVE_PHYSICAL_SHM
        *lasterror = IPCENOBUFS;
        return IPC_ERROR;
    #else
        zbuffer = ipcAlloc(buflen, IPC_PROCSHM);
        if(NULL == zbuffer)
        {
            *lasterror = IPCENOBUFS;
            return IPC_ERROR;
        }
    #endif    
    }
    zdata = ipcGetMsgData(zbuffer);
    rc = copy_from_user(zdata, buffer, (len + 1) & ~1);

    if (rc != 0)
    {
        ipcError(IPCLOG_GENERAL, ("copy_from_user failed\n"));
        ipcFree(zbuffer);
        *lasterror = IPCEFAULT;
        return IPC_ERROR;
    }
    
    if (NULL != to)
    {
        rc = ipczsendto_imp(sock, zdata, len, flags, to, tolen, lasterror);
    }
    else
    {
        rc = ipczsend_imp(sock, zdata, len, flags, lasterror);
    }

    if (IPC_ERROR == rc)
    {
        ipcFree(zbuffer);
    }
    return rc;
}


/******************************************************************************
 * Function implementations for AF_IPC
 *****************************************************************************/

/* internal data structs */
static struct net_proto_family sipc_family_ops = {
    .family     =       AF_IPC,
    .create     =       sipc_create,
    .owner      =       THIS_MODULE
};

static struct proto_ops sipc_proto_ops = {
    .family     =       AF_IPC,
    .owner      =       THIS_MODULE,
    .release    =       sipc_release,
    .bind       =       sipc_bind,
    .connect    =       sipc_connect,
    .socketpair =       sock_no_socketpair,
    .accept     =       sipc_accept,
    .getname    =       sipc_getname,
    .poll       =       sipc_poll,
    .ioctl      =       sipc_ioctl,
    .listen     =       sipc_listen,
    .shutdown   =       sipc_shutdown,
    .setsockopt =       sipc_setsockopt,
    .getsockopt =       sipc_getsockopt,
    .sendmsg    =       sipc_sendmsg,
    .recvmsg    =       sipc_recvmsg,
    .mmap       =       sock_no_mmap,
    .sendpage   =       sock_no_sendpage
};

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: socket_to_sipc_sock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function converts the "struct socket*" variable which is provided by
*   kernel to a "struct ipcSocket_t*" struct.
*   
*   NOTE: This function is tested in linux 2.6.9, not sure if it works in other
*   kernel versions.
*   
*   PARAMETER(S):
*       sock: the socket provided by kernel
*       
*   RETURN: the corresponding SIPC socket struct.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static inline ipcSocket_t* socket_to_sipc_sock(struct socket* sock)
{
    ipcSocket_t* sipc_sock = NULL;
    struct sock* sk = NULL;

    if (sock != NULL)
    {
        sk = sock->sk;
        if (sk != NULL)
        {
            sipc_sock = (ipcSocket_t*)sk->sk_protinfo;
        }
    }
    return sipc_sock;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sock_to_sipc_sock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function converts the "struct sock*" variable which is provided by
*   kernel to a "struct ipcSocket_t*" struct.
*   
*   NOTE: This function is tested in linux 2.6.9, not sure if it works in other
*   kernel versions.
*   
*   PARAMETER(S):
*       sk: the socket provided by kernel
*       
*   RETURN: the corresponding SIPC socket struct.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static inline ipcSocket_t* sock_to_sipc_sock(struct sock* sk)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (ipcSocket_t*)sk->sk_protinfo;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_alloc_sock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function allocates a "struct sock*" instance for a given sock of type
*   "struct socket*".
*   
*   NOTE: This function is tested in linux 2.6.9, not sure if it works in other
*   kernel versions.
*   
*   PARAMETER(S):
*       sock: the socket provided by kernel
*       
*   RETURN: The allocated "struct sock*" instance.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static inline struct sock* sipc_alloc_sock(struct socket* sock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct sock* sk = sk_alloc(AF_IPC, GFP_KERNEL, 1, NULL);

/*--------------------------------------- CODE -----------------------------------------*/
    if (sk != NULL)
    {
        sock_init_data(NULL, sk);
    }
    return sk;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sock_set_sipc_sock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function registers SIPC protocol to the linux protocol family.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: 0 if OK, or a negative int is returned as error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static inline void sock_set_sipc_sock(struct sock* sk, ipcSocket_t* sipc_sock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    sk->sk_protinfo = sipc_sock;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: register_sipc_protocol
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function registers SIPC protocol to the linux protocol family.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: 0 if OK, or a negative int is returned as error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int register_sipc_protocol(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return sock_register(&sipc_family_ops);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: unregister_sipc_protocol
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function removes the SIPC protocol from the linux protocol family
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: 0
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int unregister_sipc_protocol(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    sock_unregister(AF_IPC);
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_create
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates an SIPC socket for a struct socket variable, required
*   by the linux protocol framework.
*   
*   PARAMETER(S):
*       sock: The socket in the linux protocol framework
*       protocol: protocol type, only IPC_PROTO is valid
*       
*   RETURN: 0 if ok, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_create(struct socket* sock, int protocol)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct sock* sk;
    ipcSocket_t* sipc_sock;
    int rc = -ESOCKTNOSUPPORT;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SOCKET_ID);
    if ((protocol == IPC_PROTO) && 
        ((sock->type & ~(IPC_CL_SOCK | IPC_CO_SOCK | IPC_DSM_DISCARD | IPC_DSM_WAKE_UP)) == 0))
    {
        if (NULL != (sk = sipc_alloc_sock(sock)))
        {
            sipc_sock = ipcsocket_imp(AF_IPC, sock->type, IPC_PROTO, &rc);
            if ((int)sipc_sock == IPC_ERROR)
            {
                rc = translate_errorcode(rc);
                /* release it */
                sock_put(sk);
            }
            else
            {
                rc = 0;
                sock_set_sipc_sock(sk, sipc_sock);
                sock_init_data(sock, sk);

                sock->ops = &sipc_proto_ops;
                sk->sk_protocol = IPC_PROTO;
                sk_set_owner(sk, THIS_MODULE);
            }
        }
    }
    ipcTrace(SIPCEXITLOG, IPC_SOCKET_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_release
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function releases SIPC resources for a given socket.
*   
*   PARAMETER(S):
*       sock: The socket in the linux protocol framework
*       
*   RETURN: 0 if ok, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int sipc_release(struct socket *sock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
    ipcSocket_t* sipc_sock = socket_to_sipc_sock(sock);
    struct sock* sk = sock->sk;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_CLOSESOCKET_ID);
    if (ipcclosesocket_imp(sipc_sock, &rc) < 0)
    {
        rc = translate_errorcode(rc);
    }
    else
    {
        rc = 0;
    }
    if (sk != NULL)
    {
        sk->sk_protinfo = 0;
        sock_orphan(sk);
        sock->sk = NULL;
        sock_put(sk);
    }
    /* XXX: shall we always return 0? */
    ipcTrace(SIPCEXITLOG, IPC_CLOSESOCKET_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_bind
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "bind" interface to the protocol framework, it will ipcbind to 
*   bind the socket.
*   
*   PARAMETER(S):
*       sock: the socket pointer in the protocol framework
*       myaddr: the address of the socket
*       sockaddr_len: length of the address
*       
*   RETURN: 0 if OK, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_bind(struct socket *sock, struct sockaddr *myaddr, int sockaddr_len)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_BIND_ID);
    if (ipcbind_imp(socket_to_sipc_sock(sock), 
                (struct IPCSOCK_ADDR*)myaddr, sockaddr_len, &rc) < 0)
    {
        rc = translate_errorcode(rc);
    }
    else
    {
        rc = 0;
    }
    ipcTrace(SIPCEXITLOG, IPC_BIND_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_connect
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "connect" interface to the protocol framework, it calls
*   ipcconnect eventually.
*   
*   PARAMETER(S):
*       sock: the sockcet pointer int the protocol framework
*       vaddr: the address of the server
*       sockaddr_len: length of the server address
*       flags: Never used.
*       
*   RETURN: 0 if OK, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_connect(struct socket *sock, struct sockaddr *vaddr, int sockaddr_len, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_CONNECT_ID);
    if (ipcconnect_imp(socket_to_sipc_sock(sock),
                (struct IPCSOCK_ADDR*)vaddr, sockaddr_len, &rc) < 0)
    {
        rc = translate_errorcode(rc);
    }
    else
    {
        rc = 0;
    }
    ipcTrace(SIPCEXITLOG, IPC_CONNECT_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_accept
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "accept" interface for the protocol framework, it calls 
*   ipcaccept eventually.
*   
*   PARAMETER(S):
*       sock: socket pointer in the protocol framework
*       newsock: pointer to hold the new socket
*       flags: never used
*       
*   RETURN: 0 if OK, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Return ERESTARTSYS on IPCEINTR
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_accept(struct socket *sock, struct socket *newsock, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = -EINVAL;
    ipcSocket_t* new_sipc_sock;
    struct sock* new_sk;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_ACCEPT_ID);
    new_sk = sipc_alloc_sock(newsock);
    if (new_sk != NULL)
    {
        new_sipc_sock = ipcaccept_imp(socket_to_sipc_sock(sock),
                NULL, NULL, &rc);
        if ((int)new_sipc_sock == IPC_ERROR)
        {
            sock_put(new_sk);
            if (rc == IPCEINTR)
            {
                rc = -ERESTARTSYS;
            }
            else
            {
                rc = translate_errorcode(rc);
            }
        }
        else if (new_sipc_sock == socket_to_sipc_sock(sock))
        {
            /* This server socket is listened with IPC_DIRECT_CONNECT */
            sock_put(new_sk);
            rc = IPC_LOWEST_ERROR_CODE - 1;
        }
        else
        {
            sock_set_sipc_sock(new_sk, new_sipc_sock);
            sock_init_data(newsock, new_sk);

            newsock->ops = &sipc_proto_ops;
            new_sk->sk_protocol = IPC_PROTO;
            sk_set_owner(new_sk, THIS_MODULE);
            rc = 0;
        }
    }
    else
    {
        rc = -ENOMEM;
    }
    ipcTrace(SIPCEXITLOG, IPC_ACCEPT_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_getname
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "getsockname" and "getpeername" interfacce for the protocol 
*   framework, it calls ipcgetsockname eventually.
*   
*   PARAMETER(S):
*       sock: socket pointer in the protocol framework
*       addr: [out] output parameter to hold the socket address
*       sockaddr_len: [in,out] address size
*       peer: a bool value to indicate get peer name or self name
*       
*   RETURN: 0 if OK, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_getname(struct socket *sock, struct sockaddr *addr, int *sockaddr_len, int peer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = -EINVAL;
    int result = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    if (sockaddr_len != NULL)
    {
        *sockaddr_len = sizeof(struct IPCSOCK_ADDR);
    }
    if (peer)
    {
        ipcTrace(SIPCENTRYLOG, IPC_GETPEERNAME_ID);
        result = ipcgetpeername_imp(socket_to_sipc_sock(sock), (struct IPCSOCK_ADDR*)addr, sockaddr_len, &rc);
        ipcTrace(SIPCEXITLOG, IPC_GETPEERNAME_ID);
    }
    else
    {
        ipcTrace(SIPCENTRYLOG, IPC_GETSOCKETNAME_ID);
        result = ipcgetsockname_imp(socket_to_sipc_sock(sock), (struct IPCSOCK_ADDR*)addr, sockaddr_len, &rc);
        ipcTrace(SIPCEXITLOG, IPC_GETSOCKETNAME_ID);
    }
    if (result < 0)
    {
        rc = translate_errorcode(rc);
    }
    else
    {
        rc =0 ;
    }
    
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_poll
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "poll" interface for the protocol framework. It is invoked for
*   syscall "select" or "poll".
*   
*   PARAMETER(S):
*       filp: pointer to the the file struct for the socket
*       ssock: socket pointer in the protocol family
*       poll_tab: poll table, required by the interface
*       
*   RETURN: The status of the socket
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned int sipc_poll(struct file *filp, struct socket *ssock, struct poll_table_struct *poll_tab)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int mask = 0;
    ipcSocket_t* sock = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    sock = socket_to_sipc_sock(ssock);
    if (!ipcIsSocketValid(sock))
    {
        return -ENODEV;
    }

    poll_wait(filp, &sock->readwq, poll_tab);
    poll_wait(filp, &sock->writewq, poll_tab);

    if (((sock->state == IPC_SOCK_LISTENING) && (sock->newConnList.size > 0)) ||
        (sock->packetQueue.currentSize > 0))
    {
        mask |= POLLIN | POLLRDNORM;
    }
    if (!sock->isCOSocket || 
         SEQ_LT(sock->conn.nextTxSeqNum, sock->conn.maxTxSeqNum + 1))
    {
        mask |= POLLOUT | POLLWRNORM;
    }
    /* XXX: For CO sockets, if it's not suitable for reading, mark it can read */
    if (sock->isCOSocket)
    {
        if (!sock->canReceive || 
            !sock->peerCanSend || 
            (sock->state == IPC_SOCK_NOT_CONNECTED))
        {
            mask |= POLLIN | POLLRDNORM;
        }
    }

    return mask;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_ioctl
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the implementation of "ioctl" interface, it's the entry point of 
*   most SIPC functions.
*   
*   PARAMETER(S):
*       sock: pointer to the socket
*       cmd: ioctl command
*       a: ioctl argument
*       
*   RETURN: 0 if OK, otherwise IPC_ERROR (-1) is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_ioctl(struct socket *sock, unsigned int cmd, unsigned long a)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* sipc_sock = socket_to_sipc_sock(sock);
    ipcCmd_t arg;
    struct IPCSOCK_ADDR addr;
    int rc = 0;
    int len;
    unsigned long bytesleft;
    char *zbuf = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    bytesleft = copy_from_user(&arg, (void*)a, sizeof(ipcCmd_t));
    if (bytesleft != 0)
    {
        ipcError(IPCLOG_GENERAL, ("copy_from_user failed\n"));
        return -1;
    }

    switch(cmd)
    {
        case CMD_IPCBIND:
            ipcTrace(SIPCENTRYLOG, IPC_BIND_ID);
            copy_from_user(&addr, arg.u.bind.addr, sizeof(addr));
            rc = ipcbind_imp(sipc_sock, &addr,
                    sizeof(struct IPCSOCK_ADDR),
                    &arg.lasterror);
            if (IPC_ERROR == rc) 
            {
                ipcError(IPCLOG_GENERAL, ("Try to bind to: %d failed!\n", addr.ipc_service));
                rc = IPC_ERROR;
            }
            ipcTrace(SIPCEXITLOG, IPC_BIND_ID);
            break;
        case CMD_IPCZSENDTO:
            ipcTrace(SIPCENTRYLOG, IPC_ZSENDTO_ID);
            copy_from_user(&addr, arg.u.zsendto.addr, sizeof(addr));
            zbuf = (char *)ipcSMUserToVirtual((unsigned long)arg.u.zsendto.zbuffer);
            if (zbuf == NULL)
            {
                ipcError(IPCLOG_GENERAL, ("Memory corruption detected.\n"));
                rc = IPC_ERROR;
            }
            else
            {
                rc = ipczsendto_imp(sipc_sock,
                        zbuf,
                        arg.u.zsendto.dataLen,
                        arg.u.zsendto.flag,
                        &addr,
                        sizeof(struct IPCSOCK_ADDR),
                        &arg.lasterror);
            }
            ipcTrace(SIPCEXITLOG, IPC_ZSENDTO_ID);
            break;
        case CMD_IPCZRECVFROM:
            ipcTrace(SIPCENTRYLOG, IPC_ZRECVFROM_ID);
            len = sizeof(addr);
            rc = ipczrecvfrom_imp(sipc_sock,
                    &arg.u.zrecvfrom.zbuffer,
                    arg.u.zrecvfrom.flag,
                    &addr,
                    &len,
                    &arg.lasterror);
            if (rc >= 0) 
            {
                /* XXX: argument checking should be done in libipcsocket */
                /* NOTE: The source addr CAN be NULL? */
                if (arg.u.zrecvfrom.addr != NULL)
                {
                    (void)copy_to_user(arg.u.zrecvfrom.addr, &addr, sizeof(addr));
                }
                arg.u.zrecvfrom.zbuffer = (char *)ipcSMVirtualToUser((unsigned long)arg.u.zrecvfrom.zbuffer);
                if (arg.u.zrecvfrom.zbuffer == NULL)
                {
                    ipcError(IPCLOG_GENERAL, ("Memory corruption detected.\n"));
                    rc = IPC_ERROR;
                }
            }
            ipcTrace(SIPCEXITLOG, IPC_ZRECVFROM_ID);
            break;
        case CMD_IPCSETSOCKQLEN:
            ipcTrace(SIPCENTRYLOG, IPC_SETSOCKQLEN_ID);
            rc = ipcsetsockqlen_imp(sipc_sock,
                    arg.u.setsockqlen.length,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_SETSOCKQLEN_ID);
            break;
        case CMD_IPCGETSOCKNAME:
            ipcTrace(SIPCENTRYLOG, IPC_GETSOCKETNAME_ID);
            len = sizeof(addr);
            rc = ipcgetsockname_imp(sipc_sock,
                    &addr,
                    &len,
                    &arg.lasterror);
            if (rc >= 0) 
            {
                (void)copy_to_user(arg.u.getsockname.name, &addr, sizeof(addr));
            }
            ipcTrace(SIPCEXITLOG, IPC_GETSOCKETNAME_ID);
            break;
        case CMD_IPCGETPEERNAME:
            ipcTrace(SIPCENTRYLOG, IPC_GETPEERNAME_ID);
            len = sizeof(addr);
            rc = ipcgetpeername_imp(sipc_sock,
                    &addr,
                    &len,
                    &arg.lasterror);
            if (rc >= 0) 
            {
                (void)copy_to_user(arg.u.getsockname.name, &addr, sizeof(addr));
            }
            ipcTrace(SIPCEXITLOG, IPC_GETPEERNAME_ID);
            break;
        case CMD_IPCSHUTDOWN:
            ipcTrace(SIPCENTRYLOG, IPC_SHUTDOWN_ID);
            rc = ipcshutdown_imp(sipc_sock, arg.u.shutdown.how,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_SHUTDOWN_ID);
            break;
        case CMD_IPCCHAN:
            ipcTrace(SIPCENTRYLOG, IPC_CHAN_ID);
            rc = ipcchan_imp(sipc_sock, arg.u.chan.qos,
                    arg.u.chan.flag,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_CHAN_ID);
            break;
        case CMD_IPCNOTIFY:
            ipcTrace(SIPCENTRYLOG, IPC_NOTIFY_ID);
            (void)copy_from_user(&addr, arg.u.notify.name, sizeof(addr));
            rc = ipcnotify_imp(sipc_sock,
                    &addr, sizeof(addr),
                    arg.u.notify.msg, arg.u.notify.msglen, arg.u.notify.flags,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_NOTIFY_ID);
            break;
        case CMD_IPCLISTEN:
            ipcTrace(SIPCENTRYLOG, IPC_LISTEN_ID);
            rc = ipclisten_imp(sipc_sock,
                    arg.u.listen.backlog,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_LISTEN_ID);
            break;
        case CMD_IPCCONNECT:
            ipcTrace(SIPCENTRYLOG, IPC_CONNECT_ID);
            (void)copy_from_user(&addr, arg.u.connect.addr, sizeof(addr));
            rc = ipcconnect_imp(sipc_sock,
                    &addr,
                    sizeof(addr),
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_CONNECT_ID);
            break;
        case CMD_IPCZSEND:
            ipcTrace(SIPCENTRYLOG, IPC_ZSEND_ID);
            zbuf = (char *)ipcSMUserToVirtual((unsigned long)arg.u.zsend.zbuffer);
            if (zbuf == NULL)
            {
                ipcError(IPCLOG_GENERAL, ("Memory corruption detected.\n"));
                rc = IPC_ERROR;
            }
            else
            {
                rc = ipczsend_imp(sipc_sock,
                        zbuf,
                        arg.u.zsend.len,
                        arg.u.zsend.flag,
                        &arg.lasterror);
            }
            ipcTrace(SIPCEXITLOG, IPC_ZSEND_ID);
            break;
        case CMD_IPCZRECV:
            ipcTrace(SIPCENTRYLOG, IPC_ZRECV_ID);
            rc = ipczrecv_imp(sipc_sock,
                    &arg.u.zrecv.zbuffer,
                    arg.u.zrecv.flag,
                    &arg.lasterror); 
            if (rc >= 0)
            {
                arg.u.zrecvfrom.zbuffer = (char *)ipcSMVirtualToUser((unsigned long)arg.u.zrecvfrom.zbuffer);
                if (arg.u.zrecvfrom.zbuffer == NULL)
                {
                    ipcError(IPCLOG_GENERAL, ("Memory corruption detected.\n"));
                    rc = IPC_ERROR;
                }
            }
            ipcTrace(SIPCEXITLOG, IPC_ZRECV_ID);
            break;
        case CMD_IPCSEND:
            ipcTrace(SIPCENTRYLOG, IPC_SEND_ID);
            rc = ipcsend_imp(sipc_sock,
                    arg.u.send.buffer,
                    arg.u.send.datalen,
                    arg.u.send.flags,
                    NULL,
                    0,
                    &arg.lasterror);    
            ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
            break;
        case CMD_IPCRECV:
            ipcTrace(SIPCENTRYLOG, IPC_RECV_ID);
            rc = ipcrecv_imp(sipc_sock, 
                    arg.u.recv.buffer,
                    arg.u.recv.datalen,
                    arg.u.recv.flags, 
                    NULL,
                    0,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_RECV_ID);
            break;    
        case CMD_IPCSENDTO:
            ipcTrace(SIPCENTRYLOG, IPC_SENDTO_ID);
            copy_from_user(&addr, arg.u.sendto.addr, sizeof(addr));
            rc = ipcsend_imp(sipc_sock,
                    arg.u.sendto.buffer,
                    arg.u.sendto.datalen,
                    arg.u.sendto.flags,
                    &addr,
                    sizeof(struct IPCSOCK_ADDR),
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_SENDTO_ID);
            break;
        case CMD_IPCRECVFROM:
            ipcTrace(SIPCENTRYLOG, IPC_RECVFROM_ID);
            len = sizeof(addr);
            rc = ipcrecv_imp(sipc_sock,
                    arg.u.recvfrom.buffer,
                    arg.u.recvfrom.datalen,
                    arg.u.recvfrom.flags,
                    &addr,
                    &len,
                    &arg.lasterror);
            if (rc >= 0) 
            {
                (void)copy_to_user(arg.u.recvfrom.addr, &addr, sizeof(addr));
            }
            ipcTrace(SIPCEXITLOG, IPC_RECVFROM_ID);
            break;
        case CMD_IPCSETWATERMARK:
            ipcTrace(SIPCENTRYLOG, IPC_SETWATERMARK_ID);
            rc = ipcsetwatermark_imp(sipc_sock,
                    arg.u.setwatermark.lowwmark,
                    arg.u.setwatermark.highwmark,
                    &arg.lasterror);
            ipcTrace(SIPCEXITLOG, IPC_SETWATERMARK_ID);
            break;
        default:
            break;
    }

    /* copy arg to user space */
    copy_to_user((void*)a, &arg, sizeof(arg));

    /* If the call is broken by a signal, return -ERESTARTSYS */
    if ((rc < 0) && (arg.lasterror == IPCEINTR))
    {
        rc = -ERESTARTSYS;
    }
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_listen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the interface of "listen" in protocol framework. It calls ipclisten
*   eventually.
*   
*   In SIPC, if backlog is 0, it will be considered as direct connect case, 
*   but standard socket interface doesn't have such feature, so backlog is
*   changed to 1 if it's 0 to disable direct connect feature.
*   
*   PARAMETER(S):
*       sock: socket pointer
*       len: backlog 
*       
*   RETURN: 0 if OK, otherwise an error code is returned
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_listen(struct socket *sock, int len)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = -EINVAL;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_LISTEN_ID);
    if (len == 0)
    {
        ipcWarning(IPCLOG_GENERAL,
                   ("sipc_listen: DIRECT_CONNECT is not allowed\n"));
        len = 1;
    }

    if (ipclisten_imp(socket_to_sipc_sock(sock), len, &rc) < 0)
    {
        rc = translate_errorcode(rc);
    }
    else
    {
        rc =0 ;
    }
    ipcTrace(SIPCEXITLOG, IPC_LISTEN_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_shutdown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "shutdown" interface for the protocol framework. It calls 
*   ipcshutdown eventually.
*   
*   In the standard socket library, valid flags are SHUT_RD, SHUT_WR or 
*   SHUT_BOTH, but SIPC takes IPCSD_SEND, IPCSD_RECEIVE and IPCSD_BOTH as
*   parameters, this function recognizes IPC* flags instead of standard flags.
*   
*   PARAMETER(S):
*       sock: pointer to socket
*       flags: shutdown flags, IPCSD_SEND, IPCSD_RECEIVE or IPCSD_BOTH
*       
*   RETURN: 0 if OK, otherwise an error code is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
int sipc_shutdown(struct socket *sock, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = -EINVAL;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SHUTDOWN_ID);
    if (ipcshutdown_imp(socket_to_sipc_sock(sock), flags, &rc) < 0)
    {
        rc = translate_errorcode(rc);
    }
    else
    {
        rc =0 ;
    }
    ipcTrace(SIPCEXITLOG, IPC_SHUTDOWN_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_setsockopt
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "setsockopt" interface for the protocol framework.
*   
*   Not implemented.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int sipc_setsockopt(struct socket *sock, int level, int optname, char __user *optval, int optlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return -EINVAL;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_getsockopt
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "getsockopt" interface for the protocol framework.
*   
*   Not implemented.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int sipc_getsockopt(struct socket *sock, int level, int optname, char __user *optval, int __user *optlen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return -EINVAL;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_sendmsg
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the interface "sendmsg" in the protocol framework, it implements
*   syscall send/sendto/sendmsg for SIPC protocol.
*   
*   It uses IPC flags instead of standard socket API flags.
*   
*   PARAMETER(S):
*       iocb: Not used
*       sock: pointer to the socket
*       m: the message
*       total_len: message length
*       
*   RETURN: If OK, the length actually sent will be returned, otherwise an error
*           code is returned.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace
*
*****************************************************************************************/
int sipc_sendmsg(struct kiocb *iocb, struct socket *sock, struct msghdr *m, size_t total_len)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* ptr;
    int rc = -ENOMEM;
    int sentlen = 0;
    ipcSocket_t* sipc_sock = socket_to_sipc_sock(sock);

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_SEND_ID);
    if (m->msg_iov->iov_base == NULL)
    {
        ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
        return -EFAULT;
    }
    ptr = ipczalloc(total_len, IPC_PROCSHM, 0);
    if (ptr != NULL)
    {
        copy_from_user(ptr, m->msg_iov->iov_base, total_len);
        if (m->msg_name != NULL)
        {
            sentlen = ipczsendto_imp(sipc_sock, ptr, total_len, 
                    m->msg_flags, m->msg_name, m->msg_namelen, &rc);
        }
        else
        {
            sentlen = ipczsend_imp(sipc_sock, ptr, total_len, m->msg_flags, &rc);
        }
        if (sentlen < 0)
        {
            rc = translate_errorcode(rc);
            ipczfree(ptr);
        }
        else
        {
            rc = sentlen;
        }
    }
    ipcTrace(SIPCEXITLOG, IPC_SEND_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sipc_recvmsg
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the "recvmsg" for the protocol framework, it implements the syscall
*   recv/recvfrom/recvmsg.
*   
*   It uses IPC flags instead of standard socket API flags.
*   
*   PARAMETER(S):
*       iocb: not used
*       sock: pointer to the socket
*       m: message 
*       total_len: buffer length
*       flags: receive flags
*       
*   RETURN: if OK, the actually copied size is returned, otherwise an error code
*           is retured.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
* 18Jul2007  Motorola Inc.         Added ipcTrace
*
*****************************************************************************************/
int sipc_recvmsg(struct kiocb *iocb, struct socket *sock, struct msghdr *m, size_t total_len, int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcSocket_t* sipc_sock = socket_to_sipc_sock(sock);
    int rc = -EINVAL;
    int recvlen = 0;
    char* buf = m->msg_iov->iov_base;
    char* zbuf = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcTrace(SIPCENTRYLOG, IPC_RECV_ID);
    if (buf == NULL)
    {
        ipcTrace(SIPCEXITLOG, IPC_RECV_ID);
        return -EFAULT;
    }

    if (total_len > 0)
    {
        recvlen = ipczrecvfrom_imp(sipc_sock, &zbuf, flags, m->msg_name, (int*)&m->msg_namelen, &rc);
        if (recvlen < 0)
        {
            rc = translate_errorcode(rc);
        }
        else if (recvlen > total_len)
        {
            /* We can't handle this correctly, just return -EMSGSIZE */
            rc = -EMSGSIZE;
        }
        else
        {
            copy_to_user(buf, zbuf, recvlen);
            rc = recvlen;
        }

        if (zbuf != NULL)
        {
            ipczfree(zbuf);
        }
    }
    ipcTrace(SIPCEXITLOG, IPC_RECV_ID);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: translate_errorcode
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function translates SIPC error codes to UNIX API error codes.
*   
*   PARAMETER(S):
*       ec: IPC error code
*       
*   RETURN: The translated UNIX error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Jan2007  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int translate_errorcode(int ec)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    switch (ec)
    {
        case IPCEATTACH:
            return -EACCES;
        case IPCENOTREADY:
            return -EBUSY;
        case IPCEDETACH:
            return -EBUSY;
        case IPCENOTINIT:
            return -EACCES;
        case IPCEBUSY:
            return -EBUSY;
        case IPCEAFNOSUPPORT:
            return -EAFNOSUPPORT;
        case IPCENOBUFS:
            return -ENOMEM;
        case IPCEPROTONOSUPPORT:
            return -EPROTONOSUPPORT;
        case IPCEAUTH:
            return -EPERM;
        case IPCEFAULT:
            return -EFAULT;
        case IPCEINVAL:
            return -EINVAL;
        case IPCENOTSOCK:
            return -ENOTSOCK;
        case IPCEINTR:
            return -EINTR;
        case IPCECHAN:
            return -EPIPE;
        case IPCENODE:
            return -ENETUNREACH;
        case IPCEQOS:
            return -EAGAIN;
        case IPCESERVICE:
            return -ENETUNREACH;
        case IPCEWMARK:
            return -IPCEINVAL;
        case IPCESERVICETABLE:
            return -EFAULT;
        case IPCEWOULDBLOCK:
            return -EAGAIN;
        case IPCEMSGSIZE:
            return -EFAULT;
        case IPCEADDRNOTVAL:
            return -EINVAL;
        case IPCEADDRINUSE:
            return -EADDRINUSE;
        case IPCENETUNREACH:
            return -ENETUNREACH;
        case IPCENOSHMEM:
            return -ENOMEM;
        case IPCEBADBUF:
            return -EFAULT;
        case IPCENOTATTACH:
            return -EAGAIN;
        case IPCENOTIMPLEMENTED:
            return -EINVAL;
        case IPCESHUTDOWN:
            return -EPIPE;
        case IPCESOCKFULL:
            return -ENOSPC;
        case IPCECONNREFUSED:
            return -ECONNREFUSED;
        case IPCEISCONN:
            return -EISCONN;
        case IPCETIMEDOUT:
            return -ETIMEDOUT;
        case IPCESOCKNOSUPPORT:
            return -EAFNOSUPPORT;
        case IPCENOTCONN:
            return -ENOTCONN;
        case IPCEFLAG:
            return -EINVAL;
    }
    return -EINVAL;
}

