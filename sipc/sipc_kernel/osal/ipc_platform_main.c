/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_platform_main.c
*   FUNCTION NAME(S): ipcClosePrintMuHandler
*                     ipcGetNodeDsmStatus
*                     ipcMainThread
*                     ipcPlatformDataIndication
*                     ipcPlatformDestroy
*                     ipcPlatformDeviceInit
*                     ipcPlatformInit
*                     ipcPlatformNotify
*                     ipcPrintMuThread
*                     ipcSetupPrintMuHandler
*                     muShmPrintCallback
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 10May2004  Motorola Inc.         Initial Creation
* 13Mar2006  Motorola Inc.         Change kernel_thread to kthread_run
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation
* 14Dec2006  Motorola Inc.         Changed macro condition to pass build for i386
* 03Jan2007  Motorola Inc.         Support DSM message discard
* 15Jan2007  Motorola Inc.         Include ipc_macro_defs.h
* 23Feb2007  Motorola Inc.         Renamed ipcFinishMemory to ipcCloseMemLock
* 13Jul2007  Motorola Inc.         Checked compile condition IPC_SIMPLE_DEVICE
*
* 19Jul2007  Motorola Inc.         Added ipcTrace
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_macro_defs.h"
#include "ipc_common.h"
#include "ipcsocket_osal.h"
#include "ipc_router_interface.h"
#include "ipc_device_interface.h"
#include "ipc_osal_util.h"
#include "ipc_memory.h"
#include <linux/kthread.h>

#ifdef HAVE_PHYSICAL_SHM
#include "ipc_mu_shm.h"
#include "mu_drv.h"
#endif


/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/

static char* config="M10x10";
static struct task_struct* main_pid;

#if defined(HAVE_PHYSICAL_SHM) && !defined(IPC_RELEASE)
static ipcQueue_t printMuMsgQueue;
struct task_struct* printMuThread = NULL;
#endif

/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/


extern BOOL ipcExtOpen(UINT8 portId, INT32 bandwidth, INT32 framerate);
extern BOOL ipcUsbOpen(ipcPortId_t portId, UINT32 bandwidth, UINT32 framerate);

#if defined(HAVE_PHYSICAL_SHM) && !defined(IPC_RELEASE)
static void muShmPrintCallback(int channel, int data);
static void ipcSetupPrintMuHandler(void);
static void ipcClosePrintMuHandler(void);
static int ipcPrintMuThread(void* data);
#endif
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPlatformDeviceInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   Initialize the devices provided by the stack
*   
*   PARAMETER(S):
*       none
*       
*   RETURN: TRUE for success, FALSE for failure
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Apr2005  Motorola Inc.         Initial Creation
* 09Dec2005  Motorola Inc.         Modify the comments and function  header description.
* 13Jul2007  Motorola Inc.         Checked compile condition IPC_SIMPLE_DEVICE
*
*****************************************************************************************/
BOOL ipcPlatformDeviceInit()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    BOOL ret = FALSE;
    BOOL devRet = TRUE;
    INT32 bandwidth, framerate;
    ipcPortId_t portId = 0;
    char *p=config, *q=config, *t=NULL;
    char buf[80];
    int device_type = IPC_DEVICE_EXT;

    memset(ipcStack_glb.ports, 0, sizeof(ipcStack_glb.ports));
    ipcStack_glb.numOpenPorts = 0;

    if (*p == 0)
    {
       return TRUE;
    }
    while ((*p != 0) && (*q != 0) && (portId < IPC_MAX_PORTS))
    {
        t = p;
        while ((*t != 'x') && (*t != ':') && (*t != 0))
        {
            t++;
        }
        if (*t != 'x')
        {
            ipcError(IPCLOG_GENERAL, ("Config string format error\n"));
            return ret;
        }
        memcpy(buf, p, t-p);
        buf[t-p] = 0;
        switch (buf[0])
        {
            case 'M':
            case 'm':
                device_type = IPC_DEVICE_SHARED_MEMORY;
                p = &buf[1];
                break;
#ifndef IPC_SIMPLE_DEVICE
            case 'E':
            case 'e':
                device_type = IPC_DEVICE_EXT;
                p = &buf[1];
                break;
            case 'U':
            case 'u':
                device_type = IPC_DEVICE_USB;
                p = &buf[1];
                break;
#endif
        }
        
        bandwidth = atoi(p);
        q = ++t;
        while ((*q != 'x') && (*q != ':') && (*q != 0))
        {
            q++;
        }
        if ((*q != ':') && (*q != 0))
        {
            ipcError(IPCLOG_GENERAL, ("Config string format error\n"));
            return ret;
        }

        p = q + 1;
        memcpy(buf, t, q-t);
        buf[q-t] = 0;
        framerate = atoi(buf);

        ipcDebugLog(IPCLOG_GENERAL, ("Got configuration, bandwidth=%d, framerate=%d\n",
                bandwidth, framerate));

        switch (device_type)
        {
            case IPC_DEVICE_SHARED_MEMORY:
#ifdef HAVE_PHYSICAL_SHM
                devRet = ipcCreateShmDevice(portId, muWriteIpcPacket);
                if (devRet == TRUE)
                {
                    portId++;
	            ret |= devRet;
                }
#else
                ret = TRUE;
#endif /* HAVE_PHYSICAL_SHM */
                break;
#ifndef IPC_SIMPLE_DEVICE
            case IPC_DEVICE_EXT:
                devRet = ipcExtOpen(portId, bandwidth, framerate);
                if (devRet == TRUE)
                {
                    portId++;
	            ret |= devRet;
                }
                break;
                
            case IPC_DEVICE_USB:
#ifdef HAVE_USB_DEV
                devRet = ipcUsbOpen(portId, bandwidth, framerate);
                if (devRet == TRUE)
                {
                    portId++;
	            ret |= devRet;
                }
#else
                ret = TRUE;
#endif /* HAVE_USB_DEV */
                break;
#endif /* IPC_SIMPLE_DEVICE */
            default:
                ret = TRUE;
                break;
        }

    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMainThread
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the main IPC thread.  It processes all incoming IPC packets.
*   
*   PARAMETER(S):
*       LPVOID pParam
*       
*   RETURN: DWORD
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 29Nov2004  Motorola Inc.         Initial Creation
* 09May2005  Motorola Inc.         Moved main IPC thread to platform specific file
* 23May2005  Motorola Inc.         Ported to linux
* 11Aug2005  Motorola Inc.         Check return value of ipcOsalSemaWait
* 17Jan2007  Motorola Inc.         Moved out ipcStackInig 
* 18Jul2007  Motorola Inc.         Added ipcTrace funcition
*
*****************************************************************************************/
static int ipcMainThread(void* pParam)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packet;
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    while(TRUE)
    {
        ipcTrace(SIPCENTRYLOG, IPC_MAIN_ID);
        rc = ipcOsalSemaWait(ipcStack_glb.receivedPacketQueue.notEmptySema, 
            IPC_OSAL_INFINITE);
        if (rc != IPCE_OK)
        {
            ipcError(IPCLOG_GENERAL, ("System shutdown withou unloading IPC module\n"));
            break;
        }


        ipcDebugLog(IPCLOG_DATA, ("There's data in receivedPacketQueue\n"));
        if (ipcStack_glb.state == IPC_STACK_STATE_TERMINATED)
        {
            ipcDebugLog(IPCLOG_GENERAL, ("The stack is going down\n"));
            break;
        }

        packet = (ipcPacket_t *)ipcQueueGetLink(&ipcStack_glb.receivedPacketQueue,
            0, NULL);

        if (packet == NULL)
        {
            ipcError(IPCLOG_GENERAL, ("Get link from receivedPacketQueue failed\n"));
            break;
        }
        /* Send the packet to the router first */
        handleDVRTDataIndication(packet);
        ipcTrace(SIPCEXITLOG, IPC_MAIN_ID);
    }

    ipcTrace(SIPCEXITLOG, IPC_MAIN_ID);
    ipcStack_glb.state = IPC_STACK_STATE_NULL;
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPlatformInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function handles platform-specific initialization for windows
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: TRUE for success, FALSE for failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 29Nov2004  Motorola Inc.         Initial Creation
* 14Dec2004  Motorola Inc.         Remove all the MQ create out of this funtion to make everything is ready  before the thread been created
* 09May2005  Motorola Inc.         Renamed to ipcPlatformInit
* 18May2005  Motorola Inc.         Porting to linux
* 09Dec2005  Motorola Inc.         Modify the comments and function header description.
* 13Mar2006  Motorola Inc.         Change kernel_thread to kthread_run
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation
* 14Dec2006  Motorola Inc.         Changed macro condition to pass build for i386
* 17Jan2006  Motorola Inc.         Don't wait for IPC_STACK_STATE_RUNNING 
*
*****************************************************************************************/
BOOL ipcPlatformInit()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/

/*--------------------------------------- CODE -----------------------------------------*/
    /* open last error hash table for other modules in kernel space */
    main_pid = kthread_run(
        (int(*)(void*))ipcMainThread,
        NULL, "ipc_main_thread");
    if (IS_ERR(main_pid))
    {
        return FALSE;
    }

#if defined(HAVE_PHYSICAL_SHM) && !defined(IPC_RELEASE)
    ipcSetupPrintMuHandler();
#endif

    return TRUE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPlatformDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   Free resouces.
*   
*   PARAMETER(S):
*       none
*       
*   RETURN:  none
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation
* 09Dec2005  Motorola Inc.         Modify the comments and function  header description.
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation
* 14Dec2006  Motorola Inc.         Changed macro condition to pass build for i386
* 23Feb2007  Motorola Inc.         Renamed ipcFinishMemory to ipcCloseMemLock
* 01Jun2007  Motorola Inc.         Use separate mutex for each memory type
*
*****************************************************************************************/
void ipcPlatformDestroy()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
#if defined(HAVE_PHYSICAL_SHM) && !defined(IPC_RELEASE)
    ipcClosePrintMuHandler();
#endif

    ipcRemoveAllocator(IPC_DEFAULT_MEM_TYPE);
    ipcRemoveAllocator(IPC_PROCSHM);

#ifdef HAVE_PHYSICAL_SHM
    ipcRemoveAllocator(IPC_PHYSSHM);
#endif /* HAVE_PHYSICAL_SHM */
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPlatformDataIndication
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   PARAMETER(S):
*       none
*       
*   RETURN:  none
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcPlatformDataIndication()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPlatformNotify
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called to handle platform-specific asynchornous
*   notification for various events (see ipcpsnotify_imp for more).
*   
*   PARAMETER(S):
*       notification: structure that contains all data needed to send 
*                                 the actual notification
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 04Oct2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcPlatformNotify(ipcNotification_t *notification)
{
    /* Stubbed on Linux */
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcGetNodeDsmStatus
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function returns the deep sleep mode status of the node with the
*   specified IPC address (unknown, asleep, or awake).
*   
*   PARAMETER(S):
*       ipcAddr: address of the node in question
*       
*   RETURN: IPC_DSM_UNKNOWN
*           IPC_DSM_AWAKE
*           IPC_DSM_ASLEEP
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 03Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
unsigned short ipcGetNodeDsmStatus(unsigned char ipcAddr)
{
#if defined(HAVE_PHYSICAL_SHM)
    ipcPortId_t portId = ipcStack_glb.ipcAddrToPortTable[ipcAddr];
    
    if (ipcAddr == ipcStack_glb.ipcAddr)
    {
        /* pinch self...nope, not dreaming
         * (always return awake for own node) */
        return IPC_DSM_AWAKE;
    }
    else if ((portId < ipcStack_glb.numOpenPorts) &&
        (ipcStack_glb.ports[portId].deviceType == IPC_DEVICE_SHARED_MEMORY))
    {
        /* This is the BP node since we are connected to it via shared memory.
         * The MU driver can tell us whether or not it is currently in DSM */
        return muDSPIsInDSM() ? IPC_DSM_ASLEEP : IPC_DSM_AWAKE;
    }
    else
    {
        /* We are not concerned with DSM status of any other node */
        return IPC_DSM_UNKNOWN;
    }
#else
    return IPC_DSM_UNKNOWN;
#endif
}


#if defined(HAVE_PHYSICAL_SHM) && !defined(IPC_RELEASE)
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: muShmPrintCallback
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This is the callback function that is called whenever a log message is
*   received via the MU
*   
*   PARAMETER(S):
*       channel: MU Channel ID where data was received
*       data: pointer to log message
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 29Aug2005  Motorola Inc.         Initial Version
* 31Oct2005  Motorola Inc.         Fixed potential buffer overflow issues
* 17Apr2006  Motorola Inc.         Enable runtime log config
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation 
* 10Apr2007  Motorola Inc.         Add circular buffer for BP log
*
*****************************************************************************************/
static void muShmPrintCallback(int channel, int data)
{
    char * msg = (char *)ipcSMPhysicalToVirtual(data);
    if (msg != NULL)
    {
        if (IPC_OK != ipcQueuePutObject(&printMuMsgQueue, (void *)msg, 0))
        {
            /* for abnormal, we can't free msg here. */
            ipcError(IPCLOG_GENERAL, ("Insert ipcPrintMu message to queue failed.\n"));
        }
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSetupPrintMuHandler
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function creates printMuMsgQueue and starts ipcPrintMuThread.
*   
*   PARAMETER(S):
*       none
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation 
*
*****************************************************************************************/
void ipcSetupPrintMuHandler(void)
{
    if (ipcQueueOpen(&printMuMsgQueue) != IPCE_OK)
    {
        ipcError(IPCLOG_ROUTER, ("Error opening printMuMsgQueue.\n"));
        return;
    }
    
    printMuThread = kthread_run(ipcPrintMuThread, NULL, "ipc_bp_log");
    if (muRegisterCallback(MU_CHANNEL_SHM_PRINT, muShmPrintCallback) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Could not register for MU_CHANNEL_SHM_PRINT\n"));
    }    
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcClosePrintMuHandler
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function stops ipcPrintMuThread and closes printMuMsgQueue .
*   
*   PARAMETER(S):
*       none
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation 
*
*****************************************************************************************/
void ipcClosePrintMuHandler(void)
{
    if (printMuThread != NULL)
    {
        if (muRegisterCallback(MU_CHANNEL_SHM_PRINT, NULL) < 0)
        {
            ipcError(IPCLOG_GENERAL, ("Could not remove callback for MU_CHANNEL_SHM_PRINT\n"));
        }    

        force_sig(SIGKILL, printMuThread);
        printMuThread = NULL;
    }
    ipcQueueClose(&printMuMsgQueue);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPrintMuThread
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function gets messages from printMuMsgQueue and print it out.
*   
*   PARAMETER(S):
*       none
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 12Dec2006  Motorola Inc.         Refactored ipcPrintMu implementation 
* 10Apr2007  Motorola Inc.         Add circular buffer for BP log
* 25Apr2007  Motorola Inc.         Clean up KW warning
*****************************************************************************************/
int ipcPrintMuThread(void* data)
{
    char * msg;
    char strbuf[MU_LOG_BUF_SIZE];
    int rc;

    while(TRUE)
    {
        msg = (char *)ipcQueueGetObject(&printMuMsgQueue, 0, &rc);
        if (msg == NULL)
        {
            ipcError(IPCLOG_GENERAL, ("Failed to get BP log. ipcPrintMuThread will exit.\n"));
            break;
        }

        /* Copy to local buffer first (otherwise printk acts up) */
        memset(strbuf, 0, sizeof(strbuf));
        strncpy(strbuf, msg, (sizeof(strbuf) - 1));

        rc = strlen(strbuf) - 1;
        if ((rc >= 0) && (strbuf[rc] != '\n'))
        {
           strbuf[rc] = '\n';
        }
        ipcLogFunc("BPSHM:[%d] %s", (rc+1), strbuf);  
    }

    return 0;
}
#endif

ipc_string_param(config);

