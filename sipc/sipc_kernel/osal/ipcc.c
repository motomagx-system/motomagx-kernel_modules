/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipcc.c
*   FUNCTION NAME(S): fill_the_rtinfo
*                     ipc_cleanup
*                     ipc_init
*                     ipc_ioctl
*                     ipc_llseek
*                     ipc_open
*                     ipc_pshm_proc_read
*                     ipc_read
*                     ipc_release
*                     ipc_vshm_proc_read
*                     ipc_write
*                     set_stack_rt_param
*                     split_commands
*                     stack_proc_read
*                     stack_proc_write
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 27Jun2005  Motorola Inc.         Add /proc/sipc to get stack info
* 29Jun2005  Motorola Inc.         Change deprecated remap_page_range to remap_pfn_range for 2.6.10+
* 19Jul2005  Motorola Inc.         Add paddr, psize, vaddr, vsize as parameters when loading IPC stack
* 15Mar2006  Motorola Inc.         Update physical shared memory initialization
* 10Apr2006  Motorola Inc.         support config from /proc use ipc dev for physical mmap 
* 17Apr2006  Motorola Inc.         Enable runtime log config
* 25Apr2006  Motorola Inc.         Include mu_drv.h file
* 26May2006  Motorola Inc.         Support for dynamic pools
* 20Jun2006  Motorola Inc.         Add the permission control for trusted vs. non-trusted Apps
* 14Jul2006  Motorola Inc.         Use kernel variables to determine  physical shared memory address and size
* 17Nov2006  Motorola Inc.         Created char device when opening
* 29Nov2006  Motorola Inc.         Removed udev support
* 23Jan2007  Motorola Inc.         Removed socket device, added protocol framework support
* 23Feb2007  Motorola Inc.         Updated for common alloc
* 13Mar2007  Motorola Inc.         Initialize muStatistics global for MU read/write statistics
* 24Apr2007  Motorola Inc.         Added function to dump physical shared memory
* 25May2007  Motorola Inc.         Added function to handle application memory error
* 07Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
* 13Jul2007  Motorola Inc.         Checked compile flag IPC_NO_LOG
* 19Jul2007  Motorola Inc.         Added ipcTrace
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#undef __NO_VERSION__
#include <linux/module.h>
#include <linux/types.h>  /* size_t */
#include <asm/uaccess.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/device.h>


#include <linux/proc_fs.h>
#include "ipcc.h"        /* local definitions */
#include "ipclog_impl.h"
#include "ipc_platform_defs.h"
#include "ipc_ioctl.h"
#include "ipc_common.h"
#include "ipc_sock_impl.h"
#include "ipc_device_layer.h"
#include "ipc_osal_util.h"
#include "dynamic_pool.h"
#include "ipc_macro_defs.h"

#ifdef HAVE_PHYSICAL_SHM
#include "mu_drv.h"
#include "ipc_shared_memory.h"
#include "ipc_mu_shm.h"
#endif

#ifdef START_UP_TIME
#include <linux/jiffies.h>
unsigned long ulStartUpTime;
#endif

#ifdef CONFIG_ARCH_MXC91131
#   include <linux/lockup_info.h>
#   define LOG_BAD_BUF(reason) lockup_set_info(RESET_DUE_TO_SIPC, (void*)(reason))
#else
#   define LOG_BAD_BUF(reason)
#endif
/*-------------------------------------- CONSTANTS -------------------------------------*/

MODULE_AUTHOR("Motorola");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Inter-Processor Comunication Protocol Stack");

#define IPC_MAX_TYPE 2
#define IPCCHRNAME  "sipc"

#define IPC_MINOR 0
#define IPCMEM_MINOR 16 


/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
struct ipc_log_item_t {
    unsigned int value;
    const char* name;
};
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

extern int ipcPhyShmPoolWalk(void);
extern int register_sipc_protocol(void);
extern int unregister_sipc_protocol(void);
extern void dump_physical_shm(void);
extern void dump_virtual_shm(void);

ssize_t ipc_vshm_proc_read(struct file* file, 
        char* buf,  /*  user space buffer   */
        size_t len, /*  length of buffer    */
        loff_t* offset ); 

ssize_t ipc_pshm_proc_read(struct file* file, 
        char* buf,  /*  user space buffer   */
        size_t len, /*  length of buffer    */
        loff_t* offset ); 

static ssize_t stack_proc_read(struct file* file, 
        char* buf,  /*  user space buffer   */
        size_t len, /*  length of buffer    */
        loff_t* offset );    /* ignored */

static ssize_t stack_proc_write(struct file* file,
        const char __user * buf,
        size_t len,
        loff_t* offset);                                  

/*-------------------------------- STATIC DATA -------------------------------*/

static int ipc_major = IPC_MAJOR;

static BOOL runAsServer = TRUE;
static int nodeId = 2;

static struct file_operations *ipc_fops_array[] = {
    &ipc_fops,
    &ipcmem_dev_fops,
};

static struct file_operations ipc_vshm_proc_ops = {
    .read = ipc_vshm_proc_read
};

static struct file_operations ipc_pshm_proc_ops = {
    .read = ipc_pshm_proc_read
};

static struct file_operations stack_proc_ops = {
    .read = stack_proc_read,
    .write = stack_proc_write
};

#ifndef IPC_NO_LOG

#define LOG_ITEM(m) {.value = m, .name = #m}

static const struct ipc_log_item_t valid_items[] = {
    LOG_ITEM(IPCLOG_TYPE_GENERAL),
    LOG_ITEM(IPCLOG_TYPE_SESSION),
    LOG_ITEM(IPCLOG_TYPE_ROUTER),
    LOG_ITEM(IPCLOG_TYPE_DEVICE),
    LOG_ITEM(IPCLOG_TYPE_USER_APP),
    LOG_ITEM(IPCLOG_TYPE_DATA),
    LOG_ITEM(IPCLOG_TYPE_CHANNELS),
    LOG_ITEM(IPCLOG_TYPE_DEVICE_DECODE),
    LOG_ITEM(IPCLOG_TYPE_RX_FRAMES),
    LOG_ITEM(IPCLOG_TYPE_TX_FRAMES),
    LOG_ITEM(IPCLOG_TYPE_PACKET_LOGGER),
    LOG_ITEM(IPCLOG_TYPE_MEMTRACK),
    LOG_ITEM(IPCLOG_LEVEL_DEBUG),
    LOG_ITEM(IPCLOG_LEVEL_WARNING),
    LOG_ITEM(IPCLOG_LEVEL_ERROR),
    LOG_ITEM(IPCLOG_LEVEL_PANIC),
    LOG_ITEM(IPCLOG_TYPE_ALL),
};                                  

#endif /* !IPC_NO_LOG */
/*------------------------------------- GLOBAL DATA ------------------------------------*/
ulong paddr = 0;
ulong psize = 0;
/* as defualt we do not specify where starts the virtual shared memory */
ulong vaddr = 0xA0000000;
ulong vsize = POOL_SIZE;

int debug_bp = 0;

extern void* physical_shm_base;


ipcProcElem_t ipc_virt_shm_map;
ipcProcElem_t ipc_phys_shm_map;
ipcProcElem_t ipc_stack_info;

struct proc_dir_entry *ipc_root = NULL;
static struct proc_dir_entry *stack_entry = NULL;
static struct proc_dir_entry *ipc_vshm_map = NULL;
static struct proc_dir_entry *ipc_pshm_map = NULL;

/*--------------------------------------- MACROS ---------------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_open
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Open the ipcc device. This is invoked when the user-space applications open
*   the ipc device. e.g. /dev/ipcc
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 31Aug2005  Motorola Inc.         Checked the type value 
*
*****************************************************************************************/
static int ipc_open (struct inode *inode, struct file *filp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int type = TYPE(inode->i_rdev);
/*--------------------------------------- CODE -----------------------------------------*/
    /* open specific device */
    if (!filp->private_data && type) {
        if ((type < 0) || (type >= IPC_MAX_TYPE)) 
        {
            return -ENODEV;
        }
        filp->f_op = ipc_fops_array[type];
        return filp->f_op->open(inode, filp);
    }
    /* it is /dev/ipc */
    get_ipc_module();
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_release
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Release the ipc device. This is invoked when the user-space application 
*   closes the ipc device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int ipc_release (struct inode *inode, struct file *filp)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDebugLog(IPCLOG_GENERAL, ("Releasing ipc stack\n"));
    put_ipc_module();
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_read
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Read data from the ipc device, this does nothing.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static ssize_t ipc_read (struct file *filp, char *buf, size_t count,
                loff_t *f_pos)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
  return count;
}



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_write
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Write data to the ipc device, this is just a stub.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static ssize_t ipc_write (struct file *filp, const char *buf, size_t count,
                loff_t *f_pos)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return count;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_llseek
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function does nothing, it's just a stub.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static loff_t ipc_llseek (struct file *filp, loff_t off, int whence)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
   return off;
}
/*
 * The ioctl() implementation
 */

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_ioctl
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   ioctl of the ipc device.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Apr2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Added some more command types
* 16Jun2005  Motorola Inc.         Physical Shm & Virtural Shm Integration
* 19Jul2005  Motorola Inc.         return the virtual shared memory start address to user.
* 16Feb2005  Motorola Inc.         Bug fix. ipczalloc() will never return if it tries to allocate a block of physical shared memory with size more than max buffer size.
* 20Jun2006  Motorola Inc.         Add the permission control for trusted vs. non-trusted Apps
* 18Jul2007  Motorola Inc.         Added ipcTrace 
*
*****************************************************************************************/
static int ipc_ioctl (struct inode *inode, struct file *filp,
                 unsigned int cmd, unsigned long a)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct IPCSERVICE_LOCATION *srv_tab;
    ipcCmd_t arg;
    int rc = 0;
    unsigned long len;
    unsigned long bytesleft;
/*--------------------------------------- CODE -----------------------------------------*/
    bytesleft = copy_from_user(&arg, (void*)a, sizeof(arg));
    if (bytesleft != 0)
    {
        ipcError(IPCLOG_GENERAL, ("copy_from_user meets some matter...\n"));
        return -1;
    }

    switch (cmd)
    {
        case CMD_IPCLOCATESERVICE:
            ipcTrace(SIPCENTRYLOG, IPC_LOCATESERVICE_ID);
            srv_tab = (struct IPCSERVICE_LOCATION *)
            kmalloc(sizeof(struct IPCSERVICE_LOCATION) * 
                    arg.u.locateservice.maxsizeofservicetable, GFP_KERNEL);
            if (NULL == srv_tab) return -1;
            memset(srv_tab, 0, sizeof(struct IPCSERVICE_LOCATION) * 
                    arg.u.locateservice.maxsizeofservicetable);
            rc = ipclocateservice_imp(arg.u.locateservice.nodeId,
                    arg.u.locateservice.srvId,
                    srv_tab,
                    &arg.u.locateservice.sizeofservicetable, 
                    arg.u.locateservice.maxsizeofservicetable,
                    &arg.lasterror);
            len = arg.u.locateservice.sizeofservicetable;
            (void)copy_to_user(arg.u.locateservice.serviceTable, 
                               srv_tab, 
                               sizeof(struct IPCSERVICE_LOCATION) * len); 
            kfree(srv_tab);
            ipcTrace(SIPCEXITLOG, IPC_LOCATESERVICE_ID);
            break;
        case CMD_IPCGETVERSION:
            rc = ipcgetversion_imp(&arg.u.getversion.version, &arg.lasterror);
            break;
        case CMD_IPCGETSTATUS:
            rc = ipcgetstatus_imp(&arg.lasterror);
            break;
        default:
            break;
    }
    /* copy arg back to user space */
    copy_to_user((void*)a, &arg, sizeof(arg));

    return rc;
}


struct file_operations ipc_fops = {    
    llseek: ipc_llseek,
    read: ipc_read,
    write: ipc_write,
    ioctl: ipc_ioctl,
    open: ipc_open,
    release: ipc_release,
};



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: fill_the_rtinfo
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Fill the run-time information buffer.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Apr2005  Motorola Inc.         Initial Creation 
* 06Apr2007  Motorola Inc.         Print socket owner in /proc/sipc/ipc_stack
* 04Jul2007  Motorola Inc.         Enhanced return value checking
* 13Jul2007  Motorola Inc.         Checked compile flag IPC_NO_LOG
* 
*****************************************************************************************/
static void fill_the_rtinfo(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int j;
    ipcSocket_t* sock = NULL;
    ipcPort_t* port = NULL;
    ipcDevice_t* device = NULL;
    ipcDRTItem_t* item = NULL;
    ipcListLink_t link = NULL;
    char *devtype = NULL;
    ipcServiceId_t sid;

/*--------------------------------------- CODE -----------------------------------------*/
    ipcStackReport("=========================================================\n");
    ipcStackReport("NodeId=%d, ipcAddress=%d\n", ipcStack_glb.nodeId, 
            ipcStack_glb.ipcAddr);
    ipcStackReport("Version=%X, MODE=%s\n", 
    		IPC_VERSION,
#ifdef IPC_RELEASE
		"RELEASE"
#else
		"DEBUG"
#endif		
		);

    ipcStackReport("=================<<<< DEBUG FLAGS  >>>>==================\n");
#ifdef IPC_NO_LOG
    ipcStackReport("Log is disabled\n");
#else
    for (j = 0; j < sizeof(valid_items) / sizeof(*valid_items) - 1; j++)
    {
        ipcStackReport("%-28s %s\n", valid_items[j].name,
                ipcLogIsEnabled(valid_items[j].value)? "on": "off");
    }
#endif /* IPC_NO_LOG */
    
    /* log router information */
    ipcStackReport("=================<<<< ROUTER INFO >>>>===================\n");
    ipcStackReport("HigherPeerAddr: %d\n", ipcStack_glb.higherPeerAddr);
    ipcStackReport("++++ Ports:\n");
    for ( j = 0; j < ipcStack_glb.numOpenPorts; j++)
    {
        port = &ipcStack_glb.ports[j];
        if (port->deviceType == IPC_DEVICE_SHARED_MEMORY)
        {
            ipcStackReport("| -> Port %d: deviceType=MU, addrRange=[%d-%d]\n", j,
                ipcStack_glb.portToIpcAddrRangeTable[j].lowAddr,
                ipcStack_glb.portToIpcAddrRangeTable[j].highAddr);
        }
        else
        {
            device = (ipcDevice_t*)port->device;
            if (NULL != device)
            {
                switch (port->deviceType)
                {
                    case IPC_DEVICE_USB:
                    devtype = "USB";
                    break;
                case IPC_DEVICE_EXT:
                    devtype = "EXT";
                    break;
                default:
                    devtype = "UNKNOWN";
                }
                
                ipcStackReport("| -> Port %d: deviceType=%s, bandWidth=%d,"
                               "channelNum=%d, "
                               " frameRate=%d, addrRange=[%d-%d]\n", j, 
                               devtype,
                               device->totalBandwidth,
                               device->tx.channelList.size,
                               device->framesPerSecond,
                               ipcStack_glb.portToIpcAddrRangeTable[j].lowAddr,
                               ipcStack_glb.portToIpcAddrRangeTable[j].highAddr);
            }
            else
            {
                ipcError(IPCLOG_GENERAL, ("========> device is NULL!\n"));
            }
        }
    } 
    
    /* log session information */
    ipcStackReport("=================<<<< SESSION INFO >>>>==================\n");
    ipcStackReport("State: %d\n", ipcSession_glb.state);
    /* log queue size for every socket */
    if (LOCK(SrvIDTable) < 0)
    {
        return;
    }
    ipcStackReport("++++ Data packets in each socket:\n");

    for (ipcHashResetIterator(&ipcSession_glb.local_srvid_tab);
         NULL != (sock = (ipcSocket_t*)ipcHashGetNext(&ipcSession_glb.local_srvid_tab));)
    {
        while (sock)
        {            
            ipcStackReport("| -> Sock[%u (0x%x %c%c), %s]: %s, %u packets pending, queueLen is %u\n",
                sock->srvId, 
                sock->srvId,
                (sock->srvId & IPC_SRVID_P_BIT) ? 'P':'U',
                (sock->srvId & IPC_SRVID_M_BIT) ? 'M':' ',
                sock->owner,
                sock->isCOSocket? "CO": "CL",
                sock->packetQueue.currentSize, sock->packetQueue.maxQueueSize);
            sock = sock->next;
        }
    }
    UNLOCK(SrvIDTable);
    ipcStackReport("++++ DRT table:\n");

    if (LOCK(DRTTable) < 0)
    {
        return;
    }
    /* TODO: Dump the DRT table */
    for (item = (ipcDRTItem_t*)ipcSession_glb.drt.head;
         item != NULL;
         item = (ipcDRTItem_t*)item->next)
    {
        ipcStackReport("| -> Node %u[%u]: ", item->node_id, item->ipcaddr);
        for (link = item->srvId_list.head;
                link != NULL;
                link = link->next)
        {
            sid = ipcCastPtr(link->data, ipcServiceId_t);
            ipcStackReport("%u (0x%x %c%c), ", 
                sid, sid, 
                (sid & IPC_SRVID_P_BIT) ? 'P':'U',
                (sid & IPC_SRVID_M_BIT) ? 'M':' ');
        }
        ipcStackReport("\n");
    }
    UNLOCK(DRTTable);
    ipcStackReport("=========================================================\n");
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: stack_proc_read
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Function to provide status output for /proc/sipc/ipc_stack
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Apr2006  Motorola Inc.         Enable runtime log config 
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
*
*****************************************************************************************/
static ssize_t stack_proc_read(struct file* file, 
        char* buf,  /*  user space buffer   */
        size_t len, /*  length of buffer    */
        loff_t* offset )    
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    if (test_and_set_bit(0, &ipc_stack_info.mutex))
    {
        return -EBUSY;
    }
    
    
    if (*offset == 0) 
    {
        fill_the_rtinfo();
    }

    rc = getProcData(&ipc_stack_info, buf, len, (unsigned int)(*offset));
    if (rc >= 0)
    {
        *offset += rc;
    }
    else
    {
        rc = 0;
    }

    clear_bit(0, &ipc_stack_info.mutex);
    return rc;
}

/* macros defined for split command string */
#define is_delimiter(c) (((c) == '\t') || ((c) == ' ') || ((c) == '\n') || ((c) == '\r') || ((c) == 0))

#define bypass_delimiter(p) \
    do { while (is_delimiter(*p) && (*p != 0) ) p++; } while (0);

#define bypass_word(p) \
    do { while ((! is_delimiter(*p)) && (*p != 0) ) p++;} while (0);


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: split_commands
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Function to parse input commands into array
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Apr2006  Motorola Inc.         Enable runtime log config 
*
*****************************************************************************************/
static int split_commands(char* buffer, char* argv[], int max_arg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* p = buffer;
    int n = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    while (*p != 0)
    {
        bypass_delimiter(p);
        argv[n ++] = p;        
    bypass_word(p);
    if(*p != 0)
    {
            * p = 0;
            p++;        
        }
        if (n >= max_arg) 
        {
            break;
        }
    }
    return  n;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: set_stack_rt_param
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Function to enable or disable debug flag bits
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Apr2006  Motorola Inc.         Enable runtime log config 
* 13Jul2007  Motorola Inc.         Checked compile flag IPC_NO_LOG
*
*****************************************************************************************/
static void set_stack_rt_param(const char *name, const char *value)
{
#ifdef IPC_NO_LOG
    return;
#else
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    int found = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    for (i = 0; i < sizeof(valid_items)/sizeof(*valid_items); i++)
    {
        if (strcmp(name, valid_items[i].name) == 0)
        {
            if (strcmp(value, "on") == 0)
            {
                ipcLogEnable(valid_items[i].value);
            }
            else if (strcmp(value, "off") == 0)
            {
                ipcLogDisable(valid_items[i].value);
            }
            else 
            {
                ipcError(IPCLOG_GENERAL, ("ipc_stack proc:invalid value: %s=%s\n", name, value));
            }
            found = 1;
            break;
        } 
#ifdef HAVE_PHYSICAL_SHM
        else if(strcmp(name, "bplog") == 0)
        {
            i = atoi(value);
            muWrite(MU_CHANNEL_LOGFLAG, i);            
            printk(KERN_ALERT "SIPCLOG: write bplog flag %x to bp \n", i);
            found = 1;
            break;
        }
#endif /* HAVE_PHYSICAL_SHM */
    }
    if(found == 0)
    {
        ipcError(IPCLOG_GENERAL, ("ipc_stack proc: unknown opt: %s=%s\n", name, value));        
    }
#endif /* IPC_NO_LOG */
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: stack_proc_write
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Function to enable config input from /proc/sipc/ipc_stack
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Apr2006  Motorola Inc.         Enable runtime log config 
* 25May2007  Motorola Inc.         Add support for dump memory pools
*
*****************************************************************************************/
static ssize_t stack_proc_write(struct file* file,
        const char * buf,
        size_t len,
        loff_t* offset)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char *kbuf;
    int argc;
    char *argv[10];
    int i;
/*--------------------------------------- CODE -----------------------------------------*/
    kbuf = kmalloc((len + 1), GFP_KERNEL);
    if (!kbuf)
    {
        ipcError(IPCLOG_GENERAL, ("kmalloc failed!\n"));
        return -EFAULT;
    }

    copy_from_user(kbuf, buf, len);
    kbuf[len]=0;
    
    argc= split_commands(kbuf, argv, 10);
    for(i = 0; i < argc; i++)
    {        
        if(strcmp(argv[i], "-h") == 0 )
        {
            ipcLogFunc("\nSIPC proc config usage: echo item_name item_value > /proc/sipc/ipc_stack \n");
            ipcLogFunc("Example: echo bplog 0 > /proc/sipc/ipc_stack : set bp log mask to zero\n");
            ipcLogFunc("Example: echo IPCLOG_LEVEL_DEBUG on > /proc/sipc/ipc_stack : enable IPCLOG_LEVEL_DEBUG\n");
            break;
        } 
        else if (strcmp(argv[i], "dumpmem") == 0)
        {
            ipcLogFunc("User (%s) asked to dump IPC shared memory\n", current->comm);
            dump_virtual_shm();
            dump_physical_shm();
        }
        else if(i < argc - 1) 
        {
            set_stack_rt_param(argv[i], argv[i+1]);
            i++;
        }
    }

    kfree(kbuf);
    return len;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_init
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initialize the ipc kernel module.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 16Jun2005  Motorola Inc.         Physical Shm & Virtural Shm Integration
* 31Aug2005  Motorola Inc.         Checked the stack_entry value
* 11Jan2006  Motorola Inc.         Call ipcOpenMemLock() to init the  lock for physical shared memory
* 14Jul2006  Motorola Inc.         Use kernel variables to determine physical shared memory address and size
* 11Oct2006  Motorola Inc.         rollback to 4-bytes boundry
* 17Nov2006  Motorola Inc.         Created char device when opening
* 29Nov2006  Motorola Inc.         removed udev support
* 17Jan2007  Motorola Inc.         Moved initialization here
* 23Jan2007  Motorola Inc.         Removed socket device
* 19Jan2007  Motorola Inc.         Changed CONFIG_ARCH_ZEUS to CONFIG_ARCH_MXC91131 
* 20Feb2007  Motorola Inc.         Call ipcInitMemory for common allocator
* 13Mar2007  Motorola Inc.         Initialize muStatistics global for MU read/write statistics
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
*
*****************************************************************************************/
static int __init ipc_init(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int phyShmOffset = 0;    
/*--------------------------------------- CODE -----------------------------------------*/
#ifdef START_UP_TIME
    ulStartUpTime = jiffies;
    printk(KERN_ALERT "ipc_init time = 0\n");
#endif
    ipc_major = register_chrdev(ipc_major, IPCCHRNAME, &ipc_fops);
    if (ipc_major < 0) 
    {
        ipcError(IPCLOG_GENERAL, ("IPC driver can not register chrdev\n"));
        return ipc_major;
    }
    
    devfs_mk_cdev(MKDEV(ipc_major, IPC_MINOR), S_IFCHR | S_IRUGO | S_IWUGO, "ipc");
    devfs_mk_cdev(MKDEV(ipc_major, IPCMEM_MINOR), S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP, "ipcmem");

    if (register_sipc_protocol() < 0)
    {
        ipcError(IPCLOG_GENERAL,
                 ("ipc_init: Failed to register sipc protocol!\n"));
        goto fail2;
    }

    initProcElem(&ipc_virt_shm_map);
    initProcElem(&ipc_phys_shm_map);
    initProcElem(&ipc_stack_info);
    ipc_virt_shm_map.mutex = ipc_phys_shm_map.mutex = ipc_stack_info.mutex = 0;

    /* init virtual shared memory */
    vsize = (vsize + 3) & ~3;
    vsize <<= 10;

    if (ipcShmInit(vsize) < 0)
    {
        goto fail2;
    }

    ipcDebugLog(IPCLOG_GENERAL, ("=====> Virtual shared memory init OK!"));

    ipcInitMemory();

#ifdef HAVE_PHYSICAL_SHM
#if (defined CONFIG_ARCH_MXC91131) || (defined CONFIG_ARCH_ZEUS)
    if (paddr == 0 && psize == 0)
    {
        /* the pshm address and size are not specified by the module parameters
         * use the kernel variables to determine pshm address and size 
         */
         
        /* round paddr to the next 4 byte boundary */
        paddr = (aponly_end_addr + 3) & (~3);
        psize = (bp_start_addr - paddr);

        ipcPrint(IPCLOG_GENERAL, ("aponly_end_addr=0x%08x\n", (unsigned int)aponly_end_addr));
        ipcPrint(IPCLOG_GENERAL, ("bp_start_addr=0x%08x\n", (unsigned int)bp_start_addr));
    }
#endif
    ipcPrint(IPCLOG_GENERAL, ("paddr=0x%08x\n", (unsigned int)paddr));
    ipcPrint(IPCLOG_GENERAL, ("psize=0x%08x (%d)\n", (unsigned int)psize, (unsigned int)psize));

    /* init physical shared memory */
    physical_shm_base = ioremap(paddr, psize);
    phyShmOffset = (char *)physical_shm_base - (char *)paddr;
    
    /* init address used by MU driver for diagnostic counters (it uses the
     * bp_boot_info section right before the start of BP memory) */
    muStatistics = (void *)((int)physical_shm_base + psize - sizeof(muStatistics_t));

    muShmInit();
    initPlatformSharedMemory(phyShmOffset, (void *)paddr, psize, NULL);
#else
    phyShmOffset = 0;
#endif
    
    /* Init IPC Stack */
    ipcStackConfig(runAsServer, nodeId, 0);
    if (!ipcStackInit())
    {
        ipcError(IPCLOG_GENERAL, ("Error initializing IPC Stack\n"));
        goto fail1;
    }

    if (!ipcPlatformInit())
    {
        /* Main thread failed to start */
        ipcStack_glb.state = IPC_STACK_STATE_NULL;
        ipcStackTerminate();
        goto fail1;
    }

    ipcDebugLog(IPCLOG_GENERAL, ("ipcStackInit done\n"));


    if (NULL == (ipc_root = proc_mkdir("sipc", NULL)))
    {
        ipcError(IPCLOG_GENERAL, ("create /proc/sipc failed!\n"));
    }

    if (NULL == (ipc_vshm_map = create_proc_entry("ipc_vshm_map",
                    S_IFREG | S_IRUGO | S_IWUSR,
                    ipc_root)))
    {
        ipcError(IPCLOG_GENERAL, ("create /proc/sipc/ipc_vshm_map failed!\n"));
    }
    else
    {
        ipc_vshm_map->size = 40l;
        ipc_vshm_map->proc_fops = &ipc_vshm_proc_ops;
    }
    
    if (NULL == (stack_entry = create_proc_entry(
                    "ipc_stack",
                    S_IFREG | S_IRUGO | S_IWUSR,
                    ipc_root)))
    {
        ipcError(IPCLOG_GENERAL, ("Create ipc proc file system entry failed\n"));
    }
    else
    {
        stack_entry->size = 80l;
        stack_entry->proc_fops = &stack_proc_ops;
    }
    
    if (NULL == (ipc_pshm_map = create_proc_entry("ipc_pshm_map",
                    S_IFREG | S_IRUGO | S_IWUSR,
                    ipc_root)))
    {
        ipcError(IPCLOG_GENERAL, ("create /proc/sipc/ipc_pshm_map failed!\n"));
    }
    else
    {
        ipc_pshm_map->size = 80l;
        ipc_pshm_map->proc_fops = &ipc_pshm_proc_ops;
    }

    return 0; /* succeed */

fail1:
    ipcShmFinalize();
fail2:
    unregister_sipc_protocol();
    devfs_remove("ipc");
    devfs_remove("ipcmem");

    unregister_chrdev(ipc_major, IPCCHRNAME);
    return -1;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_cleanup
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Clean up the ipc module
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2005  Motorola Inc.         Initial Creation
* 06Feb2006  Motorola Inc.         Call muShmExit to remove the mu driver callbacks
* 20Jul2006  Motorola Inc.         Close multicore semaphore
* 17Nov2006  Motorola Inc.         Removed char device when closing
* 29Nov2006  Motorola Inc.         Removed udev support 
*
*****************************************************************************************/
static void __exit ipc_cleanup(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    unregister_sipc_protocol();
    devfs_remove("ipc");
    devfs_remove("ipcmem");

    unregister_chrdev(ipc_major, IPCCHRNAME);

    ipcDebugLog(IPCLOG_GENERAL, ("done unregister_chrdev!\n"));

    ipcStackTerminate();

    if (NULL != stack_entry)
    {
        remove_proc_entry("ipc_stack", ipc_root);
    }

    if (NULL != ipc_vshm_map)
    {
        remove_proc_entry("ipc_vshm_map", ipc_root);
    }

    if (NULL != ipc_pshm_map)
    {
        remove_proc_entry("ipc_pshm_map", ipc_root);
    }

    if (NULL != ipc_root)
    {
        remove_proc_entry("sipc", NULL);
    }

    ipcDebugLog(IPCLOG_GENERAL, ("Done destroying the ipc stack\n"));

#ifdef HAVE_PHYSICAL_SHM

    muShmExit();
    if (NULL != physical_shm_base)
    {
        ipcMulticoreSemaClose();
        iounmap(physical_shm_base);
        physical_shm_base = NULL;
        ipcDebugLog(IPCLOG_GENERAL, ("----------> done iounmap(physical_shm_base)\n"));
    }
#endif
    /* destroy virtual shared memory */
    ipcShmFinalize();

    ipcDebugLog(IPCLOG_GENERAL, ("IPC ipc_cleanup return \n"));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_vshm_proc_read
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
*
*****************************************************************************************/
ssize_t ipc_vshm_proc_read(struct file* file, 
        char* buf,  /*  user space buffer   */
        size_t len, /*  length of buffer    */
        loff_t* offset )  
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc; 
/*--------------------------------------- CODE -----------------------------------------*/
    if (test_and_set_bit(0, &ipc_virt_shm_map.mutex))
    {
        return -EBUSY;
    }
    
    if (*offset == 0)
    {
        ipcShmDump();
    }
    
    rc = getProcData(&ipc_virt_shm_map, buf, len, (unsigned int)(*offset));
    
    if (rc >= 0)
    {
        *offset += rc;
    }
    else
    {
        rc = 0;
    }

    clear_bit(0, &ipc_virt_shm_map.mutex);
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipc_pshm_proc_read
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
*
*****************************************************************************************/
ssize_t ipc_pshm_proc_read(struct file* file, 
        char* buf,  /*  user space buffer   */
        size_t len, /*  length of buffer    */
        loff_t* offset )    
{
#ifdef HAVE_PHYSICAL_SHM
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc; 
/*--------------------------------------- CODE -----------------------------------------*/

    if (test_and_set_bit(0, &ipc_phys_shm_map.mutex))
    {
        return -EBUSY;
    }
    
    if (*offset == 0) 
    {
        if (ipcPhyShmPoolWalk() < 0)
        {
            ipcError(IPCLOG_GENERAL, ("------> get information from physical shm failed!\n"));
        }
    }
    
    rc = getProcData(&ipc_phys_shm_map, buf, len, (unsigned int)(*offset));
    
    if (rc >= 0)
    {
        *offset += rc;
    }
    else
    {
        rc = 0;
    }

    clear_bit(0, &ipc_phys_shm_map.mutex);
    return rc;
#else
    return 0;
#endif
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: dump_physical_shm
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*       This function is used to dump physical shared memory to a file.
*
*   PARAMETER(S):
*       void
*
*   RETURN: 
*       void
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2007  Motorola Inc.         Initial Creation
* 25May2007  Motorola Inc.         Make this function also visible in release versions
*
*****************************************************************************************/
void dump_physical_shm(void)
{
#if (defined HAVE_PHYSICAL_SHM) 
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char filename[256];
/*--------------------------------------- CODE -----------------------------------------*/
    if ((physical_shm_base != 0) && (psize != 0))
    {
        snprintf(filename, 256, "%s/pshm_%lu", dump_path, psize);
        if (file_exists(filename))
        {
            ipcLogFunc("The physical shared memory dump file %s already exists, quit\n", filename);
        }
        else
        {
            write_to_file(filename, (char*)physical_shm_base, psize);
        }
    }
#endif
}

#ifndef IPC_NO_LOG

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcHandleMemoryError
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*       This function is used handle memory error event
*
*   PARAMETER(S):
*       void
*
*   RETURN: 
*       void
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcHandleMemoryError(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
#ifndef IPC_RELEASE
    dump_virtual_shm();
    dump_physical_shm();
#endif
    LOG_BAD_BUF(IPCEBADBUF);
    force_sig(SIGQUIT, current);
}

#endif /* ! IPC_NO_LOG */

ipc_ulong_param(paddr);
ipc_ulong_param(psize);
ipc_ulong_param(vaddr);
ipc_ulong_param(vsize);

ipc_int_param(ipc_major);
ipc_int_param(runAsServer);
ipc_int_param(nodeId);
ipc_int_param(debug_bp);

module_init(ipc_init);
module_exit(ipc_cleanup);

