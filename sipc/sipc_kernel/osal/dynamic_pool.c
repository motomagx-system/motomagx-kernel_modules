/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2006 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : dynamic_pool.c
*   FUNCTION NAME(S): dump_virtual_shm
*                     ipcShmAlloc
*                     ipcShmCreatePool
*                     ipcShmDestroyPool
*                     ipcShmDump
*                     ipcShmFinalize
*                     ipcShmFindPoolFromPtr
*                     ipcShmFree
*                     ipcShmInit
*                     ipcShmOffsetToPtr
*                     ipcShmPtrToOffset
*                     ipcShmShrinkPools
*                     ipcShrinkTimerFunc
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation
* 13Jul2006  Motorola Inc.         Flush TLB in ARM
* 26Dec2006  Motorola Inc.         Remove last_alloc_tick
* 30Dec2006  Motorola Inc.         Moved pool size definition to header Fixed pool search algorithm bug
* 29Jan2007  Motorola Inc.         Header files change for GPL
* 23Feb2007  Motorola Inc.         Updated for common allocator
* 24Apr2007  Motorola Inc.         Added function to dump all virtual shared memory pools
* 25May2007  Motorola Inc.         Added watermark of allocated bytes
* 05Jul2007  Motorola Inc.         Checked lock failure
* 13Jul2007  Motorola Inc.         Checked compile condition HAVE_DYNAMIC_POOL
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_osal.h"
#include "ipc_osal_util.h"
#include "ipc_defs.h"
#include "dynamic_pool.h"
#include "ipc_list.h"
#include "ipclog.h"
#include <asm/tlb.h>

extern ulong vaddr;
#ifdef HAVE_DYNAMIC_POOL
/*-------------------------------------- CONSTANTS -------------------------------------*/
#define SHRINK_TIMER_INTERVAL 6000
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/

typedef struct ipcAllocator_t
{
    /* list of all pools, not sorted. It is currently not used, but when ported to
     * IPC, it will be sorted by offset in shared memory, ascending, which will
     * accelerate search of a pool, giving a pointer */
    ipcList_t pools_by_offset;
    
    /* also a list of all pools, sorted by pool size, ascending, when ipcShmAlloc
     * is called, the allocator will try to allocate from the smallest pool first
     */
    ipcList_t pools_by_size;

    /* the size of all the existing pools */
    unsigned long total_pool_size;

    /* the limit of total_pool_size */
    unsigned long size_limit;
    
    /* the number of empty pools, used for garbage collection */
    unsigned long num_empty_pools;

    /* the number of currently used bytes */
    unsigned long num_used_bytes;

    /* the high watermark of used bytes */
    unsigned long watermark;

    /* mutex to protect memory pool operations */
    ipcMutexHandle_t mutex;

    /* a timer to shrink memory pools */
    ipcTimerHandle_t shrinkTimer;

} ipcAllocator_t;


/*------------------------------------- STATIC DATA ------------------------------------*/
static ipcAllocator_t allocator;


/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
void ipcShmShrinkPools(void);
void ipcShrinkTimerFunc(void*);

/*--------------------------------------- MACROS ---------------------------------------*/
#define UP_ALIGN(value,align) \
    (((value) + ((align) - 1)) & ~((align) - 1))

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initialize the allocator.
*   
*   PARAMETER(S):
*       size_limit: limit of the total size of all the pools
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 26Dec2006  Motorola Inc.         Remove last_alloc_tick
* 23Feb2007  Motorola Inc.         Updated for common allocator
*
*****************************************************************************************/
int ipcShmInit(unsigned long size_limit)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    void* ptr = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcListInit(&allocator.pools_by_offset);
    ipcListInit(&allocator.pools_by_size);
    allocator.total_pool_size = 0u;
    allocator.size_limit = size_limit;
    allocator.num_empty_pools = 0u;
    allocator.num_used_bytes = 0u;
    allocator.watermark = 0u;
    if (ipcOsalMutexOpen(&allocator.mutex) < 0)
    {
        return IPC_ERROR;
    }
    if (ipcOsalTimerOpen(&allocator.shrinkTimer) < 0)
    {
        ipcOsalMutexClose(allocator.mutex);
        return IPC_ERROR;
    }
    
    if (ipcOsalTimerInit(allocator.shrinkTimer, SHRINK_TIMER_INTERVAL,
                         ipcShrinkTimerFunc, NULL) < 0)
    {
        ipcOsalMutexClose(allocator.mutex);
        ipcOsalTimerClose(allocator.shrinkTimer);
        return IPC_ERROR;
    }
    /* create the default memory pool */
    ptr = ipcShmAlloc(1, 0);
    if (ptr != NULL)
    {
        ipcShmFree(ptr);
    }

    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmFinalize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Finalize the allocator.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void ipcShmFinalize(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t head = NULL;
    
/*--------------------------------------- CODE -----------------------------------------*/
    ipcOsalTimerClose(allocator.shrinkTimer);
    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Failed to lock memory pool, quit\n"));
        return;
    }
    while (NULL != (head = ipcListRemoveHead(&allocator.pools_by_offset)))
    {
        ipcShmDestroyPool((ipcShmPool_t*)head);
    }
    ipcOsalMutexClose(allocator.mutex);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmDump
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Dump the shared memory pools of the allocator.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void ipcShmDump(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t curr;
    int i = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcVirtShmReport("Failed to lock memory pool, quit\n");
        return;
    }
    ipcVirtShmReport("---- Built with dynamic pool support, "
                     "%d pools now, %u bytes totally----\n",
            allocator.pools_by_offset.size, allocator.size_limit);
    ipcVirtShmReport("---- Currently %lu bytes used, memory usage watermark is %lu bytes\n",
            allocator.num_used_bytes, allocator.watermark);
    for (curr = allocator.pools_by_offset.head; curr != NULL; curr = curr->next)
    {
        ipcShmPool_t* pool = (ipcShmPool_t*)(curr);
        ipcVirtShmReport("---- Pool[%d] addr=%p, offset=%lu, size=%lu ----\n",
                ++i, pool, pool->offset, pool->pool_size);
        dpccpoolwalk(pool->dpccshminfo);
    }
    ipcOsalMutexRelease(allocator.mutex);
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmFindPoolFromPtr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Find a pool by a pointer.
*   
*   PARAMETER(S):
*       ptr: the pointer to lookup
*       
*   RETURN: pointer of the pool in which the pointer belongs
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
ipcShmPool_t* ipcShmFindPoolFromPtr(char* ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t curr;
    ipcShmPool_t* poolToReturn = NULL;
    
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcError(IPCLOG_GENERAL, 
                ("Failed to lock allocator, quit\n"));
        return NULL;
    }
    for (curr = allocator.pools_by_offset.head; curr != NULL; curr = curr->next)
    {
        ipcShmPool_t* pool = (ipcShmPool_t*)curr;
        if ((ptr >= pool->dpccshminfo->vir_addr) &&
            (ptr < pool->dpccshminfo->vir_addr + pool->dpccshminfo->size))
        {
            poolToReturn = pool;
            break;
        }
    }
    ipcOsalMutexRelease(allocator.mutex);
    return poolToReturn;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmOffsetToPtr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Translate an offset to a pointer.
*   
*   PARAMETER(S):
*       offset: the offset in the shared memory range
*       
*   RETURN: the address of the offset
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
char* ipcShmOffsetToPtr(unsigned long offset)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t curr;
    char* ptrToReturn = NULL;
    
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
        return NULL;
    }
    for (curr = allocator.pools_by_offset.head; curr != NULL; curr = curr->next)
    {
        ipcShmPool_t* pool = (ipcShmPool_t*)curr;
        if ((offset >= pool->offset) &&
            (offset < pool->offset + pool->pool_size))
        {
            ptrToReturn = (char*)pool + (offset - pool->offset);
            break;
        }
    }
    ipcOsalMutexRelease(allocator.mutex);
    return ptrToReturn;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmAlloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocate a shared memory block.
*   
*   PARAMETER(S):
*       size: size of the block to allocate
*       flags: ignored (required for common allocator interface)
*       
*   RETURN: the address of the memory block
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation
* 13Jul2006  Motorola Inc.         Shrink if no memory
* 26Dec2006  Motorola Inc.         Remove last_alloc_tick
* 30Dec2006  Motorola Inc.         Fixed search algorithm bug
* 23Feb2007  Motorola Inc.         Updated params for common allocator
* 25May2007  Motorola Inc.         Added watermark of allocated bytes
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void* ipcShmAlloc(unsigned int size, unsigned int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev, curr;
    ipcListLink_t offset_prev = NULL;
    ipcShmPool_t* pool = NULL;
    ipcShmPool_t* temp_pool = NULL;
    char* ptr = NULL;
    unsigned long new_pool_size = 0;
    unsigned long start_offset = 0;
    unsigned long room = 0;
    unsigned long size_used = 0;
    
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
        return NULL;
    }
    for (curr = allocator.pools_by_size.head; curr != NULL; curr = curr->next)
    {
        pool = NEXT2_TO_POOL(curr);
        
        size_used = pool->dpccshminfo->size_used;
        ptr = dpccshmalloc(pool->dpccshminfo, size);
        if (ptr != NULL)
        {
            if (1 == ++pool->num_active_blocks)
            {
                /* An empty pool gets non-empty */
                --allocator.num_empty_pools;
            }

            allocator.num_used_bytes -= size_used;
            allocator.num_used_bytes += pool->dpccshminfo->size_used;
            if (allocator.num_used_bytes > allocator.watermark)
            {
                allocator.watermark = allocator.num_used_bytes;
            }
            ipcOsalMutexRelease(allocator.mutex);
            return (void *)ptr;
        }
    }
    
    /* can't be allocated, then create a new pool */

    /* calculate the size of the new pool to be created */
    new_pool_size = size + 3 * sizeof(dpcc_memblk_t) + sizeof(ipcShmPool_t);
    if (new_pool_size < MIN_POOL_SIZE)
    {
        new_pool_size = MIN_POOL_SIZE;
    }
    new_pool_size = UP_ALIGN(new_pool_size, PAGE_SIZE);
    if (new_pool_size > MAX_POOL_SIZE)
    {
        ipcOsalMutexRelease(allocator.mutex);
        return NULL;
    }
    if (new_pool_size + allocator.total_pool_size > allocator.size_limit)
    {
        /* try to free all unused memory pools, and try again */
        ipcShmShrinkPools();

        /* if still no room, return error */
        if (new_pool_size + allocator.total_pool_size > allocator.size_limit)
        {
            ipcOsalMutexRelease(allocator.mutex);
            return NULL;
        }
    }
    
    while (room < new_pool_size)
    {
        /* find a hole in the address range for the new block, try the end first */
        offset_prev = allocator.pools_by_offset.tail;
        temp_pool = (ipcShmPool_t*)offset_prev;

        if (temp_pool == NULL)
        {
            start_offset = 0;
            room = allocator.size_limit - start_offset;
        }
        else
        {
            start_offset = temp_pool->offset + temp_pool->pool_size;
            start_offset = UP_ALIGN(start_offset, PAGE_SIZE);
            room = allocator.size_limit - start_offset;

            if (room < new_pool_size)
            {
                /* we can't append the new block to the end of allocator.pools_by_offset,
                 * perform a full search in the address range */
                for (start_offset = 0, ipcListFirstPair(allocator.pools_by_offset, offset_prev, curr);
                        curr != NULL;
                        ipcListNextPair(offset_prev, curr))
                {
                    temp_pool = (ipcShmPool_t*)curr;
                    room = temp_pool->offset - start_offset;

                    if (room >= new_pool_size)
                    {
                        break;
                    }

                    start_offset = temp_pool->offset + temp_pool->pool_size;
                    start_offset = UP_ALIGN(start_offset, PAGE_SIZE);
                }
            }
        }
        if (room < new_pool_size)
        {
            if (allocator.num_empty_pools > 0)
            {
                ipcShmShrinkPools();
                continue;
            }
        }
        break;
    }
    /* if there's still not enough room for the new pool */
    if (room < new_pool_size)
    {
        /* no room for the new pool */
        ipcOsalMutexRelease(allocator.mutex);
        return NULL;
    }
    /* create the pool */
    pool = ipcShmCreatePool(start_offset, new_pool_size);
    if (pool == NULL)
    {
        ipcOsalMutexRelease(allocator.mutex);
        return NULL;
    }
    ptr = dpccshmalloc(pool->dpccshminfo, size);
    if (ptr == NULL)
    {
        /* we still can't allocate from the newly created pool? */
        ipcOsalMutexRelease(allocator.mutex);
        ipcShmDestroyPool(pool);
        return NULL;
    }
    allocator.total_pool_size += pool->pool_size;
    pool->num_active_blocks++;
    allocator.num_used_bytes += pool->dpccshminfo->size_used;
    if (allocator.num_used_bytes > allocator.watermark)
    {
        allocator.watermark = allocator.num_used_bytes;
    }

    /* insert the pool to allocator.pools_by_size */
    ipcListInsertLink(&allocator.pools_by_offset, (ipcListLink_t)pool, offset_prev);

    for (ipcListFirstPair(allocator.pools_by_size, prev, curr); curr != NULL; ipcListNextPair(prev, curr))
    {
        ipcShmPool_t* temp_pool = NEXT2_TO_POOL(curr);
        if (temp_pool->pool_size > pool->pool_size)
        {
            break;
        }
    }
    ipcListInsertLink(&allocator.pools_by_size, POOL_TO_NEXT2(pool), prev);

    ipcOsalMutexRelease(allocator.mutex);
    return (void *)ptr;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmFree
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Free a pointer.
*   
*   PARAMETER(S):
*       ptr: the pointer to free
*          
*   RETURN: 0 if succeeded, -1 otherwise.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 26Dec2006  Motorola Inc.         Remove last_alloc_tick
* 23Feb2007  Motorola Inc.         Updated for common allocator
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
int ipcShmFree(void* ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcShmPool_t* pool = NULL;
    BOOL poolTurnEmpty = FALSE;
    unsigned long size_used = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    pool = ipcShmFindPoolFromPtr((char *)ptr);

    if (pool == NULL)
    {
        return -1;
    }

    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
        return IPCEBADBUF;
    }
    size_used = pool->dpccshminfo->size_used;
    if (0 == dpccshmfree((char *)ptr))
    {
        if (--pool->num_active_blocks == 0)
        {
            poolTurnEmpty = TRUE;
            /* the pool gets empty now */
            ++allocator.num_empty_pools;
        }
        allocator.num_used_bytes -= size_used;
        allocator.num_used_bytes += pool->dpccshminfo->size_used;
    }
    else
    {
        ipcOsalMutexRelease(allocator.mutex);
        return -1;
    }
    ipcOsalMutexRelease(allocator.mutex);
    if (poolTurnEmpty && (pool != (ipcShmPool_t*)allocator.pools_by_offset.head))
    {
        if (!ipcOsalTimerIsActive(allocator.shrinkTimer))
        {
            ipcOsalTimerStart(allocator.shrinkTimer);
        }
    }
    return 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmShrinkPools
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   shrink the memory pools if necessary.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation
* 13Jul2006  Motorola Inc.         Flush TLB on ARM
* 26Dec2006  Motorola Inc.         Remove last_alloc_tick 
*
*****************************************************************************************/
void ipcShmShrinkPools(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcShmPool_t* pool = NULL;
    ipcListLink_t prev, curr;
    int num_removed = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    if (allocator.num_empty_pools != 0)
    {
        allocator.num_empty_pools = 0;

        for (ipcListFirstPair(allocator.pools_by_size, prev, curr); curr != NULL;)
        {
            pool = NEXT2_TO_POOL(curr);

            if ((pool != (ipcShmPool_t*)allocator.pools_by_offset.head) &&
                (pool->num_active_blocks == 0))
            {
                ipcListRemoveNextLink(&allocator.pools_by_size, prev);
                ipcListRemove(&allocator.pools_by_offset, pool, ipcListCompareLink);
                allocator.total_pool_size -= pool->pool_size;
                ipcShmDestroyPool(pool);
                ++num_removed;

                if (prev == NULL)
                {
                    curr = (ipcListLink_t)allocator.pools_by_size.head;
                }
                else
                {
                    curr = prev->next;
                }
            }
            else
            {
                ipcListNextPair(prev, curr);
            }
        }
    }
    if (num_removed != 0)
    {
#ifdef __ASMARM_TLB_H
        /* FIXME: we have to flush TLB manually in ARM, it seems like a bug of
         * kernel in ARM. I found a bug in my test, while I tried to create a 
         * very big pool, and all of the old pools were removed, the content in
         * the newly created pool got corrupted sometimes. Adding flush_tlb_all
         * fixed this problem. The function vfree already flushes tlb range of
         * the vma, but it seems not working, I don't know why.
         *
         * This problem only appears in ARM, i386 and uml is OK in my test.
         */
        flush_tlb_all();
#endif
        ipcDebugLog(IPCLOG_MEMTRACK, 
                ("ipcShmShrinkPools: %d pools removed\n", num_removed));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShrinkTimerFunc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is executed in a timer which is used to shrink memory pools.
*   
*   PARAMETER(S):
*       arg: not used
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void ipcShrinkTimerFunc(void* arg)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(allocator.mutex) == IPCE_OK)
    {
        ipcShmShrinkPools();
        ipcOsalMutexRelease(allocator.mutex);
    }
}

#else /* built without dynamic pool support */

typedef struct ipcAllocator_t
{
    ipcShmPool_t *pool;
    ipcMutexHandle_t mutex;

} ipcAllocator_t;

static ipcAllocator_t allocator;

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initialize the allocator.
*   
*   PARAMETER(S):
*       size_limit: limit of the total size of all the pools
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcShmInit(unsigned long size_limit)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/
    if (allocator.pool == NULL)
    {
        allocator.pool = ipcShmCreatePool(0, size_limit);
        if (allocator.pool != NULL)
        {
            if (ipcOsalMutexOpen(&allocator.mutex) < 0)
            {
                ipcError(IPCLOG_MEMTRACK,
                        ("Failed to create mutex\n"));
                ipcShmDestroyPool(allocator.pool);
                allocator.pool = NULL;
            }
            else
            {
                rc = IPC_OK;
            }
        }
        else
        {
            ipcError(IPCLOG_MEMTRACK, 
                    ("Failed to create memory pool with size %lu\n",
                     size_limit));
        }
    }
    return rc;;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmFinalize
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Finalize the allocator.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void ipcShmFinalize(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (allocator.pool != NULL)
    {
        if (ipcOsalMutexLock(allocator.mutex) < 0)
        {
            ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
            return;
        }
        ipcShmDestroyPool(allocator.pool);
        allocator.pool = NULL;
        ipcOsalMutexClose(allocator.mutex);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmDump
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Dump the shared memory pools of the allocator.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void ipcShmDump(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(allocator.mutex) < 0)
    {
        ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
        return;
    }
    dpccpoolwalk(allocator.pool->dpccshminfo);
    ipcOsalMutexRelease(allocator.mutex);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmAlloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocate a shared memory block.
*   
*   PARAMETER(S):
*       size: size of the block to allocate
*       flags: ignored (required for common allocator interface)
*       
*   RETURN: the address of the memory block
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation
* 23Feb2007  Motorola Inc.         Updated for common allocator
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
void* ipcShmAlloc(unsigned int size, unsigned int flags)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* ptr = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if (allocator.pool != NULL)
    {
        if (ipcOsalMutexLock(allocator.mutex) < 0)
        {
            ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
            return NULL;
        }
        ptr = dpccshmalloc(allocator.pool->dpccshminfo, size);
        ipcOsalMutexRelease(allocator.mutex);
    }
    return (void *)ptr;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmFree
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Free a pointer.
*   
*   PARAMETER(S):
*       ptr: the pointer to free
*          
*   RETURN: 0 if succeeded, -1 otherwise.
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation
* 23Feb2007  Motorola Inc.         Updated for common allocator
* 05Jul2007  Motorola Inc.         Checked lock failure
*
*****************************************************************************************/
int ipcShmFree(void* ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_ERROR;
/*--------------------------------------- CODE -----------------------------------------*/

    if (allocator.pool != NULL)
    {
        if (ipcOsalMutexLock(allocator.mutex) < 0)
        {
            ipcError(IPCLOG_GENERAL, ("Failed to lock allocator, quit\n"));
            return IPCEBADBUF;
        }
        rc = dpccshmfree((char *)ptr);
        ipcOsalMutexRelease(allocator.mutex);
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmFindPoolFromPtr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Find a pool by a pointer.
*   
*   PARAMETER(S):
*       ptr: the pointer to lookup
*       
*   RETURN: pointer of the pool in which the pointer belongs
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcShmPool_t* ipcShmFindPoolFromPtr(char* ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcShmPool_t* pool = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if (allocator.pool != NULL)
    {
        if ((ptr >= (char*)allocator.pool) &&
            (ptr < (char*)allocator.pool + allocator.pool->pool_size))
        {
            pool = allocator.pool;
        }
    }
    return pool; 
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmOffsetToPtr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Translate an offset to a pointer.
*   
*   PARAMETER(S):
*       offset: the offset in the shared memory range
*       
*   RETURN: the address of the offset
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 06Jun2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
char* ipcShmOffsetToPtr(unsigned long offset)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char* ptr = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

    if (allocator.pool != NULL)
    {
        if ((offset >= allocator.pool->offset) &&
            (offset < allocator.pool->offset + allocator.pool->pool_size))
        {
            ptr = (char*)allocator.pool + offset - allocator.pool->offset;
        }
    }
    return ptr;
}


#endif /* HAVE_DYNAMIC_POOL */

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmCreatePool
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Create a new shared memory pool.
*   
*   NOTE: offset MUST be aligned by page size! The page would be mapped to user
*   space, the operating system translates offset to pointer just by looking up
*   the index in a page.
*   
*   PARAMETER(S):
*       offset: The starting offset of the pool in the address range
*       size: The size of the pool to be created
*       
*   RETURN: Pointer of the newly created pool
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcShmPool_t* ipcShmCreatePool(unsigned long offset, unsigned long size)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcShmPool_t* pool = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    pool = SHM_ALLOC_BYTES(size);
    
    if (pool != NULL)
    {
        ipcDebugLog(IPCLOG_MEMTRACK, 
                ("ipcShmCreatePool: Creting pool offset=%lu, size=%lu, ptr=%p\n",
                 offset, size, pool));
        pool->next = NULL;
        pool->next2 = NULL;
        pool->num_active_blocks = 0;
        pool->offset = offset;
        pool->pool_size = size;

        pool->dpccshminfo->size = size - offsetof(ipcShmPool_t, dpccshminfo);
        pool->dpccshminfo->vir_addr = (char*)pool->dpccshminfo;
        pool->dpccshminfo->size_used = 0;

        dpccshminit(pool->dpccshminfo);
    }
    return pool;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmDestroyPool
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Destroy a shared memory pool.
*   
*   PARAMETER(S):
*       pool: the pool to destroy
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcShmDestroyPool(ipcShmPool_t* pool)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (pool != NULL)
    {
        ipcDebugLog(IPCLOG_MEMTRACK, 
                ("ipcShmDestroyPool: destroy pool %p, offset=%lu, size=%lu\n",
                 pool, pool->offset, pool->pool_size));
        SHM_FREE_MEMPOOL(pool, pool->pool_size);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmPtrToOffset
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Translate a pointer in shared memory to its offset.
*   
*   PARAMETER(S):
*       ptr: the pointer to translate
*       
*   RETURN: offset in the shared memory
*   
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2006  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
unsigned long ipcShmPtrToOffset(char* ptr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcShmPool_t* pool = NULL;
    unsigned long offset = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    pool = ipcShmFindPoolFromPtr(ptr);
    if (pool != NULL)
    {
        /* the address of pool is aligned by pool size */
        offset = pool->offset + (ptr - (char*)pool);
    }
    return offset;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: dump_virtual_shm
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Dump all virtual shared memory pools to files.
*
*   NOTE: This function doesn't attempt to lock the allocator, so it doesn't
*   guarantee thread safety. This function should be called only on emergency, such as
*   memory corruption or unexpected situations.
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2007  Motorola Inc.         Initial Creation 
* 25May2007  Motorola Inc.         Do not dump when the file already exists
* 13Jul2007  Motorola Inc.         Checked compile condition HAVE_DYNAMIC_POOL
*
*****************************************************************************************/
void dump_virtual_shm(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    char filename[256];
    ipcShmPool_t* pool = NULL;
    ipcListLink_t link = NULL;
/*--------------------------------------- CODE -----------------------------------------*/

#ifdef HAVE_DYNAMIC_POOL
    for (link = allocator.pools_by_offset.head; link != NULL; link = link->next)
#else
    link = (ipcListLink_t)(allocator.pool);
#endif
    {
        pool = (ipcShmPool_t*)link;
        snprintf(filename, 256, "%s/vshm_0x%lx_%lu", dump_path, vaddr + pool->offset, pool->pool_size);
        if (file_exists(filename))
        {
            ipcLogFunc("The virtual shared memory dump file %s already exists, quit\n", filename);
        }
        else
        {
            write_to_file(filename, (char*)pool, pool->pool_size);
        }
    }
}


