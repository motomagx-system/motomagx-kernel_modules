/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 1994, 1996, 1998, 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : dpccshmalloc.c
*   FUNCTION NAME(S): dpccpoolwalk
*                     dpccshmalloc
*                     dpccshmfree
*                     dpccshminit
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   Functions that manage allocation and de-allocation of
*   shared memory related buffers
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 20Feb1994  Motorola Inc.         Initial Creation
* 20Feb2007  Motorola Inc.         Updated for common SIPC allocator (removed usage field)
* 02Apr2007  Motorola Inc.         Unify memory log format
* 25May2007  Motorola Inc.         Print all error messages with IPCLOG_MEMTRACK
* 05Jul2007  Motorola Inc.         Removed invocation of ipcSMGetTailMark
* 19Jul2007  Motorola Inc.         Included ipc_maro_defs.h
*
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
/*
 * The algorithm used is taken from "Data Structures" by Reingold and Hansen,
 * algorithms 6.6(b) and 6.7.
 */

#include "ipclog.h"
#include "ipc_shared_memory.h"
#include "ipc_osal.h"
#include "ipc_osal_util.h"
#include "dpccshm.h"
#include "ipc_defs.h"
#include "ipc_packet.h"
#include "dynamic_pool.h"
#include "ipc_macro_defs.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
#ifdef IPC_RELEASE
#define ipcSMGetPid(buf) (current->pid)
#endif
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
extern unsigned long vaddr;
/*
 * Don't create a free block smaller than epsilon.  If splitting a requested
 * allocation out of a free block would leave a free block smaller than
 * epsilon, give the user the whole free block, don't split it.  This doesn't
 * apply to allocated blocks.  If the user asks for one byte, we just round
 * it up to four, as long as the free piece is bigger than epsilon.
 */
#define epsilon (20 + sizeof(struct dpcc_memblk))
#define PREVIOUS_ONE(addr,type) (type*)((char*)(addr) - sizeof(type)) 

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: dpccshminit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Initialize a shared memory region.
*   
*   PARAMETER(S):
*       shm_info: Pointer to shared memory pool.
*
*   RETURN: 
*       Return value only indicates status: 0 ok -1 error.
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Mar1996  Motorola Inc.         Major rework with faster algorithm
* 10Mar1996  Motorola Inc.         Modified for performance of dpccshmavail()
* 27Jul2005  Motorola Inc.         Bugfix duplicated memory free logic
* 24Aug2005  Motorola Inc.         Return error code defined in Smart IPC
* 26May2006  Motorola Inc.         Modified for dynamic pool creation
* 20Feb2007  Motorola Inc.         Removed usage field
*
*****************************************************************************************/
int dpccshminit(struct dpcc_shm_info*   shm_info)
{
    struct dpcc_memblk**        front;          /* to header of this block */
    struct dpcc_memblk*         tag;            /* boundary tag block */
    struct dpcc_memblk*         this_block;     /* block of memory */

    shm_info->eyecatch = DPCC_SHM_POOL_EYE_CATCH;


    /*
     * Fabricate two blocks:  a zero-length block that is never free, which
     * is the boundary tag, and a maximum-sized block which is initialized
     * free, from which allocations are made.
     */
    this_block = (struct dpcc_memblk *) & (shm_info[1]);
    this_block->eyecatch = DPCC_SHM_EYE_CATCH;
    this_block->shm_start_ptr = shm_info;
    this_block->size    = shm_info->size        /* size of shared segment */
        - sizeof(struct dpcc_shm_info)          /* seg header (wasted) */
        - sizeof(struct dpcc_memblk);           /* boundary tag block */

    /*
     * flink and blink both point to the boundary tag (only two nodes in the
     * circular list).
     */
    this_block->flink = this_block->blink = tag
        = (struct dpcc_memblk *) ((char *) this_block + this_block->size);
    this_block->self = DPCC_FREE;
    this_block->neighbor = DPCC_ALLOCed;


    /*
     * This is the trick Reingold and Hansen call "front":
     */
    front = PREVIOUS_ONE(((char *) this_block + this_block->size), struct dpcc_memblk *);
    *front = this_block;


    /*
     * Now, the boundary tag block:
     */
    tag->eyecatch       = DPCC_SHM_EYE_CATCH;
    tag->shm_start_ptr  = shm_info;
    tag->flink          = tag->blink            = this_block;
    tag->size           = 0;
    tag->self           = DPCC_ALLOCed;
    tag->neighbor       = DPCC_FREE;
    shm_info->freelist  = this_block;

    return 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: dpccshmfree
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Returns a buffer to it's original shared memory pool
*   
*   PARAMETER(S):
*       ptr: Pointer to buffer.
*
*   RETURN: 
*       Return value only indicates status: 0 ok -1 error.
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb1994  Motorola Inc.         Initial Creation
* 25Mar1996  Motorola Inc.         Major rework with faster algorithm
* 10Mar1998  Motorola Inc.         Modified for performance of dpccshmavail()
* 27Jul2005  Motorola Inc.         Bugfix duplicated memory free logic
* 24Aug2005  Motorola Inc.         Return error code defined in Smart IPC
* 26May2006  Motorola Inc.         Modified for dynamic pool creation
* 20Feb2007  Motorola Inc.         Removed usage field
* 25May2007  Motorola Inc.         Print all error messages with IPCLOG_MEMTRACK
*
*****************************************************************************************/
int dpccshmfree(char* ptr)
{
    struct dpcc_memblk*         dorsal; /* neighboring block above */
    char*                       end_of_seg;
    struct dpcc_memblk**        front;  /* to header of ventral block */
    struct dpcc_shm_info*       shm_info = NULL;       /* Memory pool header */
    struct dpcc_memblk*         this_block;     /* Memory block */
    struct dpcc_memblk*         ventral;/* neighboring block below */
    
    /*
     * Carefully test any pointer passed in from a user before accepting that
     * it really points to the user portion of a memory block (just past the
     * dpcc_memblk header).
     */
    if (ptr == NULL)
        return 0;

    if (((unsigned long) ptr & 3) != 0)
    {
        ipcError(IPCLOG_MEMTRACK, ("misaligned pointer argument %p\n", ptr));
        return IPCEBADBUF;
    }


    /*
     * Get back to the memory block header from the user's pointer
     */
     this_block = PREVIOUS_ONE(ptr, struct dpcc_memblk);

    

    /*
     * Does the memory block belong to this pool?
     */
    if (this_block->eyecatch == DPCC_SHM_EYE_CATCH)
    {
        shm_info = this_block->shm_start_ptr;
        if ((shm_info == NULL)
            || (((unsigned) shm_info & 3) != 0)
            || (shm_info->eyecatch != DPCC_SHM_POOL_EYE_CATCH)) 
        {
            ipcError(IPCLOG_MEMTRACK, ("damaged shared memory pool header (%p)\n"
                    "eyecatch: %d, shm_start_ptr: %p flink: %p blink: %p\n"
                    "size: %li self: %d, neighbor: %d",
                    this_block, this_block->eyecatch,
                    this_block->shm_start_ptr,
                    this_block->flink, this_block->blink, this_block->size,
                    this_block->self, this_block->neighbor));
            return IPCEBADBUF;
        }
    }

    else
    {
        ipcError(IPCLOG_MEMTRACK, ("pointer argument (%p) is not from shared memory",
                this_block));
        return IPCEBADBUF;
    }


    /*
     * Lock the memory pool before you do anything
     */
    dpcc_mutex_lock(&shm_info->mutex);

    if (!((this_block->self == DPCC_FREE)
          || (this_block->self == DPCC_ALLOCed))
        || !((this_block->neighbor == DPCC_FREE)
             || (this_block->neighbor == DPCC_ALLOCed))
        || (this_block->size
            >= (this_block->shm_start_ptr->size))
            || (this_block->size < sizeof(struct dpcc_memblk)))
    {
        ipcError(IPCLOG_MEMTRACK, ("bad pointer argument (%p) or memblk header\n"
                "eyecatch: %d, shm_start_ptr: %p flink: %p blink: %p\n"
                "size: %li self: %d, neighbor: %d",
                this_block, this_block->eyecatch, this_block->shm_start_ptr,
                this_block->flink, this_block->blink, this_block->size,
                this_block->self, this_block->neighbor));

        dpcc_mutex_unlock(&shm_info->mutex);
        return IPCEBADBUF;
    }


    /*
     * If the block was already free, then just return with an error.
     * Remember to unlock the memory pool.
     */
    if (this_block->self == DPCC_FREE)
    {
        dpcc_mutex_unlock(&shm_info->mutex);
        ipcError(IPCLOG_MEMTRACK, ("DUPLICATED VIRTUAL SHM FREE!!!\n"));
        return IPCEBADBUF;
    }


    end_of_seg = (char*)shm_info  +  shm_info->size;

    if (((char *) this_block <= (char *) shm_info)
            || ((char *) this_block > end_of_seg))
    {
        ipcError(IPCLOG_MEMTRACK, ("block not in pool: %p", this_block));

        dpcc_mutex_unlock(&shm_info->mutex);
        return IPCEBADBUF;
    }


    /* Reduce the "size used" of pool by size of this block */
    shm_info->size_used -=  this_block->size;

    /*
     * If the block just past this one is free, merge it into this one.
     */
    dorsal = (struct dpcc_memblk *) ((char *) this_block + this_block->size);

    if (((char *) dorsal <= (char *) shm_info)
            || ((char *) dorsal > end_of_seg))
    {
        ipcError(IPCLOG_MEMTRACK, ("dorsal is damaged: %p", dorsal));
    }

    if (dorsal->self == DPCC_FREE)
    {
    	  if (((char *) dorsal->flink <= (char *) shm_info)
            || ((char *) dorsal->flink > end_of_seg))
        {
            ipcError(IPCLOG_MEMTRACK, ("dorsal->flink is damaged: %p", dorsal->flink));
        }
        
        if (((char *) dorsal->blink <= (char *) shm_info)
            || ((char *) dorsal->blink > end_of_seg))
        {
            ipcError(IPCLOG_MEMTRACK, ("dorsal->blink is damaged: %p", dorsal->blink));
        }
        
        dorsal->blink->flink = dorsal->flink;   /* Delete dorsal from the
                                                 * free list */
        dorsal->flink->blink = dorsal->blink;

        if (shm_info->freelist == dorsal)
            shm_info->freelist = dorsal->blink;

        this_block->size += dorsal->size;       /* Merge dorsal onto this
                                                 * block */
    }

    else
    {
        /*
         * This block's dorsal neighbor isn't free, but that block's ventral
         * neighbor is this block, and it's now free.
         */
        dorsal->neighbor = DPCC_FREE;
    }


    /*
     * If this block's ventral neighbor is free, merge this block into it.
     */
    if (this_block->neighbor == DPCC_FREE)
    {
        /*
         * Pick up Reingold and Hansen's "front" pointer.
         */
        front = PREVIOUS_ONE(this_block, struct dpcc_memblk *);
        ventral = *front;
        ventral->size += this_block->size;
        front = &((struct dpcc_memblk **)
                  ((char *) ventral + ventral->size))[-1];
                  
        if (((char *) front <= (char *) shm_info)
            || ((char *) front > end_of_seg))
        {
            ipcError(IPCLOG_MEMTRACK, (" ventral neighbor front is damaged: %p", front));
        }
        *front = ventral;

    }

    else
    {
        /*
         * Just link this block onto the free list "any old place", and mark
         * it free.
         */
        this_block->flink = shm_info->freelist;
        this_block->blink = shm_info->freelist->blink;
        shm_info->freelist->blink->flink = this_block;
        shm_info->freelist->blink = this_block;
        this_block->self = DPCC_FREE;

        /*
         * This is the trick Reingold and Hansen call "front":
         */
        front = PREVIOUS_ONE(((char *) this_block + this_block->size), struct dpcc_memblk *);
        
        if (((char *) front <= (char *) shm_info)
            || ((char *) front > end_of_seg))
        {
            ipcError(IPCLOG_MEMTRACK, ( " any old place front is damaged: %p", front));
        }
        *front = this_block;
    }


    /*
     * Unlock the memory pool
     */
    dpcc_mutex_unlock(&shm_info->mutex);

    return 0;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: dpccshmalloc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Allocates a buffer from the particular shared memory pool.
*   
*   PARAMETER(S):
*       shm_info: pointer to shared memory information structure.
*       sz: Size of buffer that will be allocated.
*
*   RETURN: 
*       Returns pointer to shared memory buffer or 0 if error.
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb1994  Motorola Inc.         Initial Creation
* 25Mar1996  Motorola Inc.         Major rework with faster algorithm
* 10Mar1998  Motorola Inc.         Modified for performance of dpccshmavail()
* 27Jul2005  Motorola Inc.         Bugfix duplicated memory free logic
* 24Aug2005  Motorola Inc.         Return error code defined in Smart IPC
* 09Dec2005  Motorola Inc.         Modify the comments and function header description.
* 26May2006  Motorola Inc.         Modified for dynamic pool creation
* 20Feb2007  Motorola Inc.         Removed usage field
* 25May2007  Motorola Inc.         Print all error messages with IPCLOG_MEMTRACK
*
*****************************************************************************************/
char* dpccshmalloc(struct dpcc_shm_info*  shm_info,
             unsigned long          sz)
{
    char*                       end_of_seg;
    struct dpcc_memblk**        front;  /* to header of this block */
    struct dpcc_memblk*         this_block; /* Memory block (to be allocated)*/
    struct dpcc_memblk*         tmp;    /* Free memory segment */

    if (sz <= 0)
    {
        return NULL;
    }

    /*
     * Add size of memblk header and round to long word
     */
    sz = (sz + sizeof(struct dpcc_memblk) + 3) & ~3;
    /*
     * Validate the pool selector argument.
     */
    if ((shm_info == NULL)
        || (((unsigned long) shm_info & 3) != 0)
        || (shm_info->eyecatch != DPCC_SHM_POOL_EYE_CATCH)) 
    {
        ipcError(IPCLOG_MEMTRACK, ("Invalid memory pool selector"));
        return NULL;
    }


    /*
     * Lock the memory pool
     */
    dpcc_mutex_lock(&shm_info->mutex);

    this_block = shm_info->freelist;
    end_of_seg = (char*)shm_info  +  shm_info->size;

    while ((this_block != shm_info->freelist->blink)
            && (this_block->size < sz))
    {
        this_block = this_block->flink;

        if (((char *) this_block <= (char *) shm_info)
            || ((char *) this_block > end_of_seg)
            || (((unsigned) this_block & 3) != 0)
                || (this_block->eyecatch != DPCC_SHM_EYE_CATCH))
        {
            ipcError(IPCLOG_MEMTRACK, ("corrupted memory pool free list: %p",
                    this_block));

            dpcc_mutex_unlock(&shm_info->mutex);
            return NULL;
        }
    }


    if (this_block->size < sz)
    {
        /*
         * Out of memory. Unlock the memory pool.
         */
        ipcWarning(IPCLOG_GENERAL, ("memory block size requested %li", sz));

        dpcc_mutex_unlock(&shm_info->mutex);
        return NULL;
    }


    shm_info->freelist = this_block->flink;

    if ((this_block->size - sz) < epsilon)
    {
        /*
         * Allocate the entire record and remove it from the list.
         */
        this_block->blink->flink = this_block->flink;
        this_block->flink->blink = this_block->blink;
    }

    else
    {
        /*
         * Allocate the top part of the block.  Split the block in two,
         * create a new memblk header for the allocated part and adjust the
         * size of the part that remains free.
         */
        this_block->size -= sz;

        /*
         * This is the trick Reingold and Hansen call "front":
         */
        front = PREVIOUS_ONE(((char *) this_block + this_block->size), struct dpcc_memblk *);
        *front = this_block;
        this_block
            = (struct dpcc_memblk *) ((char *) this_block + this_block->size);
        this_block->eyecatch = DPCC_SHM_EYE_CATCH;
        this_block->shm_start_ptr = shm_info;
        this_block->size = sz;
        this_block->neighbor = DPCC_FREE;
    }

    this_block->self = DPCC_ALLOCed;

    tmp = (struct dpcc_memblk *) ((char *) this_block + this_block->size);
    tmp->neighbor = DPCC_ALLOCed;


    /* Add size of this block to the pool's "size used" */
    shm_info->size_used +=  this_block->size;


    /*
     * Unlock the memory pool
     */
    dpcc_mutex_unlock(&shm_info->mutex);

    return (char *) &(this_block[1]);   /* beyond the header */
}

#define SHM_INFO_TO_POOL(shminfo) \
    (ipcShmPool_t *)((char *)(shminfo)-offsetof(ipcShmPool_t,dpccshminfo))
#define USER_PTR(shminfo,ptr) \
    (vaddr+(SHM_INFO_TO_POOL(shminfo))->offset+(unsigned long)(ptr)-(unsigned long)SHM_INFO_TO_POOL(shminfo))

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: dpccpoolwalk
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Visits every block in a shared memory segment, both free and allocated,
*   calling a call-back function for each block.
*   
*   PARAMETER(S):
*       shm_info - pointer to shared memory information structure.
*
*   RETURN: 
*       Returns  0 if ok -1 if error.
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Feb1994  Motorola Inc.         Initial Creation
* 25Mar1996  Motorola Inc.         Major rework with faster algorithm
* 10Mar1998  Motorola Inc.         Modified for performance of dpccshmavail()
* 27Jul2005  Motorola Inc.         Bugfix duplicated memory free logic
* 24Aug2005  Motorola Inc.         Return error code defined in Smart IPC
* 09Dec2005  Motorola Inc.         Modify the comments and function header description.
* 26May2006  Motorola Inc.         Modified for dynamic pool creation
* 18Jul2006  Motorola Inc.         Memory pid tracking and overrun detection
* 18Aug2006  Motorola Inc.         Adjusted for IPC_RELEASE build
* 11Oct2006  Motorola Inc.         update the print out format
* 20Feb2007  Motorola Inc.         Removed usage field
* 02Apr2007  Motorola Inc.         Unify memory log format
* 25May2007  Motorola Inc.         Print all error messages with IPCLOG_MEMTRACK
* 05Jul2007  Motorola Inc.         Removed invocation of ipcSMGetTailMark
*
*****************************************************************************************/
int dpccpoolwalk(struct dpcc_shm_info* shm_info)
{
    struct dpcc_memblk *this_block;
    int i = 0;
#ifndef IPC_RELEASE
    unsigned short pid;
    struct task_struct* owner = NULL;
    unsigned long tick, cur_tick;

    cur_tick = ipcOsalGetTickCount();
#endif

    /*
     * Validate the pool selector argument.
     */
    if ((shm_info == NULL)
        || (((unsigned long) shm_info & 3) != 0)
            || (shm_info->eyecatch != DPCC_SHM_POOL_EYE_CATCH))
    {
        ipcError(IPCLOG_MEMTRACK, ("Invalid memory pool selector\n"));
        return -1;
    }

    /*
     * Lock the memory pool
     */
    dpcc_mutex_lock(&shm_info->mutex);

#ifndef IPC_RELEASE
    ipcVirtShmReport("+-----------------------------------------------------------------------------------+\n");
    ipcVirtShmReport("|   No.  |  usrptr  |  size   | flags  |  usrsz  |  pid |  task name     | age (ms) |\n");
    ipcVirtShmReport("+-----------------------------------------------------------------------------------+\n");
#else
    ipcVirtShmReport("+--------------------------------------+\n");
    ipcVirtShmReport("|   No.  |  usrptr  |  size   | flags  |\n");
    ipcVirtShmReport("+--------------------------------------+\n");
#endif

    for (this_block = (struct dpcc_memblk *) & (shm_info[1]); (this_block->size); 
         this_block = (struct dpcc_memblk *)((char *)this_block + this_block->size))
    {
        /*
         * Does the memory block header look OK?
         */
        if (((((unsigned long) this_block)& 3) != 0)
            || (this_block->eyecatch != DPCC_SHM_EYE_CATCH)
            || (this_block->shm_start_ptr != shm_info)
            || (this_block->size
                >= this_block->shm_start_ptr->size)
                || (this_block->size < sizeof(struct dpcc_memblk)))
        {
            ipcError(IPCLOG_MEMTRACK, ("corrupted memory pool free list (%p)\n"
                    "eyecatch: %d, shm_start_ptr: %p flink: %p blink: %p\n"
                    "size: %li self: %d, neighbor: %d, pid=%d",
                    this_block, this_block->eyecatch,
                    this_block->shm_start_ptr,
                    this_block->flink, this_block->blink,
                    this_block->size,
                    this_block->self,
                    this_block->neighbor,
                    ipcSMGetPid(&this_block[1])));

            dpcc_mutex_unlock(&shm_info->mutex);
            return IPCEBADBUF;
        }
        ipcVirtShmReport("| %-04d   | %-08x | %-08li| %c ",
                 i++,
                 USER_PTR(shm_info,(char *)&this_block[1]+sizeof(ipcSMBufHead_t)+IPC_PACKET_DATA_OFFSET), 
                 this_block->size,
                 this_block->self);
#ifndef IPC_RELEASE
        if (this_block->self == DPCC_ALLOCed)
        {
            tick = cur_tick - ipcSMGetTick((ipcSMBufHead_t *)&(this_block[1]));
            pid = ipcSMGetPid(&this_block[1]);
            owner = find_task_by_pid(pid);

            ipcVirtShmReport("%04s | %-08li| %-04d |%-*s| %8d |\n",
                 ipcSMCheckBuffer(ipcSMHeaderToData(&(this_block[1])), "dpccpoolwalk")? "ok": "bad", 
                 ipcSMGetUsrSize(&this_block[1])-IPC_PACKET_DATA_OFFSET,
		 pid, 16, (owner == NULL)? "dead task" : owner->comm,tick);
        }
        else
        {
            ipcVirtShmReport("     |         |      |  free          |          |\n");
        }
#else
        ipcVirtShmReport("     |\n");
#endif
     }

    ipcVirtShmReport("+-----------------------------------------------------------------------------------+\n");
    ipcVirtShmReport("|Total:(%-08d)Bytes \n", shm_info->size);
    ipcVirtShmReport("|Used :(%-08d)Bytes \n", shm_info->size_used);
    ipcVirtShmReport("|Free :(%-08d)Bytes \n", shm_info->size - shm_info->size_used);
    ipcVirtShmReport("+-----------------------------------------------------------------------------------+\n");

    /*
     * Unlock the memory pool.
     */
    dpcc_mutex_unlock(&shm_info->mutex);

    return 0;
}
