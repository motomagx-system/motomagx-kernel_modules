/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_osal_mutex.c
*   FUNCTION NAME(S): ipcOsalGetProcessId
*                     ipcOsalGetProcessName
*                     ipcOsalGetThreadId
*                     ipcOsalMutexClose
*                     ipcOsalMutexLock
*                     ipcOsalMutexOpen
*                     ipcOsalMutexRelease
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed to using sema
* 23Feb2007  Motorola Inc.         Added ipcOsalGetProcessId and ipcOsalGetProcessName
* 29Jun2007  Motorola Inc.         Make function ipcOsalGetProcessName available in release
* 05Jul2007  Motorola Inc.         Changed to uninterruptible lock
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_osal.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/
#define MUTEX_OK 0  /* NOTE: It should be same to SEMAPHORE_OK */
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalMutexOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed to use sema 
* 05Jul2007  Motorola Inc.         Removed legacy support
*
*****************************************************************************************/
int ipcOsalMutexOpen(ipcMutexHandle_t* mutex)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return ipcOsalSemaOpen(mutex, 1);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalMutexLock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed to use sema 
* 05Jul2007  Motorola Inc.         Changed to uninterruptible lock
*
*****************************************************************************************/
int ipcOsalMutexLock(ipcMutexHandle_t mutex)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ((mutex == NULL) || (mutex->status != MUTEX_OK))
    {
        return IPCE_OBJECT_NOT_FOUND;
    }
    down(&mutex->sem);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalMutexRelease
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed to use sema 
* 05Jul2007  Motorola Inc.         Removed legacy support
*
*****************************************************************************************/
int ipcOsalMutexRelease(ipcMutexHandle_t mutex)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return ipcOsalSemaSignal(mutex);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalMutexClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed to use sema 
* 05Jul2007  Motorola Inc.         Removed legacy support
*
*****************************************************************************************/
int ipcOsalMutexClose(ipcMutexHandle_t mutex)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return ipcOsalSemaClose(mutex);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalGetThreadId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Get the identifier of current thread.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
UINT32 ipcOsalGetThreadId()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (UINT32)current;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalGetProcessId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Return current Process ID
*   
*   PARAMETER(S):
*       void
*                  
*   RETURN: current pid
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 17Apr2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
UINT32 ipcOsalGetProcessId()
{
    return (UINT32)current->pid;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalGetProcessName
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to get the process name using pid
*   
*   PARAMETER(S):
*       pid: linux process ID
*                  
*   RETURN: process name
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2006  Motorola Inc.         pid tracking and overrun detection
* 20Feb2007  Motorola Inc.         Renamed function
*
*****************************************************************************************/
const char *ipcOsalGetProcessName(UINT32 pid)
{
    struct task_struct* owner;
    owner = find_task_by_pid(pid);
    return (owner == NULL) ? "dead task" : owner->comm;
}
