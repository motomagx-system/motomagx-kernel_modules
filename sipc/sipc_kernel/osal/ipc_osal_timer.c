/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_osal_timer.c
*   FUNCTION NAME(S): ipcCallbackTimerWrapper
*                     ipcOsalGetTickCount
*                     ipcOsalGetTimestamp
*                     ipcOsalSleep
*                     ipcOsalTimerClose
*                     ipcOsalTimerInit
*                     ipcOsalTimerIsActive
*                     ipcOsalTimerOpen
*                     ipcOsalTimerStart
*                     ipcOsalTimerStop
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 10May2004  Motorola Inc.         Initial Creation
* 25May2005  Motorola Inc.         Changed to use workqueue
* 12Aug2005  Motorola Inc.         Added ipcOsalGetTimestamp
* 23Aug2007  Motorola Inc.         Changed to support ltt time for TAOS
* 11May2007  Motorola Inc.         Fixed INIT_WORK error
* 05Jul2007  Motorola Inc.         Removed legacy support
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
#include "ipc_osal.h"
#include <linux/time.h>
#include <linux/delay.h>

/* Specified of TAOS to support ltt time in ipcdump */
#ifdef CONFIG_ARCH_MXC91131
#include <linux/ltt-events.h>
#endif

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCallbackTimerWrapper
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
* 11May2007  Motorola Inc.         Removed INIT_WORK
* 05Jul2007  Motorola Inc.         Removed legacy support
*
*****************************************************************************************/
static void ipcCallbackTimerWrapper(unsigned long data)
{
    ipcTimer_t* timer = (ipcTimer_t*)data;
    
    if ((timer != NULL) && (timer->callback != NULL))
    {
        /* Add an element into the workqueue */
        schedule_work(&timer->work);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalTimerOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcOsalTimerOpen(ipcTimerHandle_t *timerHandle)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcTimerHandle_t timer = kmalloc(sizeof(ipcTimer_t), GFP_KERNEL);
    if (timer == 0)
    {
        return IPCE_OUT_OF_MEMORY;
    }
    memset(timer, 0, sizeof(*timer));
    init_timer(&timer->sys_timer);
    timer->sys_timer.data = (unsigned long)timer;
    timer->sys_timer.function = &ipcCallbackTimerWrapper;
    *timerHandle = timer;
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalTimerInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
* 11May2007  Motorola Inc.         Added INIT_WORK
*
*****************************************************************************************/
int ipcOsalTimerInit(ipcTimerHandle_t timer, UINT32 milliseconds,
                     ipcOsalTimerCallback_t callback, void *param)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (timer == NULL)
    {
        return IPCE_OBJECT_NOT_FOUND;
    }
    timer->expires = milliseconds;
    timer->callback = callback;
    timer->param = param;
    init_timer(&timer->sys_timer);
    timer->sys_timer.data = (unsigned long)timer;
    timer->sys_timer.function = &ipcCallbackTimerWrapper;
    INIT_WORK(&timer->work, timer->callback, timer->param);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalTimerClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Removed legacy support
*
*****************************************************************************************/
int ipcOsalTimerClose(ipcTimerHandle_t timer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (timer == NULL)
    {
        return IPCE_OBJECT_NOT_FOUND;
    }
    if (ipcOsalTimerIsActive(timer))
    {
        del_timer_sync(&timer->sys_timer);
    }

    timer->callback = NULL;
    flush_scheduled_work();
    del_timer_sync(&timer->sys_timer);

    kfree(timer);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalTimerStart
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcOsalTimerStart(ipcTimerHandle_t timer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (timer == NULL)
    {
        return IPCE_OBJECT_NOT_FOUND;
    }
    if (ipcOsalTimerIsActive(timer))
    {
        del_timer_sync(&timer->sys_timer);
    }

    /* Set the timer once again... */
    timer->sys_timer.data = (unsigned long)timer;
    timer->sys_timer.function = &ipcCallbackTimerWrapper;

    timer->sys_timer.expires = jiffies + MS_TO_JIFFIES(timer->expires);
    add_timer(&timer->sys_timer);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalTimerStop
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Removed legacy support
*
*****************************************************************************************/
int ipcOsalTimerStop(ipcTimerHandle_t timer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (timer == NULL)
    {
        return IPCE_OBJECT_NOT_FOUND;
    }
    if (ipcOsalTimerIsActive(timer))
    {
        del_timer_sync(&timer->sys_timer);
    }
    flush_scheduled_work();
    del_timer_sync(&timer->sys_timer);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalTimerIsActive
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcOsalTimerIsActive(ipcTimerHandle_t timer)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (timer == NULL)
    {
        return FALSE;
    }
    return timer_pending(&timer->sys_timer);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalSleep
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation
* 13Jul2006  Motorola Inc.         Optimized
* 18Aug2006  Motorola Inc.         Add a return value 
*
*****************************************************************************************/
int ipcOsalSleep(UINT32 milliseconds)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    rc = msleep_interruptible(milliseconds);
    if (rc != 0)
    {
        return IPCE_ILLEGAL_USE;
    }
    return IPCE_OK;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalGetTimestamp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to return the time of day in seconds and microseconds
*   since midnight, January 1, 1970.
*        
*   PARAMETER(S):
*
*   RETURN: void
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10Oct2004  Motorola Inc.         Initial Creation
* 24Jan2007  Motorola Inc.         Taos-AP: SIPC: Use get_curr_ns_time  to get time, 
*                                  and print a message on every packet 
* 23Aug2007  Motorola Inc.         Changed to support ltt time for TAOS
*
*****************************************************************************************/
void ipcOsalGetTimestamp(long *sec, long *usec)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct timeval currentTime;
/*--------------------------------------- CODE -----------------------------------------*/
#ifdef CONFIG_ARCH_MXC91131
    ltt_lite_get_ms_time(&currentTime);
#else
    do_gettimeofday(&currentTime);
#endif
    *sec = currentTime.tv_sec;
    *usec = currentTime.tv_usec;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalGetTickCount
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
UINT32 ipcOsalGetTickCount(void)
{
    long sec, usec;
    ipcOsalGetTimestamp(&sec, &usec);

    return (UINT32)sec * 1000 + (UINT32)usec / 1000;
}

