/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_osal_sema.c
*   FUNCTION NAME(S): down_with_timeout
*                     ipcOsalSemaClose
*                     ipcOsalSemaOpen
*                     ipcOsalSemaSignal
*                     ipcOsalSemaWait
*                     process_timeout
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 20May2005  Motorola Inc.         Initial Creation
* 24Aug2005  Motorola Inc.         return error code defined in ipc_error_codes.h
* 30Aug2005  Motorola Inc.         Bugfix of ipcOsalSemaWait timeout 0
* 10May2005  Motorola Inc.         optimzie timeout for OsalSemaWait 
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_osal.h"
/*-------------------------------------- CONSTANTS -------------------------------------*/
#define SEMAPHORE_OK 0
#define SEMAPHORE_CLOSED 1
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
struct sema_wakeup_info
{
    struct semaphore *sem;
    int flag;
};
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
static void process_timeout(unsigned long __data);
static int down_with_timeout(struct semaphore *sem, long timeout);
/*--------------------------------------- MACROS ---------------------------------------*/

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalSemaOpen
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcOsalSemaOpen(ipcSemaHandle_t *semaHandle, UINT32 initialCount)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcSemaHandle_t sem = kmalloc(sizeof(ipcSemaphore_t), GFP_KERNEL);

    if (sem == NULL)
    {
        return IPCE_OUT_OF_MEMORY;
    }
    sem->status = SEMAPHORE_OK;
    sema_init(&sem->sem, initialCount);
    *semaHandle = sem;
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalSemaClose
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation
* 24Aug2005  Motorola Inc.         return error code defined in ipc_error_codes.h 
*
*****************************************************************************************/
int ipcOsalSemaClose(ipcSemaHandle_t sem)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    /* we should close all the threads waiting for the semaphore */
    if (sem == NULL)
    {
        return IPCE_OBJECT_NOT_FOUND;
    }
    while (waitqueue_active(&sem->sem.wait))
    {
        sem->status = SEMAPHORE_CLOSED;
        up(&sem->sem);
        schedule();
    }
    kfree(sem);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalSemaSignal
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcOsalSemaSignal(ipcSemaHandle_t sem)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if ((sem == NULL) || (sem->status != SEMAPHORE_OK))
    {
        return IPCE_ILLEGAL_USE;
    }
    up(&sem->sem);
    return IPCE_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: process_timeout
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static void process_timeout(unsigned long __data)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct sema_wakeup_info *pinfo = (struct sema_wakeup_info *) __data;
/*--------------------------------------- CODE -----------------------------------------*/
    pinfo->flag = 1;
    up(pinfo->sem);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: down_with_timeout
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int down_with_timeout(struct semaphore *sem, long timeout)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int result;
/*--------------------------------------- CODE -----------------------------------------*/
    struct timer_list timer;
    struct sema_wakeup_info info;
    info.sem = sem;
    info.flag = 0;

    init_timer(&timer);
    timer.expires = timeout + jiffies;
    timer.data = (unsigned long) &info;
    timer.function = process_timeout;

    add_timer(&timer);

    result = down_interruptible(sem);
    del_timer_sync(&timer);
    if (result < 0)
    {
        result = IPCE_ILLEGAL_USE;
    }
    else if (info.flag)
    {
        /* FIXME: sema will be trigged twice and need to be down one time here
         * if timeout callback occures when sema was signaled due to 
         * new packet arrived */
        result = IPCE_TIMEOUT;
    }
    return result;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOsalSemaWait
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 10May2005  Motorola Inc.         Initial Creation
* 11Aug2005  Motorola Inc.         Fixed bug on interruption of signal
* 30Aug2005  Motorola Inc.         Fixed bug of timeout 0 
*
*****************************************************************************************/
int ipcOsalSemaWait(ipcSemaHandle_t sem, UINT32 timeout)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPCE_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((sem == NULL) || (sem->status != SEMAPHORE_OK))
    {
        return IPCE_OBJECT_NOT_FOUND;
    }

    if (timeout == 0)
    {
        if (down_trylock(&sem->sem))
        {
            return IPCE_TIMEOUT;
        }
        else
        {
            return IPCE_OK;
        }
    }

    if (timeout != IPC_OSAL_INFINITE)
    {
        rc = down_with_timeout(&sem->sem, MS_TO_JIFFIES(timeout));
    }
    else
    {
        rc = down_interruptible(&sem->sem);
        if ((rc != 0) || (sem->status != SEMAPHORE_OK))
        {
            rc = IPCE_ILLEGAL_USE;
        }
    }
    return rc;
}
