/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005, 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_osal_util.c
*   FUNCTION NAME(S): atoi
*                     getProcData
*                     ipcPhyShmReport
*                     ipcStackReport
*                     ipcVirtShmReport
*                     logInfo
*                     write_to_file
*                     ipcTraceFunc
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains functions used to translate between pointers to buffers
*   in shared memory and the corresponding offset in shared memory.  This must
*   be done before sending pointers between different processes or between user
*   and kernel space because the virtual address will be different.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 07Jul2005  Motorola Inc.         Initial Version
* 24Apr2007  Motorola Inc.         Added function to write data to a file
* 25May2007  Motorola Inc.         Added function to see if a file exists
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
* 18Jul2007  Motorola Inc.         Added ipcTrace funcition
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include <linux/ctype.h>
#include <linux/mm.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/namei.h>
#include "ipc_osal_util.h"
#include "ipc_osal.h"
#include "ipclog_impl.h"
#include "ipc_macro_defs.h"

/*--------------------------------------- MACROS ---------------------------------------*/
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
#if !defined(IPC_RELEASE) && defined(CONFIG_MOT_FEAT_LTT_LITE)

const char *sipc_lttlog_header[SIPCLTTNUM] = {"enter",
                                         "exit"};

char sipc_logID[SIPC_LAST_TYPE_ID+1][SIZE_SIPCLTT_STRING] = 
{
    "SIPC_FIRST_TYPE",

    "IPC_MAIN",

    // Add function ID
    "ipcsocket", 
    "ipcbind",
    "ipcgetsockname",
    "ipcgetpeername",
    "ipcshutdown", 
    "ipcsetsockqlen",
    "ipcclosesocket", 
    "ipcchan",
    "ipcsetwatermark",
    "ipclocateservice",
    "ipcnotify",
    "ipcpsnotify",
    "ipcsendto",
    "ipczsendto",
    "ipcrecvfrom",
    "ipczrecvfrom",
    "ipclisten",
    "ipcaccept",
    "ipcconnect",
    "ipczsend",
    "ipczrecv",
    "ipcsend",
    "ipcrecv",
    "ipczalloc",
    "ipczfree",
    "ipczcalloc",
    "ipcselect",
    "ipcgetmemtype"

    "SIPC_LAST_TYPE"
};
#endif

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcTraceFunc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*    This function is to call ltt log for trace.
*   
*   Params: status  - to indicate log entry or exit 
*           type    - log type
*
*   Return: void
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 18Jul2007  Motorola Inc.         Added ipcTrace funcition
*
*****************************************************************************************/

#if !defined(IPC_RELEASE) && defined(CONFIG_MOT_FEAT_LTT_LITE)
void ipcTraceFunc(int status, int type)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int len;
    char buf[LTT_LITE_MAX_LOG_STRING_SIZE];
/*--------------------------------------- CODE -----------------------------------------*/
    if ((status < 0) 
        || (status >= SIPCLTTNUM)
        || (type <= SIPC_FIRST_TYPE_ID) 
        || (type >= SIPC_LAST_TYPE_ID))
    {
        return;
    }
    
    len = snprintf(buf, LTT_LITE_MAX_LOG_STRING_SIZE, "pid=%d,%s_%s\n", 
            (int)current->pid, 
            sipc_lttlog_header[status], 
            sipc_logID[type]);
    ltt_lite_log_string(buf, len);
}
#endif
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: atoi
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Convert string to int
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 30Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int atoi(const char* str)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int ret = 0;
    while (*str && isdigit(*str))
    {
        ret = ret * 10 + (*str - '0');
        str ++;
    }
    return ret;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: logInfo
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Try to record proc data into list
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Jul2005  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
*
*****************************************************************************************/
BOOL logInfo(ipcProcElem_t *procElem, const char *format, va_list args)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int len = 0;
    char *bufptr = NULL;

/*--------------------------------------- CODE -----------------------------------------*/
    if (procElem->procdata == NULL)
    {
        /* This is the first log */
        initProcElem(procElem);
        procElem->procdata = kmalloc(DFTPROCSIZE, GFP_KERNEL);
        if (procElem->procdata == NULL)
        {
            printk(KERN_ALERT "kmalloc failed in SIPC proc function with size :%d\n", DFTPROCSIZE);
            return FALSE;
        }
        memset(procElem->procdata, 0, DFTPROCSIZE);
        procElem->total = DFTPROCSIZE;
    }
        
    while(TRUE) 
    {   
        len = vsnprintf((procElem->procdata + procElem->len), 
                            (procElem->total- procElem->len - 1) , format, args);

        if ((len >= 0) && (len < (procElem->total- procElem->len - 1)))
        {
            procElem->len += len;
            return TRUE;
        }
        
        /* Failed to log here */
        bufptr = kmalloc(procElem->total <<= 1, GFP_KERNEL);
        if (bufptr == NULL)
        {
            printk(KERN_ALERT "Failed to get memory with size :%d in SIPC PROC Read Function\n", procElem->total);
            break;
        }
        /**/
        memset(bufptr, 0, procElem->total);
        memcpy(bufptr, procElem->procdata, procElem->len);
        kfree(procElem->procdata);
        procElem->procdata = bufptr;
    }

    kfree(procElem->procdata);
    initProcElem(procElem);
    return FALSE;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: getProcData
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Try to get proc data from list
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Jul2005  Motorola Inc.         Initial Creation 
* 05Jul2007  Motorola Inc.         Changed ipc proc read function mechanism
*
*****************************************************************************************/
int getProcData(ipcProcElem_t *procElem, char *buf, size_t len, unsigned int offset)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int err = -1;
    unsigned int cpylen = 0;

/*--------------------------------------- CODE -----------------------------------------*/
    /* the snapshot is not generated successfully */
    if (procElem->procdata == NULL)
    {
        return -ENOMEM;
    }

    if ((procElem->pos != offset)
            || (procElem->pos > procElem->len))
    {
        printk(KERN_ALERT "error offset \n");
        kfree(procElem->procdata);
        initProcElem(procElem);
        return -EFAULT;
    }

    cpylen = ((procElem->len - offset) > len)? len:(procElem->len - offset);

    if(cpylen == 0)
    {
        kfree(procElem->procdata);
        initProcElem(procElem);
        return 0;
    }

    err = copy_to_user(buf, &(procElem->procdata[procElem->pos]), cpylen);
    if(err)
    {
        printk(KERN_ALERT"copy_to_user failed\n");
        return -EFAULT;
    }

    procElem->pos += cpylen;
    return cpylen;
    
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPhyShmReport
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Util function used to log Physical shared memory info
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Jul2005  Motorola Inc.         Initial Creation
* 30Aug2005  Motorola Inc.         Initialed args 
*
*****************************************************************************************/
int ipcPhyShmReport(const char * format, ...)
{
    va_list args = NULL;
    int rc = 0;

    va_start(args, format);
    rc = logInfo(&ipc_phys_shm_map, format, args);
    va_end(args);

    return rc;
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcVirtShmReport
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Util function used to log virtual shared memory information
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Jul2005  Motorola Inc.         Initial Creation
* 30Aug2005  Motorola Inc.         Initialed args 
*
*****************************************************************************************/
int ipcVirtShmReport(const char * format, ...)
{
    va_list args = NULL;
    int rc = 0;

    va_start(args, format);
    rc = logInfo(&ipc_virt_shm_map, format, args);
    va_end(args);

    return rc;
   
}
/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcStackReport
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Util function used to log IPC stack information
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Jul2005  Motorola Inc.         Initial Creation
* 30Aug2005  Motorola Inc.         Initialed args 
*
*****************************************************************************************/
int ipcStackReport(const char * format, ...)
{
    va_list args = NULL;
    int rc = 0;

    va_start(args, format);
    rc = logInfo(&ipc_stack_info, format, args);
    va_end(args);
    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: write_to_file
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to write data to a file.
*   
*   PARAMETER(S):
*       filename: name of file to write
*       data: the data to write to the file
*       datalen: length of data to write
*
*   RETURN: 
*       void
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 24Apr2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void write_to_file(const char* filename, const char* data, unsigned int datalen)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    struct file* file = filp_open(filename, O_CREAT | O_WRONLY, 00777);
    int result = 0;
    mm_segment_t fs;
/*--------------------------------------- CODE -----------------------------------------*/

    if (!IS_ERR(file))
    {
        if (file->f_op->write != NULL)
        {
            fs = get_fs();
            set_fs(KERNEL_DS);
            result = file->f_op->write(file, data, datalen, &file->f_pos);
            set_fs(fs);
            if (result != datalen)
            {
                printk("%s:%d: failed to write %d bytes to file %s, %d bytes written\n",
                        __FUNCTION__, __LINE__, datalen, filename, result);
            }
        }
        filp_close(file, NULL);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: file_exists
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to write data to a file.
*   
*   PARAMETER(S):
*       filename: name of file to write
*
*   RETURN: 
*       If the file exists, returns 1; otherwise 0.
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25May2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
int file_exists(const char* filename)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int exists = 0;
    struct nameidata nd;
    int err = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    err = path_lookup(filename, 0, &nd);
    if (err == 0)
    {
        path_release(&nd);
        exists = 1;
    }
    return exists;
}

char* dump_path = "/home";
ipc_string_param(dump_path);

