/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_router_internal.c
*   FUNCTION NAME(S): RTRTChannelRelRequest
*                     RTRTChannelReply
*                     RTRTChannelRequest
*                     RTRTIpcAddressAssign
*                     RTRTLinkComplete
*                     RTRTLinkDown
*                     RTRTLinkReply
*                     RTRTLinkRequest
*                     handleRTRTAddressAssign
*                     handleRTRTChannelRelRequest
*                     handleRTRTChannelReply
*                     handleRTRTChannelRequest
*                     handleRTRTLinkComplete
*                     handleRTRTLinkDown
*                     handleRTRTLinkReply
*                     handleRTRTLinkRequest
*                     ipcCtrlSockDataArrival
*                     ipcCtrlSockDataDeliver
*                     ipcCtrlSockLock
*                     ipcCtrlSockMaxRetries
*                     ipcCtrlSockSentListEmpty
*                     ipcCtrlSockTxWindowAvailable
*                     ipcCtrlSockTxWindowFull
*                     ipcCtrlSockUnlock
*                     ipcDoConnTimerJob
*                     ipcDoRouterTimerJob
*                     ipcRouterDispatchPacket
*                     ipcRouterEnterLinkDownState
*                     ipcRouterEnterLinkPendingState
*                     ipcRouterEnterLinkUpState
*                     ipcRouterForceLinkDown
*                     ipcSendCtrlSockPacket
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation
* 09Feb2005  Motorola Inc.         Updated for sockets
* 15Sep2005  Motorola Inc.         Updated for Connection-oriented API
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 26Apr2007  Motorola Inc.         Reset BP on max retries
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/
#include "ipc_list.h"
#include "ipc_common.h"
#include "ipc_osal.h"
#include "ipc_router_internal.h"
#include "ipc_router_interface.h"
#include "ipc_device_layer.h"
#include "ipc_session.h"
#include "ipclog.h"
#include "ipc_packet_log_internal.h"
#include "ipc_macro_defs.h"

#if (defined HAVE_PHYSICAL_SHM) && (!defined IPC_RELEASE)
#   include "mu_drv.h"
#endif


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterForceLinkDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function forces this side of the link down because the other side
*   thinks it is down.  The link must be down on both sides so we can reconnect.
*   
*   If it is our higher peer (link to server) we will clear the router address
*   table and inform the session that we no longer have an IPC address.  We also
*   start the timer to send address request messages to try and reestablish the
*   link, and tell all of our lower peers that the link to the server is gone
*   (by sending RTRTLinkDown messages).
*   
*   If it is not the higher peer we just inform the session that we have lost
*   a range of IPC addresses so they can be removed from the DRT.
*   
*   For all cases, we will tell the device layer to free all pending packets
*   when the link goes down.
*   
*   PARAMETER(S):
*       portId: port ID of link to force down
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 04Mar2005  Motorola Inc.         Initial Creation 
* 13Jul2007  Motorola Inc.         Removed compile condition HAVE_ROUTER_PENDING
*
*****************************************************************************************/
void ipcRouterForceLinkDown(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPortId_t destPortId;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcDeviceFreePendingPackets(portId);
    
    if (portId == ipcAddrToPort(ipcStack_glb.higherPeerAddr))
    {
        /* The link to our higher peer just went down so we no longer 
         * have an IPC Address.  Reset the router address table and 
         * inform the session */
        ipcRouterSetAddressRange(IPC_INVALID_ADDR, IPC_INVALID_ADDR, IPC_INVALID_PORT);

        /* Notify session layer that we have lost our connection to the server */
        RTSSReady();

        /* Propagate this message by telling all of our lower peers that the
         * link to the server node is down */
        for (destPortId = 0; destPortId < ipcStack_glb.numOpenPorts; destPortId++)
        {
            if (destPortId != portId)
            {
                /* Send on all ports except the one that just went down */
                RTRTLinkDown(destPortId);
            }
        }
    }
    else if (ipcStack_glb.ipcAddr != IPC_INVALID_ADDR)
    {
        /* We lost a link to one of our lower peers so inform the session layer
         * that this range of addresses is now gone */
        RTSSLinkDown(ipcStack_glb.portToIpcAddrRangeTable[portId].lowAddr,
                     ipcStack_glb.portToIpcAddrRangeTable[portId].highAddr);
    }
    /* Enter linkdown state */
    ipcRouterEnterLinkDownState(portId);

    ipcWarning(IPCLOG_ROUTER,
        ("Forced link down on port %d\n", portId));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterEnterLinkDownState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Set port to link down state.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 13Jul2007  Motorola Inc.         Removed compile condition HAVE_ROUTER_PENDING
*
*****************************************************************************************/
void ipcRouterEnterLinkDownState(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcPort_t* port = &ipcStack_glb.ports[portId];

    ipcOsalTimerStop(port->timer);
    ipcOsalTimerInit(port->timer, IPC_LINK_REQ_TIMEOUT, 
            ipcDoRouterTimerJob, ipcCastPtr(portId, void*));

    port->state = IPC_CONTROL_SOCKET_LINK_DOWN;

    /* destroy the connection */
    ipcConnDestroy(port->conn, TRUE, TRUE, TRUE);
    if (port->enabled)
    {
        ipcOsalTimerStart(port->timer);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterEnterLinkPendingState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Set a port to link pending state
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
void ipcRouterEnterLinkPendingState(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = &ipcStack_glb.ports[portId];
/*--------------------------------------- CODE -----------------------------------------*/
    ipcOsalTimerStop(port->timer);
    ipcOsalTimerInit(port->timer, IPC_LINK_PENDING_TIMEOUT, 
            ipcDoRouterTimerJob, ipcCastPtr(portId, void*));
    port->state = IPC_CONTROL_SOCKET_LINK_PENDING;
    ipcOsalTimerStart(port->timer);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterEnterLinkUpState
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Set a port to link up state
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
void ipcRouterEnterLinkUpState(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = &ipcStack_glb.ports[portId];
/*--------------------------------------- CODE -----------------------------------------*/
    ipcOsalTimerStop(port->timer);
    ipcOsalTimerInit(port->timer, IPC_LINK_COMP_TIMEOUT,
            ipcDoRouterTimerJob, ipcCastPtr(portId, void*));
    port->state = IPC_CONTROL_SOCKET_LINK_UP;
    ipcOsalTimerStart(port->timer);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTLinkDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to tell a peer node that either the link to the server
*   is down or the link to that node is down on this side so they should treat
*   it as down as well (until a successful address request/reply is completed).
*   
*   PARAMETER(S):
*       portId: port to send the message on
*                  
*   RETURN: IPC_OK for success or an appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 25Feb2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int RTRTLinkDown(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
    RTRTLinkDownMsg_t *packet = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    packet = (RTRTLinkDownMsg_t *)ipcCreateRouterControlPacket(
        RTRT_LINK_DOWN_DATA_SIZE,
        IPC_MSG_RTRT_LINK_DOWN,
        IPC_INVALID_ADDR);

    if (packet == NULL)
    {
        ipcError(IPCLOG_ROUTER, ("Out of memory in RTRTLinkDown\n"));
        rc = IPCENOBUFS;
    }
    else
    {
        /* We do not know what sequence number the other side expects and
         * we do not want the device layer to keep sending this packet over
         * and over so we will put it on the default streamed channel instead
         * of the control channel.
         * It will still be recognized as a control message because the
         * channel in the packet header will be IPC_CONTROL_CHANNEL_ID. */
        rc = ipcSendCtrlSockPacket(portId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTLinkDown failed to send: %d\n", rc));
            ipcFree(packet);
        }
        else
        {
            ipcDebugLog(IPCLOG_ROUTER, ("Send RTRTLinkDown to port %d\n", portId));
        }
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTChannelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC router manager to request a peer to peer 
*   Connection from the next router in the chain.
*   
*   PARAMETER(S):
*       multihopChannelId: Unique Channel ID to be used by all nodes
*       QoS: Requested bandwidth for this channel
*                  
*   RETURN: IPC_OK for success or an appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
*
*****************************************************************************************/
int RTRTChannelRequest(ipcMultihopChannelId_t multihopChannelId, ipcQoS_t QoS)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcAddress_t destAddr;
    ipcPortId_t portId;
    RTRTChannelRequestMsg_t *packet;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Get the final destination address of the channel from multihop channel ID */
    destAddr = ipcMultihopToDestIpcAddr(multihopChannelId);
    portId = ipcAddrToPort(destAddr);

    packet = (RTRTChannelRequestMsg_t *)ipcCreateRouterControlPacket(
        RTRT_CHANNEL_REQUEST_DATA_SIZE,
        IPC_MSG_RTRT_CHANNEL_REQUEST,
        destAddr);
    
    if (packet == NULL)
    {
        rc = IPCENOBUFS;
        ipcError(IPCLOG_ROUTER, ("RTRTChannelRequest -> out of memory!!!\n"));
    }
    else
    {
        ipcOsalWrite32(packet->multihopChannelId, multihopChannelId);
        ipcOsalWrite32(packet->QoS, QoS);

        rc = ipcSendCtrlSockPacket(portId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTChannelRequest failed to send: %d\n", rc));
            ipcFree(packet);
        }
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTChannelReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC router manager to reply to a peer to peer 
*   Connection request from the next router in the chain.
*   
*   PARAMETER(S):
*       multihopChannelId: Unique Channel ID to be used by all nodes
*       status: IPC_OK or IPCE_INVALID_CHANNEL_ID
*                  
*   RETURN: IPC_OK for success or an appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
*
*****************************************************************************************/
int RTRTChannelReply(ipcMultihopChannelId_t multihopChannelId, INT8 status)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcAddress_t destAddr;
    ipcPortId_t portId;
    RTRTChannelReplyMsg_t *packet;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Get the address of the channel request originator from multihop channel ID */
    destAddr = ipcMultihopToSrcIpcAddr(multihopChannelId);
    portId = ipcAddrToPort(destAddr);

    packet = (RTRTChannelReplyMsg_t *)ipcCreateRouterControlPacket(
        RTRT_CHANNEL_REPLY_DATA_SIZE,
        IPC_MSG_RTRT_CHANNEL_REPLY,
        destAddr);
    
    if (packet == NULL)
    {
        rc = IPCENOBUFS;
        ipcError(IPCLOG_ROUTER, ("RTRTChannelRequest -> out of memory!!!\n"));
    }
    else
    {
        ipcOsalWrite32(packet->multihopChannelId, multihopChannelId);
        ipcOsalWrite8(packet->status, status);

        rc = ipcSendCtrlSockPacket(portId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTChannelReply failed to send: %d\n", rc));
            ipcFree(packet);
        }
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTChannelRelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC router manager to request a peer to peer 
*   channel release from the next router in the chain.
*   
*   PARAMETER(S):
*       multihopChannelId: Unique channel ID used by all nodes for this channel
*                  
*   RETURN: IPC_OK for success or an appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
*
*****************************************************************************************/
int RTRTChannelRelRequest(ipcMultihopChannelId_t multihopChannelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcAddress_t destAddr;
    ipcPortId_t portId;
    RTRTChannelRelRequestMsg_t *packet = NULL;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Get the final destination address of the channel from multihop channel ID */
    destAddr = ipcMultihopToDestIpcAddr(multihopChannelId);
    portId = ipcAddrToPort(destAddr);

    packet = (RTRTChannelRelRequestMsg_t *)ipcCreateRouterControlPacket(
        RTRT_CHANNEL_REL_REQUEST_DATA_SIZE,
        IPC_MSG_RTRT_CHANNEL_REL_REQUEST,
        destAddr);

    if (packet == NULL)
    {
        rc = IPCENOBUFS;
    }
    else
    {
        ipcOsalWrite32(packet->multihopChannelId, multihopChannelId);

        rc = ipcSendCtrlSockPacket(portId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTChannelRelRequest failed to send: %d\n", rc));
            ipcFree(packet);
        }
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTLinkRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send link request to peer node via port specified by portId.
*   
*   PARAMETER(S):
*       portId: the ID of the desired port
*          
*   RETURN: IPC_OK on success
*           negative values on failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 15Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int RTRTLinkRequest(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int rc = IPC_OK;

    RTRTLinkRequestMsg_t* packet = (RTRTLinkRequestMsg_t*)ipcCreateRouterControlPacket(
            RTRT_LINK_REQUEST_DATA_SIZE,
            IPC_MSG_RTRT_LINK_REQUEST,
            IPC_INVALID_ADDR);

    if (packet != NULL)
    {
        ipcSetMsgFlags(packet, IPC_PACKET_CONTROL);
        ipcOsalWrite8(packet->myAddr, ipcStack_glb.ipcAddr);

        rc = RTDVDataRequest(IPC_NONDEDICATED_CHANNEL_ID, 
                (ipcPacket_t*)packet, portId);
        if (rc != IPC_OK)
        {
            ipcFree(packet);
            if (ipcStack_glb.ports[portId].deviceType != IPC_DEVICE_SHARED_MEMORY)
            {
                /* Do not print error message for shared memory since the AP will be
                 * ready before the BP and we expect an error return in this case */
                ipcError(IPCLOG_ROUTER, ("RTRTLinkRequest failed %d\n", rc));
            }
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER, ("Allocate RTRT_LINK_REQUEST packet failed\n"));
        rc = IPCENOBUFS;
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTLinkReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send link reply to peer node via port specified by portId.
*   
*   PARAMETER(S):
*       portId: the ID of the desired port
*          
*   RETURN: IPC_OK on success
*           negative values on failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
static int RTRTLinkReply(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int rc = IPC_OK;

    RTRTLinkReplyMsg_t* packet = (RTRTLinkReplyMsg_t*)ipcCreateRouterControlPacket(
            RTRT_LINK_REPLY_DATA_SIZE,
            IPC_MSG_RTRT_LINK_REPLY,
            IPC_INVALID_ADDR);

    if (packet != NULL)
    {
        ipcSetMsgFlags(packet, IPC_PACKET_CONTROL);

        rc = RTDVDataRequest(IPC_NONDEDICATED_CHANNEL_ID, 
                (ipcPacket_t*)packet, portId);
        if (rc != IPC_OK)
        {
            ipcFree(packet);
            ipcError(IPCLOG_ROUTER, ("RTRTLinkReply failed %d\n", rc));
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER, ("Allocate RTRT_LINK_REPLY packet failed\n"));
        rc = IPCENOBUFS;
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTLinkComplete
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send link complete message to peer node via port specified by portId.
*   
*   PARAMETER(S):
*       portId: the ID of the desired port
*          
*   RETURN: IPC_OK on success
*           negative values on failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int RTRTLinkComplete(ipcPortId_t portId, BOOL resetSeqNum)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int rc = IPC_OK;
    UINT8 flags = 0;

    RTRTLinkCompleteMsg_t* packet = (RTRTLinkCompleteMsg_t*)ipcCreateRouterControlPacket(
            RTRT_LINK_COMPLETE_DATA_SIZE,
            IPC_MSG_RTRT_LINK_COMPLETE,
            IPC_INVALID_ADDR);

    ipcDebugLog(IPCLOG_ROUTER,
        ("RTRTLinkComplete called\n"));

    if (packet != NULL)
    {
        flags = IPC_PACKET_CONTROL | IPC_PACKET_RELIABLE;
        if (resetSeqNum)
        {
            flags |= IPC_PACKET_FIRST_ACKED;
        }
        ipcSetMsgFlags(packet, flags);

        rc = ipcSendCtrlSockPacket(portId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTLinkComplete failed %d\n", rc));
	    ipcFree(packet);
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER, ("Allocate RTRT_LINK_COMPLETE packet failed\n"));
        rc = IPCENOBUFS;
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTRTIpcAddressAssign
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Assign an address range to peer node, this message is transmitted via the
*   pseudo-socket, which can provide reliable data transmission.
*   
*   PARAMETER(S):
*       portId: the ID of desired port
*       
*   RETURN: return 0 on success
*           negative values on failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int RTRTIpcAddressAssign(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    int rc = IPC_OK;

    RTRTAddressAssignMsg_t* packet = (RTRTAddressAssignMsg_t*)ipcCreateRouterControlPacket(
            RTRT_ADDRESS_ASSIGN_DATA_SIZE,
            IPC_MSG_RTRT_IPC_ADDRESS_ASSIGN,
            IPC_INVALID_ADDR);

    if (packet != NULL)
    {
        ipcOsalWrite8(packet->ipcAddressRangeHigh, 
            ipcStack_glb.portToIpcAddrRangeTable[portId].highAddr);
        ipcOsalWrite8(packet->ipcAddressRangeLow,
            ipcStack_glb.portToIpcAddrRangeTable[portId].lowAddr);

        /* deliver data via control socket.
         * This is safe, because LINK_COMP must be sent prior to this message */
        rc = ipcSendCtrlSockPacket(portId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTLink failed %d\n", rc));
	    ipcFree(packet);
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER, ("Allocate RTRT_ADDRESS_ASSIGN packet failed\n"));
        rc = IPCENOBUFS;
    }

    return rc;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterDispatchPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcRouterDispatchPacket(ipcPacket_t* packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcChannelId_t channel;
    ipcMultihopChannelId_t multihopChannelId;
    ipcPacket_t *packetCopy;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/

    if ((ipcGetMsgFlags(packet) == IPC_PACKET_ACK) && 
        ((ipcGetDestServiceId(packet) == IPC_SRV_ROUTERCONTROL) ||
         (ipcGetDestServiceId(packet) == IPC_SRV_SESSIONCONTROL)))
    {
        COCOAckMsg_t* ack = (COCOAckMsg_t*)packet;
    
        ipcConnGotAck(ipcStack_glb.ports[portId].conn,
            ipcOsalRead8(ack->wasReliable),
            ipcOsalRead8(ack->seqNum),
            ipcOsalRead8(ack->maxSeqNum));
        ipcFree(packet);
        return;
    }

    if (ipcGetDestServiceId(packet) == IPC_SRV_ROUTERCONTROL)
    {
        switch (ipcGetMsgOpcode(packet))
        {
        case IPC_MSG_RTRT_LINK_REQUEST:
            ipcDebugLog(IPCLOG_ROUTER, ("Received RTRTLinkRequest on port %d\n", portId));
            handleRTRTLinkRequest((RTRTLinkRequestMsg_t*)packet, portId);
            break;
        case IPC_MSG_RTRT_LINK_REPLY:
            ipcDebugLog(IPCLOG_ROUTER, ("Received RTRTLinkReply on port %d\n", portId));
            handleRTRTLinkReply((RTRTLinkReplyMsg_t*)packet, portId);
            break;

        case IPC_MSG_RTRT_LINK_COMPLETE:
            ipcDebugLog(IPCLOG_ROUTER, ("Receive RTRTLinkComplete on port %d\n", portId));
            handleRTRTLinkComplete((RTRTLinkCompleteMsg_t*)packet, portId);
            break;

        case IPC_MSG_RTRT_IPC_ADDRESS_ASSIGN:
            ipcDebugLog(IPCLOG_ROUTER, ("Receive RTRTAddressAssign on port %d\n", portId));
            handleRTRTAddressAssign((RTRTAddressAssignMsg_t*)packet, portId);
            break;

        case IPC_MSG_RTRT_LINK_DOWN:
            ipcDebugLog(IPCLOG_ROUTER, ("Receive RTRTLinkDown on port %d\n", portId));
            handleRTRTLinkDown((RTRTLinkDownMsg_t *)packet, portId);
            break;

        case IPC_MSG_RTRT_CHANNEL_REQUEST:
            handleRTRTChannelRequest((RTRTChannelRequestMsg_t *)packet, portId);
            break;

        case IPC_MSG_RTRT_CHANNEL_REPLY:
            handleRTRTChannelReply((RTRTChannelReplyMsg_t *)packet, portId);
            break;

        case IPC_MSG_RTRT_CHANNEL_REL_REQUEST:
            handleRTRTChannelRelRequest((RTRTChannelRelRequestMsg_t *)packet, portId);
            break;

        default:
            ipcFree(packet);
            ipcError(IPCLOG_ROUTER, ("DVRTDataIndication -> invalid message opcode!\n"));
        } 
    }
    else if (ipcStack_glb.ports[portId].state == IPC_CONTROL_SOCKET_LINK_UP)
    {
        /* Not a router control message and link is up */

#ifndef IPC_NO_PACKET_LOG
        /* Log packet unless it is a log or control message */
        if (/*packet->channelId != IPC_CONTROL_CHANNEL_ID &&*/
            (ipcGetSrcServiceId(packet) != IPC_SRV_LOG_CONFIG) &&
            (ipcGetDestServiceId(packet) != IPC_SRV_LOG_CONFIG))
        {
            ipcLogPacket(packet);
        }
#endif

        if (ipcGetDestAddr(packet) == ipcStack_glb.ipcAddr)
        {
            /* This node is the final destination so pass it up to the session */
            if (RTSSDataIndication (packet) < 0)
            {
                ipcFree(packet);
            }
        }
        else /* This message is meant for another node */
        {
            portId = ipcAddrToPort(ipcGetDestAddr(packet));
            
            if (portId == IPC_INVALID_PORT)
            {
                ipcFree(packet);
                return;
            }
            if ((ipcRouterGetMemType(ipcGetDestAddr(packet)) == IPC_PHYSSHM) &&
                !ipcIsPhysicalSharedMemory(packet))
            {
                /* The next hop to the destination is through a physical shared memory port
                 * so we must copy the packet to physical shared memory */
                packetCopy = ipcCopyPacket(packet, IPC_PHYSSHM);
                ipcFree(packet);
                packet = packetCopy;
                
                if (packet == NULL)
                {
                    ipcError(IPCLOG_ROUTER, ("Failed to copy data packet to phys shared mem\n"));
                    return;
                }
            }

            if (ipcIsCtrlSocketPacket(packet))
            {
                rc = ipcSendCtrlSockPacket(portId, packet);
            }
            else
            {
                /* This message came in on a dedicated channel */
                multihopChannelId = ipcGetMultihopChannelId(ipcGetSrcAddr(packet),
                    ipcGetDestAddr(packet), ipcGetSrcServiceId(packet));

                channel = ipcMultihopToLocalChannel(multihopChannelId);

                if (channel == IPC_INVALID_CHANNEL_ID)
                {
                    channel = IPC_NONDEDICATED_CHANNEL_ID;
                }
                rc = RTDVDataRequest(channel, packet, portId);
            }
            if (rc != IPC_OK)
            {
                ipcError(IPCLOG_ROUTER, ("Failed to forward data packet\n"));
                ipcFree(packet);
            }
        }
    }
    else
    {
        /* Not a router control message and link is down */

        ipcWarning(IPCLOG_ROUTER, ("Received data packet while link down\n"));
        ipcFree(packet);
        /* Send an RTRTLinkDown message to tell the other side we are down */
        RTRTLinkDown(portId);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTLinkDown
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used handle the link down message from the peer node, this
*   happens when our peer node has lost its Connection to upper node, or it
*   thinks it has lost the Connection to us.
*   
*   PARAMETER(S):
*       packet: the packet from peer node
*       portId: the port from which the the message arrives
*       
*   RETURN: IPC_OK on success
*           negative values on failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void handleRTRTLinkDown(RTRTLinkDownMsg_t *packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcRouterForceLinkDown(portId);

    /* We are done with this packet so free it */
    ipcFree(packet);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTLinkRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle the link request from peer node. Our node
*   will only handle this message when we think the peer node is not linked with
*   us, we will send out a reply telling it we got the message.
*   
*   PARAMETER(S):
*       packet: the link request message
*       portId: the port from which the message arrives
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation
* 19Oct2005  Motorola Inc.         Added more log info 
*
*****************************************************************************************/
void handleRTRTLinkRequest(RTRTLinkRequestMsg_t* packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcPort_t* port = &ipcStack_glb.ports[portId];
    
    if (port->state != IPC_CONTROL_SOCKET_LINK_UP)
    {
        if ((ipcOsalRead8(packet->myAddr) == IPC_INVALID_ADDR) && 
            (ipcStack_glb.ipcAddr != IPC_INVALID_ADDR))
        {
            if (RTRTLinkReply(portId) == IPC_OK)
            {
                ipcRouterEnterLinkPendingState(portId);
            }
            else
            {
                ipcError(IPCLOG_ROUTER,
                    ("RTRTLinkReply failed on port %d\n", portId));
            }
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER,
            ("Got RTRTLinkRequest packet in LINK_UP state on port %d\n", portId));
        ipcRouterForceLinkDown(portId);
    }
    
    ipcFree(packet);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTLinkReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function handles the link reply from peer node, which is a reply to
*   our request. We send out a link complete message, and change our state to 
*   LINK_PENDING, waiting for the link complete message.
*   If we think the peer is already linked, we just ignore this message.
*   
*   PARAMETER(S):
*       packet: the link reply message
*       portId: the port from which the message arrives
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Sep2005  Motorola Inc.         Initial Creation
* 19Oct2005  Motorola Inc.         Added more log info 
*
*****************************************************************************************/
void handleRTRTLinkReply(RTRTLinkReplyMsg_t* packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.ports[portId].state != IPC_CONTROL_SOCKET_LINK_UP)
    {
        if (RTRTLinkComplete(portId, TRUE) == IPC_OK)
        {
            ipcRouterEnterLinkPendingState(portId);
        }
        else
        {
            ipcError(IPCLOG_ROUTER,
                ("Failed to send RTRTLinkComplete on port %d\n", portId));
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER,
            ("Got RTRTLinkReply packet but state is LINK_UP, on port %d\n",
                portId));
    }

    ipcFree(packet);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTLinkComplete
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to handle link complete message from the peer node.
*   We only handle this message in LINK_PENDING state. When we got this message,
*   the state of the port will be changed to LINK_UP, and we'll send out
*   a message to tell our peer node the address range for it if we have an
*   address.
*   In the LINK_UP state, router layer will send out link complete message
*   periodically.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Sep2005  Motorola Inc.         Initial Creation
* 19Oct2005  Motorola Inc.         Enter link up only in LINK_PENDING 
*
*****************************************************************************************/
void handleRTRTLinkComplete(RTRTLinkCompleteMsg_t* packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.ports[portId].state == IPC_CONTROL_SOCKET_LINK_PENDING)
    {
        ipcRouterEnterLinkUpState(portId);
        
        /* send back a compleet message */
        if (RTRTLinkComplete(portId, TRUE) == IPC_OK)
        {
            if (ipcStack_glb.ipcAddr != IPC_INVALID_ADDR)
            {
                if (RTRTIpcAddressAssign(portId) != IPC_OK)
                {
                    ipcError(IPCLOG_ROUTER,
                        ("Failed to send AddressAssign on port %d\n", portId));
                }
            }
        }
        else
        {
            ipcError(IPCLOG_ROUTER,
                ("Failed to send RTRTLinkComplete on port %d\n", portId));
        }
    }
    else if (ipcStack_glb.ports[portId].state == IPC_CONTROL_SOCKET_LINK_DOWN)
    {
        ipcError(IPCLOG_ROUTER,
            ("Got LINK_COMPLETE msg on port %d in LINK_DOWN\n", portId));
    }
    ipcFree(packet);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTAddressAssign
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function handles the address assign message from the peer node. If we 
*   already have an address when this message arrives, we'll shutdown the link
*   immediately, otherwise we'll accept the address range.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void handleRTRTAddressAssign(RTRTAddressAssignMsg_t* packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.ports[portId].state == IPC_CONTROL_SOCKET_LINK_UP)
    {
        if (ipcStack_glb.ipcAddr == IPC_INVALID_ADDR)
        {
            ipcStack_glb.higherPeerAddr = ipcGetSrcAddr(packet);
            ipcRouterSetAddressRange(ipcOsalRead8(packet->ipcAddressRangeLow), 
                    ipcOsalRead8(packet->ipcAddressRangeHigh), portId);
        }
        else
        {
            RTRTLinkDown(portId);
        }
        RTSSReady();
    }
    else
    {
        ipcError(IPCLOG_ROUTER,
            ("Got RTRTAddressAssign packet but state is not LINK_UP, on port %d\n",
             portId));
    }
    ipcFree(packet);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTChannelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to process a received channel request message from
*   another node.  It will first request a local channel from the device layer.
*   If this is successful, it will forward the message along to the next node in
*   the chain if there is one.
*   
*   PARAMETER(S):
*       packet: contains the RTRTChannelRequest message
*       portId: port ID on which this message was received
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
* 06Apr2007  Motorola Inc.         Change the dest PortId when forward request to the next node
*****************************************************************************************/
void handleRTRTChannelRequest(RTRTChannelRequestMsg_t *packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcChannelId_t localChannelId;
    ipcMultihopChannelId_t multihopChannelId;
    ipcAddress_t destAddr;
    ipcPortId_t destPortId;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.ports[portId].state != IPC_CONTROL_SOCKET_LINK_UP)
    {
        ipcWarning(IPCLOG_ROUTER, ("Received RTRTChannelRequest while port %d down", portId));
        ipcFree(packet);
        /* Send an RTRTLinkDown message to tell the other side we are down */
        RTRTLinkDown(portId);

        return;
    }
    
    multihopChannelId = ipcOsalRead32(packet->multihopChannelId);
    destAddr = ipcMultihopToDestIpcAddr(multihopChannelId);
    destPortId = ipcAddrToPort(destAddr);
   
    rc = RTDVChannelRequest(destPortId, ipcOsalRead32(packet->QoS));

    if (rc < 0)
    {
        /* The local channel request failed so send a reply back to the source */
        RTRTChannelReply(multihopChannelId, IPC_STATUS_NO_CHANNEL);

        /* We are done with this packet so free it */
        ipcFree(packet);
    }
    else
    {
        localChannelId = (ipcChannelId_t)rc;
        ipcAddMultihopChannel(multihopChannelId, localChannelId);

        /* The local channel request succeeded so check if final destination is peer node */
        if (ipcIsPeerNode(destAddr))
        {
            /* The final destination is a peer so send back the reply */
            RTRTChannelReply(multihopChannelId, IPC_STATUS_OK);

            /* We are done with this packet so free it */
            ipcFree(packet);
        }
        else
        {
            /* We have not reached the final destination so send it along to the next node */
            rc = ipcSendCtrlSockPacket(destPortId, (ipcPacket_t*)packet);

            if (rc != IPC_OK)
            {
                /* We failed to send the message along so close the channel and 
                 * reply with a failure */
                RTDVChannelRelRequest(localChannelId, destPortId);
                ipcRemoveMultihopChannel(multihopChannelId);

                RTRTChannelReply(multihopChannelId, IPC_STATUS_NO_CHANNEL);

                /* We are done with this packet so free it */
                ipcFree(packet);
            }
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTChannelReply
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to process a received channel reply message from
*   another node.  If the reply status is a failure, it will free the local
*   channel it had previously requested.  If this node did not originally request
*   the channel, it will forward the message along to the next node in the chain.
*   Otherwise, it will notify the session layer that the multihop channel request
*   has been completed by signaling the multihopChannelSema.
*   
*   PARAMETER(S):
*       packet: contains the RTRTChannelReply message
*       portId: port ID on which this message was received
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
*
*****************************************************************************************/
void handleRTRTChannelReply(RTRTChannelReplyMsg_t *packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcMultihopChannelId_t multihopChannelId;
    ipcChannelId_t localChannelId;
    ipcAddress_t srcAddr;
    ipcAddress_t destAddr;
    ipcPortId_t destPortId;
    UINT8 status;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.ports[portId].state != IPC_CONTROL_SOCKET_LINK_UP)
    {
        ipcWarning(IPCLOG_ROUTER, ("Received RTRTChannelReply while port %d down", portId));
        ipcFree(packet);
        /* Send an RTRTLinkDown message to tell the other side we are down */
        RTRTLinkDown(portId);

        return;
    }

    multihopChannelId = ipcOsalRead32(packet->multihopChannelId);
    localChannelId = ipcMultihopToLocalChannel(multihopChannelId);
    srcAddr = ipcMultihopToSrcIpcAddr(multihopChannelId);
    destAddr = ipcMultihopToDestIpcAddr(multihopChannelId);
    destPortId = ipcAddrToPort(destAddr);
    
    status = ipcOsalRead8(packet->status);

    if (status != IPC_STATUS_OK)
    {
        /* It was a failure so remove this channel */
        RTDVChannelRelRequest(localChannelId, destPortId);
        ipcRemoveMultihopChannel(multihopChannelId);
    }

    if (srcAddr == ipcStack_glb.ipcAddr)
    {
        /* This node originally requested the channel */
        if (ipcStack_glb.multihopChannelPending != multihopChannelId)
        {
            /* We are no longer waiting for this reply so it must have timed out. */
            ipcWarning(IPCLOG_ROUTER, ("Warning - Latent RTRT Channel Reply\n"));
        }
        else
        {
            /* We were waiting for this channel reply so store the channel ID (or error code)
             * and wake up the component that requested the channel */
            if (status == IPC_OK)
            {
                ipcStack_glb.multihopChannelReply = localChannelId;
            }
            else
            {
                ipcStack_glb.multihopChannelReply = IPCEQOS;
            }

            /* Clear the pending channel ID since we are no longer waiting for this reply */
            ipcStack_glb.multihopChannelPending = 0;
            ipcOsalSemaSignal(ipcStack_glb.multihopChannelSema);
        }

        /* We are done with this packet so free it */
        ipcFree(packet);
    }
    else
    {
        /* This node did not originally request the channel so forward it along */
        destPortId = ipcAddrToPort(srcAddr);

        rc = ipcSendCtrlSockPacket(destPortId, (ipcPacket_t*)packet);

        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTChannelReply failed to send: %d\n", rc));
            ipcFree(packet);
        }

    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleRTRTChannelRelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to process a received channel release request message
*   from another node.  It will free the local channel and forward it on to the
*   next node in the chain if there is one.
*   
*   PARAMETER(S):
*       packet: contains the RTRTChannelRelRequest message
*       portId: port ID on which this message was received
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
*
*****************************************************************************************/
void handleRTRTChannelRelRequest(RTRTChannelRelRequestMsg_t *packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcMultihopChannelId_t multihopChannelId;
    ipcChannelId_t localChannelId;
    ipcAddress_t destAddr;
    ipcPortId_t destPortId;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.ports[portId].state != IPC_CONTROL_SOCKET_LINK_UP)
    {
        ipcWarning(IPCLOG_ROUTER, ("Received RTRTChannelRelRequest while port %d down", portId));
        ipcFree(packet);
        /* Send an RTRTLinkDown message to tell the other side we are down */
        RTRTLinkDown(portId);

        return;
    }

    multihopChannelId = ipcOsalRead32(packet->multihopChannelId);
    localChannelId = ipcMultihopToLocalChannel(multihopChannelId);
    destAddr = ipcMultihopToDestIpcAddr(multihopChannelId);
    destPortId = ipcAddrToPort(destAddr);
    
    RTDVChannelRelRequest(localChannelId, destPortId);
    ipcRemoveMultihopChannel(multihopChannelId);

    if (ipcIsPeerNode(destAddr))
    {
        /* The final destination is a peer so we are done */
        ipcFree(packet);
    }
    else
    {
        /* We have not reached the final destination yet so forward it along */
        destPortId = ipcAddrToPort(destAddr);

        rc = ipcSendCtrlSockPacket(destPortId, (ipcPacket_t*)packet);
        if (rc != IPC_OK)
        {
            ipcError(IPCLOG_ROUTER, ("RTRTChannelRelRequest failed to send: %d\n", rc));
            ipcFree(packet);
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDoRouterTimerJob
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Do the real job of router timer.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
void ipcDoRouterTimerJob(void* val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPortId_t portId = ipcCastPtr(val, ipcPortId_t);
    ipcPort_t* port = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if (portId < ipcStack_glb.numOpenPorts)
    {
        port = &ipcStack_glb.ports[portId];
        switch (port->state)
        {
            case IPC_CONTROL_SOCKET_LINK_DOWN:
                RTRTLinkRequest(portId);
                ipcOsalTimerStart(port->timer);
                break;
            case IPC_CONTROL_SOCKET_LINK_PENDING:
                ipcRouterEnterLinkDownState(portId);
                break;
            case IPC_CONTROL_SOCKET_LINK_UP:
                RTRTLinkComplete(portId, FALSE);
                ipcOsalTimerStart(port->timer);
                break;
        }
    }
    else
    {
        ipcError(IPCLOG_ROUTER,
                 ("ipcDoRouterTimerJob: port id %d not valid\n",
                  portId));
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDoConnTimerJob
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Do the real job of Connection timer.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 20Sep2005  Motorola Inc.         Initial Creation
* 16Nov2005  Motorola Inc.         checked return value of ipcConnTimerTick
* 01Nov2006  Motorola Inc.         Change conn timer to nonperiodic 
*
*****************************************************************************************/
void ipcDoConnTimerJob(void* val)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int numPackets = 0;
    int packetsInConn = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcListLink_t prev = NULL;
    ipcListLink_t curr = NULL;
    ipcConn_t* conn = NULL;

    /* reuse mutex in receivedPacketQueue for ConnList */
    if (ipcOsalMutexLock(ipcStack_glb.connMutex) < 0)
    {
        ipcError(IPCLOG_GENERAL,
                ("ipcDoConnTimerJob: Failed to lock connMutex\n"));
        return;
    }

    for (ipcListFirstPair(ipcStack_glb.connList, prev, curr);
        curr != NULL;)
    {
        conn = (ipcConn_t*)curr;
        packetsInConn = conn->sentPackets.size;
        numPackets += packetsInConn;

        if ((packetsInConn != 0) && 
            (ipcConnTimerTick(conn) < 0))
        {
            if (prev == NULL)
            {
                curr = ipcStack_glb.connList.head;
            }
            else
            {
                curr = prev->next;
            }
        }
        else
        {
            ipcListNextPair(prev, curr);
        }
    }
    if (numPackets != 0)
    {
        ipcOsalTimerStart(ipcStack_glb.connTimer);
    }
    ipcOsalMutexRelease(ipcStack_glb.connMutex);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockTxWindowFull
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when the Tx window of a port is full.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCtrlSockTxWindowFull(ipcConn_t* conn, BOOL mayBlock)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcWarning(IPCLOG_GENERAL,
        ("Port[%d] TxWindow is full\n",
         ipcCastPtr(conn->privateData, ipcPortId_t)));
    return IPCEWOULDBLOCK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockTxWindowAvailable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when the Tx window gets available.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation
* 02Nov2005  Motorola Inc.         Added error checking 
* 13Jul2007  Motorola Inc.         Removed compile condition HAVE_ROUTER_PENDING
*
*****************************************************************************************/
void ipcCtrlSockTxWindowAvailable(ipcConn_t* conn)
{
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockSentListEmpty
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCtrlSockSentListEmpty(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return 0;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockLock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCtrlSockLock(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    port = &ipcStack_glb.ports[ipcCastPtr(conn->privateData, ipcPortId_t)];

    return ipcOsalMutexLock(port->mutex);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockUnlock
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCtrlSockUnlock(ipcConn_t* conn)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    port = &ipcStack_glb.ports[ipcCastPtr(conn->privateData, ipcPortId_t)];

    return ipcOsalMutexRelease(port->mutex);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockDataDeliver
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCtrlSockDataDeliver(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcPortId_t portId = ipcCastPtr(conn->privateData, ipcPortId_t);

    return RTDVDataRequest(IPC_CONTROL_CHANNEL_ID, packet, portId);
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockDataArrival
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcCtrlSockDataArrival(ipcConn_t* conn, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcConnConsumeData(conn, IPC_INVALID_ADDR, IPC_SRV_ROUTERCONTROL);
    ipcRouterDispatchPacket(packet, ipcGetPortId(packet));
    return IPC_OK;
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCtrlSockMaxRetries
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 19Sep2005  Motorola Inc.         Initial Creation 
* 26Apr2007  Motorola Inc.         Reset BP on max retries
*
*****************************************************************************************/
void ipcCtrlSockMaxRetries(ipcConn_t* conn, ipcPacket_t* pack)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcPortId_t portId = ipcCastPtr(conn->privateData, ipcPortId_t);

    ipcRouterForceLinkDown(portId);
#if (defined HAVE_PHYSICAL_SHM) && (!defined IPC_RELEASE)
    if (ipcStack_glb.ports[portId].deviceType == IPC_DEVICE_SHARED_MEMORY)
    {
        ipcError(IPCLOG_ROUTER,
                ("IPC losts its connection to BP, resetting BP!\n"));
        muTriggerGpi(MUGPI3); /* Reset BP */
    }
#endif
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcSendCtrlSockPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Send packet out from control socket.
*   
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Oct2005  Motorola Inc.         Initial Creation 
* 13Jul2007  Motorola Inc.         Removed compile condition HAVE_ROUTER_PENDING
*
*****************************************************************************************/
int ipcSendCtrlSockPacket(ipcPortId_t portId, ipcPacket_t* packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = &ipcStack_glb.ports[portId];
    ipcConn_t* conn = port->conn;
    int rc = IPCEFAULT;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((packet != NULL) && (conn != NULL))
    {
        ipcSetMsgFlags(packet, 
                ipcGetMsgFlags(packet) | IPC_PACKET_RELIABLE | IPC_PACKET_CONTROL);

        rc = ipcConnDeliverData(conn, packet, TRUE, FALSE);
    }
    return rc;
}
