/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_router_interface.c
*   FUNCTION NAME(S): DVRTDataIndication
*                     SSRTChannelRelRequest
*                     SSRTChannelRequest
*                     SSRTDataRequest
*                     handleDVRTDataIndication
*                     ipcRouterCloseConnection
*                     ipcRouterDestroy
*                     ipcRouterGetMemType
*                     ipcRouterGetPendingChannelId
*                     ipcRouterInit
*                     ipcRouterOpenConnection
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation
* 09Feb2005  Motorola Inc.         Updated for sockets
* 05Jul2005  Motorola Inc.         Added ipcRouterGetMemType
* 26Jul2005  Motorola Inc.         Log incoming packet data
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 13Jul2007  Motorola Inc.         Removed compile condition HAVE_ROUTER_PENDING
*
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_common.h"
#include "ipc_router_interface.h"
#include "ipc_router_internal.h"
#include "ipc_device_interface.h"
#include "ipc_osal.h"
#include "ipc_session.h"
#include "ipc_device_layer.h"
#include "ipc_packet_log_internal.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to initialize the router layer.  If this is the server
*   node, it will assign IPC address ranges to each port.  Otherwise, it will
*   start the address request timer.
*   
*   PARAMETER(S):
*       void
*                  
*   RETURN: TRUE if successful
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Feb2005  Motorola Inc.         Initial Creation.
* 02Nov2005  Motorola Inc.         Added error checking
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
BOOL ipcRouterInit(void)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcStack_glb.higherPeerAddr = IPC_INVALID_ADDR;

    ipcStack_glb.multihopChannelPending = 0;

    if (!ipcHashInit(&ipcStack_glb.multihopChannelTable, 11, NULL))
    {
        return FALSE;
    }

    if (ipcOsalSemaOpen(&ipcStack_glb.multihopChannelSema, 0) != IPCE_OK)
    {
        ipcError(IPCLOG_ROUTER, ("Error opening multihopChannelSema\n"));
        return FALSE;
    }

    if (ipcOsalMutexOpen(&ipcStack_glb.multihopChannelMutex) != IPCE_OK)
    {
        ipcError(IPCLOG_ROUTER, ("Error opening multihopChannelMutex\n"));
        return FALSE;
    }

    if (ipcStack_glb.isServer)
    {
        /* If this is the server node, we can already assign IPC Address ranges
         * to all the ports.  We will pass in IPC_INVALID_PORT because there is
         * no higher peer in the chain. */
        ipcRouterSetAddressRange(0, IPC_SERVER_ADDR, IPC_INVALID_PORT);
    }
    else
    {
        /* Initialzie all entries to INVALID for client nodes */
        ipcRouterSetAddressRange(IPC_INVALID_ADDR, IPC_INVALID_ADDR, IPC_INVALID_PORT);        
    }

    for (i = 0; i < ipcStack_glb.numOpenPorts; i++)
    {
        ipcPort_t* port = &ipcStack_glb.ports[i];

        ipcOsalTimerOpen(&port->timer);
        ipcRouterEnterLinkDownState(i);
    }

    /* initialize connection data structures */
    ipcOsalTimerOpen(&ipcStack_glb.connTimer);
    ipcOsalTimerInit(ipcStack_glb.connTimer,
        IPC_CONN_TIMEOUT_VAL, ipcDoConnTimerJob, NULL);
    ipcOsalTimerStart(ipcStack_glb.connTimer);

    return TRUE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to free all router layer resources.
*   
*   PARAMETER(S):
*       void
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09May2005  Motorola Inc.         Initial Creation.
* 25May2005  Motorola Inc.         Broadcast RTRTLinkDown to all port  when router is destroye
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
void ipcRouterDestroy()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *packet;
    ipcPortId_t destPortId;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Tell all neighbor nodes I will link down */
    for (destPortId = 0; destPortId < ipcStack_glb.numOpenPorts; destPortId++)
    {
        RTRTLinkDown(destPortId);
        ipcOsalTimerClose(ipcStack_glb.ports[destPortId].timer);
    }
    
    /* Free the queue */
    while (NULL != (packet = (ipcPacket_t *)ipcQueueGetLink(
        &ipcStack_glb.receivedPacketQueue, IPC_QUEUE_NON_BLOCKING, NULL)))
    {
        ipcFree(packet);
    }

    /* the mutex is not closed before this function call */
    if (ipcOsalMutexLock(ipcStack_glb.receivedPacketQueue.mutex) == IPCE_OK)
    {
        ipcStack_glb.connList.head = NULL;
        ipcOsalMutexRelease(ipcStack_glb.receivedPacketQueue.mutex);
    }

    /* Close conn timer */
    ipcOsalTimerClose(ipcStack_glb.connTimer);

    /* Destroy multihop channel table */
    ipcHashDestroy(&ipcStack_glb.multihopChannelTable);

    /* destroy routerOps */
    if (ipcStack_glb.routerOps != NULL)
    {
        ipcFree(ipcStack_glb.routerOps);
        ipcStack_glb.routerOps = NULL;
    }
    ipcOsalSemaClose(ipcStack_glb.multihopChannelSema);
    ipcOsalMutexClose(ipcStack_glb.multihopChannelMutex);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterGetPendingChannelId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function returns the local channelId assigned for the currently
*   pending multihop channel request (or IPC_INVALID_CHANNEL if no multihop
*   channel request is pending).
*   
*   PARAMETER(S):
*       void
*                  
*   RETURN: the local channelId assigned for the currently pending multihop
*           channel request (or IPC_INVALID_CHANNEL if no multihop channel
*           request is pending).
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Feb2005  Motorola Inc.         Initial Creation. 
*
*****************************************************************************************/
ipcChannelId_t ipcRouterGetPendingChannelId()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcChannelId_t channelId;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.multihopChannelPending == 0)
    {
        channelId = IPC_INVALID_CHANNEL_ID;
    }
    else
    {
        channelId = (ipcChannelId_t)(ipcStack_glb.multihopChannelPending & 0x000000FF);
    }

    return channelId;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSRTChannelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC session manager to request a channel from the
*   device layer through the router layer.
*   
*   PARAMETER(S):
*       ipcAddr: This is the IPC address of the destination node.
*       QoS: This is the requested quality of service.
*                               to bother with CRC or not
*                  
*   RETURN: IPC_OK for success or appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets
* 27Jun2005  Motorola Inc.         Added final address to channal
* 22Aug2005  Motorola Inc.         Added a param for shm channels
* 17Aug2005  Motorola Inc.         Checked the return value of ipcGetDevice() 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int SSRTChannelRequest(ipcAddress_t ipcAddr, ipcQoS_t QoS, ipcServiceId_t srcService)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPortId_t portId;
    ipcChannelId_t channelId;
    ipcTxChannel_t *channel = NULL;
    ipcDevice_t* device = NULL;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    /* First find which port the IPC Address is on */
    portId = ipcAddrToPort(ipcAddr);

    /* Next request from the device the QoS to that port */
    rc = RTDVChannelRequest(portId, QoS);

    if (rc >= 0) /* otherwise, channel request failed */
    {
        channelId = (ipcChannelId_t)rc;
        device = ipcGetDevice(portId);
        if (NULL == device)
        {
            RTDVChannelRelRequest(channelId, portId);
            ipcError(IPCLOG_ROUTER, ("SSRTChannelRequest ERROR: Invalid Port ID: %d\n", portId));
            return IPCENODE;
        }
        
        channel = device->tx.channelPointers[rc];
        channel->finalAddr = ipcAddr;

        /* Check to see if this address is a peer */
        if (!ipcIsPeerNode(ipcAddr))
        {
            /* Since the IPC Address wasn't a peer, the RTDVChannelRequest will not be sufficient.
             * We must also send an RTRTChannelRequest for the next hop.  Only one multihop
             * channel request can be active at a time, so lock the mutex */
            if (ipcOsalMutexLock(ipcStack_glb.multihopChannelMutex) < 0)
            {
                ipcError(IPCLOG_DEVICE, 
                        ("SSRTChannelRequest: Failed to lock ipcStack_glb.multihopChannelMutex\n"));
                /* XXX: Not going to release the allocated channel */
                return IPCEBUSY;
            }

            /* Determine the Multihop Channel ID for this channel and store it in the
             * global variable to keep track of the current pending channel request */
            ipcStack_glb.multihopChannelPending =
                ipcGetMultihopChannelId(ipcStack_glb.ipcAddr, ipcAddr, srcService);

            /* Add the multihop channel to the hash table */
            ipcAddMultihopChannel(ipcStack_glb.multihopChannelPending, channelId);

            rc = RTRTChannelRequest(ipcStack_glb.multihopChannelPending, QoS);
            if (rc == IPC_OK)
            {
                /* Tell the session layer that the channel request is still pending */
                rc = IPCENOTREADY;
            }
            else
            {
                ipcRemoveMultihopChannel(ipcStack_glb.multihopChannelPending);
                RTDVChannelRelRequest(channelId, portId);
                ipcOsalMutexRelease(ipcStack_glb.multihopChannelMutex);
            }
        }
        /* else, the channel has been allocated so it can be used. */
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSRTChannelRelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC session manager to request a channel be removed
*   by the device layer through the router layer.
*   
*   PARAMETER(S):
*       channelID: This is the IPC channel to be removed.
*       ipcAddr: This is the IPC address of the destination node.
*                  
*   RETURN: IPC_OK for success or appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets
* 13Mar2006  Motorola Inc.         Check portId 
*
*****************************************************************************************/
int SSRTChannelRelRequest(ipcChannelId_t channelId, ipcAddress_t ipcAddr, 
                          ipcServiceId_t srcService)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcMultihopChannelId_t multihopChannelId;
    ipcPortId_t portId;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Find the port from the destination */
    portId = ipcAddrToPort(ipcAddr);

    if (portId == IPC_INVALID_PORT)
    {
        return IPCENETUNREACH;
    }
    /* Alert the device layer to free the channel */
    rc = RTDVChannelRelRequest(channelId, portId);

    if ((rc == IPC_OK) && !ipcIsPeerNode(ipcAddr))
    {
        /* This is not a peer node so we must also send an RTRTChannelRelRequest to
         * free the multihop channel for all remaining hops.  We will also remove it
         * from the multihop channel table */
        multihopChannelId = ipcGetMultihopChannelId(ipcStack_glb.ipcAddr, ipcAddr, 
            srcService);
        ipcRemoveMultihopChannel(multihopChannelId);
        rc = RTRTChannelRelRequest(multihopChannelId);

        /* If this channel was still pending, clear the pending channel ID */
        if (channelId == ipcRouterGetPendingChannelId())
        {
            ipcStack_glb.multihopChannelPending = 0;
        }
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: SSRTDataRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC session manager to send packet data on a 
*   pre-assigned IPC channel.
*   
*   PARAMETER(S):
*       packet: This is a pointer to the IPC data and header to be sent
*                  
*   RETURN: IPC_OK for success or appropriate error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets
* 03Nov2005  Motorola Inc.         Added support for local node
* 10Nov2005  Motorola Inc.         Check packet validation
* 13Jul2006  Motorola Inc.         Lock connMutex for local packets
* 09Aug2006  Motorola Inc.         Put local packets to main queue
* 18Aug2006  Motorola Inc.         Only put local control packets in  main queue
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
*
*****************************************************************************************/
int SSRTDataRequest(ipcPacket_t *packet, ipcChannelId_t channelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPortId_t portId;
    int rc = IPC_OK;
    BOOL isControlPacket = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    if( packet ==  NULL ) 
    {
        return IPCENOBUFS;
    }
    isControlPacket = ipcIsCtrlSocketPacket(packet);

#ifndef IPC_NO_PACKET_LOG
    if ((ipcGetDestServiceId(packet) != IPC_SRV_LOG_CONFIG) &&
        (ipcGetSrcServiceId(packet) != IPC_SRV_LOG_CONFIG))
    {
        ipcLogPacket(packet);
    }
#endif

    if (ipcGetDestAddr(packet) == ipcStack_glb.ipcAddr)
    {
        /* XXX: For packets delivered to the same node, only put control 
         * packets to router queue, others are delivered to session
         * layer directly.
         */
        if (isControlPacket)
        {
            DVRTDataIndication(packet, IPC_INVALID_PORT);
        }
        else
        {
            rc = RTSSDataIndication(packet);
        }
    } 
    else
    {
        portId = ipcAddrToPort(ipcGetDestAddr(packet));

        if (portId != IPC_INVALID_PORT)
        {
            if (ipcStack_glb.ports[portId].state == IPC_CONTROL_SOCKET_LINK_UP)
            {
                /* control packets go through the pseudo-socket
                */
                if (isControlPacket)
                {
                    rc = ipcConnDeliverData(ipcStack_glb.ports[portId].conn,
                            packet,
                            TRUE,
                            FALSE);
                }
                else
                {
                    rc = RTDVDataRequest(channelId, (ipcPacket_t *)packet, portId);
                }
            }
            else
            {
                rc = IPCENETUNREACH;
            }
        }
        else
        {
            rc = IPCENODE;
        }
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: DVRTDataIndication
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC device layer to alert the IPC router that a
*   new message has arrived.
*   
*   PARAMETER(S):
*       packet: Pointer to the data that has arrived
*       portId: identifies hardware port
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets
* 03Nov2005  Motorola Inc.         Added error checking 
*
*****************************************************************************************/
void DVRTDataIndication(ipcPacket_t *packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int queueFlags = 0;
/*--------------------------------------- CODE -----------------------------------------*/

    //printData("DVRTDataIndication", ipcGetMsgData(packet), ipcGetMsgLength(packet));
    if (ipcGetMsgFlags(packet) & IPC_PACKET_HIGH_PRIORITY)
    {
        queueFlags |= IPC_QUEUE_HIGH_PRIORITY;
    }

    ipcSetPortId(packet, portId);
    /* Send it to the router */
    if (ipcQueuePutLink(&ipcStack_glb.receivedPacketQueue,
        (ipcListLink_t)packet, queueFlags) < 0)
    {
        ipcError(IPCLOG_DEVICE, ("Failed to put packet to receivedPacketQueue\n"));
        ipcFree(packet);
    }
    else
    {
        /* Some platforms may require additional work to wake up the main IPC thread */
        ipcPlatformDataIndication();
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: handleDVRTDataIndication
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC router layer to process all packets received
*   by the device layer.
*   
*   PARAMETER(S):
*       packet: Pointer to the data that has arrived
*       portId: identifies hardware port
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets
* 26Jul2005  Motorola Inc.         Log incoming packet data
* 19Sep2005  Motorola Inc.         pseudo-socket change
* 19Oct2005  Motorola Inc.         Not use pseud-sock if link down
* 03Nov2005  Motorola Inc.         Added error checking
* 09Aug2006  Motorola Inc.         Handle local packets
* 17Jan2007  Motorola Inc.         Don't handle local packets 
*
*****************************************************************************************/
void handleDVRTDataIndication(ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPortId_t portId;
    BOOL isControlPacket;
/*--------------------------------------- CODE -----------------------------------------*/
    portId = ipcGetPortId(packet);
    isControlPacket = ipcIsCtrlSocketPacket(packet);

    if ((portId < ipcStack_glb.numOpenPorts) && isControlPacket &&
        (ipcStack_glb.ports[portId].state != IPC_CONTROL_SOCKET_LINK_DOWN))
    {
        if (ipcConnGotPacket(ipcStack_glb.ports[portId].conn, packet) < 0)
        {
            ipcFree(packet);
        }
    }
    else if (portId == IPC_INVALID_PORT)
    {
        if (RTSSDataIndication(packet) < 0)
        {
            ipcFree(packet);
        }
    }
    else
    {
        ipcRouterDispatchPacket(packet, portId);
    }
}

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterGetMemType
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function returns the type of memory that should be allocated to reach
*   the destination node (physical or virtual shared memory)
*   
*   PARAMETER(S):
*       destAddr: IPC Address of destination node
*                  
*   RETURN: the flag that should be passed into ipcAlloc or ipczalloc
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 05Jul2005  Motorola Inc.         Initial Creation
* 23Feb2007  Motorola Inc.         Updated memory type defs
*
*****************************************************************************************/
int ipcRouterGetMemType(ipcAddress_t destAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int memtype = IPC_PROCSHM;
    ipcPortId_t portId;
/*--------------------------------------- CODE -----------------------------------------*/
    portId = ipcAddrToPort(destAddr);
    if ((portId < ipcStack_glb.numOpenPorts) &&
        (ipcStack_glb.ports[portId].deviceType == IPC_DEVICE_SHARED_MEMORY))
    {
        memtype = IPC_PHYSSHM;
    }

    return memtype;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterOpenConnection
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by the device layer to open a reliable connection
*   on the specified port.
*   
*   PARAMETER(S):
*       portId: the port to open the connection on
*       device: pointer to device sturcture
*       deviceType: indicates type of device (RS232, SHARED_MEMORY, etc.)
*                  
*   RETURN: TRUE for success, FALSE for failure
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 19Mar2007  Motorola Inc.         Set window size to 120
* 13Jul2007  Motorola Inc.         Removed compile condition HAVE_ROUTER_PENDING
*
*****************************************************************************************/
BOOL ipcRouterOpenConnection(ipcPortId_t portId, void *device, ipcDeviceType_t deviceType)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcStack_glb.routerOps == NULL)
    {
        ipcListInit(&ipcStack_glb.connList);
        ipcStack_glb.routerOps = ipcAlloc(sizeof(ipcConnOperations_t), 0);

        if (ipcStack_glb.routerOps == NULL)
        {
            ipcError(IPCLOG_ROUTER, ("Out of memory while creating router ops\n"));
            return FALSE;
        }
        
        ipcStack_glb.routerOps->deliverData = ipcCtrlSockDataDeliver;
        ipcStack_glb.routerOps->lock = ipcCtrlSockLock;
        ipcStack_glb.routerOps->onDataArrival = ipcCtrlSockDataArrival;
        ipcStack_glb.routerOps->onMaxRetries = ipcCtrlSockMaxRetries;
        ipcStack_glb.routerOps->onSentListEmpty = ipcCtrlSockSentListEmpty;
        ipcStack_glb.routerOps->onTxWindowAvailable = ipcCtrlSockTxWindowAvailable;
        ipcStack_glb.routerOps->onTxWindowFull = ipcCtrlSockTxWindowFull;
        ipcStack_glb.routerOps->unlock = ipcCtrlSockUnlock;
    }

    ipcStack_glb.ports[portId].device = device;
    ipcStack_glb.ports[portId].deviceType = deviceType;
    ipcStack_glb.ports[portId].state = IPC_CONTROL_SOCKET_LINK_DOWN;
    ipcStack_glb.ports[portId].enabled = TRUE;

    if (ipcOsalMutexOpen(&ipcStack_glb.ports[portId].mutex) != IPCE_OK)
    {
        ipcError(IPCLOG_ROUTER, ("Can't open mutex for port %d\n", portId));
        return FALSE;
    }

    ipcStack_glb.ports[portId].conn = (ipcConn_t *)ipcAlloc(sizeof(ipcConn_t), 0);
    if (ipcStack_glb.ports[portId].conn == NULL)
    {
        ipcError(IPCLOG_ROUTER, ("Out of memory opening router connection\n"));
        ipcOsalMutexClose(ipcStack_glb.ports[portId].mutex);
        return FALSE;
    }

    /* initialize connection of the port */
    ipcConnInit(ipcStack_glb.ports[portId].conn, 120,
        ipcCastPtr(portId, void*),
        ipcStack_glb.routerOps);

    if (ipcOsalMutexLock(ipcStack_glb.connMutex) < 0)
    {
        return FALSE;
    }
    ipcConnDelegate(ipcStack_glb.ports[portId].conn);
    ipcOsalMutexRelease(ipcStack_glb.connMutex);


    ipcStack_glb.numOpenPorts++;
    
    return TRUE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterCloseConnection
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by the device layer to close a connection when the
*   corresponding device is closed.
*   
*   PARAMETER(S):
*       portId: the port to close the connection on
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 23Sep2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcRouterCloseConnection(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcOsalMutexLock(ipcStack_glb.connMutex) < 0)
    {
        ipcError(IPCLOG_ROUTER,
                ("ipcRouterCloseConnection: Failed to lock ipcStack_glb.connMutex, ignoring\n"));
    }
    ipcConnUndelegate(ipcStack_glb.ports[portId].conn);
    ipcOsalMutexRelease(ipcStack_glb.connMutex);

    ipcFree(ipcStack_glb.ports[portId].conn);
    ipcOsalMutexClose(ipcStack_glb.ports[portId].mutex);
    ipcStack_glb.ports[portId].state = IPC_CONTROL_SOCKET_LINK_DOWN;
    ipcStack_glb.numOpenPorts--;
}
