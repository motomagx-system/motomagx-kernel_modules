/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004, 2005, 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_router_util.c
*   FUNCTION NAME(S): ipcAddMultihopChannel
*                     ipcGetMultihopChannelId
*                     ipcIsPeerNode
*                     ipcMultihopToDestIpcAddr
*                     ipcMultihopToLocalChannel
*                     ipcMultihopToSrcIpcAddr
*                     ipcRemoveMultihopChannel
*                     ipcRouterSetAddressRange
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated to use hash table 
*****************************************************************************************/

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/

#include "ipc_hash.h"
#include "ipc_common.h"
#include "ipc_router_internal.h"
#include "ipc_session.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcIsPeerNode
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to check if an address is a peer node.
*   
*   PARAMETER(S):
*       channelId: the IPC channel to close
*       portId: identifies hardware port
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 09Feb2005  Motorola Inc.         Updated for sockets 
*
*****************************************************************************************/
BOOL ipcIsPeerNode(ipcAddress_t ipcAddr)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL returnval = FALSE;
    int i;
/*--------------------------------------- CODE -----------------------------------------*/
    /* All peer addresses can be determined from the port to ipc map table,
       and from the one peer which is a higher peer on the tree topology */
    if (ipcAddr == ipcStack_glb.higherPeerAddr)
    {
        returnval = TRUE;
    }
    else
    {
        /* Check the rest of the ports if this value is a peer address */
        for (i = 0; i < ipcStack_glb.numOpenPorts; i++)
        {
            if (ipcAddr == ipcStack_glb.portToIpcAddrRangeTable[i].highAddr)
            {
                returnval = TRUE;
                break;
            }
        }
    }

    return (returnval);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcGetMultihopChannelId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to generate a unique multihop channel ID based on the
*   local channel ID, and the source and destination IPC addresses.
*   
*   PARAMETER(S):
*       srcAddr: IPC address of originating node
*       destAddr: IPC address of channel destination node
*       channel: local channel ID at originating node
*       
*   RETURN: a unique multihop channel ID
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Feb2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcMultihopChannelId_t ipcGetMultihopChannelId(ipcAddress_t srcAddr,
                                               ipcAddress_t destAddr,
                                               ipcServiceId_t srcService)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcMultihopChannelId_t multihopChannelId;
/*--------------------------------------- CODE -----------------------------------------*/
    multihopChannelId = ( ((UINT32)srcAddr     ) << 24) |
                        ( ((UINT32)destAddr    ) << 16) |
                          ((UINT32)srcService  );

    return multihopChannelId;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAddMultihopChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to add a multihop channel to the hash table
*   
*   PARAMETER(S):
*       multihopChannelId: unique reference ID used for multihop channels
*       localChannelId: the channel ID used within this node
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 08Feb2004  Motorola Inc.         Update to use hash table
* 02Nov2005  Motorola Inc.         Added error checking 
*
*****************************************************************************************/
void ipcAddMultihopChannel(ipcMultihopChannelId_t multihopChannelId,
                           ipcChannelId_t localChannelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if(!ipcHashPut(&ipcStack_glb.multihopChannelTable,
        (void *)multihopChannelId, ipcCastPtr(localChannelId, void*)))
    {
        ipcError(IPCLOG_ROUTER, ("Failed to add multihop channel"));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveMultihopChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to remove a multihop channel from the hash table
*   
*   PARAMETER(S):
*       multihopChannelId: unique reference ID used for multihop channels
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 08Feb2004  Motorola Inc.         Update to use hash table 
*
*****************************************************************************************/
void ipcRemoveMultihopChannel(ipcMultihopChannelId_t multihopChannelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcHashRemove(&ipcStack_glb.multihopChannelTable, (void *)multihopChannelId);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMultihopToLocalChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to convert a multihop channel ID to the corresponding
*   local channel ID
*   
*   PARAMETER(S):
*       multihopChannelId: unique reference ID used for multihop channels
*       
*   RETURN: the corresponding local channel ID
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27May2004  Motorola Inc.         Initial Creation.
* 08Feb2004  Motorola Inc.         Update to use hash table 
*
*****************************************************************************************/
ipcChannelId_t ipcMultihopToLocalChannel(ipcMultihopChannelId_t multihopChannelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcChannelId_t localChannel = IPC_INVALID_CHANNEL_ID;
    void *object;
/*--------------------------------------- CODE -----------------------------------------*/
    object = ipcHashFind(&ipcStack_glb.multihopChannelTable, (void *)multihopChannelId);
    if (object != NULL)
    {
        localChannel = ipcCastPtr(object, ipcChannelId_t);
    }

    return localChannel;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMultihopToSrcIpcAddr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to obtain the source IPC address for a multihop channel
*   
*   PARAMETER(S):
*       multihopChannelId: unique reference ID used for multihop channels
*       
*   RETURN: the source IPC address for this channel
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Feb2004  Motorola Inc.         Initial Creation 
* 30Mar2007  Motorola Inc.         Revised bits offset
*
*****************************************************************************************/
ipcAddress_t ipcMultihopToSrcIpcAddr(ipcMultihopChannelId_t multihopChannelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (ipcAddress_t)((multihopChannelId >> 24) & 0x000000FF);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcMultihopToDestIpcAddr
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to obtain the destination IPC address for a multihop channel
*   
*   PARAMETER(S):
*       multihopChannelId: unique reference ID used for multihop channels
*       
*   RETURN: the destination IPC address for this channel
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Feb2004  Motorola Inc.         Initial Creation 
* 30Mar2007  Motorola Inc.         Revised bits offset
*
*****************************************************************************************/
ipcAddress_t ipcMultihopToDestIpcAddr(ipcMultihopChannelId_t multihopChannelId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    return (ipcAddress_t)((multihopChannelId >> 16) & 0x000000FF);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRouterSetAddressRange
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to assign a range of address to each port (except for
*   the hither peer port if there is one)
*   
*   PARAMETER(S):
*       lowAddr: the low IPC address in our assigned range
*       highAddr: the high IPC address in our assigned range
*       highPeerPortId: the port ID for our higher peer node (it is set to
*                                   IPC_INVALID_PORT for the server node)
*       
*   RETURN: the destination IPC address for this channel
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Feb2004  Motorola Inc.         Initial Creation
* 23May2005  Motorola Inc.         Checked address range divide result
*
*****************************************************************************************/
void ipcRouterSetAddressRange(ipcAddress_t lowAddr, ipcAddress_t highAddr,
                              ipcPortId_t highPeerPortId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPortId_t portId;
    ipcAddress_t ipcAddr = 0;
    int numPortsToDivideBy;
    int numAddrPerPort = 0;
    int numAddrLeft;
/*--------------------------------------- CODE -----------------------------------------*/

    /* Initialize the entire IPC Address to Port table to point to the higher peer */
    memset(ipcStack_glb.ipcAddrToPortTable, highPeerPortId,
        sizeof(ipcStack_glb.ipcAddrToPortTable));

    /* Our own address is the high address in the range */
    ipcStack_glb.ipcAddr = highAddr;
    ipcStack_glb.ipcAddrToPortTable[ipcStack_glb.ipcAddr] = IPC_INVALID_PORT;

    if (lowAddr != IPC_INVALID_ADDR)
    {
        /* If this function is called with IPC_INVALID_ADDR as the low address, then
         * our link to the higher peer is down and we no longer have an IPC address.
         * We do not need to perform the address range assignments. */

        /* We do not include the high address in the range because we have already
         * assigned it to ourselves */
        numAddrLeft = highAddr - lowAddr;

        /* We must divide this address range among all ports */
        numPortsToDivideBy = ipcStack_glb.numOpenPorts;
        if (highPeerPortId != IPC_INVALID_PORT)
        {
            /* If there is a higher port in the chain, it already has addresses assigned
             * to it so we do not need to include it here */
            --numPortsToDivideBy;
            
            /* The address range for the higher peer may not be continuous so we will
             * represent it with IPC_INVALID_ADDR for both low and high addresses */
            ipcStack_glb.portToIpcAddrRangeTable[highPeerPortId].lowAddr = IPC_INVALID_ADDR;
            ipcStack_glb.portToIpcAddrRangeTable[highPeerPortId].highAddr = IPC_INVALID_ADDR;
        }

        if (numPortsToDivideBy <= 0)
        {
            /* There are no more ports to assign addresses to so set the entire range
             * to IPC_INVALID_PORT in the table */
            memset(&ipcStack_glb.ipcAddrToPortTable[lowAddr], IPC_INVALID_PORT, numAddrLeft);
        }
        else
        {
            /* Find the number of addresses per port and the remainder after rounding down */
            numAddrPerPort = numAddrLeft / numPortsToDivideBy;
            numAddrLeft -= numAddrPerPort * numPortsToDivideBy;
        }

        if (numAddrPerPort <= 0)
        {
            /* the number of address is less than the number of port, not enough to divide */
            return;
        }

        for (portId = 0; portId < ipcStack_glb.numOpenPorts; portId++)
        {
            if (portId != highPeerPortId)
            {
                /* Assign addresses to all ports except the one leading to our higher peer
                 * (it already has addresses assigned to it) */
                highAddr = lowAddr + numAddrPerPort - 1;
                if (numAddrLeft > 0)
                {
                    /* Add an additional address to each port while there is still a remainder */
                    --numAddrLeft;
                    ++highAddr;
                }

                ipcStack_glb.portToIpcAddrRangeTable[portId].lowAddr = lowAddr;
                ipcStack_glb.portToIpcAddrRangeTable[portId].highAddr = highAddr;

                /* Populate this range in the IPC Address to Port table */
                for (ipcAddr = lowAddr; ipcAddr <= highAddr; ipcAddr++)
                {
                    ipcStack_glb.ipcAddrToPortTable[ipcAddr] = portId;
                }

                /* Set the low address for the next port */
                lowAddr = highAddr + 1;
            }
        }
    }
}
