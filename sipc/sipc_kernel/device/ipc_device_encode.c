/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2003 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_encode.c
*   FUNCTION NAME(S): ipcAddToTxBuffer
*                     ipcDeviceFreeCurrentPackets
*                     ipcDeviceFreePendingPackets
*                     ipcDeviceOutputThread
*                     ipcFlushTxBuffer
*                     ipcLatchPacketData
*                     ipcOutputChannelData
*                     ipcStartFrame
*                     ipcTransmitPacket
*                     ipcTransmitWithEscape
*                     ipcWaitForOutput
*                     ipcWakeUpOutputThread
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains internal device layer functions used to multiplex channel
*   data into a stream of frames
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 02Jun2004  Motorola Inc.         Converted from C++ to C
* 18Sep2005  Motorola Inc.         Removed acknowledged frames
* 02Oct2006  Motorola Inc.         Use OSAL memory access macros
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 13Jul2007  Motorola Inc.         Removed inclusion of ipc_device_debug.h
*****************************************************************************************/

#ifndef IPC_SIMPLE_DEVICE
/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_device_layer.h"
#include "ipc_device_interface.h"
#include "ipc_router_interface.h"
#include "ipc_osal.h"
#include "ipc_list.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

const UINT8 IPC_DEV_ESCAPE_DATA[IPC_DEV_ESCAPE_LENGTH]          = {IPC_DEV_ESCAPE_BYTE, IPC_DEV_ESCAPE_BYTE};
const UINT8 IPC_DEV_ESCAPE_START_FRAME[IPC_DEV_ESCAPE_LENGTH]   = {IPC_DEV_ESCAPE_BYTE, IPC_DEV_FRAME_START_BYTE};
const UINT8 IPC_DEV_ESCAPE_START_PACKET[IPC_DEV_ESCAPE_LENGTH]  = {IPC_DEV_ESCAPE_BYTE, IPC_DEV_PACKET_START_BYTE};

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

void ipcOutputChannelData(ipcDevice_t *device, ipcChannelId_t channelIds[], ipcTxChannel_t *txChannels[]);
void ipcAddToTxBuffer(ipcDevice_t *device, const UINT8 *data, INT32 length);
void ipcFlushTxBuffer(ipcDevice_t *device);
void ipcTransmitWithEscape(ipcDevice_t *device, const UINT8 *data, INT32 length);
void ipcStartFrame(ipcDevice_t *device);
void ipcLatchPacketData(ipcDevice_t *device);
int ipcWaitForOutput(ipcDevice_t *device);
void ipcDeviceFreeCurrentPackets(ipcDevice_t *device);

/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceOutputThread
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This thread continually tries to send data by multiplexing all the channel
*   data into a series of frames.
*   
*   When there is not much pending data, the frames will be small and the data
*   will be sent immediately.  Eventually, when more pending data is queued up,
*   larger frames will be sent, up to the maximum frame size as determined by
*   the frame time and device bandwidth.
*   
*   PARAMETER(S):
*       portId: port ID for device we are operating on
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 01Sep2004  Motorola Inc.         Changes for acknowledged channels
* 22Oct2004  Motorola Inc.         Updated for quick synchronization
* 23Dec2004  Motorola Inc.         Converted to separate thread
* 17Aug2005  Motorola Inc.         Check return value of ipcGetDevice()
*
*****************************************************************************************/
void ipcDeviceOutputThread(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Wait until the IPC Stack has completed initialization */
    while (ipcStack_glb.state != IPC_STACK_STATE_RUNNING)
    {
        if (ipcOsalSleep(50) < 0)
        {
            ipcError(IPCLOG_DEVICE, ("ipcDeviceOutputThread: killed by user\n"));
            return;
        }
    }

    device = ipcGetDevice(portId);
    if(NULL == device)
    {
        return;
    }
    
    while (!device->terminate)
    {
        if (device->freePackets)
        {
            if (ipcLockDevice(device) < 0)
            {
                ipcError(IPCLOG_GENERAL,
                        ("ipcDeviceOutputThread: Failed to lock device, going to sleep\n"));
                return;
            }
            ipcDeviceFreeCurrentPackets(device);
            device->freePackets = FALSE;
            ipcUnlockDevice(device);
        }

        /* Construct the next frame header */
        ipcStartFrame(device);

        if (device->tx.frameHeaderLength == 0)
        {
            /* There is no data to send so we must sleep until there is */
            if (ipcWaitForOutput(device) != IPCE_OK)
            {
                break;
            }
        }
        else /* We must send a new frame */
        {
            /* Output start of frame sequence for streamed frame */
            ipcAddToTxBuffer(device, IPC_DEV_ESCAPE_START_FRAME, IPC_DEV_ESCAPE_LENGTH);

            /* Output frame header */
            ipcTransmitWithEscape(device, device->tx.frameHeader,
                ipcGetFrameHeaderLength(device->tx.frameHeader));
        
            /* Output frame channel data */
            ipcOutputChannelData(device, device->tx.frameChannelIds, device->tx.channelPointers);

            /* We are done with the frame so flush the TX buffer */
            ipcFlushTxBuffer(device);

        } /* There was data to send */

    } /* while !device->terminate */

    ipcDebugLog(IPCLOG_DEVICE, ("Port %d output thread terminated\n", device->portId));
    /* set write function to NULL to signal that output thread has terminated */
    device->driver.write = NULL;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcTransmitPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by RTDVDataRequest to transmit an IPC packet for
*   all devices except shared memory
*   
*   PARAMETER(S):
*       portId: identifies hardware port
*       channelId: the IPC channel to transmit on
*       packet: pointer to IPC packet to send 
*                  
*   RETURN: success or error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 07Apr2005  Motorola Inc.         Initial Creation
* 20Dec2005  Motorola Inc.         Changed endianess of packet checksum
* 19Dec2005  Motorola Inc.         Handle checksum endianess in macro
* 16Feb2006  Motorola Inc.         Optionally calculate CRC for data 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcTransmitPacket(ipcPortId_t portId, ipcChannelId_t channelId, ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
#ifdef BYPASS_DEVICE
    ipcDevice_t *device;
    int rc = 0;
#else
    ipcDevice_t *device;
    ipcTxChannel_t *channel = NULL;
    UINT16 crcValue = 0;
#endif
/*--------------------------------------- CODE -----------------------------------------*/
#ifdef BYPASS_DEVICE
    device = ipcGetDevice(portId);

    if (device == NULL)
    {
        ipcError(IPCLOG_DEVICE, ("RTDVDataRequest Failed: IPCENODE\n"));
        return IPCENODE;
    }
    rc = device->driver.write(device->driver.handle, 
                (UINT8*)packet, ipcGetPacketStorageSize(packet));
    if (rc >= 0)
    {
        ipcFree(packet);
    }
    return rc;
#else
    device = ipcGetDevice(portId);

    if (device == NULL)
    {
        ipcError(IPCLOG_DEVICE, ("RTDVDataRequest Failed: IPCENODE\n"));
        return IPCENODE;
    }

    if (ipcLockDevice(device) < 0)
    {
        ipcError(IPCLOG_DEVICE, 
                ("ipcTransmitPacket: Failed to lock device\n"));
        return IPCEBUSY;
    }
    
    channel = device->tx.channelPointers[channelId];

    if (channel == NULL)
    {
        ipcUnlockDevice(device);
        ipcError(IPCLOG_DEVICE, ("ipcTransmitPacket Failed: IPCECHAN\n"));
        return IPCECHAN;
    }

    if ((channelId != IPC_CONTROL_CHANNEL_ID) && 
        ((channel->numBytesPending + channel->numBytesToSend) > IPC_MAX_BYTES_PENDING))
    {
        /* We have too much data pending already */
        ipcUnlockDevice(device);
        ipcError(IPCLOG_DEVICE, ("RTDVDataRequest Failed: IPCENOBUFS\n"));
        return IPCENOBUFS;
    }

    /* Set CRC value for entire body */
    if ((ipcGetMsgFlags(packet) & IPC_PACKET_CRC))
    {
        crcValue = ipcGetDataChecksum(packet);
        if (crcValue == 0)
        {
            crcValue = ipcCrc16(0, ipcGetMsgData(packet), ipcGetMsgLength(packet));
        }
    }

    ipcSetDataChecksum(packet, crcValue);
    /* Set CRC value for header */
    crcValue = ipcCrc16(0, ipcGetHeaderStart(packet),
                IPC_HEADER_SIZE - IPC_PACKET_HEADER_CRC_BYTES);

    ipcSetChecksum(packet, crcValue);

    if (ipcGetMsgFlags(packet) & IPC_PACKET_HIGH_PRIORITY)
    {
        ipcListAddTail(&channel->highPriorityPackets, (ipcListLink_t)packet);
    }
    else
    {
        ipcListAddTail(&channel->lowPriorityPackets, (ipcListLink_t)packet);
    }

    channel->numBytesPending += ipcGetPacketTransmitSize(packet);

    /*
    ipcDebugLog(IPCLOG_DEVICE, ("Data Request, Pending = %d:%d, ToSend = %d:%d\n",
        channel->packetsPending.size, channel->numBytesPending,
        channel->packetsToSend.size, channel->numBytesToSend));
    */

    //printPacket("OUT >>>", packet);

    ipcUnlockDevice(device);

    /* Wake up output thread so it can send this packet */
    ipcWakeUpOutputThread(portId);

    return IPC_OK;
#endif
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcOutputChannelData
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function transmits all channel buffers in the current frame.
*   
*   PARAMETER(S):
*       device: pointer to the device structure
*       channelIds: array of channel IDs with data to send in this frame
*                               (terminated by IPC_INVALID_CHANNEL_ID)
*       txChannels: array of TX channel pointers
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Sep2004  Motorola Inc.         Initial Creation
* 25Jan2005  Motorola Inc.         Shared memory debug
* 27Jun2005  Motorola Inc.         Use SSRTChannelRelRequest to close channel 
* 27Mar2007  Motorola Inc.         Jump over loop after channel was removed
* 
*****************************************************************************************/
void ipcOutputChannelData(ipcDevice_t *device, 
                          ipcChannelId_t channelIds[], 
                          ipcTxChannel_t *txChannels[])
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcTxChannel_t *channel;
    UINT32 remainThisBuffer;
    UINT32 totalPacketLength;
    UINT8 *data;
    UINT32 length = 0;
    UINT32 channelIndex;
/*--------------------------------------- CODE -----------------------------------------*/
    for (channelIndex = 0; channelIds[channelIndex] != IPC_INVALID_CHANNEL_ID; channelIndex++)
    {
        /* Get next channel to output data from */
        channel = txChannels[channelIds[channelIndex]];

        /* Output all frame data for this channel */
        while (channel->frameBytesRemaining > 0)
        {

            /* If we are transmitting a new packet, get the next packet and send 
             * the start of packet sync sequence */
            if (channel->currentPacket == 0)
            {
                if (ipcLockDevice(device) < 0)
                {
                    ipcError(IPCLOG_DEVICE,
                            ("ipcOutputChannelData: Failed to lock device\n"));
                    return;
                }
                if (channel->highPriorityPackets.size > 0)
                {
                    channel->currentPacket = (char *)ipcListRemoveHead(&channel->highPriorityPackets);
                }
                else
                {
                    channel->currentPacket = (char *)ipcListRemoveHead(&channel->lowPriorityPackets);
                }
                
                if (channel->currentPacket == NULL)
                {
                    ipcError(IPCLOG_DEVICE, ("Current Packet is NULL! Abort frame.\n"));
                    ipcUnlockDevice(device);
                    return;
                }
                ipcUnlockDevice(device);

                ipcAddToTxBuffer(device, IPC_DEV_ESCAPE_START_PACKET, IPC_DEV_ESCAPE_LENGTH);

                /* Reset index to beginning of new packet */
                channel->txIndex = 0;

                /* NOTE: The escape sequence is not included in the number of frame bytes */
            }

            /* Point at next buffer to send (add index where we left off) */
            data = ipcGetHeaderStart(channel->currentPacket) + channel->txIndex;
            totalPacketLength = ipcGetPacketTransmitSize(channel->currentPacket);
   
            remainThisBuffer = totalPacketLength - channel->txIndex;

            /* Send all remaining bytes in this buffer, or the number of frame bytes
             * this channel has left to send (whichever is less) */
            length = ipcMin(channel->frameBytesRemaining, remainThisBuffer);

            /* Transmit the data with proper escape sequences */
            ipcTransmitWithEscape(device, data, length);

            channel->numBytesToSend -= length;

            channel->txIndex += length;
            channel->frameBytesRemaining -= length;
    
            /* If we have sent the complete IPC packet... */
            if (channel->txIndex >= totalPacketLength)
            {
                /* Free the current packet we just finished sending */
                ipcFree(channel->currentPacket);
                channel->currentPacket = NULL;

                /* If this channel was marked for termination, remove it if there is no more data */
                if (channel->terminate && 
                    (channel->numBytesPending == 0) && 
                    (channel->numBytesToSend == 0)) 
                {
                    //ipcCloseTxChannel(device->portId, channel->id);
                    SSRTChannelRelRequest(channel->id, 
                        channel->finalAddr,
                        channel->sourceService);

		    break;
                }
            }
            
            /*
            ipcDebugLog(IPCLOG_TX_DATA,
                ("Data: Channel %d,  Length %d,  PacketDone %d,  FrameDone %d\n",
                  channel->id, buffer.length, (channel->txIndex >= totalPacketLength),
                  (channel->frameBytesRemaining == 0) ));
            */

        } /* while this channel still has frame bytes remaining */

    } /* for each channel in the frame */
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcAddToTxBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will add data to the transmit buffer for the specifed device.
*   If the buffer is full, it will transmit the buffer first.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       data: pointer to the data buffer to transmit
*       length: length of data buffer
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Mar2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcAddToTxBuffer(ipcDevice_t *device, const UINT8 *data, INT32 length)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
#ifdef IPC_BUFFER_TX
    if (length > IPC_DEV_BUFFER_SIZE)
    {
        /* too long to buffer, send directly */
        ipcFlushTxBuffer(device);
        device->driver.write(device->driver.handle, data, length);
        return;
    }
    
    if ( (device->tx.index + length) > IPC_DEV_BUFFER_SIZE )
    {
        ipcFlushTxBuffer(device);
    }

    if (length > 0)
    {
        memcpy(&device->tx.buffer[device->tx.index], data, length);
        device->tx.index += length;
    }
#else
    device->driver.write(device->driver.handle, data, length);
#endif    
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFlushTxBuffer
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will transmit all the data in the TX Buffer
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09Mar2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcFlushTxBuffer(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
#ifdef IPC_BUFFER_TX
    if (device->tx.index > 0)
    {
        device->driver.write(device->driver.handle, device->tx.buffer, device->tx.index);
        device->tx.index = 0;
    }
#endif
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcTransmitWithEscape
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will transmit data with the proper escape sequences.  Any data
*   byte that matches the escape byte will be followed by a second escape byte.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       data: pointer to the data buffer to transmit
*       length: length of data buffer
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Oct2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcTransmitWithEscape(ipcDevice_t *device, const UINT8 *data, INT32 length)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    INT32 index = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    while (length > 0)
    {
        for (index = 0; (data[index] != IPC_DEV_ESCAPE_BYTE) && index < length; index++)
        {
            /* Loop until we reach the first escape byte */
        }

        /* Transmit all data up to the escape byte (or to the end if no escape byte was found) */
        ipcAddToTxBuffer(device, data, index);
        
        /* Insert an additional escape byte if one was found in the data */
        if ((index < length) && (data[index] == IPC_DEV_ESCAPE_BYTE))
        {
            data++;
            length--;
            ipcAddToTxBuffer(device, IPC_DEV_ESCAPE_DATA, IPC_DEV_ESCAPE_LENGTH);
        }

        /* Continue scanning the data from where we just left off */
        data += index;
        length -= index;
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcStartFrame
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   Calculates the number of frame bytes that each active channel will get in
*   the next frame.  It also creates the frame header that needs to be sent out
*   before any channel data.
*   
*   The allocation of frame bytes only takes into account actual data.  The
*   frame header and escape sequence bytes are not counted.  If the frame rate
*   is set so high that these bytes are a significant portion of the frame, the
*   overhead would be too high, so not counting them acually helps balance this
*   (at the cost of a slightly slower frame rate than what was specified).
*   
*   Frame Header Format:
*   
*   [0]  num channels in this frame
*
*   [1]  first channel id
*   [2]  high byte, length of this channel (specified in [2])
*   [3]  low byte,  length of this channel (specified in [2])
*
*   [4]  next channel id
*   [5]  high byte, length of this channel (specified in [5])
*   [6]  low byte,  length of this channel (specified in [5])
*   ...
*   [ ]  frame header CRC h
*   [ ]  frame header CRC l
*
*
*   Example (2 channels in frame):
*   
*   [0]  numChannels = 2
*  
*   [1]  channel_id (channel 1)
*   [2]  length h   (channel 1)
*   [3]  length l   (channel 1)
*  
*   [4]  channel_id (channel 2)
*   [5]  length h   (channel 2)
*   [6]  length l   (channel 2)
*  
*   [7]  frame header CRC h
*   [8]  frame header CRC l
*
*   [9]  start of data (channel 1)
*   ...
*   [ ]  start of data (channel 2)
*   ...
*   [ ]  start of data (channel 3)
*   ...
*
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: buffer containing the frame header
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 01Sep2004  Motorola Inc.         Changes for acknowledged channels
* 22Oct2004  Motorola Inc.         Updated for quick synchronization
* 27Dec2004  Motorola Inc.         Latch data to be thread safe
* 20Dec2005  Motorola Inc.         Changed endianess of frame checksum
* 19Dec2005  Motorola Inc.         Use macro to change endianess
*
*****************************************************************************************/
void ipcStartFrame(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcTxChannel_t *channel;
    UINT32 length;
    UINT32 frameBytesLeft;
    INT32 numChannels = 0;
    INT32 channelIndex;
    UINT16 frameHeaderCrc;
/*--------------------------------------- CODE -----------------------------------------*/
    frameBytesLeft = device->tx.totalBytesPerFrame;
    device->tx.frameHeaderLength = 0;

    /* Latch all pending packets into the packetsToSend list */
    ipcLatchPacketData(device);

    /* count the number of bytes allocated to each channel */
    for(channel = (ipcTxChannel_t *)device->tx.channelList.head;
        (channel != NULL) && (frameBytesLeft > 0);
        channel = channel->next)
    {
        if (channel->numBytesToSend > 0)
        {
            device->tx.frameChannelIds[numChannels++] = channel->id;

            /* find the length this channel is allowed
             * (no more than the number of frame bytes left) */
            length = ipcMin(channel->numBytesToSend, frameBytesLeft);

            if (channel->bytesPerFrame > 0)
            {
                /* We have not reached the non-dedicated channels yet so limit to the
                 * number of bytes allocated to this channel */
                length = ipcMin(length, channel->bytesPerFrame);

                ipcDebugLog(IPCLOG_DEVICE, ("Dedicated channel %d given %d frame bytes (%d to send)\n",
                    channel->id, length, channel->numBytesToSend));
            }
            else
            {
                /* For non-dedicated channels, limit size to 16 bits (this is guaranteed for
                 * dedicated channels becuase channel->bytesPerFrame is a 16-bit value) */
                length = ipcMin(length, 0xFFFF);
            }

            frameBytesLeft -= length;
            channel->frameBytesRemaining = (UINT16)length;
            
        } /* if this channel has data to send */

    } /* iterate through all channels while there are frame bytes left */

    /* If there are any channels with data to send we must construct a frame header */
    if (numChannels > 0)
    {
        /* terminate channel ID list with IPC_INVALID_CHANNEL_ID */
        device->tx.frameChannelIds[numChannels] = IPC_INVALID_CHANNEL_ID;

        /* go through all the channels with data to send and assign any excess bytes */
        for (channelIndex = 0;
             device->tx.frameChannelIds[channelIndex] != IPC_INVALID_CHANNEL_ID;
             channelIndex++)
        {
            channel = device->tx.channelPointers[device->tx.frameChannelIds[channelIndex]];

            /* if this is a dedicated channel, add any excess frame bytes */
            if (channel->bytesPerFrame > 0)
            {
                /* if there is excess, send more */
                length = channel->frameBytesRemaining + frameBytesLeft;
                length = ipcMin(length, channel->numBytesToSend);
                length = ipcMin(length, 0xFFFF); /* limit to 16 bits */

                /* remove this amount from the excess */
                frameBytesLeft -= (length - channel->frameBytesRemaining);

                channel->frameBytesRemaining = (UINT16)length;

                ipcDebugLog(IPCLOG_DEVICE, ("Dedicated channel %d increased to %d frame bytes (%d to send)\n",
                    channel->id, length, channel->numBytesToSend));
            }

            /* put the channel ID and the number of bytes being sent in this frame into the header */
            ipcSetFrameChannelId(device->tx.frameHeader, channelIndex, channel->id);
            ipcSetFrameChannelLength(device->tx.frameHeader, channelIndex, channel->frameBytesRemaining);
        }

        ipcSetFrameNumChannels(device->tx.frameHeader, channelIndex);
        device->tx.frameHeaderLength = ipcGetFrameHeaderLength(device->tx.frameHeader);

        /* calculate the frame header CRC and store it at the end of the frame header */
        length = device->tx.frameHeaderLength - IPC_FRAME_HEADER_CRC_BYTES;

        /* reverse CRC bytes to write to memory in big endian format */
        frameHeaderCrc = ipcOsalRev16(ipcCrc16(0, device->tx.frameHeader, length));
        ipcOsalWrite16(&device->tx.frameHeader[length], frameHeaderCrc);

        //printTxFrame(device);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcLatchPacketData
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function latches all pending packets by transferring the byte count
*   from numBytesPending to numBytesToSend for every channel.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Dec2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcLatchPacketData(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcTxChannel_t *channel;
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcLockDevice(device) < 0)
    {
        ipcError(IPCLOG_DEVICE,
                ("ipcLatchPacketData: Failed to lock device\n"));
        return;
    }
    for(channel = (ipcTxChannel_t *)device->tx.channelList.head;
        channel != NULL;
        channel = channel->next)
    {
        channel->numBytesToSend += channel->numBytesPending;
        channel->numBytesPending = 0;
    }
    ipcUnlockDevice(device);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWaitForOutput
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function puts the output thread to sleep until there is data to send.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: the return value of ipcOsalSemaWait
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Dec2004  Motorola Inc.         Initial Creation
* 11Aug2005  Motorola Inc.         Check return value of ipcOsalSemaWait 
*
*****************************************************************************************/
int ipcWaitForOutput(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPCE_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Wait until the output semaphore is signaled */
    rc = ipcOsalSemaWait(device->waitForOutputSema, IPC_OSAL_INFINITE);
    
    while (ipcOsalSemaWait(device->waitForOutputSema, 0) == IPCE_OK)
    {
        /* Now loop to reset the output semaphore count to zero in 
         * case it has been signaled multiple times */
    }
    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcWakeUpOutputThread
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function wakes up the output thread when there is data to send.
*   
*   PARAMETER(S):
*       portId: portID of device to wake up output thread for
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Dec2004  Motorola Inc.         Initial Creation
* 17Aug2005  Motorola Inc.         Check return value of ipcGetDevice() 
*
*****************************************************************************************/
void ipcWakeUpOutputThread(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device = ipcGetDevice(portId);
/*--------------------------------------- CODE -----------------------------------------*/
    if(device != NULL)
    {
        ipcOsalSemaSignal(device->waitForOutputSema);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceFreePendingPackets
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function clears all pending packets on this device.  It is called by
*   the router when it detects that the link is down.
*   
*   PARAMETER(S):
*       portId: port ID of device to reset
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 27Dec2004  Motorola Inc.         Initial Creation
* 21Sep2005  Motorola Inc.         Changed to ipcDeviceFreePendingPackets
*
*****************************************************************************************/
void ipcDeviceFreePendingPackets(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device;
    ipcTxChannel_t *channel;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcWarning(IPCLOG_DEVICE, ("ipcDeviceFreePackets on port %d!\n", portId));

    if ((ipcStack_glb.ports[portId].deviceType != IPC_DEVICE_SHARED_MEMORY) &&
        ((device = ipcGetDevice(portId)) != NULL))
    {
        if (ipcLockDevice(device) < 0)
        {
            ipcError(IPCLOG_GENERAL,
                    ("ipcDeviceFreePackets: Failed to lock device\n"));
            return;
        }
    
        /* Free all pending packets */
        for (channel = (ipcTxChannel_t *)device->tx.channelList.head;
             channel != NULL;
             channel = channel->next)
        {
            ipcFreeTxChannelPackets(channel);
        }

        /* Set flag to free current packet in output thread */     
        device->freePackets = TRUE;
        ipcWakeUpOutputThread(portId);
            
        ipcUnlockDevice(device);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceFreeCurrentPackets
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function frees the current packet being transmitted on all channels
*   if there is one.  It must be called from the device output thread.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 21Sep2005  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void ipcDeviceFreeCurrentPackets(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcTxChannel_t *channel;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Free all pending packets */
    for (channel = (ipcTxChannel_t *)device->tx.channelList.head;
         channel != NULL;
         channel = channel->next)
    {
        if (channel->currentPacket != NULL)
        {
            ipcFree(channel->currentPacket);
            channel->currentPacket = NULL;
        }

        /* If this channel was marked for termination, try to remove it again */
        if (channel->terminate) 
        {
            ipcCloseTxChannel(device->portId, channel->id);
        }
    }
}

#endif /* IPC_SIMPLE_DEVICE */
