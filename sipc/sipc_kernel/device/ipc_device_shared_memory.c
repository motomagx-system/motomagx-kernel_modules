/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2005 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_shared_memory.c
*   FUNCTION NAME(S): ipcCloseShmDevice
*                     ipcCreateShmDevice
*                     ipcShmTransmitPacket
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains device layer functions used for shared memory
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 07Apr2005  Motorola Inc.         Initial Creation 
*****************************************************************************************/

#ifdef HAVE_PHYSICAL_SHM

/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_device_interface.h"
#include "ipc_router_interface.h"
#include "ipc_common.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/

ipcPortId_t ipcShmPortId;
ipcShmWriteFunction_t ipcDeviceShmWrite;

/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateShmDevice
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function allocates memory for an ipcShmDevice_t structrue and initializes
*   all of its fields.  The device structure represents a physical shared memory
*   port used by the IPC device layer
*   
*   PARAMETER(S):
*       portId: the port ID assigned to this device
*       bandwidth: the total bandwidth of this device (for shared memory
*                              devices, this parameter is ignored)
*       framesPerSecond: the frame rate for this device
*       driver: pointer to the device driver data
*       
*   RETURN: TRUE if successful
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcCreateShmDevice(ipcPortId_t portId, ipcShmWriteFunction_t write)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (portId >= IPC_MAX_PORTS)
    {
        return FALSE;
    }
    
    ipcShmPortId = portId;
    ipcDeviceShmWrite = write;

    ipcRouterOpenConnection(portId, NULL, IPC_DEVICE_SHARED_MEMORY);

    return TRUE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCloseShmDevice
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function frees all memory used by the specified device.
*   
*   PARAMETER(S):
*       portId: port ID of device to close
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 08Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcCloseShmDevice(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcRouterCloseConnection(portId);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcShmTransmitPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called by RTDVDataRequest to transmit an IPC packet for
*   shared memory devices.  It will send the pointer immediately (data is not
*   multiplexed for shared memory devices).
*   
*   PARAMETER(S):
*       portId: identifies hardware port
*       channelId: the IPC channel to transmit on (ignored)
*       packet: pointer to IPC packet to send 
*                  
*   RETURN: success or error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 11Apr2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int ipcShmTransmitPacket(ipcPortId_t portId, ipcChannelId_t channelId, ipcPacket_t *packet)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsPhysicalSharedMemory(packet))
    {
        return IPCEFAULT;
    }

    if (ipcDeviceShmWrite == NULL)
    {
        ipcError(IPCLOG_DEVICE, ("ipcDeviceShmWrite is NULL\n"));
        return IPCENETUNREACH;
    }

    if (ipcDeviceShmWrite((void *)packet) < 0)
    {
        return IPCENETUNREACH;
    }

    return IPC_OK;
}


#endif /* HAVE_PHYSICAL_SHM */
