/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_debug.c
*   FUNCTION NAME(S): createDataPacket
*                     printDataImp
*                     printPacketImp
*                     printPacketListImp
*                     printRxFrameImp
*                     printTxChannelsImp
*                     printTxFrameImp
*                     processTestPacket
*                     sprintDeviceStructSizes
*                     sprintFrame
*                     verifyDataPacket
*
*--------------------------------------- PURPOSE -----------------------------------------

*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*****************************************************************************************/


#ifdef IPC_DEBUG

#include <stdarg.h>
#include "ipc_device_debug.h"
#include "ipc_device_layer.h"
#include "ipc_router_interface.h"
#include "ipclog.h"


ipcDeviceDebug_t ipcDeviceDebug_glb = {0, 0, 0, 0, "ipcdev.log"};



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sprintDeviceStructSizes
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void sprintDeviceStructSizes(char *buffer)
{
    sprintf(buffer, "device %d,  rxchan %d,  txchan %d,  maxframe %d",
        sizeof(ipcDevice_t),
        sizeof(ipcRxChannel_t),
        sizeof(ipcTxChannel_t),
        IPC_MAX_FRAME_HEADER_SIZE );
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: sprintFrame
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void sprintFrame(char *buffer, UINT8 *frame)
{
    int offset = 0;
    UINT8 i, numChannels;

    numChannels = ipcGetFrameNumChannels(frame);

    offset += sprintf(buffer + offset, "  %d Chan: ", numChannels);

    for (i = 0; i < numChannels; i++)
    {
        offset += sprintf(buffer + offset, "(%d - %d) ",
            ipcGetFrameChannelId(frame, i),
            ipcGetFrameChannelLength(frame, i));  
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: printDataImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void printDataImp(const char *description, const UINT8 *data, INT32 length)
{
    int i;
    int offset = 0;
    char *buffer;
    
    buffer = (char *)ipcAlloc(50 + (length * 4), 0);
    
    if (buffer != NULL)
    {   
        if (description != NULL)
        {
            offset += sprintf(buffer + offset, "%s (%d): ", description, length);
        }

        for (i = 0; i < length; i++)
        {
            offset += sprintf(buffer + offset, "%d ", data[i]);
        }        

        sprintf(buffer + offset, "<END>");
        ipcDebugLog(IPCLOG_DEVICE, ("%s\n", buffer));
        
        ipcFree(buffer);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: printPacketListImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void printPacketListImp(ipcList_t *list)
{
    ipcPacket_t *packet;

    for (packet = (ipcPacket_t *)list->head;
         packet != NULL;
         packet = packet->next)
    {
        printData("PACKET", ipcGetMsgData(packet), ipcGetMsgLength(packet));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: printTxChannelsImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void printTxChannelsImp(void *devicePtr)
{
    ipcDevice_t *device = (ipcDevice_t *)devicePtr;
    ipcTxChannel_t *channel;

    ipcLockDevice(device);
    for (channel = (ipcTxChannel_t *)device->tx.channelList.head;
         channel != NULL;
         channel = channel->next)
    {
        ipcDebugLog(IPCLOG_DEVICE, ("Channel id: %d, highPriPack = %d, lowPriPack = %d\n", 
                  channel->id,
                  channel->highPriorityPackets.size,
                  channel->lowPriorityPackets.size)); 

        printPacketList(&channel->highPriorityPackets);
        printPacketList(&channel->lowPriorityPackets);
    }
    ipcUnlockDevice(device);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: printRxFrameImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void printRxFrameImp(void *devicePtr)
{
    char buffer[500];
    int offset = 0;
    ipcDevice_t *device = (ipcDevice_t *)devicePtr;

    offset += sprintf(buffer + offset, "RX Frame %d", device->portId);
    sprintFrame(buffer + offset, device->rx.frameHeader);
    ipcDebugLog(IPCLOG_DEVICE, ("%s\n", buffer));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: printTxFrameImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void printTxFrameImp(void *devicePtr)
{
    char buffer[500];
    int offset = 0;
    ipcDevice_t *device = (ipcDevice_t *)devicePtr;

    offset += sprintf(buffer + offset, "TX Frame %d", device->portId);
    sprintFrame(buffer + offset, device->tx.frameHeader);
    ipcDebugLog(IPCLOG_DEVICE, ("%s\n", buffer));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: printPacketImp
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
void printPacketImp(const char *description, ipcPacket_t *packet)
{
    int i;
    int length = ipcGetMsgLength(packet);
    int offset = 0;
    char *buffer;
    
    buffer = (char *)ipcAlloc(100 + (length * 4), 0);
    
    if (buffer != NULL)
    {   
        offset += sprintf(buffer + offset, "%s src=(%d,%d) dest=(%d,%d) seqNum=%d len=%d data=",
            description,
            ipcGetSrcAddr(packet), ipcGetSrcServiceId(packet),
            ipcGetDestAddr(packet), ipcGetDestServiceId(packet),
            ipcGetSeqNum(packet), length);

        for (i = 0; i < length; i++)
        {
            offset += sprintf(buffer + offset, "%d ", ipcGetMsgData(packet)[i]);
        }        

        ipcDebugLog(IPCLOG_DEVICE, ("%s\n", buffer));
        
        ipcFree(buffer);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: createDataPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
ipcPacket_t * createDataPacket(UINT8 id, UINT16 length)
{
    int i;
    ipcPacket_t *packet;

    packet = (ipcPacket_t *)ipcAlloc(IPC_PACKET_DATA_OFFSET + length,
                                     IPC_PROCSHM); 
                                     
    if (packet == NULL) return(NULL);

    memset(packet, 0, IPC_PACKET_DATA_OFFSET);
    ipcSetMsgLength(packet, length);

    id <<= 4;

    for (i = 0; i < length; i++)
    {
        ipcGetMsgData(packet)[i] = id | (i & 0x0F);
    }

    return(packet);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: verifyDataPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
BOOL verifyDataPacket(ipcPacket_t *packet)
{
    int i;
    int length = ipcGetMsgLength(packet);
    UINT8 currentData;
    UINT8 previousData;

    /* Ignore first data byte (can be used as opcode for test packets) */
    for (i = 2; i < length; i++)
    {
        currentData = ipcGetMsgData(packet)[i];
        previousData = ipcGetMsgData(packet)[i-1];

        if ( (currentData & 0xF0) != (previousData & 0xF0) )
        {
            return FALSE;
        }

        if ( (currentData & 0x0F) != ((previousData + 1) & 0x0F) )
        {
            return FALSE;
        }
    }

    return TRUE;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: processTestPacket
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   PARAMETER(S):
*
*   RETURN: 
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Jan2007  Motorola Inc.         Initial Creation
*
*****************************************************************************************/
BOOL processTestPacket(ipcPacket_t *packet)
{
    //static UINT32 startTime;
    //static UINT32 elapsedTime;
    static int nPacketNum;
    static long lTotalBytes;
    ipcPacket_t* pPack;
    int rc;

    switch(ipcGetMsgOpcode(packet))
    {
    case BEGIN:
        //startTime = ipcOsalGetTickCount();
        nPacketNum = 0;
        lTotalBytes = 0;
        break;

    case END:
        //dwElapsedTime = ipcOsalGetTickCount() - startTime;
        //ipcDebugLog(IPCLOG_DEVICE, ("Total time: %ld packet num: %ld total bytes: %ld Baud Rate:%ld (KBytes)\n", dwElapsedTime, nPacketNum, lTotalBytes, lTotalBytes/dwElapsedTime));
        break;

    case DATA:
        ipcDebugLog(IPCLOG_DEVICE, (">>> Send Ack: %d\n", ACK));
        nPacketNum++;
        lTotalBytes += ipcGetMsgLength(packet);
        pPack = createDataPacket(0, 1);
        ipcGetMsgData(pPack)[0] = ACK;

        rc = RTDVDataRequest(IPC_NONDEDICATED_CHANNEL_ID, pPack, (ipcPortId_t)0);
        if (IPCE_OK != rc)
        {
            ipcFree(pPack);
        }
        break;

    case ACK:
        ipcOsalSemaSignal(ipcDeviceDebug_glb.deviceAckSema);
        timeBenchStop(pTimeBench4);
        break;

    default:
        return verifyDataPacket(packet);
    }
    return TRUE;
}

#endif /* IPC_DEBUG */
