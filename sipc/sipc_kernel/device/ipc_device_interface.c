/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2004 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_interface.c
*   FUNCTION NAME(S): RTDVChannelRelRequest
*                     RTDVChannelRequest
*                     RTDVDataRequest
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file implements the device layer API for the session, router, and
*   hardware abstraction layers
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 02Jun2004  Motorola Inc.         Initial Creation
* 01Sep2004  Motorola Inc.         Updated for acknowledged channels
* 22Sep2004  Motorola Inc.         Changes to merge device and router threads
* 22Mar2006  Motorola Inc.         Bug fix for PHY SHM packet retransmission
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 13Jul2007  Motorola Inc.         Removed support for BYPASS_DEVICE
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_common.h"
#include "ipc_device_interface.h"
#include "ipc_osal.h"
#include "ipc_session.h"
#include "ipc_shared_memory.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/



/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTDVChannelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the router to request a channel from the
*   device layer.
*   
*   PARAMETER(S):
*       portId: identifies hardware port
*       QoS: requested quality of service (bandwidth) in bytes per second
*                  
*   RETURN: the channel ID if successful
*           IPCENODE - invalid portID
*           IPCENOBUFS - out of memory
*           IPCEQOS - not enough bandwidth left
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 22Aug2005  Motorola Inc.         Added a parameter for shm channels 
*
*****************************************************************************************/
int RTDVChannelRequest(ipcPortId_t portId, ipcQoS_t QoS)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK; /* NEED TO CHANGE THIS! */
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSharedMemoryDevice(portId))
    {
        rc = ipcCreateTxChannel(portId, QoS, IPC_INVALID_CHANNEL_ID);
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTDVChannelRelRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the router to request a channel be removed by
*   the device layer. 
*   
*   PARAMETER(S):
*       channelId: the IPC channel to close
*       portId: identifies hardware port
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
int RTDVChannelRelRequest(ipcChannelId_t channelId, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    if (!ipcIsSharedMemoryDevice(portId))
    {
        rc = ipcCloseTxChannel(portId, channelId);
    }

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: RTDVDataRequest
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used by the IPC router to send packet data on a
*   pre-assigned IPC channel.
*   
*   PARAMETER(S):
*       channelId: the IPC channel to send the packet on
*       packet: pointer to IPC packet to send
*       portId: identifies hardware port
*                  
*   RETURN: IPC_OK - success
*           IPCENODE - invalid port ID
*           IPCENETUNREACH - link down
*           IPCECHAN - invalid channel
*           IPCENOBUFS - out of memory
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 07Apr2005  Motorola Inc.         Separated into two functions for shared memory and multiplexed devices
* 25Nov2005  Motorola Inc.         Automatic convert between memtypes
* 22Mar2006  Motorola Inc.         Bug fix for PHY SHM packet retransmission 
* 13Jul2007  Motorola Inc.         Removed support for BYPASS_DEVICE
*
*****************************************************************************************/
int RTDVDataRequest(ipcChannelId_t channelId, ipcPacket_t *packet, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    if ( packet == NULL ){
        return IPCEFAULT;
    }

    if (ipcGetSrcAddr(packet) == ipcStack_glb.ipcAddr)
    {
        ipcSetDataChecksum(packet, 0);
    }
    if (ipcIsSharedMemoryDevice(portId))
    {
        if (
            /* transmit a virtual shared memory packet */
            (!ipcIsPhysicalSharedMemory(packet))
            ||
            /* retransmit a physical shared memory packet*/
            (ipcSMGetMulticore((ipcSMDataToHeader(packet))) && (ipcIncRefCount((void *)packet, 0) > 2))
           )
        {
            ipcPacket_t* newPacket = ipcCopyPacket(packet, IPC_PHYSSHM);

            if (newPacket == NULL)
            {
                return IPCENOBUFS;
            }
            else
            {
                rc = ipcShmTransmitPacket(portId, channelId, newPacket);
            }
            if (rc < 0)
            {
                ipcFree(newPacket);
            }
            else
            {
                ipcFree(packet);
            }
        }
        else
        {
            rc = ipcShmTransmitPacket(portId, channelId, packet);
        }
    }
    else
    {
        rc = ipcTransmitPacket(portId, channelId, packet);
    }
    return rc;
}
