/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2003 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_general.c
*   FUNCTION NAME(S): ipcCalculateCrc
*                     ipcCloseDevice
*                     ipcCreateDevice
*                     ipcDeviceDestroy
*                     ipcDeviceDisable
*                     ipcDeviceEnable
*                     ipcDeviceInit
*                     ipcResetFrameTime
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains general device layer functions used internally
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 02Jun2004  Motorola Inc.         Converted from C++ to C
* 22Dec2004  Motorola Inc.         Updated for multiple threads 
*****************************************************************************************/


/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_device_layer.h"
#include "ipc_device_interface.h"
#include "ipc_osal.h"
#include "ipc_list.h"
#include "ipclog.h"
#include "ipc_router_interface.h"
#include "ipc_router_internal.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/
/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceInit
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function initializes the device layer
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 14Jan2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
BOOL ipcDeviceInit()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    ipcStack_glb.numOpenPorts = 0;
    memset(ipcStack_glb.ports, 0, sizeof(ipcStack_glb.ports));

    return ipcPlatformDeviceInit();
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceDestroy
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function frees all device layer resources
*   
*   PARAMETER(S):
*       void
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 09May2005  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcDeviceDestroy()
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
/*--------------------------------------- CODE -----------------------------------------*/
    /* Close all the devices */
    for (i = ipcStack_glb.numOpenPorts - 1; i >= 0; i--)
    {
        if (ipcIsSharedMemoryDevice(i))
        {
            ipcCloseShmDevice((ipcPortId_t)i);
        }
        else
        {
            ipcCloseDevice((ipcPortId_t)i);
        }
    }

    ipcStack_glb.numOpenPorts = 0;
    memset(ipcStack_glb.ports, 0, sizeof(ipcStack_glb.ports));
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCalculateCrc
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*
*   Computes a CRC for the various channels
*
*   This implementation uses the syndrome check form rather than the
*   polynomial division form of the CRC algorithm.  This means that
*   the data bits don't need to be appended with zeros (or the chosen
*   extention) for the polynomial divide.  However, the shift register
*   (i.e. crc_value) needs to be pre-initialized to that extention value,
*   which is normally zero.  Even though the algorithm performs the shift
*   prior to the polynomial subtraction, it is still recommended that the
*   FULL polynomial to be defined; including the high end bit.  This should
*   help in a better understanding of the algorithm.  The crc computation
*   performed here is a left-shift computation, which therefore uses a
*   polynomial word with the high bit in the MSB. For example, for a 7 bit 
*   CRC with the polynomial 1+x+x^2+x^7, the Polynomial variable should be 
*   0x87.  For reference purposes, this is the mirror image of the algorithm
*   presented in the Sklar book which uses a right-shift version.
*
*   PARAMETER(S):
*       inputBufferPtr  input data buffer
*       bufferSize: number of bytes to compute CRC over
*       crcSize: number of bits in the CRC register
*       polynomial: polynomial to compute CRC with
*       initialValue: initialization value for the CRC shift register
*
*   RETURN: remainder
*
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------

*
*****************************************************************************************/
UINT32 ipcCalculateCrc(const UINT8 *inputBufferPtr, INT32 bufferSize, INT16 crcSize,
                       UINT32 polynomial, UINT32 initialValue)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    UINT32 remainder, order;
    INT32 i, j;
    INT16 crcSize_1 = crcSize - 1;
/*--------------------------------------- CODE -----------------------------------------*/
    remainder = initialValue;
    order = 1 << crcSize_1;
    
    for (i=0; i < bufferSize; i++)
    {
        for (j=7; j >= 0; j--) /* For each bit in this byte */
        {
            /* get data bit */
            remainder ^= (((inputBufferPtr[i] >> j) & 0x01) << crcSize_1);
           
            /* shift register.
               if the the last crc bit equals 1 XOR the polynomial.  */
            if (remainder & order)
                remainder = (remainder << 1) ^ polynomial;
        
            else
                remainder <<= 1;
        }
    }
    
    return(remainder);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceEnable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function enables an ipc device.
*   
*   PARAMETER(S):
*       portId: port which will be enabled
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 31Jan2007  Motorola Inc.         Checked stack state 
*
*****************************************************************************************/
void ipcDeviceEnable(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if (portId < ipcStack_glb.numOpenPorts)
    {
        port = &ipcStack_glb.ports[portId];
        port->enabled = TRUE;
        if (ipcStack_glb.state == IPC_STACK_STATE_RUNNING)
        {
            ipcRouterForceLinkDown(portId);
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceDisable
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function disables an ipc device.
*   
*   PARAMETER(S):
*       portId: port which will be disabled
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Nov2006  Motorola Inc.         Optimize SIPC timer
* 31Jan2007  Motorola Inc.         Checked stack state 
*
*****************************************************************************************/
void ipcDeviceDisable(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPort_t* port = NULL;
/*--------------------------------------- CODE -----------------------------------------*/
    if (portId < ipcStack_glb.numOpenPorts)
    {
        port = &ipcStack_glb.ports[portId];
        port->enabled = FALSE;
        if (ipcStack_glb.state == IPC_STACK_STATE_RUNNING)
        {
            ipcRouterForceLinkDown(portId);
        }
    }
}

#ifndef IPC_SIMPLE_DEVICE

/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateDevice
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function allocates memory for an ipcDevice_t structrue and initializes
*   all of its fields.  The device structure represents a physical hardware port
*   used by the IPC device layer (USB, UART, shared memory, etc.)
*   
*   PARAMETER(S):
*       portId: the port ID assigned to this device
*       bandwidth: the total bandwidth of this device (in bits per second)
*       framesPerSecond: the frame rate for this device
*       driver: pointer to the device driver data
*       
*   RETURN: TRUE if successful
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 12Jun2004  Motorola Inc.         Converted from C++ to C 
*
*****************************************************************************************/
BOOL ipcCreateDevice(ipcPortId_t portId, ipcDeviceType_t deviceType, INT32 bandwidth,
                     INT32 framesPerSecond, void *driver)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device = NULL;
    int rc;
/*--------------------------------------- CODE -----------------------------------------*/
    if (portId >= IPC_MAX_PORTS)
    {
        return FALSE;
    }
    
    device = (ipcDevice_t *)ipcAlloc(sizeof(ipcDevice_t), 0);

    if (device == NULL)
    {
        return FALSE;
    }

    memset(device, 0, sizeof(ipcDevice_t));
    memcpy(&device->driver, driver, sizeof(ipcDeviceDriver_t));

    bandwidth >>= 3; /* Convert bits/sec to bytes/sec */
    device->portId = portId;
    device->totalBandwidth = bandwidth;
    device->framesPerSecond = framesPerSecond;
    device->tx.totalBytesPerFrame = bandwidth / framesPerSecond;
    device->tx.availableBytesPerFrame = device->tx.totalBytesPerFrame;
    device->tx.nextChannelId = IPC_FIRST_CHANNEL_ID;
    device->rx.state = IPC_RX_FRAME_START;

    ipcDebugLog(IPCLOG_DEVICE,
        ("Creating device with ID %d, of type: %d\n", device->portId, deviceType));

    rc = ipcOsalSemaOpen(&device->waitForOutputSema, 0);
    if (rc != IPCE_OK)
    {
        ipcError(IPCLOG_DEVICE, ("Error creating device sema\n"));
    }

    rc = ipcOsalMutexOpen(&device->mutex);
    if (rc != IPCE_OK)
    {
        ipcError(IPCLOG_DEVICE, ("Error creating device mutex\n"));
    }

    rc = ipcRouterOpenConnection(portId, device, deviceType);

    ipcCreateTxChannel(portId, IPC_CONTROL_CHANNEL_BANDWIDTH, IPC_CONTROL_CHANNEL_ID);
    ipcCreateTxChannel(portId, 0, IPC_NONDEDICATED_CHANNEL_ID);

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCloseDevice
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function frees all memory used by the specified device and closes all
*   associated transmit channels.  Any packets waiting to be sent on this device
*   will also be freed immediately without being sent. 
*   
*   PARAMETER(S):
*       portId: port ID of device to close
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 04Oct2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcCloseDevice(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device;
/*--------------------------------------- CODE -----------------------------------------*/
    ipcRouterCloseConnection(portId);
    
    device = ipcGetDevice(portId);
    if (device == NULL)
    {
        ipcError(IPCLOG_DEVICE, ("Closing device with ID %d, NULL Pointer!\n", portId));
    }
    else
    {
        ipcDebugLog(IPCLOG_DEVICE, ("Closing device with ID %d\n", portId));

        device->terminate = TRUE;
        ipcWakeUpOutputThread(portId); /* Wake up output thread to terminate it */

        if (device->driver.stop != NULL)
        {
            device->driver.stop(device->driver.handle); /* Wake up input thread to terminate it */
        }

        /* sleep until the input and output threads have terminated */
        while ((device->driver.read != NULL) || (device->driver.write != NULL))
        {
            if (ipcOsalSleep(10) < 0)
            {
                ipcError(IPCLOG_DEVICE, ("ipcCloseDevice: killed, return abnormally\n"));
                return;
            }
        }

        if (device->driver.close != NULL)
        {
            device->driver.close(device->driver.handle); /* Close the physical device */
        }

        /* close all transmit channels for this device */
        while (device->tx.channelList.head != NULL)
        {
            ipcCloseTxChannelImmediately(device, (ipcTxChannel_t *)device->tx.channelList.head);
        }

        ipcOsalMutexClose(device->mutex);
        ipcOsalSemaClose(device->waitForOutputSema);
 
        ipcFree(device);

        ipcDebugLog(IPCLOG_DEVICE, ("Done Closing device\n"));
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcResetFrameTime
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is used to change the frame time for an existing device.
*   It will close the device and open it again with the new frame time parameter.
*   All existing channels and pending data will be removed.
*   
*   PARAMETER(S):
*       portId: ID of device to reset frameTime for
*       frameTime: the new frame time in milliseconds
*                  
*   RETURN: success or error code
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcResetFrameTime(ipcPortId_t portId, UINT8 frameTime)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device;
    INT32 bandwidth;
    INT32 framesPerSecond = 1000 / frameTime;
    ipcDeviceDriver_t driver;
/*--------------------------------------- CODE -----------------------------------------*/
    device = ipcGetDevice(portId);

    if (device != NULL)
    {
        bandwidth = device->totalBandwidth;
        driver = device->driver;

        ipcCloseDevice(portId);

        ipcCreateDevice(portId,
                        ipcStack_glb.ports[portId].deviceType,
                        bandwidth,
                        framesPerSecond,
                        (void *)&driver);
    }
}

#endif /* IPC_SIMPLE_DEVICE */
