/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2003 - 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_decode.c
*   FUNCTION NAME(S): ipcDecodeEscapeSequence
*                     ipcDecodeFrameHeader
*                     ipcDeviceInputThread
*                     ipcFrameHeaderDone
*                     ipcPacketDone
*                     ipcResetChannelDecoder
*                     ipcResetFrameDecoder
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains internal device layer functions used to demultiplex the
*   stream of frames into channel data, and build up complete IPC packets
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 02Jun2004  Motorola Inc.         Updated to match IPC spec
* 01Sep2004  Motorola Inc.         Changes for acknowledged channels
* 22Oct2004  Motorola Inc.         Updated for quick synchronization
* 18Sep2005  Motorola Inc.         Removed acknowledged frames
* 23Feb2007  Motorola Inc.         Updated memory type defs
* 13Jul2007  Motorola Inc.         Removed inclusion of ipc_device_decode.c
*
*****************************************************************************************/

#ifndef IPC_SIMPLE_DEVICE
/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_device_layer.h"
#include "ipc_device_interface.h"
#include "ipc_router_interface.h"
#include "ipc_osal.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/
/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

void ipcFrameHeaderDone(ipcDevice_t *device);
void ipcPacketDone(ipcRxChannel_t *channel, ipcPortId_t portId);
BOOL ipcDecodeEscapeSequence(ipcDevice_t *device, UINT8 escapeByte);
void ipcDecodeFrameHeader(ipcDevice_t *device);
void ipcResetFrameDecoder(ipcDevice_t *device);
void ipcResetChannelDecoder(ipcRxChannel_t *channel);

/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDeviceInputThread
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This thread continually tries to read data and decodes incoming frames by
*   demultiplexing the channel data to rebuild the original IPC packets.
*   
*   PARAMETER(S):
*       portId: port ID for device we are operating on
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 16Jun2004  Motorola Inc.         Updated to process full buffer each time
* 01Sep2004  Motorola Inc.         Changes for acknowledged channels                  
* 22Oct2004  Motorola Inc.         Updated for quick synchronization
* 27Dec2004  Motorola Inc.         Modified to work on one byte at a time
* 25Jan2005  Motorola Inc.         Shared memory debug
* 27Jan2005  Motorola Inc.         Added frame start sequence before acked frames
* 15Apr2005  Motorola Inc.         Check return value of ipcAlloc()
* 17Aug2005  Motorola Inc.         Check return value of ipcGetDevice()
* 20Dec2005  Motorola Inc.         Changed endianess of packet len
*
*****************************************************************************************/
void ipcDeviceInputThread(ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device;
    UINT8 *data;
    INT32 length;
#ifndef BYPASS_DEVICE
    ipcRxChannel_t *channel;
    UINT32 i;
    UINT32 startIndex;
    UINT32 channelIndex;
    UINT32 limit;
#endif
/*--------------------------------------- CODE -----------------------------------------*/
    /* Wait until the IPC Stack has completed initialization */
    while (ipcStack_glb.state != IPC_STACK_STATE_RUNNING)
    {
        if (ipcOsalSleep(50) < 0)
        {
            ipcError(IPCLOG_DEVICE, ("ipcDeviceInputThread: interrupted!\n"));
            return;
        }
    }
    
    data = (UINT8 *)ipcAlloc(IPC_DEV_BUFFER_SIZE, 0);
    if (NULL == data)
    {
        return;
    }
    
    device = ipcGetDevice(portId);
    if(NULL == device)
    {
        ipcFree(data);
        return;
    }

    while (!device->terminate)
    {
        length = device->driver.read(device->driver.handle, data, IPC_DEV_BUFFER_SIZE);
#ifdef BYPASS_DEVICE
        if (length > 0)
        {
            ipcPacket_t* packet = ipcAlloc(length, IPC_PHYSSHM);

            if (packet != NULL)
            {
                memcpy(packet, data, length);
                DVRTDataIndication(packet, portId);
            }
        }
#else
        if (length < 0)
        {
            /* The device has some errors! break the thread */
            ipcError(IPCLOG_DEVICE, ("Failed to read data from port %d\n", portId));
            break;
        }
        for (i = 0; (INT32)i < length; i++)
        {
            /* Start be processing all escape sequences */
            if (device->rx.isEscapeSequence)
            {
                /* We previously received an escape byte so this next byte will complete
                 * the escape sequence */
                device->rx.isEscapeSequence = FALSE;

                if (ipcDecodeEscapeSequence(device, data[i]))
                {
                    /* The escape byte does not require further processing so continue
                     * to the next byte in the stream */
                    continue;
                }
            }
            else if (data[i] == IPC_DEV_ESCAPE_BYTE)
            {
                ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX Escape Char\n"));
                /* This is the beginning of an escape sequence */
                device->rx.isEscapeSequence = TRUE;

                /* We will not process this byte below so continue to the next one */
                continue;
            }


            switch (device->rx.state)
            {
            case IPC_RX_FRAME_START:
                /* We are waiting for the frame start sequence to be received.
                 * We will ignore all other data until this happens. */
                break;

            case IPC_RX_NUM_CHANNELS:
                ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX %d Num Channels: %d\n", i, data[i]));

                device->rx.frameHeader[0] = data[i];
                device->rx.frameIndex = 1;
                device->rx.frameLength = ipcGetFrameHeaderLength(device->rx.frameHeader);
                
                if (device->rx.frameLength > IPC_MAX_FRAME_HEADER_SIZE)
                {
                    /* We have received a corrupted frame header so ignore this frame
                     * and wait for the next frame start sequence */
                    device->rx.state = IPC_RX_FRAME_START;
                }
                else
                {
                    /* Continue receiving the frame header */
                    device->rx.state = IPC_RX_FRAME_HEADER;
                }
                break;

            case IPC_RX_FRAME_HEADER:
                ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX Frame Header: %d\n", data[i]));

                device->rx.frameHeader[device->rx.frameIndex++] = data[i];

                if (device->rx.frameIndex == device->rx.frameLength)
                {
                    //printBuffer(device->rxFrame);

                    /* We have received the complete frame header so process it */
                    ipcFrameHeaderDone(device);
                }
                break;

            case IPC_RX_DATA:
                channel = &device->rx.channels[device->rx.currentChannelId];

                //ipcDebugLog(IPCLOG_DEVICE_DECODE,
                //    ("<<< RX %d Data: %d,  State = %d\n", i, data[i], channel->state));

                switch(channel->state)
                {
                case IPC_RX_PACKET_START:
                    /* Until we receive the packet start escape sequence we must ignore all data */
                    ipcDebugLog(IPCLOG_DEVICE_DECODE, ("Ignoring Channel Data until packet start\n"));
                    break;

                case IPC_RX_PACKET_LENGTH_BYTE0:
                    channel->packetLength = data[i];
                    channel->state = IPC_RX_PACKET_LENGTH_BYTE1;
                    break;

                case IPC_RX_PACKET_LENGTH_BYTE1:
                    channel->packetLength |= data[i] << 8;
                    channel->state = IPC_RX_PACKET_LENGTH_BYTE2;
                    break;
                
                case IPC_RX_PACKET_LENGTH_BYTE2:
                    channel->packetLength |= data[i] << 16;
                    channel->state = IPC_RX_PACKET_LENGTH_BYTE3;
                    break;

                case IPC_RX_PACKET_LENGTH_BYTE3:
                    channel->packetLength |= data[i] << 24;
                    channel->packet = (UINT8 *)ipcAlloc(channel->packetLength +
                        IPC_PACKET_DATA_OFFSET, IPC_PROCSHM);

                    if (channel->packet == NULL)
                    {
                        /* If we ran out of memory, we will reset the channel decoder to wait for
                         * the next packet start byte */
                        ipcResetChannelDecoder(channel);
                        ipcError(IPCLOG_DEVICE,
                            ("Out of memory allocating packet of length %d\n", channel->packetLength));
                    }
                    else
                    {
                        ipcDebugLog(IPCLOG_DEVICE_DECODE,
                            ("<<< RX %d Packet Length %d\n", i, channel->packetLength));
                        /* Otherwise, copy the length into the newly allocated buffer */
                        ipcSetMsgLength(channel->packet, channel->packetLength);

                        /* Add 4 for length field which we already received */
                        channel->rxIndex = IPC_PACKET_HEADER_OFFSET + 4;
                        channel->packetLength += IPC_PACKET_DATA_OFFSET;
                        channel->state = IPC_RX_PACKET_HEADER;
                    }
                    break;

                case IPC_RX_PACKET_HEADER:
                    channel->packet[channel->rxIndex++] = data[i];
                    if (channel->rxIndex == IPC_PACKET_DATA_OFFSET)
                    {
                        /* We have finished reading the header so verify the CRC */
                        if (ipcCrc16(0, ipcGetHeaderStart(channel->packet), IPC_HEADER_SIZE) != 0)
                        {
                            /* The header CRC failed so we must reset the channel decoder */
                            ipcWarning(IPCLOG_DEVICE, ("Packet Header CRC failed\n"));
                            ipcResetChannelDecoder(channel);
                        }
                        else
                        {
                            ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX %d Complete Packet Header\n", i));
                            /* The header CRC was okay so check for zero-length packet */
                            if (channel->packetLength == IPC_PACKET_DATA_OFFSET)
                            {
                                ipcDebugLog(IPCLOG_DEVICE_DECODE, ("   <<< RX %d Zero-Length Packet\n", i));
                                /* It was a zero-length packet so we are already done with it */
                                ipcPacketDone(channel, device->portId);
                            }
                            else
                            {
                                /* Not zero-length so continue receiving the rest of the packet */
                                channel->state = IPC_RX_PACKET_DATA;
                            }
                        }
                    }
                    break;

                case IPC_RX_PACKET_DATA:
                    /* Most bytes we are processing will be packet data bytes so we will use 
                     * a tighter loop here for efficiency.  We can keep going until we hit an
                     * escape byte, completely receive a packet, receive all frame bytes for 
                     * this channel, or reach the last byte in the buffer we received. */
                    
                    startIndex = i; /* keep track of where we started */
                    channelIndex = channel->rxIndex;

                    /* Stop at last byte in received buffer, or when we receive all frame
                     * bytes for this channel */
                    limit = ipcMin((UINT32)length, startIndex + channel->frameBytesRemaining);

                    /* Also stop when we completely receive a packet */
                    limit = ipcMin(limit, startIndex + channel->packetLength - channelIndex);

                    do
                    {
                        channel->packet[channelIndex++] = data[i++];

                    } while ((i < limit) && (data[i] != IPC_DEV_ESCAPE_BYTE));


                    --i; /* we incremented i one time too many */
                    channel->rxIndex = channelIndex;
                    channel->frameBytesRemaining -= (i - startIndex);

                    if (channel->rxIndex == channel->packetLength)
                    {
                        /* We have received the whole packet */
                        ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX %d Complete Packet\n", i));
                        ipcPacketDone(channel, device->portId);
                    }
                    break;

                } /* switch(current channel state) */

                if (--channel->frameBytesRemaining == 0)
                {
                    /* We have completed all data for the current channel */
                    if (++device->rx.channelIndex == ipcGetFrameNumChannels(device->rx.frameHeader))
                    {
                        /* We are done receiving data for all channels in this frame */
                        ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX All Frame Data\n"));
                        device->rx.state = IPC_RX_FRAME_START;
                    }
                    else
                    {
                        /* There are more channels left so update the current channel */
                        device->rx.currentChannelId = ipcGetFrameChannelId(device->rx.frameHeader,
                            device->rx.channelIndex);
                    }
                }
                break;

            } /* switch (device->rx.state) */

        } /* loop through all buffer data */
#endif /* BYPASS_DEVICE */
    } /* until device is terminated */

    ipcFree(data);

    ipcDebugLog(IPCLOG_DEVICE, ("Port %d input thread terminated\n", device->portId));
    /* set read function to NULL to signal that input thread has terminated */
    device->driver.read = NULL;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFrameHeaderDone
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when a complete frame header has been received. It
*   will verify the frame header CRC and call ipcDecodeFrameHeader if valid.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Oct2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcFrameHeaderDone(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcCrc16(0, device->rx.frameHeader, device->rx.frameLength) != 0)
    {
        /* If the frame header CRC is incorrect, we must wait for the next
         * frame start escape sequence */
        ipcWarning(IPCLOG_DEVICE, ("RESET, Recived bad frame header CRC\n"));
        ipcResetFrameDecoder(device);
    }
    else 
    {
        /* The CRC is correct so decode the frame header */
        ipcDecodeFrameHeader(device);
    
        //printRxFrame(device);

        if (ipcGetFrameNumChannels(device->rx.frameHeader) > 0)
        {
            /* Start processing frame data */
            device->rx.state = IPC_RX_DATA;
        }
        else
        {
            /* The frame was empty so prepare for next frame */
            device->rx.state = IPC_RX_FRAME_START;
        }

    } /* CRC was correct */
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcPacketDone
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when we completely receive an IPC packet.
*   
*   PARAMETER(S):
*       channel: pointer to channel we received the packet on
*       portId: port we received the packet on
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Oct2004  Motorola Inc.         Initial Creation
* 16Feb2006  Motorola Inc.         Optionally calculate CRC for data 
*
*****************************************************************************************/
void ipcPacketDone(ipcRxChannel_t *channel, ipcPortId_t portId)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    BOOL packetOK = TRUE;
    UINT16 crcValueCalculated;
    UINT16 crcValueFetched;
/*--------------------------------------- CODE -----------------------------------------*/
    if ((ipcGetMsgFlags(channel->packet) & IPC_PACKET_CRC) &&
        (ipcGetDestAddr(channel->packet) == ipcStack_glb.ipcAddr))
    {
        crcValueCalculated = ipcCrc16(0,
                                      ipcGetMsgData(channel->packet),
                                      ipcGetMsgLength(channel->packet));
        crcValueFetched = ipcGetDataChecksum(channel->packet);

        if (crcValueCalculated != crcValueFetched)
        {
            packetOK = FALSE;
            ipcWarning(IPCLOG_DEVICE, ("Packet CRC failed, calc=%x, fetch=%x\n", 
                crcValueCalculated, crcValueFetched));
            ipcFree(channel->packet);
        }
    }
    if (packetOK)
    {
        DVRTDataIndication((ipcPacket_t *)channel->packet, portId);
    }

    channel->packet = NULL;

    channel->state = IPC_RX_PACKET_START;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDecodeEscapeSequence
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function decodes the escape sequence once the second byte has been
*   received.  Currently there are three valid escape sequences that all begin
*   with IPC_DEV_ESCAPE_BYTE and are followed by one of three bytes:
*   
*   IPC_DEV_FRAME_START_BYTE - we are at the start of a new frame
*   
*   IPC_DEV_PACKET_START_BYTE - we are at the start of a new packet
*   
*   IPC_DEV_ESCAPE_BYTE - this is just a regular data byte that happened to be
*                        equal to the escape byte
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: the total length of the frame data in bytes (not counting the header)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Sep2004  Motorola Inc.         Initial Creation                     
*
*****************************************************************************************/
BOOL ipcDecodeEscapeSequence(ipcDevice_t *device, UINT8 escapeByte)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcRxChannel_t *channel;
    BOOL doneProcessingByte = FALSE;
/*--------------------------------------- CODE -----------------------------------------*/
    switch(escapeByte)
    {
    case IPC_DEV_FRAME_START_BYTE:
        ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX Escape Frame Start\n"));
        if (device->rx.state != IPC_RX_FRAME_START)
        {
            /* We received a start of frame sequence when we were not expecting one. */
            ipcWarning(IPCLOG_DEVICE, ("RESET, Received frame start in state %d\n", device->rx.state));
            ipcResetFrameDecoder(device);
        }

        /* We can now continue to the next state */
        device->rx.state = IPC_RX_NUM_CHANNELS;
        doneProcessingByte = TRUE;
        break;

    case IPC_DEV_PACKET_START_BYTE:
        ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX Escape Packet Start\n"));
        if (device->rx.state == IPC_RX_DATA)
        {
            channel = &device->rx.channels[device->rx.currentChannelId];
            if (channel->state != IPC_RX_PACKET_START)
            {
                /* If we receive a packet start escape sequence while we are in the middle of
                 * receiving a packet, we must be out of sync.  Reset the channel decoder. */
                ipcWarning(IPCLOG_DEVICE, 
                    ("CHANNEL %d RESET, Received packet start\n", device->rx.currentChannelId));
                ipcResetChannelDecoder(channel);
            }

            ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX Begin Packet Header\n"));
            channel->state = IPC_RX_PACKET_LENGTH_BYTE0;
        }
        else if (device->rx.state != IPC_RX_FRAME_START)
        {
            /* We received a start of packet sequence when we were not expecting one.
             * We can ignore it */
            ipcWarning(IPCLOG_DEVICE, ("Received packet start in state %d\n", device->rx.state));
        }
        doneProcessingByte = TRUE;
        break;

    case IPC_DEV_ESCAPE_BYTE:
        ipcDebugLog(IPCLOG_DEVICE_DECODE, ("<<< RX Escape Data\n"));
        /* This is a regular data byte that matched the escape byte so we will break
         * from this switch statement and process it below */
        break;

    default:
        ipcWarning(IPCLOG_DEVICE, ("Received invalid escape (%d) in state %d\n", escapeByte, device->rx.state));
        /* No other byte is expected after the escape byte so we are out of sync.
         * We will ignore this error and continue processing */
    }

    return doneProcessingByte;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcDecodeFrameHeader
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function decodes the frame header which has been completely received
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: the total length of the frame data in bytes (not counting the header)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 01Sep2004  Motorola Inc.         Initial Creation                     
*
*****************************************************************************************/
void ipcDecodeFrameHeader(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    ipcRxChannel_t *channel;
    UINT8 numChannels;
/*--------------------------------------- CODE -----------------------------------------*/
    numChannels = ipcGetFrameNumChannels(device->rx.frameHeader);

    if (numChannels > 0)
    {
        /* Set current channel to the first one in the frame */
        device->rx.channelIndex = 0;
        device->rx.currentChannelId = ipcGetFrameChannelId(device->rx.frameHeader, 0);

        /* Process each channel entry in the frame */
        for (i = 0; i < numChannels; i++)
        {
            channel = &device->rx.channels[ipcGetFrameChannelId(device->rx.frameHeader, i)];

            channel->frameBytesRemaining = ipcGetFrameChannelLength(device->rx.frameHeader, i);
        }
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcResetFrameDecoder
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called when the frame decoder receives an invalid frame byte.
*   It resets the frame decoder so we can wait for the next frame start sequence
*   and begin decoding the next frame from scratch.  It also resets all channels
*   in the frame.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Oct2004  Motorola Inc.         Initial Creation                      
*
*****************************************************************************************/
void ipcResetFrameDecoder(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int i;
    int numChannels;
    ipcRxChannel_t *channel;
/*--------------------------------------- CODE -----------------------------------------*/
    /* We only need to reset the channel decoders if we have already begun processing
     * channel data in this frame */
    if (device->rx.state == IPC_RX_DATA)
    {
        numChannels = ipcGetFrameNumChannels(device->rx.frameHeader);

        for (i = 0; i < numChannels; i++)
        {
            channel = &device->rx.channels[ipcGetFrameChannelId(device->rx.frameHeader, i)];
            ipcResetChannelDecoder(channel);
        }
    }

    /* Wait for a start of frame escape sequence */
    device->rx.state = IPC_RX_FRAME_START;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcResetChannelDecoder
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function is called to reset the channel decoder when an invalid packet
*   byte is received.  It will free the current packet (unless we are receiving
*   the header) and set the packet pointer to NULL.  This causes the channel
*   decoder to ignore all data until the next packet start sequence is received.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 22Oct2004  Motorola Inc.         Initial Creation                     
* 25Jan2005  Motorola Inc.         Shared memory debug 
*
*****************************************************************************************/
void ipcResetChannelDecoder(ipcRxChannel_t *channel)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcPacket_t *currentPacket = (ipcPacket_t *)channel->packet;
/*--------------------------------------- CODE -----------------------------------------*/
    if (currentPacket != NULL)
    {
        ipcFree(channel->packet);
        channel->packet = NULL;
    }

    /* We must wait for the packet start sequence to proceed */
    channel->state = IPC_RX_PACKET_START;
}

#endif /* IPC_SIMPLE_DEVICE */
