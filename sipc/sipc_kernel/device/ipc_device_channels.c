/*****************************************************************************************
*                Template No. SWF0111   Template Version No. 00.00.02
*
*                              COPYRIGHT (C) 2003 - 2005, 2007 MOTOROLA Inc.
*
*   This program is free software; you can redistribute it and/or modify it
*   under the terms of the GNU General Public License as published by the Free
*   Software Foundation; version 2 of the License.
*
*   This program is distributed in the hope that it will be useful, but WITHOUT
*   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
*   FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
*   more details.
*
*   You should have received a copy of the GNU General Public License along
*   with this program; if not, write to the Free Software Foundation, Inc.,
*   59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*
*
*
******************************************************************************************
*
*   FILE NAME       : ipc_device_channels.c
*   FUNCTION NAME(S): ipcCloseTxChannel
*                     ipcCloseTxChannelImmediately
*                     ipcCreateTxChannel
*                     ipcFreeTxChannelPackets
*                     ipcGetNextChannelId
*                     ipcInsertTxChannel
*                     ipcRemoveTxChannel
*
*--------------------------------------- PURPOSE -----------------------------------------
*
*   This file contains functions for creating, finding, and removing IPC
*   transmit and receive channels.
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Comments
* ---------  -------------------   -------------------------------------------------------
* 28Mar2003  Motorola Inc.         Initial Creation
* 02Jun2004  Motorola Inc.         Converted from C++ to C 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
* 13Jul2007  Motorola Inc.         Removed inclusion of ipc_device_debug.h
*
*****************************************************************************************/

#ifndef IPC_SIMPLE_DEVICE
/*------------------------------- HEADER FILE INCLUDES ---------------------------------*/
#include "ipc_list.h"
#include "ipc_device_layer.h"
#include "ipclog.h"

/*-------------------------------------- CONSTANTS -------------------------------------*/

#define IPC_MIN_BYTES_PER_FRAME     100

/*------------------------------------ ENUMERATIONS ------------------------------------*/
/*---------------------------- STRUCTURE/UNION DATA TYPES ------------------------------*/
/*------------------------------------- STATIC DATA ------------------------------------*/
/*------------------------------------- GLOBAL DATA ------------------------------------*/
/*----------------------------- LOCAL FUNCTION PROTOTYPES ------------------------------*/

ipcChannelId_t ipcGetNextChannelId(ipcDevice_t *device);
void ipcInsertTxChannel(ipcDevice_t *device, ipcTxChannel_t *newChannel);
void ipcRemoveTxChannel(ipcDevice_t *device, ipcTxChannel_t *channel);

/*--------------------------------------- MACROS ---------------------------------------*/


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCreateTxChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function allocates and initializes an ipcTxChannel_t structure in
*   response to a request from the upper layers.  It returns NULL if there
*   is not enough bandwidth available.
*   
*   PARAMETER(S):
*       portId: port ID for device to create channel on
*       bandwidth: the requested channel bandwidth in bits per second
*       channelId: channel ID to assign.  If IPC_INVALID_CHANNEL_ID is
*                              passed in, the next available ID will be assigned.
*                  
*   RETURN: Valid Channel ID - success (same as channelId passed in)
*           IPCENODE - invalid port ID
*           IPCENOBUFS - out of memory
*           IPCEQOS - not enough bandwidth
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 01Sep2004  Motorola Inc.         Added acknowledged channels
* 22Feb2005  Motorola Inc.         Return error code
* 08Apr2005  Motorola Inc.         Changed device param to port ID 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcCreateTxChannel(ipcPortId_t portId, INT32 bandwidth, ipcChannelId_t id)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device = NULL;
    ipcTxChannel_t *newChannel = NULL;
    INT32 requiredBytesPerFrame = 0;
/*--------------------------------------- CODE -----------------------------------------*/
    device = ipcGetDevice(portId);

    if (device == NULL)
    {
        ipcError(IPCLOG_DEVICE, ("ipcCreateTxChannel ERROR: Invalid Port ID: %d\n", portId));
        return IPCENODE;
    }

    newChannel = (ipcTxChannel_t *)ipcAlloc(sizeof(ipcTxChannel_t), 0);

    if (newChannel == NULL)
    {
        ipcError(IPCLOG_DEVICE, ("RTDVChannelRequest -- out of memory\n"));
        return IPCENOBUFS;
    }

    if (bandwidth > 0)
    {
        bandwidth >>= 3; /* Convert bits/sec to bytes/sec */
        requiredBytesPerFrame += bandwidth / device->framesPerSecond;

        /* bytes per frame must be at least IPC_MIN_BYTES_PER_FRAME */
        requiredBytesPerFrame = ipcMax(requiredBytesPerFrame, IPC_MIN_BYTES_PER_FRAME);
        requiredBytesPerFrame = ipcMin(requiredBytesPerFrame, 0xFFFF); /* limit to 16 bits */
    }
    
    newChannel->bytesPerFrame = (UINT16)requiredBytesPerFrame;
    newChannel->frameBytesRemaining = 0;
    newChannel->numBytesToSend = 0;
    newChannel->numBytesPending = 0;
    newChannel->txIndex = 0;
    newChannel->terminate = FALSE;
    newChannel->currentPacket = NULL;
    
    ipcListInit(&newChannel->lowPriorityPackets);
    ipcListInit(&newChannel->highPriorityPackets);

    if (ipcLockDevice(device) < 0)
    {
        ipcFree(newChannel);
        ipcError(IPCLOG_DEVICE,
                ("ipcCreateTxChannel: Failed to lock device\n"));
        return IPCEBUSY;
    }
    /* Get the next available channel ID (or assign fixed ID for control channel) */
    if (id == IPC_INVALID_CHANNEL_ID)
    {
        id = ipcGetNextChannelId(device);
    }

    /* If there was a channel ID available, and there is enough bandwidth, add the channel */
    if ((id != IPC_INVALID_CHANNEL_ID) && (requiredBytesPerFrame <= device->tx.availableBytesPerFrame))
    {
        newChannel->id = id;
        device->tx.availableBytesPerFrame -= requiredBytesPerFrame;
        ipcInsertTxChannel(device, newChannel);
        device->tx.channelPointers[id] = newChannel;
        ipcUnlockDevice(device);

        return id; /* Return the Channel ID */
    }

    /* Otherwise, there was an error so we must free the channel */
    ipcUnlockDevice(device);

    ipcFree(newChannel);
    
    if (id == IPC_INVALID_CHANNEL_ID)
    {
        /* There were no more channel IDs available */
        ipcError(IPCLOG_DEVICE, ("RTDVChannelRequest -- no more channel IDs\n"));
        return IPCENOBUFS;
    }
    else
    {
        /* There was not enough bandwidth available */
        ipcError(IPCLOG_DEVICE, ("RTDVChannelRequest -- not enough bandwidth\n"));
        return IPCEQOS;
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcGetNextChannelId
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function returns the next available ID for a new transmit channel
*   
*   PARAMETER(S):
*       device: pointer to device structure
*                  
*   RETURN: next available channel ID (or IPC_INVALID_CHANNEL_ID if none are left)
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
ipcChannelId_t ipcGetNextChannelId(ipcDevice_t *device)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    int iterationsLeft = IPC_LAST_CHANNEL_ID - IPC_FIRST_CHANNEL_ID + 1;
    ipcChannelId_t id;
/*--------------------------------------- CODE -----------------------------------------*/
    id = device->tx.nextChannelId;

    /* Increment Channel ID until we find one that isn't taken or we have
     * tried every single one */
    while ((iterationsLeft-- > 0) && (device->tx.channelPointers[id] != NULL))
    {
        if (id++ == IPC_LAST_CHANNEL_ID)
        {
            id = IPC_FIRST_CHANNEL_ID;
        }
    }

    /* If we tried every channel ID and they were all taken, return IPC_INVALID_CHANNEL_ID */
    if (iterationsLeft == -1)
    {
        id = IPC_INVALID_CHANNEL_ID;
    }
    /* Otherwise, update tx.nextChannelId for next time */
    else if (id == IPC_LAST_CHANNEL_ID)
    {
        device->tx.nextChannelId = IPC_FIRST_CHANNEL_ID;
    }
    else
    {
        device->tx.nextChannelId = id + 1;
    }

    return(id);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCloseTxChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will remove a transmit channel immediately if there is no more
*   data to send.  Otherwise, it will mark it for termination so that it can be
*   removed once all the data has been sent.
*   
*   PARAMETER(S):
*       portId: port ID of device to close channel on
*       id: ID of transmit channel to close
*                  
*   RETURN: IPC_OK for success, IPCECHAN if channel not found
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation
* 07Apr2005  Motorola Inc.         Updated parameters 
* 05Jul2007  Motorola Inc.         Set error code on lock failure to IPCEBUSY
*
*****************************************************************************************/
int ipcCloseTxChannel(ipcPortId_t portId, ipcChannelId_t id)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcDevice_t *device;
    ipcTxChannel_t *channel;
    int rc = IPC_OK;
/*--------------------------------------- CODE -----------------------------------------*/
    device = ipcGetDevice(portId);
    if (device == NULL)
    {
        return IPCECHAN;
    }

    channel = device->tx.channelPointers[id];
    if (channel == NULL)
    {
        return IPCECHAN;
    }
    
    if (ipcLockDevice(device) < 0)
    {
        ipcError(IPCLOG_DEVICE,
                ("Failed to lock device\n"));
        return IPCEBUSY;
    }
    /* if no more data to send, we can remove the channel immediately */
    if ((channel->numBytesPending == 0) && (channel->numBytesToSend == 0))
    {
        ipcRemoveTxChannel(device, channel);
    }
    else /* otherwise mark it for deletion later. */
    {
        channel->terminate = TRUE;
        rc = IPCEBUSY;
    }
    ipcUnlockDevice(device);

    return rc;
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcCloseTxChannelImmediately
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will remove a transmit channel immediately even if there is
*   still data to send.  It will free all pending packets without sending them.
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       channel: pointer to transmit channel to remove
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcCloseTxChannelImmediately(ipcDevice_t *device, ipcTxChannel_t *channel)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    if (ipcLockDevice(device) < 0)
    {
        ipcError(IPCLOG_GENERAL,
                ("ipcCloseTxChannelImmediately: Failed to lock device\n"));
        return;
    }

    ipcFreeTxChannelPackets(channel);

    if (channel->currentPacket != NULL)
    {
        ipcFree(channel->currentPacket);
        channel->currentPacket = NULL;
    }

    ipcRemoveTxChannel(device, channel);

    ipcUnlockDevice(device);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcRemoveTxChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will free the channel structure and remove it from the txList
*   and txChannelLookupTable for this device.  It should not be called directly
*   since it does not protect any data structures (ipcCloseTxChannel and
*   ipcCloseTxChannelImmediately should be used instead).
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       channel: pointer to transmit channel to remove
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcRemoveTxChannel(ipcDevice_t *device, ipcTxChannel_t *channel)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    device->tx.channelPointers[channel->id] = NULL;

    /* remove channel from txChannelList */
    ipcListRemoveLink(&device->tx.channelList, (ipcListLink_t)channel);

    /* return channel bandwidth to device */
    device->tx.availableBytesPerFrame += channel->bytesPerFrame;

    ipcFree(channel);
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcInsertTxChannel
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will insert a transmit channel into the device's txList
*   according to its allocated bandwidth (highest BW first).
*   
*   PARAMETER(S):
*       device: pointer to device structure
*       newChannel: the transmit channel to insert
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcInsertTxChannel(ipcDevice_t *device, ipcTxChannel_t *newChannel)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
    ipcListLink_t prev;
    ipcListLink_t curr;
/*--------------------------------------- CODE -----------------------------------------*/
    for (ipcListFirstPair(device->tx.channelList, prev, curr); 
        curr != NULL;
        ipcListNextPair(prev, curr))
    {
        if (((ipcTxChannel_t *)curr)->bytesPerFrame < newChannel->bytesPerFrame)
        {
            /* We have reached a channel with less bandwidth than the one we are
             * inserting so we must insert the new channel before it */
            ipcListInsertLink(&device->tx.channelList, (ipcListLink_t)newChannel, prev);
            break;
        }
    }

    if (curr == NULL)
    {
        /* We made it all the way to the end of the list without inserting the
         * new channel so we will insert it at the tail */
        ipcListAddTail(&device->tx.channelList, (ipcListLink_t)newChannel);
    }
}


/*****************************************************************************************
*                Template No. SWFXXXX   Template Version No. 00.00.01
*
*   FUNCTION NAME: ipcFreeTxChannelPackets
*
*--------------------------------------- SYNOPSIS ----------------------------------------
*
*   PURPOSE/DESCRIPTION:
*   
*   This function will free all pending packets on a TX channel.  It is used
*   to reset the device when the link goes down.
*   
*   PARAMETER(S):
*       channel: the transmit channel to free packets for
*                  
*   RETURN: void
*   
*
*   MODULE DATA ACCESSED:
*
*   GLOBAL DATA ACCESSED:
*
*-------------------------------------- REVISIONS ----------------------------------------
* Date       Author                Description
* ---------  --------------------  --------------------------------------------------------
* 16Jun2004  Motorola Inc.         Initial Creation 
*
*****************************************************************************************/
void ipcFreeTxChannelPackets(ipcTxChannel_t *channel)
{
/*---------------------------------- LOCAL VARIABLES -----------------------------------*/
/*--------------------------------------- CODE -----------------------------------------*/
    while (channel->lowPriorityPackets.head != NULL)
    {
        ipcFree(ipcListRemoveHead(&channel->lowPriorityPackets));
    }
 
    while (channel->highPriorityPackets.head != NULL)
    {
        ipcFree(ipcListRemoveHead(&channel->highPriorityPackets));
    }

    channel->numBytesPending = 0;
    channel->numBytesToSend = 0;
}

#endif /* IPC_SIMPLE_DEVICE */
