// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI event services within the SUAPI kernel
 *      module.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>
#include <linux/suapi_module.h>
#include <linux/su_ktimers.h>
#include <linux/mpm.h>

/* For conversion of msecs to jiffies, the assumption in this module is that
 * LONG_MAX == MAX_SCHEDULE_TIMEOUT and that the ratio of msecs to jiffies is
 * greater than or equal to 1.
 */

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
/* Events-specific spin lock.
 * When this lock is used during the timer function handler, soft interrupts
 * are not explicitly disabled. At all other times, soft interrupts are
 * expected to be disabled when locking this spinlock because it is used
 * during soft interrupt context.
 */
spinlock_t su_events_lock = SPIN_LOCK_UNLOCKED;

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/

int suapi_alloc_event_ioctl(int arg)
{
    int event_num;
    unsigned long event_mask;

    spin_lock_bh(&su_events_lock);

    /* Check to see if there are zero bits in su_alloc_flags */
    if(current->su_alloc_flags == ~0UL){
        spin_unlock_bh(&su_events_lock);
        return -ENOSPC;
    }

    event_num = 31 - ffz(current->su_alloc_flags);

    event_mask = suEvnumToEvmask(event_num);

    current->su_event_flags &= ~(event_mask);
    current->su_alloc_flags |= event_mask;

    spin_unlock_bh(&su_events_lock);

    return event_num;
}

int suapi_alloc_event_mask_ioctl(int arg)
{
    unsigned long event_mask = (unsigned long) arg;

    spin_lock_bh(&su_events_lock);

    if((current->su_alloc_flags & event_mask) != 0)
    {
        spin_unlock_bh(&su_events_lock);
        return -EINVAL;
    }

    current->su_alloc_flags |= event_mask;
    current->su_event_flags &= ~(event_mask);

    spin_unlock_bh(&su_events_lock);

    return(0);
}

int suapi_free_event_ioctl(int arg)
{
    int ret = 0;
    unsigned long mask;

    spin_lock_bh(&su_events_lock);

    if ((0 <= arg) && (arg < 32)){
        mask = suEvnumToEvmask(arg);
        current->su_alloc_flags &= ~mask;
    }
    else
        ret = -EINVAL;

    spin_unlock_bh(&su_events_lock);

    return ret;
}

int suapi_set_event_mask_ioctl(int arg)
{
    SuapiEventInfo set_event_info;

    if (copy_from_user(&set_event_info,
                       (SuapiEventInfo *) arg,
                       sizeof(SuapiEventInfo)))
        return -EFAULT;

    return suapi_internal_set_event_mask(set_event_info.p, set_event_info.ev, SU_NOT_IN_ISR);
}

int suapi_internal_set_event_mask(pid_t pid, unsigned long mask, int in_isr)
{
    int ret = 0;
    struct task_struct *t;

    read_lock(&tasklist_lock);

    if (pid == SU_SELF_PID)
        t = current;
    else
        t = find_task_by_pid(pid);

    if (t)
    {
        spin_lock_bh(&su_events_lock);
        if (mask == (t->su_alloc_flags & mask))
        {
            t->su_event_flags |= mask;

            if (((t->su_state == SUAPI_TASK_STATE_WAIT_ALL_EVENTS) &&
                (t->su_wait_flags == (t->su_event_flags & t->su_wait_flags)))
                ||((t->su_state == SUAPI_TASK_STATE_WAIT_ANY_EVENT) &&
                (t->su_event_flags & t->su_wait_flags)))
            {
                spin_unlock_bh(&su_events_lock);
#if defined(CONFIG_MOT_FEAT_PM)
                if (in_isr)
                    mpm_handle_ioi();
#endif
                wake_up_process(t);
            }
            else
                spin_unlock_bh(&su_events_lock);
            ret = 0;
        }
        else{
            ret = -EINVAL;
            spin_unlock_bh(&su_events_lock);
        }
    }
    else
        ret = -ESRCH;

    read_unlock(&tasklist_lock);
    return(ret);
}

int suapi_clear_event_mask_ioctl(int arg)
{
    unsigned long mask;
    int ret = 0;
    pid_t pid;
    struct task_struct *t;
    SuapiEventInfo clear_event_info;

    if (copy_from_user(&clear_event_info,
                       (SuapiEventInfo *) arg,
                       sizeof(SuapiEventInfo)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (copy_to_user((SuapiEventInfo *) arg,
                     &clear_event_info,
                     sizeof(SuapiEventInfo)))
        return -EFAULT;

    mask = clear_event_info.ev;
    pid = clear_event_info.p;

    read_lock(&tasklist_lock);

    if (pid == SU_SELF_PID)
        t = current;
    else
        t = find_task_by_pid(pid);

    if (t)
    {
        spin_lock_bh(&su_events_lock);
        if (mask == (t->su_alloc_flags & mask))
        {
            clear_event_info.ev = t->su_event_flags & mask;
            t->su_event_flags &= ~mask;
        }
        else
            ret = -EINVAL;
        spin_unlock_bh(&su_events_lock);
    }
    else
        ret = -ESRCH;

    read_unlock(&tasklist_lock);

    if (!ret)
        /* Assumes output address is still writable. */
        ret = copy_to_user((SuapiEventInfo *) arg,
                           &clear_event_info,
                           sizeof(SuapiEventInfo));

    return(ret);
}

int suapi_verify_alloc_event_mask_ioctl(int arg)
{
    SuapiEventInfo verify_event_info;

    if (copy_from_user(&verify_event_info,
                       (SuapiEventInfo *) arg,
                       sizeof(SuapiEventInfo)))
        return -EFAULT;

    return suapi_internal_verify_alloc_event_mask(verify_event_info.p,
                                                  verify_event_info.ev);
}

int suapi_internal_verify_alloc_event_mask(pid_t pid, unsigned long mask)
{
    int ret = 0;
    struct task_struct *t;

    read_lock(&tasklist_lock);

    if (pid == SU_SELF_PID)
        t = current;
    else
        t = find_task_by_pid(pid);

    if (t)
    {
        spin_lock_bh(&su_events_lock);
        if (mask == (t->su_alloc_flags & mask))
            ret = 0;
        else
            ret = -EINVAL;
        spin_unlock_bh(&su_events_lock);
    }
    else
        ret = -ESRCH;

    read_unlock(&tasklist_lock);
    return(ret);
}

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
int suapi_wait_any_event_ioctl(int arg)
{
    SuapiWaitEventInfo wait_event_info;
    int ret = 0;
    Suapi_Kernel_Timer local_timer;
    Suapi_Tasklet_Handler_Data pend_info;

    if (copy_from_user(&wait_event_info,
                       (SuapiWaitEventInfo *) arg,
                       sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if(copy_to_user((SuapiWaitEventInfo *) arg,
                     &wait_event_info,
                     sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    spin_lock_bh(&su_events_lock);
    if (wait_event_info.ev != (wait_event_info.ev & current->su_alloc_flags))
    {
        spin_unlock_bh(&su_events_lock);
        return -EINVAL;
    }
    if (0 > wait_event_info.timeout)
    {
        spin_unlock_bh(&su_events_lock);
        return -ERANGE;

    }

    if (wait_event_info.ev & current->su_event_flags)
    {
        current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
        current->su_wait_flags = 0;
        wait_event_info.ev &= current->su_event_flags;
        current->su_event_flags &= ~wait_event_info.ev;
        spin_unlock_bh(&su_events_lock);
        ret = copy_to_user((SuapiWaitEventInfo *) arg,
                       &wait_event_info,
                       sizeof(SuapiWaitEventInfo));
        return ret;
    }
    else
    {
        if (wait_event_info.timeout == 0)
        {
            /* Don't wait */
            spin_unlock_bh(&su_events_lock);
            return -EAGAIN;
        }
    }
    spin_unlock_bh(&su_events_lock);
 
    pend_info.tasklet_done = 0;
    if (wait_event_info.timeout != LONG_MAX)
    {
        pend_info.pid = current->pid;
        rtc_sw_task_init(&local_timer.ltimer, suapi_tasklet_handler_fn, (unsigned long)&pend_info);
        rtc_sw_task_schedule(wait_event_info.timeout, &local_timer.ltimer);
    }    
    spin_lock_bh(&su_events_lock);
    current->su_wait_flags = wait_event_info.ev;
    for (;;)
    {
        if (signal_pending(current))
        {
            ret = -ERESTARTSYS;
            break;
        }
        current->su_state = SUAPI_TASK_STATE_WAIT_ANY_EVENT;
        if (wait_event_info.ev & current->su_event_flags)
        {
            ret = 0;
            break;
        }
        if (wait_event_info.ev != (wait_event_info.ev & current->su_alloc_flags))
        {
            ret = -EINVAL;
            break;
        }
        if (wait_event_info.timeout != LONG_MAX)
        {
            if(pend_info.tasklet_done)
            {
                ret = -ETIMEDOUT;
                break;
            }
        }
        /* Wait forever. */
        set_current_state(TASK_INTERRUPTIBLE);
        spin_unlock_bh(&su_events_lock);
        schedule();

        /* Go into deep sleep. */
        if (unlikely(current->flags & PF_FREEZE))
            refrigerator(PF_FREEZE);

        spin_lock_bh(&su_events_lock);
    }

    set_current_state(TASK_RUNNING);
    current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
    current->su_wait_flags = 0;
    if (!ret)
    {
        wait_event_info.ev &= current->su_event_flags;
        current->su_event_flags &= ~wait_event_info.ev;
    }
    else
        wait_event_info.ev = 0;

    spin_unlock_bh(&su_events_lock);

    if (wait_event_info.timeout != LONG_MAX)
    {
        if (!pend_info.tasklet_done)
            rtc_sw_task_kill(&local_timer.ltimer);
    }

    /* Assumes output address is still writable. */
    if (!ret)
        ret = copy_to_user((SuapiWaitEventInfo *) arg,
                           &wait_event_info,
                           sizeof(SuapiWaitEventInfo));

    return(ret);
}
#else
int suapi_wait_any_event_ioctl(int arg)
{
    SuapiWaitEventInfo wait_event_info;
    int ret = 0;
    long expires = 0;


    if (copy_from_user(&wait_event_info,
                       (SuapiWaitEventInfo *) arg,
                       sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if(copy_to_user((SuapiWaitEventInfo *) arg,
                     &wait_event_info,
                     sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    /* Compute expiration here so that it is not recalculated each time through
     * the loop. If timeout is LONG_MAX (wait forever) or 0 (don't wait), the
     * expires variable will never be used.
     */
    expires = msecs_to_jiffies(wait_event_info.timeout);

    spin_lock_bh(&su_events_lock);
    for (;;)
    {
        current->su_state = SUAPI_TASK_STATE_WAIT_ANY_EVENT;
        current->su_wait_flags = wait_event_info.ev;
        if (wait_event_info.ev != (wait_event_info.ev & current->su_alloc_flags))
        {
            ret = -EINVAL;
            break;
        }
        if (wait_event_info.ev & current->su_event_flags)
        {
            ret = 0;
            break;
        }
        if (signal_pending(current))
        {
            ret = -ERESTARTSYS;
            break;
        }
        if (wait_event_info.timeout == 0)
        {
            /* Don't wait */
            ret = -EAGAIN;
            break;
        }
        else if (wait_event_info.timeout == LONG_MAX)
        {
            /* Wait forever. */
            set_current_state(TASK_INTERRUPTIBLE);
            spin_unlock_bh(&su_events_lock);
            schedule();

            /* Go into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_events_lock);
            set_current_state(TASK_RUNNING);
        }
        else
        {
            /* HZ must be 1 or more, so number of jiffies will be less than
             * number of milliseconds. Since our milliseconds are less than
             * LONG_MAX due to type, the number of jiffies will be less than
             * LONG_MAX. So it's safe to convert to jiffies.
             */
            if (0 > wait_event_info.timeout || expires > MAX_SCHEDULE_TIMEOUT)
            {
                ret = -ERANGE;
                break;
            }

            set_current_state(TASK_INTERRUPTIBLE);
            spin_unlock_bh(&su_events_lock);
            expires = schedule_timeout(expires);

            /* Go into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_events_lock);
            set_current_state(TASK_RUNNING);
            if (0 == expires)
            {
                ret = -ETIMEDOUT;
                break;
            }
        }
    }

    current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
    current->su_wait_flags = 0;
    if (!ret)
        wait_event_info.ev &= current->su_event_flags;
    else
        wait_event_info.ev = 0;

    if (!ret)
        current->su_event_flags &= ~wait_event_info.ev;

    spin_unlock_bh(&su_events_lock);

    /* Assumes output address is still writable. */
    if (!ret)
        ret = copy_to_user((SuapiWaitEventInfo *) arg,
                           &wait_event_info,
                           sizeof(SuapiWaitEventInfo));

    return(ret);
}

#endif

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
int suapi_wait_all_event_ioctl(int arg)
{
    SuapiWaitEventInfo wait_event_info;
    int ret = 0;
    Suapi_Kernel_Timer local_timer;
    Suapi_Tasklet_Handler_Data pend_info;

    if (copy_from_user(&wait_event_info,
                       (SuapiWaitEventInfo *) arg,
                       sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if(copy_to_user((SuapiWaitEventInfo *) arg,
                     &wait_event_info,
                     sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    spin_lock_bh(&su_events_lock);
    if (wait_event_info.ev != (wait_event_info.ev & current->su_alloc_flags))
    {
        spin_unlock_bh(&su_events_lock);
        return -EINVAL;
    }
    if (0 > wait_event_info.timeout)
    {
        spin_unlock_bh(&su_events_lock);
        return -ERANGE;

    }
    if (wait_event_info.ev == (wait_event_info.ev & current->su_event_flags))
    {
        current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
        current->su_wait_flags = 0;
        current->su_event_flags &= ~wait_event_info.ev;
        spin_unlock_bh(&su_events_lock);
        ret = copy_to_user((SuapiWaitEventInfo *) arg,
                     &wait_event_info,
                     sizeof(SuapiWaitEventInfo));
        return ret;
    }
    else
    {
        if (wait_event_info.timeout == 0)
        {
            /* Don't wait */
            spin_unlock_bh(&su_events_lock);
            return -EAGAIN;
        }
    }
    spin_unlock_bh(&su_events_lock);

    pend_info.tasklet_done = 0;
    if (wait_event_info.timeout != LONG_MAX)
    {
        pend_info.pid = current->pid;
        rtc_sw_task_init(&local_timer.ltimer, suapi_tasklet_handler_fn, (unsigned long)&pend_info);
        rtc_sw_task_schedule(wait_event_info.timeout, &local_timer.ltimer);

    }

    spin_lock_bh(&su_events_lock);
    current->su_wait_flags = wait_event_info.ev;
    current->su_state = SUAPI_TASK_STATE_WAIT_ALL_EVENTS;
    for (;;)
    {
        if (signal_pending(current))
        {
            ret = -ERESTARTSYS;
            break;
        }
        if (wait_event_info.ev == (wait_event_info.ev & current->su_event_flags))
        {
            ret = 0;
            break;
        }
        if (wait_event_info.ev != (wait_event_info.ev & current->su_alloc_flags))
        {
            ret = -EINVAL;
            break;
        }
        if (wait_event_info.timeout != LONG_MAX)
        {
            if(pend_info.tasklet_done)
            {
                ret = -ETIMEDOUT;
                break;
            }
        }
        /* Wait forever. */
        set_current_state(TASK_INTERRUPTIBLE);
        spin_unlock_bh(&su_events_lock);
        schedule();

        /* Go into deep sleep. */
        if (unlikely(current->flags & PF_FREEZE))
            refrigerator(PF_FREEZE);

        spin_lock_bh(&su_events_lock);
    }

    current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
    set_current_state(TASK_RUNNING);
    current->su_wait_flags = 0;
    if (ret!=0)
        wait_event_info.ev = 0;
    else
        current->su_event_flags &= ~wait_event_info.ev;

    spin_unlock_bh(&su_events_lock);

    if (wait_event_info.timeout != LONG_MAX)
    {
        if (!pend_info.tasklet_done)
            rtc_sw_task_kill(&local_timer.ltimer);
    }

    /* Assumes output address is still writable. */
    if (!ret)
        ret = copy_to_user((SuapiWaitEventInfo *) arg,
                         &wait_event_info,
                         sizeof(SuapiWaitEventInfo));

    return(ret);
}
#else
int suapi_wait_all_event_ioctl(int arg)
{
    SuapiWaitEventInfo wait_event_info;
    int ret = 0;
    long expires = 0;


    if (copy_from_user(&wait_event_info,
                       (SuapiWaitEventInfo *) arg,
                       sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if(copy_to_user((SuapiWaitEventInfo *) arg,
                     &wait_event_info,
                     sizeof(SuapiWaitEventInfo)))
        return -EFAULT;

    /* Compute expiration here so that it is not recalculated each time through
     * the loop. If timeout is LONG_MAX (wait forever) or 0 (don't wait), the
     * expires variable will never be used.
     */
    expires = msecs_to_jiffies(wait_event_info.timeout);

    spin_lock_bh(&su_events_lock);
    for (;;)
    {
        current->su_state = SUAPI_TASK_STATE_WAIT_ALL_EVENTS;
        current->su_wait_flags = wait_event_info.ev;
        set_current_state(TASK_INTERRUPTIBLE);
        if (wait_event_info.ev != (wait_event_info.ev & current->su_alloc_flags))
        {
            ret = -EINVAL;
            break;
        }
        if (wait_event_info.ev == (wait_event_info.ev & current->su_event_flags))
        {
            ret = 0;
            break;
        }
        if (signal_pending(current))
        {
            ret = -ERESTARTSYS;
            break;
        }
        if (wait_event_info.timeout == 0)
        {
            /* Don't wait */
            ret = -EAGAIN;
            break;
        }
        else if (wait_event_info.timeout == LONG_MAX)
        {
            /* Wait forever. */
            spin_unlock_bh(&su_events_lock);
            schedule();

            /* Go into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_events_lock);
        }
        else
        {
            /* HZ must be 1 or more, so number of jiffies will be less than
             * number of milliseconds. Since our milliseconds are less than
             * LONG_MAX due to type, the number of jiffies will be less than
             * LONG_MAX. So it's safe to convert to jiffies.
             */
            if (0 > wait_event_info.timeout || expires > MAX_SCHEDULE_TIMEOUT)
            {
                ret = -ERANGE;
                break;
            }

            spin_unlock_bh(&su_events_lock);
            expires = schedule_timeout(expires);

            /* Go into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_events_lock);
            if (0 == expires)
            {
                ret = -ETIMEDOUT;
                break;
            }
        }
    }

    current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
    set_current_state(TASK_RUNNING);
    current->su_wait_flags = 0;
    if (ret!=0)
        wait_event_info.ev = 0;
    else
        current->su_event_flags &= ~wait_event_info.ev;

    spin_unlock_bh(&su_events_lock);
    /* Assumes output address is still writable. */
    if (!ret)
        ret = copy_to_user((SuapiWaitEventInfo *) arg,
                         &wait_event_info,
                         sizeof(SuapiWaitEventInfo));

    return(ret);
}
#endif

int suapi_get_event_mask_ioctl(int arg)
{
    int ret = 0;
    struct task_struct *t;
    pid_t pid;
    unsigned long event_mask = 0; /* just to stop a warning */

    ret = get_user(pid,(int *) arg);
    if(ret)
        return(ret);

    /* Attempt to copy data to output address to ensure it is writable. */
    ret = put_user(pid,(int *) arg);
    if(ret)
        return(ret);

    read_lock(&tasklist_lock);

    if (pid == SU_SELF_PID)
        t = current;
    else
        t = find_task_by_pid(pid);

    if (t)
    {
        spin_lock_bh(&su_events_lock);
        event_mask = t->su_event_flags;
        spin_unlock_bh(&su_events_lock);
    }
    else
        ret = -ESRCH;

    read_unlock(&tasklist_lock);

    if (!ret)
        /* Assumes output address is still writable. */
        ret = put_user((int)(event_mask),(int *) arg);

    return(ret);
}
