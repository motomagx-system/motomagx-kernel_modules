// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI sysfs file entries in the /sys
 *      filesystem from within the SUAPI kernel module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>

#include <linux/suapi_module.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
/* Declare and initialize the dev_attr_su_sysconfig variable. */
DEVICE_ATTR(su_sysconfig, S_IRUGO, suapi_show_su_sysconfig, NULL);
/* Declare and initialize the dev_attr_minfo variable. */
DEVICE_ATTR(minfo, S_IRUGO, suapi_show_minfo, NULL);

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/

ssize_t suapi_show_su_sysconfig(struct device *dev, char *buf)
{
    sprintf(buf, "\n\
Min Timeout:\t\t\t%u\n\
Max Timeout:\t\t\t%u\n\
Number of Queues:\t\t%u\n\
Number of Memory Partitions:\t%u\n\
Number of Timers:\t\t%u\n\
Number of Binary Semaphores:\t%u\n\
Number of Counting Semaphores:\t%u\n\
Number of Mutex Semaphores:\t%u\n\
Memory Promotion:\t\t%s\n\
Name Services Hash Table Size:\t\t%u\n\
Number of Ports:\t\t%u\n\
Number of Flag Groups:\t\t%u\n\
Real time floor:\t\t%u\n\
Memory Block Alignment:\t\t%u\n\
Total Memory Size:\t\t%u\n",
        suapi_sysconfig_ptr->mintimeout,
        suapi_sysconfig_ptr->maxtimeout,
        suapi_sysconfig_ptr->nqueues,
        suapi_sysconfig_ptr->npartition,
        suapi_sysconfig_ptr->ntimers,
        suapi_sysconfig_ptr->nbinarysemaphores,
        suapi_sysconfig_ptr->ncountingsemaphores,
        suapi_sysconfig_ptr->nmutexsemaphores,
        (suapi_sysconfig_ptr->memorypromotion)?"On":"Off",
        suapi_sysconfig_ptr->namehashtablesize,
        suapi_sysconfig_ptr->nports,
        suapi_sysconfig_ptr->nflaggroups,
        suapi_sysconfig_ptr->realtimefloor,
        suapi_sysconfig_ptr->memblkalignment,
        suapi_sysconfig_ptr->total_mem_size);

    return strlen(buf);
}

ssize_t suapi_show_minfo(struct device *dev, char *buf)
{
#define MINFOLINE 50
    char * localp;
    int i;

    sprintf(buf, "\n\
Memory promotion is %s\n\
              Alloc  Total  Hiwt   Start\n\
ID Block Size Blocks Blocks Blocks Address\n\
== ========== ====== ====== ====== ==========\n",
        (suapi_sysconfig_ptr->memorypromotion)?"ON":"OFF");
    for(localp = buf + strlen(buf), i = 0;
         i < suapi_sysconfig_ptr->npartition && localp < ((buf+4096)-MINFOLINE);
         i++)
    {
        localp += snprintf(
             localp,
             MINFOLINE,
             "%2d 0x%08x 0x%04x 0x%04x 0x%04x 0x%08x\n",
             i,
             suapi_pcb_ptr[i].blocksize,
             suapi_pcb_ptr[i].nb_blocks-suapi_pcb_ptr[i].free_buffers,
             suapi_pcb_ptr[i].nb_blocks,
             suapi_pcb_ptr[i].nb_blocks-suapi_pcb_ptr[i].min_free_buffers,
             (unsigned int) SU_KERNEL_TO_USER(suapi_pcb_ptr[i].part_start_add));
    }

    return strlen(buf);
}
