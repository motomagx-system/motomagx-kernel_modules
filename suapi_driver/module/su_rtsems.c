/*
 * Functions in this file are based on kernel/rt.c. Differences between the
 * original functions and the ones in this file are commented and marked
 * with %%.
 *
 * Real-Time Preemption Support
 *
 * started by Ingo Molnar:
 *
 *  Copyright (C) 2004 Red Hat, Inc., Ingo Molnar <mingo@redhat.com>
 *
 * lock debugging, locking tree, deadlock detection:
 *
 *  Copyright (C) 2004, LynuxWorks, Inc., Igor Manyilov, Bill Huey
 *  Released under the General Public License (GPL).
 *
 */

// Copyright (C) 2006,2007 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.
// 2007-01-22 Motorola       Removed WARN_ON() so no absolute path in object.
//                           Removed filename and line number from
//                           su_sema_init().

#include <linux/config.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/module.h>
#include <linux/spinlock.h>
#include <linux/kallsyms.h>
#include <linux/syscalls.h>
#include <linux/interrupt.h>
#include <linux/suapi_module.h>
#include <linux/su_ktimers.h>

/* %%: eip was removed as a parameter for su_task_blocks_on_lock since it was
 * %%: used for debugging purposes only.
 */
static void
su_task_blocks_on_lock(struct su_rt_mutex_waiter *waiter, struct task_struct *task,
		   struct su_rt_mutex *lock)
{
	/* %%: deadlock check and save of waiter in task struct were removed */

	waiter->lock = lock;
	waiter->task = task;

	/* %%: init of priority inheritance list was removed */

	/*
	 * Add SCHED_NORMAL tasks to the end of the waitqueue (FIFO):
	 */
	if (!rt_task(task)) {
		list_add_tail(&waiter->list, &lock->wait_list);
		return;
	}

	/* %%: manipulation of priority inheritance list was removed */

	/*
	 * Add RT tasks to the head:
	 */
	list_add(&waiter->list, &lock->wait_list);
}

/*
 * initialise the lock:
 */
static void __su_init_rt_mutex(struct su_rt_mutex *lock, int save_state, int debug,
				char *name)
{
	lock->owner = NULL;
	spin_lock_init(&lock->wait_lock);
	INIT_LIST_HEAD(&lock->wait_list);

	/* %%: debug code was removed. */
}

static void su_set_new_owner(struct su_rt_mutex *lock, struct task_struct *old_owner,
			struct task_struct *new_owner)
{
	if (new_owner)
		trace_special_pid(new_owner->pid, new_owner->prio, 0);

	/* %%: manipulation of priority inheritance list was removed */

	lock->owner = new_owner;
	lock->owner_prio = new_owner->prio;

	/* %%: debug code was removed. */
}

/*
 * handle the lock release when processes blocked on it that can now run
 * - the spinlock must be held by the caller
 */
/* %%: The eip debug parameter was removed. */
static inline struct task_struct * su_pick_new_owner(struct su_rt_mutex *lock,
		struct task_struct *old_owner, int save_state)
{
	struct su_rt_mutex_waiter *w, *waiter = NULL;
	struct task_struct *new_owner;
	struct list_head *curr;

	/*
	 * Get the highest prio one:
	 *
	 * (same-prio RT tasks go FIFO)
	 */
	list_for_each(curr, &lock->wait_list) {
		w = list_entry(curr, struct su_rt_mutex_waiter, list);
		trace_special_pid(w->task->pid, w->task->prio, 0);
		/*
		 * Break out upon meeting the first non-RT-prio
		 * task - we inserted them to the tail, so if we
	 	 * see the first one the rest is SCHED_NORMAL too:
	 	 */
		if (!rt_task(w->task))
			break;
		if (!waiter || w->task->prio <= waiter->task->prio)
			waiter = w;
	}

	/*
	 * If no RT waiter then pick the first one:
	 */
	if (!waiter)
		waiter = list_entry(lock->wait_list.next,
					struct su_rt_mutex_waiter, list);
	trace_special_pid(waiter->task->pid, waiter->task->prio, 0);

	/* %%: manipulation of priority inheritance list was removed */

	new_owner = waiter->task;
	list_del_init(&waiter->list);

	/* %%: manipulation of priority inheritance list was removed */

	su_set_new_owner(lock, old_owner, new_owner);
	/* Don't touch waiter after ->task has been NULLed */
	mb();
	waiter->task = NULL;
	new_owner->blocked_on = NULL;

	/* %%: debug code was removed. */

	return new_owner;
}

static inline void su_init_lists(struct su_rt_mutex *lock)
{
	// we have to do this until the static initializers get fixed:
	if (!lock->wait_list.prev && !lock->wait_list.next)
		INIT_LIST_HEAD(&lock->wait_list);

	/* %%: debug code was removed. */
}

/*
 * get a lock - interruptible with timeout
 */
/* %%: The eip debug parameter was removed. */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
static int __sched __su_down_interruptible_timeout(struct su_rt_mutex *lock,
					signed long timeout)
{
	struct task_struct *task = current;
	unsigned long nosched_flag;
	struct su_rt_mutex_waiter waiter;
	int ret;
        Suapi_Kernel_Timer local_timer;
        Suapi_Tasklet_Handler_Data pend_info;

	/* %%: debug code was removed. */

	spin_lock(&lock->wait_lock);

	su_init_lists(lock);

	if (!lock->owner) {
		/* granted */
	        /* %%: debug and pi manipulation  code was removed. */
		su_set_new_owner(lock, NULL, task);
		spin_unlock(&lock->wait_lock);

		return 0;
	}

	if (timeout <= 0) {
	    spin_unlock(&lock->wait_lock);
	    /* %%: debug code was removed. */
	    return -EAGAIN;
	}

	set_task_state(task, TASK_INTERRUPTIBLE);

	su_task_blocks_on_lock(&waiter, task, lock);

	/* %%: debug code was removed. */

	/* we don't need to touch the lock struct anymore */
	spin_unlock(&lock->wait_lock);

	might_sleep();

	nosched_flag = current->flags & PF_NOSCHED;

	/* %%: debug code was removed. */

	current->flags &= ~PF_NOSCHED;

        pend_info.tasklet_done = 0;
        if (timeout != LONG_MAX)
        {
            pend_info.pid = current->pid;
            rtc_sw_task_init(&local_timer.ltimer, suapi_tasklet_handler_fn, (unsigned long)&pend_info);
            rtc_sw_task_schedule(timeout, &local_timer.ltimer);
        }

	ret = 0;
	/* wait to be given the lock */
	for (;;) {
            if (signal_pending(current)) {
                /*
                 * Remove ourselves from the wait list if we
                 * didnt get the lock - else return success:
                 */

                /* %%: debug and pi manipulation code was removed. */

                spin_lock(&lock->wait_lock);
                if (waiter.task) {
                    list_del_init(&waiter.list);
                    ret = -EINTR;
                }
                spin_unlock(&lock->wait_lock);
                break;
            }

            if (!waiter.task)
                break;

            schedule();
            set_task_state(task, TASK_INTERRUPTIBLE);

            if (timeout != LONG_MAX) {
                if (pend_info.tasklet_done) {
                    /*
                     * Remove ourselves from the wait list if we
                     * didnt get the lock - else return success:
                     */

                    /* %%: debug and pi manipulation code was removed. */

                    spin_lock(&lock->wait_lock);
                    if (waiter.task) {
                        list_del_init(&waiter.list);
                        ret = -ETIMEDOUT;
                    }
                    spin_unlock(&lock->wait_lock);
                    break;
                }
            }

	}

        set_task_state(task, TASK_RUNNING);
	current->flags |= nosched_flag;

        if (timeout != LONG_MAX)
        {
            if (!pend_info.tasklet_done) {
                rtc_sw_task_kill(&local_timer.ltimer);
            }
        }
	return ret;
}
#else
static int __sched __su_down_interruptible_timeout(struct su_rt_mutex *lock,
                                        signed long timeout)
{
        struct task_struct *task = current;
        unsigned long nosched_flag;
        struct su_rt_mutex_waiter waiter;
        int ret;

        /* %%: debug code was removed. */

        spin_lock(&lock->wait_lock);

        su_init_lists(lock);

        if (!lock->owner) {
                /* granted */
                /* %%: debug and pi manipulation  code was removed. */
                su_set_new_owner(lock, NULL, task);
                spin_unlock(&lock->wait_lock);

                return 0;
        }

        if (timeout <= 0) {
            spin_unlock(&lock->wait_lock);
            /* %%: debug code was removed. */
            return -EAGAIN;
        }

        set_task_state(task, TASK_INTERRUPTIBLE);

        su_task_blocks_on_lock(&waiter, task, lock);

        /* %%: debug code was removed. */

        /* we don't need to touch the lock struct anymore */
        spin_unlock(&lock->wait_lock);

        might_sleep();

        nosched_flag = current->flags & PF_NOSCHED;

        /* %%: debug code was removed. */

        current->flags &= ~PF_NOSCHED;

        ret = 0;
        /* wait to be given the lock */
        for (;;) {
                if (signal_pending(current)) {
                        /*
                         * Remove ourselves from the wait list if we
                         * didnt get the lock - else return success:
                         */

                        /* %%: debug and pi manipulation code was removed. */

                        spin_lock(&lock->wait_lock);
                        if (waiter.task) {
                                list_del_init(&waiter.list);
                                ret = -EINTR;
                        }
                        spin_unlock(&lock->wait_lock);
                        break;
                }
                if (!waiter.task)
                        break;

                timeout = schedule_timeout(timeout);

                if (timeout <= 0) {
                        /*
                         * Remove ourselves from the wait list if we
                         * didnt get the lock - else return success:
                         */

                        /* %%: debug and pi manipulation code was removed. */

                        spin_lock(&lock->wait_lock);
                        if (waiter.task) {
                                list_del_init(&waiter.list);
                                ret = -ETIMEDOUT;
                        }
                        spin_unlock(&lock->wait_lock);
                        break;
                }

                set_task_state(task, TASK_INTERRUPTIBLE);
        }

        task->state = TASK_RUNNING;
        current->flags |= nosched_flag;

        return ret;
}
#endif

/*
 * release the lock:
 */
static void __su_up_mutex(struct su_rt_mutex *lock, int save_state)
{
	struct task_struct *old_owner, *new_owner;

	spin_lock(&lock->wait_lock);

	/* %%: debug and pi manipulation code was removed. */

	old_owner = lock->owner;
	lock->owner = NULL;
	new_owner = NULL;
	if (!list_empty(&lock->wait_list))
		new_owner = su_pick_new_owner(lock, old_owner, save_state);

	/* %%: manipulation of priority inheritance list was removed */

	if (new_owner) {
		if (save_state)
			wake_up_process_mutex(new_owner);
		else
			wake_up_process(new_owner);
	}

	spin_unlock(&lock->wait_lock);
	/* no need to check for preempt here - we just handled it */
}

void fastcall __su_sema_init(struct su_semaphore *sem, int val, int debug,
			  char *name)
{
	atomic_set(&sem->count, val);
	switch (val) {
	case 0:
		__su_init_rt_mutex(&sem->lock, 0, debug, name);
		/* The semaphore was just created, so we should be able to
		 * acquire it now with no contention.
		 */
		__su_down_interruptible_timeout(&sem->lock, 0);
		break;
	default:
		__su_init_rt_mutex(&sem->lock, 0, debug, name);
		break;
	}
}

/*
 * In the down() variants we use the mutex as the semaphore blocking
 * object: we always acquire it, decrease the counter and keep the lock
 * locked if we did the 1->0 transition. The next down() will then block.
 *
 * In the up() path we atomically increase the counter and do the
 * unlock if we were the one doing the 0->1 transition.
 */

static inline void __su_down_complete(struct su_semaphore *sem)
{
	int count = atomic_dec_return(&sem->count);

	if (count > 0)
		__su_up_mutex(&sem->lock, 0);
}

int fastcall su_down_interruptible_timeout(struct su_semaphore *sem, signed long timeout)
{
	int ret;

	/* %%: debug code was removed. */

	ret = __su_down_interruptible_timeout(&sem->lock, timeout);
	if (ret)
		return ret;
	__su_down_complete(sem);
	return 0;
}

void fastcall su_up(struct su_semaphore *sem)
{
	int count;

	/* %%: debug code was removed. */

	/*
	 * Disable preemption to make sure a highprio trylock-er cannot
	 * preempt us here and get into an infinite loop:
	 */
	preempt_disable();
	count = atomic_inc_return(&sem->count);
	/*
	 * If we did the 0 -> 1 transition then we are the ones to unlock it:
	 */
	if (count == 1)
		__su_up_mutex(&sem->lock, 0);
	preempt_enable();
}

int fastcall su_sema_count(struct su_semaphore *sem)
{
	/* %%: debug code was removed. */

	return atomic_read(&sem->count);
}
