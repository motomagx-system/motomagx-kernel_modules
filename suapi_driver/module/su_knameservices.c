// Copyright (C) 2006-2008 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI name services within the SUAPI kernel
 *      module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.
// 2007-04-10 Motorola       Fix off by one error in kmalloc in registername.
// 2008-01-09 Motorola       Stop timer handling during panic.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>
#include <linux/suapi_module.h>
#include <linux/su_ktimers.h>
#include <linux/mpm.h>

/* For conversion of msecs to jiffies, the assumption in this module is that
 * LONG_MAX == MAX_SCHEDULE_TIMEOUT and that the ratio of msecs to jiffies is
 * greater than or equal to 1.
 */

/************** LOCAL CONSTANTS **********************************************/
#define SUMAXNAMELEN 256 /* includes terminating NULL */

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

typedef struct suNamesNode
{
    struct hlist_node hlnode;
    const char *sname;
    UINT32 value;
} suNamesNode;

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static unsigned int su_NameHash(const char *);
static int suapi_name_args(int,suNameInfo *,char *);
static void suapi_names_tasklet_handler_fn(unsigned long __data);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/
/* Name services spin lock.
 * Interrupts are expected to remain enabled when locking this spinlock.
 */
static spinlock_t su_names_lock = SPIN_LOCK_UNLOCKED;

/* Increments each time name is registered. */
static int name_services_modification_count = 0;

static DECLARE_WAIT_QUEUE_HEAD(nameserviceswq);

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
/* Name services hash list of names and values. */
struct hlist_head *suNames = NULL;

/************** GLOBAL EXPORT DEFINITIONS ************************************/
EXPORT_SYMBOL(suapi_findname);

/************** FUNCTION DEFINITIONS *****************************************/
static unsigned int su_NameHash(const char *sname)
{
    unsigned int rvalue;
    unsigned int i;

    /* The first two letters are likely p/. May need to rewrite this
       in assembly or use jhash?. */
    for (rvalue = 0, i = 0; sname[i]; i++)
       rvalue ^= sname[i] << (i % 28);

    return rvalue % SU_NAME_HASHTABLE_SIZE;
}

static int suapi_name_args(int arg, suNameInfo *ninfo, char *sname)
{
    long namelen;
    
    if (NULL == suNames)
        return -ENOSYS;

    if (copy_from_user(ninfo,
                       (struct suNameInfo *) arg,
                       sizeof(struct suNameInfo)))
        return -EFAULT;

    namelen = strlen_user(ninfo->name);
    if (!namelen)
        return -EFAULT;

    if (namelen >= SUMAXNAMELEN)
        return -ENAMETOOLONG;

    if (copy_from_user(sname, ninfo->name, namelen+1) != 0)
        return -EFAULT;
    return 0;
}

/*
 * DESCRIPTION: suapi_findname_ioctl
 *     This function uses SUAPI name services to search for the given string.
 *
 * INPUTS:
 *     filp is the pointer to the /dev/suapi file struct.
 *     arg is a pointer to the suNameInfo struct containing arguments and the
 *     return value.
 *
 * OUTPUTS:
 *     0 on success, negative for an error code.
 *
 * IMPORTANT NOTES:
 *     The result of the find name operation is stored in the "value" field of
 *     suNameInfo struct that is copied back to user space.
 *     This function should only be called by the /dev/suapi ioctl handler.
 */
int suapi_findname_ioctl(struct file *filp, int arg)
{
    suNameInfo ninfo;
    char sname[SUMAXNAMELEN];
    int err;
    UINT32 value;

    err = suapi_name_args(arg,&ninfo,sname);
    
    if (err)
        return err;

    value = suapi_findname(sname, ninfo.timeout, &err);

    if (err == 0)
    {
        ninfo.value = value;
        if (copy_to_user((struct suNameInfo *) arg,
                         &ninfo,
                         sizeof(struct suNameInfo)))
            return - EFAULT;
    }
    
    return err;
}

/*
 * DESCRIPTION: suapi_findname
 *     This function uses SUAPI name services to search for the given string.
 *
 * INPUTS:
 *     sname is the pointer to the string to search for.
 *     timeout is the maximum time to wait for the name in milliseconds.
 *     err is a pointer to a integer to hold the error status upon return.
 *
 * OUTPUTS:
 *     0 if an error occurred, otherwise the result of the find name is returned.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
UINT32 suapi_findname(const char* sname, long timeout, int* err)
{
    struct hlist_node *t; /* temporary variable for
                             hlist_for_each_entry_rcu() */
    struct suNamesNode *i; /* loop variable for hlist_for_each_entry_rcu() */
    int hash;
    int last_checked;
    UINT32 value;
    Suapi_Kernel_Timer local_timer;
    Suapi_Tasklet_Handler_Data pend_info;

    hash = su_NameHash(sname);

    if (0 == timeout)
    {
        rcu_read_lock();
        hlist_for_each_entry_rcu(i,t,&(suNames[hash]),hlnode)
            if (0==strcmp(i->sname,sname))
            {
                value = i->value;
                rcu_read_unlock();
                *err = 0;
                return value;
            }
        rcu_read_unlock();
        *err = -ENOENT;
        return 0;
    }

    if (0 > timeout)
    {
        *err = -ERANGE;
        return 0;
    }

    pend_info.tasklet_done = 0;
    if (LONG_MAX != timeout)
    {
        pend_info.pid = current->pid;
        rtc_sw_task_init(&local_timer.ltimer, suapi_names_tasklet_handler_fn, (unsigned long) &pend_info);
        rtc_sw_task_schedule(timeout, &local_timer.ltimer);
    }

    /* wait and loop */
    do
    {
        last_checked = name_services_modification_count;

        rcu_read_lock();
        hlist_for_each_entry_rcu(i,t,&(suNames[hash]),hlnode)
        if (0==strcmp(i->sname,sname))
        {
            value = i->value;
            rcu_read_unlock();
            *err = 0;
            if (LONG_MAX != timeout)
            {
                if (!pend_info.tasklet_done) 
                    rtc_sw_task_kill(&local_timer.ltimer); 
            }
            return value;
        }
        rcu_read_unlock();

        *err = wait_event_interruptible(nameserviceswq, ((last_checked != name_services_modification_count) || pend_info.tasklet_done));

        if (pend_info.tasklet_done)
        {
            *err = -ETIMEDOUT;
            break;
        }
    }  while (0 == *err);

    /* Following case is required in case of not timeout but 
     * wait_event_interruptible returning err
     */
    if (LONG_MAX != timeout)
    {
        if (!pend_info.tasklet_done)
            rtc_sw_task_kill(&local_timer.ltimer);
    }
    return 0;
}
#else
UINT32 suapi_findname(const char* sname, long timeout, int* err)
{
    struct hlist_node *t; /* temporary variable for
                             hlist_for_each_entry_rcu() */
    struct suNamesNode *i; /* loop variable for hlist_for_each_entry_rcu() */
    long expires = 0;
    int hash;
    int last_checked;
    UINT32 value;

    hash = su_NameHash(sname);

    if (0 == timeout)
    {
        rcu_read_lock();
        hlist_for_each_entry_rcu(i,t,&(suNames[hash]),hlnode)
            if (0==strcmp(i->sname,sname))
            {
                value = i->value;
                rcu_read_unlock();
                *err = 0;
                return value;
            }
        rcu_read_unlock();
        *err = -ENOENT;
        return 0;
    }

    if (LONG_MAX != timeout)
    {
        if (0 > timeout)
        {
            *err = -ERANGE;
            return 0;
        }

        /* HZ must be 1 or more, so # jiffies will be less than
         * # milliseconds. Since our milliseconds are less than
         * LONG_MAX due to type, # jiffies will be less than
         * LONG_MAX. So it's safe to convert to jiffies.
         */

        expires = msecs_to_jiffies(timeout);
        if (expires >= MAX_SCHEDULE_TIMEOUT)
        {
            *err = -ERANGE;
            return 0;
        }
    }

    /* wait and loop */
    do
    {
        last_checked = name_services_modification_count;

        rcu_read_lock();
        hlist_for_each_entry_rcu(i,t,&(suNames[hash]),hlnode)
            if (0==strcmp(i->sname,sname))
            {
                value = i->value;
                rcu_read_unlock();
                *err = 0;
                return value;
            }
        rcu_read_unlock();

        if (LONG_MAX != timeout)
        {
            expires = wait_event_interruptible_timeout(nameserviceswq, last_checked != name_services_modification_count, expires);
            if (expires < 0)
                *err = expires;
            else if (expires == 0)
                *err = -ETIMEDOUT;
        }
        else
            *err = wait_event_interruptible(nameserviceswq, last_checked != name_services_modification_count);
    }  while (0 == *err);

    return 0;
}
#endif

/*
 * Do not call this function from ISRs.
 */
int suapi_registername(struct file *filp, int arg)
{
    struct suNameInfo ninfo;
    struct hlist_node *t; /* temporary variable for
                             hlist_for_each_entry_rcu() */
    suNamesNode *i; /* loop variable for hlist_for_each_entry_rcu() */
    char sname[SUMAXNAMELEN];

    suNamesNode *newnode;
    int hash, err;

    err = suapi_name_args(arg,&ninfo,sname);

    if (err)
        return err;

    hash = su_NameHash(sname);

    newnode = kmalloc(sizeof(struct suNamesNode),GFP_KERNEL);
    if (! newnode)
    {
        return -ENOMEM;
    }

    /* add 1 to length to account for null terminating character */
    newnode->sname = kmalloc(strlen(sname)+1, GFP_KERNEL);
    if (! newnode->sname)
    {
        kfree(newnode);
        return -ENOMEM;
    }
    strcpy((char *) (newnode->sname),sname);
    newnode->value = ninfo.value;
    spin_lock(&su_names_lock);

    /*
     * Don't need rcu_read_lock() or *_rcu as protected by
     * su_names_lock from modification
     */

    hlist_for_each_entry(i,t,&(suNames[hash]),hlnode)
        if (0==strcmp(i->sname,sname))
        {
            /* found a duplicate */
            spin_unlock(&su_names_lock);

            kfree(newnode->sname);
            kfree(newnode);
            return -EEXIST;
        }
            
    hlist_add_head_rcu(&(newnode->hlnode), &(suNames[hash]));
    /*
     * The order here is important, as this count tells waiters that
     * they need to recheck the list. We may have added something in
     * the middle of their check before they waited.
     */
    name_services_modification_count++;
    spin_unlock(&su_names_lock);

    wake_up(&nameserviceswq);

    return 0;
}

/*
 * Do not call this function from ISRs.
 */
int suapi_unregistername(struct file *filp, int arg)
{
    struct suNameInfo ninfo;
    struct hlist_node *t; /* temporary variable for
                             hlist_for_each_entry() */
    suNamesNode *i; /* loop variable for hlist_for_each_entry() */
    char sname[SUMAXNAMELEN];
    int hash, err;

    err = suapi_name_args(arg,&ninfo,sname);
    if (err)
        return err;

    ninfo.value = 0;

    hash = su_NameHash(sname);

    /*
     * Pre-verify that we can write so that don't return error when
     * actually unregistered the name. Don't copy to/from user when
     * holding a spinlock.
     */

    if (copy_to_user((struct suNameInfo *) arg,
                     &ninfo,
                     sizeof(struct suNameInfo)))
        return - EFAULT;

    spin_lock(&su_names_lock);

    /*
     * Do not use the _rcu iterator here, as su_names_lock protects
     * from modification by other functions.
     */

    hlist_for_each_entry(i,t,&(suNames[hash]),hlnode)
        if (0==strcmp(i->sname,sname))
        {
            ninfo.value = i->value;
            hlist_del_rcu(&(i->hlnode));
            /*
             * The above removed the element from the linked list. On
             * SMP we must assume that another task may have gotten
             * the entry just prior and we must 'wait' for them to
             * finish the compare (and possible reading of the value)
             * before deleting the string. We know this when all
             * rcu_read_locks() have been unlocked.
             *
             * We can go ahead and unlock su_names_lock as any new
             * holders of that lock will never see this node.
             */
            spin_unlock(&su_names_lock);
            synchronize_kernel();

            kfree(i->sname);
            kfree(i);
            if (copy_to_user((struct suNameInfo *) arg,
                             &ninfo,
                             sizeof(struct suNameInfo)))
                return - EFAULT;
            return 0;
        }

    spin_unlock(&su_names_lock);
    return -ENOENT;
}

/*
 * Assumes that no other simultaneous uses of suNames are possible as
 * SUAPI device is closed in order to unload.
 */

void suapi_clean_names(void)
{
    struct hlist_head *myNames;
    struct hlist_node *t1,*t2; /* temporary variables for
                                  hlist_for_each_entry_safe() */
    suNamesNode *i; /* loop variable for hlist_for_each_entry_safe() */
    int hash;

    spin_lock(&su_names_lock);

    /* The su_names_lock does NOT prevent others from concurrently
     * walking the list, just can't add or delete from list.
     * No matter, no other SUAPI service can be in progress anyway
     * and readers don't lock the entire time they use suNames so
     * setting NULL would cause a problem if they could be in
     * progress.
     */
    myNames = suNames;
    suNames = NULL;

    spin_unlock(&su_names_lock);

    if (NULL == myNames)
        return;

    for (hash = 0; hash < SU_NAME_HASHTABLE_SIZE; hash++)
    {
        /* Do not use the _rcu iterator here, since the list is no longer
         * visible and su_names_lock protects from the other deletion
         * routine when list made invisible. Must use *_safe as continuing
         * through loop after deleting the current entry. Must use
         * hlist_del_rcu() as others may still be walking the now hidden
         * list for reading. (Not really as device is closed, but not
         * harmful to use it.) 
         */

        hlist_for_each_entry_safe(i,t1,t2,&(myNames[hash]),hlnode)
        {
            hlist_del_rcu(&(i->hlnode));
            /*
             * The above removed the element from the linked list. On
             * SMP we must assume that another task may have gotten
             * the entry just prior and we must 'wait' for them to
             * finish the compare (and possible reading of the value)
             * before deleting the string. We know this when all
             * rcu_read_locks() have been unlocked.
             *
             * Not really necessary as SUAPI device is closed.
             */
            synchronize_kernel();
            kfree(i->sname);
            kfree(i);
        }
    }

    /* The myNames array is not freed here. It is part of a larger memory
     * region (pointed to by suapi_private_data_ptr) that is freed in
     * suapi_clean_names().
     */
}

/*
 * DESCRIPTION: suapi_names_tasklet_handler_fn
 *     This function is a tasklet handler routine which get scheduled
 *     on expiry of RTC timer started by findname service.
 *
 * INPUTS:
 *     __data is the pointer to Suapi_Tasklet_Handler_Data
 *
 * OUTPUTS:
 *     0 on success, negative for an error code.
 *
 * IMPORTANT NOTES:
 *     None.
 */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
static void suapi_names_tasklet_handler_fn(unsigned long __data)
{
    Suapi_Tasklet_Handler_Data *pend_info = (Suapi_Tasklet_Handler_Data *) __data;
    struct task_struct *t;

    /*
     * If a panic occurred then stop handling timers so we
     * can get a core dump that is consistent with the time of panic.
     */
    if (su_previous_panic != 0)
    {
        return;
    }

    t = find_task_by_pid(pend_info->pid);
    if (t)
    {
        pend_info->tasklet_done = 1;
#if defined(CONFIG_MOT_FEAT_PM)
        mpm_handle_ioi();
#endif
        wake_up(&nameserviceswq);
    }
    return;
}
#endif

