// Copyright (C) 2006, 2007 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements miscellaneous utilities within the SUAPI kernel
 *      module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>

#include <linux/suapi_module.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/
/*--------------------------------------------------------------------------*/
/*               String and File Manipulation Functions                     */
/*--------------------------------------------------------------------------*/

char * __init suapi_fgets(char *s, int size, struct file *input)
{
    int count;
    char * nl;

    /* This assumes that f_pos starts at 0 on a newly opened file.
     * kernel_read() does not update f_pos, which is why it must be done
     * manually below.
     *
     * The fgets() library interface (which this function is mimicing) reads
     * until it either finds a newline or until it reaches "size" bytes read.
     * suapi_fgets() will always read "size" bytes and then adjust f_pos to
     * be either the position after the newline or the position after the
     * last byte read. This causes kernel_read to usually read more than
     * necessary, but it avoids having to read one byte at a time from the
     * input file.
     */
    count = kernel_read(input, input->f_pos, s, size-1);

    if (count <= 0 || count >= size)
        return NULL;

    s[count] = '\0';

    nl = strchr(s,'\n');

    /* If newline exists in string, terminate after newline. */
    if (nl)
    {
        *(nl + 1) = '\0';
        input->f_pos += ((nl + 1) - s);
    }
    else /* else no newline in string, so put NULL at end. */
    {
        input->f_pos += count;
    }

    return s;
}

char * __init suapi_strtok(char *s1, const char *s2)
{
    /* Initialize save_s1 to NULL the first time this function is called. */
    static char *save_s1 = NULL;
    char * tmp;

    if (s1 != NULL)
        save_s1 = s1;

    /* strsep() will update save_s1 to point to the character after the
     * separator so that save_s1 is ready for the next time this function is
     * called with s1 == NULL.
     *
     * strsep() does not work exactly like strtok(). strtok() will skip all
     * items in s2 until it finds something not in s2, then it updates
     * save_s1. However, strsep() returns when it finds the first thing in s2.
     * Therefore, strsep() is called in a loop until it finds the first non-s2
     * character or until it reaches the end of the string.
     */
    do
    {
        tmp = strsep(&save_s1, s2);
    } while ((tmp != NULL) && (strlen(tmp) == 0));

    return tmp;
}
