// Copyright (C) 2006, 2008 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI timer services within the SUAPI kernel
 *      module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.
// 2008-01-09 Motorola       Stop timer handling during panic.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>
#include <linux/suapi_module.h>
#include <linux/su_ktimers.h>
#include <linux/mpm.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static Suapi_Kernel_Timer *lock_timer(int which);
static inline void unlock_timer(Suapi_Kernel_Timer *timr);
static int timer_allocated(Suapi_Kernel_Timer *, int);

static void suapi_timer_fn(unsigned long);
static void suapi_internal_timer_fn(Suapi_Kernel_Timer *);
static int suapi_internal_stop_timer(int which,
                                     Suapi_Kernel_Timer **timrout,
                                     int *timeleftout);
/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
Suapi_Kernel_Timer *timers = NULL;

atomic_t timer_count;

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/
static Suapi_Kernel_Timer *lock_timer(int which)
{
    Suapi_Kernel_Timer *timr;
    int tindex = which & 0xFFFF;

    if (tindex >= SU_NTIMER)
        return NULL;

    timr = &timers[tindex];

    spin_lock_bh(&timr->sut_lock);

    return timr;
}

static inline void unlock_timer(Suapi_Kernel_Timer *timr)
{
    spin_unlock_bh(&timr->sut_lock);
}

/*
 * The timer_allocated() verifying the use counter matches.
 */
static int timer_allocated(Suapi_Kernel_Timer *timr, int which)
{
    if (timr->ktid != which)
        return -ENOENT;
    if (timr->ktstate == TFREE)
        return -ENOENT;
    return 0;
}

static void suapi_timer_fn(unsigned long __data)
{
    Suapi_Kernel_Timer *timr = (Suapi_Kernel_Timer *) __data;

    /*
     * If a panic occurred then stop handling timers so we 
     * can get a core dump that is consistent with the time of panic.
     */
    if (su_previous_panic != 0)
    {
        return;
    }

    spin_lock(&timr->sut_lock);
    suapi_internal_timer_fn(timr);
    spin_unlock(&timr->sut_lock);
}

/* Must be called with timer's lock held but soft IRQs enabled. */
static void suapi_internal_timer_fn(Suapi_Kernel_Timer *timr)
{
    void * dup_msg = NULL;
    void * kmsgp = timr->ktmessage_msg;
    u64 expires = 0;
    int ret = 0;

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    if (timr->ktstate & TCYCLIC)
        expires = timr->ktperiod;
#else
    if (timr->ktstate & TCYCLIC)
        expires = get_jiffies_64() + msecs_to_jiffies(timr->ktperiod);

#endif

    if (timr->ktstate & TMESSAGE)
    {
        if (timr->ktstate & TCYCLIC)
        {
            dup_msg = suapi_internal_recreate_message(kmsgp,
                                                    suGetMessageLength(kmsgp));
            /* Copy message data */
            if (!IS_ERR(dup_msg))
            {
                memcpy(dup_msg, kmsgp, suGetMessageLength(kmsgp));
                ret = suapi_internal_send_message_to_port(
                                          (SU_PORT_HANDLE) timr->ktmessage_port,
                                          dup_msg, SU_IN_ISR);
            }
            else if (!IS_ERR(dup_msg) != -ENOMEM)
                ret = PTR_ERR(dup_msg);
        }
        else
        {
            dup_msg = kmsgp;
            timr->ktmessage_msg = NULL;
            if (dup_msg)
                ret = suapi_internal_send_message_to_port(
                                          (SU_PORT_HANDLE) timr->ktmessage_port,
                                          dup_msg, SU_IN_ISR);
        }
    }
#ifdef CONFIG_SUAPI
    else if (timr->ktstate & TEVENT)
    {
        struct task_struct *t;

        read_lock(&tasklist_lock);

        t = find_task_by_pid(timr->ktevent_pid);

        if (t)
        {
            spin_lock(&su_events_lock);
            if ((t->su_alloc_flags & timr->ktevent_mask) == timr->ktevent_mask)
            {
                t->su_event_flags |= timr->ktevent_mask;

                if ((t->su_state == SUAPI_TASK_STATE_WAIT_ALL_EVENTS) &&
                    (t->su_wait_flags == (t->su_event_flags & t->su_wait_flags)))
                {
#if defined(CONFIG_MOT_FEAT_PM)
                    mpm_handle_ioi();
#endif
                    wake_up_process(t);
                }
                else if ((t->su_state == SUAPI_TASK_STATE_WAIT_ANY_EVENT) &&
                         (t->su_event_flags & t->su_wait_flags))
                {
#if defined(CONFIG_MOT_FEAT_PM)
                    mpm_handle_ioi();
#endif
                    wake_up_process(t);
                }
            }
            spin_unlock(&su_events_lock);
        }
        read_unlock(&tasklist_lock);
    }
#endif

    if (TCYCLIC & timr->ktstate && ret == 0)
    {
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
        rtc_sw_task_schedule(expires, &timr->ltimer);
#else
        mod_timer(&timr->ltimer,(unsigned long) expires);
#endif
    }
    else /* If not cyclic or if an error occurred, turn off everything except
          * the TSTOPPED flag.
          */
        timr->ktstate = TSTOPPED;
}

static int suapi_internal_stop_timer(int which,
                                     Suapi_Kernel_Timer **timrout,
                                     int *timeleftout)
{
    int result;
    unsigned int timeleft;
    Suapi_Kernel_Timer *timr;
#if !defined (CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    unsigned long local_jiffies;
#endif

    if (NULL == (timr = lock_timer(which)))
        return -EINVAL;

    /* Soft interrupts disabled */

    if ((result = timer_allocated(timr,which)))
    {
        unlock_timer(timr);
        return result;
    }

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    timeleft = rtc_sw_task_stop(&timr->ltimer);

    /* Since this timer is now off of the RTC stopwatch timers list,
     * there is no need to keep soft interrupts disabled.
     */
#else
    /* Save jiffies before removing timer from timer list so jiffies don't go
     * negative and cause an overflow later.
     */
    local_jiffies = jiffies;
    del_timer(&timr->ltimer); /* explicitly OK to delete timer even if
                               * not on list. */

    /* Since this timer is now off of the kernel timers list, there is no need
     * to keep soft interrupts disabled.
     */
#endif
    local_bh_enable();

    if (TSTARTED & timr->ktstate)
    {
        if (timeleftout)
        {
#if !defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
            timeleft = jiffies_to_msecs(timr->ltimer.expires - local_jiffies);
#endif
            if (0 == timeleft)
                timeleft = 1; /* special case */

            *timeleftout = timeleft;
        }

        if (TMESSAGE & timr->ktstate)
        {
            /* Delete the message if it was not yet delivered. */
            if (timr->ktmessage_msg != NULL)
            {
                suapi_internal_free_mem((void *)SU_MSG2HDR(timr->ktmessage_msg),
                                        &timr->sut_lock);
                timr->ktmessage_msg = NULL;
            }
        }
    }
    /* Timer must already be TSTOPPED. */
    else if (timeleftout)
    {
        *timeleftout = 0;
    }

    timr->ktstate = (timr->ktstate & TCYCLIC) | TSTOPPED;
    *timrout = timr;

    /* Returning with timer lock held. */
    return 0;
}

int suapi_create_timer_ioctl(unsigned long arg)
{
    int which, full_id, result;
    Suapi_Kernel_Timer *new_timer;
    int iscyclical;

    result = get_user(iscyclical, (int *) arg);
    if (result)
        return result;

    /* Attempt to copy data to output address to ensure it is writable. */
    result = put_user(iscyclical, (int *) arg);
    if (result)
        return result;

    while (atomic_sub_return(1,&timer_count) >= 0)
    {
        for (which = 0; which < SU_NTIMER;which++)
            if (timers[which].ktstate == TFREE)
            {
                new_timer = lock_timer(which);
                if (new_timer->ktstate == TFREE)
                {
                    if (iscyclical)
                        new_timer->ktstate = TSTOPPED | TCYCLIC;
                    else
                        new_timer->ktstate = TSTOPPED;

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
                    rtc_sw_task_init(&new_timer->ltimer, suapi_timer_fn, (unsigned long)new_timer);
#else
                    init_timer(&new_timer->ltimer);
                    new_timer->ltimer.expires = 0;
                    new_timer->ltimer.data = (unsigned long) new_timer;
                    new_timer->ltimer.function = suapi_timer_fn;
#endif
                    full_id = (new_timer->ktid & 0x7FFF0000) | which ;
                    new_timer->ktid = full_id;
                    unlock_timer(new_timer);

                    /* Assumes output address is still writable. */
                    result = put_user(full_id, (int *) arg);
                    return result;
                }
                /* lost a race for allocation */
                unlock_timer(new_timer);
            }
        /* Looked through the whole list. Try again. */
        atomic_add(1, &timer_count);
    }
    /* None left, so fail. */
    atomic_add(1, &timer_count);
    return -ENOSPC;
}

int suapi_delete_timer_ioctl(unsigned long arg)
{
    int which, result;
    Suapi_Kernel_Timer *timr;

    result = get_user(which, (int *) arg);
    if (result)
        return result;

    result = suapi_internal_stop_timer(which,
                                       &timr,
                                       NULL);
    if (result)
        return result;

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    rtc_sw_task_kill(&timr->ltimer);
#endif
    /* timer locked, soft interrupts enabled */

    timr->ktstate = TFREE;
    timr->ktid += 1 << 16; /* update usage count for next
                               * time, allocation will clear MSBit */
    spin_unlock(&timr->sut_lock);

    atomic_add(1, &timer_count);

    return result;
}

int suapi_start_timer_ioctl(unsigned long arg)
{
    int result = 0;
    struct suTimer  start_timer_info;
    Suapi_Kernel_Timer *timr;
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    u32 expires;
#else
    u64 expires;
#endif
    void * kmsg_p = NULL;

#if !defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    expires = get_jiffies_64();
#endif

    if (copy_from_user(&start_timer_info,
                       (struct suTimer *) arg,
                       sizeof(struct suTimer)))
        return -EFAULT;

    /*
     * Protect against rogue callers. The SUAPI service will set one
     * or the other of TMESSAGE and TEVENT.
     */

    if ((start_timer_info.state != TMESSAGE) && (start_timer_info.state != TEVENT))
        return -EINVAL;

    if (start_timer_info.state == TMESSAGE)
    {
        kmsg_p = SU_USER_TO_KERNEL(start_timer_info.stmessage_msg);
        /* Messages must be within the SUAPI memory partition pool. */
        if (! SU_IN_PARTITION_RANGE(SU_MSG2HDR(kmsg_p)) )
            return -EBADMSG;
        /* No support for sending a message to a process from a timer */
        if (SU_ISPHPID(start_timer_info.stmessage_port))
            return -EPROTO;
        /* Ensure port handle is in range of port table. */
        if (SU_PHOUTOFRANGE(start_timer_info.stmessage_port))
            return -ENOENT;
    }

    if (start_timer_info.period < 0)
        start_timer_info.period = 0;

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    expires = start_timer_info.period;
#else
    /* jiffies resolution >= 1 ms. */
    expires += msecs_to_jiffies(start_timer_info.period);
#endif

#ifdef CONFIG_SUAPI
    if (start_timer_info.state == TEVENT)
    {
        struct task_struct *t;

        /* do sanity test on target pid */
        read_lock(&tasklist_lock);

        if (start_timer_info.stevent_pid == SU_SELF_PID)
        {
            t = current;
            start_timer_info.stevent_pid = current->pid;
        }
        else
            t = find_task_by_pid(start_timer_info.stevent_pid);

        if (t)
        {
            spin_lock_bh(&su_events_lock);
            if ((t->su_alloc_flags & start_timer_info.stevent_mask)
                != start_timer_info.stevent_mask)
            {
                spin_unlock_bh(&su_events_lock);
                read_unlock(&tasklist_lock);
                return -EINVAL;
            }
            spin_unlock_bh(&su_events_lock);
        }
        else
        {
            read_unlock(&tasklist_lock);
            return -ESRCH;
        }
        read_unlock(&tasklist_lock);
        /*
         * Target task could disappear, so same check will occur when
         * timer expires.
         */
    }
#endif
    if (NULL == (timr = lock_timer(start_timer_info.full_id)))
        return -EINVAL;

    /* Soft interrupts disabled, timer locked */

    if ((result = timer_allocated(timr,start_timer_info.full_id)))
    {
        unlock_timer(timr);
        return result;
    }

    if ((timr->ktstate & TCYCLIC) &&
        (start_timer_info.period <= 0))
    {
        unlock_timer(timr);
        return -ERANGE;
    }

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    if (TSTARTED & timr->ktstate)
    {
        rtc_sw_task_stop(&timr->ltimer);
    }
#else
    del_timer(&timr->ltimer); /* explicitly OK to delete timer even if
                               * not on list. */
#endif

    local_bh_enable();

    if ((TSTARTED & timr->ktstate) && (TMESSAGE & timr->ktstate))
    {
        /* Delete the message if it was not yet delivered. */
        if (timr->ktmessage_msg != NULL)
        {
            suapi_internal_free_mem((void *)SU_MSG2HDR(timr->ktmessage_msg),
                                    &timr->sut_lock);
            timr->ktmessage_msg = NULL;
        }
    }

    timr->ktperiod = start_timer_info.period;
    timr->ktslip = start_timer_info.slip;

    if (start_timer_info.state & TMESSAGE)
    {
        timr->ktmessage_msg = kmsg_p;
        timr->ktmessage_port = start_timer_info.stmessage_port;
        timr->ktstate = (timr->ktstate & TCYCLIC) | TMESSAGE | TSTARTED;
    }
    else /* if (start_timer_info.state & TEVENT) */
    {
#ifdef CONFIG_SUAPI
        timr->ktevent_mask = start_timer_info.stevent_mask;
        timr->ktevent_pid = start_timer_info.stevent_pid;
        timr->ktstate = (timr->ktstate & TCYCLIC) | TEVENT | TSTARTED;
#else
        spin_unlock(&timr->sut_lock);
        return -ENOSYS;
#endif
    }

    /* XXX - add high res support */

    /* Expire the timer if requested timeout period is 0. The expiration must
     * happen immediately as required by SUAPI. A RTC timer with expiration
     * <= 0 will not expire until the next request update.
     */
    if (start_timer_info.period <= 0)
    {
        if (start_timer_info.state & TMESSAGE)
        {
            /* Since period is <= 0, this cannot be a cyclic timer. */
            timr->ktstate = TSTOPPED;
            timr->ktmessage_msg = NULL;
            spin_unlock(&timr->sut_lock);
            /* Send message after unlocking timer in case sendmessage panics. */
            result = suapi_internal_send_message_to_port(
                               (SU_PORT_HANDLE) start_timer_info.stmessage_port,
                               kmsg_p, SU_NOT_IN_ISR);
        }
        else
        {
            suapi_internal_timer_fn(timr);
            spin_unlock(&timr->sut_lock);
        }
    }
    else
    {
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
        rtc_sw_task_schedule(expires, &timr->ltimer);
#else
        mod_timer(&timr->ltimer,(unsigned long) expires);
#endif
        spin_unlock(&timr->sut_lock);
    }


    return result;
}

int suapi_stop_timer_ioctl(unsigned long arg)
{
    int result, timeleft;
    SuapiStopTimer stop_timer_info;
    Suapi_Kernel_Timer *timr;

    if (copy_from_user(&stop_timer_info,
                       (SuapiStopTimer *) arg,
                       sizeof(SuapiStopTimer)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (stop_timer_info.time_left)
        if ((result = put_user(-1, stop_timer_info.time_left)))
            return result;

    result = suapi_internal_stop_timer(stop_timer_info.tmhandle,
                                       &timr,
                                       &timeleft);

    /* nothing else to do for stopping a timer */
    if (0 == result)
        spin_unlock(&timr->sut_lock);

    /* Assumes output address is still writable. */
    if (stop_timer_info.time_left)
        result = put_user(timeleft,stop_timer_info.time_left);

    return result;
}

int suapi_time_left_ioctl(unsigned long arg)
{
    int which, result;
    Suapi_Kernel_Timer *timr;
    unsigned int timeleft;

    which = (int) arg;

    if (NULL == (timr = lock_timer(which)))
        return -EINVAL;

    if ((result = timer_allocated(timr,which)))
    {
        unlock_timer(timr);
        return result;
    }

    if (TSTARTED & timr->ktstate)
    {
        /* see note in suapi_internal_stop_timer for why timeleft must
         * be unsigned int
         */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
        timeleft = rtc_sw_task_time_left(&timr->ltimer);
#else
        timeleft = jiffies_to_msecs(timr->ltimer.expires - jiffies);
#endif

        if (0 == timeleft)
            timeleft = 1; /* special case */
    }
    else
        timeleft = 0;

    unlock_timer(timr);

    return timeleft;
}

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
int suapi_get_ms_ioctl(int arg)
{
    u64 value;
    value = rtc_sw_msfromboot();
    return(put_user(value,(u64 *) arg));
}
#else
int suapi_get_ms_ioctl(int arg)
{
    u64 value = get_jiffies_64() - INITIAL_JIFFIES;
#if HZ <= 1000 && !(1000 % HZ)
    value *=  (1000 / HZ);
#elif HZ > 1000 && !(HZ % 1000)
    value = (value + (HZ / 1000) - 1) / (HZ / 1000);
#else
    value *= 1000;
    do_div(value, HZ);
#endif
    return(put_user(value,(u64 *) arg));
}
#endif

int suapi_sleep_ioctl(unsigned long arg)
{
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
    int result = 0;
    Suapi_Kernel_Timer local_timer;
    Suapi_Tasklet_Handler_Data pend_info;
    unsigned int duration;
    unsigned long timeleft;

    result = get_user(duration, (unsigned int *) arg);
    if (result)
        return result;

    if (duration < 0)
        return -EINVAL;

    pend_info.pid = current->pid;
    pend_info.tasklet_done = 0;
    rtc_sw_task_init(&local_timer.ltimer, suapi_tasklet_handler_fn, (unsigned long)&pend_info);

    set_current_state(TASK_INTERRUPTIBLE);
    rtc_sw_task_schedule(duration, &local_timer.ltimer);

    for (;;)
    {
        schedule();
        set_current_state(TASK_INTERRUPTIBLE);
        if (signal_pending(current))
        {
            result = -EINTR;
            break;
        }
        if (pend_info.tasklet_done)
        {
            break;
        }
        /* Going into deep sleep. */
        if (unlikely(current->flags & PF_FREEZE))
            refrigerator(PF_FREEZE);
    }
    set_current_state(TASK_RUNNING);
    timeleft = rtc_sw_task_time_left(&local_timer.ltimer);
    if (!pend_info.tasklet_done)
        rtc_sw_task_kill(&local_timer.ltimer);
    return result;
#else
    return -ENOTTY;
#endif
}

/*
 * DESCRIPTION: suapi_tasklet_handler_fn
 *     This function is a tasklet handler routine which get scheduled
 *     on expiry of RTC timer started various SUAPI services that pend.
 *
 * INPUTS:
 *     __data is the pointer to Suapi_Tasklet_Handler_Data
 *
 * OUTPUTS:
 *     0 on success, negative for an error code.
 *
 * IMPORTANT NOTES:
 *     None.
 */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
void suapi_tasklet_handler_fn(unsigned long __data)
{
    Suapi_Tasklet_Handler_Data *pend_info = (Suapi_Tasklet_Handler_Data *) __data;
    struct task_struct *t;

    /*
     * If a panic occurred then stop handling timers so we 
     * can get a core dump that is consistent with the time of panic.
     */
    if (su_previous_panic != 0)
    {
        return;
    }

    t = find_task_by_pid(pend_info->pid);
    if (t)
    {
        pend_info->tasklet_done = 1;
#if defined(CONFIG_MOT_FEAT_PM)
        mpm_handle_ioi();
#endif
        wake_up_process(t);
    }
}
#endif

