/*****************************************************************************
 *  Acknowledgments: Portions of this code were modified from examples       *
 *  in "Linux Device Drivers, Third Edition" by Jonathan Corbet, Alessandro  *
 *  Rubini, and Greg Kroah-Hartman, published by O'Reilly & Associates.      *
 *  Permission to redistribute in source or binary form is expressly given.  *
 *****************************************************************************/

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI kernel module.
 */


// Copyright (C) 2006-2008 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-10-26 Motorola       Change order of unregister.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.
// 2006-11-16 Motorola       Make SUAPI kernel module build for x86.
// 2007-01-26 Motorola       Check for watchdog panics.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM
// 2008-01-09 Motorola       Don't allow other suapi calls after a panic.
// 2008-01-09 Motorola       Fix realtime process hang issue
// 2008-05-22 Motorola       Add parameter for SUSPEND with IRQ enabled/disabled
 
/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>

#define CONFIG_SUAPI_TEST
#include <linux/suapi_module.h>
#include <linux/su_ktimers.h>

#ifdef CONFIG_DEVFS_FS
/* XXX - seems DEVFS is buggy and going away? Replaced with udev? */

#include <linux/devfs_fs_kernel.h>
#else
#error("Need DEVFS or replacement");
#endif

/* For conversion of msecs to jiffies, the assumption in this module is that
 * LONG_MAX == MAX_SCHEDULE_TIMEOUT and that the ratio of msecs to jiffies is
 * greater than or equal to 1.
 */

#include <asm/bootinfo.h>
#include <linux/su_panic_codes.h>

#ifdef CONFIG_MOT_FEAT_PM
#include <linux/mpm.h>
#endif

/************** LOCAL CONSTANTS **********************************************/
#define MODNAME "suapi"
#define PRIVATE_DEVICE_NAME "suapipriv"

/*---- flags ----*/
/* External value of the low power flag group handle. Used for initialization.*/
#define SU_LOWPWR_FLAG_GRP_EXT 1
#define SU_FG_ALLOC      1    /* Used to indicate an allocated flag group. */

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
#ifdef CONFIG_DEVFS_FS
/* XXX - don't know why example has this type. */
typedef int devfs_handle_t;
#endif

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/*--- driver-required function prototypes ---*/
static int suapi_open(struct inode *inode, struct file *filp);
static int suapi_mmap(struct file *, struct vm_area_struct *);
static int suapi_private_mmap(struct file *, struct vm_area_struct *);
static int suapi_read(struct file *, char __user *, size_t, loff_t *);
static int suapi_private_read(struct file *, char __user *, size_t, loff_t *);
static int suapi_release(struct inode *inode, struct file *filp);
static long suapi_unlocked_ioctl(struct file *filp,
                                 unsigned int cmd,
                                 unsigned long arg);
static struct page *suapi_vma_nopage(struct vm_area_struct *,
                                     unsigned long,
                                     int *);
static struct page *suapi_private_vma_nopage(struct vm_area_struct *,
                                     unsigned long,
                                     int *);
static void suapi_device_release(struct device *dev);

/************** LOCAL MACROS *************************************************/

/* SU_SZ_TO_NUMPGS: rounds up requested size to page boundary. */
#define SU_SZ_TO_NUMPGS(s) (((s) + (PAGE_SIZE-1)) >> PAGE_SHIFT)

#define SU_ROUND_TO_DOUBLE_WORD(value) \
    ((value < sizeof(long long)) ? \
     sizeof(long long) : \
    (((value + (sizeof(long long) - 1))/sizeof(long long))*(sizeof(long long))) )

/*
 * This macro converts a token name and sets field to its value.
 * It is only useful in suapi_init_module as it relies on
 * sysconfig_st, token, and valuestr to be present.
 */
#define S(name,field) \
                if (!strcmp(token,#name)) \
                { \
                    *((unsigned long *) &(sysconfig_st.field)) = \
                        simple_strtoul(valuestr,NULL,0);\
                    continue; \
                }

/************** LOCAL VARIABLES **********************************************/
static const char *modname = MODNAME;
static const char *private_device_name = PRIVATE_DEVICE_NAME;

/* spin lock for general SUAPI data locking.
 * Interrupts are expected to remain enabled when locking this spinlock.
 */
spinlock_t su_lock = SPIN_LOCK_UNLOCKED;

static struct file_operations suapi_fops =
{
    .owner = THIS_MODULE,
    .open = suapi_open,
    .release = suapi_release,
    .read = suapi_read,
    .unlocked_ioctl = suapi_unlocked_ioctl,
    .mmap = suapi_mmap,
};

static struct file_operations suapi_private_fops =
{
    .owner = THIS_MODULE,
    .open = suapi_open,
    .release = suapi_release,
    .read = suapi_private_read,
    .mmap = suapi_private_mmap,
};

/* Module methods used during mmap memory allocation and access. */
struct vm_operations_struct suapi_vm_ops =
{
    .nopage =   suapi_vma_nopage,
};

/* Module methods used during mmap memory allocation and access. */
struct vm_operations_struct suapi_vm_private_ops =
{
    .nopage =   suapi_private_vma_nopage,
};

static struct device_driver suapi_driver =
{
    .name           = MODNAME,
    .bus            = &platform_bus_type,
    .probe          = suapi_probe,
    .suspend        = suapi_suspend,
    .resume         = suapi_resume,
};

static struct platform_device suapi_device =
{
    .name           = MODNAME,
    .id             = 0,
    .dev.driver     = &suapi_driver,
    .dev.release    = suapi_device_release,
};

static int suapi_major = -ENOSYS;
static int suapi_private_major = -ENOSYS;

/* Global variables used for accessing the SUAPI private and global shared
 * data segment.
 */
void * suapi_kernel_start_ptr = NULL;
void * suapi_private_start_ptr = NULL;
void * suapi_private_end_ptr = NULL;

static SU_PART_SIZE_INFO * su_part_sizes_ptr = NULL;

/* The default configuration file can be overridden by specifying
 * suapi_config_file when loading this module.
 */
static char *suapi_config_file = "/etc/suapi/suapi.su_sysconfig";
module_param(suapi_config_file, charp, S_IRUGO);

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
/* Kernel pointer to SUAPI shared global data structure. */
SU_GLOBAL_DATA * suapi_global_data_ptr;

SU_PRIVATE_DATA * suapi_private_data_ptr = NULL;

/* Pointer to SUAPI configuration data. */
SU_SYSCONFIG * suapi_sysconfig_ptr;

/* This variable contains the value of the user space address of the shared
 * memory segment. This value is used in calculations when converting between
 * user space and kernel space addresses.
 */
void * suapi_user_shmaddress_ptr = NULL;

/* suapi_memparts_begin_ptr and suapi_memparts_end_ptr contain the beginning
 * and end addresses of the SUAPI partition memory area within the SUAPI
 * shared memory region. They are used as spot checks to ensure that memory
 * addresses which are passed in or stored in the shared memory region are not
 * pointing to kernel areas outside the domain of SUAPI.
 */
void * suapi_memparts_begin_ptr = NULL;
void * suapi_memparts_end_ptr = NULL;

/* suapi_shmsize_glb contains the size of the SUAPI shared memory region. */
int suapi_shmsize_glb = 0;

/* suFlagGroupTable_ptr contains a pointer to the area within the SUAPI shared
 * memory region which contains the flag group table structure.
 */
struct suFlagGroup * suFlagGroupTable_ptr = NULL;

/* suapi_pcb_ptr contains a pointer to the area within the SUAPI shared memory
 * region which contains the partition control block. The partition control
 * block describes the allocation and distribution of the SUAPI memory
 * partitions.
 */
SU_PCB * suapi_pcb_ptr = NULL;

/*
 * suLowPower_ptr contains a pointer to the area within the SUAPI
 * private shared memory region which contains the low power flags and
 * related arrays of pids for debugging.
 *
 * suLowPower_advice_driver_num is the MPM's handle for advising about
 * low power flag changes.
 */

SU_LPWR *suLowPower_ptr = NULL;

#if defined(MPM_ADVICE_DRIVER_IS_BUSY)
int suLowPower_advice_driver_num = -1;
#endif

/* suapi_logger_config_ptr contains a pointer to the area within the SUAPI
 * shared memory region which contains the logger config area.
 */
char * suapi_logger_config_ptr = NULL;

/************** GLOBAL EXPORT DEFINITIONS ************************************/
EXPORT_SYMBOL(suapi_global_data_ptr);

/************** FUNCTION DEFINITIONS *****************************************/
static int suapi_open(struct inode *inode,struct file *filp)
{
    return 0;
}

static int suapi_release(struct inode *node, struct file *filp)
{
    return 0;
}

/*
 * DESCRIPTION: suapi_read
 *     The read method of /dev/suapi reads the SUAPI shared memory segment.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being read.
 *     count: number of bytes to read.
 *     f_pos: the current offset into the SUAPI shared memory region for this
 *            instance of the open file.
 *
 * OUTPUTS:
 *     buf: user space address of buffer to be filled.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static int suapi_read(struct file *filp, char __user *buf, size_t count,
                      loff_t *f_pos)
{
    if (*f_pos >= SU_GLOBAL_SIZE)
        return 0;
    if (*f_pos + count > SU_GLOBAL_SIZE)
        count = SU_GLOBAL_SIZE - *f_pos;

    if (copy_to_user (buf,
                      suapi_kernel_start_ptr + *f_pos,
                      count))
        return -EFAULT;

    *f_pos += count;
    return count;
}

/*
 * DESCRIPTION: suapi_private_read
 *     The read method of /dev/suapipriv reads additional data structures from
 *     within the SUAPI kernel module that are not meant to be accessed by
 *     applications but are only made readable for panic and debug utilities.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being read.
 *     count: number of bytes to read.
 *     f_pos: the current offset into the SUAPI shared memory region for this
 *            instance of the open file.
 *
 * OUTPUTS:
 *     buf: user space address of buffer to be filled.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static int suapi_private_read(struct file *filp, char __user *buf, size_t count,
                      loff_t *f_pos)
{
    if (*f_pos >= SU_PRIVATE_SIZE)
        return 0;
    if (*f_pos + count > SU_PRIVATE_SIZE)
        count = SU_PRIVATE_SIZE - *f_pos;

    if (copy_to_user (buf,
                      suapi_private_start_ptr + *f_pos,
                      count))
        return -EFAULT;

    *f_pos += count;
    return count;
}

static long suapi_unlocked_ioctl(struct file *filp,
                       unsigned int cmd,
                       unsigned long arg)
{
    int result = 0;

    /* 
     * If a panic occurred then don't allow suapi calls after that so that we 
     * can get a core dump that is consistent with the time of panic.
     */
    if (su_previous_panic != 0)
    {
        /* if signal occurs or something wakes it up, just put it back to sleep */
        while (1)
        {
            set_current_state(TASK_UNINTERRUPTIBLE);
            schedule();
        }
    }

    switch (cmd)
    {
        case SUAPI_CREATE_TIMER:
            return(suapi_create_timer_ioctl(arg));
        case SUAPI_DELETE_TIMER:
            return(suapi_delete_timer_ioctl(arg));
        case SUAPI_START_TIMER:
            return(suapi_start_timer_ioctl(arg));
        case SUAPI_STOP_TIMER:
            return(suapi_stop_timer_ioctl(arg));
        case SUAPI_TIMELEFT:
            return(suapi_time_left_ioctl(arg));
#ifdef CONFIG_SUAPI
        case SUAPI_ALLOC_EVENT:
            return(suapi_alloc_event_ioctl(arg));
        case SUAPI_ALLOC_EVENT_MASK:
            return(suapi_alloc_event_mask_ioctl(arg));
        case SUAPI_VERIFY_ALLOC_EVENT_MASK:
            return(suapi_verify_alloc_event_mask_ioctl(arg));
        case SUAPI_FREE_EVENT:
            return(suapi_free_event_ioctl(arg));
        case SUAPI_CLEAR_EVENT_MASK:
            return(suapi_clear_event_mask_ioctl(arg));
        case SUAPI_SET_EVENT_MASK:
            return(suapi_set_event_mask_ioctl(arg));
        case SUAPI_GET_EVENT_MASK:
            return(suapi_get_event_mask_ioctl(arg));
        case SUAPI_WAIT_ANY_EVENT:
            return(suapi_wait_any_event_ioctl(arg));
        case SUAPI_WAIT_ALL_EVENT:
            return(suapi_wait_all_event_ioctl(arg));
        case SUAPI_QUEUES_SEND_MSG_AND_WAIT:
            return(suapi_queues_send_msg_and_wait_ioctl(arg));
        case SUAPI_QUEUES_SEND_MESSAGE:
            return(suapi_send_message_ioctl(arg));
        case SUAPI_REGISTER_EVENT_MASK_WITH_QUEUE:
            return(suapi_register_event_mask_with_queue_ioctl(arg));
#endif /* CONFIG_SUAPI */
        case SUAPI_ALLOC_MEM:
            return(suapi_alloc_mem_ioctl(arg));
        case SUAPI_FREE_MEM:
            return(suapi_free_mem_ioctl((void *)arg));
        case SUAPI_QUEUES_WITH_FD:
            return(suapi_queues_with_fd_ioctl(filp, arg));
        case SUAPI_QUEUES_WAKE_SELECT:
            return(suapi_queues_wake_select_ioctl(filp, arg));
        case SUAPI_CREATE_QUEUE:
            return(suapi_create_queue_ioctl(arg));
        case SUAPI_DELETE_QUEUE:
            return(suapi_delete_queue_ioctl((SU_QUEUE_HANDLE) arg));
        case SUAPI_RECEIVE_MESSAGE_FROM_QUEUE:
            return(suapi_receive_message_from_queue_ioctl(arg));
        case SUAPI_GET_NUM_MESSAGES_ON_QUEUE:
            return(suapi_get_num_messages_on_queue_ioctl((SU_QUEUE_HANDLE)arg));
        case SUAPI_DELETE_PORT:
            return(suapi_delete_port_ioctl((SU_PORT_HANDLE) arg));
        case SUAPI_CREATE_PORT_FROM_QUEUE:
            return(suapi_create_port_ioctl(arg, SU_MESSAGE_PORT));
        case SUAPI_CREATE_LOGGER_PORT:
            return(suapi_create_port_ioctl(arg, SU_LOGGER_PORT));
        case SUAPI_LOG_MESSAGE:
            return(suapi_log_message_ioctl(arg));
        case SUAPI_LOG_DATA:
            return(suapi_log_data_ioctl(arg));
        case SUAPI_SEM_CREATE_BINARY:
            return(suapi_sem_create_ioctl(SU_SEMA_BINARY,arg));
        case SUAPI_SEM_CREATE_COUNTING:
            return(suapi_sem_create_ioctl(SU_SEMA_COUNTING,arg));
        case SUAPI_SEM_CREATE_MUTEX:
            return(suapi_sem_create_ioctl(SU_SEMA_MUTEX,arg));
        case SUAPI_SEM_ACQUIRE:
            return(suapi_sem_acquire_ioctl(arg));
        case SUAPI_SEM_RELEASE:
            return(suapi_sem_release_ioctl(arg));
        case SUAPI_SEM_DELETE:
            return(suapi_sem_delete(arg));
        case SUAPI_PANIC:
            return(suapi_panic_ioctl(arg));
        case SUAPI_GET_MS:
            return(suapi_get_ms_ioctl(arg));

#ifdef CONFIG_SUAPI_TEST
        case SUAPI_SUSPEND_PM:
        {
            unsigned long flags;
            int rval;
	    
#define SUSPEND_WITH_IRQ_DISEBLED   0
#define SUSPEND_WITH_IRQ_ENABLED    1

            if(SUSPEND_WITH_IRQ_ENABLED != arg)
	    {	    
                local_irq_save(flags);
                rval = suapi_suspend((struct device *) &arg,(u32) arg,(u32) arg);
                local_irq_restore(flags);
	    }
	    else
            {
	        rval = suapi_suspend((struct device *) &arg,(u32) arg,(u32) arg);
	    }
            return rval;
        }
        case SUAPI_RESUME_PM:
            return(suapi_resume((struct device *) &arg,(u32) arg));
#endif

        case SUAPI_FINDNAME:
            return(suapi_findname_ioctl(filp,arg));
        case SUAPI_REGISTERNAME:
            return(suapi_registername(filp,arg));
        case SUAPI_UNREGISTERNAME:
            return(suapi_unregistername(filp,arg));
        case SUAPI_GET_SH_MEM_SIZE:
            return( put_user(suapi_private_data_ptr->shared_size,
                             (unsigned long *)arg) );
        case SUAPI_GET_SYSCONFIG:
            if (copy_to_user ((struct SU_SYSCONFIG_ST *)arg,
                      suapi_sysconfig_ptr,
                      sizeof(struct SU_SYSCONFIG_ST)))
                return -EFAULT;
            return 0;
        case SUAPI_GET_SH_RGN_ADDR:
            return(put_user(suapi_private_data_ptr->kshared_p, (void **)arg));
        case SUAPI_PORTS_DISABLE_LOGGING:
            return(suapi_disable_port_logging((SU_PORT_HANDLE) arg));
        case SUAPI_PORTS_ENABLE_LOGGING:
            return(suapi_enable_port_logging((SU_PORT_HANDLE) arg));
        case SUAPI_PORTS_LOGGING_ENABLED:
            return(suapi_port_logging_enabled((SU_PORT_HANDLE) arg));
        case SUAPI_PORTS_GET_PORTID:
            return(suapi_get_portid((SU_PORT_HANDLE) arg));
        case SUAPI_PORTS_GET_ROUTING:
            return(suapi_get_port_routing((SU_PORT_HANDLE) arg));
        case SUAPI_PORTS_SET_ROUTING:
            return(suapi_set_port_routing(arg));
        case SUAPI_SLEEP:
            return (suapi_sleep_ioctl(arg));
        case SUAPI_ALLOC_LOW_POWER_FLAG:
            return(suapi_alloc_low_power_flag_ioctl(arg));
        case SUAPI_FREE_LOW_POWER_FLAG:
            return(suapi_free_low_power_flag_ioctl(arg));
        case SUAPI_ENABLE_LOW_POWER_FLAG:
            return(suapi_enable_lower_power_flag(arg));
        case SUAPI_DISABLE_LOW_POWER_FLAG:
            return(suapi_disable_lower_power_flag(arg));
        case SUAPI_GET_LOW_POWER_FLAG:
            return(suapi_get_lower_power_flag(arg));
        default:
            printk("suapi : unknown IOCTL \n");
            result = -ENOTTY;
            break;
    }
    return result;
}

/*
 * DESCRIPTION: suapi_mmap
 *     The mmap method of /dev/suapi.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being memory mapped.
 *     vma: pointer to the virtual memory area strcutre for this instance of
 *          the file being memory mapped.
 *
 * OUTPUTS:
 *     N/A
 *
 * IMPORTANT NOTES:
 *     The actual work of memory mapping is not done until the kernel tries to
 *     actually page an address.
 */
static int suapi_mmap(struct file *filp,struct vm_area_struct *vma)
{
    vma->vm_ops = &suapi_vm_ops;

    vma->vm_flags |= VM_RESERVED;

    return 0;
}

/*
 * DESCRIPTION: suapi_vma_nopage
 *     The paging method of /dev/suapi. This function converts the requested
 *     user space address of the memory mapped region to the kernel address
 *     and gets the page.
 *
 * INPUTS:
 *     vma: pointer to the virtual memory area strcutre for this instance of
 *          the memory mapped file.
 *     address: user space address of the requested memory.
 *
 * OUTPUTS:
 *     type: This variable tells the kernel why the fault occurred and whether
 *           it should produce an error. In our case, it is just a minor fault
 *           and the kernel will bump the minor page fault count.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static struct page *suapi_vma_nopage(struct vm_area_struct *vma,
                                unsigned long address, int *type)
{
    unsigned long offset;
    struct page *page = NOPAGE_SIGBUS;

    offset = (address - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT);

    if (offset >= SU_GLOBAL_SIZE)
        return page;

    page = vmalloc_to_page(suapi_kernel_start_ptr + offset);

    /* Increment page count. */
    get_page(page);

    if (type)
        *type = VM_FAULT_MINOR;

    return page;
}

/*
 * DESCRIPTION: suapi_private_mmap
 *     The mmap method of /dev/suapipriv, only to be used by panic and debug
 *     utilities.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being memory mapped.
 *     vma: pointer to the virtual memory area structure for this instance of
 *          the file being memory mapped.
 *
 * OUTPUTS:
 *     N/A
 *
 * IMPORTANT NOTES:
 *     The actual work of memory mapping is not done until the kernel tries to
 *     actually page an address.
 */
static int suapi_private_mmap(struct file *filp,struct vm_area_struct *vma)
{
    vma->vm_ops = &suapi_vm_private_ops;

    /* This device must only be mapped read-only. */
#if defined(CONFIG_ARM)
    if ((vma->vm_page_prot & L_PTE_WRITE) == L_PTE_WRITE)
        return -EACCES;
#else
    if (pgprot_val(vma->vm_page_prot) != pgprot_val(PAGE_READONLY))
        return -EACCES;
#endif

    vma->vm_flags |= VM_RESERVED;

    return 0;
}

/*
 * DESCRIPTION: suapi_private_vma_nopage
 *     The paging method of /dev/suapipriv. This function converts the requested
 *     user space address of the memory mapped region to the kernel address
 *     and gets the page.
 *
 * INPUTS:
 *     vma: pointer to the virtual memory area strcutre for this instance of
 *          the memory mapped file.
 *     address: user space address of the requested memory.
 *
 * OUTPUTS:
 *     type: This variable tells the kernel why the fault occurred and whether
 *           it should produce an error. In our case, it is just a minor fault
 *           and the kernel will bump the minor page fault count.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static struct page *suapi_private_vma_nopage(struct vm_area_struct *vma,
                                unsigned long address, int *type)
{
    unsigned long offset;
    struct page *page = NOPAGE_SIGBUS;

    offset = (address - vma->vm_start) + (vma->vm_pgoff << PAGE_SHIFT);

    if (offset >= SU_PRIVATE_SIZE)
        return page;

    page = vmalloc_to_page(suapi_private_start_ptr + offset);

    /* Increment page count. */
    get_page(page);

    if (type)
        *type = VM_FAULT_MINOR;

    return page;
}

static void suapi_device_release(struct device *dev)
{
    return;
}


static void suapi_cleanup_module(void)
{
    int i;

    /* This must be done prior to freeing the shared memory area because
     * SU_NUM_SEMAPHORES references data there.
     */
    if (semarray)
    {
        for (i = 0; i < SU_NUM_SEMAPHORES; i++)
        {
            if (semarray[i].sem != NULL)
                suapi_sem_delete(SUAPI_CREATE_SHANDLE(semarray[i].deletions,i));
        }
    }

    if (timers)
    {
        for (i = 0; i < SU_NTIMER; i++)
        {
            if (timers[i].ktstate != TFREE)
                rtc_sw_task_kill(&timers[i].ltimer);
        }
    }

    suapi_clean_names();

    if (suapi_kernel_start_ptr)
        vfree(suapi_kernel_start_ptr);

    if (su_part_sizes_ptr)
        kfree(su_part_sizes_ptr);

    if (suapi_private_start_ptr)
        vfree(suapi_private_start_ptr);

#if defined(MPM_ADVICE_DRIVER_IS_BUSY)
    if (suLowPower_advice_driver_num >= 0)
        mpm_unregister_with_mpm(suLowPower_advice_driver_num);
#endif

    device_remove_file(&(suapi_device.dev), &dev_attr_su_sysconfig);
    device_remove_file(&(suapi_device.dev), &dev_attr_minfo);
    platform_device_unregister(&suapi_device);
    driver_unregister(&suapi_driver);

#ifdef CONFIG_DEVFS_FS
    if (suapi_private_major >=0)
    {
        unregister_chrdev(suapi_private_major,private_device_name);
        devfs_remove(private_device_name);
    }

    if (suapi_major >=0)
    {
        unregister_chrdev(suapi_major,modname);
        devfs_remove(modname);
    }
#endif

    misc_deregister(&supanic_device);
}

static int __init suapi_init_module(void)
{
    int result, i;
    struct file *fp;
    int portsz, qsz, totpartsz, pcbsz, flgtblsz, flgalcsz, log_cnfg_sz;
    int timersz, semsz, namehashsz, privdatasz, syscfgsz, globalstsz;
    char *buf, *token, *valuestr, *retptr;
    char *partition_map = NULL, *save_partition_buf = NULL;
    int buflen;
    int private_region_size = 0;
    /* SUAPI configuration data as read in from configuration file. */
    SU_SYSCONFIG sysconfig_st;

    result = driver_register(&suapi_driver);
    if (result != 0)
        return result;

    result = platform_device_register(&suapi_device);
    if (result != 0)
    {
        driver_unregister(&suapi_driver);
        return result;
    }

    if (device_create_file(&(suapi_device.dev), &dev_attr_su_sysconfig) < 0)
        printk("suapi_init_module: unable to cretae SUAPI attribute file.\n");
    if (device_create_file(&(suapi_device.dev), &dev_attr_minfo) < 0)
        printk("suapi_init_module: unable to cretae SUAPI attribute file.\n");

#ifdef CONFIG_DEVFS_FS
    suapi_major = register_chrdev(0,modname,&suapi_fops);

    if (suapi_major < 0)
    {
        suapi_cleanup_module();
        return suapi_major;
    }

    devfs_mk_cdev(MKDEV(suapi_major,0), S_IFCHR | S_IRUSR , modname);

    suapi_private_major = register_chrdev(0,private_device_name,&suapi_private_fops);

    if (suapi_private_major < 0)
    {
        suapi_cleanup_module();
        return suapi_private_major;
    }

    devfs_mk_cdev(MKDEV(suapi_private_major,0), S_IFCHR | S_IRUSR , private_device_name);
#else
    return -ENOSYS;
#endif

    // Register the supanic misc device.
    if (misc_register(&supanic_device))
    {
        printk("Could not register /dev/supanic");
        goto fail;
    }
    else
    {
#ifdef SUAPI_DEBUG
        printk("Got /dev/supanic minor: %d\n", supanic_device.minor);
#endif
    }

    fp = filp_open(suapi_config_file, O_RDONLY, 0);
    if (IS_ERR(fp))
    {
        result = PTR_ERR(fp);
        printk(
         "suapi_init_module: Failed opening configuration file %s! Error: %d\n",
         suapi_config_file, result);
        goto fail;
    }

    memset((void *)(&sysconfig_st), 0, sizeof(SU_SYSCONFIG));

    if (NULL == (buf = kmalloc(SU_CFG_BUFSIZE, GFP_KERNEL)))
    {
        result = -ENOMEM;
        printk("suapi_init_module: Could not kmalloc config buffer space.\n");
        filp_close(fp, NULL);
        goto fail;
    }

    /* Need a default value > 0 for flaggroups if missing in config file. */
    *((int *) &(sysconfig_st.nflaggroups)) = 1;

    while (buf && (suapi_fgets(buf, SU_CFG_BUFSIZE, fp) != NULL))
    {
        i = 2;
        buflen = strlen(buf);

        /* If line is bigger than SU_CFG_BUFSIZE, get the rest of it. */
        while (buflen && buf[buflen-1] != '\n')
        {
            char * tmpbuf;
            tmpbuf = kmalloc(SU_CFG_BUFSIZE*i, GFP_KERNEL);
            if (tmpbuf == NULL)
            {
                result = -ENOMEM;
                printk("suapi_init_module: Could not reallocate buf\n");
                if (buf) kfree(buf);
                filp_close(fp, NULL);
                goto fail;
            }

            /* Copy data to new buffer. */
            strcpy(tmpbuf,buf);
            kfree(buf);
            buf = tmpbuf;

            retptr = suapi_fgets((buf+buflen),SU_CFG_BUFSIZE,fp);
            if (retptr != NULL)
            {
                buflen = strlen(buf);
                i++;
            }
            else
            {
                result = -EIO;
                printk("suapi_init_module: Could not read input from %s\n",
                        suapi_config_file);
                if (buf) kfree(buf);
                filp_close(fp, NULL);
                goto fail;
            }
        }

        /* If the line contains a colon, this might be a token:value pair. */
        if(strchr(buf,':'))
        {
            /* The following will skip any leading whitespace and
             * return all other characters up to
             * a blank, a tab, or a colon.
             */
            if ((token = suapi_strtok(buf, " \t:")) == NULL)
                continue;

            /* If the first character on the line is a hash, it must
             * be a comment, so continue. */
            if (*token == '#')
                continue;

            /* Otherwise, assume the next token is a value. (It could be
             * that the first and second tokens were separated by
             * whitespace only.  If it turns out that the file has extra
             * or incorrectly formed content, it will be ignored by the
             * code below.)
             */

            /* Handle the partition table special case first. */
            if (!strcmp(token,"SU_CONFIG_PART_TABLE"))
            {
                char * p = token + strlen(token);

                /* Skip past the null characters that strtok() put in the
                 * string in order to find out if there is really a partition
                 * map. If not, just continue on to next line in the file.
                 */
                while (*p == '\0' && p < buf+buflen) p++;
                if (p >= buf+buflen)
                    continue;

                /* Store the partition map for now until the entire
                 * config file has been processed. This is because the
                 * partition map depends on the number of partitions.
                 */
                partition_map = p;

                /* Save the start address of the buffer that contains the
                 * partition map so it can be freed later. Then allocate a new
                 * buffer for processing of the rest of the file.
                 */
                save_partition_buf = buf;
                if (NULL == (buf = kmalloc(SU_CFG_BUFSIZE, GFP_KERNEL)))
                {
                    result = -ENOMEM;
                    printk("suapi_init_module: ");
                    printk("Could not kmalloc new config buffer space.\n");
                    filp_close(fp, NULL);
                    if(save_partition_buf) kfree(save_partition_buf);
                    goto fail;
                }
            }
            else
            {
                /* If there is no value after the token, continue.  */
                if ((valuestr = suapi_strtok(NULL, " \t:")) == NULL)
                    continue;

                S(SU_CONFIG_MAXTIMEOUT,maxtimeout);
                S(SU_CONFIG_MINTIMEOUT,mintimeout);
                S(SU_CONFIG_NQUEUES,nqueues);
                S(SU_CONFIG_NPARTITION,npartition);
                S(SU_CONFIG_NTIMER,ntimers);
                S(SU_CONFIG_NUM_BINARY_SEMAPHORE,nbinarysemaphores);
                S(SU_CONFIG_NUM_COUNTING_SEMAPHORE,ncountingsemaphores);
                S(SU_CONFIG_NUM_MUTEX_SEMAPHORE,nmutexsemaphores);
                S(SU_CONFIG_MEMORY_PROMOTION,memorypromotion);
                S(SU_CONFIG_NPORT,nports);
                S(SU_CONFIG_NAME_HASHTABLE_SIZE,namehashtablesize);
                if (!strcmp(token,"SU_CONFIG_NFLAGGROUP"))
                {
                    /*
                     * One is added to the number of flag groups that
                     * are configured. This is to allow for the
                     * condition when the number of configured
                     * flaggroups is 0.
                     */
                    *((int *) &(sysconfig_st.nflaggroups)) = 
                                            simple_strtoul(valuestr,NULL,0) + 1;
                }
                else if (!strcmp(token,"SU_CONFIG_SHMADDRESS"))
                {
                    suapi_user_shmaddress_ptr =
                                    (void *) simple_strtoul(valuestr,NULL,0);
                }
            }
        }
    }

    if (buf) kfree(buf);
    filp_close(fp, NULL);

    /* Process the {<size>,<number>} pairs for each partition. */
    if (sysconfig_st.npartition && partition_map)
    {
        su_part_sizes_ptr =
            kmalloc(sysconfig_st.npartition * sizeof(SU_PART_SIZE_INFO),
                    GFP_KERNEL);
        if (NULL == su_part_sizes_ptr)
        {
            result = -ENOMEM;
            printk("suapi_init_module: Could not kmalloc space for su_part_sizes_ptr.\n");
            if(save_partition_buf) kfree(save_partition_buf);
            goto fail;
        }

        /* Handle the first partition before going into the for loop because
         * it's a little different.
         */
        if ((valuestr = suapi_strtok(partition_map, "{}, \t")) != NULL)
            su_part_sizes_ptr[0].blocksize = simple_strtoul(valuestr,NULL,0);
        if ((valuestr = suapi_strtok(NULL, "{}, \t")) != NULL)
            su_part_sizes_ptr[0].nb_blocks = simple_strtoul(valuestr,NULL,0);

        for (i = 1; i < sysconfig_st.npartition; i++)
        {
            if ((valuestr = suapi_strtok(NULL, "{}, \t")) != NULL)
               su_part_sizes_ptr[i].blocksize = simple_strtoul(valuestr,NULL,0);
            if ((valuestr = suapi_strtok(NULL, "{}, \t")) != NULL)
               su_part_sizes_ptr[i].nb_blocks = simple_strtoul(valuestr,NULL,0);
        }
    }

    if (save_partition_buf) kfree(save_partition_buf);

    /*************************************************************************/
    /* Compute total sizes of shared and private memory regions based on     */
    /* system configuration values.                                          */
    /**************************************************************************/


    /************ Sizes of data structures in GLOBAL memory region. **********/
    globalstsz = SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_GLOBAL_DATA));

    /**** Flag services need space for the flag group table and the allocated
     *    bit array.
     */
    flgtblsz = sysconfig_st.nflaggroups * 
        SU_ROUND_TO_DOUBLE_WORD(sizeof(struct suFlagGroup));
    flgalcsz = SU_BITS_TO_UINT32(sysconfig_st.nflaggroups) * sizeof(UINT32);

    /* Datalogger ports need space for the port configuration area. */
    log_cnfg_sz = LOGGER_CONFIG_SIZE;

    /* add 0x8 in case it is not 8 byte aligned */
    totpartsz = 8;

    for (i = 0; i < sysconfig_st.npartition; i++)
    {
        totpartsz +=
            (su_part_sizes_ptr[i].blocksize * su_part_sizes_ptr[i].nb_blocks);
    }


    /*********** Sizes of data structures in PRIVATE memory region. **********/

    privdatasz = SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_PRIVATE_DATA));

    syscfgsz = SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_SYSCONFIG));

    timersz = sysconfig_st.ntimers *
                    SU_ROUND_TO_DOUBLE_WORD(sizeof(struct Suapi_Kernel_Timer));

    semsz = (sysconfig_st.nbinarysemaphores
                + sysconfig_st.ncountingsemaphores
                + sysconfig_st.nmutexsemaphores)
                    * SU_ROUND_TO_DOUBLE_WORD(sizeof(struct SuapiSemArray));

    namehashsz = sysconfig_st.namehashtablesize *
                    SU_ROUND_TO_DOUBLE_WORD(sizeof(struct hlist_head));

    /**** Port services need space for the port structure table. */
    portsz = sysconfig_st.nports *
        SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_PORT_HANDLE_STRUCT));

    /**** Queue services need space for the queue structure table. */
    qsz = sysconfig_st.nqueues * SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_QUEUE));

    /**** Memory services need space for the partition control block. */
    pcbsz = sysconfig_st.npartition * SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_PCB));

    /* Store total size for shared memory segment. */
    suapi_shmsize_glb =
        globalstsz             /* SU_GLOBAL_DATA structure   */
        + flgtblsz             /* flag group table           */
        + flgalcsz             /* flag group alloc bit array */
        + log_cnfg_sz          /* logger config area         */
        + totpartsz;           /* shared memory partitions   */
    /* Round up to page boundary */
    suapi_shmsize_glb = SU_SZ_TO_NUMPGS(suapi_shmsize_glb) * PAGE_SIZE;

    /* Compute total size for private memory region. */
    private_region_size =
        privdatasz            /* SU_PRIVATE_DATA structure  */
        + syscfgsz            /* SU_SYSCONFIG structure     */
        + timersz             /* array of timer structures  */
        + semsz               /* array of sem structures    */
        + namehashsz          /* array of name structures   */
        + portsz              /* array of port structures   */
        + qsz                 /* array of queue structures  */
        + pcbsz               /* prtn control block structs */
        + sizeof(SU_LPWR);    /* low power flags */

    /* Round up to page boundary */
    private_region_size = SU_SZ_TO_NUMPGS(private_region_size) * PAGE_SIZE;

    /* Allocate private memory region. */
    suapi_private_start_ptr = vmalloc(private_region_size);
    if (!suapi_private_start_ptr)
    {
        printk("suapi_init_module: ");
        printk("Failed allocating SUAPI private memory region of size %d\n",
               private_region_size);
        result = -ENOMEM;
        goto fail;
    }

    memset(suapi_private_start_ptr, 0, private_region_size);
    suapi_private_end_ptr = suapi_private_start_ptr + private_region_size;

    suapi_private_data_ptr = (SU_PRIVATE_DATA *) suapi_private_start_ptr;

    /* Allocate shared memory region. */
    suapi_kernel_start_ptr = vmalloc(suapi_shmsize_glb);
    if (!suapi_kernel_start_ptr)
    {
        printk("suapi_init_module: ");
        printk("Failed allocating SUAPI shared memory region of size %d\n",
               suapi_shmsize_glb);
        result = -ENOMEM;
        goto fail;
    }

    memset(suapi_kernel_start_ptr, 0, suapi_shmsize_glb);

    suapi_global_data_ptr = (SU_GLOBAL_DATA *)suapi_kernel_start_ptr;

    /*************************************************************************
     * Initialize the private memory region.                                 *
     *************************************************************************/
    suapi_private_data_ptr->private_size = private_region_size;
    suapi_private_data_ptr->kprivate_p = suapi_private_start_ptr;
    suapi_private_data_ptr->shared_size = suapi_shmsize_glb;
    suapi_private_data_ptr->kshared_p = suapi_kernel_start_ptr;

    /* The following sizes are not rounded to a double word. That will be done
     * by the debug utilities as needed.
     */
    suapi_private_data_ptr->timersz = sizeof(struct Suapi_Kernel_Timer);
    suapi_private_data_ptr->semsz = sizeof(struct SuapiSemArray);
    suapi_private_data_ptr->namehashsz = sizeof(struct hlist_head);
    suapi_private_data_ptr->queuesz = sizeof(SU_QUEUE);

    /* Copy sysconfig structure to the private memory region. The sysconfig
     * structure is located right after the SU_PRIVATE_DATA struct in the
     * private memory region.
     */
    suapi_sysconfig_ptr = (SU_SYSCONFIG *)
                                (suapi_private_start_ptr + privdatasz);
    memcpy((struct SU_SYSCONFIG_ST *) suapi_sysconfig_ptr,
            &(sysconfig_st),
            sizeof(SU_SYSCONFIG));

    /* Initialize Timers. */
    timers = (struct Suapi_Kernel_Timer *)
                   ((unsigned long) suapi_sysconfig_ptr + syscfgsz);
    for (i = 0; i < SU_NTIMER; i++)
        timers[i].sut_lock = SPIN_LOCK_UNLOCKED;

    /* Initialization of SUAPI semaphore structures. */

    /* The following contain the current count of free binary, counting,
     * and mutex semaphores.
     */
    atomic_set(&num_free_binary,SU_NUM_BINARY_SEMAPHORE);
    atomic_set(&num_free_counting,SU_NUM_COUNTING_SEMAPHORE);
    atomic_set(&num_free_mutex,SU_NUM_MUTEX_SEMAPHORE);

    semarray = (struct SuapiSemArray *) ((unsigned long) timers + timersz);

    /* Initialize deletion counts and semaphore pointers. */
    for (i = 0; i < SU_NUM_SEMAPHORES; i++)
    {
        semarray[i].deletions = 1;
        semarray[i].sem = NULL;
    }

    atomic_set(&timer_count,SU_NTIMER);

    /* Initialization of name services hash table array. */
    suNames = (struct hlist_head *) ((unsigned long) semarray + semsz);

    for (i = 0; i < SU_NAME_HASHTABLE_SIZE; i++)
        INIT_HLIST_HEAD(&(suNames[i]));

    /* Store a pointer to the port structures table. It exists in the private
     * memory region after the name services hash table.
     */
    suPortTable_ptr = (struct SU_PORT_HANDLE_STRUCT *)
                        ((unsigned long) suNames + namehashsz);

    /* Store a pointer to the queue structures table. It exists in the private
     * memory region after the port structures table.
     */
    su_qarray_ptr = (SU_QUEUE *) ((unsigned long) suPortTable_ptr + portsz);

    /* Initialize wait queue within each SUAPI queue structure. */
    for (i = 0; i < SU_NQUEUES; i++)
        init_waitqueue_head(&(su_qarray_ptr[i].suqwaiters));

    /* Store the location of the start of the memory PCB table. It exists in
     * the private memory region after the array of queue structures.
     */
    suapi_pcb_ptr = (SU_PCB *) ((unsigned long) su_qarray_ptr + qsz);

    /* Store the location of the start of the low power flags. It
     * exists in the private memory region after the memory PCB table.
     * Since the private memory was zeroed, there is no further
     * initialization necessary.
     */

    suLowPower_ptr = (SU_LPWR *) ((unsigned long) suapi_pcb_ptr + pcbsz);

#if defined(MPM_ADVICE_DRIVER_IS_BUSY)
    suLowPower_advice_driver_num =  mpm_register_with_mpm(modname);
#endif

    /*************************************************************************/
    /* Initialize the shared memory region.                                  */
    /* Store global pointers at the beginning of the memory region.          */
    /*************************************************************************/

    /* Initialize the SU_GLOBAL_DATA structure which is at the beginning of
     * the shared memory.
     */

    /* Copy the start address into the first and second words of the region. */
    suapi_global_data_ptr->user_shmaddress = suapi_user_shmaddress_ptr;

    /* Store the location of the start of the flag group table. It exists in
     * the shared memory region after the global data structure.
     */
    suapi_global_data_ptr->suFlagGroupTable =
                                    suapi_kernel_start_ptr + globalstsz;
    suFlagGroupTable_ptr = suapi_global_data_ptr->suFlagGroupTable;

    /* Store the location of the start of the flag group allocation bit array.
     * It appears after the flag group table.
     */
    suapi_global_data_ptr->suFlagGroupAlloc =
        suapi_global_data_ptr->suFlagGroupTable + flgtblsz;

    /* Store the location of the start of the logger port config
     * area. It appears after the flag group allocation bit array.
     */
    suapi_global_data_ptr->logger_config =
        suapi_global_data_ptr->suFlagGroupAlloc + flgalcsz;
    suapi_logger_config_ptr = suapi_global_data_ptr->logger_config;

    /* Store the location of the start of the memory partitions. It appears
     * after the logger port configuration area in the shared memory region.
     * The memory pool needs to be 8 byte aligned.
     */
    suapi_memparts_begin_ptr = suapi_logger_config_ptr + log_cnfg_sz;

    if (((UINT32)suapi_memparts_begin_ptr & 0x7) != 0)
        suapi_memparts_begin_ptr =
                (void*)(((UINT32)(suapi_memparts_begin_ptr + 0x7)) & (~0x7));

    suapi_memparts_end_ptr = suapi_memparts_begin_ptr + totpartsz;

    /*
     * Create and initialize atomic ops mutex.
     */
    suapi_global_data_ptr->suAtomicOpsMutex = 0;

    /*
     * Create and initialize flag group mutex.
     */
    suapi_global_data_ptr->suFlagsMutex = 0;

    /* Memory partition Initialization:
     * The following function initializes all of the associated data
     * structures.
     */
    if (suMemInit(su_part_sizes_ptr, suapi_memparts_begin_ptr) != 0)
    {
        printk("suapi_init_module: ");
        printk("Failed initializing SUAPI shared memory region\n");
        result = -ENOMEM;
        goto fail;
    }

    kfree(su_part_sizes_ptr);
    su_part_sizes_ptr = NULL;

    /* Initialize logger port conf area. */
    result = suLogConfInit();

    if (result)
        goto fail;

#ifndef PU_REASON_WATCHDOG
#define PU_REASON_WATCHDOG (0x8000)
#endif

    if (PU_REASON_WATCHDOG & mot_powerup_reason())
        suPanic(SU_WATCHDOG_PANIC_ID,0);

    return 0;
  fail:
    suapi_cleanup_module();
    return result;
}

module_init(suapi_init_module);
module_exit(suapi_cleanup_module);
MODULE_AUTHOR("Motorola Mobile Devices SUAPI Team");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Motorola Mobile Devices SUAPI support module.");
