// Copyright (c) 2006,2007 Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI memory services within the SUAPI kernel
 *      module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>

#include <linux/suapi_module.h>
#include <linux/su_mem_hdr.h>
#include <linux/su_panic_codes.h>

/************** LOCAL CONSTANTS **********************************************/
#define SU_ALLOC_MEMORY_MAGIC_PAD 0xFEED
#define SU_FREE_MEMORY_MAGIC 0xDEAD 
#define SU_FREE_MEMORY_MAGIC_PAD 0xBEEF

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/
/* SU_BLOCK_IN_PARTITION evaluates to TRUE if the specified block is in the
 * partition described by the specified pcb structure and FALSE otherwise.
 * block is expected to be pointing to the memory header.
 */
#define SU_BLOCK_IN_PARTITION(block,pcb) \
    ( ((char*)(block) >= (char*)(pcb).part_start_add) \
       && ((char*)(block) < (char*)(pcb).part_end_add) \
       && ((((char*)(block)-(char*)(pcb).part_start_add)%(pcb).blocksize) == 0))

/************** LOCAL VARIABLES **********************************************/
/* Memory services-specific spin lock.
 * Soft interrupts are expected to be disabled when locking this spinlock
 * because it is used during soft interrupt context.
 */
static spinlock_t su_memory_lock = SPIN_LOCK_UNLOCKED;

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/
/*
 * DESCRIPTION: suMemInit
 *     This service is called to initialize the SUAPI memory pool
 *     data structures.
 *
 * INPUTS:
 *     su_part_sizes_ptr: array containing <block size>,<number of
 *                        blocks> pairs for each partition.
 *
 * OUTPUTS:
 *     Returns 0 on success, -1 on failure.
 *
 * IMPORTANT NOTES:
 *     This function must be called before any other SUAPI memory
 *     services.
 *
 */
int __init suMemInit(SU_PART_SIZE_INFO * su_part_sizes_ptr,
                     void * su_memory_pool_area)
{
    char * paddr;       /* current partition base address */
    UINT16 part_count;  /* partition counter */
    UINT16 block_count; /* block counter */
    char * pheader;     /* block header pointer */
    char * p_nextblock; /* pointer on the next memory block */
    UINT16 pre_part_block_size = 0; /* used to store the block size of
                                       the previous partition */
    UINT32 total_mem = 0;

    paddr = (char*)su_memory_pool_area;
    for (part_count = 0; part_count < SU_NPARTITION; part_count++)
    {
        /* check minimum block size */
        if ( su_part_sizes_ptr[part_count].blocksize < 16 )
            return -1;

        /* verify block size are multiple of 8 bytes */
        if ( (su_part_sizes_ptr[part_count].blocksize % 8) != 0)
            return - 1;

        suapi_pcb_ptr[part_count].blocksize = 
                                    su_part_sizes_ptr[part_count].blocksize;
        suapi_pcb_ptr[part_count].nb_blocks = 
                                    su_part_sizes_ptr[part_count].nb_blocks;

        total_mem +=
            (suapi_pcb_ptr[part_count].blocksize * 
                        suapi_pcb_ptr[part_count].nb_blocks);

        /* Verify that partition block sizes across the partitions are in
         * increasing order and that duplicate block sized partitions do
         * not exist.
         */
        if ( suapi_pcb_ptr[part_count].blocksize <= pre_part_block_size)
            return -1;
        pre_part_block_size = suapi_pcb_ptr[part_count].blocksize;

        /* Update the debug information in the current PCB */

        suapi_pcb_ptr[part_count].part_start_add = (void*) paddr;
        suapi_pcb_ptr[part_count].part_end_add =
            (void*)(paddr +
                    suapi_pcb_ptr[part_count].nb_blocks *
                    suapi_pcb_ptr[part_count].blocksize);
        suapi_pcb_ptr[part_count].free_buffers = 
                                        suapi_pcb_ptr[part_count].nb_blocks;
        suapi_pcb_ptr[part_count].min_free_buffers  = 
                                        suapi_pcb_ptr[part_count].nb_blocks;

        /* Verify the partition 8 byte alignment. */
        if(((UINT32)paddr & 0x7)!= 0)
            return -1;

        /* Initialize all the block headers */
        pheader = paddr;
        p_nextblock = NULL;
        for( block_count = 0;
             block_count < suapi_pcb_ptr[part_count].nb_blocks;
             block_count ++)
        {
            /* Set the block to free */
            ((SU_FREE_MEM_HDR*)pheader)->magic = SU_FREE_MEMORY_MAGIC;
            ((SU_FREE_MEM_HDR*)pheader)->magic_pad = SU_FREE_MEMORY_MAGIC_PAD;
            /* Update the linker list */
            *(UINT32*)(pheader + sizeof(SU_FREE_MEM_HDR)) = (UINT32)p_nextblock;
            /* Update the variable pointer on the next block */
            p_nextblock = pheader;
            /* Switch to the next block */
            pheader = pheader + suapi_pcb_ptr[part_count].blocksize;
        }
        /* Set the address of the first free block in the PCB */
        suapi_pcb_ptr[part_count].first_free_block = p_nextblock;
        /* Update the address for the next partition. */
        paddr= paddr + 
                (suapi_pcb_ptr[part_count].blocksize *
                    suapi_pcb_ptr[part_count].nb_blocks);
    }/*End of for loop*/

    return 0;
}

/*
 * DESCRIPTION: suapi_alloc_mem_ioctl
 *     This function allocates contiguous bytes of variable length
 *     memory for application use. Memory is allocated by finding the
 *     SUAPI memory partition with the smallest block size that is greater
 *     than or equal to the amount of memory requested.
 *
 *     The partition control block is indexed by the partition ID.
 *
 * INPUTS:
 *     arg: contains the requested buffer size.
 *
 * OUTPUTS:
 *     Copies back to arg a (userland) pointer to a block of the requested
 *     size. Returns ENOMEM if memory is not available.
 *
 * IMPORTANT NOTES:
 *     1. Assumes that memory partitions [0..SU_PARTITIONS] are sorted in
 *        ascending size order.
 *     2. Assumes that initialization has been performed on SUAPI memory
 *        partitions.
 */
int suapi_alloc_mem_ioctl(int arg)
{
    void *bufptr;
    unsigned int bufsize;
    int result;
    int bestfitpartition = -1;

    result = get_user(bufsize, (unsigned int *) arg);
    if (result < 0)
        return(result);

    /* Attempt to copy data to output address to ensure it is writable. */
    result = put_user(bufsize, (int *) arg);
    if (result < 0)
        return(result);

    bufptr = suapi_internal_alloc_mem(bufsize, &bestfitpartition);

    if (!IS_ERR(bufptr))
    {
        /* All pointer manipulation of SUAPI memory partitions which is done
         * inside the kernel uses kernel addresses. Bufptr must be converted to
         * user space address.
         * Assume put_user() succeeds since arg was writable above.
         */
        (void) put_user((int)SU_KERNEL_TO_USER(bufptr), (int *) arg);
    }
    else
    {
        result = PTR_ERR(bufptr);
        /* Assume put_user() succeeds since arg was writable above. */
        (void) put_user((int)bestfitpartition, (int *) arg);
    }
    return result;
}

/*
 * DESCRIPTION: suapi_internal_alloc_mem
 *     Internal function that does the real work of allocating a memory buffer.
 *
 * INPUTS:
 *     bufsize: requested size
 *
 * OUTPUTS:
 *     * bestfitpartition:
 *         When no memory is available, this variable is assigned the value of
 *         the best fit partition.
 *
 *     On success, this function returns the allocated memory pointer.
 *     When out of memory, this function panics if called from interrupt
 *         context and returns -ENOMEM otherwise.
 *     When data corruption is detected, this function panics.
 *         suPanic() will return if called from interrupt context, and this
 *         function will return -EFAULT.
 *
 * IMPORTANT NOTES:
 *     1. Calling functions should use IS_ERR() to check for a valid pointer.
 *     2. It is assumed that bestfitpartition points to valid memory.
 */
void * suapi_internal_alloc_mem(int bufsize, int *bestfitpartition)
{
    void *bufptr;
    char *free_bufptr;
    unsigned char id;         /* Partition control block index       */
    SU_ALLOC_MEM_HDR memHdr;  /* Returned memory pre header          */
    unsigned int bufsize_plus_header;
    SU_PCB pcb;

    *bestfitpartition = -1;

    bufsize_plus_header = bufsize + sizeof(SU_ALLOC_MEM_HDR);

    for (id = 0; id < SU_NPARTITION; id++)
    {
        if (bufsize_plus_header <= suapi_pcb_ptr[id].blocksize)
        {
            spin_lock_bh(&su_memory_lock);
            /* Copy PCB into local variable so that we can validate it. */
            memcpy(&pcb, &(suapi_pcb_ptr[id]), sizeof(SU_PCB));
            bufptr = (char*)(pcb.first_free_block);

            if (bufptr != NULL)
            {
                /* Validate pcb and bufptr */
                if (! SU_IN_PARTITION_RANGE(pcb.part_start_add)
                      || ! SU_IN_PARTITION_RANGE(pcb.part_end_add)
                      || ! SU_BLOCK_IN_PARTITION(bufptr,pcb) )
                {
                    spin_unlock_bh(&su_memory_lock);
                    suPanic(SU_PANIC_ALLOC_MEM_5, 3,
                            SU_SIZE_32, SU_RSET(EFAULT,SU_EFAULT),
                            SU_SIZE_8, id,
                            sizeof(SU_PCB), &pcb);
                    return ERR_PTR(-EFAULT);
                }

                /* Check a possible memory overwrite */
                if ( (((SU_FREE_MEM_HDR *)bufptr)->magic != SU_FREE_MEMORY_MAGIC)
                      || (((SU_FREE_MEM_HDR *)bufptr)->magic_pad
                                != SU_FREE_MEMORY_MAGIC_PAD) )
                {
                    spin_unlock_bh(&su_memory_lock);
                    suPanic(SU_PANIC_ALLOC_MEM_6, 4,
                            SU_SIZE_32, SU_RSET(EFAULT,SU_EFAULT),
                            SU_SIZE_8, id,
                            SU_SIZE_32, bufptr,
                            sizeof(SU_FREE_MEM_HDR), bufptr);
                    return ERR_PTR(-EFAULT);
                }

                /* verify next free block is in correct partition */
                free_bufptr = *(void **)(bufptr + sizeof(SU_FREE_MEM_HDR));
                if ( (free_bufptr != NULL)
                     && (! SU_BLOCK_IN_PARTITION(free_bufptr,pcb)) )
                {
                        spin_unlock_bh(&su_memory_lock);
                        suPanic(SU_PANIC_ALLOC_MEM_7, 5,
                                SU_SIZE_32, SU_RSET(EFAULT,SU_EFAULT),
                                SU_SIZE_8, id,
                                SU_SIZE_32, bufptr,
                                sizeof(SU_FREE_MEM_HDR), bufptr,
                                SU_SIZE_32,free_bufptr);
                        return ERR_PTR(-EFAULT);
                }

                /* Update the linked list */
                suapi_pcb_ptr[id].first_free_block = free_bufptr;

                /* Break the link with the other free buffers */
                *(void **)(bufptr + sizeof(SU_FREE_MEM_HDR)) = 0;

                /* Update the debug information */
                pcb.free_buffers --;
                if ( pcb.free_buffers < pcb.min_free_buffers)
                {
                    /* Update in the real structure, not the copy. */
                    suapi_pcb_ptr[id].min_free_buffers = pcb.free_buffers;
                }
                suapi_pcb_ptr[id].free_buffers = pcb.free_buffers;

                spin_unlock_bh(&su_memory_lock);

                memHdr.partID = id;
                memHdr.taskID = SU_COMPRESS_RTOS_TASKID(current->pid);
                memHdr.nbytes = bufsize;
                memHdr.magic_pad1 = SU_ALLOC_MEMORY_MAGIC_PAD; 

                *((SU_ALLOC_MEM_HDR *)bufptr) = memHdr;
                bufptr += sizeof(SU_ALLOC_MEM_HDR);

                return bufptr;
            }

            spin_unlock_bh(&su_memory_lock);

            /* Update bestfitpartition variable. */
            if (*bestfitpartition == -1)
            {
                *bestfitpartition = id;
            }

            /*
             * When here, the best fit partition is exhausted. Break unless
             * memory promotion is configured on.
             */
            if (SU_MEMORY_PROMOTION == 0)
            {
                break;
            }
        }
    }

    /* When here, failed to allocate memory. */
    return ERR_PTR(-ENOMEM);
}

/*
 * DESCRIPTION: suapi_free_mem_ioctl
 *
 *     This function frees memory previously allocated by suAllocMem().
 *
 * INPUTS:
 *     arg contains the (user space) address of the memory to be freed.
 *
 * OUTPUTS:
 *     The memory pointed to by bufptr is returned to the system.
 *
 *
 * IMPORTANT NOTES:
 *     1. The use of previously freed memory may have unpredictable results.
 *     2. suapi_internal_free_mem() panics if it encounters a duplicate free or
 *        memory corruption.
 */
int suapi_free_mem_ioctl(void * user_bufptr)
{
    void * kmem_p;

    /* Silently handle null user_bufptr, requested by user. */
    if (user_bufptr == (void *)0)
        return 0;

    /* Convert user_bufptr to kernel address of mapped SUAPI memory buffer. */
    kmem_p = SU_USER_TO_KERNEL(user_bufptr);

    suapi_internal_free_mem(kmem_p, NULL);
    return 0;
}

/*
 * DESCRIPTION: suapi_internal_free_mem
 *     Internal function that does the real work of freeing a memory buffer.
 *
 * INPUTS:
 *     bufptr: address of the memory to be freed
 *     lock:    If not NULL, this parameter contains a pointer to a spin lock
 *              structure to be unlocked if a panic occurs.
 *
 * OUTPUTS:
 *     When duplicate free is detected, this function panics.
 *     If bufptr is invalid or corrupted, panics.
 *     If pcb is invalid, this function panics.
 *     If magic pad of next block is corrupted, this function panics.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
void suapi_internal_free_mem(void * bufptr, spinlock_t *lock)
{
    SU_ALLOC_MEM_HDR memHdr, *memhdr_p;
    SU_FREE_MEM_HDR *memFreeHdr;
    SU_PCB pcb;

    if (! SU_IN_PARTITION_RANGE(bufptr))
    {
        if (lock)
            spin_unlock(lock);
        suPanic(SU_PANIC_FREE_MEM_3, 2,
                SU_SIZE_32, SU_RSET(EINVAL,SU_EINVPARAM),
                SU_SIZE_32, bufptr);
        return;
    }

    /*
     * Make freeing memory an atomic operation. Don't allow an
     * ISR or another task to free memory after we've obtained the
     * partition id or we'd free memory twice without detection.
     */
    spin_lock_bh(&su_memory_lock);

    /*
     * The partition ID for the memory is stored in the header preceding
     * the buffer.
     */
    memhdr_p = (SU_ALLOC_MEM_HDR *)bufptr - 1;
    memHdr = *memhdr_p;
    memFreeHdr = (SU_FREE_MEM_HDR *)memhdr_p;

    /* Try to determine if this is a duplicate free. */
    if (memFreeHdr->magic == SU_FREE_MEMORY_MAGIC)
    {
        spin_unlock_bh(&su_memory_lock);
        if (lock)
            spin_unlock(lock);
        suPanic(SU_PANIC_FREE_MEM_4, 3,
                SU_SIZE_32, SU_RSET(EIDRM,SU_EDELETED),
                SU_SIZE_32, bufptr,
                sizeof(SU_ALLOC_MEM_HDR), &memHdr);
        return;
    }

    /* Try to determine if a memory overwrite occurred. */
    if ( (memHdr.magic_pad1 != SU_ALLOC_MEMORY_MAGIC_PAD)
         || memHdr.partID >= SU_NPARTITION )
    {
        spin_unlock_bh(&su_memory_lock);
        if (lock)
            spin_unlock(lock);
        suPanic(SU_PANIC_FREE_MEM_4, 3,
                SU_SIZE_32, SU_RSET(EINVAL, SU_EINVPARAM),
                SU_SIZE_32, bufptr,
                sizeof(SU_ALLOC_MEM_HDR), &memHdr);
        return;
    }

    memcpy(&pcb, &(suapi_pcb_ptr[memHdr.partID]), sizeof(SU_PCB));
    if (! SU_IN_PARTITION_RANGE(pcb.part_start_add)
        || ! SU_IN_PARTITION_RANGE(pcb.part_end_add)
        || ((pcb.first_free_block != NULL)
            && (! SU_BLOCK_IN_PARTITION(pcb.first_free_block,pcb) ) ) )
    {
        spin_unlock_bh(&su_memory_lock);
        if (lock)
            spin_unlock(lock);
        suPanic(SU_PANIC_FREE_MEM_5, 3,
                SU_SIZE_32, SU_RSET(EFAULT, SU_EFAULT),
                SU_SIZE_16, memHdr.partID,
                sizeof(SU_PCB), &pcb);
        return;
    }

    /* Check if the  buffer belongs to the partition. */
    if (! SU_BLOCK_IN_PARTITION(memhdr_p,pcb))
    {
        spin_unlock_bh(&su_memory_lock);
        if (lock)
            spin_unlock(lock);
        suPanic(SU_PANIC_FREE_MEM_6, 4,
                SU_SIZE_32, SU_RSET(EINVAL,SU_EINVPARAM),
                SU_SIZE_16, memHdr.partID,
                SU_SIZE_32, bufptr,
                sizeof(SU_PCB), &pcb);
        return;
    }

    /*
     * Check header of next block in partition to see if it was
     * overwritten or corrupted.
     *
     * Both alloc mem and free mem hdr have magic_pad size half word.
     *
     * Can't check the last block in the partition since we would be either
     * checking the block in the next partition or if this is the last partition
     * then we would be checking memory not in any partition.  
     */

     if (  ((void *)memhdr_p != (suapi_pcb_ptr[memHdr.partID].part_end_add - suapi_pcb_ptr[memHdr.partID].blocksize)) &&
           (((SU_ALLOC_MEM_HDR*)((unsigned int)memhdr_p + suapi_pcb_ptr[memHdr.partID].blocksize))->magic_pad1 != SU_FREE_MEMORY_MAGIC_PAD) &&
           (((SU_ALLOC_MEM_HDR*)((unsigned int)memhdr_p + suapi_pcb_ptr[memHdr.partID].blocksize))->magic_pad1 != SU_ALLOC_MEMORY_MAGIC_PAD) )
    {
        spin_unlock_bh(&su_memory_lock);
        if (lock)
            spin_unlock(lock);
        suPanic(SU_PANIC_FREE_MEM_4, 3,
                SU_SIZE_32, SU_RSET(EFAULT, SU_EFAULT),
                SU_SIZE_32, bufptr,
                sizeof(SU_ALLOC_MEM_HDR), &memHdr);

        return;
    } 



    /* Update the debug information */
    suapi_pcb_ptr[memHdr.partID].free_buffers ++;

    /*
     * Save the information for debugging.
     */
    memFreeHdr->allocatetaskID = memHdr.taskID;
    memFreeHdr->freetaskID = SU_COMPRESS_RTOS_TASKID(current->pid);

    memFreeHdr->magic = SU_FREE_MEMORY_MAGIC;
    memFreeHdr->magic_pad = SU_FREE_MEMORY_MAGIC_PAD;

    /* Update the linked list of free blocks */
    *(void **)bufptr = pcb.first_free_block;

    /* Store freed buffer in free list head of real PCB, not in the copy. */
    suapi_pcb_ptr[memHdr.partID].first_free_block = (void*)memhdr_p;

    spin_unlock_bh(&su_memory_lock);

    return;
}
