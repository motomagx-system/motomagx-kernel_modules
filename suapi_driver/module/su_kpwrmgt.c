// Copyright (C) 2006, 2007 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI power management services within the
 *      SUAPI kernel module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#ifdef CONFIG_MOT_FEAT_PM
#include <linux/mpm.h>
#endif

#include <linux/suapi_module.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/

/*
 * DESCRIPTION: suapi_probe
 *     The probe function is intended for a module with real H/W to go
 *     look for it. Since SUAPI has no H/W, there is nothing to
 *     do. But apparantly power management system will not suspend us
 *     if we aren't defining a probe function.
 * INPUTS:
 *     dev: device structure pointer
 *
 * OUTPUTS:
 *     Returns 0 for success.
 *
 * IMPORTANT NOTES:
 *     N/A
 */

int suapi_probe(struct device *dev)
{
#ifdef KPP_DEBUG
        printk("Probe function called.\n");
#endif
        return 0;
}


/*
 * DESCRIPTION: suapi_suspend
 *     The suspend function is intended for a module with real H/W to shut
 *     it down. Since SUAPI has no H/W, there is nothing to
 *     do. But SUAPI does have applications that depend on being able
 *     to block the suspension and this is the only place that does so
 *     without race conditions.
 *
 *     The Power Management system calls a device's registered suspend
 *     function three times during suspending with interrupts enabled
 *     or without interrupts enabled. The first set of suspend calls are
 *     made with interrupts enabled. If EAGAIN is returned, the
 *     triplet of calls are later made with interrupts
 *     disabled. Otherwise, only the first triplet of calls are made.
 *
 * INPUTS:
 *     dev: device structure pointer - unused by SUAPI
 *     state: power management state - unused by SUAPI
 *     level: power management level - unused by SUAPI
 *
 * OUTPUTS:
 *     Returns:
 *        0      - suspension OK to proceed
 *        EBUSY  - suspension can not proceed as device is in use
 *        EAGAIN - check again with interrupts disabled
 *
 * IMPORTANT NOTES:
 *     Locks are not always held. Thus a SUAPI service to disabled low
 *     power could be in progress but the green light for suspension
 *     is only given with interrupts disabled and so the application
 *     will not be given control again thinking that it had disabled
 *     low power until after the phone wakes up. This is further
 *     minimized as the system must be idle before trying to go to
 *     sleep.
 *
 *     The other case is that a SUAPI service to enabled low power is
 *     in progress but we errantly prevent going to sleep. Again this
 *     is OK as going to sleep will be retried and the system must be
 *     idle before trying to go to sleep.
 */

int suapi_suspend(struct device *dev, u32 state, u32 level)
{

    if (su_low_power_flags_allocation & su_low_power_flags_setting)
        return -EBUSY;

    if (!irqs_disabled())
        return -EAGAIN;

    return 0;
}

/*
 * DESCRIPTION: suapi_resume
 *     The resume function is intended for a module with real H/W to power
 *     it up. Since SUAPI has no H/W, there is nothing to do.
 *
 *     The Power Management system calls a device's registered resume
 *     function three times during resuming. SUAPI can't actually
 *     suspend, so does nothing.
 *
 * INPUTS:
 *     dev: device structure pointer - unused by SUAPI
 *     level: power management level - unused by SUAPI
 *
 * OUTPUTS:
 *     Returns:
 *        0      - resume OK to proceed
 *
 * IMPORTANT NOTES:
 *     N/A
 */

int suapi_resume(struct device *dev, u32 level)
{
    return 0;
}

/*
 * The SUAPI low power flags are 32 bits stored in two words. One word
 * is the allocation word where each bit is 0 = unused or 1 =
 * in-use. The other word is the setting word where each bit is 0 =
 * low-power OK or 1 = low-power not OK.
 *
 * Low power is OK if the AND of the two words results in 0.
 *
 * Writing to the words must be protected by locking.
 *
 * The final passes of SUAPI PM suspend routine reads the words with interrupts
 * disabled to prevent race conditions with the low power flags services. The
 * first pass doesn't but that's OK as it errors on the side of preventing
 * low power mode.
 *
 * We avoid disabling interrupts in the write functions by ensuring
 * the allocation bit is not set until the setting bit is
 * valid. The write functions are only invoked from user space, so no
 * ISR concerns either.
 */

/*
 * The lock su_low_power_flags_lock protects writing to the low power
 * flags.
 */

DEFINE_SPINLOCK(su_low_power_flags_lock);


/*
 * DESCRIPTION: suapi_notify_pm
 *     The notify function handles notifying power management that
 *     there is a change in status of the driver regarding whether it
 *     can be suspended.
 *
 * INPUTS:
 *     None.
 *
 * OUTPUTS:
 *     None.
 *
 * IMPORTANT NOTES:
 *     Lock is held by caller while giving advice to prevent race
 *     condition where advice bit gets stuck "disabled".
 */

static void suapi_notify_pm(void)
{
#if defined(MPM_ADVICE_DRIVER_IS_BUSY)
    int what_advice;

    if (su_low_power_flags_allocation & su_low_power_flags_setting)
        what_advice = MPM_ADVICE_DRIVER_IS_BUSY;
    else
        what_advice = MPM_ADVICE_DRIVER_IS_NOT_BUSY;

    if (suLowPower_advice_driver_num >= 0)
        (void) mpm_driver_advise(suLowPower_advice_driver_num,what_advice);
#endif
}

/*
 * DESCRIPTION: suapi_alloc_low_power_flag_ioctl
 *     This service allocates one of the SUAPI low power flags to the
 *     caller and initializes it to the desired state.
 *
 * INPUTS:
 *     initial_state: 0 - allow suspension, 1 - prevent suspension
 *
 * OUTPUTS:
 *     Returns:
 *        0-31   : index of the low power flag allocated
 *        EINVAL : error indicating invalid initial_state
 *        ENOSPC : error indicating out of flags
 *
 * IMPORTANT NOTES:
 *     Lock is held when searching for a free flag to prevent two
 *     callers from getting the same flag. Lock is also held while
 *     the flag is initialized to keep other writers changes to the
 *     word from getting lost.
 *
 *     Lock is held while giving advice to prevent race condition
 *     where advice bit gets stuck "disabled".
 */

int suapi_alloc_low_power_flag_ioctl(int initial_state)
{
    int rvalue;

    if (1 < (unsigned int) initial_state)
        return -EINVAL;

    spin_lock(&su_low_power_flags_lock);

    if (0 == ~su_low_power_flags_allocation)
    {
        spin_unlock(&su_low_power_flags_lock);
        return(-ENOSPC);
    }

    rvalue = ffz(su_low_power_flags_allocation);

    SU_SAVE_LPWR_ALLOCATOR(rvalue);
    SU_SAVE_LPWR_SETTER(rvalue);

    su_low_power_flags_setting |= ((unsigned int) initial_state) << rvalue;
    su_low_power_flags_allocation |= 1UL << rvalue;
    suapi_notify_pm();

    spin_unlock(&su_low_power_flags_lock);

    return(rvalue);
}

/*
 * DESCRIPTION: suapi_free_low_power_flag_ioctl
 *     This service frees a SUAPI low power flag allocated to the
 *     caller and initializes it to "enabled".
 *
 * INPUTS:
 *     arg : number of the allocated flag
 *
 * OUTPUTS:
 *     Returns:
 *        0      : success
 *        EFBIG  : the argument is outside the range of possible flags
 *        EINVAL : the specified flag is not allocated
 *
 * IMPORTANT NOTES:
 *     Lock is held when checking the allocation of the flag to
 *     prevent two callers from freeing the same flag. Lock is also
 *     held while freeing flag and clearing its setting to keep other
 *     writers changes to the word from getting lost.
 *
 *     Lock is held while giving advice to prevent race condition
 *     where advice bit gets stuck "disabled".
 */

int suapi_free_low_power_flag_ioctl(int arg)
{
    su_low_power_flag_t mask;

    if (SU_NUM_LPWR_FLAGS <= (unsigned int) arg)
        return(-EFBIG);

    mask = 1UL << arg;

    spin_lock(&su_low_power_flags_lock);

    if (0 == (mask & su_low_power_flags_allocation))
    {
        spin_unlock(&su_low_power_flags_lock);
        return(-EINVAL);
    }

    SU_SAVE_LPWR_ALLOCATOR(arg);

    su_low_power_flags_setting &= ~mask;
    su_low_power_flags_allocation &= ~mask;

    suapi_notify_pm();

    spin_unlock(&su_low_power_flags_lock);


    return(0);
}

/*
 * DESCRIPTION: suapi_disable_low_power_flag_ioctl
 *     This service disables a SUAPI low power flag allocated to the
 *     caller and advises the MPM.
 *
 * INPUTS:
 *     arg : number of the allocated flag
 *
 * OUTPUTS:
 *     Returns:
 *        0      : success
 *        EFBIG  : the argument is outside the range of possible flags
 *        EINVAL : the specified flag is not allocated
 *
 * IMPORTANT NOTES:
 *     Lock is held when checking the allocation of the flag to
 *     prevent another caller from freeing the flag while we are
 *     modifying it. Lock is also held while modifying flag setting
 *     to keep other writers changes to the setting word from getting lost.
 *
 *     Lock is held while giving advice to prevent race condition
 *     where advice bit gets stuck "disabled".
 */

int suapi_disable_lower_power_flag(int arg)
{
    su_low_power_flag_t mask;

    if (SU_NUM_LPWR_FLAGS <= (unsigned int) arg)
        return(-EFBIG);

    mask = 1UL << arg;

    spin_lock(&su_low_power_flags_lock);

    if (0 == (mask & su_low_power_flags_allocation))
    {
        spin_unlock(&su_low_power_flags_lock);
        return(-EINVAL);
    }

    SU_SAVE_LPWR_SETTER(arg);

    su_low_power_flags_setting |= mask;
    suapi_notify_pm();

    spin_unlock(&su_low_power_flags_lock);

    return(0);
}

/*
 * DESCRIPTION: suapi_enable_low_power_flag_ioctl
 *     This service enables a SUAPI low power flag allocated to the
 *     caller and advises the MPM.
 *
 * INPUTS:
 *     arg : number of the allocated flag
 *
 * OUTPUTS:
 *     Returns:
 *        0      : success
 *        EFBIG  : the argument is outside the range of possible flags
 *        EINVAL : the specified flag is not allocated
 *
 * IMPORTANT NOTES:
 *     Lock is held when checking the allocation of the flag to
 *     prevent another caller from freeing the flag while we are
 *     modifying it. Lock is also held while modifying flag setting
 *     to keep other writers changes to the setting word from getting lost.
 *
 *     Lock is held while giving advice to prevent race condition
 *     where advice bit gets stuck "disabled".
 */

int suapi_enable_lower_power_flag(int arg)
{
    su_low_power_flag_t mask;

    if (SU_NUM_LPWR_FLAGS <= (unsigned int) arg)
        return(-EFBIG);

    mask = 1UL << arg;

    spin_lock(&su_low_power_flags_lock);

    if (0 == (mask & su_low_power_flags_allocation))
    {
        spin_unlock(&su_low_power_flags_lock);
        return(-EINVAL);
    }

    SU_SAVE_LPWR_SETTER(arg);

    su_low_power_flags_setting &= ~mask;

    suapi_notify_pm();

    spin_unlock(&su_low_power_flags_lock);

    return(0);
}

/*
 * DESCRIPTION: suapi_get_low_power_flag_ioctl
 *     This service returns whether it is OK to suspend based on the
 *     SUAPI low power flags. Since the MPM is a kernel module, the
 *     .suspend function is actually used when attempting to suspend.
 *
 * INPUTS:
 *     arg : UNUSED
 *
 * OUTPUTS:
 *     Returns:
 *        0      : SUAPI would not prevent suspension
 *        EBUSY  : SUAPI would prevent suspension
 *
 * IMPORTANT NOTES:
 *     Lock is held when checking the allocation of the flag to
 *     prevent another caller from freeing the flag while we are
 *     looking at it.
 *
 */

int suapi_get_lower_power_flag(int arg)
{
    int retval = 0;

    spin_lock(&su_low_power_flags_lock);

    if (su_low_power_flags_allocation & su_low_power_flags_setting)
        retval = -EBUSY;

    spin_unlock(&su_low_power_flags_lock);

    return(retval);
}
