// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI semaphore services within the SUAPI
 *      kernel module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>

#include <linux/suapi_module.h>


/************** LOCAL CONSTANTS **********************************************/
#define SU_SEM_UNOWNED ((pid_t) -1)

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/
/* This implementation uses SUAPI internal semaphore structures for mutexes if
 * either PREEMPT_RT or DOWN_TIMEOUT configuration is not set. Therefore, must
 * use SUAPI internal functions if using SUAPI internal structures.
 */
#if defined(CONFIG_PREEMPT_RT) && defined(CONFIG_MOT_FEAT_DOWN_TIMEOUT)
/* SUAPI_SEMA_INIT is only used for mutexes under these conditions */
#define SUAPI_SEMA_INIT sema_init
/* SUAPI_SEMA_INIT_NOCHECK is only used for non-mutexes under these conditions */
#define SUAPI_SEMA_INIT_NOCHECK su_sema_init
#define SUAPI_SEMA_DOWN(sptr,expires) \
        ((sptr->type == SU_SEMA_MUTEX) \
          ? down_interruptible_timeout(&(sptr->s.mutex.sem),expires) \
          : su_down_interruptible_timeout(&(sptr->s.bin_count.sem),expires))
#define SUAPI_SEMA_UP(sptr) \
        ((sptr->type == SU_SEMA_MUTEX) \
          ? up(&(sptr->s.mutex.sem)) \
          : su_up(&(sptr->s.bin_count.sem)))
#define SUAPI_SEMA_COUNT(sptr) \
        ((sptr->type == SU_SEMA_MUTEX) \
          ? sema_count(&(sptr->s.mutex.sem)) \
          : su_sema_count(&(sptr->s.bin_count.sem)))
#else
#define SUAPI_SEMA_INIT su_sema_init
#define SUAPI_SEMA_INIT_NOCHECK su_sema_init_nocheck
#define SUAPI_SEMA_DOWN(sptr,expires) \
        ((sptr->type == SU_SEMA_MUTEX) \
          ? su_down_interruptible_timeout(&(sptr->s.mutex.sem),expires) \
          : su_down_interruptible_timeout(&(sptr->s.bin_count.sem),expires))
#define SUAPI_SEMA_UP(sptr) \
        ((sptr->type == SU_SEMA_MUTEX) \
          ? su_up(&(sptr->s.mutex.sem)) \
          : su_up(&(sptr->s.bin_count.sem)))
#define SUAPI_SEMA_COUNT(sptr) \
        ((sptr->type == SU_SEMA_MUTEX) \
          ? su_sema_count(&(sptr->s.mutex.sem)) \
          : su_sema_count(&(sptr->s.bin_count.sem)))
#endif

/************** LOCAL VARIABLES **********************************************/

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
/* num_free_binary, num_free_counting, and num_free_mutex contain the current
 * number of binary, counting, and mutex semaphores which are not in use.
 */
atomic_t num_free_binary;
atomic_t num_free_counting;
atomic_t num_free_mutex;

/* semarray[] contains pointers to all three types of semaphores: binary,
 * counting, and mutex. Its size is determined by adding the total number
 * of each type as specified in the system configuraiton file ( which is
 * /etc/suapi/suapi.su_sysconfig, by default).
 */
struct SuapiSemArray * semarray = NULL;

/************** GLOBAL EXPORT DEFINITIONS ************************************/

/************** FUNCTION DEFINITIONS *****************************************/
int suapi_sem_create_ioctl(int type, int arg)
{
    struct suIoctlCreateSemArgs iargs;
    unsigned short index;
    struct SuapiSemaphore * sptr;
    atomic_t * num_sems;
    int result;

    if (copy_from_user(&iargs,
                       (struct suIoctlCreateSemArgs *) arg,
                       sizeof(struct suIoctlCreateSemArgs)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (copy_to_user((struct suIoctlCreateSemArgs *) arg,
                             &iargs,
                             sizeof(struct suIoctlCreateSemArgs)))
        return -EFAULT;

    switch (type)
    {
        case SU_SEMA_COUNTING:
            /* If creating a counting semaphore, fail if the initial count is
             * greater than the upper bound.
             */
            if (iargs.s.bound > 0 && iargs.initial_count > iargs.s.bound)
                return -EINVAL;
            num_sems = &num_free_counting;
            break;

        case SU_SEMA_BINARY:
        case SU_SEMA_MUTEX:
            /* If creating either a binary or a mutex semaphore, ensure that
             * initial count is either locked or unlocked.
             */
            if (iargs.initial_count != 1 && iargs.initial_count != 0)
                return -EINVAL;

            if (type == SU_SEMA_BINARY)
                num_sems = &num_free_binary;
            else
                num_sems = &num_free_mutex;
            break;
        default:
            return -EINVAL;
            break;
    }

    sptr = kmalloc(sizeof(struct SuapiSemaphore), GFP_KERNEL);
    if (sptr == NULL)
        return -ENOMEM;

    /* num_sems is a pointer to the variable containing the total number of
     * available semaphores  of the specified type. Atomically decrement the
     * semaphore count.
     */
    while (atomic_sub_return(1,num_sems) >= 0)
    {
        /* Enter critical section when a free entry is found. */
        for (index = 0; index < SU_NUM_SEMAPHORES; index++)
        {
            if (semarray[index].sem == NULL)
            {
                spin_lock(&su_lock);
                /* Check to see if the entry is still free. */
                if (semarray[index].sem == NULL)
                {
                    semarray[index].sem = sptr;
                    /* It's safe to unlock now because this entry isn't free. */
                    spin_unlock(&su_lock);
                    sptr->wait_count = 0;
                    sptr->type = type;

                    /* If counting, bound is passed in. If binary, bound is 1.
                     * Mutexes don't have a bound.
                     */
                    if (type == SU_SEMA_COUNTING)
                        sptr->s.bin_count.bound = iargs.s.bound;
                    else if (type == SU_SEMA_BINARY)
                        sptr->s.bin_count.bound = 1;
                    /* If mutex, initialize nest count and owner. */
                    else /* (type == SU_SEMA_MUTEX) */
                    {
                        if (iargs.initial_count == 0) /* locked */
                        {
                            sptr->s.mutex.nestcount = 1;
                            sptr->s.mutex.ownerpid = current->pid;
                        }
                        else /* unlocked */
                        {
                            sptr->s.mutex.nestcount = 0;
                            sptr->s.mutex.ownerpid = SU_SEM_UNOWNED;
                        }
                    }

                    /* Initialize lock state. */
                    if (type == SU_SEMA_MUTEX)
                        SUAPI_SEMA_INIT(&(sptr->s.mutex.sem),
                                        iargs.initial_count);
                    else
                        SUAPI_SEMA_INIT_NOCHECK(
                            &(sptr->s.bin_count.sem),
                            iargs.initial_count);

                    iargs.s.shandle = SUAPI_CREATE_SHANDLE(
                                        semarray[index].deletions,
                                        index);

                    /* Copy the newly created semaphore handle to user space.
                     * Assumes arg still writable.
                     */
                    result = copy_to_user((struct suIoctlCreateSemArgs *) arg,
                                          &iargs,
                                          sizeof(struct suIoctlCreateSemArgs));

                    return result;
                } /* end of if entry is still free after lock */

                /* If get here, a race occured which caused the entry to no
                 * longer be free, so unlock and go on to next entry.
                 */
                spin_unlock(&su_lock);
            } /* end of if entry is free before locking */
        } /* end of for loop */

        /* There must have been some other race after decrementing the sem
         * count so start over.
         */
        atomic_add(1, num_sems);
    }

    /* If we get here, there are no more semaphores available. Free the semaphore
     * ptr and return.
     */
    kfree (sptr);
    atomic_add(1, num_sems);
    return -ENOSPC;
}

int suapi_sem_acquire_ioctl(int arg)
{
    union SuapiSemAcquireArgs iargs;
    signed long expires;
    unsigned short deletions, index;
    int retval;
    struct SuapiSemaphore * sptr;

    if (copy_from_user(&iargs,
                       (union SuapiSemAcquireArgs *) arg,
                       sizeof(union SuapiSemAcquireArgs)))
        return -EFAULT;

    if (iargs.input.timeout >= LONG_MAX)
    {
        expires = MAX_SCHEDULE_TIMEOUT;
    }
    else if (iargs.input.timeout == 0)
    {
        expires = 0;
    }
    else
    {
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
        expires = iargs.input.timeout;

        if (iargs.input.timeout < 0)
        {
            return -ERANGE;
        }
#else
        expires = msecs_to_jiffies(iargs.input.timeout);

        if ( (iargs.input.timeout < 0) ||
             (expires > MAX_SCHEDULE_TIMEOUT) )
        {
            return -ERANGE;
        }
#endif
    }

    deletions = SUAPI_GET_SEM_DELETIONS(iargs.input.shandle);
    index = SUAPI_GET_SEM_INDEX(iargs.input.shandle);

    spin_lock(&su_lock);

    if (index >= SU_NUM_SEMAPHORES
        || semarray[index].deletions != deletions
        || semarray[index].sem == NULL)
    {
        spin_unlock(&su_lock);
        return -ENOENT;
    }

    sptr = semarray[index].sem;

    if ((sptr->type == SU_SEMA_MUTEX) && (sptr->s.mutex.ownerpid == current->pid))
    {
        sptr->s.mutex.nestcount++;
        spin_unlock(&su_lock);
        return 0;
    }

    sptr->wait_count++;

    spin_unlock(&su_lock);

    /* down_interruptible_timeout() will return 0 if successfully
     * acquired the lock, -EAGAIN if the timeout period is 0 and the
     * lock cannot be acquired, -ETIMEDOUT if the timeout expired while
     * waiting for the lock, and -EINTR if the wait was interrupted.
     */
    retval = SUAPI_SEMA_DOWN(sptr, expires);

    spin_lock(&su_lock);
    sptr->wait_count--;
    if (semarray[index].deletions == deletions)
    {
        if ((sptr->type == SU_SEMA_MUTEX) && retval == 0)
        {
            sptr->s.mutex.nestcount = 1;
            sptr->s.mutex.ownerpid = current->pid;
        }

        spin_unlock(&su_lock);
        return retval;
    }
    /* Fall through to handle semaphore deleted case. */

    /* If here, the semaphore was deleted before it could be acquired.
     * If this is the last thread using the semaphore, delete the memory
     * associated with the semaphore.
     */
    if (sptr->wait_count <= 0)
    {
        /* If this is a MUTEX and it is held, release it so that the owner's
         * task structure can be cleaned up of dangling waiter and priority
         * inheritance lists. It's okay to release a mutex even if I'm not
         * the owner because it's being deleted and the structure is going
         * away.
         */
        if (sptr->type == SU_SEMA_MUTEX && SUAPI_SEMA_COUNT(sptr) == 0)
            SUAPI_SEMA_UP(sptr);
        kfree (sptr);
    }
    else
    {
        SUAPI_SEMA_UP(sptr);
    }

    spin_unlock(&su_lock);
    return -EIDRM;
}

int suapi_sem_release_ioctl(int arg)
{
    union SuapiReleaseSemArgs iargs;
    unsigned short deletions, index;
    struct SuapiSemaphore * sptr;
    int result;

    if (copy_from_user(&iargs,
                       (union SuapiReleaseSemArgs *) arg,
                       sizeof(union SuapiReleaseSemArgs)))
        return -EFAULT;

    deletions = SUAPI_GET_SEM_DELETIONS(iargs.shandle);
    index = SUAPI_GET_SEM_INDEX(iargs.shandle);

    spin_lock(&su_lock);

    if (index >= SU_NUM_SEMAPHORES
        || semarray[index].deletions != deletions
        || semarray[index].sem == NULL)
    {
        spin_unlock(&su_lock);
        return -ENOENT;
    }

    sptr = semarray[index].sem;
    if (sptr->type == SU_SEMA_MUTEX)
    {
        if (sptr->s.mutex.ownerpid != current->pid)
        {

            iargs.mtx.type = sptr->type;
            iargs.mtx.ownpid = sptr->s.mutex.ownerpid;
            iargs.mtx.nestcount = sptr->s.mutex.nestcount;

            spin_unlock(&su_lock);

            /* Assumes output address is still writable. Don't need to undo. */
            result = copy_to_user((union SuapiReleaseSemArgs *) arg,
                                  &iargs,
                                  sizeof(union SuapiReleaseSemArgs));

            /* copy_to_user() failure trumps -EACCESS error. */
            if (result == 0) result = -EACCES;
            return result;
        }

        if (sptr->s.mutex.nestcount > 0)
        {
            sptr->s.mutex.nestcount--;
        }

        if (sptr->s.mutex.nestcount <= 0)
        {
            sptr->s.mutex.ownerpid = SU_SEM_UNOWNED;
            SUAPI_SEMA_UP(sptr);
        }
        spin_unlock(&su_lock);
    }
    else
    {
        if ((sptr->s.bin_count.bound > 0)
             && (SUAPI_SEMA_COUNT(sptr) >= sptr->s.bin_count.bound))
        {
            iargs.cntg.type = sptr->type;
            iargs.cntg.count = SUAPI_SEMA_COUNT(sptr);
            iargs.cntg.bound = sptr->s.bin_count.bound;

            spin_unlock(&su_lock);

            /* Assumes output address is still writable. */
            result = copy_to_user((union SuapiReleaseSemArgs *) arg,
                                  &iargs,
                                  sizeof(union SuapiReleaseSemArgs));

            /* copy_to_user() failure trumps -ERANGE error. */
            if (result == 0) result = -ERANGE;
            return result;
        }

        /* Keep spin lock held across both the bounds check and the "up" so
         * that a preemption can't happen at this point, release the same
         * semaphore, come back and release it again. This would cause the
         * count to become one greater than it should be.
         */
        SUAPI_SEMA_UP(sptr);
        spin_unlock(&su_lock);
    }
    return 0;
}

int suapi_sem_delete(int arg)
{
    unsigned short deletions, index;
    unsigned int shandle = (unsigned int) arg;
    struct SuapiSemaphore * sptr;

    deletions = SUAPI_GET_SEM_DELETIONS(shandle);
    index = SUAPI_GET_SEM_INDEX(shandle);

    spin_lock(&su_lock);

    if (index >= SU_NUM_SEMAPHORES
        || semarray[index].deletions != deletions
        || semarray[index].sem == NULL)
    {
        spin_unlock(&su_lock);
        return -ENOENT;
    }

    /* Increment semaphore's deletion count again so semaphore handle won't
     * equal SU_INVALID_HANDLE.
     */
    if (++(semarray[index].deletions) == 0)
        ++(semarray[index].deletions);

    switch (semarray[index].sem->type)
    {
        case SU_SEMA_BINARY :
            atomic_add(1, &num_free_binary);
            break;
        case SU_SEMA_COUNTING :
            atomic_add(1, &num_free_counting);
            break;
        case SU_SEMA_MUTEX :
            atomic_add(1, &num_free_mutex);
            break;
    }

    sptr = semarray[index].sem;
    semarray[index].sem = NULL;

    spin_unlock(&su_lock);

    /* If there are no waiters on this semaphore, just delete it. However, if
     * there are waiters, wake them up. The last waiter will delete the
     * space associated with the semaphore.
     */
    if (sptr->wait_count <= 0)
    {
        /* If this is a MUTEX and it is held, release it so that the owner's
         * task structure can be cleaned up of dangling waiter and priority
         * inheritance lists. It's okay to release a mutex even if I'm not
         * the owner because it's being deleted and the structure is going
         * away.
         */
        if (sptr->type == SU_SEMA_MUTEX && SUAPI_SEMA_COUNT(sptr) == 0)
            SUAPI_SEMA_UP(sptr);
        kfree(sptr);
    }
    else
    {
        SUAPI_SEMA_UP(sptr);
    }

    return 0;
}
