// Copyright (C) 2006-2008, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.


/*
 *  DESCRIPTION:
 *      This file implements the datalogger portion of the SUAPI kernel module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-14 Motorola       Initialize spinlock log_buffer_lock.
// 2006-11-16 Motorola       Make SUAPI build for x86 for smobee.
// 2007-01-24 Motorola       Test Watchdog panic.
// 2007-02-05 Motorola       Add supanic ioctl to cause core dump.
// 2007-03-09 Motorola       Fix double reboot of watchdog panic.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM
// 2008-01-09 Motorola       Make previous_panic global.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#if !defined(CONFIG_ARM)
#include <linux/interrupt.h>
#endif
#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>
#include <linux/fs.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>
#include <linux/list.h>
#include <linux/stat.h>
#include <linux/workqueue.h>

#ifdef CONFIG_MOT_POWER_IC_ATLAS
#include <linux/power_ic.h>
#endif

#include <linux/suapi_module.h>
#include <linux/su_pallog.h>
#include <linux/su_panic_codes.h>

/*==========================================
 * Mot Security Settings
 *=========================================*/
#ifdef CONFIG_MOT_FEAT_OTP_EFUSE
#include <linux/ks_otp_api.h>
#endif /* CONFIG_MOT_FEAT_OTP_EFUSE */

#define LOG_BUFFER_SIZE 65536

/* Maximum size of each "flip" file. */
#define MAX_FILE_SIZE 65536

/* These create a "flip" file - when one fills up, the other is overwritten. */
#define FLIP_FILE_A "/ezxlocal/panic/supanic_block.a"
#define FLIP_FILE_B "/ezxlocal/panic/supanic_block.b"

/* These macros implement the circular buffer access count. */
#define SET_BUSY(what) (what--)
#define SET_IDLE(what) (what++)

/* This is the name of the device created to read panic data. */
#define SU_PANIC_DEV_NAME "supanic"

/* Defines the maximum length of a panic block. */
#define PANIC_MAX_BLOCK 65535

/* Defines the maximum size of the data portion of a logger message. */
#define LOG_MAX_MSG_LEN (65535 - SIZE_HEADER_LOG_DATA)

/* Message ID unique to panic indication messages for SUAPI on Linux. */
#define PANIC_PRIMITVE_MSG_ID 0xA6200

/* Message ID unique to private data dump messages for SUAPI on Linux. */
#define PRIVATE_DATA_DUMP_MSG_ID 0xA6201

/* Message ID unique to shared memory dump messages for SUAPI on Linux. */
#define SHARED_MEM_DUMP_MSG_ID 0xA6202

/* Message ID unique to process list messages for SUAPI on Linux. */
#define SU_PROCESS_LIST_MSG_ID 0xA6203

/* Maximum number of entries in the process list message. */
#define MAX_PID_TABLE_COUNT 2000

/* Pointer to buffer that holds the process list to be output on panic. */
char* pid_list_buf = NULL;

/* Length of process id / name data in buffer. */
unsigned short pid_list_len = 0;

/* Variable to indicate if we are in a panic */
int su_previous_panic = 0;

/* pid of process that panicked */
pid_t supanic_saved_pid=0;

/* This type is used to track what data a read to /dev/supanic is returning. */
typedef enum SU_PANIC_OUTPUT_STATE
{
    SU_PANIC_REMAINING_BUFFER,
    SU_PANIC_BASIC_DATA,
    SU_PRIVATE_DATA_SECTION,
    SU_SHARED_MEM,
    SU_PROCESS_LIST
} SU_PANIC_OUTPUT_STATE_T;

/* Spin lock to prevent race conditions in buffer writing. */
spinlock_t log_buffer_lock =  SPIN_LOCK_UNLOCKED;

/* The circular buffer which data is logged to. */
char log_buffer_real[LOG_BUFFER_SIZE];
int log_buffer_size = LOG_BUFFER_SIZE;

/* The amount of data in the buffer before we tell the AP logger to flush. */
int log_buffer_quota = LOG_BUFFER_SIZE / 2;

/*
 * These pointers track the state of the buffer and are shared between the SUAPI
 * module and the datalogger.
 */
char* log_buffer = NULL;
char* log_read_ptr = NULL;
char* log_write_ptr = NULL;

/* Internal state in this module to determine if buffer has already filled up. */
static int log_buffer_full = 0;

/* State shared between the AP datalogger indicates if buffer has overflowed. */
int log_buffer_overwritten = 0;

/* Indicates if the AP logger is reading data. */
int log_active = 0;

/* Statistics to track logger progress. */
unsigned int log_attempted_bytes = 0;
unsigned int log_lost_bytes= 0;

/* Count shared between the datalogger indicates if writes are in progress. */
int log_busy_count = 0;

/* Internal state that indicates if the panic handler is ready to output data. */
static int supanic_data_ready = 0;

/* waitqueue used for AP logger thread to wait for data. */
DECLARE_WAIT_QUEUE_HEAD(log_wait_q);

/* waitqueue used to unblock /dev/supanic read when data is ready. */
static DECLARE_WAIT_QUEUE_HEAD(supanic_wait_q);

/* Guarantees that only one process can open /dev/supanic at a time. */
static atomic_t supanic_available = ATOMIC_INIT(1);

/* Protects writing panic blocks to file and the linked list. */
static DECLARE_MUTEX(supanic_mutex);

/* Linked list that holds panic blocks which are to be read from /dev/supanic. */
static LIST_HEAD(panic_block_list);

/* This panic block is only used if there was an error processing the panic. */
static struct panic_block_t default_pb;

static char *suapi_logger_conf_file = "/etc/suapi/sulog.conf";
module_param(suapi_logger_conf_file, charp, S_IRUGO);

EXPORT_SYMBOL(log_buffer);
EXPORT_SYMBOL(log_buffer_size);
EXPORT_SYMBOL(log_read_ptr);
EXPORT_SYMBOL(log_write_ptr);
EXPORT_SYMBOL(log_buffer_overwritten);
EXPORT_SYMBOL(log_active);
EXPORT_SYMBOL(log_attempted_bytes);
EXPORT_SYMBOL(log_lost_bytes);
EXPORT_SYMBOL(log_busy_count);
EXPORT_SYMBOL(log_wait_q);
EXPORT_SYMBOL(log_buffer_lock);

    
typedef struct panic_block_t
{
    /* Lets us make a linked list of panic blocks. */
    struct list_head list;

    /* We need the pointer to the "work struct" to free it after it runs. */
    struct work_struct* work;
    
    /* Log message header. */
    struct SuapiLogCommonHdr header;

    /* Common panic information. */
    struct SuapiPanicData panic_data; 
    
    /* Variable data portion. */
    char var_data[];
} panic_block_t;


static int supanic_open(struct inode *inode, struct file *fp);
static int supanic_release(struct inode *inode, struct file *fp);
static ssize_t supanic_read(struct file *fp, char * buffer,
                            size_t count, loff_t * offset);
static unsigned int supanic_poll(struct file * fp,
                                 struct poll_table_struct * poll_table);
static void write_panic_flip_file(char* data, unsigned int length);
static void suapi_internal_panic(SuapiLogData *log_data_arg);
static void queued_panic_handler(void *arg);
static int supanic_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg);

static struct file_operations supanic_fops =
{
    owner:      THIS_MODULE,
    llseek:     NULL,
    read:       supanic_read,
    aio_read:   NULL,
    write:      NULL,
    aio_write:  NULL,
    poll:       supanic_poll,
    ioctl:      supanic_ioctl,
    mmap:       NULL,
    open:       supanic_open,
    flush:      NULL,
    release:    supanic_release,
    fsync:      NULL,
    lock:       NULL,
    readv:      NULL,
    writev:     NULL,
    fsync:      NULL,
    lock:       NULL,
};

struct miscdevice supanic_device =
{
    minor:      MISC_DYNAMIC_MINOR,
    name:       SU_PANIC_DEV_NAME,
    fops:       &supanic_fops,
    devfs_name: SU_PANIC_DEV_NAME,
};

#ifdef LOGGER_DEBUG_MODE
#define LOGGER_DEBUG(args...) debug_print(args)

/*
 * DESCRIPTION: debug_print
 *     Prints debug messages for this file.
 *
 * INPUTS:
 *     const char* text: printk style string
 *     ...: variable arguments to be formatted by previous string.
 *
 * OUTPUTS:
 *     N/A
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static void debug_print(const char* text, ...)
{
    va_list ap;
    va_start(ap, text);
    printk("SUAPI: su_klogger.c: DEBUG: ");
    vprintk(text, ap);
    printk("\n");
    va_end(ap);
}

#else
#define LOGGER_DEBUG(args...)
#endif

/*
 * DESCRIPTION: supanic_ioctl 
 *     Main ioctl for supanic device. 
 *
 * INPUTS:
 *     struct inode* inode:  inode pointer passed on by application
 *     struct file* filp: file pointer passed on by appliation
 *     unsigned int cmd: cmd passed from user
 *     unsigned long arg: optional argument passed from user
 *
 * OUTPUTS:
 *     Error code if error occurs, 0 otherwise.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static int supanic_ioctl(struct inode *inode, 
                       struct file *filp,
                       unsigned int cmd,
                       unsigned long arg)
{
    int result = 0;
    struct task_struct* t;
    switch(cmd)
    {
        case SUPANIC_PROCESS_DUMP:
        {
     
            if (supanic_saved_pid != 0)
            {
                t = find_task_by_pid(supanic_saved_pid);

                if (t)  
                {
                    send_sig(SIGABRT,t, 0);
                }
                else
                {
                    printk("error finding panic task %d\n", supanic_saved_pid);
                    result = -ESRCH;
                }
            }
        
            break;
        }
        default:
        {
            printk("supanic dev: unknown IOCTL\n");
            result = -ENOTTY;
            break;
        }

    }
    return result;

}


/*
 * DESCRIPTION: write_data
 *     Copies data to the logger buffer and checks for wrapping.
 *
 * INPUTS:
 *     char * dest: address in buffer to write data to.
 *     char * src: address to copy data from.
 *     int len: length of data to copy.
 *
 * OUTPUTS:
 *     char *: pointer to next byte after copy is made.
 *
 * IMPORTANT NOTES:
 *     This function assumes that the buffer has already been checked that there
 *     is enough space available before the log_read_ptr.  It only checks for
 *     buffer wrapping.  This also assumes that we are in a critical section that
 *     won't allow other tasks to log messages while we're doing this.
 */
static char* write_data(char * dest, char * src, int len)
{
    int bytes_till_end = (int)((log_buffer + LOG_BUFFER_SIZE) - dest);

    if (bytes_till_end > len)
        bytes_till_end = len;

    /* Write data until buffer end. */
    memcpy(dest, src, bytes_till_end);

    /* Set up destination ptr for return value. */
    dest += len;
    if (dest >= log_buffer + LOG_BUFFER_SIZE)
        dest -= LOG_BUFFER_SIZE;
    
    len -= bytes_till_end;

    /* If write wraps, do second portion. */
    if (len)
        memcpy(log_buffer, src + bytes_till_end, len);

    return dest;
}

/*
 * DESCRIPTION: determine_wake_up
 *     Determines if the buffer quota has been reached and if the AP logger
 *     thread that reads the data should be woken up.
 *
 * INPUTS:
 *     None.
 *
 * OUTPUTS:
 *     None.
 *
 * IMPORTANT NOTES:
 *     None.
 */

static void determine_wake_up(void)
{
    int ptr_diff;

    /* If the AP logger isn't running, then there's nothing to wake up. */
    if (log_active == 0)
        return;
    
    /* Find the remaining data that can be safely flushed. */
    ptr_diff = log_write_ptr - log_read_ptr;
    
    /* Handle circular buffer wrapping. */
    if (ptr_diff < 0)
        ptr_diff += log_buffer_size;
    

    if (ptr_diff >= log_buffer_quota)
    {
        LOGGER_DEBUG("waking up AP Logger SUAPI thread\n");
        wake_up_interruptible(&log_wait_q);
    }
}


/*
 * DESCRIPTION: logger_reserve_and_write_header
 *     Reserves space in the logger buffer for a message, and writes the header.
 *
 * INPUTS:
 *     unsigned int block_length: total length of message
 *     char mtype: bitfield containing type of message and source.
 *     unsigned int msgid: ID of the message
 *
 * OUTPUTS:
 *     char *: pointer to location to write data after the header.  NULL if the
 *     buffer did not contain room for the message.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
static char* logger_reserve_and_write_header(unsigned int block_length, char mtype, unsigned int msgid)
{
    char *write_pointer;
    unsigned int time_stamp;
    int available_buffer;
    unsigned long irq_flags;
    SuapiLogCommonHdr hdr;
    
    LOGGER_DEBUG("entering reserve header; len = %d, mtype = %d, msgid = %x\n",block_length, mtype, msgid);
    
    if (block_length < SIZE_HEADER_LOG_DATA)
        return(NULL);

    time_stamp = jiffies;

    LOGGER_DEBUG("timestamp = %u\n",time_stamp);
    
    /* Disable interrupts */
    spin_lock_irqsave(&log_buffer_lock, irq_flags);

    write_pointer = log_write_ptr;

    LOGGER_DEBUG("write pointers = %x\n", write_pointer);
    
    if (log_read_ptr <= write_pointer)
        available_buffer = log_buffer_size - (write_pointer - log_read_ptr);
    else
        available_buffer = log_read_ptr - write_pointer;

    /*
     * SIZE_HEADER_BUFFER_FULL bytes added to reserve
     * buffer-full-message. LOGGER_BUFFER_MINIMUM_SEPARATION bytes
     * added to make impossible for the write pointer to reach the
     * read pointer.
     */
    available_buffer -= (SIZE_HEADER_BUFFER_FULL + LOGGER_BUFFER_MINIMUM_SEPARATION);

    LOGGER_DEBUG("availabe buffer = %d\n",available_buffer);

    log_attempted_bytes += block_length;

    /* If we're not currently flushing data. */
    if (0 == log_active)
    {
        /* Not logging so just set the overwrite bit if necessary. */
        if (!(available_buffer > block_length))
        {
            /* Due to this operation a resync will be needed by the flusher. */
            /* We need to flag this "overwrite" condition to the flusher. */
            log_buffer_overwritten = 1;
        }
    }
    else if (available_buffer < block_length)
    {
        LOGGER_DEBUG("out of room in buffer\n");

        log_lost_bytes += block_length;
        
        /* Not enough room since datalogger is backed up. */
        if (log_buffer_full)
        {
            spin_unlock_irqrestore(&log_buffer_lock, irq_flags);
        }
        else
        {
            log_buffer_full = 1;

            /* Write out buffer full header (it has no data). */
            SET_BUSY(log_busy_count);

            /* Update write pointer so new messages go after this one. */
            log_write_ptr += SIZE_HEADER_BUFFER_FULL;

            if (log_write_ptr >= log_buffer + LOG_BUFFER_SIZE)
                log_write_ptr -= LOG_BUFFER_SIZE;
            
            /* Restore interrupts */
            spin_unlock_irqrestore(&log_buffer_lock, irq_flags);

            /* Set up common message header. */
            hdr.sync_1 = SYNC_PATTERN_1;
            hdr.sync_2 = SYNC_PATTERN_2 | LOGGER_ARBITRARY_MSG_TYPE;
            hdr.msg_len = htons(SIZE_HEADER_BUFFER_FULL);
            hdr.timestamp = htonl(time_stamp);
            hdr.msg_id = htonl(BUFFER_FULL_INDICATION_ID);

            write_data(write_pointer,(char*)&hdr,sizeof(hdr));
                
            SET_IDLE(log_busy_count);
            
            LOGGER_DEBUG("wrote buffer full message\n");
        }
        
        return (NULL);
    }
    
    /* There is enough room. Actual datalogging occurs. */
    SET_BUSY(log_busy_count);
    
    log_buffer_full = 0;
    
    /* Update write pointer of the buffer so new messages go after this one.*/
    log_write_ptr += block_length;

    if (log_write_ptr >= log_buffer + LOG_BUFFER_SIZE)
        log_write_ptr -= LOG_BUFFER_SIZE;
    
    /* Restore interrupts */
    spin_unlock_irqrestore(&log_buffer_lock, irq_flags);
    
    /* Set up common message header. */
    hdr.sync_1 = SYNC_PATTERN_1;
    hdr.sync_2 = SYNC_PATTERN_2 | mtype;
    hdr.msg_len = htons(block_length);
    hdr.timestamp = htonl(time_stamp);
    hdr.msg_id = htonl(msgid);

    write_pointer = write_data(write_pointer,(char*)&hdr,SIZE_HEADER_LOG_DATA);
    
    LOGGER_DEBUG("writing message; new log_write_ptr is %x\n",log_write_ptr);
    LOGGER_DEBUG("log_read_ptr is %x\n",log_read_ptr);
    LOGGER_DEBUG("log_buffer is %x\n",log_buffer);
    LOGGER_DEBUG("write_pointer is %x\n",write_pointer);

    return (write_pointer);
}

/*
 * DESCRIPTION: suapi_log_message_ioctl
 *     ioctl handler for calls to suPalLogMessage.
 *
 * INPUTS:
 *     int arg: pointer to a SuapiLogMsg structure containing message information.
 *
 * OUTPUTS:
 *     int: 0 for success, negative for error condition.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_log_message_ioctl(int arg)
{
    SuapiLogMsg log_msg_arg;
    int retaction, retval;
    void * kmsg_p;
    SU_MSG_HDR * kmsghdr_p;

    LOGGER_DEBUG("Entering suLogMessage\n");
    
    if (copy_from_user(&log_msg_arg,(SuapiLogMsg *) arg,sizeof(SuapiLogMsg)))
        return -EFAULT;

    kmsg_p = SU_USER_TO_KERNEL(log_msg_arg.input.message);
    kmsghdr_p = SU_MSG2HDR(kmsg_p);

    if ( ! SU_IN_PARTITION_RANGE(kmsghdr_p) )
        return -EINVAL;

    retaction = suapi_internal_log_message(
                                kmsg_p,
                                log_msg_arg.input.msg_action);
    retval = put_user(retaction, (int *)arg);
    return retval;
}

/* suapi_internal_log_message assumes the following:
 *     - message is a pointer to valid SUAPI shared memory
 */
int suapi_internal_log_message(void * message, UINT32 msg_action)
{
    char * write_pointer;
    SU_PORT_HANDLE dest_ph;
    SU_PORT_HANDLE_STRUCT * dest_port;
    unsigned int msg_len, msg_id;
    char msg_type;
    int logging_enabled = 0;
    int epst_intercept = 0;
    SuapiLogMsgHdr msg_hdr =
        {.dest_port_handle =
                (void*)(htonl((u32)suGetMessagePortHandle(message))),
         .reply_port_handle =
                (void*)(htonl((u32)suGetMessageReplyPort(message))),
         .reserved = 0,
         .msg_priority = suGetMessagePriority(message)
        };

    /* Just return if logging is globally disabled. */
    if (suapi_global_data_ptr->logger_enabled == 0)
    {
        LOGGER_DEBUG("Logging is disabled, returning\n");
        return(SU_TEST_PERFORM_ACTION);
    }

    dest_ph = suGetMessagePortHandle(message);

    /* Logging is not supported when sending a message to a process. */
    if (SU_ISPHPID(dest_ph))
        return SU_TEST_PERFORM_ACTION;

    /* Check message destination port for validity since it is used as
     * an index into the port table array.
     */
    if (SU_PHOUTOFRANGE(dest_ph))
        return SU_TEST_PERFORM_ACTION;

    dest_port = SU_PH2PHPTR(dest_ph);

    /* Get length of message body */
    msg_len = suGetMessageLength(message);

    /* Check that message address plus length is within partition memory since
     * msg_len bytes may be logged.
     */
    if ( ! SU_IN_PARTITION_RANGE(message + msg_len) )
        return SU_TEST_PERFORM_ACTION;

    msg_id = suGetMessageType(message);

    switch (msg_action)
    {
        case SU_MSGACTION_SEND:
            msg_type = LOGGER_SEND_MSG_TYPE;
            msg_len += SIZE_HEADER;

            /* Check if logging is enabled on this port */
            if (suLoggingEnabled(dest_port))
                logging_enabled = 1;

            if (suGetPortRouting(dest_port) == SU_ROUTE_INTERCEPT)
                epst_intercept = 1;
            break;

        case SU_MSGACTION_RECEIVE:
            msg_type = LOGGER_RECEIVE_MSG_TYPE;

            /* If is a receive action, we must only log the header packet */
            msg_len = SIZE_HEADER;

            /* Check if logging is enabled on this port */
            if (suLoggingEnabled(dest_port))
                logging_enabled = 1;
            break;

        case SU_MSGACTION_REPLY:
            msg_type = LOGGER_REPLY_MSG_TYPE;
            msg_len += SIZE_HEADER;

            /* Check if logging is enable on reply port of the message body   */
            /* because this is a REPLY ACTION: the original message that      */
            /* caused the reply was received on a port for which logging is   */
            /* enabled                                                        */
            if (suGetMessageReplyLoggingBit(message))
                logging_enabled = 1;

            if (suGetPortRouting(dest_port) == SU_ROUTE_INTERCEPT)
                epst_intercept = 1;
            break;

        default:
            return SU_TEST_PERFORM_ACTION;
            break;
    }

    if ((epst_intercept == 0) && (logging_enabled == 0))
    {
        LOGGER_DEBUG("Logging not enabled, returning from suLogMessage\n");
        return SU_TEST_PERFORM_ACTION;
    }

    write_pointer = logger_reserve_and_write_header(msg_len, msg_type, msg_id);

    /* logger_busy count was incremented by logger_reserve_and_write_header ! */
    if (write_pointer)
    {
        LOGGER_DEBUG("write_pointer here: %x\n",write_pointer);
        
        /* Need to finish writing the header and copy out the message. */
        write_pointer = write_data(write_pointer,
                                   (char*)&msg_hdr,
                                   sizeof(msg_hdr));

        LOGGER_DEBUG("write_pointer here: %x\n",write_pointer);

        /* If it is a receive port logging, we must only log the header packet */
        if (msg_action != SU_MSGACTION_RECEIVE)
            write_data(write_pointer, (char*)message,
                       msg_len - SIZE_HEADER);
        
        SET_IDLE(log_busy_count);
    }

    determine_wake_up();
    
    LOGGER_DEBUG("msg pointer = %x\n",message);
    LOGGER_DEBUG("msg len = %d\n",msg_len);
    LOGGER_DEBUG("msg id = %d\n",msg_id);
    LOGGER_DEBUG("dest port = %x\n",msg_hdr.dest_port_handle);
    LOGGER_DEBUG("reply port = %x\n",msg_hdr.reply_port_handle);
    LOGGER_DEBUG("reserved = %d\n",msg_hdr.reserved);
    LOGGER_DEBUG("priority = %d\n",msg_hdr.msg_priority);
    LOGGER_DEBUG("msg type = %d\n",msg_type);
    LOGGER_DEBUG("msg copy data = %d\n",(msg_action != SU_MSGACTION_RECEIVE));
    LOGGER_DEBUG("suLogMessage logged %d bytes\n", msg_len);
    LOGGER_DEBUG("log_busy_count = %d\n",log_busy_count);

    if (epst_intercept)
    {
        suapi_internal_delete_message(message);
        return(SU_TEST_JUST_RETURN);
    }       
    else
    {
        return(SU_TEST_PERFORM_ACTION);
    }
}

/*
 * DESCRIPTION: suapi_log_data_ioctl
 *     ioctl handler for calls to suPalLogData.
 *
 * INPUTS:
 *     int arg: pointer to SuapiLogData structure containing message information.
 *
 * OUTPUTS:
 *     int: 0 for success, negative for error condition.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_log_data_ioctl(int arg)
{
    SuapiLogData log_data_arg;
    va_list ap;
    unsigned int size_of_next_data;
    int  number_of_pairs;
    unsigned int size_of_data = SIZE_HEADER_LOG_DATA;
    char* write_pointer;
    char* pointer;
    char data8;
    short data16;
    int data;
    u64 data64;

    LOGGER_DEBUG("entering suLogData");
    
    if (copy_from_user(&log_data_arg,(SuapiLogData *) arg,sizeof(SuapiLogData)))
        return -EFAULT;
    
    number_of_pairs = log_data_arg.num_pairs;
    ap = log_data_arg.ap;
    
    /* retrieve size of the list received */
    while (number_of_pairs != 0)
    {                 
        size_of_next_data = va_arg(ap,int);
                
        /* check if the next arg is a pointer or data       */
        /* we consider that SU_SIZE_8 is 1 (byte) and so on */
                
        switch (size_of_next_data)
        {
            case SU_SIZE_8:
                size_of_data += 1;
                data = va_arg(ap, int);
                break;
            
            case SU_SIZE_16:        
                size_of_data += 2;                
                data = va_arg(ap, int);
                break;
            
            case SU_SIZE_32:       
                size_of_data += 4;                
                data = va_arg(ap, int);
                break;

            case SU_SIZE_64:
                size_of_data += 8;
                data64 = va_arg(ap, u64);
                break;       
                
            case 0:
                /*
                 * to log a message without data, it is allowed to use 2
                 * alternatives only:
                 * suLogData( phandle, msgid, 1, 0, NULL ) or
                 * suLogData( phandle, msgid, 0 ). Later case is preferable
                 */
                                                                
                /* if next arg is a pointer then retrieve the pointer */
                pointer = (char*) va_arg(ap, void *);
                        
                if (!((pointer == NULL) && (log_data_arg.num_pairs == 1)))
                    return -EINVAL;
                break;       

            default:
                /* check invalid size */
                if (size_of_next_data < 0 ) 
                    return -EINVAL;
                                
                /* next arg is a pointer then retrieve the pointer */
                pointer = (char*) va_arg(ap, void * );
                                
                if (pointer == NULL)
                    return -EINVAL;
                
                size_of_data += size_of_next_data;
                
                /* check if size is too big */
                if ( (size_of_data + SIZE_HEADER_BUFFER_FULL +
                      LOGGER_BUFFER_MINIMUM_SEPARATION) > log_buffer_size)
                    return -EINVAL;
                break;                            
        }

        number_of_pairs--;                
        
    };
        
    write_pointer = logger_reserve_and_write_header(size_of_data,
                                                    LOGGER_ARBITRARY_MSG_TYPE,
                                                    log_data_arg.msg_id);

    if (write_pointer)
    {
        /* Loop through pairs again, this time to store them. */
        number_of_pairs = log_data_arg.num_pairs;
        ap = log_data_arg.ap;
    
        while (number_of_pairs != 0)
        {
            size_of_next_data = va_arg(ap,unsigned int);

            switch (size_of_next_data)
            {
                case SU_SIZE_8:
                    data8 = va_arg(ap,int);
                    write_pointer = write_data(write_pointer, (char*)&data8, 1);
                    break;

                case SU_SIZE_16:
                    data16 = va_arg(ap,int);
                    write_pointer = write_data(write_pointer, (char*)&data16, 2);
                    break;

                case SU_SIZE_32:
                    data = va_arg(ap,int);
                    write_pointer = write_data(write_pointer, (char*)&data, 4);
                    break;

                case SU_SIZE_64:
                    data64 = va_arg(ap,u64);
                    write_pointer = write_data(write_pointer, (char*)&data64, 8);
                    break;

                default:
                    pointer = (char*) va_arg(ap, void *);
                    
                    if (!access_ok(VERIFY_READ, pointer, size_of_next_data))
                        return -EFAULT;
                    
                    write_pointer = write_data(write_pointer, pointer,
                                               size_of_next_data);

                    break;
            }
            number_of_pairs--;
        }

        SET_IDLE(log_busy_count);
    }

    determine_wake_up();
    
    LOGGER_DEBUG("suLogData logged %d bytes",size_of_data);
    LOGGER_DEBUG("log_busy_count = %d\n",log_busy_count);

    return 0;
}

void suPanic(UINT32 code, UINT32 num_pairs, ... )
{
    SuapiLogData iarg;
    va_list ap; 

    va_start(ap, num_pairs);

    iarg.msg_id = code;
    iarg.num_pairs = num_pairs;
    iarg.ap = ap;

    suapi_internal_panic(&iarg);

    va_end(ap);
}

/*
 * DESCRIPTION: suapi_panic_ioctl
 *     ioctl handler for calls to suPalPanic.
 *
 * INPUTS:
 *     int arg: pointer to SuapiLogData structure containing panic information.
 *
 * OUTPUTS:
 *     int: 0 for success, negative for error condition.
 *
 * IMPORTANT NOTES:
 *     This function never returns.
 */
int suapi_panic_ioctl(int arg)
{
    SuapiLogData log_data_arg;
    
    LOGGER_DEBUG("entering suapi_panic_ioctl");
    
    if (copy_from_user(&log_data_arg,(SuapiLogData *) arg,sizeof(SuapiLogData)))
        suapi_internal_panic(NULL);
    else
        suapi_internal_panic(&log_data_arg);

    return 0;
}

void suapi_internal_panic(SuapiLogData *log_data_arg)
{
    va_list ap;
    unsigned int size_of_next_data;
    int  number_of_pairs;
    char* pointer;
    char data8;
    short data16;
    int data;
    u64 data64;
    char* tmp_panic_ptr;
    unsigned int length = 0;
    panic_block_t* pb = NULL;
    struct timeval tv;
    unsigned int panic_id;
    struct task_struct * task = current;
    int tmp;
    int count = 0;
    int malloc_flag = GFP_KERNEL;
    struct work_struct *work = NULL;


    LOGGER_DEBUG("entering suapi_panic_ioctl");

    if (su_previous_panic == 0)
    {
        su_previous_panic = 1;

        if (log_data_arg != NULL)
        {
            panic_id = log_data_arg->msg_id;

            /*
             * Watchdog panics are from previous boot, so don't
             * prevent additional SUAPI panics if they happen.
             *
             * Note it is safe to decrement su_previous_panic here without 
             * locking since this should only occur during insmod and no
             * one would be making SUAPI calls yet.
             */

            if (SU_WATCHDOG_PANIC_ID == panic_id)
                su_previous_panic--;
        }
        else
        {
            panic_id = SU_PANIC_UNKNOWN_ID;
            goto panic_data_fail;
        }
        
        if (in_interrupt())
            malloc_flag = GFP_ATOMIC;
        
        read_lock(&tasklist_lock);
        
        do
        {
            count++;
            task = prev_task(task);
        } while (task != current && count < MAX_PID_TABLE_COUNT);
        
        pid_list_buf = kmalloc(count * MAX_PID_TABLE_SIZE, malloc_flag);
        
        if (pid_list_buf != NULL)
        {
            memset(pid_list_buf, 0, count * MAX_PID_TABLE_SIZE);
            
            task = current;
            
            do
            {
                tmp = snprintf(pid_list_buf + pid_list_len,
                               MAX_PID_TABLE_SIZE - 1, "/%d/%s", task->pid,
                               task->comm);
                
                if (tmp <= 0)
                {
                    LOGGER_DEBUG("Error in storing process id and name: %d %s\n",
                                 task->pid, task->comm);
                }
                else
                {
                    if (tmp > MAX_PID_TABLE_SIZE - 1)
                        tmp = MAX_PID_TABLE_SIZE - 1;
                    
                    /* Increase total length. */
                    pid_list_len += tmp + 1;
                }
                
                task = prev_task(task);
                count--;
                
            } while (task != current && count > 0);
        }
        
        read_unlock(&tasklist_lock);            
        
        number_of_pairs = log_data_arg->num_pairs;
        ap = log_data_arg->ap;
        /* retrieve size of the list received */
        while (number_of_pairs != 0)
        {                 
            size_of_next_data = va_arg(ap,int);
            
            /* check if the next arg is a pointer or data       */
            /* we consider that SU_SIZE_8 is 1 (byte) and so on */
            
            switch (size_of_next_data)
            {
                case SU_SIZE_8:
                    length += 1;
                    data = va_arg(ap, int);
                    break;
                    
                case SU_SIZE_16:        
                    length += 2;                
                    data = va_arg(ap, int);
                    break;
                    
                case SU_SIZE_32:       
                    length += 4;                
                    data = va_arg(ap, int);
                    break;

                case SU_SIZE_64:
                    length += 8;
                    data64 = va_arg(ap, u64);
                    break;       
                    
                case 0:
                    /* if next arg is a pointer then retrieve the pointer */
                    pointer = (char*) va_arg(ap, void *);
                    
                    if (!((pointer == NULL) && (log_data_arg->num_pairs == 1)))
                    {
                        length = 0;
                        goto panic_data_fail;
                    }
                    break;       
                    
                default:
                    /* check invalid size */
                    if (size_of_next_data < 0 )
                    {
                        length = 0;
                        goto panic_data_fail;
                    }
                    
                    /* next arg is a pointer then retrieve the pointer */
                    pointer = (char*) va_arg(ap, void * );
                    
                    if (pointer == NULL)
                    {
                        length = 0;
                        goto panic_data_fail;
                    }
                    
                    length += size_of_next_data;
                    break;                            
            }
            
            number_of_pairs--;                
            
        };    

        /* check if size is too big */
        if (length > PANIC_MAX_BLOCK)
        {
            length = 0;
            goto panic_data_fail;
        }
        
        /* Allocate memory for entire panic block. */
        pb = (panic_block_t*)kmalloc(sizeof(struct list_head) + length
                                     + sizeof(struct work_struct*)
                                     + sizeof(SuapiLogCommonHdr)
                                     + sizeof(SuapiPanicData), malloc_flag);

        if (pb == NULL)
        {
            length = 0;
            goto panic_data_fail;
        }

        /* Initialize panic block to zero. */
        memset(pb, 0, length);
        
        /* Initialize common message header. */
        pb->header.sync_1 = SYNC_PATTERN_1;
        pb->header.sync_2 = SYNC_PATTERN_2 | LOGGER_ARBITRARY_MSG_TYPE;
        pb->header.msg_len = htons(length + sizeof(SuapiLogCommonHdr) +
                                   sizeof(SuapiPanicData));
        
        pb->header.timestamp = htonl(jiffies);
        pb->header.msg_id = htonl(PANIC_PRIMITVE_MSG_ID);
        
        /* Initialize panic data fields preceeding variable arguments. */

#ifdef CONFIG_MOT_POWER_IC_ATLAS
        /* Get time of day from secure lock through power IC */
        if (power_ic_rtc_get_time(&tv) != 0)
        {
            /* failed to get current time, set to 0 */
            tv.tv_sec = 0;
        }

#else
        do_gettimeofday(&tv);
#endif

        pb->panic_data.seconds = htonl(tv.tv_sec);
        pb->panic_data.maj_ver = 0;
        pb->panic_data.min_ver = 0;
        pb->panic_data.min_min_ver = 0;

        pb->panic_data.uptime = htonl(jiffies);
        
        pb->panic_data.panic_id = htonl(panic_id);


        if ((in_interrupt()) || 
            (SU_WATCHDOG_PANIC_ID == panic_id))
        {
            pb->panic_data.pid = 0;
            supanic_saved_pid = 0;
            memset(pb->panic_data.pid_name, 0, MAX_PID_TABLE_SIZE);
        }
        else
        {
            pb->panic_data.pid = htonl(current->pid);
            supanic_saved_pid = current->pid;
            /* Copy pid name to array - the array does not have to be NULL terminated. */
            strncpy(pb->panic_data.pid_name, current->comm, MAX_PID_TABLE_SIZE);
        }

        
        pb->panic_data.panic_length = htonl(length);
        
        tmp_panic_ptr = pb->var_data;
        
        /* copy argument list to allocated memory */
        number_of_pairs = log_data_arg->num_pairs;
        ap = log_data_arg->ap;
        
        /* Loop through pairs again, this time to store them. */
        while (number_of_pairs != 0)
        {
            size_of_next_data = va_arg(ap,unsigned int);
            
            switch (size_of_next_data)
            {
                case SU_SIZE_8:
                    data8 = va_arg(ap,int);
                    memcpy(tmp_panic_ptr, (char*)&data8, 1);
                    tmp_panic_ptr += 1;
                    break;
                    
                case SU_SIZE_16:
                    data16 = va_arg(ap,int);
                    memcpy(tmp_panic_ptr, (char*)&data16, 2);
                    tmp_panic_ptr += 2;
                    break;
                    
                case SU_SIZE_32:
                    data = va_arg(ap,int);
                    memcpy(tmp_panic_ptr, (char*)&data, 4);
                    tmp_panic_ptr += 4;
                    break;
                    
                case SU_SIZE_64:
                    data64 = va_arg(ap,u64);
                    memcpy(tmp_panic_ptr, (char*)&data64, 8);
                    tmp_panic_ptr += 8;
                    break;
                    
                default:
                    pointer = (char*) va_arg(ap, void *);
                    
                    if (copy_from_user(tmp_panic_ptr, pointer, size_of_next_data))
                        goto panic_data_fail;
                    
                    tmp_panic_ptr += size_of_next_data;
                    
                    break;
            }
            number_of_pairs--;
        }
        
      panic_data_fail:
        if (pb == NULL)
        {
            LOGGER_DEBUG("Panic data collection failure, using static memory.\n");
            
            pb = &default_pb;
            /* Initialize common message header. */
            pb->header.sync_1 = SYNC_PATTERN_1;
            pb->header.sync_2 = SYNC_PATTERN_2 | LOGGER_ARBITRARY_MSG_TYPE;
            pb->header.msg_len = htons(sizeof(SuapiLogCommonHdr) +
                                       sizeof(SuapiPanicData));
            
            pb->header.timestamp = 0;
            pb->header.msg_id = htonl(PANIC_PRIMITVE_MSG_ID);
            
            /* Initialize panic data fields preceeding variable arguments. */
            pb->panic_data.seconds = 0;
            pb->panic_data.maj_ver = 0;
            pb->panic_data.min_ver = 0;
            pb->panic_data.min_min_ver = 0;
            
            pb->panic_data.uptime = 0;
            pb->panic_data.panic_id = htonl(panic_id);
        
            pb->panic_data.pid = htonl(current->pid);
        
            /* Copy pid name to array.  Clear any unused spaces in fixed size array,
             * But the array does not have to be NULL terminated.
             */
            memset(pb->panic_data.pid_name, 0, MAX_PID_TABLE_SIZE);
            strncpy(pb->panic_data.pid_name, current->comm, MAX_PID_TABLE_SIZE);
            
            pb->panic_data.panic_length = 0;
        }
        
        // Send file writing and linked list updating to the shared work queue.
        work = (struct work_struct*)kmalloc(sizeof(struct work_struct), malloc_flag);
        
        if (work != NULL)
        {
            /* Save pointer to work_struct for later memory free.
             * However, if we use the static panic block because of an error, then
             * we cannot do this without risk of corruption.
             */
            if (pb != &default_pb)
                pb->work = work;
            else
                pb->work = NULL;
        
            LOGGER_DEBUG("Scheduling panic block for work queue\n");
            
            INIT_WORK(work, queued_panic_handler, (void*)pb);
            schedule_work(work);
        }
    }

    // Task should spin waiting for abort signal to core dump.

    if ((!in_interrupt()) && (SU_WATCHDOG_PANIC_ID != panic_id))
    {
        LOGGER_DEBUG("Task will now be suspending!\n");
    
        do
        {
            set_current_state(TASK_INTERRUPTIBLE);
            schedule();

        } while(!signal_pending(current));
    }

    LOGGER_DEBUG("Interrupt context; will be returning from suPanic call\n");
    
    return;
}

/*
 * DESCRIPTION: queued_panic_handler
 *     This function is to put on the Linux kernel shared work queue. This is
 *     done primarily because these operations are not acceptable to do from
 *     interrupt context.  SUAPI panics can be called from interrupts.
 *
 * INPUTS:
 *     void* arg - pointer the panic block allocated for this panic.
 *
 * OUTPUTS:
 *     None.
 *
 * IMPORTANT NOTES:
 *     Mutex locking needs to be done, particularly around the linked list update
 *     and the setting of supanic_data_ready (both of which are accessed by the
 *     /dev/supanic driver).
 */
static void queued_panic_handler(void *arg)
{
    panic_block_t* pb = (panic_block_t*)arg;
    int length = ntohs(pb->header.msg_len);

    LOGGER_DEBUG("Entering queued_panic_handler\n");

    /* Free dynamically allocated work queue struct. */
    if (pb->work != NULL)
        kfree(pb->work);
    
    /*
     * Writing the panic block to file and adding to the linked list need to be
     * protected.
     */    
    down(&supanic_mutex);

    write_panic_flip_file((char*)&pb->header, length);

    if (htonl(SU_WATCHDOG_PANIC_ID) == pb->panic_data.panic_id)
    {
#ifdef CONFIG_MOT_FEAT_OTP_EFUSE
        unsigned char security_setting = MOT_SECURITY_MODE_PRODUCTION;
        int security_err = 0;
   
        mot_otp_get_mode(&security_setting, &security_err);
        if (security_err != 0 ||
            (security_setting != MOT_SECURITY_MODE_ENGINEERING &&
             security_setting != MOT_SECURITY_MODE_NO_SECURITY))
        {
            /*
             * Prevent double reboots on product phones for watchdogs
             * as the problem was already cleared.
             */
            kfree(pb);
            up(&supanic_mutex);
            return;
        }
#else
        printk(KERN_ERR "Error: Motorola OTP security is not enabled. Not panicking for watchdog\n");
        kfree(pb);
        up(&supanic_mutex);
        return;
#endif /* CONFIG_MOT_FEAT_OTP_EFUSE */
    }

    list_add_tail(&pb->list, &panic_block_list);

    supanic_data_ready = 1;

    up(&supanic_mutex);
    
    /*
     * After panic data is collected and stored in the file, tell driver for
     * /dev/supanic there's data to read.
     */
    wake_up_interruptible(&supanic_wait_q);

    LOGGER_DEBUG("Leaving queued_panic_handler\n");
}

/*
 * DESCRIPTION: write_panic_flip_file
 *     Writes a panic block to the SUAPI panic block "flip" file.
 *
 * INPUTS:
 *     char* data - pointer to the data to write.
 *     unsigned int length - the number of bytes to write.
 *
 * OUTPUTS:
 *     None.
 *
 * IMPORTANT NOTES:
 *     This function needs to be protected against concurrent calls..
 */
static void write_panic_flip_file(char* data, unsigned int length)
{
    struct timespec a_time, b_time;
    loff_t a_size, b_size, new_size;
    char* old;
    char* new;
    struct iattr trunc_attr;
    struct file * fp;
    int err;
    mm_segment_t old_fs;
    
    LOGGER_DEBUG("Entering write_panic_flip_file, going to write %d bytes\n",
                 length);
    
    trunc_attr.ia_size = 0;
    trunc_attr.ia_valid = ATTR_SIZE | ATTR_CTIME;    

    // We first need to determine the most recently modified (newer) file.
    fp = filp_open(FLIP_FILE_A, O_RDONLY, 0);

    if (IS_ERR(fp))
    {
        fp = filp_open(FLIP_FILE_A, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR);

        if (IS_ERR(fp))
        {
            printk("Couldn't create file %s\n",FLIP_FILE_A);
            return;
        }
    }

    // Get modified time and size, and close.
    a_time = fp->f_dentry->d_inode->i_mtime;
    a_size = i_size_read(fp->f_dentry->d_inode);
    filp_close(fp, NULL);
    
    fp = filp_open(FLIP_FILE_B, O_RDONLY, 0);
    
    if (IS_ERR(fp))
    {
        fp = filp_open(FLIP_FILE_B, O_RDONLY | O_CREAT, S_IRUSR | S_IWUSR);
        
        if (IS_ERR(fp))
        {
            printk("Couldn't create file %s\n",FLIP_FILE_B);
            return;
        }
    }

    // Get modified time and size, and close.
    b_time = fp->f_dentry->d_inode->i_mtime;
    b_size = i_size_read(fp->f_dentry->d_inode);
    filp_close(fp, NULL);
    
    // Compare modified times.
    if (a_time.tv_sec < b_time.tv_sec)
    {
        old = FLIP_FILE_A;
        new = FLIP_FILE_B;
        new_size = b_size;
    }
    else if (a_time.tv_sec > b_time.tv_sec)
    {
        old = FLIP_FILE_B;
        new = FLIP_FILE_A;
        new_size = a_size;
    }
    else
    {
        if (a_time.tv_nsec < b_time.tv_nsec)
        {
            old = FLIP_FILE_A;
            new = FLIP_FILE_B;
            new_size = b_size;
        }
        else
        {
            old = FLIP_FILE_B;
            new = FLIP_FILE_A;
            new_size = a_size;
        }
    }
    
    if (length > MAX_FILE_SIZE - new_size)
    {
        LOGGER_DEBUG("Need to erase old panic block file\n");
        
        // No more room in most recent file; we must erase the oldest one.
        fp = filp_open(old, O_WRONLY, 0);

        if (IS_ERR(fp))
        {
            printk("Couldn't open file %s\n",old);
            return;
        }
        
        /* Reset the offset to zero */
        vfs_llseek(fp, 0, 0);        

        /* Reset the size to zero */
        down(&(fp->f_dentry->d_inode->i_sem));
        down_write(&(fp->f_dentry->d_inode->i_alloc_sem));
        notify_change(fp->f_dentry, &trunc_attr);
        up_write(&(fp->f_dentry->d_inode->i_alloc_sem));
        up(&(fp->f_dentry->d_inode->i_sem));
    }
    else
    {
        LOGGER_DEBUG("Going to write to newer panic block file\n");
        
        // Still room in the newer file, so open it.
        fp = filp_open(new, O_WRONLY | O_APPEND, 0);

        if (IS_ERR(fp))
        {
            printk("Couldn't open file %s\n",new);
            return;
        }
    }

    // Finally write the data.  Linux kernel writing requires the calls to
    // get_fs and set_fs.
    old_fs = get_fs();
    set_fs(get_ds());
    err = vfs_write(fp, data, length, &fp->f_pos);
    set_fs(old_fs);
    
    if (err < 0)
    {
        printk("Write to panic block failed with %d\n", err);
    }

    filp_close(fp, NULL);
}


/*
 * DESCRIPTION: suLogConfInit
 *     This service is called to initialize the logger port config area and
 *     global on/off control.  It also initializes the logger circular buffer.
 *
 * INPUTS:
 *     None.
 *
 * OUTPUTS:
 *     int - 0 if success, error code on failure.
 *
 * IMPORTANT NOTES:
 *     This function must be called before SUAPI ports are registered, or risk
 *     losing logging capabilities on these ports.
 *     The SUAPI global memory area must be allocated and zeroed before this
 *     function is called.
 *
 */

int __init suLogConfInit(void)
{
    struct file *fp;
    char * conf_ptr = suapi_logger_config_ptr;
    char line[SU_CFG_BUFSIZE];
    int count;
    int length;
    int ignore_line = 0;

    suapi_global_data_ptr->logger_enabled = 0;

    log_buffer = kmalloc(LOG_BUFFER_SIZE, GFP_KERNEL);

    if (!log_buffer)
        return -ENOMEM;

    log_read_ptr = log_buffer;
    log_write_ptr = log_buffer;

    fp = filp_open(suapi_logger_conf_file, O_RDONLY, 0);

    /* File is optional - if it doesn't exist, just initialize list to empty. */
    if (IS_ERR(fp))
    {
        LOGGER_DEBUG("Couldn't open logger config file %s.",
                      suapi_logger_conf_file);
        *(conf_ptr) = '\0';
        return 0;
    }

    /* First line indicates global logging on or off. */
    /* Should say ENABLED or DISABLED. */
    if (suapi_fgets(line, SU_CFG_BUFSIZE, fp) != NULL)
    {
        if (strncmp(line, "ENABLED", 7) == 0)
            suapi_global_data_ptr->logger_enabled = 1;

        /* Read in port names. */
        count = 0;
        while ((suapi_fgets(line, SU_CFG_BUFSIZE, fp) != NULL) &&
               count < LOGGER_CONFIG_NUMBER_OF_NAMES)
        {
            length = strlen(line);

            /*
             * If the first two characters aren't "p/", or the port name is
             * too long, then ignore the line.
             */
            if (line[0] == 'p' && line[1] == '/' &&
                length <= LOGGER_CONFIG_NAME_SIZE + 1 &&
                ignore_line == 0)
            {
                /* Don't copy newlines. */
                if (line[length - 1] == '\n')
                    length--;

                /* Copy port to config area. */
                strncpy(conf_ptr, line, length);
                conf_ptr += LOGGER_CONFIG_NAME_SIZE;
                count++;
            }
            else
            {
                /* Ignore further reads until we get a newline. */
                if (line[length - 1] != '\n')
                    ignore_line = 1;
                else
                    ignore_line = 0;
            }
        }
    }

    filp_close(fp, NULL);

    LOGGER_DEBUG("global logging control == %d\n",
                 suapi_global_data_ptr->logger_enabled);

    return 0;
}


/******************* /dev/supanic handlers *************************************/


/*
 * DESCRIPTION: supanic_open
 *     Open handler - prevents concurrent opens by multiple processes.
 *
 * INPUTS:
 *     struct inode* inode - pointer to device inode
 *     struct file* fp - pointer to file struct
 *
 * OUTPUTS:
 *     int - 0 for success, negative for error code
 *
 * IMPORTANT NOTES:
 *     Prevents concurrent opens by multiple processes.
 */
static int supanic_open(struct inode *inode, struct file *fp)
{
    /* device information */
    if (! atomic_dec_and_test (&supanic_available))
    {
        atomic_inc(&supanic_available);
#ifdef SU_TEST_WATCHDOG
        
        {
            unsigned long flags;
            /*
             * This code deliberately disables interrupts and loops to
             * cause a watchdog panic.
             */
            local_irq_save(flags);
            while(1);
            local_irq_restore(flags);
        }
#endif
        return -EBUSY; /* already open */
    }
    
    return 0;
}

/*
 * DESCRIPTION: supanic_release
 *     Release (close) handler - prevents concurrent opens by multiple processes.
 *
 * INPUTS:
 *     struct inode* inode - pointer to device inode
 *     struct file* fp - pointer to file struct
 *
 * OUTPUTS:
 *     int - 0 for success, negative for error code
 *
 * IMPORTANT NOTES:
 *     Prevents concurrent opens by multiple processes.
 */
static int supanic_release(struct inode *inode, struct file *fp)
{
    /* release the device */
    atomic_inc(&supanic_available);

    return 0;
}


/*
 * DESCRIPTION: supanic_read
 *    Read handler for /dev/supanic.
 *
 * INPUTS:
 *     struct file* fp - pointer to file struct
 *     char* buffer - pointer to the buffer to copy data to.
 *     size_t count - the maximum number of bytes to copy.
 *     loff_t * offset - the offset into the device to read (only somewhat used).
 *
 * OUTPUTS:
 *     ssize_t - Number of bytes read for success, negative for error code
 *
 * IMPORTANT NOTES:
 *     This read is somewhat unusual, in that it uses a state machine to read out
 *     several different types of memory in order.  Thus several static variables
 *     are needed, which requires that only one process (should be the panic
 *     daemon) only opens this device.
 */
static ssize_t supanic_read(struct file *fp, char * buffer,
        size_t count, loff_t * offset)
{
    // Next type of data to output
    static SU_PANIC_OUTPUT_STATE_T output_next = SU_PANIC_REMAINING_BUFFER;

    // Used to do a one-time initialization of other statics.
    static int init = 1;

    /* Gets a snapshot of the buffer pointers to flush it out after panic. */
    static char* local_read_ptr = NULL;
    static char* local_write_ptr = NULL;

    // hdr_offset is how much of the current message header has been output.
    static int hdr_offset = 0;

    // hdr_offset is how much of the current message data has been output.
    static int msg_offset = 0;

    // pointer to the current position in the panic block linked list.
    static struct list_head * pb_ptr = &panic_block_list;

    // Amount of bytes to output 
    static unsigned int pb_length = 0;

    // Flag indicating if the SUAPI private data has already been dumped or not.
    static int private_data_done = 0;

    // Flag indicating if the SUAPI shared memory has already been dumped or not.
    static int shared_mem_done = 0;

    struct list_head * old_pb_ptr;
    
    panic_block_t* pb;
    SuapiLogCommonHdr header;
    int hdr_written = 0;
    int ptr_diff;

    LOGGER_DEBUG("Entering supanic_read\n");
    
    /* Check for valid args */
    if (fp == NULL || buffer == NULL) 
        return -EINVAL;

    /* See if panic has occurred and data is ready. */
    if (supanic_data_ready == 0)
    {
        /* Don't block if they said not to. */
        if (fp->f_flags & O_NONBLOCK)
            return -EAGAIN;
    
        /* Sleep until panic data is ready to be read. */
        wait_event_interruptible(supanic_wait_q, supanic_data_ready);
    }

    // This initialization should only be done once (after the first panic).
    if (init)
    {
        init = 0;

        local_read_ptr = log_read_ptr;
        local_write_ptr = log_write_ptr;
        
        if (log_buffer_overwritten)
        {
            LOGGER_DEBUG("SUAPI buffer is overwritten\n");
            
            local_read_ptr = local_write_ptr +
                SIZE_HEADER_BUFFER_FULL + LOGGER_BUFFER_MINIMUM_SEPARATION;
            
            /* Readjust read pointer if it wrapped. */
            if (local_read_ptr >= log_buffer + log_buffer_size)
                local_read_ptr -= log_buffer_size;
        }

        LOGGER_DEBUG("local_read_ptr == %x, local_write_ptr == %x\n",
                     local_read_ptr, local_write_ptr);
    }
    
    LOGGER_DEBUG("Panic output state is %d\n", output_next);

    switch (output_next)
    {
        case SU_PANIC_REMAINING_BUFFER:
            /* Find the remaining data that can be flushed. */
            ptr_diff = local_write_ptr - local_read_ptr;
            
            /* Handle circular buffer wrapping. */
            if (ptr_diff < 0)
            {
                ptr_diff = log_buffer + log_buffer_size - local_read_ptr;

                if (ptr_diff <= count)
                {
                    count = ptr_diff;

                    /*
                     * If write ptr equals the buffer start, there is
                     * no more data to flush.
                     */
                    if (local_write_ptr == log_buffer)
                        output_next = SU_PANIC_BASIC_DATA;
                }
            }
            else
            {
                if (ptr_diff <= count)
                {
                    /* There is no more data to flush. */
                    count = ptr_diff;
                    output_next = SU_PANIC_BASIC_DATA;
                }
            }

            copy_to_user(buffer, local_read_ptr, count);

            /* Update the read pointer, and adjust for wrapping if necessary. */
            local_read_ptr += count;
            if (local_read_ptr >= log_buffer + log_buffer_size)
                local_read_ptr = log_buffer;
            
            break;
            
        // Need to get basic panic block(s) (set up by suPanic call)
        case SU_PANIC_BASIC_DATA:

            /* pb_length == 0 indicates we need to read a panic block off of
             * the linked list (we're either just starting or finished dumping
             * the last one).
             */
            if (pb_length == 0)
            {
                if (pb_ptr->next == &panic_block_list)
                {
                    // No more panic blocks
                    count = 0;

                    // Protect update of flag to prevent race conditions.
                    down(&supanic_mutex);
                    supanic_data_ready = 0;
                    up(&supanic_mutex);
                    break;
                }
                else
                {
                    // Protext linked list updates.
                    down(&supanic_mutex);

                    // Step through linked list
                    old_pb_ptr = pb_ptr;
                    pb_ptr = pb_ptr->next;
                    
                    // Free already read panic block, unless this is the first one
                    if (old_pb_ptr != &panic_block_list)
                    {
                        pb = list_entry(old_pb_ptr, struct panic_block_t, list);
                        list_del(old_pb_ptr);
                        kfree(pb);
                    }
                    
                    // Now, get next panic block
                    pb = list_entry(pb_ptr, struct panic_block_t, list);
                    up(&supanic_mutex);

                    pb_length = ntohl(pb->panic_data.panic_length)
                        + sizeof(struct list_head)
                        + sizeof(struct work_struct*)
                        + sizeof(struct SuapiLogCommonHdr)
                        + sizeof(struct SuapiPanicData);
                    *offset = sizeof(struct list_head)
                        + sizeof(struct work_struct*);
                }
            }
            else
                pb = list_entry(pb_ptr, struct panic_block_t, list);
            
            /* If we are outputting the remainder of the panic block,
             * set up state to get shared mem on next read, if not done already.
             */
            if (count > pb_length - *offset)
            {
                count = pb_length - *offset;
                copy_to_user(buffer, (char*)pb + *offset, count);

                if (private_data_done == 0)
                    output_next = SU_PRIVATE_DATA_SECTION;
                
                *offset = 0;
                pb_length = 0;
            }
            else
            {
                copy_to_user(buffer, (char*)pb + *offset, count);
                *offset += count;
            }
            break;
            
       case SU_PRIVATE_DATA_SECTION:
            /* If we haven't started or finished writing the message header,
             * do so now.
             */
            if (hdr_offset != sizeof(header))
            {
                /* Set up the header for the message. */
                header.sync_1 = SYNC_PATTERN_1;
                header.sync_2 = SYNC_PATTERN_2 | LOGGER_ARBITRARY_MSG_TYPE;
                header.timestamp = htonl(jiffies);
                header.msg_id = htonl(PRIVATE_DATA_DUMP_MSG_ID);

                /* Calculate the message length field in the header.
                 * LOG_MAX_MSG_LEN is the maximum amount of data that be put in
                 * the message.  Then add in the header size.
                 */
                header.msg_len = SU_PRIVATE_SIZE - *offset;
                if (header.msg_len > LOG_MAX_MSG_LEN)
                    header.msg_len = LOG_MAX_MSG_LEN;

                header.msg_len += sizeof(header);

                header.msg_len = htons(header.msg_len);

                /* Calculate amount of header to output.  Count is maximum
                 * that can be output in this read call.
                 */
                hdr_written = sizeof(header) - hdr_offset;
                if (hdr_written > count)
                    hdr_written = count;

                copy_to_user(buffer, &header + hdr_offset, hdr_written);

                /* Update header offset.  If the amount we just copied equals
                 * the requested count, then we're done.
                 */
                hdr_offset += hdr_written;
                count -= hdr_written;
                if (count == 0)
                    break;
            }

            /* Cap count if it is greater than the remainder of this message. */
            if (count > LOG_MAX_MSG_LEN - msg_offset)
                count = LOG_MAX_MSG_LEN - msg_offset;

            /* Cap count if it is greater than the remaining memory segment. */
            if (count > SU_PRIVATE_SIZE - *offset)
                count = SU_PRIVATE_SIZE - *offset;

            copy_to_user(buffer + hdr_written, suapi_private_start_ptr + *offset,count);

            *offset += count;
            msg_offset += count;

            /* If we've reached the end of a message, indicate we need to write
             * a new header on next read.
             */
            if (msg_offset == LOG_MAX_MSG_LEN)
            {
                msg_offset = 0;
                hdr_offset = 0;
                LOGGER_DEBUG("Reached end of message.\n");
            }

            /* If we've reached an end of the entire segment, update the state
             * to the next dump type for the next read.
             */
            if (*offset >= SU_PRIVATE_SIZE)
            {
                *offset = 0;
                msg_offset = 0;
                hdr_offset = 0;
                private_data_done = 1;

                if (shared_mem_done == 0)
                    output_next = SU_SHARED_MEM;

                LOGGER_DEBUG("Reached end of private data segment.\n");
            }           

            break;

        case SU_SHARED_MEM:
            /* If we haven't started or finished writing the message header,
             * do so now.
             */
            if (hdr_offset != sizeof(header))
            {
                /* Set up the header for the message. */
                header.sync_1 = SYNC_PATTERN_1;
                header.sync_2 = SYNC_PATTERN_2 | LOGGER_ARBITRARY_MSG_TYPE;
                header.timestamp = htonl(jiffies);
                header.msg_id = htonl(SHARED_MEM_DUMP_MSG_ID);

                /* Calculate the message length field in the header.
                 * LOG_MAX_MSG_LEN is the maximum amount of data that be put in
                 * the message.  Then add in the header size.
                 */
                header.msg_len = SU_GLOBAL_SIZE - *offset;
                if (header.msg_len > LOG_MAX_MSG_LEN)
                    header.msg_len = LOG_MAX_MSG_LEN;

                header.msg_len += sizeof(header);

                header.msg_len = htons(header.msg_len);
                
                /* Calculate amount of header to output.  Count is maximum
                 * that can be output in this read call.
                 */
                hdr_written = sizeof(header) - hdr_offset;
                if (hdr_written > count)
                    hdr_written = count;

                copy_to_user(buffer, &header + hdr_offset, hdr_written);

                /* Update header offset.  If the amount we just copied equals
                 * the requested count, then we're done.
                 */
                hdr_offset += hdr_written;
                count -= hdr_written;
                if (count == 0)
                    break;
            }

            /* Cap count if it is greater than the remainder of this message. */
            if (count > LOG_MAX_MSG_LEN - msg_offset)
                count = LOG_MAX_MSG_LEN - msg_offset;

            /* Cap count if it is greater than the remaining memory segment. */
            if (count > SU_GLOBAL_SIZE - *offset)
                count = SU_GLOBAL_SIZE - *offset;

            copy_to_user(buffer + hdr_written, suapi_kernel_start_ptr + *offset,count);

            *offset += count;
            msg_offset += count;

            /* If we've reached the end of a message, indicate we need to write
             * a new header on next read.
             */
            if (msg_offset == LOG_MAX_MSG_LEN)
            {
                msg_offset = 0;
                hdr_offset = 0;
                LOGGER_DEBUG("Reached end of message.\n");
            }

            /* If we've reached an end of the entire segment, update the state
             * to the next dump type for the next read.
             */
            if (*offset >= SU_GLOBAL_SIZE)
            {
                *offset = 0;
                msg_offset = 0;
                hdr_offset = 0;
                shared_mem_done = 1;

                output_next = SU_PROCESS_LIST;

                LOGGER_DEBUG("Reached end of shared memory segment.\n");
            }

            break;

        case SU_PROCESS_LIST:
            /* If we haven't started or finished writing the message header,
             * do so now.
             */
            if (hdr_offset != sizeof(header))
            {
                /* Set up the header for the message. */
                header.sync_1 = SYNC_PATTERN_1;
                header.sync_2 = SYNC_PATTERN_2 | LOGGER_ARBITRARY_MSG_TYPE;
                header.timestamp = htonl(jiffies);
                header.msg_id = htonl(SU_PROCESS_LIST_MSG_ID);
                header.msg_len = htons(pid_list_len + sizeof(header));

                /* Calculate amount of header to output.  Count is maximum
                 * that can be output in this read call.
                 */
                hdr_written = sizeof(header) - hdr_offset;
                if (hdr_written > count)
                    hdr_written = count;

                copy_to_user(buffer, &header + hdr_offset, hdr_written);

                /* Update header offset.  If the amount we just copied equals
                 * the requested count, then we're done.
                 */
                hdr_offset += hdr_written;
                count -= hdr_written;
                if (count == 0)
                    break;
            }

            if (pid_list_len - *offset < count)
                count = pid_list_len - *offset;
            
            copy_to_user(buffer + hdr_written, pid_list_buf + *offset, count);
            *offset += count;

            if (*offset >= pid_list_len)
            {
                *offset = 0;
                output_next = SU_PANIC_BASIC_DATA;

                kfree(pid_list_buf);

                LOGGER_DEBUG("Reached end of process list segment.\n");
            }

            break;
            
        default:
            count = 0;
            // Protect update of flag to prevent race conditions.
            down(&supanic_mutex);
            supanic_data_ready = 0;
            up(&supanic_mutex);
            break;
    };

    LOGGER_DEBUG("count == %d, hdr_written == %d\n, offset == %d\n",
                 count, hdr_written, *offset);
    
    return (ssize_t)(count + hdr_written);
}

/*
 * DESCRIPTION: supanic_poll
 *     Poll handler - notifies when there is panic data to read.
 *
 * INPUTS:
 *     struct file* fp - pointer to file struct
 *     struct poll_table_struct* poll_table - used to holds wait queues to notify.
 *
 * OUTPUTS:
 *     unsigned int - ready events for success, negative for error code
 *
 * IMPORTANT NOTES:
 *     The wait queue is woken and supanic_data_ready is TRUE after a panic
 *     is handled by suPanic.
 */

static unsigned int supanic_poll(struct file * fp,
                                 struct poll_table_struct * poll_table)
{
    unsigned int mask = 0;

    /* Check for valid args */
    if (fp == NULL || poll_table == NULL) 
        return -EINVAL;
    
    poll_wait(fp, &supanic_wait_q, poll_table);

    /* Only reading is supported. */
    if (supanic_data_ready)
        mask |= POLLIN;

    return mask;
}
