// Copyright (c) 2006, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

/*
 *  DESCRIPTION:
 *      This file implements the SUAPI port, queue, and message services within
 *      the SUAPI kernel module.
 */
 
// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.

/************** INCLUDES *****************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/time.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <linux/poll.h>
#include <linux/vmalloc.h>
#include <linux/syscalls.h>
#include <asm/semaphore.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/miscdevice.h>

#include <linux/suspend.h>
#include <linux/suapi_module.h>
#include <linux/su_ktimers.h>
#include <linux/mpm.h>
#include <linux/su_panic_codes.h>

/* For conversion of msecs to jiffies, the assumption in this module is that
 * LONG_MAX == MAX_SCHEDULE_TIMEOUT and that the ratio of msecs to jiffies is
 * greater than or equal to 1.
 */

/************** LOCAL CONSTANTS **********************************************/

/* Message priorities may be low, medium, or high. */
#define SU_PRIO_LOW 0
#define SU_PRIO_MEDIUM 1
#define SU_PRIO_HIGH 2

/* Queue priority masks. */
#define SU_PRIO_LOW_MASK    0x00000001
#define SU_PRIO_MEDIUM_MASK 0x00000002
#define SU_PRIO_HIGH_MASK   0x00000004

/*
 * The state of the queue is defined by one of these constants or the address of
 * its registered events:
 *
 * SU_QAVAIL - the queue is available for allocation
 *
 * SU_QUSED - the queue is in use.
 *
 */
#define SU_QAVAIL  0
#define SU_QUSED  -1

/* 
 * Defines special event mask. 
 */  
#define SU_NO_EVENT_MASK 0x00

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static unsigned int suapi_q_poll(struct file *, struct poll_table_struct *);

static INT32 suFreeQueue(SU_QUEUE *);
static void su_subqueue_init(SU_SUBQUEUE_HEAD *);
static int su_queue_rmqueue_fifo (SU_QUEUE *);
static int su_queue_rmqueue_priority (SU_QUEUE *);
static void su_queue_rmqueue_nodes (SU_MINNODE *);
static SU_MINNODE * su_queue_dequeue_fifo(SU_QUEUE * const);
static SU_MINNODE * su_queue_dequeue_priority(SU_QUEUE * const );
static SU_MINNODE * su_subqueue_remove_head(SU_SUBQUEUE_HEAD *const );
static int su_queue_enqueue_fifo(SU_QUEUE * const, SU_MINNODE *);
static int su_queue_enqueue_priority(SU_QUEUE * const, SU_MINNODE *);
static int su_subqueue_add_tail(SU_SUBQUEUE_HEAD *const, SU_MINNODE *const);
static int suapi_internal_send_message_to_queue(SU_QUEUE *, void *, int in_isr);

/************** LOCAL MACROS *************************************************/
/* In SUAPI, handles must be >= 1, so add 1 to the index to make it a handle. */
/* These values are not converted to queue handles yet since they must pass
 * through the ioctl return value and they should be ints for that.
 */
#define SU_QINDEX2QH(index) ((int) (index) + 1)

/* Now convert it back. */
#define SU_QH2QINDEX(qh) ((int)(qh) - 1)

/* This macro takes the index of a SUAPI queue handle, checks that it is in the
 * right range, and converts it to the kernel address of the specified queue
 * structure. If it is not in the right range, it evaluates to NULL
 */
#define SU_QINDEX2QPTR(index) \
    ( (((unsigned int) (index))>=SU_NQUEUES) ? NULL : (&(su_qarray_ptr[index])))

/* Convert queue handle to kernel address of queue structure. */
#define SU_QH2QPTR(qh) SU_QINDEX2QPTR(SU_QH2QINDEX(qh))

/*
 * The bits in su_prioritymask represent whether or not a message of that
 * priority exists on the queue,the lowest order bit representng the lowest 
 * priority.  
 */

/*
 * SU_SET_PRIORITY_BIT(): This macro sets the appropriate priority bit in
 * the queues su_prioritymask. 
 */

#define SU_SET_PRIORITY_BIT(q,p) \
        (((SU_QUEUE *)(q))->su_prioritymask |= (0x1 << (p)))

/*
 * SU_HIGHEST_PRIORITY_BIT(): This macro uses FF1() to determine which
 * priority is currently the highest on the given queue.
 */

#define SU_HIGHEST_PRIORITY_BIT(q) \
  ((UINT8) \
    ( \
      (((SU_QUEUE *)(q))->su_prioritymask & SU_PRIO_HIGH_MASK) \
          ? SU_PRIO_HIGH \
          : (((SU_QUEUE *)(q))->su_prioritymask & SU_PRIO_MEDIUM_MASK) \
                 ? SU_PRIO_MEDIUM \
                 : SU_PRIO_LOW \
    ) \
  )

/*
 * SU_CLEAR_PRIORITY_BIT(): This macro clears the priority bit in qhandle->
 * su_prioritymask. This is done when the last message of that priority is
 * removed from the queue.
 */

#define SU_CLEAR_PRIORITY_BIT(q,p) \
        ((((SU_QUEUE *)(q))->su_prioritymask) &= (~(0x1 << (p))))

#define su_queue_is_empty(q) (!(q->su_qmsgcount))
#define su_subqueue_is_empty(q) (q->su_q_tailpred == (SU_MINNODE *)q)

/* SU_SQP2QH() converts a subqueue pointer to a queue handle. */
#define SU_SQP2QH(sqp) \
           SU_QINDEX2QH(((UINT32)sqp - (UINT32)su_qarray_ptr)/sizeof(SU_QUEUE));


/************** LOCAL VARIABLES **********************************************/
/* Queues-specific spin lock.
 * Soft interrupts are expected to be disabled when locking this spinlock
 * because it is used during soft interrupt context.
 */
static spinlock_t su_queues_lock = SPIN_LOCK_UNLOCKED;

/************** SUAPI MODULE GLOBAL VARIABLES ********************************/
SU_PORT_HANDLE_STRUCT * suPortTable_ptr;

SU_QUEUE * su_qarray_ptr;

/************** GLOBAL EXPORT DEFINITIONS ************************************/
EXPORT_SYMBOL(suapi_enable_port_logging);
EXPORT_SYMBOL(suapi_disable_port_logging);

/************** FUNCTION DEFINITIONS *****************************************/

#ifdef CONFIG_SUAPI
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
int suapi_queues_send_msg_and_wait_ioctl(int arg)
{
    void * kmsgp;
    SU_MSG_HDR * kmsghdrp;
    int ret = 0;
    void * message = NULL;
    Suapi_Kernel_Timer local_timer;
    Suapi_Tasklet_Handler_Data pend_info;

    struct suSendMsgAndWaitData argdata;

    if (copy_from_user(&argdata,
                       (struct suSendMsgAndWaitData *) arg,
                       sizeof(struct suSendMsgAndWaitData)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (copy_to_user((struct suSendMsgAndWaitData *) arg,
                         &argdata,
                         sizeof(struct suSendMsgAndWaitData)))
        return -EFAULT;

    if (0 >= argdata.timeout)
    {
        ret = -ERANGE;
        return ret;
    }

    if ((argdata.phandle == SU_INVALID_HANDLE)
        || SU_PHOUTOFRANGE(argdata.phandle)
        || ( ! SU_ISPHPID(argdata.phandle)
            &&  (SU_PH2PHPTR(argdata.phandle)->port_type == SU_LOGGER_PORT) ) )
        return -EINVAL;

    kmsgp = SU_USER_TO_KERNEL(argdata.message);
    kmsghdrp = SU_MSG2HDR(kmsgp);
    if (! SU_IN_PARTITION_RANGE(kmsghdrp))
        return -EBADMSG;

    kmsghdrp->replyport = (SU_PORT_HANDLE) current->pid;

    spin_lock_bh(&su_events_lock);

    /* Since current->su_wait_flags is only used while waiting for events, it
     * can do double-duty as the holder of the reply message pointer.  The same
     * task can't be waiting for events and waiting for a reply message at the
     * same time.
     */
    current->su_wait_flags = 0;
    current->su_state = SUAPI_TASK_STATE_WAIT_REPLY_MESSAGE;

    spin_unlock_bh(&su_events_lock);

    ret = suapi_internal_send_message_to_port(argdata.phandle, kmsgp, SU_NOT_IN_ISR);

    if (ret != 0)
    {
        spin_lock_bh(&su_events_lock);
        /* If a message was received, store it for later deletion. */
        if (current->su_wait_flags != 0)
            message = (void *) current->su_wait_flags;

        current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
        current->su_wait_flags = 0;
        spin_unlock_bh(&su_events_lock);

        /* If there was an error but a message was received, delete it. */
        if (message)
            suapi_internal_delete_message(SU_USER_TO_KERNEL(message));

        return ret;
    }

    pend_info.tasklet_done = 0;
    if (argdata.timeout != LONG_MAX)
    {
        pend_info.pid = current->pid;
        rtc_sw_task_init(&local_timer.ltimer, suapi_tasklet_handler_fn, (unsigned long) &pend_info);
        rtc_sw_task_schedule(argdata.timeout, &local_timer.ltimer);
    }

    spin_lock_bh(&su_events_lock);
    for (;;)
    {
        if (signal_pending(current))
        {
            ret = -ERESTARTSYS;
            break;
        }
        if (current->su_wait_flags)
        {
            ret = 0;
            break;
        }
        if (argdata.timeout != LONG_MAX)
        {
            if (pend_info.tasklet_done)
            {
                ret = -ETIMEDOUT;
                break;
            }
        }
        /* Wait forever. */
        set_current_state(TASK_INTERRUPTIBLE);

        spin_unlock_bh(&su_events_lock);

        schedule();

        /* Going into deep sleep. */
        if (unlikely(current->flags & PF_FREEZE))
            refrigerator(PF_FREEZE);

        spin_lock_bh(&su_events_lock);
    }

    current->su_state = SUAPI_TASK_STATE_NOT_WAITING;

    /* Message is assumed valid since it was checked prior to sending.
     * Since there is no more processing inside the kernel based on the
     * message header, there is no need to check header's validity here.
     */
    argdata.message = (void *) current->su_wait_flags;

    current->su_wait_flags = 0;
    set_current_state(TASK_RUNNING);

    if (argdata.timeout != LONG_MAX)
    {
        if (!pend_info.tasklet_done)
            rtc_sw_task_kill(&local_timer.ltimer);
    }

    spin_unlock_bh(&su_events_lock);

    /* If there was an error but a message was received, delete it. */
    if (ret && argdata.message)
        suapi_internal_delete_message(SU_USER_TO_KERNEL(argdata.message));

    if (!ret)
        /* Assumes output address is still writable. */
        copy_to_user((struct suSendMsgAndWaitData *) arg,
                         &argdata,
                         sizeof(struct suSendMsgAndWaitData));

    return(ret);
}
#else
int suapi_queues_send_msg_and_wait_ioctl(int arg)
{
    void * kmsgp;
    SU_MSG_HDR * kmsghdrp;
    int ret = 0;
    void * message = NULL;
    long expires = 0;

    struct suSendMsgAndWaitData argdata;

    if (copy_from_user(&argdata,
                       (struct suSendMsgAndWaitData *) arg,
                       sizeof(struct suSendMsgAndWaitData)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (copy_to_user((struct suSendMsgAndWaitData *) arg,
                         &argdata,
                         sizeof(struct suSendMsgAndWaitData)))
        return -EFAULT;

    if (0 >= argdata.timeout)
        return -ERANGE;

    if ((argdata.phandle == SU_INVALID_HANDLE)
        || SU_PHOUTOFRANGE(argdata.phandle)
        || ( ! SU_ISPHPID(argdata.phandle)
            &&  (SU_PH2PHPTR(argdata.phandle)->port_type == SU_LOGGER_PORT) ) )
        return -EINVAL;

    kmsgp = SU_USER_TO_KERNEL(argdata.message);
    kmsghdrp = SU_MSG2HDR(kmsgp);
    if (! SU_IN_PARTITION_RANGE(kmsghdrp))
        return -EBADMSG;

    kmsghdrp->replyport = (SU_PORT_HANDLE) current->pid;

    spin_lock_bh(&su_events_lock);

    /* Since current->su_wait_flags is only used while waiting for events, it
     * can do double-duty as the holder of the reply message pointer.  The same
     * task can't be waiting for events and waiting for a reply message at the
     * same time.
     */
    current->su_wait_flags = 0;
    current->su_state = SUAPI_TASK_STATE_WAIT_REPLY_MESSAGE;

    spin_unlock_bh(&su_events_lock);

    ret = suapi_internal_send_message_to_port(argdata.phandle, kmsgp);

    if (ret != 0)
    {
        spin_lock_bh(&su_events_lock);
        /* If a message was received, store it for later deletion. */
        if (current->su_wait_flags != 0)
            message = (void *) current->su_wait_flags;

        current->su_state = SUAPI_TASK_STATE_NOT_WAITING;
        current->su_wait_flags = 0;
        spin_unlock_bh(&su_events_lock);

        /* If there was an error but a message was received, delete it. */
        if (message)
            suapi_internal_delete_message(SU_USER_TO_KERNEL(message));

        return ret;
    }

    /* Compute expiration here so that it is not recalculated each time through
     * the loop. If timeout is LONG_MAX (wait forever) or 0 (don't wait), the
     * expires variable will never be used.
     */
    expires = msecs_to_jiffies(argdata.timeout);

    spin_lock_bh(&su_events_lock);
    for (;;)
    {
        if (current->su_wait_flags)
        {
            ret = 0;
            break;
        }
        if (signal_pending(current))
        {
            ret = -ERESTARTSYS;
            break;
        }
        if (argdata.timeout == LONG_MAX)
        {
            /* Wait forever. */
            set_current_state(TASK_INTERRUPTIBLE);

            spin_unlock_bh(&su_events_lock);

            schedule();

            /* Going into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_events_lock);
        }
        else
        {
            if (expires >= MAX_SCHEDULE_TIMEOUT)
            {
                ret = -ERANGE;
                break;
            }

            set_current_state(TASK_INTERRUPTIBLE);

            spin_unlock_bh(&su_events_lock);
            expires = schedule_timeout(expires);

            /* Go into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_events_lock);
            if (0 >= expires && !current->su_wait_flags)
            {
                ret = -ETIMEDOUT;
                break;
            }
        }
    }

    current->su_state = SUAPI_TASK_STATE_NOT_WAITING;

    /* Message is assumed valid since it was checked prior to sending.
     * Since there is no more processing inside the kernel based on the
     * message header, there is no need to check header's validity here.
     */
    argdata.message = (void *) current->su_wait_flags;

    current->su_wait_flags = 0;
    set_current_state(TASK_RUNNING);

    spin_unlock_bh(&su_events_lock);

    /* If there was an error but a message was received, delete it. */
    if (ret && argdata.message)
        suapi_internal_delete_message(SU_USER_TO_KERNEL(argdata.message));

    if (!ret)
        /* Assumes output address is still writable. */
        copy_to_user((struct suSendMsgAndWaitData *) arg,
                         &argdata,
                         sizeof(struct suSendMsgAndWaitData));

    return(ret);
}
#endif
#endif /* CONFIG_SUAPI */

/*
 * DESCRIPTION: suapi_q_poll
 *     The SUAPI queue poll method of /dev/suapi allows a thread to select
 *     on a queue until a message appears.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being select-ed.
 *     wait: poll table wait structure provided and maintained by the kernel.
 *
 * OUTPUTS:
 *     If message is available on the queue, returns the mask indicating that
 *         messages may be received from the queue without blocking.
 *     If queue is in free state, returns POLLERR
 *
 * IMPORTANT NOTES:
 *     select() will return success even when POLLERR is returned here because
 *        a read of the queue will not result in the thread blocking.
 */
static unsigned int suapi_q_poll(struct file *filp,
                 struct poll_table_struct *wait)
{
    SU_QUEUE * q = filp->private_data;
    int ret = 0;

    poll_wait(filp, &(q->suqwaiters), wait);

    /* If the queue is not allocated, then return an error. */
    if (q->su_state == 0)
        return POLLERR;

    if (q->su_qmsgcount)
        ret = POLLIN | POLLRDNORM;

    return ret;
}

/*
 * DESCRIPTION: suapi_queues_with_fd_ioctl
 *     This function associates the specified queue handle with a file
 *     descriptor.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being ioctl-ed.
 *     arg: pointer to the queue handle being associated with filp.
 *
 * OUTPUTS:
 *     N/A
 *
 * IMPORTANT NOTES:
 *     Converts the qhandle to a user space address, associates the qhandle
 *     with the queue's waiter list, and makes the file poll-able by modifying
 *     the file's poll method function pointer.
 */
int suapi_queues_with_fd_ioctl(struct file *filp, int arg)
{
    SU_QUEUE_HANDLE qhandle;
    SU_QUEUE * kqp;
    int ret;

    ret = get_user(qhandle, (SU_QUEUE_HANDLE *) arg);
    if (ret)
        return ret;

    kqp = SU_QH2QPTR(qhandle);

    if (kqp == NULL)
        return -ENOENT;

    /* Check that queue is in use. */
    if (kqp->su_state == SU_QAVAIL)
        return -ENOENT;

    filp->private_data = kqp;
    filp->f_op->poll = suapi_q_poll;

    return 0;
}

/*
 * DESCRIPTION: suapi_queues_wake_select_ioctl
 *     Wakes waiters select-ing on the specified queue.
 *
 * INPUTS:
 *     filp: Pointer to the file structure of the file being ioctl-ed.
 *     arg: pointer to the queue handle being waited on.
 *
 * OUTPUTS:
 *     N/A
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_queues_wake_select_ioctl(struct file *filp, int arg)
{
    int result;
    SU_QUEUE_HANDLE qhandle;
    SU_QUEUE * kqp;

    result = get_user(qhandle, (SU_QUEUE_HANDLE *) arg);
    if (result < 0)
        return(result);

    kqp = SU_QH2QPTR(qhandle);

    if (kqp == NULL)
        return -ENOENT;

    wake_up_all(&(kqp->suqwaiters));

    return 0;
}

/*
 * DESCRIPTION: suapi_create_queue_ioctl
 *     This function initializes an SU_QUEUE structure.
 *
 * INPUTS:
 *     is_fifo_queue      - determines which queue methods to use
 *
 * OUTPUTS: None
 */
int suapi_create_queue_ioctl(int is_fifo_queue)
{
    int i;
    UINT32 index = 0;
    SU_QUEUE * kqp = NULL;

    /*
     * Search for and allocate an available queue.
     */
    while(index < SU_NQUEUES)
    {
        /*
         * IF queue is available,
         *      Try to allocate it.
         * ELSE
         *      Goto next queue.
         */
        if (su_qarray_ptr[index].su_state == SU_QAVAIL)
        {
            /*
             * Allocate queue in atomic way.
             * IF task lost the race to allocate
             *          Start over
             */
            spin_lock_bh(&su_queues_lock);
            if (su_qarray_ptr[index].su_state == SU_QAVAIL)
            {
                su_qarray_ptr[index].su_state = SU_QUSED;
                kqp = ((SU_QUEUE *) &(su_qarray_ptr[index]));
                spin_unlock_bh(&su_queues_lock);
                /* break out of the loop. */
                break;
            }
            spin_unlock_bh(&su_queues_lock);
            /*
             * If queue was not still free after the lock was acquired,
             * a higher priority task reserved the queue before this task
             * could get to it.  Start over.
             */
            index = 0;
        }
        else
        {
            /* Try next queue. */
            index += 1;
        }
    } /* End of while loop. */

    /* No free queues. */
    if (NULL == kqp)
        return -ENOSPC;

    /* Initialize the queue and return. */

    if (is_fifo_queue == IS_FIFO_QUEUE)
        kqp->su_qtype = IS_FIFO_QUEUE;
    else
        kqp->su_qtype = IS_PRIORITY_QUEUE;

    kqp->su_waiting_tid  = SU_INVALID_HANDLE;
    kqp->su_event_thandle    = SU_INVALID_HANDLE;
    kqp->su_emask        = SU_NO_EVENT_MASK;
    kqp->su_qmsgcount    = 0;
    kqp->su_prioritymask = 0;

    for(i=0; i<SU_NUM_QUEUE_PRIORITIES; i++)
        su_subqueue_init(&kqp->su_subqueue[i]);

    /* In SUAPI, handles must be >= 1, so add 1 to the index. */
    return (SU_QINDEX2QH(index));
}

/*
 * DESCRIPTION: su_subqueue_init
 *     This function initializes a SU_SUBQUEUE_HEAD structure.
 *
 * INPUTS:
 *     qh   - Pointer to SU_SUBQUEUE_HEAD structure.
 *
 * OUTPUTS: None
 */
static void su_subqueue_init(SU_SUBQUEUE_HEAD *qh)
{
    qh->su_q_head     = (SU_MINNODE *)&qh->su_q_null;
    qh->su_q_null     = 0;
    qh->su_q_tailpred = (SU_MINNODE *)&qh->su_q_head;
}

/*
 * DESCRIPTION: suapi_delete_queue_ioctl
 *     This function removes a generic queue.
 *
 * INPUTS:
 *  qhandle  - Queue handle
 *
 * OUTPUTS:
 *  Returns ENOENT if qhandle is bad.
 *
 * IMPORTANT NOTES:
 *  None
 */
int suapi_delete_queue_ioctl(SU_QUEUE_HANDLE qhandle)
{
    SU_QUEUE * kqp = SU_QH2QPTR(qhandle);

    /* Verify qhandle is valid */
    if ( kqp == NULL )
        return -ENOENT;

    if (kqp->su_qtype == IS_FIFO_QUEUE)
        return(su_queue_rmqueue_fifo(kqp));
    else
        return(su_queue_rmqueue_priority(kqp));
}

/*
 * DESCRIPTION: su_queue_rmqueue_fifo
 *     This function provides FIFO-specific functionality when deleting a
 *     queue.
 *
 * INPUTS:
 *     kqp  - Pointer to FIFO queue.
 *
 * OUTPUTS:
 *     Returns 0 on success, -ENOENT if queue is already deleted.
 *
 *IMPORTANT NOTES:
 *     Assumes a valid queue pointer is passed.
 *
 */
static int su_queue_rmqueue_fifo (SU_QUEUE * kqp)
{
    SU_MINNODE * node;
    int retval;

    /* Protect from other tasks trying to delete this queue. */
    spin_lock_bh(&su_queues_lock);

    /* If suFreeQueue fails, then return */
    if((retval = suFreeQueue(kqp)) != 0)
    {
        spin_unlock_bh(&su_queues_lock);
        return retval;
    }

    /*
     * Save head of fifo node list so that the memory associated
     * any remaining unreceived nodes can be freed after the
     * critical section.
     */
    node = kqp->su_subqueue[SU_PRIO_LOW].su_q_head;

    spin_unlock_bh(&su_queues_lock);
    su_queue_rmqueue_nodes(node);

    return 0;
}

/*
 * DESCRIPTION: su_queue_rmqueue_priority
 *     This function provides priority-specific functionality when deleting a
 *     queue.
 *
 * INPUTS:
 *     kqp  - Pointer to priority queue.
 *
 * OUTPUTS:
 *     Returns 0 on success, -ENOENT if queue is already deleted.
 *
 *IMPORTANT NOTES:
 *     Assumes a valid queue pointer is passed.
 */
static int su_queue_rmqueue_priority (SU_QUEUE * kqp)
{
    SU_MINNODE * node_low, * node_med, * node_high;
    int retval;

    /* Protect from other tasks trying to delete this queue. */
    spin_lock_bh(&su_queues_lock);

    /* If suFreeQueue fails, return here */
    if((retval = suFreeQueue(kqp)) != 0)
    {
        spin_unlock_bh(&su_queues_lock);
        return retval;
    }

    /*
     * Save pointer to subqueue array so that the memory associated
     * the subqueue array and any remaining unreceived nodes can be freed
     * after the critical section.
     */
    node_low = kqp->su_subqueue[SU_PRIO_LOW].su_q_head;
    node_med = kqp->su_subqueue[SU_PRIO_MEDIUM].su_q_head;
    node_high = kqp->su_subqueue[SU_PRIO_HIGH].su_q_head;

    /* End of protected region. */
    spin_unlock_bh(&su_queues_lock);

    su_queue_rmqueue_nodes(node_low);
    su_queue_rmqueue_nodes(node_med);
    su_queue_rmqueue_nodes(node_high);
    return 0;
}

/*
 * DESCRIPTION: suFreeQueue
 *      This function marks a queue as available and wakes up any task(s)
 *      which may be waiting on the queue.
 *
 * INPUTS:
 *      kqp - Pointer to the queue.
 *
 * OUTPUTS:
 *      Returns 0 if successful, otherwise returns -ENOENT.
 *
 * IMPORTANT NOTES:
 *     This service is called from the queue deletion functions.
 *
 *     suFreeQueue MUST be called from within a critical section to prevent
 *     other tasks from freeing this queue or manipulating its objects.
 */
static INT32 suFreeQueue(SU_QUEUE * kqp)
{
    struct task_struct *t;

    switch((int)(kqp->su_state))
    {
        /* Somebody already deleted this queue if it is in these
         * states.
         */
        case SU_QAVAIL:
            return -ENOENT;

        /*
         * Otherwise, the queue should be active, maybe with some
         * events registered.
         */
        case SU_QUSED:
        default:

            /*
             * Mark the queue as being deleted so ISRs stop sending
             * messages on it.
             */
            kqp->su_state = SU_QAVAIL;

            /* If a task is blocked on this queue, wake it up. */
            if (kqp->su_waiting_tid != SU_INVALID_HANDLE)
            {
                read_lock(&tasklist_lock);
                t = find_task_by_pid(kqp->su_waiting_tid);
                if (t)
                    wake_up_process(t);
                /* Ignore the case where process id not found. It was checked
                 * for validity before it was put into that field and if the
                 * task was killed, it doesn't need to be woken.
                 */
                read_unlock(&tasklist_lock);
            }

            /* When queue is deleted, wake any threads waiting on the queue's
             * file descriptor. The file's poll method will determine that the
             * queue is freed and return appropriately.
             */
            wake_up_all(&(kqp->suqwaiters));

            break;
    }

    /*
     * No need to clear out remaining fields in qhandle because suCreateQueue()
     * will be doing this when the queue gets allocated again.
     */

    return 0;
}

/*
 * DESCRIPTION: su_queue_rmqueue_nodes
 *     This function walks a linked list, removing nodes and freeing the
 *     memory associated with each node.
 *
 * INPUTS:
 *     head    - The head of a linked list of type SU_MINNODE.
 *
 * OUTPUTS: None
 *
 * IMPORTANT NOTES:
 *     Although the linked list of nodes is always ended by a NULL pointer
 *     stored in the subqueue, the value of this NULL pointer is never changed.
 *     Therefore, it is not necessary to disable interrupts or hold locks.
 *
 */
static void su_queue_rmqueue_nodes (SU_MINNODE * head)
{
    SU_MINNODE * node, * tmpptr;

    node = head;
    do {
        if (! SU_IN_SUAPI_MEMORY(node))
        {
            suPanic(SU_PANIC_DELETE_QUEUE_2, 2,
                    SU_SIZE_32, SU_RSET(EFAULT,SU_EFAULT),
                    SU_SIZE_32, node);
            return;
        }

        /* Calling suapi_internal_free_mem() instead of suDeleteMessage() so
         * that we don't have to add sizeof SU_MSG_HDR only to have
         * suDeleteMessage() subtract sizeof SU_MSG_HDR before calling
         * suFreeMem().
         */
        if ((tmpptr = node->su_n_next) != NULL)
            suapi_internal_free_mem(node,NULL);

    } while ((node = tmpptr) != NULL);

    return;
}

#ifdef CONFIG_SUAPI
/*
 * DESCRIPTION: suapi_register_event_mask_with_queue_ioctl
 *     This service provides the capability of multiple waits for messages
 *     by registering a task and an event mask with the given queue. When a
 *     message is sent to the queue and no task is waiting for messages,
 *     the events denoted by event_mask are set for the task. If a task is
 *     waiting on the queue when the message send occurs, it is
 *     implementation-dependent whether the events are still set. If the
 *     given task is already registered for this queue, the task and
 *     event_mask replaces the registered event_mask. An event mask of 0
 *     removes the event notification for this queue to this task.
 *
 * INPUTS:
 *     qhandle is handle of the queue to be associated with event
 *     notification.
 *
 *     thandle is handle of task which is to receive event notification;
 *     SU_SELF is equivalent to the calling task's handle
 *
 *     event_mask Bit mask of events to be set for task when a message send
 *     operation occurs on queue
 *
 * OUTPUTS:
 *     Returns errors when detected
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_register_event_mask_with_queue_ioctl(int arg)
{
    int rvalue = 0;
    SU_QUEUE * kqp;
    pid_t pid;
    struct SuapiQueueRegEvMask SuapiQueueRegEvMask;

    if (copy_from_user(&SuapiQueueRegEvMask,
                       (struct SuapiQueueRegEvMask *) arg,
                       sizeof(struct SuapiQueueRegEvMask)))
        return -EFAULT;

    if (SU_SELF_PID == SuapiQueueRegEvMask.thandle)
        pid = current->pid;
    else
        pid = SuapiQueueRegEvMask.thandle;

    kqp = SU_QH2QPTR(SuapiQueueRegEvMask.qhandle);

    /*
     * Stop ISRs from sending messages to the queue until we can check for a
     * valid queue, set registration variables, and determine whether or not
     * to set the event based on the contents of the queue.
     */
    spin_lock_bh(&su_queues_lock);

    if ( kqp == NULL || kqp->su_state == SU_QAVAIL)
    {
        spin_unlock_bh(&su_queues_lock);
        return -ENOENT;
    }

    if (SuapiQueueRegEvMask.event_mask != SU_NO_EVENT_MASK)
    {
        /*
        * Check if the event mask is valid
        */
        /* Pass thandle to verify event instead of pid. If thandle is SU_SELF,
        * the verify function will not have to look up its own PID.
        */
        rvalue =
            suapi_internal_verify_alloc_event_mask(SuapiQueueRegEvMask.thandle,
                                               SuapiQueueRegEvMask.event_mask);
        if ( 0 > rvalue)
        {
            spin_unlock_bh(&su_queues_lock);
            return rvalue;
        }
    }

    /* Reset the event registration for the following reasons:
     * 1. The caller is trying to set the event to SU_NO_EVENT_MASK. This will
     *    allow the caller to reset the registration if the previously
     *    registered task is killed after the above event verification.
     * 2. There are no events registered with this queue.
     * 3. The same process is already registered and wants to change the events.
     */
    if (SuapiQueueRegEvMask.event_mask == SU_NO_EVENT_MASK
        || kqp->su_emask == SU_NO_EVENT_MASK
        || pid == kqp->su_event_thandle)
    {
        if ((! su_queue_is_empty(kqp)) && (SuapiQueueRegEvMask.event_mask != SU_NO_EVENT_MASK))
        {
            /*
             * THEN there is already a message on the queue and
             * the queue's event mask is not 0, so
             * post the event(s) to the task.
             */
            rvalue = suapi_internal_set_event_mask(SuapiQueueRegEvMask.thandle,
                                                   SuapiQueueRegEvMask.event_mask, SU_NOT_IN_ISR);
            if ( 0 > rvalue)
            {
                spin_unlock_bh(&su_queues_lock);
                return rvalue;
            }
        }

        kqp->su_emask = SuapiQueueRegEvMask.event_mask;
        kqp->su_event_thandle = pid;
    }
    else
    {
        /* error - another task has already registered an event with
         * this queue */
        rvalue = -EBUSY;
    }

    spin_unlock_bh(&su_queues_lock);
    return rvalue;
}
#endif /* CONFIG_SUAPI */

/*
 * DESCRIPTION: suapi_receive_message_from_queue_ioctl
 *     This service removes a message from the head of the queue qhandle
 *     and returns it to the calling application if the queue is
 *     not empty. This service waits at least timeout milliseconds, if the
 *     queue is empty. If timeout equals SU_NOWAIT, the service does not
 *     block and returns SU_EEMPTY if no messages are available.
 *
 * INPUTS:
 *         qhandle is the handle to the queue.
 *         timeout is the wait time in milliseconds Special values are
 *             SU_NOWAIT and SU_WAIT_FOREVER. The values 0 and 0xFFFFFFFF are
 *                 reserved.
 *
 * OUTPUTS:
 *         On success, this service copies the message pointer to user space
 *         and returns 0.
 *         On failure, this service returns a negative error value.
 *
 * IMPORTANT NOTES:
 *     none
 */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
int suapi_receive_message_from_queue_ioctl(int arg)
{
    int rval = 0;
    SU_QUEUE * kqp;
    union SuapiRcvMsgFromQueue SuapiRcvMsgFromQueue;
    Suapi_Kernel_Timer local_timer;
    Suapi_Tasklet_Handler_Data pend_info;

    if (copy_from_user(&SuapiRcvMsgFromQueue,
                       (union SuapiRcvMsgFromQueue *) arg,
                       sizeof(union SuapiRcvMsgFromQueue)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (copy_to_user((union SuapiRcvMsgFromQueue *) arg,
                         &SuapiRcvMsgFromQueue,
                         sizeof(union SuapiRcvMsgFromQueue)))
        return -EFAULT;

    if (0 > SuapiRcvMsgFromQueue.data.timeout)
        return -ERANGE;

    kqp = SU_QH2QPTR(SuapiRcvMsgFromQueue.data.qhandle);

    spin_lock_bh(&su_queues_lock);

    if ( kqp == NULL || kqp->su_state == SU_QAVAIL)
    {
        spin_unlock_bh(&su_queues_lock);
        return -ENOENT;
    }
    /* If get here, no messages exist. If timeout is 0, user doesn't want
     * to wait, so return with ENOMSG.
     */
    if (SuapiRcvMsgFromQueue.data.timeout == 0 && su_queue_is_empty(kqp))
    {
        spin_unlock_bh(&su_queues_lock);
        return -ENOMSG;
    }

    if (!su_queue_is_empty(kqp))
    {
        void * msg;
        if (kqp->su_qtype == IS_FIFO_QUEUE)
            msg = su_queue_dequeue_fifo(kqp);
        else
            msg = su_queue_dequeue_priority(kqp);
        /* convert address to user space pointer. */
        SuapiRcvMsgFromQueue.message = SU_KERNEL_TO_USER(msg);
        spin_unlock_bh(&su_queues_lock);
        rval = copy_to_user((union SuapiRcvMsgFromQueue *) arg,
                            &SuapiRcvMsgFromQueue,
                            sizeof(union SuapiRcvMsgFromQueue));
        return rval;

    }

    pend_info.tasklet_done = 0;
    if (SuapiRcvMsgFromQueue.data.timeout != LONG_MAX)
    {
        pend_info.pid = current->pid;
        rtc_sw_task_init(&local_timer.ltimer, suapi_tasklet_handler_fn, (unsigned long)&pend_info);
        rtc_sw_task_schedule(SuapiRcvMsgFromQueue.data.timeout, &local_timer.ltimer);
    }

    for(;;)
    {
        /* Second and subsequent times through the loop, it is possible that
         * the waiting task was interrupted by receiving a signal, so handle
         * that case here.
         */
        if (signal_pending(current))
        {
            rval = -EINTR;
            break;
        }
        
        /* If message exists on queue, break out of loop and receive it. */
        if (! su_queue_is_empty(kqp))
        {
            rval = 0;
            break;
        }
        /* At this point, the queue is empty and the caller wants to wait. */
        if (kqp->su_waiting_tid != SU_INVALID_HANDLE)
        {
            rval = -EBUSY;
            break;
        }
        else
        {
            kqp->su_waiting_tid = current->pid;

            /* Wait forever. */
            set_current_state(TASK_INTERRUPTIBLE);
            spin_unlock_bh(&su_queues_lock);
            schedule();

            /* Go into deep sleep. */
            if (unlikely(current->flags & PF_FREEZE))
                refrigerator(PF_FREEZE);

            spin_lock_bh(&su_queues_lock);
 
            /* If message does not exist on queue and timeout */
            if (SuapiRcvMsgFromQueue.data.timeout != LONG_MAX)
            {
                if (pend_info.tasklet_done)
                {
                    /* In the case of timeout and then deletion, this will
                     * return the timeout error but leave the su_waiting_tid
                     * value alone in case the queue was recreated and another
                     * task is already waiting again on this queue.
                     */
                    if (SU_QAVAIL != kqp->su_state
                         && kqp->su_waiting_tid == current->pid)
                    {
                        kqp->su_waiting_tid = SU_INVALID_HANDLE;
                    }
                    rval = -ETIMEDOUT;
                    break;
                }
            }

            if (SU_QAVAIL == kqp->su_state
                || kqp->su_waiting_tid != current->pid)
            {
                /* Then queue was deleted while this thread waited on it.
                 * Explicitly don't reset su_waiting_tid.
                 */
                rval = -EIDRM;
                break;
            }
            else
            {
                /* it's okay to do this here because the lock is held and if
                 * this service decides to wait again, it will reset this
                 * before releasing the lock.
                 */
                kqp->su_waiting_tid = SU_INVALID_HANDLE;
            }
            if (! su_queue_is_empty(kqp))
            {
                rval = 0;
                kqp->su_waiting_tid = SU_INVALID_HANDLE;
                break;
            }
        } /* ENDIF no other thread is waiting on this queue */
    } /* END FOR EVER */

    set_current_state(TASK_RUNNING);

    /* An rval of 0 indicates that there is a message on the queue. We know
     * that this is still true since we hold the lock.
     */
    if (rval == 0)
    {
        void * msg;

        if (kqp->su_qtype == IS_FIFO_QUEUE)
            msg = su_queue_dequeue_fifo(kqp);
        else
            msg = su_queue_dequeue_priority(kqp);
        /* convert address to user space pointer. */
        SuapiRcvMsgFromQueue.message = SU_KERNEL_TO_USER(msg);
    }

    if (SuapiRcvMsgFromQueue.data.timeout != LONG_MAX)
    {
        if (!pend_info.tasklet_done)
        {
            rtc_sw_task_kill(&local_timer.ltimer);
        }
    }

    spin_unlock_bh(&su_queues_lock);

    if (rval == 0)
        copy_to_user((union SuapiRcvMsgFromQueue *) arg,
                        &SuapiRcvMsgFromQueue,
                        sizeof(union SuapiRcvMsgFromQueue));
    return rval;
}
#else
int suapi_receive_message_from_queue_ioctl(int arg)
{
    int rval = 0;
    long expires = 0;
    SU_QUEUE * kqp;
    union SuapiRcvMsgFromQueue SuapiRcvMsgFromQueue;

    if (copy_from_user(&SuapiRcvMsgFromQueue,
                       (union SuapiRcvMsgFromQueue *) arg,
                       sizeof(union SuapiRcvMsgFromQueue)))
        return -EFAULT;

    /* Attempt to copy data to output address to ensure it is writable. */
    if (copy_to_user((union SuapiRcvMsgFromQueue *) arg,
                         &SuapiRcvMsgFromQueue,
                         sizeof(union SuapiRcvMsgFromQueue)))
        return -EFAULT;

    kqp = SU_QH2QPTR(SuapiRcvMsgFromQueue.data.qhandle);

    if (0 > SuapiRcvMsgFromQueue.data.timeout)
        return -ERANGE;

    /* Compute expiration here so that it is not recalculated each time through
     * the loop. If timeout is LONG_MAX (wait forever) or 0 (don't wait), the
     * expires variable will never be used.
     */
    expires = msecs_to_jiffies(SuapiRcvMsgFromQueue.data.timeout);

    spin_lock_bh(&su_queues_lock);

    if ( kqp == NULL || kqp->su_state == SU_QAVAIL)
    {
        spin_unlock_bh(&su_queues_lock);
        return -ENOENT;
    }

    for(;;)
    {
        /* If message exists on queue, break out of loop and receive it. */
        if (! su_queue_is_empty(kqp))
        {
            rval = 0;
            break;
        }
        /* If get here, no messages exist. If timeout is 0, user doesn't want
         * to wait, so return with ENOMSG.
         */
        if (SuapiRcvMsgFromQueue.data.timeout == 0)
        {
            rval = -ENOMSG;
            break;
        }
        /* Second and subsequent times through the loop, it is possible that
         * the waiting task was interrupted by receiving a signal, so handle
         * that case here.
         */
        if (signal_pending(current))
        {
            rval = -EINTR;
            break;
        }
        /* At this point, the queue is empty and the caller wants to wait. */
        if (kqp->su_waiting_tid != SU_INVALID_HANDLE)
        {
            rval = -EBUSY;
            break;
        }
        else
        {
            kqp->su_waiting_tid = current->pid;

            if (SuapiRcvMsgFromQueue.data.timeout == LONG_MAX)
            {
                /* Wait forever. */
                set_current_state(TASK_INTERRUPTIBLE);
                spin_unlock_bh(&su_queues_lock);
                schedule();

                /* Go into deep sleep. */
                if (unlikely(current->flags & PF_FREEZE))
                    refrigerator(PF_FREEZE);

                spin_lock_bh(&su_queues_lock);
            }
            else
            {
                /* User wants to wait for specific timeout. */
                if (expires >= MAX_SCHEDULE_TIMEOUT)
                {
                    kqp->su_waiting_tid = 0;
                    rval = -ERANGE;
                    break;
                }

                set_current_state(TASK_INTERRUPTIBLE);
                spin_unlock_bh(&su_queues_lock);
                expires = schedule_timeout(expires);

                /* Go into deep sleep. */
                if (unlikely(current->flags & PF_FREEZE))
                    refrigerator(PF_FREEZE);

                spin_lock_bh(&su_queues_lock);
                if (0 >= expires)
                {
                    /* In the case of timeout and then deletion, this will
                     * return the timeout error but leave the su_waiting_tid
                     * value alone in case the queue was recreated and another
                     * task is already waiting again on this queue.
                     */
                    if (SU_QAVAIL != kqp->su_state
                        && kqp->su_waiting_tid == current->pid)
                    {
                        kqp->su_waiting_tid = SU_INVALID_HANDLE;
                    }
                    rval = -ETIMEDOUT;
                    break;
                }
            }

            if (SU_QAVAIL == kqp->su_state
                || kqp->su_waiting_tid != current->pid)
            {
                /* Then queue was deleted while this thread waited on it.
                 * Explicitly don't reset su_waiting_tid.
                 */
                rval = -EIDRM;
                break;
            }
            else
            {
                /* it's okay to do this here because the lock is held and if
                 * this service decides to wait again, it will reset this
                 * before releasing the lock.
                 */
                kqp->su_waiting_tid = SU_INVALID_HANDLE;
            }
        } /* ENDIF no other thread is waiting on this queue */
    } /* END FOR EVER */

    set_current_state(TASK_RUNNING);

    /* An rval of 0 indicates that there is a message on the queue. We know
     * that this is still true since we hold the lock.
     */
    if (rval == 0)
    {
        void * msg;

        if (kqp->su_qtype == IS_FIFO_QUEUE)
            msg = su_queue_dequeue_fifo(kqp);
        else
            msg = su_queue_dequeue_priority(kqp);
        /* convert address to user space pointer. */
        SuapiRcvMsgFromQueue.message = SU_KERNEL_TO_USER(msg);
    }

    spin_unlock_bh(&su_queues_lock);

    if (rval == 0)
        rval = copy_to_user((union SuapiRcvMsgFromQueue *) arg,
                            &SuapiRcvMsgFromQueue,
                            sizeof(union SuapiRcvMsgFromQueue));

    return rval;
}
#endif

/*
 * DESCRIPTION: su_queue_dequeue_fifo
 *     This function dequeues an SU_MINNODE object from the front of a FIFO
 *     queue.
 *
 * INPUTS:
 *     kqp - Pointer to FIFO queue.
 *
 * OUTPUTS:
 *     node - Pointer to node dequeued.
 *
 * IMPORTANT NOTES:
 *     A valid pointer to a queue structure is assumed to have been passed.
 *     Check for an empty queue has been done prior to calling this function.
 *     It is assumed that soft interrupts are disabled and the su_queues_lock
 *         spinlock is held.
 *     It is assumed that su_subqueue_remove_head() will not return if it
 *         encounters an error.
 */
static SU_MINNODE * su_queue_dequeue_fifo(SU_QUEUE * const kqp)
{
    SU_SUBQUEUE_HEAD  *const qhp = &kqp->su_subqueue[SU_PRIO_LOW];
    SU_MINNODE        *node       = NULL;

    node = (SU_MINNODE *)su_subqueue_remove_head(qhp);
    kqp->su_qmsgcount--;

    return(node);
}

/*
 * DESCRIPTION: su_queue_dequeue_priority
 *     This function dequeues an SU_MINNODE object from a priority queue.
 *
 * INPUTS:
 *     kqp - Pointer to priority queue.
 *
 * OUTPUTS:
 *     node - Pointer to node dequeued.
 *
 * IMPORTANT NOTES:
 *     A valid pointer to a queue structure is assumed to have been passed.
 *     Check for an empty queue has been done prior to calling this function.
 *     It is assumed that soft interrupts are disabled and the su_queues_lock
 *         spinlock is held.
 *     It is assumed that su_subqueue_remove_head() will not return if it
 *         encounters an error.
 */
static SU_MINNODE * su_queue_dequeue_priority(SU_QUEUE * const kqp)
{
    SU_SUBQUEUE_HEAD  * qhp;
    SU_MINNODE *node = NULL;
    UINT32 highest_priority;

    /* SU_HIGHEST_PRIORITY_BIT gets the index of the highest priority subqueue
     * that contains messages. If the information is corrupted, it returns the
     * lowest proiority index.
     */
    highest_priority = SU_HIGHEST_PRIORITY_BIT(kqp);
    qhp = kqp->su_subqueue + highest_priority;
    node = (SU_MINNODE *)su_subqueue_remove_head(qhp);

    kqp->su_qmsgcount--;

    if ( su_subqueue_is_empty(qhp) )
        SU_CLEAR_PRIORITY_BIT(kqp, highest_priority);

    return(node);
}

/* DESCRIPTION: su_subqueue_remove_head
 *     This function dequeues a SU_MINNODE object
 *     at the head of a priority subqueue.
 *
 * INPUTS:
 *     q  - Pointer to a priority subqueue.
 *
 * OUTPUTS: Pointer to dequeued node.
 *
 * IMPORTANT NOTES:
 *     q is assumed to be valid.
 *     It is assumed that soft interrupts are disabled and the su_queues_lock
 *         spinlock is held.
 *     It is assumed that a check for an empty queue has been done prior to
 *         calling this function.
 */
static SU_MINNODE * su_subqueue_remove_head(SU_SUBQUEUE_HEAD *const q)
{
    SU_MINNODE *const n = q->su_q_head;
    SU_MINNODE *next;
    SU_MINNODE *prev;
    SU_QUEUE_HANDLE qhandle;

    /* Use SU_IN_PARTITION_RANGE() for 'n' since su_subqueue_remove_head()
     * should not have been called when the queue is empty and therefore 'n'
     * should be pointing to a message header.
     */
    if ( ! SU_IN_PARTITION_RANGE(n) )
    {
        spin_unlock_bh(&su_queues_lock);
        qhandle = SU_SQP2QH(q);
        suPanic(SU_PANIC_RECEIVE_MESSAGE_2, 3,
                SU_SIZE_32, SU_RSET(EFAULT, SU_EFAULT),
                SU_SIZE_32, qhandle,
                sizeof(SU_SUBQUEUE_HEAD), q);
        return NULL;
    }

    next = n->su_n_next;
    prev = n->su_n_prev;

    /* Use SU_IN_SUAPI_MEMORY() for the next and prev pointers since they
     * may be pointing back into the subqueue's header.
     */
    if (! SU_IN_SUAPI_MEMORY(next) || ! SU_IN_SUAPI_MEMORY(prev) )
    {
        spin_unlock_bh(&su_queues_lock);
        qhandle = SU_SQP2QH(q);
        suPanic(SU_PANIC_RECEIVE_MESSAGE_3, 4,
                SU_SIZE_32, SU_RSET(EFAULT, SU_EFAULT),
                SU_SIZE_32, qhandle,
                SU_SIZE_32, n,
                sizeof(SU_MINNODE), n);
        return NULL;
    }

    q->su_q_head = next;
    next->su_n_prev = prev;

    return(n);
}

/*
 * DESCRIPTION: suapi_send_message_ioctl:
 *     This service sends the supplied message to the port designated by
 *     the supplied port handle.
 *
 * INPUTS:
 *     message - The message to be sent.
 *     phandle - The handle of the port to which the message will be sent.
 *
 * OUTPUTS:
 *      On success, returns 0.
 *      On failure, returns negative errno.
 *
 * IMPORTANT NOTES:
 *      N/A
 */
int suapi_send_message_ioctl(int arg)
{
    struct suSendMsgData {
        void * message;
        SU_PORT_HANDLE phandle;
    } suSendMsgData;

    void * kmsgp;

    if (copy_from_user(&suSendMsgData,
                       (struct suSendMsgData *) arg,
                       sizeof(struct suSendMsgData)))
        return -EFAULT;

    kmsgp = SU_USER_TO_KERNEL(suSendMsgData.message);

    if (! SU_IN_PARTITION_RANGE(SU_MSG2HDR(kmsgp)))
        return -EBADMSG;

    if ((suSendMsgData.phandle == SU_INVALID_HANDLE)
        || SU_PHOUTOFRANGE(suSendMsgData.phandle)
        || (! (SU_ISPHPID(suSendMsgData.phandle))
            && (SU_PH2PHPTR(suSendMsgData.phandle)->port_type==SU_LOGGER_PORT)))
        return -EINVAL;

    return(suapi_internal_send_message_to_port(suSendMsgData.phandle, kmsgp, SU_NOT_IN_ISR));
}

/*
 * DESCRIPTION: suapi_internal_send_message_to_port
 *
 * INPUTS:
 *      phandle - port handle to be sent
 *      kmsgp   - pointer to message
 *      in_isr  - indicates if the function is called in soft isr context.
 *
 * OUTPUTS:
 *      On success, returns 0.
 *      On failure, returns negative errno.
 *
 * IMPORTANT NOTES:
 *      phandle and kmsgp are assumed to be valid.
 */
int suapi_internal_send_message_to_port(SU_PORT_HANDLE phandle, void * kmsgp, int in_isr)
{
    SU_PORT_HANDLE_STRUCT * port;
    int returnval = 0;

    suSetMessagePortHandle(kmsgp, phandle);

    if (SU_ISPHPID(phandle))
    {
        struct task_struct *t;
        pid_t dest_pid = (pid_t) phandle;

        read_lock(&tasklist_lock);

        t = find_task_by_pid(dest_pid);

        if (t)
        {
            spin_lock_bh(&su_events_lock);

            if ( (t->su_state == SUAPI_TASK_STATE_WAIT_REPLY_MESSAGE)
                && (t->su_wait_flags == 0) )
            {
                t->su_wait_flags = (int) SU_KERNEL_TO_USER(kmsgp);
                spin_unlock_bh(&su_events_lock);
#if defined(CONFIG_MOT_FEAT_PM)
                if (in_isr)
                    mpm_handle_ioi();
#endif
                wake_up_process(t);
            }
            else
            {
                spin_unlock_bh(&su_events_lock);
                returnval = -ENOENT;
            }
        }
        else
        {
            returnval = -ESRCH;
        }

        read_unlock(&tasklist_lock);
    }
    else
    {
        port = SU_PH2PHPTR(phandle);

        /*
         * If destination's logging is enabled then by default enable the
         * logging of the replies to this message.
         */
        if (suLoggingEnabled(port))
            suSetMessageReplyLoggingBit(kmsgp, SU_LOGGING_ENABLED);

        /* log the message */
        if(SU_TEST_JUST_RETURN ==
                suapi_internal_log_message(kmsgp,SU_MSGACTION_SEND))
            return 0;

        returnval = suapi_internal_send_message_to_queue(
                                SU_QH2QPTR(port->port_transport.qhandle),
                                SU_MSG2HDR(kmsgp), in_isr);
    }

    return returnval;
}

static int suapi_internal_send_message_to_queue(SU_QUEUE * kqp, void * kmsghdrp, int in_isr)
{
    int saved_qmsgcount;
    struct task_struct *t;
    int ret = 0;

    spin_lock_bh(&su_queues_lock);

    if ( kqp == NULL || SU_QAVAIL == kqp->su_state )
    {
        spin_unlock_bh(&su_queues_lock);
        return -ENOENT;
    }

    /* Save message count to check if this is the first message in the queue */
    saved_qmsgcount = kqp->su_qmsgcount;

    if (kqp->su_qtype == IS_FIFO_QUEUE)
        ret = su_queue_enqueue_fifo(kqp, (SU_MINNODE *) kmsghdrp);
    else
        ret = su_queue_enqueue_priority(kqp, (SU_MINNODE *) kmsghdrp);

    if (ret != 0)
    {
        spin_unlock_bh(&su_queues_lock);
        return ret;
    }

    /*
     * There are three ways to wait for a message to be received:
     *  1) block inside of suReceiveMessageFromQueue(),
     *  2) wait on a file descriptor associated with the queue using select(),
     *     pselect(), or poll().
     *  3) wait on a SUAPI event associated with the queue using SUAPI event
     *     services.
     */
    /* 1) If a task is blocked within suReceiveMessageFromQueue, wake it up. */
    if (kqp->su_waiting_tid != SU_INVALID_HANDLE)
    {
        read_lock(&tasklist_lock);
        t = find_task_by_pid(kqp->su_waiting_tid);
        if (t)
        {
#if defined(CONFIG_MOT_FEAT_PM)
           if (in_isr)
               mpm_handle_ioi();
#endif
            wake_up_process(t);
        }
        /* Ignore the case where process id not found. It was checked
         * for validity before it was put into that field and if the
         * task was killed, it doesn't need to be woken.
         */
        read_unlock(&tasklist_lock);
    }

    /* 2) If this call to suSendMessageToQueue() moved the queue from an empty
     *    condition to a non-empty condition, then wake up any tasks which may
     *    have been waiting on the queue's file descriptor.
     */
    if (saved_qmsgcount == 0)
    {
        /* When message is sent, wake any threads waiting on the queue's
         * file descriptor. The file's poll method will determine that a
         * message has been delivered return appropriately.
         */
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
#if defined(CONFIG_MOT_FEAT_PM)
           if (in_isr)
               mpm_handle_ioi();
#endif
#endif
        wake_up_all(&(kqp->suqwaiters));
    }

#ifdef CONFIG_SUAPI
    /* 3) Post the registered events for queues defined under SUAPI. */
    if (0 != kqp->su_emask)
    {
        /* Post the event(s) to the task. Ignore any errors (such as task
         * no longer valid) since the message has already been sent.
         */
        (void) suapi_internal_set_event_mask(kqp->su_event_thandle,
                                             kqp->su_emask, in_isr);
    }
#endif /* CONFIG_SUAPI */

    spin_unlock_bh(&su_queues_lock);

    return 0;
}

/*
 * DESCRIPTION: suapi_get_num_messages_on_queue_ioctl
 *     This service retrieves the number of messages on the queue specified by
 *     qhandle.
 *
 * INPUTS:
 *     qhandle - Handle to the queue.
 *
 * OUTPUTS:
 *     Returns the number of messages on the queue.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_get_num_messages_on_queue_ioctl(SU_QUEUE_HANDLE qhandle)
{
    SU_QUEUE * kqp;

    kqp = SU_QH2QPTR(qhandle);

    /*
     * Interrupts are not disabled during this function. Even if interrupts
     * were disabled, the message count could become stale as soon as
     * interrupts are re-enabled.
     */
    if ( kqp == NULL || SU_QAVAIL == kqp->su_state )
        return -ENOENT;

    return(kqp->su_qmsgcount);
}

/*
 * DESCRIPTION: su_queue_enqueue_fifo
 *     This function enqueues a NODE object in a FIFO queue.
 *
 * INPUTS:
 *     kqp      - pointer to a generic queue.  No need to convert it to a
 *                FIFO pointer because there is nothing FIFO-specific
 *                about this processing.
 *     n        - Pointer to NODE to enqueue.
 *
 * OUTPUTS:
 *     Returns the retval received from su_subqueue_add_tail().
 *
 * IMPORTANT NOTES:
 *  The following is assumed:
 *     A valid queue pointer is passed.
 *     A valid minnode pointer is passed.
 *     Soft interrupts are disabled
 *     The su_queues_lock spinlock is held.
 */
static int su_queue_enqueue_fifo(SU_QUEUE * const kqp, SU_MINNODE *n)
{
    SU_SUBQUEUE_HEAD *const qhdr = &kqp->su_subqueue[SU_PRIO_LOW];
    int ret;

    if ((ret = su_subqueue_add_tail(qhdr, n)) == 0)
        ++(kqp->su_qmsgcount);

    return ret;
}

/*
 * DESCRIPTION: su_queue_enqueue_priority
 *     This function enqueues an SU_MINNODE object onto a priority queue.
 *
 * INPUTS:
 *     kqp      - Pointer to priority queue.
 *     n        - Pointer to NODE to enqueue.
 *
 * OUTPUTS:
 *     Returns the retval received from su_subqueue_add_tail().
 *
 *IMPORTANT NOTES:
 *  The following is assumed:
 *     A valid queue pointer is passed.
 *     A valid minnode pointer is passed.
 *     Soft interrupts are disabled.
 *     The su_queues_lock spinlock is held.
 */
static int su_queue_enqueue_priority(SU_QUEUE * const kqp, SU_MINNODE *n)
{
    UINT32 msgpriority = ((SU_MSG_HDR *)n)->priority;
    int ret;

    switch (msgpriority)
    {
        case SU_PRIO_HIGH:
        case SU_PRIO_MEDIUM:
        case SU_PRIO_LOW:
            break;
        default:
            msgpriority = SU_PRIO_LOW;
    }

    SU_SET_PRIORITY_BIT(kqp, msgpriority);

    if ((ret=su_subqueue_add_tail(&kqp->su_subqueue[msgpriority],n)) == 0)
        ++(kqp->su_qmsgcount);

    return ret;
}

/*
 * DESCRIPTION: su_subqueue_add_tail
 *     This function enqueues an SU_MINNODE object at the tail of a subqueue.
 *
 * INPUTS:
 *     q  - Pointer to a priority subqueue.
 *     n  - Pointer to node to be added to the end of the subqueue.
 *
 * OUTPUTS:
 *     Returns 0 on success.
 *     If pointers on  linked list have been corrupted, this function panics.
 *     If a panic occurs and this function is called from soft interrupt
 *         context, this function returns -EFAULT.
 *
 * IMPORTANT NOTES:
 *  The following is assumed:
 *     Soft interrupts are disabled
 *     The su_queues_lock spinlock is held.
 *     q and n are valid.
 *
 * Kernel datalogger panic will return if called from interrupt context and not
 *     return if called from thread context.
 */
static int su_subqueue_add_tail(SU_SUBQUEUE_HEAD *const q, SU_MINNODE *const n)
{
    SU_MINNODE *const pred = q->su_q_tailpred;
    SU_MINNODE *prednext;
    SU_QUEUE_HANDLE qhandle;

    /* Use SU_IN_SUAPI_MEMORY() since either of these could be pointing into the
     * subqueue's header.
     */
    if (! SU_IN_SUAPI_MEMORY(pred))
    {
        if (! in_interrupt())
            spin_unlock_bh(&su_queues_lock);

        qhandle = SU_SQP2QH(q);

        suPanic(SU_PANIC_SEND_MESSAGE_3, 3,
                SU_SIZE_32, SU_RSET(EFAULT, SU_EFAULT),
                SU_SIZE_32, qhandle,
                sizeof(SU_SUBQUEUE_HEAD), q);
        return -EFAULT;
    }

    prednext = pred->su_n_next;

    if (! SU_IN_SUAPI_MEMORY(prednext))
    {
        if (! in_interrupt())
            spin_unlock_bh(&su_queues_lock);

        qhandle = SU_SQP2QH(q);

        suPanic(SU_PANIC_SEND_MESSAGE_4, 4,
                SU_SIZE_32, SU_RSET(EFAULT, SU_EFAULT),
                SU_SIZE_32, qhandle,
                SU_SIZE_32, pred,
                sizeof(SU_MINNODE), pred);
        return -EFAULT;
    }

    n->su_n_next = prednext;
    n->su_n_prev = pred;

    pred->su_n_next = n;
    q->su_q_tailpred = n;

    return 0;
}

/*
 * DESCRIPTION: suapi_internal_recreate_message
 *     This service creates a new message with the header information of the
 *     message passed in.
 *     The size information from the message is not copied.
 *     The message data is not copied.
 *
 * INPUTS:
 *     The message size and contents are implementation defined.
 *     msgp   -- The pointer to message that is to be recreated.
 *     size   -- The new size which the caller requires for the data portion of
 *               the recreated message.
 *
 * OUTPUTS:
 *     On success, returns a pointer to the data portion of the new messagee.
 *     On failure, returns a negative error value.
 *
 * NOTES:
 *     It is assumed that kmsgp points to valid SUAPI partition memory.
 *     Calling functions should use IS_ERR() to check for a valid pointer.
 */
void * suapi_internal_recreate_message(void * kmsgp, UINT32 size)
{
    SU_MSG_HDR *message;
    SU_MSG_HDR *orig_msg;
    int bestfitpartition;

    /* Get access to the original message's header. */
    orig_msg = SU_MSG2HDR(kmsgp);

    /* Allocate enough memory for message header plus application data. */
    message = suapi_internal_alloc_mem(size + SU_SIZEOF_MSG_HDR,
                                       &bestfitpartition);

    if (IS_ERR(message))
        return message;

    /* Initialize message header. */
    message->su_minnode.su_n_next = message->su_minnode.su_n_prev = NULL;
    message->length = size;
    message->type = orig_msg->type;
    message->replyport = orig_msg->replyport;
    message->reply_logging = orig_msg->reply_logging;
    message->destport = orig_msg->destport;
    message->priority = orig_msg->priority;

    /* Hide message header from the application. */
    return SU_HDR2MSG(message);
}

/*
 * DESCRIPTION: suapi_enable_port_logging
 *     This function enables logging on the provided port.
 *
 * INPUTS:
 *     phandle is the handle of the port to log on.
 *
 * OUTPUTS:
 *     0 on success, negative for an error code.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_enable_port_logging(SU_PORT_HANDLE phandle)
{
    SU_PORT_HANDLE_STRUCT * port;

    if (SU_ISPHPID(phandle))
        return 0;

    /* The phandle must be in the range of valid port handles. */
    if (SU_PHOUTOFRANGE(phandle))
        return -ENOENT;

    port = SU_PH2PHPTR(phandle);

    port->logging_enabled = SU_LOGGING_ENABLED;

    return 0;
}

/*
 * DESCRIPTION: suapi_disable_port_logging
 *     This function disables logging on the provided port.
 *
 * INPUTS:
 *     phandle is the handle of the port to stop logging.
 *
 * OUTPUTS:
 *     0 on success, negative for an error code.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_disable_port_logging(SU_PORT_HANDLE phandle)
{
    SU_PORT_HANDLE_STRUCT * port;

    if (SU_ISPHPID(phandle))
        return 0;

    /* The phandle must be in the range of valid port handles. */
    if (SU_PHOUTOFRANGE(phandle))
        return -ENOENT;

    port = SU_PH2PHPTR(phandle);

    port->logging_enabled = SU_LOGGING_DISABLED;

    return 0;
}

/*
 * DESCRIPTION: suapi_create_port_ioctl
 *     This function finds a free port and initializes it based on the
 *     port_type parameter.
 *
 * INPUTS:
 *     port_type is the type of port to be created.  It is assumed to be either
 *         SU_LOGGER_PORT or SU_MESSAGE_PORT
 *
 * OUTPUTS:
 *     On Success: Port handle of newly-allocated port is copied to user
 *                 space and 0 is returned.
 *     On Failure: Negative error code is returned.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_create_port_ioctl(int arg, int port_type)
{
    int index = 0;
    int ret;
    union suCreatePortData sucpdata;
    sucpdata.in.qh = 0;
    sucpdata.in.portid = 0;

    if (port_type == SU_MESSAGE_PORT)
    {
        if (copy_from_user(&sucpdata,
                           (union suCreatePortData *) arg,
                           sizeof(union suCreatePortData)))
            return -EFAULT;
    }

    /* Attempt to copy data to output address to ensure it is writable. */
    if((ret = put_user((SU_PORT_HANDLE) -1,(SU_PORT_HANDLE *) arg)))
        return ret;

    while (index < (int)SU_NPORT)
    {
        spin_lock_bh(&su_queues_lock);
        if (suPortTable_ptr[index].port_type == SU_UNALLOCATED_PORT)
        {
            suPortTable_ptr[index].port_type = port_type;
            spin_unlock_bh(&su_queues_lock);
            break;
        }
        index++;
        spin_unlock_bh(&su_queues_lock);
    }
    if (index == SU_NPORT)
        return -ENOSPC;

    /* Initialize the port's fields */
    suPortTable_ptr[index].port_transport.qhandle = sucpdata.in.qh;
    suPortTable_ptr[index].port_id = sucpdata.in.portid;
    suPortTable_ptr[index].logging_enabled = SU_LOGGING_DISABLED;
    suPortTable_ptr[index].routing_mode = SU_ROUTE_NORMAL;

    put_user(SU_PINDEX2PH(index), (SU_PORT_HANDLE *) arg);

    return 0;
}

/*
 * DESCRIPTION: suapi_delete_port_ioctl
 * This service deletes the port designated by the given handle.
 *
 * INPUTS:
 *  phandle - handle to the port to be deleted
 *
 *
 * OUTPUTS:
 *  On success, returns 0.
 *  On failure, returns negative errno value.
 *
 * IMPORTANT NOTES:
 *  N/A
 */
int suapi_delete_port_ioctl(SU_PORT_HANDLE phandle)
{
    SU_PORT_HANDLE_STRUCT * port;

   /*
    * Locking around this will cause this to wait to delete this port
    * if another task is already within a queue or port critical
    * section.
    */
    spin_lock_bh(&su_queues_lock);

    /*
     * The high order bit must be set and phandle must be in the range
     * of valid port handles.
     */
    if (SU_ISPHPID(phandle))
    {
        spin_unlock_bh(&su_queues_lock);
        return -EPROTO;
    }
    if (SU_PHOUTOFRANGE(phandle))
    {
        spin_unlock_bh(&su_queues_lock);
        return -ENOENT;
    }

    port = SU_PH2PHPTR(phandle);

    port->port_type = SU_UNALLOCATED_PORT;

    spin_unlock_bh(&su_queues_lock);

    return 0;
}

/*
 * DESCRIPTION : suapi_logging_enabled
 *     This ioctl function checks whether logging is enabled on a port.
 *     
 * INPUTS :
 *     phandle - The port handle
 *     
 * OUTPUTS :
 *     Returns 1 if enabled
 *     else returns 0.
 *     
 * IMPORTANT NOTES :
 *      N/A
 */
int suapi_port_logging_enabled(SU_PORT_HANDLE phandle)
{
    SU_PORT_HANDLE_STRUCT * port;

    if (SU_ISPHPID(phandle))
        return 0;

    /* The phandle must be in the range of valid port handles. */
    if (SU_PHOUTOFRANGE(phandle))
        return -ENOENT;

    port = SU_PH2PHPTR(phandle);

    if (port->logging_enabled)
        return 1;
    else
        return 0;
}

/*
 * DESCRIPTION : suapi_get_portid
 *     This ioctl gets the port id from the port handle
 *
 * INPUTS :
 *     phandle - The port handle
 *
 * OUTPUTS :
 *     The message port id.
 *
 * IMPORTANT NOTES :
 *     N/A
 */
int suapi_get_portid(SU_PORT_HANDLE phandle)
{
    SU_PORT_HANDLE_STRUCT * port;

    if (SU_ISPHPID(phandle))
        return 0;

    /* The phandle must be in the range of valid port handles. */
    if (SU_PHOUTOFRANGE(phandle))
        return -ENOENT;

    port = SU_PH2PHPTR(phandle);

    return (port->port_id);
}

/*
 * DESCRIPTION : suapi_get_routing
 *     This ioctl returns the current routing mode for the port.
 *     
 * INPUTS:
 *     port - The port to find the routing mode of.
 *     
 * OUTPUTS:
 *     routing mode of the port.
 *     
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_get_port_routing(SU_PORT_HANDLE phandle)
{
    SU_PORT_HANDLE_STRUCT * port;

    if (SU_ISPHPID(phandle))
        return SU_ROUTE_NORMAL;

    /* The phandle must be in the range of valid port handles. */
    if (SU_PHOUTOFRANGE(phandle))
        return -ENOENT;

    port = SU_PH2PHPTR(phandle);

    return (port->routing_mode);
}

/*
 * DESCRIPTION: suapi_set_port_routing
 *     This ioctl sets the specified routing mode for the port.
 *
 * INPUTS:
 *     port - The port to set the mode for.
 *     mode - The mode to which the port routing will be set.
 *
 * OUTPUTS:
 *    Returns 0 on success.
 *    Returns EFAULT if memory copy fails and ENOENT if phandle is invalid.
 *
 * IMPORTANT NOTES:
 *     N/A
 */
int suapi_set_port_routing(int arg)
{
    SU_PORT_HANDLE_STRUCT * port;

    struct suSetPortRoutingData suSetPortRoutingData;

    if (copy_from_user(&suSetPortRoutingData,
                           (struct suSetPortRoutingData *) arg,
                           sizeof(struct suSetPortRoutingData)))
        return -EFAULT;

    if (SU_ISPHPID(suSetPortRoutingData.phandle))
        return 0;

    if (SU_PHOUTOFRANGE(suSetPortRoutingData.phandle))
            return -ENOENT;

    port = SU_PH2PHPTR(suSetPortRoutingData.phandle);
    port->routing_mode = suSetPortRoutingData.mode;
    return 0;
}
