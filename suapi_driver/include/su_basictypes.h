// Copyright (c) 2006, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

/*
 *  DESCRIPTION:
 *      The su_basictypes.h header file contains constants and basic types
 *      that are described in the user guide and reference manual. 
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

#ifndef SU_BASICTYPES_H
#define SU_BASICTYPES_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/************** CONSTANTS ****************************************************/

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

/************** STRUCTURES, ENUMS, AND TYPEDEFS ******************************/

/* Basic Types defined in the SUAPI User and Reference Manual, Appendix B */

typedef signed char INT8;
typedef unsigned char UINT8;
typedef signed short int INT16;
typedef unsigned short int UINT16;
typedef signed int INT32;
typedef unsigned int UINT32;


typedef signed long long int INT64;
typedef unsigned long long int UINT64;

#if 0
/* Linux definition */
typedef bool BOOL;
typedef bool BOOLEAN;
#else
/* P2K definition */
typedef unsigned char BOOL;
typedef unsigned char BOOLEAN;
#endif

#ifdef __cplusplus
}
#endif
#endif

