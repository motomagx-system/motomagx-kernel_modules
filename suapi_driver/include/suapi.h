// Copyright (c) 2006,2007, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

/*
 *  DESCRIPTION:
 *      The suapi.h header file contains constants, structures, enums,
 *  typedefs, function prototypes, class definitions, and global
 *  variable external references that comprise the user interface to SUAPI.
 *
 *  Use suapi.h in your C file it will pull in anything else needed
 *  to use the SU API.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2007-01-26 Motorola       Update release number.
// 2007-03-19 Motorola       Update release number.
// 2007-04-02 Motorola       Include errno.h before unistd.h.

#ifndef SUAPI_INCLUDE 
#define SUAPI_INCLUDE  

#ifdef __cplusplus
extern "C" {
#endif 

/************** HEADER FILE INCLUDES *****************************************/
#include <stdarg.h>
#include <stddef.h>

/* prevents redeclaration errors */
#include <limits.h>

#include "su_basictypes.h"

#include <sys/types.h>
#include <errno.h>
#include <linux/unistd.h>
#include <linux/su_linux_extensions.h>

/************** CONSTANTS ****************************************************/

/* SU_INVALID_HANDLE is used to identify invalid handles.
 *
 *  It should be cast to the object's proper handle type.
 */
#define SU_INVALID_HANDLE 0


/* The SU_ISR and SU_SELF defines are special values used to indicate
   interrupt subroutine (ISR) context and the shorthand task handle for
   the calling task. */

#define SU_ISR  ((SU_TASK_HANDLE) (-1))
#define SU_SELF ((SU_TASK_HANDLE) (-2))

/* The following constants are used to identify the size of values passed to
 * variable argument routines when data values are passed instead of pointers
 * to data.
 */
#define SU_SIZE_8  ((int)-1)  /* To pass eight bits of data */
#define SU_SIZE_16 ((int)-2)  /* To pass sixteen bits of data */
#define SU_SIZE_32 ((int)-3)  /* To pass 32 bits of  data */
#define SU_SIZE_64 ((int)-4)  /* To pass 64 bits of  data */

/* ------------------------ events ------------------------------- */
/*
 * Defines the invalid event number.
*/
#define SU_NO_EVENT 32

/*
 * Defines special event mask.
*/

#define SU_NO_EVENT_MASK 0x00

/* ------------------------ run level ---------------------------- */

/* The following constants are used to identify SUAPI defined run levels. */
#define SU_RUN_LEVEL_INITIAL        0
#define SU_RUN_LEVEL_GLOBALS_INITED 16
#define SU_RUN_LEVEL_SUAPI_INITED   32
#define SU_RUN_LEVEL_SCHED_STARTED  64
#define SU_RUN_LEVEL_SHUTDOWN       192
#define SU_RUN_LEVEL_PANIC          248

/* ------------------------ time services ------------------------ */

/* The SU_NOWAIT and SU_WAIT_FOREVER defines are special values used
   to indicate special wait conditions. */

#define SU_NOWAIT           0   
#define SU_WAIT_FOREVER             LONG_MAX               


/* SU_TIME_MAX and SU_TIME_MIN define the values returned by 
 * suTicksToMsec to indicate overflow. 
 */

static const long SU_TIME_MAX __attribute__ ((deprecated)) = LONG_MAX;
static const long SU_TIME_MIN __attribute__ ((deprecated)) = LONG_MIN;

/* ------------------------ cache services ------------------------ */

/*
 * The following constants are used to indicate which cache to reference.
 * When a service is called with more than one cache, the order in which
 * each cache is processed is the order of the bit flags from right to left,
 * thus the values of the below constants dictate the order in which they
 * will be processed.
 */

#define SU_CACHE_L1_DATA  0x01
#define SU_CACHE_L1_INSTR 0x02

#define SU_CACHE_L1_ALL (SU_CACHE_L1_DATA | SU_CACHE_L1_INSTR)

#define SU_CACHE_L2_DATA  0x04
#define SU_CACHE_L2_INSTR 0x08

#define SU_CACHE_L2_ALL (SU_CACHE_L2_DATA | SU_CACHE_L2_INSTR)

#define SU_CACHE_L1_AND_L2_DATA  (SU_CACHE_L1_DATA | SU_CACHE_L2_DATA)
#define SU_CACHE_L1_AND_L2_INSTR  (SU_CACHE_L1_INSTR | SU_CACHE_L2_INSTR)

/* These defines are left for backwards compatibility only */
#define SU_CACHE_DATA  (SU_CACHE_L1_DATA | SU_CACHE_L2_DATA)
#define SU_CACHE_INSTR  (SU_CACHE_L1_INSTR | SU_CACHE_L2_INSTR)

/*
 * This define is used to reference all of the caches.
 * This should be updated if a cache is added or removed.
 */
#define SU_CACHE_ALL (SU_CACHE_L1_ALL | SU_CACHE_L2_ALL)

/*
 * The following constant is used to reference the entire cache when used
 * as the length for flushing or invalidating the cache
 */
#define SU_ENTIRE_CACHE ULONG_MAX


/* ------------------------ flag services ------------------------- */

/* Values used to indicate a flag's value */
#define SU_FLAG_CLEAR    0    /* Used to indicate a clear flag */
#define SU_FLAG_SET      1    /* Used to indicate a set flag */


/* ------------------------ low power services -------------------- */

/* These define the allowable states of a low power flag */
#define SU_LOWPWR_ENABLE     SU_FLAG_CLEAR /* indicates an enabled flag */
#define SU_LOWPWR_DISABLE    SU_FLAG_SET   /* indicates a disabled flag */

/* ------------------------ port/queue services ------------------- */


/* Message priorities may be low, medium, or high. */
#define SU_PRIO_LOW 0
#define SU_PRIO_MEDIUM 1
#define SU_PRIO_HIGH 2

/* The following values are used to indicate the type of action that cause
 * suPalLogMessage to be called.
 */
#define SU_MSGACTION_SEND        0
#define SU_MSGACTION_RECEIVE     1
#define SU_MSGACTION_REPLY       2

/* The following values are used to indicate the routing modes for external
 * testing.
 */
#define SU_ROUTE_NORMAL          0
#define SU_ROUTE_MIRROR          1
#define SU_ROUTE_INTERCEPT       2

/* The following values are used to indicate the return values of
 * suPalLogMessage.
 */
#define SU_TEST_JUST_RETURN      0
#define SU_TEST_PERFORM_ACTION   1

/* The following value is used to indicate that no return port is specified
 * when a message is created.
 */
#define SU_NO_REPLY_PORT     SU_INVALID_HANDLE

/* This is the port which has logging always enabled. It is used only for
 * suLogData. All the other operations are undefined.
 */
#define SU_PORT_LOG_ALWAYS   SU_INVALID_HANDLE

/*
 * This is the message id for the name and port handle pair which is being
 * logged in suRegisterPortName.
 * *** This message id has been deprecated in favor of SU_LOG_PORT_NAME_2. It
 * *** has been kept for backward compatibility.
 */
#define SU_LOG_PORT_NAME     0x800f0

/*
 * This is the message id for the port handle and name pair logged in
 * suRegisterPortName.
 *
 * IMPORTANT: If the @LOG changes verify the dump target in the top level
 *            Makefile doesn't need updating. If it is changed to something
 *            rtos or architecture specific it will have to be updated.
 */
#define SU_LOG_PORT_NAME_2   0x800f1    /* @LOG SU_LOG_PORT_NAME_INFO_STRUCT */

/* ------------------------ semaphore services -------------------- */

/* The SU_SEM_LOCKED and SU_SEM_UNLOCKED are special values used to
   indicate the condition of a semaphore. */

#define SU_SEM_LOCKED   0
#define SU_SEM_UNLOCKED 1

/************** STRUCTURES, ENUMS, AND TYPEDEFS ******************************/

/* ------------------------ time types ---------------------------- */
/* The SU_TIME type is used for variables that are durations or
 * timeouts in milliseconds.
 *
 * Times provided in milliseconds are converted to clock ticks before
 * being passed to RTOS services.The maximum SU_TIME value is 2^31 -
 * 1 clock ticks. The maximum SU_TIME value in milliseconds is
 * determined by the characteristics of the host hardware.
 */
typedef int SU_TIME;

typedef INT64 SU_TIME64;

/* ------------------------ misc types ---------------------------- */

/* The type SU_INTERRUPT is a 32-bit unsigned integer used to store the
 * interrupt state.
 */
typedef int SU_INTERRUPT;

/* The SU_QUEUE_HANDLE type is used for variables that identify queues.
   On Linux, it is implemented as an index into the SU_QUEUE structure array.

   The queue storage will be allocated during configuration.
   The number of queues can be obtained from su_sysconfig.nqueues.
 */

typedef UINT32 SU_QUEUE_HANDLE;

/* The SU_SIZE type is the generic type for passing sizes to SUAPI
   service calls. Since sizes are never returned, the application is
   free to keep sizes however it wants. In particular, the maximum
   number of messages in a queue could be stored in an unsigned char
   and likewise for the number of buffers in a memory pool. */

typedef UINT32 SU_SIZE;

/* The SU_RET_STATUS type is the return type for many SUAPI services
   containing both the RTOS error code and the SUAPI error code
   enumeration. The bits corresponding to the RTOS error code are
   masked by 0xFF00. The bits corresponding to the SUAPI error code
   are masked by 0xFF.

   The SU_ERROR_STATUS type represents just the SUAPI error code.

   The SU_RTOS_STATUS type represents just the RTOS error code.

*/

typedef INT32 SU_RET_STATUS;
typedef INT32 SU_ERROR_STATUS;
typedef int SU_RTOS_STATUS;

/*  The SU_SYSCONFIG type is used for system configuration information.
 */

typedef const struct SU_SYSCONFIG_ST
{
  SU_TIME maxtimeout;
  SU_TIME mintimeout;
  unsigned int nqueues;
  unsigned int npartition;
  unsigned int ntimers;
  unsigned int nbinarysemaphores;
  unsigned int ncountingsemaphores;
  unsigned int nmutexsemaphores;
  unsigned int memorypromotion;
  unsigned int namehashtablesize;
  unsigned int nports;
  unsigned int nflaggroups;
  unsigned int ntasks;
  unsigned int time_slice_quantum;
  unsigned int nbuckets;
  unsigned int heap_size;
  unsigned int nheapbuckets;
  unsigned int realtimefloor;
  unsigned int memblkalignment;
  unsigned int total_mem_size;
} SU_SYSCONFIG;

/* SU_EENUM is an enumeration used for SUAPI return status codes. */
enum
SU_EENUM {
    SU_OK = 0,              /* Successful completion */
    SU_EDELETED,            /* Resource deleted */
    SU_EEMPTY,              /* Resource is empty */
    SU_EINUSE,              /* Resource in use  */
    SU_EINVFUNC,            /* Invalid function  */
    SU_EINVPARAM,           /* Invalid parameter */
    SU_EINVTIME,            /* Invalid timeout specified */
    SU_ENOEXIST,            /* Invalid object handle  */
    SU_ENOMEM,              /* Insufficient memory */
    SU_ETIMEOUT,            /* Timeout occurred before service completed */
    SU_ETOOMANY,            /* Object resource limit exceeded */
    SU_EFULL,               /* Resource is full */
    SU_EUNIMPLEMENTED,      /* Feature not implemented on this platform */
    SU_EINVEVENT,           /* Event not allocated to calling task */
    SU_EOWN,                /* Task tried to release mutex it doesn't own */
    SU_EINVFLAG,            /* Specified flag is unallocated */
    SU_EINVLPF=SU_EINVFLAG, /* Specified LP flag is unallocated */
    SU_ENOTFOUND,           /* Object was not found (suFindName) */
    SU_EINVFLGGRP,          /* Specified flag group is invalid */
    SU_ESTACKTRACE,         /* Error occurred during stack trace */
    SU_ESTACKINFO,          /* Error occurred while generating stack report */
    SU_ETASKINFO,           /* Error occurred while generating task report*/
    SU_ESEMFAILED,          /* Internal RTOS call by SUAPI failed */
    SU_EINIT,               /* Initialization was not done */
    SU_ESTACKTRACE_EXCEP,   /* Stack trace failed on exception frame */
    SU_ESTACKTRACE_FUNC,    /* Stack trace failed on function frame */
    SU_ESTACKTRACE_FRSZ,    /* Stack trace failed on frame size calculation */
    SU_EINTR,               /* Service was interrupted. */
    SU_EFAULT               /* Address was bad. */
};

/* ------------------------ task types ---------------------------- */

/*
 * The SU_TASK_PRI type is used for task priorities. The type is
 * purposely made the same as RTOS and the conversion routines rely on
 * this.
 */
typedef int SU_TASK_PRI;

typedef pid_t SU_TASK_HANDLE;

/* ------------------------ semaphore types ----------------------- */
/* The SU_SEMA_HANDLE type is used to identify semaphores.  */

typedef UINT32 SU_SEMA_HANDLE;

/* The SU_SEMA_STATE type is the type used for semaphore states. */
typedef UINT32 SU_SEMA_STATE;



/* ------------------------ event types --------------------------- */

/*
 * This data type defines the event number used in event services.
 */
typedef UINT32 SU_EVNUM;

/* The type SU_EVMASK is a 32 bit integer used to specify events. */

typedef unsigned long SU_EVMASK;

/*
 * This mask shows which events in an event group have been allocated for use.
 */
typedef UINT32 SU_EVENT_ALLOC_MASK;

/* ------------------------ port types ---------------------------- */

/* The SU_PORT_HANDLE identifies a port. */
typedef void *SU_PORT_HANDLE;

/*
 * This struct definition describes the message that is logged when a port name
 * is registered. It must NOT be referenced by any subscriber unit code - it is
 * to be used by the host tool only.
 *
 * The value chosen for SU_LOG_PORT_NAME_ARRAY_SIZE is arbitrary. It was chosen
 * to be sufficiently large for a long time. Any changes to the value must be
 * be reflected in the host tool.
 */
#define SU_LOG_PORT_NAME_ARRAY_SIZE     128
typedef struct
{
    UINT32  phandle;
    char    name[SU_LOG_PORT_NAME_ARRAY_SIZE];
} SU_LOG_PORT_NAME_INFO_STRUCT;

/* ------------------------ queue types --------------------------- */


/* ------------------------ timer types --------------------------- */
typedef int SU_TIMER_HANDLE;


/* ------------------------ version types ------------------------- */

/* The type SU_VERSION holds the version information returned from
   su_version(). */

typedef const struct SU_VERSION_ST
{
  unsigned short su_suapi_version;
  unsigned char su_rtos;
  unsigned char su_suapi_ir;
} SU_VERSION;

/*
 * Macros to get major and minor version numbers
 */
#define SU_GET_MAJOR_VERSION( val ) \
   (((unsigned short) val) >> 8)

#define SU_GET_MINOR_VERSION( val ) \
   ( ( (unsigned short) val) & (0xff) )


/* ------------------------ flag/lowpwr types --------------------- */
/* SU_FLAGGROUP_HANDLE identifies an allocated flag group */
typedef UINT32 SU_FLAGGROUP_HANDLE;

/* SU_FLAG_HANDLE identifies an allocated flag*/
typedef UINT32 SU_FLAG_HANDLE;

/* handle type used to identify low power flags */
typedef SU_FLAG_HANDLE SU_LOWPWR_HANDLE;



/* ------------------------ cache types --------------------------- */
/* SU_CACHE_INFO type used to store control information about the caches */
typedef struct SU_CACHE_INFO
{
    UINT32 su_l1_instr_cache;
    UINT32 su_l1_data_cache;
    UINT32 su_l2_instr_cache;
    UINT32 su_l2_data_cache;
} SU_CACHE_INFO;

/* SU_CACHE_TYPE identifies the cache type for the cache control services */
typedef UINT32 SU_CACHE_TYPE;



/* ------------------------ task types ---------------------------- */

/*
 * Tag structures allow optional parameters to be passed to some SUAPI
 * services.  SU_TAG and SU_TAG_ARRAY are defined for this purpose.
 */

typedef UINT32 SU_TAG;
typedef struct
{
    SU_TAG      tag;
    void *      data;
} SU_TAG_ARRAY;

/* All tag lists should be terminated with SU_TAG_DONE */
#define SU_TAG_DONE {SU_TAG_LAST, 0}

enum
SU_TAG_CREATION_PARAMS
{
    SU_TAG_LAST=0,     /* terminates the array of tag items in a SU_TAG_ARRAY */
    SU_TRUNMODE,       /* the running mode choices are SU_T_USER and SU_T_SUPV*/
    SU_TUSERSIZE,      /* specifies the size of the user stack */
    SU_TSUPVSIZE,      /* specifies the size of the supervisor stack */
    SU_TPRIO,          /* the task should be run at this priority */
    SU_DS_INIT_IDLESTK_LOCATION,    /* lowest valid address on idle stack */
    SU_DS_INIT_IDLESTK_SIZE,        /* idle stack size in bytes */
    SU_DS_INIT_ISRSTK_LOCATION,     /* lowest valid address on ISR stack */
    SU_DS_INIT_ISRSTK_SIZE         /* ISR stack size in bytes */
};

/* SU_T_USER and SU_T_SUPV are used for specifying the task run levels. */
/* define per the CPSR bits */
#define SU_T_USER ((void *) 0x10)
#define SU_T_SUPV ((void *) 0x1F)


/* ------------------------ runlevel types ------------------------ */

/* SU_RUN_LEVEL identifies a subscriber unit run level */
typedef UINT8 SU_RUN_LEVEL;


/* ------------------------ name services types ------------------- */

/*SU_NAME_INFO structure is used by suFindAllNames() service to pass the
 *name and value pair to the user defined function. The memory for this
 *structure is provided by the caller.
 */

typedef struct{
        const char *  su_name;
        UINT32        su_value;
} SU_NAME_INFO;

typedef void SU_FIND_FCN(const SU_NAME_INFO *, UINT32, UINT32);


/************** FUNCTION PROTOTYPES ******************************************/

/* ------------------------ Miscellaneous Services -------------------*/

SU_VERSION *
suVersion(void);

SU_SYSCONFIG *
suSysConfig(void);

SU_ERROR_STATUS
suError( SU_RET_STATUS error);


/* ------------------------ Atomic Operation Services -----------------*/
UINT8  suAtomicAdd8 (UINT8  *ptr, UINT8  value);
UINT16 suAtomicAdd16(UINT16 *ptr, UINT16 value);
UINT32 suAtomicAdd32(UINT32 *ptr, UINT32 value);
UINT64 suAtomicAdd64(UINT64 *ptr, UINT64 value);

UINT8  suAtomicAnd8 (UINT8  *ptr, UINT8  value);
UINT16 suAtomicAnd16(UINT16 *ptr, UINT16 value);
UINT32 suAtomicAnd32(UINT32 *ptr, UINT32 value);
UINT64 suAtomicAnd64(UINT64 *ptr, UINT64 value);

UINT8  suAtomicOr8 (UINT8  *ptr, UINT8  value);
UINT16 suAtomicOr16(UINT16 *ptr, UINT16 value);
UINT32 suAtomicOr32(UINT32 *ptr, UINT32 value);
UINT64 suAtomicOr64(UINT64 *ptr, UINT64 value);

UINT32 suAtomicCmpXChg(UINT32 *, UINT32, UINT32);

/* ------------------------ Cache Services ----------------------------*/

void
suEnableCache(SU_CACHE_TYPE caches);

void
suDisableCache(SU_CACHE_TYPE caches);

void
suFlushCache(SU_CACHE_TYPE caches, void *address, SU_SIZE length);

void
suInvCache(SU_CACHE_TYPE caches, void *address, SU_SIZE length);

void
suFlushInvCache(SU_CACHE_TYPE caches, void *address, SU_SIZE length);

void
suBdryFlushInvCache(SU_CACHE_TYPE caches, void *address, SU_SIZE length);

void
suGetCacheInfo(SU_CACHE_INFO *cache_info);

/* ------------------------ Event Services --------------------------- */
SU_EVMASK
suClearSelfEventMask(SU_EVMASK events_to_clear, SU_RET_STATUS *err);

SU_EVMASK
suWaitAnyEventMask(SU_EVMASK event_mask, SU_TIME timeout,
                SU_RET_STATUS *err);

SU_EVMASK
suWaitAllEventMask(SU_EVMASK event_mask, SU_TIME timeout,
                   SU_RET_STATUS *err);

void
suSetEvent(SU_TASK_HANDLE thandle, SU_EVNUM event_num, SU_RET_STATUS *err);

void
suSetEventMask(SU_TASK_HANDLE thandle, SU_EVMASK events_to_set,
                  SU_RET_STATUS *err);

void
suFreeEvent(SU_EVNUM event_num);

SU_EVMASK
suClearSelfEvent(SU_EVNUM event_num, SU_RET_STATUS *err);

SU_EVNUM
suAllocEvent(SU_RET_STATUS *err);

void
suAllocEventMask(SU_EVMASK event_mask, SU_RET_STATUS *err);

SU_EVMASK
suEvnumToEvmask( SU_EVNUM event_num);

SU_EVMASK
suGetEventMask(SU_TASK_HANDLE thandle, SU_RET_STATUS *err);

/* ------------------------ Time Services ----------------------- */

SU_TIME64
suGetTimeInMs(void);

/* ------------------------ Name Services ---------------------------- */
void
suRegisterName(const char * name, UINT32 handle, SU_RET_STATUS * err);

void
suFindAllNames(SU_FIND_FCN fcn, SU_NAME_INFO *info, UINT32 arg1,
                UINT32 arg2, SU_RET_STATUS * err);

UINT32
suFindName(const char * name, SU_TIME timeout, SU_RET_STATUS *err);

UINT32
suUnregisterName(const char * name);


/* ------------------------ Flag Services ----------------------- */
SU_FLAG_HANDLE
suAllocFlag(SU_FLAGGROUP_HANDLE fghandle,int initial_state,SU_RET_STATUS *err);

SU_FLAGGROUP_HANDLE
suCreateFlagGroup(SU_RET_STATUS *err);

void
suDeleteFlagGroup(SU_FLAGGROUP_HANDLE fghandle, SU_RET_STATUS *err);

int
suFlagGroupAllClear(SU_FLAGGROUP_HANDLE fghandle);

void
suFreeFlag(SU_FLAGGROUP_HANDLE fghandle, SU_FLAG_HANDLE fhandle,
           SU_RET_STATUS *err);

void
suSetFlag(SU_FLAGGROUP_HANDLE fghandle, SU_FLAG_HANDLE fhandle, int new_state,
          SU_RET_STATUS *err);


/* ------------------------ Low Power Services ----------------------- */
SU_LOWPWR_HANDLE
suAllocLowPowerFlag(int initial_state, SU_RET_STATUS *err);

void
suFreeLowPowerFlag(SU_LOWPWR_HANDLE lphandle, SU_RET_STATUS *err);

void
suEnableLowPowerFlag(SU_LOWPWR_HANDLE lphandle, SU_RET_STATUS *err);

void
suDisableLowPowerFlag(SU_LOWPWR_HANDLE lphandle, SU_RET_STATUS *err);

int
suLowPowerEnabled(void);


/* ------------------------ Memory Services -------------------------- */

void *
suAllocMem(SU_SIZE bufsize, SU_RET_STATUS *err);

void
suFreeMem(void *bufptr);

/* ------------------------ Panic Services ----------------------- */
void
suPanic(UINT32 code, UINT32 num_pairs, ...);

/* ------------------------ Interrupt Services ----------------------- */


SU_INTERRUPT
suDisableAllInt(void);

SU_INTERRUPT
suDisableInt(void);

SU_INTERRUPT
suSetInt(SU_INTERRUPT prevstate);

/* ------------------------ Task Services ---------------------------- */
void
suTaskWrapper(int data);

SU_TASK_HANDLE
suCreateTask (void (*fn)(void), SU_TAG_ARRAY *task_props,
              SU_RET_STATUS *err);

void
suDeleteTask (SU_TASK_HANDLE thandle, SU_RET_STATUS *err);

void
suSleep(SU_TIME duration, SU_RET_STATUS *err);

SU_TASK_HANDLE
suGetSelfHandle(void);

SU_TASK_PRI
suGetSelfPriority(void);

void suDisableSched(void);

void suEnableSched(void);

void suDisableNRTSched(void);
void suEnableNRTSched(void);

/* ------------------------ run level Services ----------------------- */

SU_RUN_LEVEL
suGetRunLevel(void);

void
suSetRunLevel(SU_RUN_LEVEL newlevel, SU_RET_STATUS *err);

/* ------------------------ Port Services ---------------------------- */

SU_PORT_HANDLE
suCreatePortFromQueue(SU_QUEUE_HANDLE qhandle, UINT16 portid, SU_RET_STATUS * err);

void
suDeletePort(SU_PORT_HANDLE phandle, SU_RET_STATUS * err);

void
suReplyMessage(void * message, SU_RET_STATUS * err);

void
suSendMessage(void * message, SU_PORT_HANDLE phandle, SU_RET_STATUS * err);

void *
suSendMessageAndWait(void * message, SU_PORT_HANDLE phandle, SU_TIME timeout, SU_RET_STATUS * err);

void *
suCreateMessage ( UINT32 size, UINT32 type, SU_PORT_HANDLE replyport,
                  SU_RET_STATUS * err );

void
suDeleteMessage ( void * message, SU_RET_STATUS * err);

void
suRegisterPortName(const char *name, SU_PORT_HANDLE phandle, SU_RET_STATUS *err);

SU_PORT_HANDLE
suUnregisterPortName(const char *name);

/* Data logging Services */

SU_PORT_HANDLE
suCreateLoggerPort(SU_RET_STATUS * err);

void
suLogData(SU_PORT_HANDLE phandle, UINT32 msgid, UINT32 num_pairs, ...);

/* External Test services */
UINT32
suGetPortRouting( SU_PORT_HANDLE port );

void
suSetPortRouting( SU_PORT_HANDLE port, UINT32 mode );

UINT16
suGetMessagePortId ( void *msg);

UINT32
suGetMessageLength ( void *msg);

UINT32
suGetMessageType( void *msg);

void
suSetMessageType( void *msg, UINT32 type);

SU_PORT_HANDLE
suGetMessageReplyPort( void *msg);

void
suSetMessageReplyPort( void *msg, SU_PORT_HANDLE replyport);

SU_PORT_HANDLE
suGetMessagePortHandle( void *msg);

INT32
suLoggingEnabled( SU_PORT_HANDLE phandle);

void
suEnablePortLogging( SU_PORT_HANDLE phandle);

void
suDisablePortLogging( SU_PORT_HANDLE phandle);

UINT16
suGetPortIdFromPortHandle( SU_PORT_HANDLE phandle);

int
suGetMessageReplyLoggingBit( void *msg);

UINT8
suGetMessagePriority( void *msg);

void
suSetMessagePriority( void *msg, UINT8 priority);


/* ------------------------ Queue Services ---------------------------- */

SU_QUEUE_HANDLE
suCreateQueue ( SU_RET_STATUS * err );

void
suDeleteQueue (SU_QUEUE_HANDLE qhandle, SU_RET_STATUS * err);

void
suRegisterEventMaskWithQueue(SU_QUEUE_HANDLE qhandle, SU_TASK_HANDLE thandle,
                                SU_EVMASK event_mask, SU_RET_STATUS * err);

void *
suReceiveMessageFromQueue( SU_QUEUE_HANDLE qhandle, SU_TIME timeout,
                           SU_RET_STATUS * err);

UINT32
suGetNumMessagesOnQueue (SU_QUEUE_HANDLE qhandle, SU_RET_STATUS * err);

void *
suRecreateMessage (void *msgp, UINT32 size, SU_RET_STATUS * err);

SU_QUEUE_HANDLE
suCreatePriorityQueue( SU_RET_STATUS * err );

/* ------------------------ Suspend/Resume Services ----------------------- */
void
suSuspendTask(SU_TASK_HANDLE thandle, SU_RET_STATUS *err);

void
suResumeTask(SU_TASK_HANDLE thandle, SU_RET_STATUS *err);




/* ------------------------ Semaphore Services ---------------------------- */
SU_SEMA_HANDLE
suCreateBSem(SU_SEMA_STATE initial_state, SU_RET_STATUS *err);

void
suDeleteSem(SU_SEMA_HANDLE shandle, SU_RET_STATUS *err);

int
suReleaseSem(SU_SEMA_HANDLE shandle, SU_RET_STATUS *err);

int
suAcquireSem(SU_SEMA_HANDLE shandle, SU_TIME timeout, SU_RET_STATUS *err);

SU_SEMA_HANDLE
suCreateCSem(SU_SEMA_STATE initial_count, SU_SEMA_STATE bound, SU_RET_STATUS *err);

SU_SEMA_HANDLE
suCreateMSem(SU_SEMA_STATE initial_state, SU_RET_STATUS *err);

/* ------------------------ Timer Services ---------------------------- */

SU_TIMER_HANDLE
suCreateTimer(SU_RET_STATUS *err);

SU_TIMER_HANDLE
suCreateCyclicTimer(SU_RET_STATUS *err);

void
suDeleteTimer(SU_TIMER_HANDLE tmhandle, SU_RET_STATUS *err);

void
suStartEventTimer(SU_TIMER_HANDLE tmhandle, SU_TIME period,
                  SU_TASK_HANDLE task_handle, SU_EVMASK event_mask,
                  SU_RET_STATUS *err);

void
suStartMessageTimer(SU_TIMER_HANDLE tmhandle, SU_TIME period,
                    SU_PORT_HANDLE phandle, void *message, SU_RET_STATUS *err);

void
suStartEventTimerWithSlip(SU_TIMER_HANDLE tmhandle, SU_TIME period,
                          SU_TIME slip, SU_TASK_HANDLE task_handle,
                          SU_EVMASK event_mask, SU_RET_STATUS *err);

void
suStartMessageTimerWithSlip(SU_TIMER_HANDLE tmhandle, SU_TIME period,
                            SU_TIME slip, SU_PORT_HANDLE phandle,
                            void *message, SU_RET_STATUS *err);

void
suStopTimer(SU_TIMER_HANDLE tmhandle, SU_TIME *time_left,SU_RET_STATUS *err);

SU_TIME
suTimeLeft(SU_TIMER_HANDLE tmhandle, SU_RET_STATUS *err);



#define suEnabledSched XXX
#define suDisableSched XXX


/************** MACRO DEFINITIONS ********************************************/

/******** version info **********/
/* These macros should change for every build. */

#define SU_SUAPI_LABEL       "@(#)SUAPI_C_3.0.31_R"
#define SU_SUAPI_VERSION     SU_SUAPI_VERSION_3_0
#define SU_SUAPI_IR          SU_SUAPI_IR31


/* These macros remain unchanged during builds. */
/*
 * The SU_RTOS_* constants provide meaning to the su_version.su_rtos
 * field.
 */
enum
SU_RTOS_VERSION{
    SU_RTOS_VRTXMC = 1,
    SU_RTOS_ROS,
    SU_RTOS_PSOS,
    SU_RTOS_RTAI,
    SU_RTOS_RTXC,
    SU_RTOS_LINUX
};


/*
 * The following definitions allow detection of current version and
 * revision at compile time.
 * A version number is: (major number * 256) + minor number
 */
#define SU_SUAPI_VERSION_1_0    (0x100)
#define SU_SUAPI_VERSION_2_0    (0x200)
#define SU_SUAPI_VERSION_2_1    (0x201)
#define SU_SUAPI_VERSION_3_0    (0x300)

/* The following definition allows detection of current IR at compile time. */
#define SU_SUAPI_IR00           0
#define SU_SUAPI_IR01           1
#define SU_SUAPI_IR02           2
#define SU_SUAPI_IR03           3
#define SU_SUAPI_IR04           4
#define SU_SUAPI_IR05           5
#define SU_SUAPI_IR06           6
#define SU_SUAPI_IR07           7
#define SU_SUAPI_IR08           8
#define SU_SUAPI_IR09           9
#define SU_SUAPI_IR10           10
#define SU_SUAPI_IR11           11
#define SU_SUAPI_IR12           12
#define SU_SUAPI_IR13           13
#define SU_SUAPI_IR14           14
#define SU_SUAPI_IR15           15
#define SU_SUAPI_IR16           16
#define SU_SUAPI_IR17           17
#define SU_SUAPI_IR18           18
#define SU_SUAPI_IR19           19
#define SU_SUAPI_IR20           20
#define SU_SUAPI_IR21           21
#define SU_SUAPI_IR22           22
#define SU_SUAPI_IR23           23
#define SU_SUAPI_IR24           24
#define SU_SUAPI_IR25           25
#define SU_SUAPI_IR26           26
#define SU_SUAPI_IR27           27
#define SU_SUAPI_IR28           28
#define SU_SUAPI_IR29           29
#define SU_SUAPI_IR30           30
#define SU_SUAPI_IR31           31
#define SU_SUAPI_IR32           32
#define SU_SUAPI_IR33           33
#define SU_SUAPI_IR34           34
#define SU_SUAPI_IR35           35
#define SU_SUAPI_IR36           36
#define SU_SUAPI_IR37           37
#define SU_SUAPI_IR38           38


/************** GLOBAL VARIABLES *********************************************/

extern int suapi_dev;

#ifdef __cplusplus
}
#endif
#endif /* SUAPI_INCLUDE */

