// Copyright (c) 2006, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

/*
 *  DESCRIPTION:
 *      The su_pallog.h header file contains constants specific to the Linux
 *      SUAPI datalogger services.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

#ifndef SU_PALLOG_H
#define SU_PALLOG_H

#ifdef __KERNEL__


typedef struct SuapiLogMsgHdr
{
    /*
     * The next 4 entries are contiguous for simpler copying.  Don't rearrange!
     * The order must match that of the standard datalogger message header.
     */
    
    void *dest_port_handle;    /* Destination port handle.       */
    void *reply_port_handle;   /* Reply port handle.             */
    char reserved;             /* Not used in header.            */
    char msg_priority;         /* Priority of the message.       */
} SuapiLogMsgHdr;

typedef struct SuapiLogCommonHdr
{
    /*
     * These entries are contiguous for simpler copying.  Don't rearrange!
     * The order must match that of the standard datalogger message header.
     */
    unsigned char sync_1;
    unsigned char sync_2;
    unsigned short msg_len;
    unsigned int timestamp;
    unsigned int msg_id;
} SuapiLogCommonHdr;
    
/* Maximum size of an process ID / process name pair to be output. */
#define MAX_PID_TABLE_SIZE 32

typedef struct SuapiPanicData
{
    /*
     * These entries are contiguous for simpler copying.  Don't rearrange!
     * The order must match that of the standard datalogger panic data fields.
     */
    unsigned int panic_id;
    unsigned int seconds;       // Seconds since 1970
    unsigned char maj_ver;      // Various version numbers: major
    unsigned char min_ver;      // minor version
    unsigned char min_min_ver;  // minor-minor version
    u64 uptime;                 // Time phone has been booted in jiffies
    unsigned int pid;           // id of the process that panicked
    char pid_name[MAX_PID_TABLE_SIZE]; // String name of the process that panicked
    unsigned int panic_length;  // Length of the following panic data
} __attribute__ ((packed)) SuapiPanicData;

/* Temp file for parsed configuration file to be read by kernel module. */
#define LOGGER_TEMP_FILE "/tmp/sulogconf_data"

#define SIZE_HEADER_LOG_DATA 12
#define SIZE_HEADER_BUFFER_FULL SIZE_HEADER_LOG_DATA
#define LOGGER_BUFFER_MINIMUM_SEPARATION 1

#define SYNC_PATTERN_1 0xb5
#define SYNC_PATTERN_2 0xc4
#define LOGGER_ARBITRARY_MSG_TYPE 0x03
#define BUFFER_FULL_INDICATION_ID 0x80240

#endif /* _KERNEL_ */

#ifdef __cplusplus
extern "C" {
#endif

/************** CONSTANTS ****************************************************/

/* The size of the datalogger message header logged from suPalLogMessage. */
#define SIZE_HEADER 22

/* The maximum size (characters) of a port name that can have logging enabled. */
#define LOGGER_CONFIG_NAME_SIZE 16

/* The maximum number of port names that can have logging enabled. */
#define LOGGER_CONFIG_NUMBER_OF_NAMES 64


#ifdef __cplusplus
}
#endif
#endif /* SU_PALLOG_H */
