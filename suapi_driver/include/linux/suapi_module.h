// This file is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the License, or (at your option) any later version.
//
// This file is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA 02111-1307 USA  

// Copyright (C) 2006-2008 Motorola

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.
// 2007-01-22 Motorola       Removed __FILE__ to prevent pathnames in objects.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM
// 2008-01-09 Motorola       Add extern for su_previous_panic.

/************** HEADER FILE INCLUDES *****************************************/
#ifdef __KERNEL__
#include <linux/device.h>
#endif

#include <linux/su_ioctl.h>
#include <linux/su_mess_hdr.h>

/************** CONSTANTS ****************************************************/
/*---- general ----*/
/* SU_INVALID_HANDLE is used to identify invalid handles. 
 *  
 *  It should be cast to the object's proper handle type.
 */
#define SU_INVALID_HANDLE 0

/* Buffer size used when reading suapi.su_sysconfig file. */
#define SU_CFG_BUFSIZE 256

/* SU_SELF_PID indicates that the caller is performing an action for itself. */
#define SU_SELF_PID -2

/*---- ports ----*/
/* The following values are used to indicate the logging status of a port */
#define SU_LOGGING_DISABLED      0
#define SU_LOGGING_ENABLED       1

/* The following values are used to indicate the routing modes for external
 * testing.
 */
#define SU_ROUTE_NORMAL          0
#define SU_ROUTE_MIRROR          1
#define SU_ROUTE_INTERCEPT       2

/* The following values are used to indicate the return values of
 * suPalLogMessage.
 */
#define SU_TEST_JUST_RETURN      0
#define SU_TEST_PERFORM_ACTION   1

/* The following values are used to indicate whether or not a port is
 * allocated, if it is allocated, whether it is a message or a logger port.
  */
#define SU_UNALLOCATED_PORT      0
#define SU_MESSAGE_PORT          1
#define SU_LOGGER_PORT           2

/*--- ports ---*/


/*
   The SU_PORT_HANDLE_STRUCT structure contains all the information relating
   to a port.
*/
typedef struct SU_PORT_HANDLE_STRUCT
{
  UINT8 port_type;
  unsigned int logging_enabled : 1;
  unsigned int routing_mode    : 2;
  unsigned int port_pad        : 5; /* to  explicitly use all the eight bits. */
  UINT16 port_id;
  union {
      SU_QUEUE_HANDLE qhandle;  /* port_type = SU_MESSAGE_PORT */
  } port_transport;
} SU_PORT_HANDLE_STRUCT;


/*---- queues ----*/
/* SU_NUM_QUEUE_PRIORITIES specifies the total number of priorities
 * supported by the implementation
 */
#define SU_NUM_QUEUE_PRIORITIES 3

/* The following values are used to indicate the type of action that cause
 * suPalLogMessage to be called.
 */
#define SU_MSGACTION_SEND        0
#define SU_MSGACTION_RECEIVE     1
#define SU_MSGACTION_REPLY       2

/*---- logger ----*/
#define LOGGER_SEND_MSG_TYPE         0x00
#define LOGGER_RECEIVE_MSG_TYPE      0x01
#define LOGGER_REPLY_MSG_TYPE        0x02

/* The maximum size (characters) of a port name that can have logging enabled. */
#define LOGGER_CONFIG_NAME_SIZE 16

/* The maximum number of port names that can have logging enabled. */
#define LOGGER_CONFIG_NUMBER_OF_NAMES 64

#define LOGGER_CONFIG_SIZE (LOGGER_CONFIG_NAME_SIZE * LOGGER_CONFIG_NUMBER_OF_NAMES)

/*---- panic ----*/
/* The following constants are used to identify the size of values passed to
 * variable argument routines when data values are passed instead of pointers
 * to data.
 */
#define SU_SIZE_8  ((int)-1)  /* To pass eight bits of data */
#define SU_SIZE_16 ((int)-2)  /* To pass sixteen bits of data */
#define SU_SIZE_32 ((int)-3)  /* To pass 32 bits of  data */
#define SU_SIZE_64 ((int)-4)  /* To pass 64 bits of  data */

#define SU_PANIC_CODE_GEN( number ) ( (0x80000000) + (number) )

/* SUAPI panic codes that are used in the kernel module. */
#define SU_PANIC_ALLOC_MEM_5                    SU_PANIC_CODE_GEN(117)
#define SU_PANIC_ALLOC_MEM_6                    SU_PANIC_CODE_GEN(118)
#define SU_PANIC_ALLOC_MEM_7                    SU_PANIC_CODE_GEN(119)
#define SU_PANIC_ALLOC_MEM_8                    SU_PANIC_CODE_GEN(120)
#define SU_PANIC_FREE_MEM_3                     SU_PANIC_CODE_GEN(121)
#define SU_PANIC_FREE_MEM_4                     SU_PANIC_CODE_GEN(122)
#define SU_PANIC_FREE_MEM_5                     SU_PANIC_CODE_GEN(123)
#define SU_PANIC_FREE_MEM_6                     SU_PANIC_CODE_GEN(124)
#define SU_PANIC_DELETE_QUEUE_2                 SU_PANIC_CODE_GEN(125)
#define SU_PANIC_RECEIVE_MESSAGE_2              SU_PANIC_CODE_GEN(126)
#define SU_PANIC_RECEIVE_MESSAGE_3              SU_PANIC_CODE_GEN(127)
#define SU_PANIC_SEND_MESSAGE_3                 SU_PANIC_CODE_GEN(128)
#define SU_PANIC_SEND_MESSAGE_4                 SU_PANIC_CODE_GEN(129)
#define SU_PANIC_UNKNOWN_ID                     SU_PANIC_CODE_GEN(137)

/************** STRUCTURES, ENUMS, AND TYPEDEFS ******************************/

/* Note: The SU_SUBQUEUE_HEAD structure cannot have anything inserted before
 *or in between its minnode members.
 */
typedef struct
{
    SU_MINNODE * su_q_head;
    SU_MINNODE * su_q_null;
    SU_MINNODE * su_q_tailpred;
} SU_SUBQUEUE_HEAD;

/* SU_QUEUE:  Defines a queue, which contains pointers to the beginning and
 * end of the queue as well as information required to implement queue
 * manipulation.
 */
typedef struct SU_QUEUE
{
    SU_SUBQUEUE_HEAD su_subqueue[SU_NUM_QUEUE_PRIORITIES];/*subqueues*/
    UINT32 su_prioritymask;  /* keeps status info. for each priority */
    UINT32 su_qtype;         /* priority or Fifo queue type. */
    int su_state;            /* used to define the queue state. */
    pid_t su_waiting_tid;    /* handle of the waiting task */
    pid_t su_event_thandle;  /* task handle for the event  */
    unsigned long su_emask;  /* event mask, 0 = unallocated entry */
    UINT32 su_qmsgcount;     /* total number of messages on the queue */
#ifdef __KERNEL__
    wait_queue_head_t suqwaiters; /* List of waiters within select() */
#endif
} SU_QUEUE;


/*--- flags ---*/
/*
 * The suFlagGroup structure contains the information used for
 * generic flags.  The su_flag_allocated field is a bit array that stores
 * which flags are allocated and unallocated, and the su_flag_value field
 * is a bit array which stores the current status of each flag.
 */
struct suFlagGroup
{
   UINT32 su_flag_allocated[1];
   UINT32 su_flag_value[1];
};

/*
 * The su_low_power_flag_t type is defined for saving the allocation
 * and current settings of the SUAPI low power flags. The type allows
 * for 32 flags that can be individually allocated and set/cleared by
 * applications the use the SUAPI Low Power Flag services.
 */

typedef unsigned long su_low_power_flag_t;

/*
 * The SU_NUM_LPWR_FLAGS defines the number of SUAPI Low Power Flags
 * that can be allocated.
 */

#define SU_NUM_LPWR_FLAGS (sizeof(su_low_power_flag_t) * 8)

/*
 * The SU_LPWR structure and typedef formats the SUAPI low power flags
 * section of the SUAPI private memory. Each bit in the
 * low_power_flags_allocation  field defines whether a low power flag
 * is allocated (1) or free (0). Each bit low_power_flags_setting
 * field defines whether a low power flag allows low power mode (0) or
 * disallows lower power mode (1).
 *
 * The bits are indexed 0=LSBit, 31=MSBit.
 *
 * The calling process ID is saved in low_power_flags_allocator when
 * low power flags are allocated and freed. Likewise, the calling PID
 * is saved in low_power_flags_setter when low power flags are enabled
 * and disabled. Since allocation and free options potentially change
 * the settings, low_power_flags_setter is also updated then as well.
 *
 * The macros SU_SAVE_LPWR_ALLOCATOR() and SU_SAVE_LPWR_SETTER()
 * provide a convenient method to update the PID fields. The macros
 * su_low_power_flags_location and su_low_power_flags_setting provide
 * a convenient short-hand for referencing the flags data.
 */

typedef struct SU_LPWR
{
    su_low_power_flag_t low_power_flags_allocation;
    su_low_power_flag_t low_power_flags_setting;

    pid_t low_power_flags_allocator[32];
    pid_t low_power_flags_setter[32];
} SU_LPWR;

#define su_low_power_flags_allocation \
        suLowPower_ptr->low_power_flags_allocation
#define su_low_power_flags_setting  \
        suLowPower_ptr->low_power_flags_setting
#define SU_SAVE_LPWR_ALLOCATOR(which)  \
        (suLowPower_ptr->low_power_flags_allocator[which]=current->pid)
#define SU_SAVE_LPWR_SETTER(which) \
        (suLowPower_ptr->low_power_flags_setter[which]=current->pid)

/*--- memory ---*/
typedef struct SU_PCB
{
  void * first_free_block; /* first free block in the linker list */
  void * part_start_add;   /* partition start address */
  void * part_end_add;     /* partition end address */
  UINT16 blocksize;        /* block size in bytes */
  UINT16 nb_blocks;        /* number of blocks */
  UINT16 free_buffers;     /* number of free buffers */
  UINT16 min_free_buffers; /* minimum number of free buffers */
} SU_PCB;

typedef struct SU_PART_SIZE_INFO
{
    UINT16 blocksize;
    UINT16 nb_blocks;
} SU_PART_SIZE_INFO;

/*--- system configuration ---*/
/*  The SU_SYSCONFIG type is used for system configuration information */
typedef const struct SU_SYSCONFIG_ST
{
  unsigned int maxtimeout;
  unsigned int mintimeout;
  unsigned int nqueues;
  unsigned int npartition;
  unsigned int ntimers;
  unsigned int nbinarysemaphores;
  unsigned int ncountingsemaphores;
  unsigned int nmutexsemaphores;
  unsigned int memorypromotion;
  unsigned int namehashtablesize;
  unsigned int nports;
  unsigned int nflaggroups;
  unsigned int ntasks;
  unsigned int time_slice_quantum;
  unsigned int nbuckets;
  unsigned int heap_size;
  unsigned int nheapbuckets;
  unsigned int realtimefloor;
  unsigned int memblkalignment;
  unsigned int total_mem_size;
} SU_SYSCONFIG;

/* Map for SUAPI shared memory region. */
typedef struct su_global_data_str {
    void * user_shmaddress;
    unsigned int su_lowpwr_flags_glb; /* UNUSED */
    int suFlagsMutex;
    int suAtomicOpsMutex;
    void * suFlagGroupTable;
    void * suFlagGroupAlloc;
    char * logger_config;
    char logger_enabled;
} SU_GLOBAL_DATA;


typedef struct su_private_kernel_data {
    unsigned long private_size;
    void * kprivate_p;
    unsigned long shared_size;
    void * kshared_p;
    unsigned long timersz;    /* size of 1 timer structure */
    unsigned long semsz;      /* size of 1 semsz structure */
    unsigned long namehashsz; /* size of 1 name structure */
    unsigned long queuesz;    /* size of 1 queue structure */
} SU_PRIVATE_DATA;

/*--- tasks ---*/
typedef pid_t SU_RTOS_TASKID;
typedef UINT16 SU_RTOS_COMPRESSED_TASKID;

enum {
    SUAPI_TASK_STATE_NOT_WAITING,
    SUAPI_TASK_STATE_WAIT_ANY_EVENT,
    SUAPI_TASK_STATE_WAIT_ALL_EVENTS,
    SUAPI_TASK_STATE_WAIT_REPLY_MESSAGE
};

/*-- semaphores --*/

#ifdef __KERNEL__
/* This structure was copied from the kernel code: include/linux/rt_lock.h.
 * Differences between the original and this are commented and marked with %%
 */
struct su_rt_mutex {
        /* %%: wait_lock was changed from raw_spinlock_t to spinlock_t */
        spinlock_t              wait_lock;
        struct list_head        wait_list;
        struct task_struct      *owner;
        int                     owner_prio;
/*      %%: Fields used only for debug and deadlock detect were removed:
 *      int                     debug;
 *      int                     save_state;
 *      struct list_head        held_list;
 *      unsigned long           acquire_eip;
 *      char                    *name, *file;
 *      int                     line;
 */
};

/*
 * This is the control structure for tasks blocked on an
 * RT mutex:
 */
/* This structure was copied from the kernel code: include/linux/rt_lock.h
 * Differences between the original and this are commented and marked with %%
 */
struct su_rt_mutex_waiter {
        struct su_rt_mutex *lock;
        struct list_head list;
        /* %%: pi_list (priority inheritence list) was removed. */
        struct task_struct *task;

        /* %%: eip (debugging) was removed. */
};

/*
 * semaphores - an RT-mutex plus the semaphore count:
 */
/* This structure was copied from the kernel code: include/linux/rt_lock.h */
struct su_semaphore {
        atomic_t count;
        struct su_rt_mutex lock;
};

/* SuapiSemaphore defines a SUAPI semaphore's attributes. */
struct SuapiSemaphore {
    unsigned short type;                /* type (counting, mutex, binary) */
    unsigned short wait_count;          /* current number of waiters */
    union {
        struct {
            unsigned int nestcount;     /* Nest count for mutex semaphore. */
            pid_t ownerpid;             /* PID of owner of a mutex semaphore. */
/* If available, use the Linux RT mutexes for SUAPI mutex semaphores. */
#if defined(CONFIG_PREEMPT_RT) && defined(CONFIG_MOT_FEAT_DOWN_TIMEOUT)
            struct semaphore sem;       /* Linux semaphore--used for waiting,
                                         * priority inheritance, priority
                                         * wakeup, and lock count (held/free) */
#else
            struct su_semaphore sem;    /* SUAPI internal semaphore structure
                                         * used for waiting, priority wakeup,
                                         * and keeping lock count. */
#endif
        } mutex;
        struct {        /* Shared binary and counting structure. */
            unsigned int bound;         /* Upper bound if counting, 1 if
                                         * binary. */
            struct su_semaphore sem;    /* SUAPI internal semaphore structure
                                         * used for waiting, priority wakeup,
                                         * and keeping lock count. */
        } bin_count;
    } s;
};

/* SuapiSemArray is the structure of the global semaphore array. */
struct SuapiSemArray {
    unsigned short deletions;      /* Helps detect deleted semaphore handle. */
    unsigned short pad;
    struct SuapiSemaphore *sem;    /* NULL indicates semaphore is free. */
};
#endif /* __KERNEL__ */

/*--- timers ---*/
#define ktevent_pid su_timer_user.stevent_pid
#define ktevent_mask su_timer_user.stevent_mask
#define ktmessage_port su_timer_user.stmessage_port
#define ktmessage_msg su_timer_user.stmessage_msg
#define ktstate su_timer_user.state
#define ktperiod su_timer_user.period
#define ktslip su_timer_user.slip
#define ktid su_timer_user.full_id

#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
typedef struct Suapi_Tasklet_Handler_Data
{
    pid_t pid;
    int tasklet_done;
} Suapi_Tasklet_Handler_Data;
#endif

#if defined(CONFIG_MOT_FEAT_PM)
#define SU_IN_ISR 1
#define SU_NOT_IN_ISR 0
#endif

/* SU_EENUM is an enumeration used for SUAPI return status codes. */
enum
SU_EENUM {
    SU_OK = 0,              /* Successful completion */
    SU_EDELETED,            /* Resource deleted */
    SU_EEMPTY,              /* Resource is empty */
    SU_EINUSE,              /* Resource in use  */
    SU_EINVFUNC,            /* Invalid function  */
    SU_EINVPARAM,           /* Invalid parameter */
    SU_EINVTIME,            /* Invalid timeout specified */
    SU_ENOEXIST,            /* Invalid object handle  */
    SU_ENOMEM,              /* Insufficient memory */
    SU_ETIMEOUT,            /* Timeout occurred before service completed */
    SU_ETOOMANY,            /* Object resource limit exceeded */
    SU_EFULL,               /* Resource is full */
    SU_EUNIMPLEMENTED,      /* Feature not implemented on this platform */
    SU_EINVEVENT,           /* Event not allocated to calling task */
    SU_EOWN,                /* Task tried to release mutex it doesn't own */
    SU_EINVFLAG,            /* Specified flag is unallocated */
    SU_EINVLPF=SU_EINVFLAG, /* Specified LP flag is unallocated */
    SU_ENOTFOUND,           /* Object was not found (suFindName) */
    SU_EINVFLGGRP,          /* Specified flag group is invalid */
    SU_ESTACKTRACE,         /* Error occurred during stack trace */
    SU_ESTACKINFO,          /* Error occurred while generating stack report */
    SU_ETASKINFO,           /* Error occurred while generating task report*/
    SU_ESEMFAILED,          /* Internal RTOS call by SUAPI failed */
    SU_EINIT,               /* Initialization was not done */
    SU_ESTACKTRACE_EXCEP,   /* Stack trace failed on exception frame */
    SU_ESTACKTRACE_FUNC,    /* Stack trace failed on function frame */
    SU_ESTACKTRACE_FRSZ,    /* Stack trace failed on frame size calculation */
    SU_EINTR,               /* Service was interrupted. */
    SU_EFAULT               /* Address was bad. */
};

/************** GLOBAL FUNCTION PROTOTYPES ***********************************/
#ifdef __KERNEL__
/*-- data logger --*/
int suLogConfInit(void);
int suapi_log_message_ioctl(int arg);
int suapi_internal_log_message(void *, UINT32);
int suapi_log_data_ioctl(int arg);
int suapi_panic_ioctl(int arg);
void suPanic(UINT32 code, UINT32 num_pairs, ... );

/*--- events ---*/
#ifdef CONFIG_SUAPI
int suapi_internal_verify_alloc_event_mask(pid_t, unsigned long);
int suapi_internal_set_event_mask(pid_t, unsigned long, int in_isr);
int suapi_alloc_event_ioctl(int);
int suapi_alloc_event_mask_ioctl(int);
int suapi_free_event_ioctl(int);
int suapi_set_event_mask_ioctl(int);
int suapi_clear_event_mask_ioctl(int);
int suapi_verify_alloc_event_mask_ioctl(int);
int suapi_wait_any_event_ioctl(int);
int suapi_wait_all_event_ioctl(int);
int suapi_get_event_mask_ioctl(int);
#endif /* CONFIG_SUAPI */

/*--- memory ---*/
int suMemInit(SU_PART_SIZE_INFO *, void *);
int suapi_alloc_mem_ioctl(int);
int suapi_free_mem_ioctl(void *);
void suapi_internal_free_mem(void *, spinlock_t *);
void * suapi_internal_alloc_mem(int , int *);

/*--- name services ---*/
int suapi_findname_ioctl(struct file *,int);
UINT32 suapi_findname(const char *, long, int *);
int suapi_registername(struct file *,int);
int suapi_unregistername(struct file *,int);
void suapi_clean_names(void);

/*-- ports --*/
int suapi_enable_port_logging(SU_PORT_HANDLE phandle);
int suapi_disable_port_logging(SU_PORT_HANDLE phandle);
int suapi_internal_send_message_to_port(SU_PORT_HANDLE, void *, int in_isr);
int suapi_create_logger_port_ioctl(int);
int suapi_delete_port_ioctl(SU_PORT_HANDLE);
int suapi_port_logging_enabled(SU_PORT_HANDLE);
int suapi_get_portid(SU_PORT_HANDLE);
int suapi_get_port_routing(SU_PORT_HANDLE);
int suapi_set_port_routing(int);

/*-- power management --*/
int suapi_probe(struct device *);
int suapi_suspend(struct device *, u32, u32);
int suapi_resume(struct device *, u32); 
int suapi_alloc_low_power_flag_ioctl(int);
int suapi_free_low_power_flag_ioctl(int);
int suapi_enable_lower_power_flag(int);
int suapi_disable_lower_power_flag(int);
int suapi_get_lower_power_flag(int);

/*-- queues --*/
int suapi_create_queue_ioctl(int);
int suapi_delete_queue_ioctl(SU_QUEUE_HANDLE);
int suapi_receive_message_from_queue_ioctl(int);
int suapi_send_message_ioctl(int);
int suapi_get_num_messages_on_queue_ioctl(SU_QUEUE_HANDLE);
void * suapi_internal_recreate_message(void *, UINT32);
int suapi_queues_with_fd_ioctl(struct file *, int);
int suapi_queues_wake_select_ioctl(struct file *, int);
int suapi_create_port_ioctl(int, int);

#ifdef CONFIG_SUAPI
int suapi_register_event_mask_with_queue_ioctl(int);
int suapi_queues_send_msg_and_wait_ioctl(int);
#endif /* CONFIG_SUAPI */

/*-- semaphores --*/
int suapi_sem_create_ioctl(int, int);
int suapi_sem_acquire_ioctl(int);
int suapi_sem_release_ioctl(int);
int suapi_sem_delete(int);
void FASTCALL(__su_sema_init(struct su_semaphore *,int,int,char *));
int FASTCALL(su_down_interruptible_timeout(struct su_semaphore *, signed long));
void FASTCALL(su_up(struct su_semaphore *));
int FASTCALL(su_sema_count(struct su_semaphore *));

/*-- string and file manipulation functions --*/
char *suapi_fgets(char *, int, struct file *);
char *suapi_strtok(char *s1, const char *s2);

/*-- sysfs debug functions --*/
ssize_t suapi_show_su_sysconfig(struct device *, char *);
ssize_t suapi_show_minfo(struct device *, char *);

/*--- timers ---*/
int suapi_get_ms_ioctl(int);
int suapi_create_timer_ioctl(unsigned long);
int suapi_delete_timer_ioctl(unsigned long);
int suapi_start_timer_ioctl(unsigned long);
int suapi_stop_timer_ioctl(unsigned long);
int suapi_time_left_ioctl(unsigned long);

int suapi_sleep_ioctl(unsigned long);
void suapi_tasklet_handler_fn(unsigned long);

#endif /* __KERNEL__ */

/************** MACRO DEFINITIONS ********************************************/
/*--- general ---*/

/* SU_USER_TO_KERNEL: converts mmapped user address to kernel address.
 * "u" is assumed to be an address within the SUAPI shared memory region.
 */
#define SU_USER_TO_KERNEL(u) \
                (suapi_kernel_start_ptr + \
                    ((void *)(u) - suapi_user_shmaddress_ptr))

/* SU_KERNEL_TO_USER: converts kernel address to mmapped user address. */
#define SU_KERNEL_TO_USER(k) \
                (suapi_user_shmaddress_ptr + \
                    ((void *)(k) - suapi_kernel_start_ptr))

#define SU_GLOBAL_SIZE (suapi_shmsize_glb)
#define SU_PRIVATE_SIZE (suapi_private_data_ptr->private_size)

/* SU_IN_SUAPI_MEMORY: Evaluates to TRUE if p is in partition memory or in the
 *                     private memory region. Evaluates to FALSE otherwise.
 * p must not be an expression with side effects since it is evaluated twice.
 */
#define SU_IN_SUAPI_MEMORY(p) \
    ( SU_IN_PARTITION_RANGE(p) || \
        (((void *)(p) >= suapi_private_start_ptr) \
            && ((void *)(p) < suapi_private_end_ptr)) )

/* SU_IN_PARTITION_RANGE: Evaluates to TRUE if p is in partition memory and
 *                        FALSE otherwise.
 * p must not be an expression with side effects since it is evaluated twice.
 */
#define SU_IN_PARTITION_RANGE(p) (((void *)(p) >= suapi_memparts_begin_ptr) \
                                      && ((void *)(p) < suapi_memparts_end_ptr))

 
/*--- queues ---*/
/* SU_NQUEUES: total number of SUAPI queues. */
#define SU_NQUEUES (suapi_sysconfig_ptr->nqueues)

/* This macro defines the size of a message header.  It shoud be used
 * instead of sizeof(SU_MSG_HDR).
 */     

/* This macro returns a pointer to message header */
#define SU_MSG2HDR(msgp) \
   ((SU_MSG_HDR *)(((UINT32) (msgp)) - (SU_SIZEOF_MSG_HDR)))

/* This macro returns a pointer to message data */
#define SU_HDR2MSG(msg_hdr_p) \
   ((void *)(((UINT32) (msg_hdr_p)) + (SU_SIZEOF_MSG_HDR)))

/*---- ports ----*/
/* SU_NPORT: total number of SUAPI ports. */
#define SU_NPORT (suapi_sysconfig_ptr->nports)

/* On Linux, the port handle is referenced as a pointer inside of SUAPI, but
 * outside of SUAPI it is passed as an index into the port structures array.
 *
 * Also, for the suSendMessageAndWait() service in Linux, the port handle can
 * either be a process ID or an index into the suPortTable array.  Set the high
 * order bit to indicate that it is an index as opposed to a PID since PIDs
 * are always a positive signed integer.
 */

#define SU_PH2PINDEX(ph)  (0x7FFFFFFF&((int) (ph)))
#define SU_PINDEX2PH(idx) ((SU_PORT_HANDLE) (0x80000000|(idx)))
#define SU_ISPHPID(ph)    ((0x80000000 & ((int) ph)) == 0)

/* SU_PH2PHPTR: converts a port handle to a pointer to the port handle
 *   structure. This macro first takes the port handle, converts it to an
 *   index, and then uses that index to get the address of the port
 *   structure. If ph is SU_INVALID_HANDLE, leave it that way.
 *   This macro does not check to insure the port handle is not a process ID.
 */
#define SU_PH2PHPTR(ph) \
        ((ph==SU_INVALID_HANDLE) ? \
                SU_INVALID_HANDLE : (&(suPortTable_ptr[SU_PH2PINDEX(ph)])))

#define SU_PHOUTOFRANGE(ph) ((SU_ISPHPID(ph))?(char)0:(SU_PH2PINDEX(ph) >= SU_NPORT))

#define suGetMessageLength(msg) \
                   ((SU_MSG2HDR(msg))->length)

#define suGetMessagePortHandle(msg) ((SU_MSG2HDR(msg))->destport)

#define suGetMessagePriority(msg) ((UINT8)((SU_MSG2HDR(msg))->priority))

#define suGetMessageReplyLoggingBit(msg) ((SU_MSG2HDR(msg))->reply_logging)

#define suGetMessageReplyPort(msg) ((SU_MSG2HDR(msg))->replyport)

#define suGetMessageType(msg) ((SU_MSG2HDR(msg))->type)

/*This service returns the current port routing mode.*/
#define suGetPortRouting(port) ((UINT32)((port)->routing_mode))

/* This service returns the current logging status for a port */
#define suLoggingEnabled(port) ((int)((port)->logging_enabled))

#define suSetMessagePortHandle(msg, msgdestport) \
    (((SU_MSG2HDR(msg))->destport) = (msgdestport))

#define suSetMessageReplyLoggingBit(msg, logging_status) \
    ((SU_MSG2HDR(msg))->reply_logging = (logging_status))

/*--- timers ---*/
#define SU_NTIMER (suapi_sysconfig_ptr->ntimers)

/*--- name services ---*/
#define SU_NAME_HASHTABLE_SIZE (suapi_sysconfig_ptr->namehashtablesize)

/*--- memory ---*/
/* SU_NPARTITION: total number of SUAPI memory partitions. */
#define SU_NPARTITION (suapi_sysconfig_ptr->npartition)
#define SU_MEMORY_PROMOTION (suapi_sysconfig_ptr->memorypromotion)

/*--- flags ---*/
/* This macro is used to calculate the number of bits in the unsigned type..*/
#define SU_BITS_PER_BYTE        8
#define SU_UINT32_BITS          (sizeof(UINT32) * SU_BITS_PER_BYTE)

/*
 * This macro makes a bit mask with the bit indicated by the parameter index
 * set to 1. It can be used to set a bit in an unsigned bit array to the value
 * of 1 by logically or'ing the mask to the appropriate word in the array. This
 * macro starts filling in the bit array from MSB to LSB.
 */
#define SU_BIT_ONE_MASK(index) \
    (((UINT32) 1) << (SU_UINT32_BITS - (index) - 1))

/*
 * This macro makes a bit mask with the bit indicated by the parameter index
 * set to 0. It can be used to set a bit in an unsigned bit array to the value
 * of 0 by logically and'ing the mask to the appropriate word in the array.
 */
#define SU_BIT_ZERO_MASK(index) (~ SU_BIT_ONE_MASK(index))

/* This macro is called to set a bit in an array to a specified value. */
#define SU_SET_BIT(array,bit,value) \
    ((value == 1) ? \
    ((array)[(bit)/SU_UINT32_BITS] |= SU_BIT_ONE_MASK((bit) % SU_UINT32_BITS)):\
    ((array)[(bit)/SU_UINT32_BITS] &= SU_BIT_ZERO_MASK((bit) % SU_UINT32_BITS))) 

/*
 *  The following macro converts the size of a bit array into the size
 *  of a UINT32 array which represents it.
 */
#define SU_BITS_TO_UINT32(nBits) \
    (((nBits) + SU_UINT32_BITS -1) / SU_UINT32_BITS)

/*--- tasks ---*/
#define SU_COMPRESS_RTOS_TASKID(uncompressed_tid) \
      ((SU_RTOS_COMPRESSED_TASKID) (uncompressed_tid))
#define SU_UNCOMPRESS_RTOS_TASKID(compressed_tid) \
      ((SU_RTOS_TASKID) (compressed_tid))

/*--- semaphores ---*/
/* The following macro describes the total number of binary semaphores. */
#define SU_NUM_BINARY_SEMAPHORE (suapi_sysconfig_ptr->nbinarysemaphores)
/* The following macro describes the total number of counting semaphores. */
#define SU_NUM_COUNTING_SEMAPHORE (suapi_sysconfig_ptr->ncountingsemaphores)
/* The following macro describes the total number of mutex semaphores. */
#define SU_NUM_MUTEX_SEMAPHORE (suapi_sysconfig_ptr->nmutexsemaphores)

/* The following macro describes the total number of all semaphores. */
#define SU_NUM_SEMAPHORES (SU_NUM_BINARY_SEMAPHORE + SU_NUM_COUNTING_SEMAPHORE \
        + SU_NUM_MUTEX_SEMAPHORE)

/* The following macros are used in creating semaphore handles. A semaphore
 * handle contains the deletion count in the upper half-word and the index
 * into the semarray in the lower half-word.
 */
#define SUAPI_SEM_DEL_SHIFT 16
#define SUAPI_SEM_IDX_MASK  0x0000FFFF
#define SUAPI_CREATE_SHANDLE(d,i) \
        ((unsigned int)(((d)<<SUAPI_SEM_DEL_SHIFT) | (SUAPI_SEM_IDX_MASK&(i))))

/* SUAPI_GET_SEM_DELETIONS: Given a sem handle, return the deletion count. */
#define SUAPI_GET_SEM_DELETIONS(sh) ((sh) >> SUAPI_SEM_DEL_SHIFT)
/* SUAPI_GET_SEM_INDEX: Given a sem handle, return the index into semarray. */
#define SUAPI_GET_SEM_INDEX(sh) (SUAPI_SEM_IDX_MASK & (sh))

#define su_sema_init(sem, val) \
                __su_sema_init(sem, val, 1, #sem)
#define su_sema_init_nocheck(sem, val) \
                __su_sema_init(sem, val, 0, #sem)

/*---- messages ----*/
#define suapi_internal_delete_message(message) \
    suapi_internal_free_mem((void *)SU_MSG2HDR(message),NULL)

/*---- events ------*/
#ifdef CONFIG_SUAPI
/* Convert event number to event mask. Assume the event_number is in between
 * 0 and 31 inclusive. Otherwise, the result is undefined.
 */
#define suEvnumToEvmask(event_num) \
    (1UL << (31 - (event_num))) 
#endif

/************** GLOBAL VARIABLES *********************************************/
#ifdef __KERNEL__
/*---- general ----*/
/* Kernel pointer to SUAPI shared global data structure. */
extern SU_GLOBAL_DATA * suapi_global_data_ptr;
extern SU_PRIVATE_DATA * suapi_private_data_ptr;
extern void * suapi_kernel_start_ptr;
extern void * suapi_private_start_ptr;
extern void * suapi_private_end_ptr;
extern SU_PORT_HANDLE_STRUCT * suPortTable_ptr;
extern void * suapi_user_shmaddress_ptr;
extern void * suapi_memparts_begin_ptr;
extern void * suapi_memparts_end_ptr;
extern int suapi_shmsize_glb;
extern struct suFlagGroup * suFlagGroupTable_ptr;
extern SU_PCB * suapi_pcb_ptr;
extern char * suapi_logger_config_ptr;

/* spin lock for general SUAPI data locking.
 * Interrupts are expected to remain enabled when locking this spinlock.
 */
extern spinlock_t su_lock;

/* Kernel copy of SUAPI configuration data. */
extern SU_SYSCONFIG * suapi_sysconfig_ptr;

/*---- events ----*/
#ifdef CONFIG_SUAPI
/* Events-specific spin lock.
 * When this lock is used during the timer function handler, soft interrupts
 * are not explicitly disabled. At all other times, soft interrupts are
 * expected to be disabled when locking this spinlock because it is used
 * during soft interrupt context.
 */
extern spinlock_t su_events_lock;
#endif /* CONFIG_SUAPI */

/*---- flags ----*/
/*
 * The suLowPower_advice_driver_num is used with the MPM to identify
 * the SUAPI driver. When SUAPI registers with the MPM, this variable
 * is set and used.
 *
 * It is initialized to -1 (EPERM) only to prevent accidental
 * notifications prior to registration.
 */

extern int suLowPower_advice_driver_num;

/*
 * The suLowPower_ptr is used by the kernel to access the Low Power
 * data structure (SU_LPWR) in SUAPI private data region.
 */

extern SU_LPWR *suLowPower_ptr;

/*---- logger ----*/
/* Implements /dev/supanic which sends panic data to the panic daemon. */
extern struct miscdevice supanic_device;

/*
 * Global from datalogger that indicates whether we are already in a panic.
 * Note that this is safe to use without locking because only time it would 
 * change from panic to non-panic case is if a watchdog occurred and that 
 * would change during insmod so no one else would be making SUAPI calls.
 */
extern int su_previous_panic;

/*---- name services ----*/
extern struct hlist_head *suNames;

/*--- queues ---*/
extern SU_QUEUE * su_qarray_ptr;

/*--- semaphores ---*/
extern struct SuapiSemArray * semarray;
extern atomic_t num_free_binary;
extern atomic_t num_free_counting;
extern atomic_t num_free_mutex;

/*--- sysfs ---*/
/* These variables are defined and initialized in su_ksysfs.c by the
 * DEVICE_ATTR() macro.
 */
extern struct device_attribute dev_attr_su_sysconfig;
extern struct device_attribute dev_attr_minfo;

/*---- timers ----
extern Suapi_Kernel_Timer *timers;
*/
extern atomic_t timer_count;

/*---- logger ----*/
extern char* log_buffer;
extern int log_buffer_size;
extern char* log_read_ptr;
extern char* log_write_ptr;
extern int log_buffer_overwritten;
extern int log_active;
extern unsigned int log_attempted_bytes;
extern unsigned int log_lost_bytes;
extern int log_busy_count;
extern wait_queue_head_t log_wait_q;
extern spinlock_t log_buffer_lock;

#endif /* __KERNEL__ */
