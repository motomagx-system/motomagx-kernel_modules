// Copyright (c) 2006,2007, Motorola.

// This file is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the License, or (at your option) any later version.
//
// This file is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA 02111-1307 USA  

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.
// 2007-02-05 Motorola       Add supanic ioctl.
// 2007-10-19 Motorola       Upmerge from LJ6.1 to LJ6.3 to update the new interface of PM

#ifndef SU_IOCTL_INCLUDE
#define SU_IOCTL_INCLUDE

#ifdef __KERNEL__

#include <asm/types.h>

typedef s8 INT8;
typedef u8 UINT8;
typedef s16 INT16;
typedef u16 UINT16;
typedef s32 INT32;
typedef u32 UINT32;
typedef s64 INT64;
typedef u64 UINT64;

typedef INT32 SU_RET_STATUS;

typedef UINT32 SU_QUEUE_HANDLE;
typedef void *SU_PORT_HANDLE;
typedef UINT32 SU_SEMA_HANDLE;
typedef UINT32 SU_SEMA_STATE;
typedef UINT32 SU_SIZE;

/* The following constants are used to identify the size of values passed to 
 * variable argument routines when data values are passed instead of pointers 
 * to data. 
 */
#define SU_SIZE_8  ((int)-1)  /* To pass eight bits of data */
#define SU_SIZE_16 ((int)-2)  /* To pass sixteen bits of data */
#define SU_SIZE_32 ((int)-3)  /* To pass 32 bits of  data */
#define SU_SIZE_64 ((int)-4)  /* To pass 64 bits of  data */

#endif

#include <linux/su_linux_extensions.h>

/* These constants are used as input to the create queue ioctl(). */
#define IS_FIFO_QUEUE (1)
#define IS_PRIORITY_QUEUE (2)

#define TFREE      0x00 /* all bits off */
#define TCYCLIC    0x02 /* must also be TSTARTED, or TSTOPPED */
#define TMESSAGE   0x04 /* must also be TSTARTED, or TSTOPPED, and not TEVENT */
#define TEVENT     0x08 /* must also be TSTARTED, or TSTOPPED, and not
                         * TMESSAGE
                         */

#define TSTARTED   0x10 /* only 1 of TSTARTED or TSTOPPED */
#define TSTOPPED   0x40

struct suTimer 
{
    unsigned int full_id;
    int period;
    int slip;
    int state;

    union 
    {
        struct Event_Timer
        {
            pid_t pid;
            int mask;
        } event;
        struct Message_Timer
        {
            int ph;
            void *msg;
        } message;
    } data;
};

#define stevent_pid data.event.pid
#define stevent_mask data.event.mask
#define stmessage_port data.message.ph
#define stmessage_msg data.message.msg

typedef struct SuapiStopTimer
{
    int tmhandle;
    int *time_left;
} SuapiStopTimer;


typedef struct suNameInfo
{
    UINT32 value;
    const char *name;
    long timeout; /* The SU_TIME is an int but uses LONG_MAX? */
} suNameInfo;

typedef struct SuapiSendMsgAndWait
{
    void *reply_message;
    pid_t waiter_pid;
} SuapiSendMsgAndWait;

struct SuapiQueueRegEvMask
{
    SU_QUEUE_HANDLE qhandle;
    pid_t thandle;
    unsigned int event_mask;
};

union SuapiRcvMsgFromQueue
{
    struct
    {
        SU_QUEUE_HANDLE qhandle;
        int timeout;
    } data;
    void * message;
};

struct suSendMsgAndWaitData
{
    void * message;
    SU_PORT_HANDLE phandle;
    int timeout;
};
union suCreatePortData
{
    SU_PORT_HANDLE phandle;
    struct
    {
        SU_QUEUE_HANDLE qh;
        UINT16 portid;
    } in;
};

struct suSetPortRoutingData
{
    SU_PORT_HANDLE phandle;
    UINT32 mode;
};


struct suSendMsgData {
    void * message;
    SU_PORT_HANDLE phandle;
};

/*
 * This enum defines the type of semaphore.
 */
enum {
    SU_SEMA_BINARY = 0,
    SU_SEMA_COUNTING,
    SU_SEMA_MUTEX
};

struct suIoctlCreateSemArgs {
    unsigned int initial_count;
    union {
        unsigned int bound;
        unsigned int shandle;
    } s;
};

union SuapiSemAcquireArgs {
    struct {
        unsigned int shandle;
        int timeout;
    } input;
    struct {
        unsigned int type;
        unsigned int count;
        unsigned int bound;
    } cntg;
    struct {
        unsigned int type;
        unsigned short ownpid;
        unsigned short nestcount;
    } mtx;
};

union SuapiReleaseSemArgs {
    unsigned int shandle;
    struct {
        unsigned int type;
        unsigned int count;
        unsigned int bound;
    } cntg;
    struct {
        unsigned int type;
        unsigned short ownpid;
        unsigned short nestcount;
    } mtx;
};

/* Argument for SUAPI_LOG_DATA ioctl(). */

typedef struct SuapiLogData
{
    unsigned int msg_id;       /* Message ID.                    */
    unsigned int num_pairs;    /* Number of pairs passed.        */
    va_list ap;                /* Variable arg pairs passed.     */
} SuapiLogData;

/* Argument for SUAPI_LOG_MESSAGE ioctl() */

typedef union SuapiLogMsg 
{   
    UINT32 retaction;          /* Perform action or just return. */
    struct {
        void *message;         /* Pointer to the message.        */  
        UINT32 msg_action;     /* Msg action (send, rcv, reply). */    
    } input;
} SuapiLogMsg;

union suAllocMemData {
	SU_SIZE bufsize; /* Input to SUAPI_ALLOC_MEM ioctl. */
	void *  bufptr;  /* On success, output of SUAPI_ALLOC_MEM ioctl. */
	int bestfitpartition; /* On failure, output of SUAPI_ALLOC_MEM ioctl. */
};

typedef struct SuapiEventInfo
{
    pid_t p;
    unsigned long ev;
} SuapiEventInfo;

typedef struct SuapiWaitEventInfo
{
    unsigned long ev;
    int timeout;
} SuapiWaitEventInfo;

/* The macro SU_RSET takes the Linux return code and the SUAPI error
 * code and combines them into the type SU_RET_STATUS.
 *
 * int linuxrc;          Return code from failing system call
 * enum SU_EENUM suapiec;   Error code as defined by SUAPI for failure
 */
#define SU_RSET(linuxrc, suapiec) \
        ((SU_RET_STATUS) (((linuxrc) << 8) | (suapiec)))

/* SUPANIC device ioctl enum */
enum {
    SUPANIC_PROCESS_DUMP= 0,
};

/* SUAPI device ioctl enum */
enum {
    SUAPI_PANIC_ENUM = SUAPI_PANIC,
    SUAPI_QUEUES_WITH_FD_ENUM = SUAPI_QUEUES_WITH_FD,
    SUAPI_CREATE_TIMER,
    SUAPI_DELETE_TIMER,
    SUAPI_START_TIMER,
    SUAPI_STOP_TIMER,
    SUAPI_TIMELEFT,
    SUAPI_ALLOC_EVENT,
    SUAPI_ALLOC_EVENT_MASK,
    SUAPI_VERIFY_ALLOC_EVENT_MASK,
    SUAPI_FREE_EVENT,
    SUAPI_SET_EVENT_MASK,
    SUAPI_CLEAR_EVENT_MASK,
    SUAPI_WAIT_ANY_EVENT,
    SUAPI_WAIT_ALL_EVENT,
    SUAPI_GET_EVENT_MASK,
    SUAPI_QUEUES_SEND_MSG_AND_WAIT,
    SUAPI_QUEUES_WAKE_SELECT,
    SUAPI_CREATE_QUEUE,
    SUAPI_QUEUES_SEND_MESSAGE,
    SUAPI_DELETE_QUEUE,
    SUAPI_REGISTER_EVENT_MASK_WITH_QUEUE,
    SUAPI_RECEIVE_MESSAGE_FROM_QUEUE,
    SUAPI_GET_NUM_MESSAGES_ON_QUEUE,
    SUAPI_CREATE_LOGGER_PORT,
    SUAPI_CREATE_PORT_FROM_QUEUE,
    SUAPI_PORTS_DISABLE_LOGGING,
    SUAPI_PORTS_ENABLE_LOGGING,
    SUAPI_PORTS_LOGGING_ENABLED,
    SUAPI_PORTS_GET_PORTID,
    SUAPI_PORTS_GET_ROUTING,
    SUAPI_PORTS_SET_ROUTING,
    SUAPI_DELETE_PORT,
    SUAPI_ALLOC_MEM,
    SUAPI_FREE_MEM,
    SUAPI_LOG_MESSAGE,
    SUAPI_LOG_DATA,
    SUAPI_SEM_CREATE_BINARY,
    SUAPI_SEM_CREATE_COUNTING,
    SUAPI_SEM_CREATE_MUTEX,
    SUAPI_SEM_ACQUIRE,
    SUAPI_SEM_RELEASE,
    SUAPI_SEM_DELETE,
    SUAPI_FINDNAME,
    SUAPI_REGISTERNAME,
    SUAPI_UNREGISTERNAME,
    SUAPI_GET_MS,
    SUAPI_GET_SH_MEM_SIZE,
    SUAPI_GET_SH_RGN_ADDR,
    SUAPI_GET_SYSCONFIG,
    SUAPI_SLEEP,
    SUAPI_ALLOC_LOW_POWER_FLAG,
    SUAPI_FREE_LOW_POWER_FLAG,
    SUAPI_ENABLE_LOW_POWER_FLAG,
    SUAPI_DISABLE_LOW_POWER_FLAG,
    SUAPI_GET_LOW_POWER_FLAG,
#ifdef CONFIG_SUAPI_TEST
    /* keep these at the end of list */
    SUAPI_SUSPEND_PM = 100,
    SUAPI_RESUME_PM = 101,
#endif
};

#endif /* SU_IOCTL_INCLUDE */
