// Copyright (c) 2006, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

// Date       Author         Comment
// 2006-11-06 Motorola       Port SUAPI to Linux.

/*
 * The SU_ALLOC_MEM_HDR is the structure of the word(s) that precedes the
 * memory returned by the suAllocMem service. It contains the
 * partition ID that the memory was allocated from, along with the
 * task ID of the calling task and the original amount of memory
 * requested.
 */
typedef struct SU_ALLOC_MEM_HDR
{
    UINT16 magic_pad1;
    UINT16 partID;      /* SUAPI Partition # from where memory is allocated.*/
    UINT16 taskID;       /* Task ID of the calling task. */
    UINT16 nbytes;      /* Amount of memory the caller requested. */
} SU_ALLOC_MEM_HDR;

/*
 * The SU_FREE_MEM_HDR is the structure of the second word of a memory 
 * block after it was freed. It contains the task ID of the creator
 * task, copied from the SU_ALLOC_MEM_HDR taskID at the time of freeing,
 * along with the task ID of the calling task and a magic number,
 * 0xDEAD.
 */
typedef struct SU_FREE_MEM_HDR
{
    UINT16 magic_pad;
    UINT16 magic;
    UINT16 allocatetaskID;
    UINT16 freetaskID;
} SU_FREE_MEM_HDR;
