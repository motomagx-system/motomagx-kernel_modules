// Copyright (c) 2006, Motorola All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met: 

//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer. 
//    * Redistributions in binary form must reproduce the above
//      copyright notice, this list of conditions and the following
//      disclaimer in the documentation and/or other materials provided
//      with the distribution. 
//    * Neither the name of the Motorola nor the names of its
//      contributors may be used to endorse or promote products derived
//      from this software without specific prior written permission. 

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

// Date       Author         Comment
// 2006-11-06 Motorola       Port SUAPI to Linux.

/*---- messages ----*/

/* SU_MINNODE:  The base structure which contains the forward and backward
 * pointers used in the queues implementation.  The queue head and message
 * structures (SU_SUBQUEUE_HEAD and SU_MSG_HDR) are based upon this base
 * structure.
 */
typedef struct SU_MINNODE
{
    struct SU_MINNODE * su_n_next; /* Pointer to next node in linked list.*/
    struct SU_MINNODE * su_n_prev; /* Pointer to previous node in list. */
} SU_MINNODE;

/* SU_MSG_HDR:  Contains the structure for individual message nodes. */

typedef struct SU_MSG_HDR
{
  SU_MINNODE su_minnode;        /* this must be first */
  void *replyport;              /* send reply to this port */
  unsigned int  reply_logging:1;/* indicates if logging for replies is needed*/
  unsigned int  priority:2;     /* priority of message */
  unsigned int  padding:5;      /* unused bits */
  unsigned int  length:24;      /* message size of data portion */
  UINT32 type;                  /* implementation-dependent message type */
  void *destport;               /* port handle of the destination port */
} SU_MSG_HDR;

/* The SU_AFTER_MSG_HDR structure is used to ensure proper alignment when
 * creating and accessing a SUAPI message.  The offset of the beginning_of_data
 * field is calculated when skipping over the message header in order to return
 * a pointer to the usable data portion of a message.
 */
typedef struct SU_AFTER_MSG_HDR
{
   SU_MSG_HDR su_msg_hdr;
   union beginning_of_data
   {
      INT8 su_int8;
      UINT8 su_uint8;
      INT16 su_int16;
      UINT16 su_uint16;
      INT32 su_int32;
      UINT32 su_uint32;
      INT64 su_int64;
      UINT64 su_uint64;
   } beginning_of_data;
} SU_AFTER_MSG_HDR;

#define SU_SIZEOF_MSG_HDR      ((UINT32)(offsetof(SU_AFTER_MSG_HDR, \
                                                  beginning_of_data)))

#define suGetMessageReplyPort(msg) ((SU_MSG2HDR(msg))->replyport)
#define suSetMessageReplyPort(msg, msgreplyport) \
   ((SU_MSG2HDR(msg))->replyport = (msgreplyport))
#define suGetMessageLength(msg) \
                   ((SU_MSG2HDR(msg))->length)

#define suGetMessageType(msg) \
                   ((SU_MSG2HDR(msg))->type)

#define suSetMessageType(msg, msgtype) \
   ((SU_MSG2HDR(msg))->type = (msgtype))

#define suGetMessagePortHandle(msg) ((SU_MSG2HDR(msg))->destport)

#define suGetMessagePortId(msg) (((SU_MSG2HDR(msg))->destport)->port_id)
#define suGetMessagePriority(msg) ((UINT8)((SU_MSG2HDR(msg))->priority))

#define suSetMessagePriority(msg, msgpriority) \
   ((SU_MSG2HDR(msg))->priority = ((UINT8) msgpriority))

#define suGetMessageReplyLoggingBit(msg) ((SU_MSG2HDR(msg))->reply_logging)

/* This macro returns a pointer to message header */
#define SU_MSG2HDR(msgp) \
   ((SU_MSG_HDR *)(((UINT32) (msgp)) - (SU_SIZEOF_MSG_HDR)))

/* This macro returns a pointer to message data */
#define SU_HDR2MSG(msg_hdr_p) \
   ((void *)(((UINT32) (msg_hdr_p)) + (SU_SIZEOF_MSG_HDR)))
