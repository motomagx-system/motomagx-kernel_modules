// This file is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1 of
// the License, or (at your option) any later version.
//
// This file is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free
// Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
// MA 02111-1307 USA

// Copyright (c) 2006, Motorola

// Date       Author         Comment
// 2006-11-14 Motorola       Switch to RTC stopwatch timer.

/************** HEADER FILE INCLUDES *****************************************/
#include <linux/config.h>
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
#ifdef __KERNEL__
#include <linux/rtc.h>
#endif
#endif

/************** CONSTANTS ****************************************************/

/************** STRUCTURES, ENUMS, AND TYPEDEFS ******************************/
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
/* The following structure has been copied from rtc.h
 * Care must be taken while modifying this structure.
 */
#if !defined(__KERNEL__)
struct rtc_sw_request {
        /* stored in ticks */
        unsigned long interval;       /* normal alarm interval */
        unsigned long remain;         /* remaining time in interval */

        int type;                     /* strict or fuzzy timer */

        int dummy1[5];

        void *prev;
        void *next;

        int dummy2[7];
};
#endif

typedef struct Suapi_Kernel_Timer
{
    struct suTimer su_timer_user;
    struct rtc_sw_request ltimer;
    spinlock_t sut_lock;
} Suapi_Kernel_Timer;
#else
typedef struct Suapi_Kernel_Timer
{
    struct suTimer su_timer_user;
    struct timer_list ltimer;
    spinlock_t sut_lock;
} Suapi_Kernel_Timer;
#endif

/************** GLOBAL FUNCTION PROTOTYPES ***********************************/

/************** MACRO DEFINITIONS ********************************************/
#if defined(CONFIG_MOT_FEAT_ACTIVE_AND_LOW_POWER_INTERVAL_TIMERS)
#define TIMEOUT_FIELD  interval
#else
#define TIMEOUT_FIELD  expires
#endif

/************** GLOBAL VARIABLES *********************************************/

extern Suapi_Kernel_Timer *timers;

