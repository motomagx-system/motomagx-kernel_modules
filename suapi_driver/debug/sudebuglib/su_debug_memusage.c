// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the memusage SUAPI debug library function.
 *
 *      NOTE that this code is not reentrant.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>
#include <linux/su_mem_hdr.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/
/* TASK_HASH_SIZE defines the size of the task list hash array. */
#define TASK_HASH_SIZE 50

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(void);
static void cleanup(void);
static void print_task_usage (SU_HASH_HANDLE hash_h,
                              unsigned int key,
                              unsigned int arg);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/
static SU_HASH_HANDLE task_usgcnt_hash = (SU_HASH_HANDLE) 0;
static SU_HASH_HANDLE task_requested_hash = (SU_HASH_HANDLE) 0;
static SU_HASH_HANDLE free_block_hash = (SU_HASH_HANDLE) 0;
static SU_HASH_HANDLE task_name_hash = (SU_HASH_HANDLE) 0;

/************** FUNCTIONS ****************************************************/
void cleanup(void)
{
    /* hash_delete() silently ignores invalid hash handles. */
    su_hash_delete(task_usgcnt_hash);
    su_hash_delete(task_requested_hash);
    su_hash_delete(free_block_hash);
    task_requested_hash = (SU_HASH_HANDLE) 0;
    task_usgcnt_hash = (SU_HASH_HANDLE) 0;
    free_block_hash = (SU_HASH_HANDLE) 0;
    task_name_hash = (SU_HASH_HANDLE) 0;
}

/* This function is called by su_hash_traverse_all_keys() for each key in the
 * hash tree.
 */
static void print_task_usage (SU_HASH_HANDLE hash_h,
                              unsigned int key,
                              unsigned int arg)
{
    unsigned int number_of_requests, total_requested;
    pid_t pid = (pid_t) key;
    SU_PCB * u_pcbp = (SU_PCB *) arg;
    int bytes_used;
    char * pidname = NO_CMDLINE;

    /* Get number of times this task requested memory from this partition. */
    if (su_hash_get_value(task_usgcnt_hash, key, &number_of_requests) < 0)
        /* Something bad happened, so just return. */
        return;

    /* Calculate the number of bytes used by multiplying the actual number
     * of usable bytes in a block by the number of times the task requested
     * memory.
     */
    bytes_used = number_of_requests *
                        (u_pcbp->blocksize - sizeof(SU_ALLOC_MEM_HDR));

    /* Get the total memory requested by this task from this partition. */
    if (su_hash_get_value(task_requested_hash, key, &total_requested) < 0)
        /* Something bad happened, so just return. */
        return;

    /* Get the name of the process. */
    su_hash_get_value(task_name_hash, key, (unsigned int *)(&pidname));

    printf("Task %2d (%s) Usage: %4d   ", pid, pidname, number_of_requests);
    printf("Bytes requested: %6d   ", total_requested);
    printf("Bytes used: %6d", bytes_used);
    printf("  (%.1f%%)\n", total_requested * 100.0 / bytes_used);
}

/* This service assumes parameters are vaild.
 *      ptrs: list of pointers into private and shared SUAPI memory.
 *      verbose: Caller wants verbose output
 *      pidstart: Starting index of partition to check.
 *      pidend: Ending index of partition to check.
 *
 * RETURN VALUES:
 *     0 on success
 *     SU_DEBUG_FAILURE_NO_MEM when an internal malloc() fails.
 */
int suapi_debug_memusage(SU_REGION_PTRS * ptrs,
                         int verbose,
                         int pidstart,
                         int pidend)
{
    int partid, retval;
    SU_GLOBAL_DATA * su_global_data;
    SU_SYSCONFIG * su_sysconfig;
    SU_PCB * u_pcb_tablep;
    char * pidname;

    /* The user space shared memory address used by SUAPI on the phone. */
    void * su_uaddr;

    su_global_data = (SU_GLOBAL_DATA *) ptrs->shared;
    su_sysconfig = (SU_SYSCONFIG *) ptrs->sysconfig;
    su_uaddr = su_global_data->user_shmaddress;
    task_name_hash = ptrs->prochash;

    /* Output memory promotion value. */
    printf("\n");
    if (su_sysconfig->memorypromotion == 1)
        printf("Memory promotion:           ON\n\n");
    else
        printf("Memory promotion:           OFF\n\n");

    u_pcb_tablep = (SU_PCB *) ptrs->pcb;

    /* Output partition data from pidstart to pidend. */
    for (partid = pidstart; partid <= pidend; partid++)
    {
        int unused, nfree, blocknum;
        int partition_corrupted = 0;
        void * k_freep, * k_blockp;
        unsigned int v;
        SU_PCB * u_pcb;

        unused = 0;

        /* Get partition information for the current partition. */
        u_pcb = &(u_pcb_tablep[partid]);

#if SUAPI_DEBUG
        printf("pcb for partition %d:\n", partid);
        printf("\tpcb[%d].first_free_block == 0x%08X\n", partid, u_pcb->first_free_block);
        printf("\tpcb[%d].part_start_add == 0x%08X\n", partid, u_pcb->part_start_add);
        printf("\tpcb[%d].part_end_add == 0x%08X\n", partid, u_pcb->part_end_add);
        printf("\tpcb[%d].blocksize == %d\n", partid, u_pcb->blocksize);
        printf("\tpcb[%d].nb_blocks == %d\n", partid, u_pcb->nb_blocks);
        printf("\tpcb[%d].free_buffers == %d\n", partid, u_pcb->free_buffers);
        printf("\tpcb[%d].min_free_buffers == %d\n", partid, u_pcb->min_free_buffers);
#endif

        /* If u_pcb->free_buffers is 0, at least allocate 1 hash. Then, if
         * that value is corrupted and there really are blocks in the free
         * list, the free list hash will at least work.
         */
        free_block_hash = su_hash_create(
                           (u_pcb->free_buffers) ? (u_pcb->free_buffers) : (1) );
        if (free_block_hash == NULL)
        {
            cleanup();
            return SU_DEBUG_FAILURE_NO_MEM;
        }

#if SUAPI_DEBUG
        printf("free_block_hash in partition %d:\n", partid);
        printf("\tfree_block_hash->size == %d\n", ((SU_HASH *)free_block_hash)->size);
        printf("\ttfree_block_hash->root == 0x%08X\n",
                                    ((SU_HASH *)free_block_hash)->root);
        printf("\ttfree_block_hash->hash_array == 0x%08X\n",
                                    ((SU_HASH *)free_block_hash)->hash_array);
#endif

        /* Initialize hash table that will contain the usage count for each
         * task using blocks in this partition.
         */
        task_usgcnt_hash = su_hash_create(TASK_HASH_SIZE);
        if (task_usgcnt_hash == NULL)
        {
            cleanup();
            return SU_DEBUG_FAILURE_NO_MEM;
        }

#if SUAPI_DEBUG
        printf("task_usgcnt_hash in partition %d:\n", partid);
        printf("\ttask_usgcnt_hash->size == %d\n", ((SU_HASH *)task_usgcnt_hash)->size);
        printf("\tttask_usgcnt_hash->root == 0x%08X\n",
                                        ((SU_HASH *)task_usgcnt_hash)->root);
        printf("\tttask_usgcnt_hash->hash_array == 0x%08X\n",
                                        ((SU_HASH *)task_usgcnt_hash)->hash_array);
#endif

        /* Initialize hash table that will contain the total amount of requested
         * blocks for each task using blocks in this partition.
         */
        task_requested_hash = su_hash_create(TASK_HASH_SIZE);
        if (task_requested_hash == NULL)
        {
            cleanup();
            return SU_DEBUG_FAILURE_NO_MEM;
        }

#if SUAPI_DEBUG
        printf("task_requested_hash in partition %d:\n", partid);
        printf("\ttask_requested_hash->size == %d\n",
                                  ((SU_HASH *)task_requested_hash)->size);
        printf("\ttask_requested_hash->root == 0x%08X\n",
                                  ((SU_HASH *)task_requested_hash)->root);
        printf("\ttask_requested_hash->hash_array == 0x%08X\n",
                                  ((SU_HASH *)task_requested_hash)->hash_array);
#endif

        printf("Analyzing Partition %d (%d blocks of size %d)\n",
                partid, u_pcb->nb_blocks, u_pcb->blocksize);

        /* Print the address if in verbose mode. */
        if (verbose)
            printf("\nPartition %d address --> 0x%08X\n",
                    partid, K2U(su_uaddr, u_pcb->part_start_add));

        /* Loop through free list once and hash the results so that later it
         * can be determined whether or not a block is supposed to be on the
         * free list without having to search the list again.
         *
         * Use nb_blocks (the total number of blocks in this partition) as the
         * upper bound instead of free_buffers (number of blocks on free list)
         * so that the number free in the PCB can be compared against the actual
         * number found on the free list.
         */
        for (nfree = 0, k_freep = u_pcb->first_free_block;
             nfree < u_pcb->nb_blocks && k_freep != NULL;
             nfree++)
        {
            if ( (k_freep < u_pcb->part_start_add ) ||
                 (k_freep >= u_pcb->part_end_add  ) ||
                 ((k_freep - u_pcb->part_start_add) % u_pcb->blocksize) )
            {
#if SUAPI_DEBUG
                printf("corrupted free pointer == 0x%08X\n", k_freep);
#endif
                partition_corrupted = 1;
                break;
            }

            /* Add this block pointer to the free block list. */
            retval = su_hash_add(free_block_hash, (unsigned int) k_freep, 1);
            if (retval != 0)
            {
                /* Although su_hash_add() may fail due to either bad HASH handle
                 * or malloc failure, this check assumes malloc failure since
                 * bad SU_HASH handle was already checked.
                 */
                cleanup();
                return SU_DEBUG_FAILURE_NO_MEM;
            }

            /* Turn k_freep into a pointer that can be used by memusage,
             * then skip the free hdr, then dereference to get the next
             * k_freep pointer in the free list.
             */
            k_freep = *( (void **) (K2U(ptrs->shared,k_freep) +
                                      sizeof(SU_FREE_MEM_HDR) ) );
        }

        if (partition_corrupted)
        {
            printf("Partition is corrupted, the block counts will not be\n");
            printf("accurate.  Use chkpart to check the consistency of this\n");
            printf("partition.\n\n");
        }

        /* Loop over each block. Determine if it is used or not. If it is used,
         * increment the total number of bytes requested by each task.
         */
        for (k_blockp = u_pcb->part_start_add, blocknum = 0;
             blocknum < u_pcb->nb_blocks;
             blocknum++, k_blockp += u_pcb->blocksize)
        {
            SU_ALLOC_MEM_HDR * u_memhdrp;

            u_memhdrp = (SU_ALLOC_MEM_HDR *) K2U(ptrs->shared,k_blockp);

            /* If su_hash_get_value() returns a negative value, this block is
             * not in the free list hash. Also check to that the header looks
             * consistent with an allocated block.
             */
            if (su_hash_get_value(free_block_hash,(unsigned int)k_blockp,&v) < 0
             && u_memhdrp->magic_pad1 == 0xFEED
             && u_memhdrp->partID == partid       )
            {

#if SUAPI_DEBUG
            pidname = NO_CMDLINE;
            su_hash_get_value(task_name_hash,u_memhdrp->taskID,(unsigned int *)(&pidname));
            printf("Part %d:\n\
                Block 0x%08X:\n\
                    u_memhdrp->taskID == %d (%s)\n\
                    u_memhdrp->nbytes == %d\n",
                    partid, k_blockp, u_memhdrp->taskID, pidname, u_memhdrp->nbytes);
#endif

                /* task_usgcnt_hash is being used to count the number times a
                 * task has allocated blocks from this partition.
                 *
                 * If taskID doesn't already exist in the task_usgcnt_hash
                 * hash table, su_hash_increment_value() will add the taskID to
                 * the hash table with an initial value of 1. If taskID already
                 * exists in the hash table, su_hash_increment_value() will
                 * increment the existing value by the specified amount (1, in
                 * this case).
                 */
                retval = su_hash_increment_value(task_usgcnt_hash,
                                               (unsigned int) u_memhdrp->taskID,
                                               1);
                if (retval != 0)
                {
                    cleanup();
                    return SU_DEBUG_FAILURE_NO_MEM;
                }

                /* task_requested_hash is being used to keep track of the total
                 * amount of memory each task has requested from this partition.
                 */
                retval = su_hash_increment_value(task_requested_hash,
                                              (unsigned int) u_memhdrp->taskID,
                                              (unsigned int) u_memhdrp->nbytes);
                if (retval != 0)
                {
                    cleanup();
                    return SU_DEBUG_FAILURE_NO_MEM;
                }

                if (verbose)
                {
                    pidname = NO_CMDLINE;
                    su_hash_get_value(task_name_hash,
                                      u_memhdrp->taskID,
                                      (unsigned int *) (&pidname));
                    printf("  Block %3d Info     %5d (%s)    %5d   0x%08x\n",
                           blocknum, u_memhdrp->taskID, pidname,
                           u_memhdrp->nbytes, K2U(su_uaddr, k_blockp));
                }
            }
            else /* block is unused */
            {
                unused++;
                if (verbose)
                {
                    printf ("  Block %3d Unused                    0x%08x\n",
                             blocknum, K2U(su_uaddr, k_blockp));
                }
            }
        } /* end of looping through blocks in a partition */

        if (blocknum != u_pcb->nb_blocks)
        {
            printf("Start address field is corrupted in PCB of partition %d\n",
                   blocknum);
        }

        /* hash_traverse_all_keys() will run over all of the keys of the
         * specified hash in ascending order and, for each key, run the
         * specified function.
         */
        su_hash_traverse_all_keys(task_usgcnt_hash,
                                  print_task_usage,
                                  (unsigned int) u_pcb);

        /* Print how many blocks are unused */
        printf("Unused Blocks: %5d\n\n", unused);

        if (unused != nfree)
        {
            printf("Number of blks on free list (%d) different ", nfree);
            printf("from number of blks counted free (%d)\n", unused);
        }

        su_hash_delete(free_block_hash);
        su_hash_delete(task_usgcnt_hash);
        su_hash_delete(task_requested_hash);
        task_requested_hash = (SU_HASH_HANDLE) 0;
        task_usgcnt_hash = (SU_HASH_HANDLE) 0;
        free_block_hash = (SU_HASH_HANDLE) 0;
    } /* end of looping through partitions */

    cleanup();
    return 0;
}
