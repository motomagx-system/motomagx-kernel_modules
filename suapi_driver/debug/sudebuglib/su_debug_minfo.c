// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the minfo SUAPI debug library function.
 *
 *      NOTE that this code is not reentrant.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/
#define MINFOLINE 50

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/

/* This service assumes parameters are vaild.
 *      ptrs: list of pointers into private and shared SUAPI memory.
 *
 * RETURN VALUES:
 *     0 on success
 *     SU_DEBUG_FAILURE_NO_MEM when an internal malloc() fails.
 */
int suapi_debug_minfo(SU_REGION_PTRS * ptrs)
{
    int partid, retval;
    SU_GLOBAL_DATA * su_global_data;
    SU_SYSCONFIG * su_sysconfig;
    SU_PCB * u_pcb_tablep;

    su_global_data = (SU_GLOBAL_DATA *) ptrs->shared;
    su_sysconfig = (SU_SYSCONFIG *) ptrs->sysconfig;

    printf("\n\
Memory promotion is %s\n\
              Alloc  Total  Hiwt   Start\n\
ID Block Size Blocks Blocks Blocks Address\n\
== ========== ====== ====== ====== ==========\n",
        (su_sysconfig->memorypromotion)?"ON":"OFF");

    u_pcb_tablep = (SU_PCB *) ptrs->pcb;

    for(partid = 0; partid < su_sysconfig->npartition; partid++)
    {
        SU_PCB * u_pcb;
        int retval;

        /* Get partition information for the current partition. */
        u_pcb = &(u_pcb_tablep[partid]);

        printf("%2d 0x%08x 0x%04x 0x%04x 0x%04x 0x%08x\n",
            partid,                                 /* partition ID           */
            u_pcb->blocksize,                       /* block size             */
            u_pcb->nb_blocks - u_pcb->free_buffers, /* number allocated       */
            u_pcb->nb_blocks,                       /* total number of blocks */
                                                    /* high water mark        */
            u_pcb->nb_blocks - u_pcb->min_free_buffers,
                                                    /* start address          */
            K2U(su_global_data->user_shmaddress, u_pcb->part_start_add));
    }

    return 0;
}
