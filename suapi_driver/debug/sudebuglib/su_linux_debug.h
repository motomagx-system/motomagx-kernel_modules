// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      Header file for SUAPI debugging services on Linux
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.

/************** INCLUDES *****************************************************/
#define _LARGEFILE64_SOURCE
#define _XOPEN_SOURCE
#define _ISOC99_SOURCE

#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
#include <stdarg.h>
#include <stdio.h>

/* The __KERNEL__ define is necessary to access the hlist structures from
 * list.h. The other defines are necessary to prevent inclusion of unnecessary
 * and unrelated kernel structures.
 */
#define __KERNEL__
#ifndef _LINUX_STDDEF_H
#define _LINUX_STDDEF_H
#endif
#ifndef _LINUX_PREFETCH_H
#define _LINUX_PREFETCH_H
#endif
#ifndef __ASM_ARM_SYSTEM_H
#define __ASM_ARM_SYSTEM_H
#endif

#include <asm/types.h>
#include <linux/list.h>


/* Now undefine __KERNEL__ so that it doesn't interfere with includes within
 * files which include this file.
 */
#undef __KERNEL__

#include <linux/timer.h>

/* this section copied from suapi_module.h as I was not able to set
 * __KERNEL__ to get it without causing numerous issues with included
 * makefiles.
 */
typedef s8 INT8;
typedef u8 UINT8;
typedef s16 INT16;
typedef u16 UINT16;
typedef s32 INT32;
typedef u32 UINT32;
typedef s64 INT64;
typedef u64 UINT64;

typedef INT32 SU_RET_STATUS;

typedef UINT32 SU_QUEUE_HANDLE;
typedef void *SU_PORT_HANDLE;
typedef UINT32 SU_SEMA_HANDLE;

typedef UINT32 SU_SEMA_STATE;
typedef UINT32 SU_SIZE;

/* The following constants are used to identify the size of values passed to 
 * variable argument routines when data values are passed instead of pointers 
 * to data. 
 */
#define SU_SIZE_8  ((int)-1)  /* To pass eight bits of data */
#define SU_SIZE_16 ((int)-2)  /* To pass sixteen bits of data */
#define SU_SIZE_32 ((int)-3)  /* To pass 32 bits of  data */
#define SU_SIZE_64 ((int)-4)  /* To pass 64 bits of  data */

/* end of section copied from suapi_module.h */

#include <linux/suapi_module.h>
#include <su_hash.h>

/************** CONSTANTS ****************************************************/
#define SUAPI_SHARED_DEVICE "/dev/suapi"
#define SUAPI_PRIVATE_DEVICE "/dev/suapipriv"
#define NO_CMDLINE ""

/*------ timers -------*/
#define TFREE      0x00 /* all bits off */
#define TCYCLIC    0x02 /* must also be TSTARTED, or TSTOPPED */
#define TMESSAGE   0x04 /* must also be TSTARTED, or TSTOPPED, and not TEVENT */
#define TEVENT     0x08 /* must also be TSTARTED, or TSTOPPED, and not 
                         * TMESSAGE
                         */
#define TSTARTED   0x10 /* only 1 of TSTARTED or TSTOPPED */
#define TSTOPPED   0x40

/* DEFAULT_DATA_LENGTH: the default number of bytes to print when dumping
 * message data.
 */
#define DEFAULT_DATA_LENGTH 0x200


/************** STRUCTURES, ENUMS, AND TYPEDEFS ******************************/
/*------ queues -------*/
/* Constants defined in su_kqueues.c */
#define SU_QAVAIL  0
#define SU_QUSED  -1
#define IS_FIFO_QUEUE (1)       /* queue is allocated as a fifo queue */
#define IS_PRIORITY_QUEUE (2)   /* queue is allocated as a priority queue */
#define LOW 0                   /* low priority linked list for this queue */
#define MEDIUM 1                /* medium priority linked list for this queue */
#define HIGH 2                  /* high priority linked list for this queue */

enum {
    SU_DEBUG_FAILURE_BAD_PARAMETER = 1,
    SU_DEBUG_FAILURE_FILE_OPEN,
    SU_DEBUG_FAILURE_FILE_READ,
    SU_DEBUG_FAILURE_FILE_SEEK,
    SU_DEBUG_FAILURE_NO_MEM,
    SU_DEBUG_FAILURE_BAD_ADDRESS,
};

typedef struct SU_REGION_PTRS {
    void * private;
    void * sysconfig;
    void * timers;
    void * semarray;
    void * Names;
    void * PortTable;
    void * qarray;
    void * pcb;
    void * shared;
    SU_HASH_HANDLE prochash;
    unsigned short procsize;
    unsigned short core_file;
} SU_REGION_PTRS;

/************** MACROS *******************************************************/
/* CVT converts the specified address to and from the specified base addresses.
 *
 * 'addr'     is the address to be converted
 * 'frombase' is the base from which the address is being converted
 * 'tobase'   is the base to which the address is being converted
 */
#define CVT(addr,frombase,tobase) \
                ( (void *)(tobase) + ((void *)(addr) - (void *)(frombase)) )

/* K2U converts the specified address from a kernel address to a user space
 * address in the SUAPI shared memory region. It does this by computing the
 * offset from 'base' into the region.
 *
 * 'base' address representing the beginning of shared memory in user space.
 * 'addr' is a kernel address somewhere in the shared memory region.
 *
 * This macro assumes that 'ptrs->private' is a pointer to the beginning of the
 * SUAPI private memory region which has been mmap()-ed to the current process.
 */
#define K2U(base,addr) \
    ( ((void *)(base)) + \
        (((void *)(addr)) - ((SU_PRIVATE_DATA *)(ptrs->private))->kshared_p) )

/* Used in calculating sizes of structures. */
#define SU_ROUND_TO_DOUBLE_WORD(value) \
    ((value < sizeof(long long)) ? \
     sizeof(long long) : \
    (((value + (sizeof(long long) - 1))/sizeof(long long))*(sizeof(long long))) )

/* lseekread seeks to a position in the specified file and reads from that
 * location.  lseekread is a macro instead of a function so that the calling
 * code isn't cluttered with the error handling.
 *
 * The cast to loff_t on offset allows lseek64() to interpret offset as
 * a positive number when offset >= 0x80000000.
 */
#define lseekread(fd, offset, buf, nbytes)                                  \
{                                                                           \
    if (lseek64((fd), (loff_t) (offset), SEEK_SET) == -1)                   \
    {                                                                       \
        fprintf(stderr, "%s: unable to seek to kernel address 0x%08X: ",    \
                argv[0], (unsigned int) (offset));                          \
        perror("");                                                         \
        return SU_DEBUG_FAILURE_FILE_READ;                                  \
    }                                                                       \
    if (read((fd), (void *) (buf), (nbytes)) < 0)                           \
    {                                                                       \
        fprintf(stderr, "%s: unable to read kernel address 0x%08X: ",       \
                argv[0], (unsigned int) (offset));                          \
        perror("");                                                         \
        return SU_DEBUG_FAILURE_FILE_READ;                                  \
    }                                                                       \
}

/* These macros is defined in su_kqueues.c */
#define SU_QH2QINDEX(qh) ((int)(qh) - 1)
#define SU_QINDEX2QH(index) ((int) (index) + 1)

/************** PROTOTYPES ***************************************************/
int suapi_debug_chkpart(SU_REGION_PTRS * ptrs,
                        int maxdepth,
                        int verbose,
                        int pidstart,
                        int pidend);

int suapi_debug_memusage(SU_REGION_PTRS * ptrs,
                         int verbose,
                         int pidstart,
                         int pidend);

int suapi_debug_minfo(SU_REGION_PTRS * ptrs);

int suapi_debug_tminfo(SU_REGION_PTRS * ptrs, int a_flag);

int suapi_debug_tm(SU_REGION_PTRS * ptrs, int tmindex);

int suapi_debug_tmlist(SU_REGION_PTRS * ptrs);

int suapi_debug_mesg(SU_REGION_PTRS * ptrs, SU_MSG_HDR * msghdrp, unsigned int limit);

int suapi_debug_mesgdata(SU_REGION_PTRS * ptrs, SU_MSG_HDR * msghdrp, unsigned int limit);

void * cvt_only_if_in_shared(SU_REGION_PTRS * ptrs, void * addr);

int su_map_files(char * suapi_file_name, SU_REGION_PTRS * ptrs);

void su_map_cleanup(SU_REGION_PTRS * ptrs);
