// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the chkpart SUAPI debug library function.
 *
 *      NOTE that this code is not reentrant.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>
#include <linux/su_mem_hdr.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/
/* #define SUAPI_DEBUG 1 */

#define MAXNULLBLOCKS 20

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void print_null_free_list_pointers (SU_HASH_HANDLE hash_h,
                                           unsigned int key,
                                           unsigned int arg);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/
static int fd = -1;

/************** FUNCTIONS ****************************************************/

/* This function will be called when traversing the null free list pointer
 * hash table and will print out all NULL pointers on the free list.
 */
static void print_null_free_list_pointers (SU_HASH_HANDLE hash_h,
                                           unsigned int key,
                                           unsigned int arg)
{
    unsigned int value;

    su_hash_get_value(hash_h, key, &value);
    printf("    Index %3d (0x%08x):  NULL FREE LIST POINTER\n", key, value );
}

/* This service assumes parameters are vaild.
 *      ptrs: list of pointers into private and shared SUAPI memory.
 *      maxdepth: Maximum depth for searching the free list.
 *      verbose: Caller wants verbose output
 *      pidstart: Starting index of partition to check.
 *      pidend: Ending index of partition to check.
 *
 * RETURN VALUES:
 *     0 on success
 *     SU_DEBUG_FAILURE_NO_MEM when an internal malloc() fails.
 */
int suapi_debug_chkpart(SU_REGION_PTRS * ptrs,
                        int maxdepth,
                        int verbose,
                        int pidstart,
                        int pidend)
{
    int partid, retval;
    SU_PRIVATE_DATA * private_data_p;
    SU_GLOBAL_DATA * global_data_p;
    SU_SYSCONFIG * su_sysconfig_p;
    SU_PCB * u_pcb_tablep;

    /* The user space shared memory address that the phone was using. */
    void * su_uaddr;

    private_data_p = (SU_PRIVATE_DATA *) ptrs->private;
    global_data_p = (SU_GLOBAL_DATA *) ptrs->shared;
    su_sysconfig_p = (SU_SYSCONFIG *) ptrs->sysconfig;
    su_uaddr = global_data_p->user_shmaddress;

    /* Nothing to do if there are no partitions. */
    if (su_sysconfig_p->npartition == 0)
    {
        printf("There are no configured partitions.\n");
        return 0;
    }

    u_pcb_tablep = (SU_PCB *) ptrs->pcb;

    /* Process each requested partition. */
    for (partid = pidstart; partid <= pidend; partid++)
    {
        SU_HASH_HANDLE null_next_free_hash = (SU_HASH_HANDLE) 0;
        SU_PCB * u_pcb;
        int blocknum;
        void * k_blockp;
        void * k_prev;
        void * k_freep;
        int found_null = 0;
        int found_alloc = 0;
        int found_free = 0;
        int found_bad = 0;
        int found_error = 0;                /* set to 1 if any error found */
        int bytes_requested = 0;
        int freelisterror = 0;
        int nfree = 0;

        printf("\nChecking partition %d for consistency.\n", partid);

        /* Get partition information for the current partition being checked. */
        u_pcb = &(u_pcb_tablep[partid]);

        if (verbose)
        {
            printf("Partition %d PCB: Size=%d Alloc=%d Total=%d \
start=0x%08x end=0x%08x\n",
                   partid,                            /* partition ID         */
                   u_pcb->blocksize,                  /* block size           */
                   u_pcb->nb_blocks-u_pcb->free_buffers,/* allocated blocks   */
                   u_pcb->nb_blocks,                  /* total blocks         */
                   K2U(su_uaddr,u_pcb->part_start_add),/* partition start addr */
                   K2U(su_uaddr, u_pcb->part_end_add)  /* partition end addr */);
        }

        null_next_free_hash = su_hash_create(MAXNULLBLOCKS);
        if (null_next_free_hash == NULL)
            return SU_DEBUG_FAILURE_NO_MEM;

        /* Loop through all blocks in the specified partition and check the
         * validity of each one.
         */
        for (k_blockp = u_pcb->part_start_add, blocknum = 0;
             blocknum < u_pcb->nb_blocks;
             blocknum++, k_blockp += u_pcb->blocksize)
        {
            SU_ALLOC_MEM_HDR *u_allochdrp;
            SU_FREE_MEM_HDR  *u_freehdrp;
            unsigned int offset;
            unsigned int calculated_offset;

            u_allochdrp = (SU_ALLOC_MEM_HDR *) (K2U(ptrs->shared, k_blockp));
            u_freehdrp = (SU_FREE_MEM_HDR *) (K2U(ptrs->shared, k_blockp));

            /* If this is a free block, the word after the ALLOC/FREE memory
             * header should contain a link in the free list. But even if the
             * block looks allocated, there are tests that can be done on that
             * word to help identify corruption.
             */
            k_freep = *((void **)(u_freehdrp + 1));

            offset = k_freep - u_pcb->part_start_add;
            calculated_offset = (offset/u_pcb->blocksize) * u_pcb->blocksize;

            if (verbose)
                printf("    Index %3d (0x%08x):  ",
                       blocknum, K2U(su_uaddr,k_blockp));

            /* IF the block appears free based on the magic numbers
             *    AND if the word after the SU_FREE_MEM_HDR header is NULL
             *        OR if the word after the SU_FREE_MEM_HDR header is within
             *           this partition and lies on a block boundary,
             * THEN this block must be free.
             */
            if (   ( u_freehdrp->magic == 0xDEAD                     )
                && ( u_freehdrp->magic_pad == 0xBEEF                 )
                && (    ( k_freep == NULL                          )
                     || ( ( k_freep >= u_pcb->part_start_add     )
                       && ( k_freep < u_pcb->part_end_add        )
                       && ( offset == calculated_offset          ) ) ) )
            {
                if (verbose)
                    printf("FREE   nextptr=0x%08x\n",
                            (k_freep) ? K2U(su_uaddr,k_freep) : k_freep);

                found_free++;

                /* If this is a free block, then keep track of which blocks
                 * contain NULL pointers on the free list (ie, a NULL pointer
                 * should indicate the end of the free list). This will be
                 * checked later for errors.
                 */
                if (k_freep == NULL)
                {
                    found_null++;
                    if (found_null <= MAXNULLBLOCKS)
                    {
                        retval = su_hash_add(null_next_free_hash,
                                          blocknum,
                                          (unsigned int) K2U(su_uaddr,k_blockp));
                        if (retval < 0)
                        {
                          /* Although su_hash_add() may fail due to either bad
                           * HASH handle or malloc failure, this check assumes
                           * malloc failure since bad SU_HASH handle was
                           * already checked.
                           */
                            su_hash_delete(null_next_free_hash);
                            return SU_DEBUG_FAILURE_NO_MEM;
                        }
                    }
                }
            }
            /* If it's not free, check that this block is allocated. */
            else if( (u_allochdrp->magic_pad1 == 0xFEED                     )
                  && (u_allochdrp->partID == partid                         )
                  && (u_allochdrp->nbytes <=
                             u_pcb->blocksize - sizeof(SU_ALLOC_MEM_HDR)    )  )
            {
                if (verbose)
                {
                    char * pidname = NO_CMDLINE;

                    su_hash_get_value(ptrs->prochash,
                                      u_allochdrp->taskID,
                                      (unsigned int *)(&pidname));
                    printf("ALLOC  ");
                    printf("partid=0x%02x taskid=%02d (%s) size=0x%04x\n",
                            u_allochdrp->partID,
                            u_allochdrp->taskID,
                            pidname,
                            u_allochdrp->nbytes);
                }
                found_alloc++;
                bytes_requested += u_allochdrp->nbytes;
            }
            /* It's not free and it's not allocated, so it must be bad. */
            else
            {
                if (verbose == 0)
                    printf("    Index %3d (0x%08x):  ",
                            blocknum, K2U(su_uaddr,k_blockp));

                /* Print both words of the header. */
                printf("BADBLK header = 0x%08x 0x%08x\n",
                        *((unsigned int *)u_freehdrp),
                        *(((unsigned int *)u_freehdrp)+1));

                found_bad++;
                found_error++;
            }
        }

        /* If there are free blocks we should have found the end of the free
         * list.
         */
        if ((found_null == 0) && (found_free != 0))
        {
            found_error++;
            printf("No end of free list found in Partition %d\n", partid);
        }
        /* There should only be one end of the free list. */
        else if (found_null > 1)
        {
            found_error++;
            freelisterror++;
            printf("Found %d null pointers in free list of partition %d. Only \
1 should exist.\n", found_null, partid);

            if (found_null > MAXNULLBLOCKS)
                printf("Printing only the first %d null headers.\n",
                        MAXNULLBLOCKS);

            su_hash_traverse_all_keys(null_next_free_hash,
                                      print_null_free_list_pointers,
                                      0);
        }

        su_hash_delete(null_next_free_hash);

        /* Print out efficiency of all current allocations. */
        if (found_alloc == 0)
        {
            printf(
                  "Allocation Efficiency: No allocations in this partition.\n");
        }
        else
        {
            int bytes_reserved;
            float efficiency;

            bytes_reserved =
                  (found_alloc * (u_pcb->blocksize - sizeof(SU_ALLOC_MEM_HDR)));
            efficiency = ((bytes_requested * 100.0) / bytes_reserved);

            printf("Allocation Efficiency: (%d bytes requested / %d bytes \
reserved) = %.1f%%\n", bytes_requested, bytes_reserved, efficiency);
        }

        if (found_bad != 0)
        {
            /* Free count in PCB doesn't match what was found. */
            if (found_free != u_pcb->free_buffers)
            {
                found_error++;
                printf("    The number of free blocks found (%d) was different \
than the number in the PCB (%d) of partition %d.\n",
found_free, u_pcb->free_buffers, partid);
            }

            /* Alloc count in PCB doesn't match what was found. */
            if (found_alloc != (u_pcb->nb_blocks - u_pcb->free_buffers))
            {
                found_error++;
                printf("    The number of allocated blocks found (%d) was \
different than the number in the PCB (%d) of partition %d.\n",
found_alloc, (u_pcb->nb_blocks - u_pcb->free_buffers), partid);
            }
        }

        /* Walk through free list making sure it is consistent. */
        printf("Checking free list for partition %d.\n", partid);

        nfree = 0,
        k_prev = NULL,
        k_freep = u_pcb->first_free_block;

        while ((nfree < maxdepth) && (k_freep != NULL))
        {
            /* Check to see if the next free list pointer is either out of the
             * range of this partition or if it is not on a block boundary.
             */
            if (   ( k_freep < u_pcb->part_start_add                 )
                || ( k_freep >= u_pcb->part_end_add                  )
                || ( (k_freep - u_pcb->part_start_add) % u_pcb->blocksize ) )
            {
                found_error++;
                freelisterror++;

                /* If k_prev is NULL, the free list header in the PCB is
                 * corrupted.
                 */
                if (k_prev == NULL)
                    printf("    PCB free list header: ");
                else
                    printf("    Index %d (0x%08x):  ",
                            (k_prev - u_pcb->part_start_add)/u_pcb->blocksize,
                            K2U(su_uaddr,k_prev));

                printf("BADBLK ");

                /* Is k_freep on a block boundary? */
                if ((k_freep - u_pcb->part_start_add) % u_pcb->blocksize)
                    printf("nextptr=0x%08x (illegal ptr - not a block ptr)\n",
                            K2U(su_uaddr, k_freep));
                else /* else k_freep must be out of range */
                    printf("nextptr=0x%08x (ptr not in partition %d)\n",
                            K2U(su_uaddr, k_freep), partid);

                break; /* break out of while loop */
            }
            else
            {
                /* Get next link in free list. */
                k_prev = k_freep;

                /* Turn k_freep into a pointer that can be used by chkpart,
                 * then skip the free hdr, then dereference to get the next
                 * k_freep pointer in the free list.
                 */
                k_freep = *( (void **) (K2U(ptrs->shared,k_freep) +
                                          sizeof(SU_FREE_MEM_HDR) )  );
            }
            nfree++;
        }

        /* If the above loop didn't follow the free list all the way to the
         * end, inform the user.
         */
        if ((nfree == maxdepth) && (k_freep != NULL))
            printf("Didn't follow free list to end. Stopped after %d \
iterations.\n", maxdepth);

        if ((found_error == 0) && (freelisterror == 0))
            printf("No errors found in partition %d.\n", partid);
        else if (freelisterror == 0)
            printf("Partition %d free list looks consistent.\n", partid);
    }

    return 0;
}
