// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the hash utilites for SUAPI debug services.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static int su_hash_get_index(SU_HASH_HANDLE hash_h, unsigned int key);
static void su_hash_add_tree_node(SU_HASH_NODE ** add_to,
                                  SU_HASH_NODE * new_node);
static void su_hash_run_function(SU_HASH_HANDLE hash_h,
                                 SU_HASH_NODE * n,
                                 SU_HASH_TRAVERSE_FNC func,
                                 unsigned int arg);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/
/* su_hash_create(): Given a size, this function will allocate space for the
 * hash table and initialize the hash structures.
 */
SU_HASH_HANDLE su_hash_create(int hash_size)
{
    int i;
    SU_HASH * hash;

    /* Allocate a hash structure. The last field in the hash structure is the
     * array of pointers to hash nodes. That is why hash_size times
     * sizeof(SU_HASH_NODE *) is added to the size of a SU_HASH structure.
     *
     * Each pointer in hash_array is the first link in the chain of nodes to
     * accommodate any key resolution conflicts.
     */
    hash = malloc(sizeof(SU_HASH) + (sizeof(SU_HASH_NODE *) * hash_size));

    if (hash == NULL)
        return NULL;

    memset(hash,0,sizeof(SU_HASH) + (sizeof(SU_HASH_NODE *) * hash_size));
    hash->size = hash_size;

    return (SU_HASH_HANDLE) hash;
}

/* su_hash_delete() removes the hash table that was created with
 * su_hash_create().
 */
void su_hash_delete(SU_HASH_HANDLE hash_h)
{
    SU_HASH * hash = (SU_HASH *)hash_h;
    SU_HASH_NODE *node;
    SU_HASH_NODE *temp_node;
    int i, hash_size;

    if (hash == NULL || hash->size == HASHMAGIC)
        return;

    hash_size = hash->size;
    hash->size = HASHMAGIC;

    for (i = 0; i < hash_size; i++)
        for (node = hash->hash_array[i]; node; node = temp_node)
        {
            temp_node = node->next;
            free(node);
        }

    free(hash);
}

/* su_hash_add_tree_node() is called from su_hash_add() only if the node
 * does not already exist in the tree.
 */
static void su_hash_add_tree_node(SU_HASH_NODE ** add_to,
                                  SU_HASH_NODE * new_node)
{
    if (*add_to)
    {
        if (new_node->key < (*add_to)->key)
            su_hash_add_tree_node( &( (*add_to)->left ), new_node);
        else if (new_node->key > (*add_to)->key)
            su_hash_add_tree_node( &( (*add_to)->right ), new_node);
        /* If node already exists, do nothing. */
    }
    else
    {
        *add_to = new_node;
    }
}

/* su_hash_get_index(): Given a key, return the index into the hash array.
 *    This function is internal to the hash services.  It assumes that hash_h
 *    has already been checked and is valid.
 */
static int su_hash_get_index(SU_HASH_HANDLE hash_h, unsigned int key)
{
    SU_HASH * hash = (SU_HASH *)hash_h;

    return (int) (key % hash->size);
}

/* su_hash_add(), add the given key value pair to the hash array.
 * RETURN VALUES:
 *     SU_HASH_INVALID_HANDLE: invalid hash handle
 *     SU_HASH_NO_MEM: insufficient memory
 */
int su_hash_add(SU_HASH_HANDLE hash_h, unsigned int key, unsigned int value)
{
    SU_HASH * hash = (SU_HASH *)hash_h;
    int i;
    SU_HASH_NODE *node;

    if (hash == NULL || hash->size == HASHMAGIC)
        return SU_HASH_INVALID_HANDLE;

    i = su_hash_get_index(hash_h, key);

    /* If the key is already in the conflict chain just change its value. */
    for (node = hash->hash_array[i]; node; node = node->next)
    {
        if (node->key == key)
        {
            node->value = value;
            return 0;
        }
    }

    /* If control reaches here, the key was not already in the chain. Create
     * a new entry and add it at the beginning of this hash entry's conflict
     * chain.
     */
    node = malloc(sizeof(SU_HASH_NODE));
    if (node == NULL)
        return SU_HASH_NO_MEM;
    node->key = key;
    node->value = value;
    node->right = node->left = NULL;
    node->next = hash->hash_array[i];
    hash->hash_array[i] = node;

    /* Now add the new node to the sorted list of keys. */
    su_hash_add_tree_node(&(hash->root),node);

    return 0;
}

/* su_hash_remove(): Given a key, remove the node from the hash table.
 * RETURN VALUES:
 *     SU_HASH_INVALID_HANDLE: invalid hash handle
 *     SU_HASH_INVALID_KEY: invalid key
 */
int su_hash_remove(SU_HASH_HANDLE hash_h, unsigned int key)
{
    SU_HASH * hash = (SU_HASH *)hash_h;
    int i;
    SU_HASH_NODE *node, *prev;

    if (hash == NULL || hash->size == HASHMAGIC)
        return SU_HASH_INVALID_HANDLE;

    i = su_hash_get_index(hash_h, key);

    for (prev = NULL, node = hash->hash_array[i];
         node;
         prev = node, node = node->next)
    {
        if (node->key == key)
        {
            /* This added check would not have been necessary if the
             * SU_HASH_NODE structure had both a prev and next pointer,
             * but the cost would have been the added pointer just for
             * the case of removal.
             */
            if (prev)
                prev->next = node->next;
            else
                /* When the last item is removed, hash->hash_array[i] will be
                 * set back to NULL.
                 */
                hash->hash_array[i] = node->next;
            free(node);
            return 0;
        }
    }

    /* invalid key */
    return SU_HASH_INVALID_KEY;
}

/* su_hash_get_value(): Given the specified key, retrieve the associated value.
 * RETURN VALUES:
 *     SU_HASH_INVALID_HANDLE: invalid hash handle
 *     SU_HASH_INVALID_KEY: invalid key
 *     SU_HASH_NO_MEM: value points to invalid memory
 */
int su_hash_get_value(SU_HASH_HANDLE hash_h,
                      unsigned int key,
                      unsigned int * value)
{
    SU_HASH * hash = (SU_HASH *)hash_h;
    int i;
    SU_HASH_NODE *node;

    if (hash == NULL || hash->size == HASHMAGIC)
        return SU_HASH_INVALID_HANDLE;

    if (value == NULL)
        return SU_HASH_NO_MEM;

    i = su_hash_get_index(hash_h, key);

    for (node = hash->hash_array[i]; node; node = node->next)
    {
        if(node->key == key)
        {
            *value = node->value;
            return 0;
        }
    }

    /* invalid key */
    return SU_HASH_INVALID_KEY;
}

/* su_hash_increment_value(): increments value in key entry by the amount
 * specified by incr. If entry does not yet exist, su_hash_increment_value()
 * will create it and initialize the entry to incr.
 *
 * RETURN VALUES:
 *     SU_HASH_INVALID_HANDLE: invalid hash handle
 *     SU_HASH_NO_MEM: insufficient memory
 */
int
su_hash_increment_value(SU_HASH_HANDLE hash_h,
                        unsigned int key,
                        unsigned int incr)
{
    SU_HASH * hash = (SU_HASH *)hash_h;
    unsigned int v;
    int ret;

    if (hash == NULL || hash->size == HASHMAGIC)
        return SU_HASH_INVALID_HANDLE;

    ret = su_hash_get_value(hash_h, key, &v);

    /* If su_hash_get_value() was successful, increment the value. */
    if (ret == 0)
        v += incr;
    else
        v = incr;

    return su_hash_add(hash_h, key, v);
}

/*
 * su_hash_run_function() is internal to the hash services and recursively
 * called by the su_hash_get_all_keys() function to run the user-specified
 * function on each hash node.
 */
static void su_hash_run_function(SU_HASH_HANDLE hash_h,
                                 SU_HASH_NODE * n,
                                 SU_HASH_TRAVERSE_FNC func,
                                 unsigned int arg)
{
    if (n)
    {
        su_hash_run_function(hash_h, n->left, func, arg);
        func(hash_h, n->key,arg);
        su_hash_run_function(hash_h, n->right, func, arg);
    }
    return;
}

/* su_hash_traverse_all_keys() traverses the hash from lowest key to highest and
 * runs the specified function on each node.
 * RETURN VALUES:
 *     SU_HASH_INVALID_HANDLE: invalid hash handle
 */
int su_hash_traverse_all_keys(SU_HASH_HANDLE hash_h,
                              SU_HASH_TRAVERSE_FNC func,
                              unsigned int arg)
{
    SU_HASH * hash = (SU_HASH *)hash_h;

    if (hash == NULL || hash->size == HASHMAGIC)
        return SU_HASH_INVALID_HANDLE;

    su_hash_run_function(hash_h, hash->root, func, arg);

    return 0;
}
