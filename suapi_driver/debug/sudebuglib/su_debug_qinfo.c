// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the su_debug_mesg() and su_debug_mesgdata() SUAPI
 *      debug library functions which print message information for the
 *      specified SUAPI messages.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/

/* cvt_only_if_in_shared:
 *   If the specified address is a kernel address in the range of the SUAPI
 *   shared memory region, this function converts it to a user space address
 *   in the range that was active at the time of the panic dump.
 *
 * INPUTS:
 *   ptrs: List of pointers into private and shared SUAPI memory. These
 *         pointers are in the address range of the calling process.
 *   addr: Address to be converted.
 *
 * RETURN VALUES:
 *     If addr is a kernel address in the range of the SUAPI shared memory
 *     region, this function returns the corresponding user space address
 *     in the range that was active at the time of the panic dump.
 *     Otherwise, this function returns addr.
 */
void * cvt_only_if_in_shared(SU_REGION_PTRS * ptrs, void * addr)
{
    SU_PRIVATE_DATA * privatep;
    privatep = (SU_PRIVATE_DATA *) ptrs->private;
    if (addr >= privatep->kshared_p
        && addr < privatep->kshared_p + privatep->shared_size)
        return K2U(((SU_GLOBAL_DATA *) ptrs->shared)->user_shmaddress, addr);
    else
        return addr;
}

/* suapi_debug_mesgdata() prints the data portion of the specified message.
 *
 * INPUTS:
 *   ptrs:    List of pointers into private and shared SUAPI memory. These
 *            pointers are in the address range of the calling process.
 *   msghdrp: Pointer to an SU_MSG_HDR structure which must be printed. This
 *            address is in the range of the calling process.
 *   limit:   Total amount of data to be printed.
 *
 * RETURN VALUES:
 *     0 on success
 */
int suapi_debug_mesgdata(SU_REGION_PTRS * ptrs, SU_MSG_HDR * msghdrp, unsigned int limit)
{
    SU_GLOBAL_DATA * globalp = (SU_GLOBAL_DATA *) ptrs->shared;
    void * shaddr = globalp->user_shmaddress;
    void * p;
    unsigned char c, s[17];
    void * beginmsgdata = (void *) (msghdrp+1);
    void * endmsgdata = beginmsgdata + msghdrp->length;
    int i;

    printf("    Message Data:\n");

    memset(s,0,17);

    /* This loop prints each line of the message data in the following format:
     *    0xAAAAAAAA:  XXXXXXXX  XXXXXXXX  XXXXXXXX  XXXXXXXX  CCCCCCCCCCCCCCCC
     *
     * Where AAAAAAAA - represents the user space address of the message data
     *       XXXXXXXX - represents 16 bytes of data in hex format in 4 sets of
     *                  4-bytes each
     *       CCCCC... - represents 16 bytes of data in ascii format 
     */
    for (i = 0, p = beginmsgdata; p < endmsgdata && limit > 0;p++, i++, limit--)
    {
        /* If at beginning of a line, print the address. */
        if (i == 0)
            printf("    0x%08X:", CVT(p, globalp, shaddr));

        /* Break up the hex data into 4-byte chunks. */
        if ((i%4) == 0)
            printf("  ");

        /* Print each byte of the data in hex. */
        c = *((unsigned char *)p);
        printf("%02X", c);

        /* If alphanumeric or punctuation, store byte in ascii. */
        if (isalnum(c) || ispunct(c))
            s[i] = c;
        /* If space or tab, store a space. */
        else if (isblank(c))
            s[i] = ' ';
        /* Otherwise, store byte as a period. */
        else
            s[i] = '.';

        /* If at end of line, print the ascii representation of the data. */
        if (i == 15)
        {
            i = -1;
            printf("    %s\n",s);
            memset(s,0,17);
        }
    }


    /* If the data did not end exactly after 16 bytes, pad the rest before
     * printing the ascii output. The following "if" accounts for the case
     * where the end of line was already reached in the above loop and the
     * counter rolled over.
     */
    if (i != 0)
    {
        while (i < 16)
        {
            /* Pad remainder of hex output: 2 spaces for each byte. */
            printf("  ");
            /* Also, must add padding spaces just like in the above loop. */
            if ((i%4) == 0)
                printf("  ");
            i++;
        }
        /* Now print the rest of the ascii data at the end of the line. */
        printf("    %s\n",s);
    }

    return 0;
}

/* suapi_debug_mesg() prints the message header of the specified message.
 *
 * INPUTS:
 *   ptrs:    List of pointers into private and shared SUAPI memory. These
 *            pointers are in the address range of the calling process.
 *   msghdrp: Pointer to an SU_MSG_HDR structure which must be printed. This
 *            address is in the range of the calling process.
 *   limit:   Total amount of data to be printed.
 *
 * RETURN VALUES:
 *     0 on success
 */
int suapi_debug_mesg(SU_REGION_PTRS * ptrs, SU_MSG_HDR * msghdrp, unsigned int limit)
{
    SU_GLOBAL_DATA * globalp = (SU_GLOBAL_DATA *) ptrs->shared;
    SU_PRIVATE_DATA * privatep = (SU_PRIVATE_DATA *) ptrs->private;
    void * shaddr = globalp->user_shmaddress;
    void * next = (void *) msghdrp->su_minnode.su_n_next;
    void * prev = (void *) msghdrp->su_minnode.su_n_prev;
    void * mhdruser;

    /* msghdrp is pointing into an address in the range of the current process.
     * Convert it to the SUAPI user space address for display purposes.
     */
    mhdruser = CVT(msghdrp, globalp, shaddr);

    /* "next" and "prev" are kernel addresses. If they are in the shared memory
     * range, convert them to shared memory addresses for display purposes.
     * Otherwise, leave them alone so the caller can see if they are pointing
     * to the queue header or if they are corrupted.
     */
    next = cvt_only_if_in_shared(ptrs, next);
    prev = cvt_only_if_in_shared(ptrs, prev);

    printf("\n\
Message: (0x%08X)\n\
    next:       0x%08X    destport:      0x%08X\n\
    prev:       0x%08X    replyport:     0x%08X\n\
    type:       0x%08X    priority:               %d\n\
    length:           %4d    reply_logging:          %d\n",
            mhdruser,
            next, msghdrp->destport,
            prev, msghdrp->replyport,
            msghdrp->type, msghdrp->priority,
            msghdrp->length, msghdrp->reply_logging);

    suapi_debug_mesgdata(ptrs, msghdrp, limit);

    return 0;
}
