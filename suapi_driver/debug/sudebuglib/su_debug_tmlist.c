// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the tmlist SUAPI debug library function.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>
#include <linux/su_ktimers.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/

/* This service assumes parameters are valid.
 *      ptrs: list of pointers into private and shared SUAPI memory.
 *
 * RETURN VALUES:
 *     0 on success
 */
int suapi_debug_tmlist(SU_REGION_PTRS * ptrs)
{
    SU_PRIVATE_DATA * supriv_p = (SU_PRIVATE_DATA *) ptrs->private;
    void * timer_p;
    unsigned int i, numallocated = 0;
    SU_SYSCONFIG * su_sysconfig_p = (SU_SYSCONFIG *) ptrs->sysconfig;
    void * start_timer_array = ptrs->timers;
    void * end_timer_array = ptrs->semarray;

    printf("\n\
ID  Alarm      Period     Slip     Type Task/Port  Evmask/Msg\n\
=== ========== ========== ======== ==== ========== ==========\n");

    /* 
     * Because of the variable nature of the Suapi_Kernel_Timer
     * structure, the timers array must be traversed by adding the size of the
     * timer structure to the pointer rather than by dereferencing the array
     * using an index.
     */
    for (timer_p = start_timer_array;
         timer_p < end_timer_array;
         timer_p += supriv_p->timersz)
    {
        struct Suapi_Kernel_Timer timer_st;

        memset(&timer_st, 0, sizeof(struct Suapi_Kernel_Timer));

        memcpy(&timer_st,
               timer_p,
               sizeof(struct Suapi_Kernel_Timer));

        /* Only print data for running timers. */
        if (timer_st.su_timer_user.state & TSTARTED)
        {
            /* Mask off deletion count from full_id. */
            printf("%3d ", timer_st.su_timer_user.full_id&0xFFFF);
            printf("0x%08X ", timer_st.ltimer.TIMEOUT_FIELD);
            printf("0x%08X ", timer_st.su_timer_user.period);
            printf("0x%08X ", timer_st.su_timer_user.slip);
            printf("%4.4s ",
                    (timer_st.su_timer_user.state & TMESSAGE)?"MESG":"EVNT");
            printf("0x%08X ", timer_st.su_timer_user.data.event.pid);
            printf("0x%08X\n", timer_st.su_timer_user.data.event.mask);
        }
    }

    return 0;
}
