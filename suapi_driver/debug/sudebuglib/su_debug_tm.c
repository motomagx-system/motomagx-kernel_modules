// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the tm SUAPI debug library function.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-11-12 Motorola       Switch to RTC stopwatch timer.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>
#include <linux/su_ktimers.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/
#define EVENT_TIMER_FORMAT \
"    timer_type:  SU_EVENT_TIMER\n\
    task:        0x%08X\n\
    evmask:      0x%08X\n\
    started:     %d\n\
    cycle:       %d\n\
    alarm:       0x%08X\n\
    slip:        0x%08X\n"

#define MESSAGE_TIMER_FORMAT \
"    timer_type:  SU_MESSAGE_TIMER\n\
    port:        0x%08X\n\
    msgptr:      0x%08X\n\
    started:     %d\n\
    cycle:       %d\n\
    alarm:       0x%08X\n\
    slip:        0x%08X\n"

#define STOPPED_TIMER_FORMAT \
"    Timer type:  STOPPED\n\
    field1:      0x%08X\n\
    field2:      0x%08X\n\
    started:     %d\n\
    cycle:       %d\n\
    alarm:       0x%08X\n\
    slip:        0x%08X\n"

#define UNALLOCATED_TIMER_FORMAT \
"    =============================================\n\
    NOT ALLOCATED, but here's the data anyway....\n\
    =============================================\n\
    Timer type:  NOT ALLOCATED\n\
    field1:      0x%08X\n\
    field2:      0x%08X\n\
    started:     %d\n\
    cycle:       %d\n\
    alarm:       0x%08X\n\
    slip:        0x%08X\n"

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/

/* This service assumes parameters are valid.
 *      ptrs: list of pointers into private and shared SUAPI memory.
 *      tmindex: index into timer array of timer to be displayed.
 *
 * RETURN VALUES:
 *     0 on success
 */
int suapi_debug_tm(SU_REGION_PTRS * ptrs, int tmindex)
{
    SU_PRIVATE_DATA * supriv_p = (SU_PRIVATE_DATA *) ptrs->private;
    SU_SYSCONFIG * su_sysconfig_p = (SU_SYSCONFIG *) ptrs->sysconfig;
    void * start_timer_array = ptrs->timers;
    void * end_timer_array = ptrs->semarray;

    /* SUAPI kernel timer pointer */
    struct Suapi_Kernel_Timer *skt_p;
    char * format;

    skt_p = (struct Suapi_Kernel_Timer *)
                (start_timer_array + (tmindex * supriv_p->timersz));

    printf("\nNumber of timers: %d\n", su_sysconfig_p->ntimers);

    printf("Timer #%d:\n", tmindex);

    if (skt_p->su_timer_user.state == TFREE)
        format = UNALLOCATED_TIMER_FORMAT;
    else if (skt_p->su_timer_user.state & TEVENT)
        format = EVENT_TIMER_FORMAT;
    else if (skt_p->su_timer_user.state & TMESSAGE)
        format = MESSAGE_TIMER_FORMAT;
    else
        format = STOPPED_TIMER_FORMAT;

    printf(format, 
       skt_p->su_timer_user.data.event.pid,
       skt_p->su_timer_user.data.event.mask,
       ((skt_p->su_timer_user.state & TSTARTED) != 0),
       (skt_p->su_timer_user.state & TCYCLIC) ? skt_p->su_timer_user.period : 0,
       skt_p->ltimer.TIMEOUT_FIELD,
       skt_p->su_timer_user.slip);

    return 0;
}
