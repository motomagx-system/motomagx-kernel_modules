// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the tminfo SUAPI debug library function.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.
// 2006-12-14 Motorola       Switch to RTC timer. 

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>
#include <linux/su_ktimers.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/

/* This service assumes parameters are valid.
 *      ptrs: list of pointers into private and shared SUAPI memory.
 *      a_flag: indicates whether unallocated timers should be displayed.
 *
 * RETURN VALUES:
 *     0 on success
 */
int suapi_debug_tminfo(SU_REGION_PTRS * ptrs, int a_flag)
{
    SU_PRIVATE_DATA * supriv_p = (SU_PRIVATE_DATA *) ptrs->private;
    SU_SYSCONFIG * su_sysconfig_p = (SU_SYSCONFIG *) ptrs->sysconfig;
    void * start_timer_array = ptrs->timers;
    void * end_timer_array = ptrs->semarray;
    void * timer_p;
    unsigned int i, numallocated = 0;

    /* SUAPI kernel timer pointer */
    struct Suapi_Kernel_Timer *skt_p;

    /* 
     * Because of the variable nature of the Suapi_Kernel_Timer
     * structure, the timers array must be traversed by adding the size of the
     * timer structure to the pointer rather than by dereferencing the array
     * using an index.
     */
    for (timer_p = start_timer_array;
         timer_p < end_timer_array;
         timer_p += supriv_p->timersz)
    {
        skt_p = (struct Suapi_Kernel_Timer *) timer_p;
        if (skt_p->su_timer_user.state != TFREE) numallocated++;
    }

    printf("\nNumber of timers: %d\n", su_sysconfig_p->ntimers);
    printf("Current number of timers allocated: %d\n", numallocated);

    printf("\
Timer       TimerType                Task/Port   Evmask/Msg\n\
==========  =======================  ==========  ==========\n");

    for (i = 0, timer_p = start_timer_array;
         timer_p < end_timer_array;
         timer_p += supriv_p->timersz, i++)
    {
        skt_p = (struct Suapi_Kernel_Timer *) timer_p;

        if (skt_p->su_timer_user.state == TFREE)
        {
            if (a_flag)
                printf("Timer%5d  NOT ALLOCATED\n", i);
        }
        else if (skt_p->su_timer_user.state & TEVENT)
            printf("Timer%5d  SU_EVENT_TIMER          0x%08X  0x%08X\n",
                    i,
                    skt_p->su_timer_user.data.event.pid,
                    skt_p->su_timer_user.data.event.mask);
        else if (skt_p->su_timer_user.state & TMESSAGE)
            printf("Timer%5d  SU_MESSAGE_TIMER        0x%08X  0x%08X\n",
                    i,
                    skt_p->su_timer_user.data.message.ph,
                    skt_p->su_timer_user.data.message.msg);
        else
            printf("Timer%5d  STOPPED                 0x00000000  0x00000000\n",
                    i);
    }

    return 0;
}
