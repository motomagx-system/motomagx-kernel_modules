// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the data structures for the hash utilites for
 *      SUAPI debug services.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** GLOBAL CONSTANTS **********************************************/

/* HASHMAGIC in the size field of a SU_HASH structure indicates a hash handle is
 * no longer in use.
 */
#define HASHMAGIC 0xD00FBA11

/* SU_HASH function return values. */
#define SU_HASH_INVALID_HANDLE (-1)
#define SU_HASH_INVALID_KEY    (-2)
#define SU_HASH_NO_MEM         (-3)

/************** GLOBAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
/* SU_HASH_NODE is a structure that contains a key/value pair and a pointer to
 * an SU_HASH_NODE structure to account for any conflicts with the hash entry.
 * This structure also contains left and right pointers so that the hash
 * nodes can be sorted into a binary tree based on key value.
 * The sorted tree is written when a new key is added to the hash. The sorted
 * tree is read only when the entire tree must be accessed at once in sorted
 * order.
 */
typedef struct SU_HASH_NODE
{
    unsigned int key;
    unsigned int value;
    struct SU_HASH_NODE *next;
    struct SU_HASH_NODE *left;
    struct SU_HASH_NODE *right;
} SU_HASH_NODE;

/* SU_HASH contains the hash size and the hash table.
 *     The size field contains the size of the hash array.
 *     The root field is the root of a binary tree which contains the sorted
 *         list of keys. It is updated whenever a new node is added to the hash
 *         but reading of individual key nodes never searches the binary tree.
 *         For that, the hash_array is used only when the entire tree must be
 *         accessed in ascending order of keys is this tree read.
 *     The hash_array is an array of pointers to SU_HASH_NODE. Based on the key,
 *         entries are hashed into and read from this array.
 * Space will be allocated for the hash_array field as part of hash
 * initialization.
 */
typedef struct SU_HASH
{
    int size;
    SU_HASH_NODE * root;         /* Binary tree of nodes sorted by key. */
    SU_HASH_NODE * hash_array[0];/* array of SU_HASH_NODE ptrs for hash list */
} SU_HASH;

/* SU_HASH_HANDLE is a handle that is used by the caller when accessing hash
 * functions.
 */
typedef struct SU_HASH * SU_HASH_HANDLE;

/* SU_HASH_TRAVERSE_FNC is the prototype used by hash_traverse_all_keys() to
 * define the function parameter which is passed to it and which it will
 * run on each node in the hash.
 */
typedef void SU_HASH_TRAVERSE_FNC(SU_HASH_HANDLE hash_h,
                                  unsigned int key,
                                  unsigned int arg);

/************** GLOBAL FUNCTION PROTOTYPES ************************************/
SU_HASH_HANDLE su_hash_create(int hash_size);
void su_hash_delete(SU_HASH_HANDLE hash_h);

int su_hash_add(SU_HASH_HANDLE hash_h, unsigned int key, unsigned int value);
int su_hash_remove(SU_HASH_HANDLE hash_h, unsigned int key);
int su_hash_get_value(SU_HASH_HANDLE hash_h,
                      unsigned int key,
                      unsigned int * value);

int su_hash_increment_value(SU_HASH_HANDLE hash_h,
                            unsigned int key,
                            unsigned int incr);
int su_hash_traverse_all_keys(SU_HASH_HANDLE hash_h,
                              SU_HASH_TRAVERSE_FNC func,
                              unsigned int arg);

/************** GLOBAL MACROS *************************************************/

/************** GLOBAL VARIABLES **********************************************/
