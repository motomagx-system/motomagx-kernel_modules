// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements functions useful to all Linux debug utilities.
 *
 *  IMPORTANT NOTEs:
 *     Don't link the functions in this file with an executable that is also
 *     linked with the SUAPI library.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>

/************** LOCAL CONSTANTS **********************************************/
/* Each datalogger msg header begins with B5C followed by two bits of location
 * from which the dump came followed by two bits of type. The AP panic dump
 * location is 01 (binary) and we don't care about the type. By &-ing off
 * the lower 2 bits using the LOGGER_SYNC_MASK, a specified sync pattern can
 * be checked for validity.
 */
#define LOGGER_SYNC 0xB5C4
#define LOGGER_SYNC_MASK 0xFFFC

/* These are the logger message magic numbers that mark the beginning of a
 * message section. They are converted from big endian to little endian.
 */
#define LOGGER_PRIVATE_MAGIC 0x01620a00 /* In big endian: 0x000a6201 */
#define LOGGER_SHARED_MAGIC  0x02620a00 /* In big endian: 0x000a6202 */
#define LOGGER_PROCTBL_MAGIC 0x03620a00 /* In big endian: 0x000a6203 */

#define LOGGER_MSG_SIZE 0xFFFF

#define PROCTABLE_HASH_SIZE 50

#define CMDFILELENGTH 32  /* max length of "/proc/<pid>/cmdline" */
#define CMDLINELENGTH 64  /* max # of bytes to read from /proc/<pid>/cmdline" */

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
/* The panic dump message structure appears at the beginning of each
 * datalogger message. The following structure must match the SuapiLogCommonHdr
 * structure defined in klogger.c. The sync pattern is combined into a short
 * here instead of two chars like it is in the kernel so that byte swapping can
 * be done on it before checking.
 */
struct logger_msg_hdr_st {
    unsigned short sync;     /* sync pattern or-ed with location and type */
    unsigned short size;     /* size of message section */
    unsigned int timestamp;  /* time stamp */
    unsigned int msg_id;     /* magic number that marks the type of message */
};

typedef enum {
    LOGMSG_BEGIN,
    LOGMSG_PRIVATE = LOGMSG_BEGIN,
    LOGMSG_SHARED,
    LOGMSG_PROCTABLE,
    LOGMSG_END,
} numlogmsgs_t;

/* logger_messages_st: A panic dump file contains segments of data up to
 *      65535 bytes each. Each segment is marked by a magic number. If the data
 *      is larger than 65535 bytes, more segments marked by the same magic
 *      number are included until all the data of that type is included in the
 *      panic dump. This structure is used by su_map_files() to collect the SUAPI
 *      private, shared, and process list segments into contiguous memory.
 *      
 */
struct logger_messages_st {
    char * name;        /* string containing the name of the logger segment */
    unsigned int magic; /* magic number identifies a specific message segment */
    void * location;    /* pointer to beginning of collected segments */
    unsigned short size;/* size of data in a collected segment */
};

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void * collect_logger_messages(int fd, unsigned int rqst_msgid,
                                unsigned short *tot_data_size, int *err);

/************** LOCAL MACROS *************************************************/
/* This macro checks if the sync pattern is between 0xB5C4-0xB5C7, which
 * indicates that this is a logger message and that it came from the AP panic
 * dump.
 */
#define DATALOGGER_SYNC_PATTERN(sp) \
        ( ((sp) & LOGGER_SYNC_MASK) == LOGGER_SYNC)

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/

/* collect_logger_messages():
 * DESCRIPTION:
 *     This function collects all of the segments of the specified datalogger
 *     message ID into one contiguous memory region and returns a pointer to
 *     it.
 *
 * INPUTS:
 *     fd: open file descriptor of a datalogger panic dump
 *     rqst_msgid: logger message ID which marks the segments to be collected
 *
 * OUTPUTS:
 *     *tot_data_size: size of region returned by this function
 *     *err: set to 0 on success or an error code otherwise
 *
 * RETURNS:
 *     If this function can find all segments of the specified datalogger
 *     message, it allocates space and returns a pointer to the collected
 *     segments. On failure, this function sets the err parameter and returns
 *     NULL.
 *
 * IMPORTANT NOTEs:
 *     The panic dump file is made up of several (possibly unrelated)
 *     datalogger message segments. Each segment starts with a special sync
 *     pattern, which marks the beginning of next segment. Each segment
 *     begins with a datalogger message header.
 *
 *     The datalogger splits up the message segments into 65535-byte chunks,
 *     so they must be collected, stripped of their headers, and put back
 *     together so they can be accessed as one contiguous memory region.
 *
 *     This function allocates space for the specified memory region.
 *
 * ASSUMPTIONS:
 *     The segments marked by message IDs of the same type are in ascending
 *     order.
 *     If this function encounters one with size less than 65535-bytes, then it
 *     is done with the collection process.
 *     *err and *msg_size are pointing to valid memory.
 */
static void * collect_logger_messages(int fd, unsigned int rqst_msgid,
                                unsigned short *tot_data_size, int *err)
{
    void * region = NULL;
    int ret;
    struct logger_msg_hdr_st logger_msg_hdr;
    unsigned short sync;
    unsigned char sync_byte_1, sync_byte_2;
    unsigned short msg_size;

    *tot_data_size = 0;
    *err = 0;

    /* Make sure fd is at the beginning of the file. */
    if (lseek(fd, (off_t) 0, SEEK_SET) < 0)
    {
        *err = SU_DEBUG_FAILURE_FILE_SEEK;
        return NULL;
    }

    while ((ret=read(fd,&logger_msg_hdr,sizeof(struct logger_msg_hdr_st))) > 0)
    {
        /* Since the datalogger message header is in big endian format, the
         * sync pattern must be converted to little endian.
         */
        swab(&logger_msg_hdr.sync, &sync, sizeof(unsigned short));

        /* If the sync pattern is not at the beginning of the logger message
         * keep looking byte-by-byte until either the sync pattern is found
         * or the end of the file is found.
         */
        if ( ! DATALOGGER_SYNC_PATTERN(sync) )
        {
            /* Core file may be munged! See if we can sync back up. */

            /* Start looking again one byte after the previous read. */
            ret =
              lseek(fd,(off_t) -(sizeof(struct logger_msg_hdr_st)-1), SEEK_CUR);
            if (ret < 0)
            {
                if (region)
                    free(region);
                *err = SU_DEBUG_FAILURE_FILE_SEEK;
                return NULL;
            }
            continue;
        }

        /* Since the datalogger message header is in big endian format, the
         * size of the message section must be converted to little endian.
         */
        swab(&logger_msg_hdr.size, &msg_size, sizeof(unsigned short));

        /* Size of message segment includes message header, which has already
         * been read from fd, so subtract that out from the msg_size to get the
         * size of the data portion of the message segment.
         */
        msg_size -= sizeof(struct logger_msg_hdr_st);

        if (logger_msg_hdr.msg_id == rqst_msgid)
        {
            void * tmp;
            tmp = realloc(region, *tot_data_size + msg_size);
            if (tmp == NULL)
            {
                if (region)
                    free(region);
                *err = SU_DEBUG_FAILURE_NO_MEM;
                return NULL;
            }
            region = tmp;

            /* Concatenate the data portion of the current message section onto
             * the end of the region.
             */
            if (read(fd, (region + *tot_data_size), msg_size) < 0)
            {
                if (region)
                    free(region);
                *err = SU_DEBUG_FAILURE_FILE_READ;
                return NULL;
            }

            *tot_data_size += msg_size;

            /* If the size of the current message is less then LOGGER_MSG_SIZE,
             * then we have found all of the segments that match this message
             * ID.
             */
            if ((msg_size + sizeof(struct logger_msg_hdr_st)) < LOGGER_MSG_SIZE)
                return region;
        }
        else
        {
            /* This is not the requested message ID, so skip past it to
             * the next message.
             */
            if (lseek(fd, (off_t) msg_size, SEEK_CUR) < 0)
            {
                if (region)
                    free(region);
                *err = SU_DEBUG_FAILURE_FILE_SEEK;
                return NULL;
            }
        }
    }

    if (ret < 0)
    {
        if (region)
            free(region);
        *err = SU_DEBUG_FAILURE_FILE_READ;
        return NULL;
    }

    return region;
}


/* free_names():
 * DESCRIPTION:
 *     This function is called by the cleanup routine to clean up each node
 *     that was allocated to hold names of the processes in the process table.
 *
 * INPUTS:
 *     hash_h: handle to the hash table containing the process table
 *     key: the process ID which is used as a key to the hash
 *     arg: unused, but must exist to conform to format of hash traversal tool
 *
 * OUTPUTS:
 *     none
 *
 * RETURNS:
 *     none
 */
static void free_names (SU_HASH_HANDLE hash_h,unsigned int key,unsigned int arg)
{
    char * pname;
    if (su_hash_get_value(hash_h, key, (unsigned int *)&pname) < 0)
        return;
    free(pname);
}

/* su_map_cleanup():
 * DESCRIPTION:
 *     This function cleans up the pointers, memory maps,  and hashes that were
 *     allocated during su_map_files().
 *
 * INPUTS:
 *     ptrs: list of pointers into private and shared SUAPI memory.
 *
 * OUTPUTS:
 *     none
 *
 * RETURNS:
 *     none
 */
void su_map_cleanup(SU_REGION_PTRS * ptrs)
{
    if (ptrs->core_file)
    {
        if (ptrs->private)
            free(ptrs->private);
        if (ptrs->shared)
            free(ptrs->shared);
    }
    else
    {
        if (ptrs->private)
        {
            if (ptrs->shared)
                munmap(ptrs->shared,
                   ((SU_PRIVATE_DATA *)ptrs->private)->shared_size);
            munmap(ptrs->private,
                   ((SU_PRIVATE_DATA *)ptrs->private)->private_size);
        }
    }
    if(ptrs->prochash)
    {
        /* When called on a running system, name nodes must be freed. */
        if (!ptrs->core_file)
            su_hash_traverse_all_keys(ptrs->prochash, free_names, 0);
        su_hash_delete(ptrs->prochash);
    }
}

/* su_map_files():
 * DESCRIPTION:
 *     This function does the processing necessary for accessing the private
 *     and shared memory regions of SUAPI. When this function returns
 *     successfully, the necessary files will have been memory mapped and
 *     the ptrs structure will be filled in with pointers to the beginning of
 *     the various SUAPI data structures.
 *
 * INPUTS:
 *     suapi_file_name: Name of panic dump file to be processed. If this
 *                      argument is NULL, this function will open and memory
 *                      map /dev/suapi and /dev/suapipriv.
 *
 * OUTPUTS:
 *     ptrs: This pointer is assumed to point to valid memory. On success, this
 *           function fills in the structure with pointers to the various SUAPI
 *           data structures.
 *
 */
int su_map_files(char * suapi_file_name, SU_REGION_PTRS * ptrs)
{
    int pfd;
    SU_PRIVATE_DATA private_st;
    SU_SYSCONFIG * su_sysconfig_ptr;
    struct logger_messages_st logger_messages[] =
                            { {"private",  LOGGER_PRIVATE_MAGIC, NULL, 0},
                              {"shared",   LOGGER_SHARED_MAGIC, NULL, 0},
                              {"proctable",LOGGER_PROCTBL_MAGIC, NULL, 0}
                            };

    ptrs->private = NULL;
    ptrs->shared = NULL;
    ptrs->prochash = (SU_HASH_HANDLE) 0;

    if (suapi_file_name == NULL)
    {
        suapi_file_name = SUAPI_PRIVATE_DEVICE;
        ptrs->core_file = 0;
    }
    else
    {
        ptrs->core_file = 1;
    }

    pfd = open(suapi_file_name, O_RDONLY);
    if (pfd < 0)
    {
        fprintf(stderr, "unable to open ");
        perror(suapi_file_name);
        return SU_DEBUG_FAILURE_FILE_OPEN;
    }

    if (ptrs->core_file)
    {
        int i, err;
        char * p;

        /* Collect SUAPI memory regions into contiguous segments. */
        for (i = LOGMSG_BEGIN; i < LOGMSG_END; i++)
        {
            logger_messages[i].location =
                collect_logger_messages(pfd,
                                        logger_messages[i].magic,
                                        &(logger_messages[i].size),
                                        &err);
            /* If there is no proctable, continue anyway. */
            if (logger_messages[i].location == NULL && i != LOGMSG_PROCTABLE)
            {
                fprintf(stderr,"\nNo %s region found: ",logger_messages[i].name);
                switch(err)
                {
                    case SU_DEBUG_FAILURE_BAD_PARAMETER:
                        fprintf(stderr,
                                "Ill formatted panic dump file %s\n",
                                suapi_file_name);
                        break;
                    case SU_DEBUG_FAILURE_FILE_READ:
                    case SU_DEBUG_FAILURE_FILE_SEEK:
                        perror(suapi_file_name);
                        break;
                    case SU_DEBUG_FAILURE_NO_MEM:
                        fprintf(stderr,
                            "Could not allocate space for % region ",
                            logger_messages[i].name);
                        perror(suapi_file_name);
                        break;
                }
                close(pfd);
                return err;
            }
        }
        ptrs->private   = logger_messages[LOGMSG_PRIVATE].location;
        ptrs->shared    = logger_messages[LOGMSG_SHARED].location;
        p = (char *) logger_messages[LOGMSG_PROCTABLE].location;

        /* If the proctable exists and the first character is '/', then
         * this function expects that each entry in the proctable is a
         * null terminated string of the format "/<pid>/<executable>".
         */
        if (p && *p == '/')
        {
            ptrs->prochash = su_hash_create(PROCTABLE_HASH_SIZE);
            if (ptrs->prochash != (SU_HASH_HANDLE) 0)
            {
                char * ptmp;
                int nt = 0;
                char * procend = (char *)
                                    logger_messages[LOGMSG_PROCTABLE].location +
                                        logger_messages[LOGMSG_PROCTABLE].size;

                /* Determine if the entries in the proc table are
                 * NULL-terminated.
                 */
                ptmp = p;
                while (ptmp < procend && *ptmp != '\0') ptmp++;
                if (ptmp < procend)
                    nt = 1;
                while (p < procend)
                {
                    int pid, retval;
                    char * pname;

                    if (*p != '/')
                        break; /* Bad format, so get out. */
                    if (!nt)
                        *p = '\0'; /* to terminate previous proc name string */
                    p++;
                    pname = strchr(p,'/');
                    if (pname)
                    {
                        *pname = '\0'; /* NULL-terminate pid string */
                        pname++; /* skip to beginning of proc name string */
                    }
                    else
                        break; /* ill formatted string, just go on */

                    /* p now points to the beginning of the pid and pname to
                     * the beginning of the name of the process executable.
                     * Put the name in the hashtable keyed by the pid.
                     */
                    pid = strtoul(p,NULL,0);
                    retval = su_hash_add(ptrs->prochash,
                                         pid,
                                         (unsigned int) pname);
                    if (retval != 0)
                        break; /* Just continue without a full process table. */

                    /* Skip past pname. Each entry in the proctable in the panic
                     * dump file may or may not be NULL terminated.
                     */
                    if (nt)
                        p = pname + strlen(pname) + 1;
                    else
                    {
                        p = strchr(pname,'/');
                        if (p == NULL)
                        {
                            /* then this must be the last entry; terminate it */
                            *(procend - 1) = '\0';
                            break;
                        }
                    }
                }
            }
        }

        /* Fill in private data structure. */
        memcpy(&private_st, ptrs->private, sizeof(SU_PRIVATE_DATA));
        close(pfd);
    }
    else
    {
        int fd;
        DIR * dir;

        /* Read private data structure from beginning of private region. */
        if (read(pfd, &private_st, sizeof(SU_PRIVATE_DATA)) < 0)
        {
            fprintf(stderr, "unable to read private memory header from ");
            perror(suapi_file_name);
            close(pfd);
            return SU_DEBUG_FAILURE_FILE_READ;
        }

        /* Memory map the private region so that its data can be accessed
         * using pointer manipulation.
         */
        ptrs->private = mmap(0,
                             private_st.private_size,
                             PROT_READ,
                             MAP_SHARED,
                             pfd,
                             0);
        if (ptrs->private == MAP_FAILED)
        {
            fprintf(stderr, "unable to mmap private memory region from ");
            perror(suapi_file_name);
            close(pfd);
            return SU_DEBUG_FAILURE_FILE_READ;
        }

        /* Closing the file does not unmap it. */
        close(pfd);

        fd = open(SUAPI_SHARED_DEVICE, O_RDONLY);
        if (fd < 0)
        {
            fprintf(stderr, "unable to open SUAPI shared device ");
            perror(SUAPI_SHARED_DEVICE);
            su_map_cleanup(ptrs);
            return SU_DEBUG_FAILURE_FILE_OPEN;
        }

        /* Memory map the shared region so that its data can be accessed
         * using pointer manipulation.
         */
        ptrs->shared = mmap(0,
                             private_st.shared_size,
                             PROT_READ,
                             MAP_SHARED,
                             fd,
                             0);
        close(fd);
        if (ptrs->shared == MAP_FAILED)
        {
            fprintf(stderr, "unable to mmap shared memory region from ");
            perror(suapi_file_name);
            su_map_cleanup(ptrs);
            return SU_DEBUG_FAILURE_FILE_READ;
        }

        /* Get proc list, and build process hash table. */
        /* If any one of these operations fails, just continue processing
         * without the process table.
         */
        if (dir = opendir("/proc"))
        {
            struct dirent * ent;
            ptrs->prochash = su_hash_create(PROCTABLE_HASH_SIZE);

            while(ent = readdir(dir))
            {
                int pid;
                if ((pid = strtoul(ent->d_name,NULL,0)) != 0)
                {
                    char cmdfile[CMDFILELENGTH];
                    char * cmdstr;
                    int nb, retval;

                    fd = -1;
                    cmdstr = NULL;
                    nb = -1;
                    sprintf(cmdfile,"/proc/%d/cmdline",pid);
                    if (    ((fd = open(cmdfile,O_RDONLY))       >= 0)
                         && ((cmdstr = malloc(CMDLINELENGTH)) != NULL)
                         && ((nb = read(fd,cmdstr,CMDLINELENGTH)) > 0) )
                    {
                        /* add to hash */
                        if (nb >= CMDLINELENGTH)
                            cmdstr[CMDLINELENGTH-1] = '\0';
                        else
                            cmdstr[nb] = '\0';
                        close(fd);

                        retval = su_hash_add(ptrs->prochash,
                                            pid,
                                            (unsigned int) cmdstr);
                        if (retval != 0)
                        {
                            close(fd);
                            free(cmdstr);
                            break; /* continue without a full process table. */
                        }

                    }
                    else
                    {
                        if (fd > 0) close(fd);
                        if (cmdstr != NULL) free(cmdstr);
                        if (nb != 0)
                        {
                            /* just go on without prochash */
                            break;
                        }
                        /* else nb == 0; this is a daemon with no cmdline */
                    }
                }
            }
        }
    }

    ptrs->sysconfig = ptrs->private +
                            SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_PRIVATE_DATA));
    su_sysconfig_ptr = (SU_SYSCONFIG *) ptrs->sysconfig;

    ptrs->timers = ptrs->sysconfig +
                            SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_SYSCONFIG));

    ptrs->semarray = ptrs->timers +
                            (su_sysconfig_ptr->ntimers *
                                SU_ROUND_TO_DOUBLE_WORD(private_st.timersz));

    ptrs->Names =
        ptrs->semarray +
            ( (su_sysconfig_ptr->nbinarysemaphores
               + su_sysconfig_ptr->ncountingsemaphores
               + su_sysconfig_ptr->nmutexsemaphores    ) * 
                                    SU_ROUND_TO_DOUBLE_WORD(private_st.semsz) );

    ptrs->PortTable =
        ptrs->Names +
            (su_sysconfig_ptr->namehashtablesize *
                    SU_ROUND_TO_DOUBLE_WORD(private_st.namehashsz));

    ptrs->qarray = ptrs->PortTable +
                    (su_sysconfig_ptr->nports *
                        SU_ROUND_TO_DOUBLE_WORD(sizeof(SU_PORT_HANDLE_STRUCT)));

    ptrs->pcb = ptrs->qarray + (su_sysconfig_ptr->nqueues *
                                    SU_ROUND_TO_DOUBLE_WORD(private_st.queuesz));

#ifdef SUAPI_DEBUG
    printf("ptrs:\n\tprivate = 0x%08X\n\tsysconfig = 0x%08X\n\ttimers = 0x%08X\n\
            \tsemarray = 0x%08X\n\tNames = 0x%08X\n\tPortTable = 0x%08X\n\
            \tqarray = 0x%08X\n\tpcb = 0x%08X\n\tshared = 0x%08X\n",
            ptrs->private, ptrs->sysconfig, ptrs->timers, ptrs->semarray, ptrs->Names,
            ptrs->PortTable, ptrs->qarray, ptrs->pcb, ptrs->shared);
#endif

    return 0;
}
