// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the su_q, su_qb, su_qf, and su_qinfo SUAPI debug
 *      service which print information about SUAPI queues.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
enum {
    Q = 0,
    QB,
    QF,
    QINFO,
};

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(char * name, int Iam);

/************** LOCAL MACROS *************************************************/
#define DEFAULT_NUM_OF_MESSAGES 100

/************** LOCAL VARIABLES **********************************************/
static const char * const optarg_format_str[] = {
"f:?",    /* q        */
"f:l:?",  /* qb       */
"f:l:?",  /* qf       */
"af:?"    /* qinfo    */
};

static const char * const usage_format_str[] = {
/* usage format string for su_q: */
"\nUsage: %s [-f<filename>] [-?] <qindex>\n\
    Print information about the specified queue.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -?            - Display this message.\n\
    <qindex>      - Index of queue that you want to display.\n",

/* Usage format string for su_qb. */
"\nUsage: %s [-f<filename>] [-l <limit>] [-?] <msghdrp>\n\
    Print the messages on the given linked list following the backward link.\n\
    The end of this linked list can be obtained by calling su_q and\n\
    referencing the \"tail\" value of any subqueue which contains messages.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -l <limit>    - Limit on # of messages that will be traversed. Default\n\
                    is 100.\n\
    -?            - Display this message.\n\
    <msghdrp>     - Pointer to a message header, not a message.  If you\n\
                    give an address, it must start with \"0x\".  A pointer\n\
                    to a message may be converted to a pointer to a\n\
                    message header with the su_msg2hdr command.\n",

/* Usage format string for su_qf. */
"\nUsage: %s [-f<filename>] [-l <limit>] [-?] <msghdrp>\n\
    Print the messages on the given linked list following the forward link.\n\
    The beginning of this linked list can be obtained by calling su_q and\n\
    referencing the \"head\" value of any subqueue which contains messages.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -l <limit>    - Limit on # of messages that will be traversed. Default\n\
                    is 100.\n\
    -?            - Display this message.\n\
    <msghdrp>     - Pointer to a message header, not a message.  If you\n\
                    give an address, it must start with \"0x\".  A pointer\n\
                    to a message may be converted to a pointer to a\n\
                    message header with the su_msg2hdr command.\n",

/* Usage format string for su_qinfo. */
"\nUsage: %s [-f<filename>] [-a] [-?]\n\
    Print information about all configured queues.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -a            - Report on all queues including those that are not in use.\n\
    -?            - Display this message.\n"
};

/************** FUNCTIONS ****************************************************/
static void usage(char * name, int Iam)
{
    fprintf(stderr, usage_format_str[Iam], name);
}

int main(int argc, char * argv[])
{
    int c, retval;
    int a_flag = 0;
    int i, qsize;
    unsigned int Iam;
    unsigned int limit = DEFAULT_NUM_OF_MESSAGES;
    unsigned int param1;
    char * suapi_file_name = NULL;
    char * cmd;
    SU_REGION_PTRS ptrs;
    SU_SYSCONFIG * su_sysconfig;
    SU_GLOBAL_DATA * globalp;
    SU_PRIVATE_DATA * privatep;
    SU_QUEUE * q;

    if ((cmd = strstr(argv[0],"su_q")) && strlen(cmd) == 4)
        Iam = Q;
    else if ((cmd = strstr(argv[0],"su_qb")) && strlen(cmd) == 5)
        Iam = QB;
    else if ((cmd = strstr(argv[0],"su_qf")) && strlen(cmd) == 5)
        Iam = QF;
    else if ((cmd = strstr(argv[0],"su_qinfo")) && strlen(cmd) == 8)
        Iam = QINFO;
    else /* error */
    {
        fprintf(stderr,"%s is not a valid SUAPI debug command.\n", argv[0]);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    opterr = 0;

    while ((c = getopt (argc, argv, optarg_format_str[Iam])) != -1)
    {
        switch (c)
        {
            case 'a':
                a_flag = 1;
                break;      
            case 'l':
                limit = strtoul(optarg,NULL,0);
                break;
            case 'f':
                suapi_file_name = optarg;
                break;      
            case '?':
            default:
                usage(argv[0], Iam);
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
                break;
        }
    }

    /* All but qinfo require an additional parameter. */
    if (Iam == QINFO)
    {
        if ((argc-optind) != 0)
        {
            fprintf(stderr, "Too many parameters\n");
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
    }
    else
    {
        if ((argc-optind) < 1 || (argc-optind) > 1)
        {
            fprintf(stderr, "Wrong number of parameters\n");
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
        param1 = strtoul(argv[optind],NULL,0);
    }

    retval = su_map_files(suapi_file_name, &ptrs);
    if (retval)
    {
        usage(argv[0], Iam);
        return retval;
    }

    su_sysconfig = (SU_SYSCONFIG *) ptrs.sysconfig;
    globalp = (SU_GLOBAL_DATA *) ptrs.shared;
    privatep = (SU_PRIVATE_DATA *) ptrs.private;
    qsize = ((SU_PRIVATE_DATA *)ptrs.private)->queuesz;

    switch(Iam)
    {
        case Q:
        {
            char * waiterpid = NO_CMDLINE;
            char * eventpid = NO_CMDLINE;

            /* param1 is expected to be an index into qarray. */
            if (param1 >= su_sysconfig->nqueues)
            {
                usage(argv[0], Iam);
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
            }

            /* Get pointer to SU_QUEUE structure that user is querying. */
            q = (SU_QUEUE *) (ptrs.qarray + (param1 * qsize));

            /* If a process is waiting, get its name from the proc table. */
            if (q->su_waiting_tid)
                su_hash_get_value(ptrs.prochash,
                                    q->su_waiting_tid,
                                    (unsigned int *)(&waiterpid));

            /* If events are registered, get process's name from proctable. */
            if (q->su_event_thandle)
                su_hash_get_value(ptrs.prochash,
                                   q->su_event_thandle,
                                   (unsigned int *)(&eventpid));

            /* The next and prev pointers of the subqueues are stored as kernel
             * addresses. If they point to addresses in the SUAPI shared memory
             * region, they are converted to the user space address range that
             * was active at the time of the panic dump. Otherwise, they are
             * left as they were so that the user can see if the addresses are
             * sane or corrupted.
             */
            printf("\
Queue #%d (0x%08X)\n\
    qhandle: 0x%08X\n\
  * lopri        head:   0x%08X    null:  0x%08X    tail:  0x%08X\n\
    medpri       head:   0x%08X    null:  0x%08X    tail:  0x%08X\n\
    hipri        head:   0x%08X    null:  0x%08X    tail:  0x%08X\n\
    Priority mask:       0x%08X\n\
    Queue Type:          %s\n\
    State:               %d (%s)\n\
    Waiting in RcvMsg:   0x%08X (%s)\n\
    Event process ID:    0x%08X (%s)\n\
    Event mask:          0x%08X\n\
    Total msgs:          %d\n\
\n\
* used for FIFO subqueue\n",
                   param1, CVT(q,ptrs.private,privatep->kprivate_p),
                   SU_QINDEX2QH(param1),
                   cvt_only_if_in_shared(&ptrs,q->su_subqueue[LOW].su_q_head),
                   q->su_subqueue[LOW].su_q_null,
                   cvt_only_if_in_shared(&ptrs,q->su_subqueue[LOW].su_q_tailpred),
                   cvt_only_if_in_shared(&ptrs,q->su_subqueue[MEDIUM].su_q_head),
                   q->su_subqueue[MEDIUM].su_q_null,
                   cvt_only_if_in_shared(&ptrs,q->su_subqueue[MEDIUM].su_q_tailpred),
                   cvt_only_if_in_shared(&ptrs,q->su_subqueue[HIGH].su_q_head),
                   q->su_subqueue[HIGH].su_q_null,
                   cvt_only_if_in_shared(&ptrs,q->su_subqueue[HIGH].su_q_tailpred),
                   q->su_prioritymask, 
                   (q->su_qtype==IS_FIFO_QUEUE) ? "FIFO" : "PRIORITY",
                   q->su_state, (q->su_state==SU_QUSED) ? "IN USE":"NOT IN USE",
                   q->su_waiting_tid, waiterpid,
                   q->su_event_thandle, eventpid,
                   q->su_emask,
                   q->su_qmsgcount);

            break;
        }
        case QB:
        case QF:
        {
            void * ksharbegin = privatep->kshared_p;
            void * ksharend = privatep->kshared_p + privatep->shared_size;
            void * qbegin = CVT(ptrs.qarray,ptrs.private,privatep->kprivate_p);
            void * qend = CVT(ptrs.pcb,ptrs.private,privatep->kprivate_p);
            void * m;
            int msgsprocessed = 0;
            SU_MSG_HDR * msghdrp;

            /* param1 comes in as a SUAPI user space address. Convert it to
             * a kernel space address, and check if it is in the range of the
             * SUAPI shared memory region. "m" will be a kernel address at the
             * beginning of each interation of the following loop.
             *
             * Only print out the number of messages specified by "limit".
             */
            m = (void *) param1;
            if (m < qbegin || m >= qend)
                m = CVT(param1, globalp->user_shmaddress, privatep->kshared_p);
            while (m >= ksharbegin && m < ksharend && limit > 0)
            {
                /* Now convert the message address to something that can be
                 * accessed in the address range of the current process.
                 */
                msghdrp = (SU_MSG_HDR *) CVT(m, ksharbegin, ptrs.shared);
                suapi_debug_mesg(&ptrs, msghdrp, DEFAULT_DATA_LENGTH);
                if (Iam == QB)
                    m = (void *) msghdrp->su_minnode.su_n_prev;
                else
                    m = (void *) msghdrp->su_minnode.su_n_next;
                msgsprocessed++;
                limit--;
            }

            /* If we exitted the loop before the limit was reached AND the
             * next/prev address is not in the range of the SUAPI qarray,
             * then the message pointer is corrupt. 'm' is expected to be
             * in the range of qarray structures at this point if "limit"
             * has not been reached.
             */
            if (limit > 0 && (m < qbegin || m >= qend))
            {
                fprintf(stderr,"Address in message chain is corrupt: 0x%08X\n",
                                 m);
                usage(argv[0], Iam);
                return SU_DEBUG_FAILURE_BAD_ADDRESS;
            }

            printf ("\nTraversed %d message(s).\n\n", msgsprocessed);
            break;
        }
        case QINFO:
        {
            void * qptr;
            int qnumalloc = 0;

            /* First loop through all queues and find out total allocated. */
            for (qptr = ptrs.qarray, i = 0;
                i < su_sysconfig->nqueues;
                qptr += qsize, i++)
            {
                if (((SU_QUEUE *)qptr)->su_state == SU_QUSED)
                    qnumalloc++;
            }

            printf("\
Number of queues: %d\n\
Current number of queues allocated: %d\n\
Queue         Queue Handle    Type        Tot Msgs    Queue Addr\n\
==========    ============    ========    ========    ==========\n",
    su_sysconfig->nqueues, qnumalloc);

            for (qptr = ptrs.qarray, i = 0;
                i < su_sysconfig->nqueues;
                qptr += qsize, i++)
            {
                q = (SU_QUEUE *) qptr;

                if (q->su_state == SU_QUSED)
                    printf("Queue %4d      0x%08X    %-8s    %8d    0x%08X\n",
                            i, SU_QINDEX2QH(i),
                            (q->su_qtype==IS_FIFO_QUEUE) ? "FIFO" : "PRIORITY",
                            q->su_qmsgcount,
                            CVT(qptr,ptrs.private,privatep->kprivate_p));
                else if (a_flag)
                    printf("Queue %4d      0x%08X    NOT IN USE\n", i, SU_QINDEX2QH(i));
            }

            break;
        }
    }
    su_map_cleanup(&ptrs);

    return retval;
}
