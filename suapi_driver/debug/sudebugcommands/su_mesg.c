// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the su_hdr2msg, su_mesg, su_mesgdata, and
 *      su_msg2hdr  SUAPI debug service which print information about SUAPI
 *      messages.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
enum {
    HDR2MSG = 0,
    MESG,
    MESGDATA,
    MSG2HDR,
};

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(char * name, int Iam);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/
static const char * const optarg_format_str[] = {
"?",      /* hdr2msg  */
"f:l:?",  /* mesg     */
"f:l:?",  /* mesgdata */
"?"       /* msg2hdr  */
};

static const char * const usage_format_str[] = {
/* usage format string for su_hdr2msg: */
"\nUsage: %s [-?] <msghdrp>\n\
    Converts address of message header to address of message.\n\
    -?         - Display this message.\n\
    <msghdrp>  - Pointer to a message, not a message header. msg must start\n\
                 with \"0x\".  A pointer to a message header may be converted\n\
                 to a pointer to a message with the su_msg2hdr command.\n",

/* usage format string for su_mesg: */
"\nUsage: %s [-f<filename>] [-l <limit>] [-?] <msghdrp>\n\
    Print message information.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -l <limit>    - Limit on # of bytes of the message data that will be\n\
                    printed.  Default is 512.\n\
    -?            - Display this message.\n\
    <msghdrp>     - Pointer to a message header, not a message. msghdrp\n\
                    must start with \"0x\".  A pointer to a message may be\n\
                    converted to a pointer to a message header with the\n\
                    su_msg2hdr command.\n",

/* usage format string for su_mesgdata: */
"\nUsage: %s [-f<filename>] [-l <limit>] [-?] <msghdrp>\n\
    Print the message data information.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -l <limit>    - Limit on # of bytes of the message data that will be\n\
                    printed.  Default is 512.\n\
    -?            - Display this message.\n\
    <msghdrp>     - Pointer to a message header, not a message. msghdrp\n\
                    must start with \"0x\".  A pointer to a message may be\n\
                    converted to a pointer to a message header with the\n\
                    su_msg2hdr command.\n",

/* usage format string for su_msg2hdr: */
"\nUsage: %s [-?] <msgp>\n\
    Converts address of message to address of message header.\n\
    -?           - Display this message.\n\
    <msgp>       - Pointer to a message, not a message header. msgp must\n\
                   start with \"0x\".  A pointer to a message header may be\n\
                   converted to a pointer to a message with the su_hdr2msg\n\
                   command.\n"
};

/************** FUNCTIONS ****************************************************/
static void usage(char * name, int Iam)
{
    fprintf(stderr, usage_format_str[Iam], name);
}

int main(int argc, char * argv[])
{
    int c, retval;
    int i;
    unsigned int Iam;
    unsigned int limit = DEFAULT_DATA_LENGTH;
    unsigned int param1;
    char * suapi_file_name = NULL;
    char * cmd;
    SU_REGION_PTRS ptrs;
    SU_GLOBAL_DATA * globalp = NULL;

    if ((cmd = strstr(argv[0],"su_hdr2msg")) && strlen(cmd) == 10)
        Iam = HDR2MSG;
    else if ((cmd = strstr(argv[0],"su_mesg")) && strlen(cmd) == 7)
        Iam = MESG;
    else if ((cmd = strstr(argv[0],"su_mesgdata")) && strlen(cmd) == 11)
        Iam = MESGDATA;
    else if ((cmd = strstr(argv[0],"su_msg2hdr")) && strlen(cmd) == 10)
        Iam = MSG2HDR;
    else /* error */
    {
        fprintf(stderr,"%s is not a valid SUAPI debug command.\n", argv[0]);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    opterr = 0;

    while ((c = getopt (argc, argv, optarg_format_str[Iam])) != -1)
    {
        switch (c)
        {
            case 'l':
                limit = strtoul(optarg,NULL,0);
                break;
            case 'f':
                suapi_file_name = optarg;
                break;      
            case '?':
            default:
                usage(argv[0], Iam);
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
                break;
        }
    }

    /* Get address parameter */
    if ((argc-optind) < 1 || (argc-optind) > 1)
    {
        usage(argv[0], Iam);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }
    param1 = strtoul(argv[optind],NULL,0);

    /* For su_mesg and su_mesgdata, map memory. */
    if (Iam == MESG || Iam == MESGDATA)
    {
        retval = su_map_files(suapi_file_name, &ptrs);
        if (retval)
        {
            usage(argv[0], Iam);
            return retval;
        }

        globalp = (SU_GLOBAL_DATA *) ptrs.shared;
    }

    switch(Iam)
    {
        case HDR2MSG:
        {
            printf ("0x%08X\n", param1 + sizeof(SU_MSG_HDR));
            break;
        }
        case MESG:
        case MESGDATA:
        {
            /* In this case, param1 is the user space address of the message
             * header in the range of user_shmaddress. It must be converted to
             * the address space of the current process so it can be
             * dereferenced by this utility.
             */
            SU_MSG_HDR * msghdrp;
            msghdrp = (SU_MSG_HDR *) CVT(param1,
                                         globalp->user_shmaddress, ptrs.shared);
            if (Iam == MESG)
                suapi_debug_mesg(&ptrs, msghdrp, limit);
            else
                suapi_debug_mesgdata(&ptrs, msghdrp, limit);
            break;
        }
        case MSG2HDR:
        {
            printf ("0x%08X\n", param1 - sizeof(SU_MSG_HDR));
            break;
        }
    }

    if (Iam == MESG || Iam == MESGDATA)
        su_map_cleanup(&ptrs);

    return 0;
}
