// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the su_sysconfig SUAPI debug service which prints
 *      SUAPI system configuration information.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(void);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/
static void usage(void)
{
    fprintf(stderr,"\nUsage: su_sysconfig [-f<filename>] [-?]\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -?            - Display this message.\n");
}

int main(int argc, char * argv[])
{
    int c, retval = 0;
    char * suapi_file_name = NULL;
    SU_REGION_PTRS ptrs;
    SU_SYSCONFIG * su_sysconfig;

    opterr = 0;

    while ((c = getopt (argc, argv, "f:?")) != -1)
    {
        switch (c)
        {
            case 'f':
                suapi_file_name = optarg;
                break;      
            case '?':
            default:
                usage();
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
                break;
        }
    }

    if ((argc-optind) != 0)
    {
        fprintf(stderr, "Too many parameters\n");
        usage();
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    retval = su_map_files(suapi_file_name, &ptrs);
    if (retval)
    {
        usage();
        return retval;
    }

    su_sysconfig = (SU_SYSCONFIG *) ptrs.sysconfig;

    printf("\n\
SUAPI SU_SYSCONFIG table information:\n\
       maxtimeout:                   0x%08X\n\
       mintimeout:                   0x%08X\n\
       nqueues:                      %d\n\
       npartition:                   %d\n\
       ntimers:                      %d\n\
       nbinarysemaphores:            %d\n\
       ncountingsemaphores:          %d\n\
       nmutexsemaphores:             %d\n\
       memorypromotion:              %d\n\
       namehashtablesize:            %d\n\
       nports:                       %d\n\
       nflaggroups:                  %d\n\
       ntasks:                       %d\n\
       time_slice_quantum:           %d\n\
       nbuckets:                     %d\n\
       heap_size:                    %d\n\
       nheapbuckets:                 %d\n\
       realtimefloor:                %d\n",
  su_sysconfig->maxtimeout,
  su_sysconfig->mintimeout,
  su_sysconfig->nqueues,
  su_sysconfig->npartition,
  su_sysconfig->ntimers,
  su_sysconfig->nbinarysemaphores,
  su_sysconfig->ncountingsemaphores,
  su_sysconfig->nmutexsemaphores,
  su_sysconfig->memorypromotion,
  su_sysconfig->namehashtablesize,
  su_sysconfig->nports,
  su_sysconfig->nflaggroups,
  su_sysconfig->ntasks,
  su_sysconfig->time_slice_quantum,
  su_sysconfig->nbuckets,
  su_sysconfig->heap_size,
  su_sysconfig->nheapbuckets,
  su_sysconfig->realtimefloor
);

    su_map_cleanup(&ptrs);

    return retval;
}
