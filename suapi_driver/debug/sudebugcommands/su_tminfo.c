// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the tm, tmlist, and tminfo SUAPI debug commands
 *      which print information on all configured timers.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
enum {
    TMINFO = 0,
    TMLIST,
    TM,
};

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(char * name, int Iam);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/
static const char * const optarg_format_str[] = {
"f:a?", /* tminfo */
"f:?",  /* tmlist */
"f:?"   /* tm     */
};

static const char * const usage_format_str[] = {
/* usage format string for su_tminfo: */
"\nUsage: %s [-f<filename>] [-a] [-?]\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -a            - Print information about all configured timers, including\n\
                    those that are not in use. If this flag is not specified,\n\
                    this command only reports on the timers that are in use.\n\
    -?            - Display this message.\n",

/* Usage format string for su_tmlist. */
"\nUsage: %s [-f<filename>] [-?]\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -?            - Display this message.\n",

/* Usage format string for su_tm. */
"\nUsage: %s [-f<filename>] [-?] <tmindex>\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -?            - Display this message.\n\
    <tmindex>     - Index of timer to report on.\n"
};

/************** FUNCTIONS ****************************************************/
static void usage(char * name, int Iam)
{
    fprintf(stderr, usage_format_str[Iam], name);
}

int main(int argc, char * argv[])
{
    int c, retval, tmindex;
    int a_flag = 0;
    char * suapi_file_name = NULL;
    int Iam;
    SU_REGION_PTRS ptrs;
    SU_SYSCONFIG * su_sysconfig_p;

    if (strstr(argv[0],"su_tmlist"))
        Iam = TMLIST;
    else if (strstr(argv[0],"su_tminfo"))
        Iam = TMINFO;
    else /* must be tm */
        Iam = TM;

    opterr = 0;

    while ((c = getopt (argc, argv, optarg_format_str[Iam])) != -1)
    {
        switch (c)
        {
            case 'a':
                a_flag = 1;
                break;      
            case 'f':
                suapi_file_name = optarg;
                break;      
            case '?':
            default:
                usage(argv[0], Iam);
                return 1;
                break;
        }
    }
    if ((Iam == TMINFO) || (Iam == TMLIST))
    {
        if ((argc-optind) != 0)
        {
            fprintf(stderr, "Too many parameters\n");
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
    }
    else
    {
        /* Check the <tmindex> parameter. */
        if ((argc-optind) > 1)
        {
            fprintf(stderr, "%s: too many parameters\n", argv[0]);
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
        else if ((argc-optind) == 0)
        {
            fprintf(stderr, "%s: <tmindex> not specified\n", argv[0]);
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
        else
        {
            tmindex = atoi(argv[optind]);
        }
    }

    retval = su_map_files(suapi_file_name, &ptrs);
    if (retval)
    {
        usage(argv[0], Iam);
        return retval;
    }

    su_sysconfig_p = (SU_SYSCONFIG *) ptrs.sysconfig;

    switch(Iam)
    {
        case TMLIST:
            retval = suapi_debug_tmlist(&ptrs);
            break;

        case TMINFO:
            retval = suapi_debug_tminfo(&ptrs, a_flag);
            break;

        case TM:
            if (tmindex < 0
                || tmindex >= su_sysconfig_p->ntimers)
            {
                fprintf(stderr,
                        "%s: timer index tmindex == %d is out of range.\n",
                        argv[0], tmindex);
                usage(argv[0], Iam);
                su_map_cleanup(&ptrs);
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
            }

            retval = suapi_debug_tm(&ptrs, tmindex);
            break;
    }

    su_map_cleanup(&ptrs);

    return retval;
}
