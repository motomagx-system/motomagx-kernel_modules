// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the p and pinfo SUAPI debug services which print
 *      information about SUAPI ports.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/************** LOCAL CONSTANTS **********************************************/
/* PORT_INDEX_MASK will take off the task bit and the creation count. */
#define PORT_INDEX_MASK 0x00FFFFFF

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
enum {
    P = 0,
    PINFO,
};

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(char * name, int Iam);
static char * port_type_str(unsigned char pt);
static char * routing_mode_str(unsigned int rm);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/
static const char * const optarg_format_str[] = {
"f:?",    /* p        */
"af:?"    /* pinfo    */
};

static const char * const usage_format_str[] = {
/* usage format string for su_p: */
"\nUsage: %s [-f<filename>] [-?] <port>\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -?            - Print this message.\n\
    <port>        - The port to report on. May be a port index or port handle.\n",

/* Usage format string for su_pinfo. */
"\nUsage: %s [-f<filename>] [-a] [-?]\n\
    Print information about all of the configured ports.\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -a            - Print information about all configured ports, including\n\
                    those that are not in use. If this flag is not specified,\n\
                    this command only reports on the ports that are in use.\n\
    -?            - Print this message.\n"
};

/************** FUNCTIONS ****************************************************/
static void usage(char * name, int Iam)
{
    fprintf(stderr, usage_format_str[Iam], name);
}

/* port_type_str:
 *    Converts a port type value into its corresponding string.
 *
 * INPUTS:
 *    pt: port type value
 *
 * OUTPUTS:
 *    None
 *
 * RETURNS:
 *    String corresponding to port type value.
 *
 */
static char * port_type_str(unsigned char pt)
{
    switch (pt)
    {
        case SU_MESSAGE_PORT:
            return ("SU_MESSAGE_PORT");
            break;
        case SU_LOGGER_PORT:
            return ("SU_LOGGER_PORT");
            break;
        default:
            return ("SU_UNALLOCATED_PORT");
            break;
    }
}


/* routing_mode_str:
 *    Converts a port routing mode value into its corresponding string.
 *
 * INPUTS:
 *    rm: port routing mode value
 *
 * OUTPUTS:
 *    None
 *
 * RETURNS:
 *    String corresponding to port routing mode value or "UNKNOWN" if
 *    routing mode is not recognized.
 *
 */
static char * routing_mode_str(unsigned int rm)
{
    switch (rm)
    {
        case SU_ROUTE_NORMAL:
            return ("SU_ROUTE_NORMAL");
            break;
        case SU_ROUTE_MIRROR:
            return ("SU_ROUTE_MIRROR");
            break;
        case SU_ROUTE_INTERCEPT:
            return ("SU_ROUTE_INTERCEPT");
            break;
        default:
            return ("UNKNOWN");
            break;
    }
}

int main(int argc, char * argv[])
{
    int c, retval;
    int a_flag = 0;
    int i, qsize;
    unsigned int Iam;
    unsigned int port, pindex;
    char * suapi_file_name = NULL;
    char * cmd;
    SU_REGION_PTRS ptrs;
    SU_SYSCONFIG * su_sysconfig;
    SU_GLOBAL_DATA * globalp;
    SU_PRIVATE_DATA * privatep;
    SU_PORT_HANDLE_STRUCT * ptblp, * pptr;

    if ((cmd = strstr(argv[0],"su_p")) && strlen(cmd) == 4)
        Iam = P;
    else if ((cmd = strstr(argv[0],"su_pinfo")) && strlen(cmd) == 8)
        Iam = PINFO;
    else /* error */
    {
        fprintf(stderr,"%s is not a valid SUAPI debug command.\n", argv[0]);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    opterr = 0;

    while ((c = getopt (argc, argv, optarg_format_str[Iam])) != -1)
    {
        switch (c)
        {
            case 'a':
                a_flag = 1;
                break;      
            case 'f':
                suapi_file_name = optarg;
                break;      
            case '?':
            default:
                usage(argv[0], Iam);
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
                break;
        }
    }

    /* su_p has an additional parameter. */
    if (Iam == P)
    {
        if ((argc-optind) < 1 || (argc-optind) > 1)
        {
            fprintf(stderr, "Wrong number of parameters\n");
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
        port = strtoul(argv[optind],NULL,0);
    }
    else if (Iam == PINFO)
    {
        if ((argc-optind) != 0)
        {
            fprintf(stderr, "Too many parameters\n");
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
    }

    retval = su_map_files(suapi_file_name, &ptrs);
    if (retval)
    {
        usage(argv[0], Iam);
        return retval;
    }

    su_sysconfig = (SU_SYSCONFIG *) ptrs.sysconfig;
    globalp = (SU_GLOBAL_DATA *) ptrs.shared;
    privatep = (SU_PRIVATE_DATA *) ptrs.private;
    ptblp = (SU_PORT_HANDLE_STRUCT *)ptrs.PortTable;

    switch(Iam)
    {
        case P:
        {
            /* port is expected to be either an index into the port table array
             * or a port handle obtained from other SUAPI debug commands.
             * A port handle can be converted to an index by stripping off the
             * top byte. If it's already an index, the top byte doesn't matter
             * because port indexes don't get that large, so always do the
             * stripping.
             */
            pindex = PORT_INDEX_MASK & port;
            if (pindex >= su_sysconfig->nports)
            {
                usage(argv[0], Iam);
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
            }

            pptr = &(ptblp[pindex]);

            if (pptr->port_type == SU_UNALLOCATED_PORT)
            {
                printf("\n\
    =========================================\n\
    NOT IN USE, but here's the data anyway...\n\
    =========================================\n");
            }
            printf("\
Port #%d\n\
    port_handle from structure: 0x%08X\n\
    port_type:                  %d (%s)\n\
    logging_enabled:            %d (%s)\n\
    routing_mode:               %d (%s)\n\
    port_id:                    0x%04X\n\
    qhandle:                    0x%08X\n\
    qindex:                     %d\n",
                pindex,
                0, /* pptr->port_handle, */
                pptr->port_type, port_type_str(pptr->port_type),
                pptr->logging_enabled, ((pptr->logging_enabled) ? "YES" : "NO"),
                pptr->routing_mode, routing_mode_str(pptr->routing_mode),
                pptr->port_id,
                pptr->port_transport.qhandle,
                SU_QH2QINDEX(pptr->port_transport.qhandle));
            break;
        }

        case PINFO:
        {
            int pnumalloc = 0;

            /* First loop through all ports and find out total allocated. */
            for (pindex = 0; pindex < su_sysconfig->nports; pindex++)
            {
                if (ptblp[pindex].port_type != SU_UNALLOCATED_PORT)
                    pnumalloc++;
            }

            printf("\
Number of ports: %d\n\
Current number of ports allocated: %d\n\
Port       PortHandle  PortType          PortId  QHandle     QIndex\n\
=========  ==========  ================  ======  ==========  ======\n",
                    su_sysconfig->nports, pnumalloc);

            for (pindex = 0; pindex < su_sysconfig->nports; pindex++)
            {
                if (ptblp[pindex].port_type != SU_UNALLOCATED_PORT)
                    printf("Port %4d  0x%08X  %16.16s  0x%04X  0x%08X  %6d\n",
                            pindex,
                            0, /* ptblp[pindex].port_handle, */
                            port_type_str(ptblp[pindex].port_type),
                            ptblp[pindex].port_id,
                            ptblp[pindex].port_transport.qhandle,
                            SU_QH2QINDEX(ptblp[pindex].port_transport.qhandle));
                else if (a_flag)
                    printf("Port %4d    NOT IN USE\n", pindex);
            }

            break;
        }
    }
    su_map_cleanup(&ptrs);

    return retval;
}
