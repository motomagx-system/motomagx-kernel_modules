// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 


/*
 *  DESCRIPTION:
 *      This file implements the minfo SUAPI debug service which prints current
 *      statistics on the SUAPI memory pools.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(void);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/
static void usage(void)
{
    fprintf(stderr,"\nUsage: su_minfo [-f<filename>] [-?]\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -?            - Display this message.\n");
}

int main(int argc, char * argv[])
{
    int c, retval;
    char * suapi_file_name = NULL;
    SU_REGION_PTRS ptrs;

    opterr = 0;

    while ((c = getopt (argc, argv, "f:?")) != -1)
    {
        switch (c)
        {
            case 'f':
                suapi_file_name = optarg;
                break;      
            case '?':
            default:
                usage();
                return 1;
                break;
        }
    }

    if ((argc-optind) != 0)
    {
        fprintf(stderr, "Too many parameters\n");
        usage();
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    retval = su_map_files(suapi_file_name, &ptrs);
    if (retval)
    {
        usage();
        return retval;
    }

    retval = suapi_debug_minfo(&ptrs);

    su_map_cleanup(&ptrs);

    return retval;
}
