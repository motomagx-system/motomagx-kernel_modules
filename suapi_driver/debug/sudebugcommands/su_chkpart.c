// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the chkpart SUAPI debug service which checks the
 *      specified partitions for bad blocks.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

/************** LOCAL CONSTANTS **********************************************/
#define DEFAULT_MAXDEPTH 400

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(void);

/************** LOCAL MACROS *************************************************/

/************** LOCAL VARIABLES **********************************************/

/************** FUNCTIONS ****************************************************/
static void usage(void)
{
    fprintf(stderr,"\n\
Usage: su_chkpart [-f<filename>] [-m <maxdepth>] [-v] [-?] \
[<pidstart> [<pidend>]]\n\
    -f <filename> - Name of panic dump file (uses /dev/suapi* if omitted).\n\
    -m <maxdepth> - Maximum depth for searching the free list. Default is 400.\n\
    -v            - Request verbose output.\n\
    -?            - Display this message.\n\
    <pidstart>    - ID of starting partition to check. If <pidend> is not\n\
                    given, this is the only partition that is checked.\n\
    <pidend>      - ID of ending partition to check.\n\n");
}

int main(int argc, char * argv[])
{
    int verbose = 0;
    int maxdepth = DEFAULT_MAXDEPTH;
    int pidstart = -1;
    int pidend = -1;
    int c, retval;
    char * suapi_file_name = NULL;
    SU_REGION_PTRS ptrs;
    SU_SYSCONFIG * su_sysconfig_ptr;

    opterr = 0;

    while ((c = getopt (argc, argv, "f:m:v?")) != -1)
    {
        switch (c)
        {
            case 'f':
                suapi_file_name = optarg;
                break;      
            case 'm':
                maxdepth = atoi(optarg);
                if (maxdepth < 0)
                {
                    fprintf(stderr, "%s: maxdepth--bad value: %d\n",
                            argv[0], maxdepth);
                    usage();
                    return SU_DEBUG_FAILURE_BAD_PARAMETER;
                }
                break;
            case 'v':
                verbose = 1;
                break;
            case '?':
            default:
                usage();
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
                break;
        }
    }

    if ((argc-optind) > 2)
    {
        fprintf(stderr, "%s: too many parameters\n", argv[0]);
        usage();
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }
    else
    {
        if ((argc-optind) > 0)
        {
            pidstart = atoi(argv[optind]);
            if (pidstart < 0)
            {
                fprintf(stderr,
                        "%s: partition index pidstart == %d is out of range.\n",
                        argv[0], pidstart);
                usage();
                return SU_DEBUG_FAILURE_BAD_PARAMETER;
            }
            if ((argc-optind) > 1)
            {
                pidend = atoi(argv[optind+1]);
                if (pidend < 0)
                {
                    fprintf(stderr, 
                          "%s: partition index pidend == %d is out of range.\n",
                          argv[0], pidend);
                    usage();
                    return SU_DEBUG_FAILURE_BAD_PARAMETER;
                }
            }
        }
        /* else no partitions specified on command line, so use defaults */
    }

    retval = su_map_files(suapi_file_name, &ptrs);
    if (retval)
    {
        usage();
        return retval;
    }

    su_sysconfig_ptr = (SU_SYSCONFIG *) ptrs.sysconfig;

    /* If pidstart is -1, neither pidstart nor pidend were supplied so
     * use the defaults.
     */
    if (pidstart == -1)
    {
        pidstart = 0;
        pidend = su_sysconfig_ptr->npartition - 1;
    }

    /* If pidend is still -1, pidstart was specified but pidend was not. */
    if (pidend == -1)
        pidend = pidstart;

    /* Determine if requested partition values are out of range. */
    if (pidstart >= su_sysconfig_ptr->npartition)
    {
        fprintf(stderr, "%s: partition index pidstart == %d is out of range.\n",
                argv[0], pidstart);
        usage();
        su_map_cleanup(&ptrs);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    if (pidend >= su_sysconfig_ptr->npartition)
    {
        fprintf(stderr, "%s: partition index pidend == %d is out of range.\n",
                argv[0], pidend);
        usage();
        su_map_cleanup(&ptrs);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    /* Now that the SUAPI private and shared memory regions are mapped into
     * this process, the SUAPI data structures can be accessed via pointers.
     */
    retval = suapi_debug_chkpart(&ptrs,
                                 maxdepth,
                                 verbose,
                                 pidstart,
                                 pidend);

    switch (retval)
    {
        case SU_DEBUG_FAILURE_NO_MEM:
            fprintf(stderr,
                    "%s: insufficient memory to perform internal allocations\n",
                    argv[0]);
            break;
    }

    su_map_cleanup(&ptrs);

    return retval;
}
