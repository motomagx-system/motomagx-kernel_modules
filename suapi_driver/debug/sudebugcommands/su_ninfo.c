// Copyright (C) 2006 Motorola
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version. 
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details. 
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
// USA 

/*
 *  DESCRIPTION:
 *      This file implements the ninfo, namehash, nf, nb, and nfind SUAPI debug
 *      commands which print information on the name services table.
 */

// Date       Author         Comment
// 2006-09-29 Motorola       Port SUAPI to Linux.

/************** INCLUDES *****************************************************/
#include <su_linux_debug.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>

/************** LOCAL CONSTANTS **********************************************/
/* name length is restricted to 16 characters + NULL. */
#define NAMELEN 17

/************** LOCAL STRUCTURES, ENUMS, AND TYPEDEFS ************************/
typedef struct suNamesNode
{
    struct hlist_node hlnode;
    unsigned int sname;        /* pointer to name string (char * in kernel) */
    unsigned int value;        /* value associated with name string */
} suNamesNode;

enum {
    NINFO = 0,
    NAMEHASH,
    NF,
    NB,
    NFIND,
};

/************** LOCAL FUNCTION PROTOTYPES ************************************/
static void usage(char * name, unsigned int Iam);
static unsigned int hash_this_name(const char *sname, unsigned int hash_size);

/************** LOCAL MACROS *************************************************/
/* These macros help the NF and NB check the node pointer and find the next
 * node in the chain.
 * They expect to be called within the context of the "Iam" and su_priv_p
 * variables.
 */

/* NOTDONE: continues while (p) is not 0 and not in the range of the SUAPI
 * private memory area
 */
#define NOTDONE(p)                                                             \
 ( (((p) != 0)                                                                 \
     && ( ((p) < (unsigned int) su_priv_p->kprivate_p)                         \
          || ((p)>(unsigned int)su_priv_p->kprivate_p+su_priv_p->private_size) \
        )                                                                      \
   )                                                                           \
 )

/************** LOCAL VARIABLES **********************************************/
static const char * const optarg_format_str[] = {
"?",    /* ninfo    */
"?",    /* namehash */
"l:?",  /* nf       */
"l:?",  /* nb       */
"l:?",  /* ninfo    */
};

static const char * const usage_format_str[] = {

/* Usage format string for su_ninfo: */
"\nUsage: %s [-?]\n\
    Displays the address of the first node of each name hash list entry.\n\
    Address may be used as input to the su_nf and su_nb commands.\n\
    -?     Display this message.\n",

/* Usage format string for su_namehash. */
"\nUsage: %s [-?] <name>\n\
    Returns the hashed value of the named string. May be used as input to\n\
    the su_nf and su_nb commands if the name has also been registered.\n\
    -?     Display this message.\n\
    <name> The name for which to search. The use of quotes around the name\n\
           is optional.\n",

/* Usage format string for su_nf. */
"\nUsage: %s [-l <limit>] [-?] <nptr>\n\
    Beginning at <nptr>, this command prints the information about each name\n\
    node in the hash list, following the forward link\n\
    -l<limit> Limit on the number of names that will be traversed. This stops\n\
             infinite loops in case the address given is invalid or the name\n\
             list has become corrupted. You can change the limit by supplying\n\
             a different value for this argument.  Default is 100.\n\
    -?       Display this message.\n\
    <nptr>   Specifies a name element. If an index is given, it specifies\n\
             the name element at the head of the name hash table entry\n\
             corresponding to the index. If a pointer is given, it must point\n\
             to the address of a name element and must start with \"0x\".\n",

/* Usage format string for su_nb. */
"\nUsage: %s [-l <limit>] [-?] <nptr>\n\
    Beginning at <nptr>, this command prints the information about each name\n\
    node in the hash list, following the backward link\n\
    -l<limit> Limit on the number of names that will be traversed. This stops\n\
             infinite loops in case the address given is invalid or the name\n\
             list has become corrupted. You can change the limit by supplying\n\
             a different value for this argument.  Default is 100.\n\
    -?       Display this message.\n\
    <nptr>   Specifies a name element. If an index is given, it specifies\n\
             the name element at the head of the name hash table entry\n\
             corresponding to the index. If a pointer is given, it must point\n\
             to the address of a name element and must start with \"0x\".\n",

/* Usage format string for su_nfind. */
"\nUsage: %s [-l <limit>] [-?] <name>\n\
    Return the value associated with a registered name.\n\
    -l<limit> Limit on the number of names that will be traversed. This stops\n\
             infinite loops in case the address given is invalid or the name\n\
             list has become corrupted. You can change the limit by supplying\n\
             a different value for this argument.  Default is 100.\n\
    -?       Display this message.\n\
    <name>   The name for which to search. The use of quotes around the name\n\
             is optional.\n",
};

/************** FUNCTIONS ****************************************************/
static void usage(char * name, unsigned int Iam)
{
    fprintf(stderr, usage_format_str[Iam], name);
}

static unsigned int hash_this_name(const char *sname, unsigned int hash_size)
{
    unsigned int rvalue;
    unsigned int i;

    for (rvalue = 0, i = 0; sname[i]; i++)
       rvalue ^= sname[i] << (i % 28);

    return rvalue % hash_size;
}

int main(int argc, char * argv[])
{
    unsigned int kmem_fd;
    int c, i, retval;
    unsigned int Iam;
    SU_PRIVATE_DATA *su_priv_p = NULL;
    struct hlist_head * names = NULL;
    char * cmd;
    unsigned int nptr = 0;
    unsigned int limit = 100;
    suNamesNode namenode = {{0,0},0,0};
    char namestr[NAMELEN];
    char * name_to_find;
    SU_SYSCONFIG *su_sysconfig_p = NULL;
    SU_REGION_PTRS ptrs;

    if ((cmd = strstr(argv[0],"su_ninfo")) && strlen(cmd) == 8)
        Iam = NINFO;
    else if ((cmd = strstr(argv[0],"su_namehash")) && strlen(cmd) == 11)
        Iam = NAMEHASH;
    else if ((cmd = strstr(argv[0],"su_nf")) && strlen(cmd) == 5)
        Iam = NF;
    else if ((cmd = strstr(argv[0],"su_nb")) && strlen(cmd) == 5)
        Iam = NB;
    else if ((cmd = strstr(argv[0],"su_nfind")) && strlen(cmd) == 8)
        Iam = NFIND;
    else /* error */
    {
        fprintf(stderr,"%s is not a valid SUAPI debug command.\n", argv[0]);
        return SU_DEBUG_FAILURE_BAD_PARAMETER;
    }

    opterr = 0;

    while ((c = getopt (argc, argv, optarg_format_str[Iam])) != -1)
    {
        switch (c)
        {
            case 'l':
                limit = strtoul(optarg,NULL,0);
                break;
            case '?':
            default:
                usage(argv[0], Iam);
                return 1;
                break;
        }
    }

    retval = su_map_files(NULL, &ptrs);
    if (retval)
    {
        usage(argv[0], Iam);
        return retval;
    }
    su_priv_p = (SU_PRIVATE_DATA *)ptrs.private;
    su_sysconfig_p = (SU_SYSCONFIG *) ptrs.sysconfig;

    if (Iam == NF || Iam == NB || Iam == NFIND)
    {
        if ((argc-optind) < 1 || (argc-optind) > 1)
        {
            usage(argv[0], Iam);
            su_map_cleanup(&ptrs);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
        if (Iam == NFIND)
        {
            name_to_find = argv[optind];
            nptr = hash_this_name(name_to_find,
                                  su_sysconfig_p->namehashtablesize);
        }
        else
        {
            nptr = strtoul(argv[optind],NULL,0);
        }
    }
    else if (Iam == NINFO)
    {
        if ((argc-optind) != 0)
        {
            fprintf(stderr, "Too many parameters\n");
            usage(argv[0], Iam);
            return SU_DEBUG_FAILURE_BAD_PARAMETER;
        }
    }

    names = (struct hlist_head *) ptrs.Names;

    switch (Iam)
    {
        case NINFO:
            /* Loop through the name hash table and print the pointer to the
             * beginning of each list.
             */

            printf("\
Number of name hash table entries: %d\n\
Entry       ListHead\n\
==========  ==========\n", su_sysconfig_p->namehashtablesize);

            for (i = 0; i < su_sysconfig_p->namehashtablesize; i++)
                printf("Entry%5d  0x%08X\n", i, names[i].first);

            break;

        case NAMEHASH:
            printf("%d\n",
                    hash_this_name(argv[1], su_sysconfig_p->namehashtablesize));
            break;
        case NF:
        case NB:
        case NFIND:
            kmem_fd = open64("/dev/kmem", O_RDONLY);
            if (kmem_fd < 0)
            {
                fprintf(stderr, "%s: unable to open ", argv[0]);
                perror("/dev/kmem");
                su_map_cleanup(&ptrs);
                return SU_DEBUG_FAILURE_FILE_OPEN;
            }

            /* If nptr is an index (as opposed to an address), get the address
             * of the first element in the specified name list.
             */
            if (nptr < su_sysconfig_p->namehashtablesize)
            {
                 /* Without the cast to (unsigned int), the compiler sign
                  * extends the value of "first" when converting to loff_t.
                  */
                nptr = (unsigned int) names[nptr].first;
            }

            if (Iam != NFIND)
            {
                printf("\nNumber of name hash table entries: %d\n",
                       su_sysconfig_p->namehashtablesize);
            }

            for(i = 0; NOTDONE(nptr) && (i < limit); i++)
            {
                /* lseekread() is a macro which will cause the program to
                 * return if an error occurs. Therefore, no error checking
                 * is necessary here.
                 */
                /* Read names node structure. */
                lseekread(kmem_fd, nptr, &namenode, sizeof(namenode));

                /* Read name string. */
                lseekread(kmem_fd, namenode.sname, &(namestr[0]), NAMELEN);
                if (Iam == NFIND)
                {
                    if (!strncmp(namestr, name_to_find, NAMELEN))
                    {
                        /* The name was found. */
                        printf("0x%08X\n", namenode.value);
                        close(kmem_fd);
                        su_map_cleanup(&ptrs);
                        return 0;
                    }
                }
                else
                {
                    printf("\
Name Node: (0x%08X)\n\
    su_Name (0x%08X): \"%s\"\n\
    su_Handle:            %d (0x%08X)\n\
    su_Next:              0x%08X\n\
    su_Previous:          0x%08X\n",
                       nptr,                           /* addr of Name node  */
                       namenode.sname, namestr,        /* name addr & string */
                       namenode.value, namenode.value, /* value in dec & hex */
                       namenode.hlnode.next,           /* value of next ptr  */
                       namenode.hlnode.pprev);         /* value of prev ptr  */
                }

              /* Follow link backward if this is NB, forwards if NF or NFIND. */
                if (Iam == NB)
                    nptr = (unsigned int) namenode.hlnode.pprev;
                else
                    nptr = (unsigned int) namenode.hlnode.next;
            }

            close(kmem_fd);
            break;
    }

    /* If NFIND and control reaches here, name must not have been found. */
    if (Iam == NFIND)
        printf("NOTFOUND\n");

    su_map_cleanup(&ptrs);

    return 0;
}
