/******************************************************************************
 * IPC Link Driver netmux_sdma_linkdriver.c                                   *
 *                                                                            *
 * Copyright (C) Motorola 2006-2007                                           *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions are     *
 * met:                                                                       *
 *                                                                            *
 * o Redistributions of source code must retain the above copyright notice,   *
 *   this list of conditions and the following disclaimer.                    *
 * o Redistributions in binary form must reproduce the above copyright        *
 *   notice, this list of conditions and the following disclaimer in the      *
 *   documentation and/or other materials provided with the distribution.     *
 * o Neither the name of Motorola nor the names of its contributors may be    *
 *   used to endorse or promote products derived from this software without   *
 *   specific prior written permission.                                       *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,  *
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR     *
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR           *
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR         *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 *                                                                            *
 ******************************************************************************/
/*   DATE        OWNER       COMMENT                                          *
 *   ----------  ----------  -----------------------------------------------  *
 *   2006/09/28  Motorola    Initial version                                  *
 *   2006/10/19  Motorola    Fixed scheduling while atomic issues             *
 *   2006/12/26  Motorola    Check bd status when mxc_dma_get_config() returns*
 *   2007/01/05  Motorola    Fixed race conditions that existed between SDMA  *
 *                           and MU tasklets                                  *
 *   2007/04/11  Motorola    Increased LOC_MAX_RCV_SIZ and REM_MAX_RCV_SIZ to *
 *                           1552                                             *
 *   2007/07/05  Motorola    Treat SUSPEND_ACK like SUSPEND_REQ               *
 *   2007/08/16  Motorola    Delay before saying ok to suspend                *
 *   2007/09/13  Motorola    Implement AP LD workaround for HelloMoto hang    *
 *   2007/09/24  Motorola    Add comments.                                    *
 ******************************************************************************/
 
/* netmux_sdma_linkdriver.c is responsible for communicating with the NetMUX  *
 * above and with the physical link driver below.  An SDMA data channel is    *
 * used to send and receive NetMUX data and a Message Unit (MU) channel is    *
 * used to send and receive Power Management (PM) messages over the IPC link. */
                                                
/* Note that this driver employs the defined asynchronous read and write      *
 * mechanism of the IPC driver, which buffers this driver from the SDMA and   *
 * MU drivers, below.  Since it is not guaranteed that reads and/or writes    *
 * will not occur immediately upon being set up, this driver can not count    *
 * on the fact that the operations are completed until the callbacks are      *
 * invoked.                                                                   */

#include <linux/module.h>
#include <linux/config.h>
#include <linux/sched.h>
#include <linux/interrupt.h>
#include <linux/fcntl.h>
#include <linux/major.h>
#include <linux/slab.h>
#include <linux/skbuff.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/netdevice.h>
#include <linux/mpm.h>
#include <linux/device.h>
#include <linux/spinlock.h>

#include <asm/arch/sdma.h>
#include <asm/arch/mxc_ipc.h>
#include <asm/arch/mxc_mu.h>
#include <asm/io.h>

/* NON CACHE BUFFER HACK*/
#include <asm/arch/sdma.h>

#include <ldprotocol.h>

/*
 * Define our module
 */
MODULE_DESCRIPTION("NetMUX Link driver for Linux");
MODULE_LICENSE("Dual BSD/GPL");


/*
 * If debugging or logging is turned on setup the appropriate
 * macros.
 */
#ifdef _DEBUG_
    #define DEBUG(args...) printk("IPC Link Driver: " args)
#else
    #define DEBUG(args...)
#endif

#ifdef _LOG_
    #define LOG(args...) printk(args)

    void LOGSKBUFF (struct sk_buff* skb)
    {
        int index;

        printk("Commbuff: 0x%p  Length: %d  Data:", skb, skb->len);
        for (index = 0; index < skb->len; index++)
        {
            if (!(index&0x0F))
                printk("\n");

            printk("0x%x ", skb->data[index]);
        }

        printk("\n");
    }
#else
    #define LOG(args...)
    #define LOGSKBUFF(skb)
#endif


/*
 * Define the receive buffer sizes.  Note that the remote receive buffer size
 * will be reset by the remote IPC Link Driver so don't depend on this
 * definition.
 */
#define LOC_MAX_RCV_SIZ  1552
#define REM_MAX_RCV_SIZ  1552

/*
 * Define the MU parameters
 */
#define MU_CHANNEL    3

enum {
    NM_LD_PM_STATE_NOT_READY = 0,
    NM_LD_PM_STATE_READY,
    NM_LD_PM_STATE_SUSPENDING,
    NM_LD_PM_STATE_SUSPENDED,
    NM_LD_PM_STATE_WAKING
}nm_ld_states;

#define IOI_OFF 0
#define IOI_ON  1

/*
 * note that these definitinons have to match the same
 * set defined on the BP in cn_ild_config.h
 */
enum {
    NM_LD_MSG_SUSPEND_REQ = 1,
    NM_LD_MSG_SUSPEND_ACK,
    NM_LD_MSG_RESUME
}nm_ld_msgs;

/* To keep things simple, let's have just one lock that we can use to protect
 * the critical areas of the IPC Link Driver.  The two classes of code that need
 * protection are operations involving the nm_ld_pm_state and operations
 * involving synchronization between the sdma_transmitting flag and the
 * mux_deferred flag.
 */
spinlock_t ild_lock = SPIN_LOCK_UNLOCKED;

/* Define a place to hold the remote receive buffer size */
static unsigned int remMaxRcvSiz = REM_MAX_RCV_SIZ;

static int SDMA_CHANNEL_TRANSMIT = IPC_TX_CHANNEL;
static int SDMA_CHANNEL_RECEIVE  = IPC_RX_CHANNEL;

module_param(SDMA_CHANNEL_TRANSMIT, int, 0600);
module_param(SDMA_CHANNEL_RECEIVE, int, 0600);

static struct INTERFACELINK iflink;
static struct INTERFACEMUX  ifmux;
/*Non cached Buffer Hack*/
#define NON_CACHE_BUFFER
#ifdef NON_CACHE_BUFFER
static char *rbuf;
static dma_addr_t rpaddr;
#endif

static struct sk_buff* receive_commbuff;
static unsigned long   mux_deferred;

static unsigned long nm_ld_pm_state = NM_LD_PM_STATE_NOT_READY;

static struct tasklet_struct write_callback;
static struct tasklet_struct read_callback;

static struct sk_buff_head send_queue;
#define SEND_QUEUE_MAX_SIZE	5
#define SEND_QUEUE_LOW_MARK	((SEND_QUEUE_MAX_SIZE / 2) + 1)
#define SEND_QUEUE_LEN		(skb_queue_len(&send_queue))
#define SEND_QUEUE_ISEMPTY	(skb_queue_empty(&send_queue))
// the reference count for the commbuff is incremented before
// it is added to the send_queue
#define SEND_QUEUE_ADD(buf)	skb_get(buf); \
                                skb_queue_tail(&send_queue, buf)
// after removing the commbuff from the send_queue, its reference
// count will be decremented
#define SEND_QUEUE_REMOVE	(kfree_skb(skb_dequeue(&send_queue)))
#define SEND_QUEUE_GET_HEAD	(skb_peek(&send_queue))

static HW_CTRL_IPC_OPEN_T mu_channel_desc;
static HW_CTRL_IPC_CHANNEL_T *mu_channel_handle;

static unsigned long mu_read_value;
static unsigned long mu_send_value;
#define MU_R_DATA_SIZE  (sizeof(mu_read_value))
#define MU_W_DATA_SIZE  (sizeof(mu_send_value))

static char * const mu_read_buffer = (char *)(&mu_read_value);
static char * const mu_send_buffer = (char *)(&mu_send_value);


static void          MUReadCallback       (HW_CTRL_IPC_READ_STATUS_T*);
static void          MUWriteCallback      (HW_CTRL_IPC_WRITE_STATUS_T*);
static void          MUNotifyCallback     (HW_CTRL_IPC_NOTIFY_STATUS_T*);
static void          MUXTransmitComplete  (unsigned long arg);
static void          MUXReceiveComplete   (unsigned long arg);
static void          SDMAReceiveComplete  (void* arg);
static void          SDMATransmitComplete (void* arg);
static void          SDMATransmit     (void);
static unsigned long SDMAQueue            (void* commbuff);
static unsigned long SDMAInform           (void* param1, void* param2);
static int           PMSuspend            (struct device * dev, u32 state,
                                           u32 level);

static wait_queue_head_t pmeventq;

// This structure contains pointers to the power management callback functions.
static struct device_driver ipc_link_driver = {
   .name    = "ipc_link_driver",
   .bus     = &platform_bus_type,
   .probe   = NULL,
   .suspend = PMSuspend,
   .resume  = NULL,
};

static struct platform_device ipc_link_device = {
   .name = "ipc_link_driver",
   .id   = 0,
};

/*
 * MUReadCallback handles incoming MU data.
 * Note that this function is called in non-interrrupt,
 * tasklet context.
 */
static void MUReadCallback (HW_CTRL_IPC_READ_STATUS_T* read_status)
{
    HW_CTRL_IPC_STATUS_T status;

    DEBUG("%s(nb_bytes = %d) nm_ld_pm_state=%lu\n", __FUNCTION__,
          read_status->nb_bytes, nm_ld_pm_state);

    spin_lock_bh(&ild_lock);

    if (mu_read_value == NM_LD_MSG_SUSPEND_REQ)
    {
        if ((!SEND_QUEUE_ISEMPTY && nm_ld_pm_state == NM_LD_PM_STATE_READY) ||
            (nm_ld_pm_state == NM_LD_PM_STATE_WAKING))
	{
            mu_send_value = NM_LD_MSG_RESUME;
	}
	else
	{
            mu_send_value = NM_LD_MSG_SUSPEND_ACK;
	    if (nm_ld_pm_state == NM_LD_PM_STATE_READY)
	    {
	        nm_ld_pm_state = NM_LD_PM_STATE_SUSPENDED;
	    }
	    else if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDING)
	    {
	       if (!SEND_QUEUE_ISEMPTY)
	       {
		   mu_send_value = NM_LD_MSG_RESUME;
		   nm_ld_pm_state = NM_LD_PM_STATE_WAKING;
	       }
	       else
	       {
		   nm_ld_pm_state = NM_LD_PM_STATE_SUSPENDED;
	       }
	       // make sure interrupts of interest are on
	       mxc_mu_set_ioi_response(IOI_ON);
	       wake_up_interruptible(&pmeventq);
	    }
	    else if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDED)
	    {
	        //this case should not be possible based on the correct
		//operation of the PM IPC Link Driver state machine
		panic("IPC Link Driver received NM_LD_MSG_SUSPEND_REQ message \
                      in wrong state (i.e. NM_LD_PM_STATE_SUSPENDED)\n");
	    }
	}

	status = hw_ctrl_ipc_write(mu_channel_handle, mu_send_buffer, 
			           MU_W_DATA_SIZE);
        if (status != HW_CTRL_IPC_STATUS_OK)
            panic("IPC Link Driver failed to send MU %d response: %d\n",
                   mu_send_value, status);
    }
    else if (mu_read_value == NM_LD_MSG_SUSPEND_ACK)
    {
        if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDING)
	{
	    if (!SEND_QUEUE_ISEMPTY)
	    {
	        mu_send_value = NM_LD_MSG_RESUME;
		nm_ld_pm_state = NM_LD_PM_STATE_WAKING;
	        status = hw_ctrl_ipc_write(mu_channel_handle, mu_send_buffer, 
			                   MU_W_DATA_SIZE);
                if (status != HW_CTRL_IPC_STATUS_OK)
                    panic("IPC Link Driver failed to send MU resume response: %d\n",
                          status);
	    }
	    else
	    {
                nm_ld_pm_state = NM_LD_PM_STATE_SUSPENDED;
	    }
            // make sure interrupts of interest are on
	    mxc_mu_set_ioi_response(IOI_ON);
	    wake_up_interruptible(&pmeventq);
	}
	else if (nm_ld_pm_state != NM_LD_PM_STATE_SUSPENDED)
	{
	    //this case should not be possible based on the correct
            //operation of the PM IPC Link Driver state machine
            panic("IPC Link Driver received SUSPEND_ACK message in wrong \
                  state: %d\n", nm_ld_pm_state);
	}
    }
    else if (mu_read_value == NM_LD_MSG_RESUME)
    {
        if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDING)
	{
	    // make sure interrupts of interest are on
	    mxc_mu_set_ioi_response(IOI_ON);
	    wake_up_interruptible(&pmeventq);
	    nm_ld_pm_state = NM_LD_PM_STATE_READY;
	    if (!SEND_QUEUE_ISEMPTY)
	    {
                spin_unlock_bh(&ild_lock);
                SDMATransmit();
                spin_lock_bh(&ild_lock);
	    }
	}
	else if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDED)
	{
            nm_ld_pm_state = NM_LD_PM_STATE_READY;
	}
    }
    else
    {
        panic("IPC Link Driver received unknown MU message: %d\n",
              (int)mu_read_value);
    }

    // set up the next MU read
    status = hw_ctrl_ipc_read(mu_channel_handle, mu_read_buffer,
                              MU_R_DATA_SIZE);

    if (status != HW_CTRL_IPC_STATUS_OK)
        panic("IPC Link Driver failed to set up an MU read: %d\n", status);

    spin_unlock_bh(&ild_lock);
}

/*
 * MUWriteCallback is used to find out when the MU write has been read by
 * the driver on the BP side.
 * Note that this function is called in non-interrrupt, tasklet context.
 */
static void MUWriteCallback (HW_CTRL_IPC_WRITE_STATUS_T* write_status)
{

    DEBUG("%s(nb_bytes = %d) nm_ld_pm_state=%lu\n", __FUNCTION__,
          write_status->nb_bytes, nm_ld_pm_state);

    spin_lock_bh(&ild_lock);

    // for now, we really only care about the completion
    // for the RESUME message used to wake up the BP
    if (nm_ld_pm_state == NM_LD_PM_STATE_WAKING)
    {
	nm_ld_pm_state = NM_LD_PM_STATE_READY;
        if (!SEND_QUEUE_ISEMPTY)
	{
            spin_unlock_bh(&ild_lock);
	    SDMATransmit();
	}
	else
	{
	    spin_unlock_bh(&ild_lock);
	}
    }
    else
    {
        spin_unlock_bh(&ild_lock);
    }
}

/*
 * MUNotifyCallback informs us of any error on the MU
 */
static void MUNotifyCallback (HW_CTRL_IPC_NOTIFY_STATUS_T* status)
{
    DEBUG("%s(0x%p)\n", __FUNCTION__, status);

    printk("An error occurred on the MU channel: %d, %d\n", status->status,
           status->channel->channel_nb);
}

/* PMSuspend is called by the local power management code to ask us
 * to go to sleep.  We may be able to answer the question immediately
 * or we may have to negotiate with the BP.  If negotiation is needed
 * then we'll block the caller while we check.
 */
static int PMSuspend (struct device * dev, u32 state, u32 level)
{
    HW_CTRL_IPC_STATUS_T status;

    DEBUG("%s(0x%p, 0x%x, 0x%x) nm_ld_pm_state=%lu\n", __FUNCTION__, dev, state,
          level, nm_ld_pm_state);

    spin_lock_bh(&ild_lock);

    if (nm_ld_pm_state == NM_LD_PM_STATE_READY)
    {
        if (!SEND_QUEUE_ISEMPTY)
	{
            spin_unlock_bh(&ild_lock);
	    return -EBUSY;
	}
	else
	{
            // next, see if we need to ask the BP

            nm_ld_pm_state = NM_LD_PM_STATE_SUSPENDING;
            // make sure interrupts of interest are off so that the expected
            // SUSPEND_ACK message isn't interpreted as something that should
            // keep the system awake
            mxc_mu_set_ioi_response(IOI_OFF);
            mu_send_value = NM_LD_MSG_SUSPEND_REQ;

            status = hw_ctrl_ipc_write(mu_channel_handle, mu_send_buffer,
                                       MU_W_DATA_SIZE);

            if (status != HW_CTRL_IPC_STATUS_OK)
                panic("%d IPC link driver failed to ask BP for sleep\n", status);

            spin_unlock_bh(&ild_lock);

            // block until we get a response from the BP
            wait_event_interruptible
              (pmeventq, (nm_ld_pm_state != NM_LD_PM_STATE_SUSPENDING));

	}
    }
    else
    {
        // in the future, possibly consider if blocking the PM makes sense
	// here or not
        spin_unlock_bh(&ild_lock);
    }

    // return a value appropriate to whether the BP is suspended or not
    // if a signal occurred then we're still in SUSPENDING and we'll stay there
    // a low probability race condition exists here, since nm_ld_pm_state could
    // change before we return from this function
    if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDED)
    {
        status = mxc_mu_dsp_pmode_status();
        DEBUG("%s: status register: 0x%x\n", __FUNCTION__, status);
// I don't like the U build.  
//	status = mxc_mu_dsp_pmode_status();
//	printk ("link driver suspending, dsp status: %d\n", status);
       return 0;
    }
    else
    {
        return -EBUSY;
    }
}

/*
 * MUXTransmitComplete is triggered by the write callback and
 * runs in a non-interrupt, tasklet context. This function frees
 * the transmitted buffer and lets the NetMUX know that it is safe
 * to send again (if needed).
 *
 * Params:
 * arg -- not used
 */
static void MUXTransmitComplete (unsigned long arg)
{
    DEBUG("%s(%lu)\n", __FUNCTION__, arg);

    spin_lock_bh(&ild_lock);
    SEND_QUEUE_REMOVE;
    if (mux_deferred)
    {
        if (SEND_QUEUE_LEN <= SEND_QUEUE_LOW_MARK)
	{
            mux_deferred = 0;
	    spin_unlock_bh(&ild_lock);
	    ifmux.MUXInform((void*)LDP_INFORM_RECOVERED, ifmux.id);
	}
	else
	{
            spin_unlock_bh(&ild_lock);
	}
    }
    else
    {
        spin_unlock_bh(&ild_lock);
    }

    // we are guaranteed that currently there is no SDMA transfer in
    // progress
    if (!SEND_QUEUE_ISEMPTY)
    {
        SDMATransmit();
    }
}

/*
 * MUXReceiveComplete is triggered by the read callback and runs
 * in a non-interrupt, tasklet context. This function passes the
 * received data up to the NetMUX, allocates a new receive buffer,
 * and starts the next SDMA read.
 *
 * Params:
 * arg -- not used.
 */
static void MUXReceiveComplete (unsigned long arg)
{
    dma_request_t   dmarequest;
    int             result;

    DEBUG("%s(%lu)\n", __FUNCTION__, arg);

    mxc_dma_get_config(SDMA_CHANNEL_RECEIVE, &dmarequest, 0);

    if(dmarequest.bd_error || (dmarequest.count > LOC_MAX_RCV_SIZ))
    {
        panic("Error in SDMA when Linkdriver attempted to receive data:\n"
              "dmarequest.sourceAddr: %p\ndmarequest.destAddr: %p\n"
              "dmarequest.count: %lu\ndmarequest.bd_done: %d\n"
              "dmarequest.bd_cont: %d\ndmarequest.bd_error: %d\n", 
              dmarequest.sourceAddr, dmarequest.destAddr, dmarequest.count, 
              dmarequest.bd_done, dmarequest.bd_cont, dmarequest.bd_error);
    }
    else
    {
        memcpy(receive_commbuff->data,rbuf, dmarequest.count );

        skb_put(receive_commbuff, dmarequest.count);
    }

    ifmux.MUXReceive((void*)receive_commbuff, ifmux.id);

    // note that this kfree call only decrements our reference
    dev_kfree_skb(receive_commbuff);


    receive_commbuff = dev_alloc_skb(LOC_MAX_RCV_SIZ);
 
   if (!receive_commbuff)
        panic("IPC Link Driver failed to obtain receive skbuff\n");

#ifdef NON_CACHE_BUFFER
//     dma_free_coherent(NULL, LOC_MAX_RCV_SIZ, rbuf, rpaddr);
//     rbuf = dma_alloc_coherent(NULL, LOC_MAX_RCV_SIZ, &rpaddr, GFP_DMA);
//     receive_commbuff->data=rbuf;
//    memcpy( rbuf, receive_commbuff->data, LOC_MAX_RCV_SIZ);
    
#endif

    memset(&dmarequest, 0, sizeof(dma_request_t));
#ifdef NON_CACHE_BUFFER
    dmarequest.destAddr = rpaddr;
#endif

#ifndef NON_CACHE_BUFFER
    dmarequest.destAddr = (char*)sdma_virt_to_phys(receive_commbuff->data);
#endif

   dmarequest.count    = LOC_MAX_RCV_SIZ;

    // invalidate the cache before starting the next read
    // note that performance may be better if DMA_FROM_DEVICE is used but
    // that change will require taking care of possible cache line crossings
    #ifndef NON_CACHE_BUFFER
    consistent_sync(receive_commbuff->data, dmarequest.count,
                    DMA_BIDIRECTIONAL);
    #endif

    result = mxc_dma_set_config(SDMA_CHANNEL_RECEIVE, &dmarequest, 0);

    if (result != 0)
    {
        dev_kfree_skb(receive_commbuff);
        panic("IPC link driver failed to setup SDMA receive request: %d\n",
              result);
    }

    mxc_dma_start(SDMA_CHANNEL_RECEIVE);
}

/*
 * SDMAReceiveComplete is the read callback for the sdma.
 * All we do here is let the PM code know that an interesting
 * interrupt has occurred and jump out of interrupt context to
 * let the bottom half do the rest of the work.
 *
 * Params:
 * arg -- the channel number
 */
static void SDMAReceiveComplete (void* arg)
{
    DEBUG("%s(0x%p)\n", __FUNCTION__, arg);

#ifdef CONFIG_MOT_FEAT_PM
    // let PM know that an interesting interrupt has occurred
    mpm_handle_ioi();
#endif

    tasklet_schedule(&read_callback);
}

/*
 * SDMATransmitComplete is the write callback for the sdma.
 * All we do here is jump out of interrupt context and let
 * the bottom half do all the work.
 *
 * Params:
 * arg -- the channel
 */
static void SDMATransmitComplete (void* arg)
{
    DEBUG("%s(0x%p)\n", __FUNCTION__, arg);

    tasklet_schedule(&write_callback);
}

/*
 * SDMATransmit is called whenever data needs to be pushed to
 * the SDMA, from the send_queue. The ild_lock must not be held
 * when invoking this function. It is assumed that this function
 * will only be invoked when the send_queue is not empty.
 */
static void SDMATransmit (void)
{
    struct sk_buff* transmit_commbuff = NULL;
    dma_request_t   dmarequest;
    int             result;

    DEBUG("%s\n", __FUNCTION__);

    spin_lock_bh(&ild_lock);
    // the head element is _not_ removed from the list and the
    // ref count is unchanged
    transmit_commbuff = SEND_QUEUE_GET_HEAD;
    spin_unlock_bh(&ild_lock);

    memset(&dmarequest, 0, sizeof(dma_request_t));

    dmarequest.sourceAddr = (char*)sdma_virt_to_phys(transmit_commbuff->data);
    dmarequest.count      = transmit_commbuff->len;

    // flush the cache before starting the write
    // note that performance may be better if DMA_TO_DEVICE is used but
    // that change will require taking care of possible cache line crossings
    consistent_sync(transmit_commbuff->data, dmarequest.count,
                    DMA_BIDIRECTIONAL);

    result = mxc_dma_set_config(SDMA_CHANNEL_TRANSMIT, &dmarequest, 0);

    LOG("SDMATransmit()-->\n");
    LOGSKBUFF(transmit_commbuff);

    if (result != 0)
        panic("IPC link driver failed to setup SDMA transmit request: %d\n",
              result);

    mxc_dma_start(SDMA_CHANNEL_TRANSMIT);
}

/*
 * SDMAQueue is called by the NetMUX to send data. If the send_queue is not
 * full, data will be added to it, transmission will be started (if the SDMA
 * was free), and ERROR_NONE will be returned to the NetMUX; otherwise the
 * NetMUX will be deferred and asked to contact us later.
 *
 * Params:
 * param -- the data to be sent.
 */
static unsigned long SDMAQueue (void* param)
{
    HW_CTRL_IPC_STATUS_T status;

    DEBUG("%s(0x%p) nm_ld_pm_state=%lu\n", __FUNCTION__, param, nm_ld_pm_state);

    spin_lock_bh(&ild_lock);

    /* tell NetMUX to wait since the queue is full */
    if (SEND_QUEUE_LEN == SEND_QUEUE_MAX_SIZE)
    {
        mux_deferred = 1;
        spin_unlock_bh(&ild_lock);
        return LDP_ERROR_RECOVERABLE;
    }

    /* if we're negotiating sleep or waking up the BP, add to the queue */
    else if ((nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDING) ||
             (nm_ld_pm_state == NM_LD_PM_STATE_WAKING))
    {
        SEND_QUEUE_ADD((struct sk_buff*)param);
        spin_unlock_bh(&ild_lock);
        return LDP_ERROR_NONE;
    }

    /* if we're already sleeping, add to queue and wake up the BP */
    else if (nm_ld_pm_state == NM_LD_PM_STATE_SUSPENDED)
    {
        SEND_QUEUE_ADD((struct sk_buff*)param);

        mu_send_value = NM_LD_MSG_RESUME;
        nm_ld_pm_state = NM_LD_PM_STATE_WAKING;

        status = hw_ctrl_ipc_write(mu_channel_handle, mu_send_buffer,
                                   MU_W_DATA_SIZE);

        if (status != HW_CTRL_IPC_STATUS_OK)
            panic("IPC Link Driver failed to wake up BP: %d\n", status);

        spin_unlock_bh(&ild_lock);

        return LDP_ERROR_NONE;
    }

    // here we are guaranteed that the queue is not full and the state 
    // is NM_LD_PM_STATE_READY

    SEND_QUEUE_ADD((struct sk_buff*)param);

    // if the queue was empty before our addition above, then
    // the SDMA was free and therefore we can begin the next
    // transmission
    if (SEND_QUEUE_LEN - 1 == 0)
    {
        spin_unlock_bh(&ild_lock);
        /* we're going to send a packet */
        SDMATransmit();
    }
    else
    {
        spin_unlock_bh(&ild_lock);
    }

    return LDP_ERROR_NONE;
}

/*
 * SDMAInform is not used currently. Theoretically the NetMUX can use
 * this function to communicate with the SDMA.
 *
 * Params:
 * param1 -- type?
 * param2 -- data?
 */
static unsigned long SDMAInform (void* param1, void* param2)
{
    DEBUG("%s(0x%p, 0x%p)\n", __FUNCTION__, param1, param2);

    switch((unsigned long)param1)
    {
        case LDP_INFORM_SHUTDOWN:
        {
        }break;

        default:break;
    }

    return LDP_ERROR_NONE;
}

/*
 * init_module initializes everything.
 */
int init_module (void)
{
    HW_CTRL_IPC_STATUS_T status;
    dma_request_t      dmarequest;
    dma_channel_params dmaparam;
    int                sendch;
    int                recvch;
    int                result;

    DEBUG("%s()\n", __FUNCTION__);

    DEBUG("SDMA_CHANNEL_TRANSMIT=%d SDMA_CHANNEL_RECEIVE=%d\n",
          SDMA_CHANNEL_TRANSMIT, SDMA_CHANNEL_RECEIVE);

    // pretend that we're a real driver by calling functions to register our
    // suspend function to be called by the power management subsystem
    if (((result = driver_register(&ipc_link_driver)) != 0) ||
        ((result = platform_device_register(&ipc_link_device)) != 0))
    {
        goto failed;
    }

    tasklet_init(&write_callback, &MUXTransmitComplete, 0);
    tasklet_init(&read_callback, &MUXReceiveComplete, 0);
    skb_queue_head_init(&send_queue);

    init_waitqueue_head(&pmeventq);

    mu_channel_desc.type            = HW_CTRL_IPC_SHORT_MSG;
    mu_channel_desc.index           = MU_CHANNEL;
    mu_channel_desc.read_callback   = &MUReadCallback;
    mu_channel_desc.write_callback  = &MUWriteCallback;
    mu_channel_desc.notify_callback = &MUNotifyCallback;

    // set the PM state before opening the MU channel
    nm_ld_pm_state = NM_LD_PM_STATE_READY;

    mu_channel_handle = hw_ctrl_ipc_open(&mu_channel_desc);
    if (mu_channel_handle == NULL)
    {
        printk("IPC link driver failed to obtain MU channel\n");

        goto unregDev;
    }

    sendch = SDMA_CHANNEL_TRANSMIT;
    recvch = SDMA_CHANNEL_RECEIVE;

    result = mxc_request_dma(&sendch, "ipc_tx");
    if (result != 0)
    {
        printk("IPC link driver failed to obtain SDMA transmit channel: %d\n",
               result);

        goto closeMUChan;
    }

    result = mxc_request_dma(&recvch, "ipc_rx");
    if (result != 0)
    {
        printk("IPC link driver failed to obtain SDMA receive channel: %d\n",
               result);

        goto freeXmitChan;
    }

    dmaparam.watermark_level = 0;
    dmaparam.per_address     = 0;
    dmaparam.peripheral_type = DSP;
    dmaparam.transfer_type   = emi_2_dsp;
    dmaparam.event_id        = 0;
    dmaparam.event_id2       = 0;
    dmaparam.bd_number       = 1;
    dmaparam.callback        = &SDMATransmitComplete;
    dmaparam.arg             = (void*)SDMA_CHANNEL_TRANSMIT;
    dmaparam.word_size       = TRANSFER_8BIT;

    result = mxc_dma_setup_channel(SDMA_CHANNEL_TRANSMIT, &dmaparam);

    if (result != 0)
    {
        printk("IPC link driver failed to setup SDMA transmit channel: %d\n",
               result);

        goto freeRecvChan;
    }

    dmaparam.transfer_type = dsp_2_emi;
    dmaparam.callback        = &SDMAReceiveComplete;
    dmaparam.arg             = (void*)SDMA_CHANNEL_RECEIVE;

    result = mxc_dma_setup_channel(SDMA_CHANNEL_RECEIVE, &dmaparam);

    if (result != 0)
    {
        printk("IPC link driver failed to setup SDMA receive channel: %d\n",
               result);

        goto freeRecvChan;
    }

    iflink.LinkSend         = &SDMAQueue;
    iflink.LinkInform       = &SDMAInform;
    iflink.localMaxRcvSize  = LOC_MAX_RCV_SIZ;
    iflink.remoteMaxRcvSize = remMaxRcvSiz;

    mux_deferred      = 0;

    result = RegisterMUXLink(&iflink, &ifmux);
    if (result != LDP_ERROR_NONE)
    {
        printk("IPC link driver failed to register with NetMUX: %d\n", result);
        goto freeRecvChan;
    }

    status = hw_ctrl_ipc_read(mu_channel_handle, mu_read_buffer,
                              MU_R_DATA_SIZE);
    if (status != HW_CTRL_IPC_STATUS_OK)
    {
        printk("IPC link driver failed to post the MU read buffer: %d\n",
               status);
        goto unregMux;
    }

    receive_commbuff = dev_alloc_skb(LOC_MAX_RCV_SIZ);
    if (!receive_commbuff)
    {
        printk("IPC link driver failed to allocate receive buffer\n");
        goto unregMux;
    }

    memset(&dmarequest, 0, sizeof(dma_request_t));

#ifdef NON_CACHE_BUFFER
     rbuf = dma_alloc_coherent(NULL, LOC_MAX_RCV_SIZ, &rpaddr, GFP_DMA);
//     receive_commbuff->data=rbuf;
#endif

    memset(&dmarequest, 0, sizeof(dma_request_t));
#ifndef NON_CACHE_BUFFER
    dmarequest.destAddr = (char*)sdma_virt_to_phys(receive_commbuff->data);
#endif

#ifdef NON_CACHE_BUFFER
    dmarequest.destAddr = rpaddr;
#endif

    dmarequest.count    = LOC_MAX_RCV_SIZ;

    // invalidate the cache before starting the first read
    // note that this should use DMA_FROM_DEVICE
    // but that doesn't seem to be working yet
#ifndef  NON_CACHE_BUFFER
    consistent_sync(receive_commbuff->data, dmarequest.count,
                    DMA_BIDIRECTIONAL);
#endif

    result = mxc_dma_set_config(SDMA_CHANNEL_RECEIVE, &dmarequest, 0);

    if (result != 0)
    {
        printk("IPC link driver failed to post receive buffer: %d\n", result);

        goto freeDataBuf;
    }

    mxc_dma_start(SDMA_CHANNEL_RECEIVE);

    /* return good status */
    return 0;

freeDataBuf:
    dev_kfree_skb(receive_commbuff);
unregMux:
    UnregisterMUXLink(ifmux.id);
freeRecvChan:
    mxc_free_dma(SDMA_CHANNEL_RECEIVE);
freeXmitChan:
    mxc_free_dma(SDMA_CHANNEL_TRANSMIT);
closeMUChan:
    hw_ctrl_ipc_close(mu_channel_handle);
unregDev:
    nm_ld_pm_state = NM_LD_PM_STATE_NOT_READY;
    platform_device_unregister(&ipc_link_device);
    driver_unregister(&ipc_link_driver);

failed:
    /* return a failure status */
    return result;
}


/*
 * cleanup_module cleans up everything.
 */
void cleanup_module (void)
{
    DEBUG("%s()\n", __FUNCTION__);

    UnregisterMUXLink(ifmux.id);

    hw_ctrl_ipc_close(mu_channel_handle);
    nm_ld_pm_state = NM_LD_PM_STATE_NOT_READY;

    mxc_dma_stop(SDMA_CHANNEL_TRANSMIT);
    mxc_dma_stop(SDMA_CHANNEL_RECEIVE);

    mxc_free_dma(SDMA_CHANNEL_TRANSMIT);
    mxc_free_dma(SDMA_CHANNEL_RECEIVE);

    skb_queue_purge(&send_queue);

    if (receive_commbuff)
        dev_kfree_skb(receive_commbuff);

    platform_device_unregister(&ipc_link_device);
    driver_unregister(&ipc_link_driver);
}
