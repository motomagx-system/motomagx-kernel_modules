###############################################################################
# IPC Link Driver Makefile                                                    #
#                                                                             #
# Copyright (c) Motorola 2006-2007                                            #
#                                                                             #
# Redistribution and use in source and binary forms, with or without          #
# modification, are permitted provided that the following conditions are met: #
#                                                                             #
# o Redistributions of source code must retain the above copyright notice,    #
#   this list of conditions and the following disclaimer.                     #
# o Redistributions in binary form must reproduce the above copyright notice, #
#   this list of conditions and the following disclaimer in the documentation #
#   and/or other materials provided with the distribution.                    #
# o Neither the name of Motorola nor the names of its contributors may be     #
#   used to endorse or promote products derived from this software without    #
#   specific prior written permission.                                        #
#                                                                             #
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" #
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE   #
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE  #
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE    #
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR         #
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF        #
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS    #
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN     #
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)     #
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE  #
# POSSIBILITY OF SUCH DAMAGE.                                                 #
#                                                                             #
###############################################################################
#   DATE        OWNER       COMMENT                                           #
#   ----------  ----------  ------------------------------------------------  #
#   2006/09/28  Motorola    Initial version                                   #
#   2007/03/08  Motorola    Removed TOOLPREFIX information                    #
#   2007/06/21  Motorola    Enable GCOV                                       #
###############################################################################

# Source in the top-level make options for MOT build environment
include $(BOOTSTRAP)

ifeq($(TEST_COVERAGE),1)
GCOV_FLAGS      = -fprofile-arcs -ftest-coverage
EXTRA_CFLAGS += $(GCOV_FLAGS)
endif

#For now do not build anything for x86, this should probably be moved to prodconf
ifneq($(HW_ARCH),i686)

KERNEL_MAKE = cd $(STD_DIRPATH)/linux_build && $(MAKE) ARCH=$(ARCH) M=$(PWD)/sdma INSTALL_MOD_PATH=$(BUILDTOP) STD_INCPATH=$(STD_INCPATH) DEBUG=$(DEBUG) LOG=$(LOG) EXTRA_CFLAGS="$(EXTRA_CFLAGS)"

.PHONY: impl api_build distclean 
 
api_build: $(PROPFILES)

ifneq($(FEAT_NETMUX),0)
impl:
	$(KERNEL_MAKE) modules
	$(KERNEL_MAKE) modules_install
endif

distclean: clean
	$(KERNEL_MAKE) clean

endif # ifneq($(HW_ARCH),i686)
