/*********************************************************
**  File: tcmd.c - Test command support functions for AP datalogger
**
**        Copyright 2006, 2007 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**
*********************************************************/

/*********************************************************************
 * Revision History
 *  #   Date        Author          Comment
 * === ==========  ==============  ===================================
 *   1 10/03/2006   Motorola        Initial version
 *   2 10/11/2006   Motorola        Added support for direct IP dumping
 *   3 10/24/2006   Motorola        Fixed kernel panic retrieval issue on large
 *                                  page NAND flash products
 *   4 11/01/2006   Motorola        Fixed kpanic retrieval response endianness
 *   5 12/06/2006   Motorola        Fixed missing fput and sys_close calls
 *   6 01/24/2007   Motorola        Fixed SUAPI log port enable command
 *   7 03/25/2007   Motorola        Modify for libaplog/aplogd support
 *
 *********************************************************************/

#include "common.h"
#include "tcmd.h"

/************************************************
 * Externs declared in aplogger-core.c
 ************************************************/
extern int enable_tcmd;                     // Whether to enable TCMD
extern atomic_t tcmd_inuse;                 // Atomic for tcmd execution
extern wait_queue_head_t logger_tcmd_wait;  // TCMD waitqueue
extern struct completion suapi_thread_complete;// if SUAPI thread is complete
extern char suapi_thread_run;               // Whether SUAPI thread should run
extern char logger_tcmd_enable_logging;     // logging dis/en-abled
extern char logger_tcmd_fork_exec;          // Fork and exec logging control

/************************************************
 * Local global variable definitions
 ************************************************/

/* Command function table */
static struct logger_tcmd_table_t logger_tcmd_table[] = {
    {TCMD_LOGGING_START, logger_tcmd_logging_start},
    {TCMD_LOGGING_STOP, logger_tcmd_logging_stop},
    {TCMD_SUAPI_PORT_ENABLE, logger_tcmd_suapi_port_enable},
    {TCMD_SUAPI_PORT_RETRIEVE, logger_tcmd_suapi_port_retrieve},
    {TCMD_PID_TABLE, logger_tcmd_pid_table},
    {TCMD_FORK_EXEC_START, logger_tcmd_fork_exec_start},
    {TCMD_FORK_EXEC_STOP, logger_tcmd_fork_exec_stop},
    {TCMD_NET_LOGGING_START, logger_tcmd_net_logging_start},
    {TCMD_NET_LOGGING_STOP, logger_tcmd_net_logging_stop},
    {TCMD_FORCE_PANIC, logger_tcmd_force_panic},
    {TCMD_EOL, NULL},
};
static char * logger_tcmd_request = NULL;           // Ptr to request
static char * logger_tcmd_response = NULL;          // Ptr to response
static char logger_tcmd_request_recvd = FALSE;      // If request has been made
static char logger_tcmd_response_ready = FALSE;     // If response is ready
static int logger_tcmd_request_size = 0;            // strlen of request
static int logger_tcmd_response_size = 0;           // strlen of response

static LIST_HEAD(dir_file_list);

/* logger_tcmd_write()
 *
 * Description: This function will be called any time a write is done to the
 * TCMD character device. This will be the basis for TCMD requests
 *
 * @file_ptr: pointer to the TCMD file struct
 * @buffer: pointer to the TCMD command
 * @count: length of the buffer (TCMD request)
 * @offset: how far from the start to start writing (unused)
 *
 * Return: (ssize_t) number of bytes written / error
 */
ssize_t logger_tcmd_write(struct file *file_ptr,
        const char * __user buffer, size_t count, loff_t * offset)
{

    /* Check for valid arguments */
    if (file_ptr == NULL || buffer == NULL)
    {
        printk(KERN_ERR "APLOGGER WARNING: Problem with tcmd_write\n");
        return -EINVAL;
    }

    /* Try to lock TCMD mutex */
    DPRINTS("Trying to lock TCMD mutex\n");
    atomic_inc(&tcmd_inuse);
    if (atomic_read(&tcmd_inuse) > 1)
    {
        atomic_dec(&tcmd_inuse);
        DPRINTT("APLOGGER ERROR: New TCMD before original has completed\n");
        return -EAGAIN;
    }

    /* Malloc and copy space for the request; Reset response */
    logger_tcmd_request_size = count;
    logger_tcmd_request = (char *)kmalloc(logger_tcmd_request_size + 1,
            GFP_KERNEL);
    copy_from_user(logger_tcmd_request, buffer, count);
    logger_tcmd_request[count] = '\0';
    logger_tcmd_request_recvd = TRUE;
    logger_tcmd_response_ready = FALSE;
    logger_tcmd_response_size = 0;
    logger_tcmd_response = NULL;
    
    /* Start the parsing / action thread */
    kernel_thread(logger_tcmd_action, NULL, CLONE_FS | CLONE_FILES);
    
    return (ssize_t)count;
}


/* logger_tcmd_read()
 *
 * Description: This function will be called any time a read is performed on
 * the TCMD character device. This will be the basis for returning data based
 * on the last command.
 *
 * @file_ptr: pointer to the TCMD file struct
 * @buffer: where to write TCMD response (pointer)
 * @count: max size of the buffer
 * @offset: how far to start reading from the start
 *
 * Returns: (ssize_t) number of bytes read / error code
 */
ssize_t logger_tcmd_read(struct file *file_ptr, char * __user buffer,
        size_t count, loff_t * offset)
{
    unsigned int copy_len = -1;      /* Length to copy / returns was copied */
    
    if (file_ptr == NULL || buffer == NULL || count == 0) 
        /* Check for valid args */
    {
        printk(KERN_ERR "APLOGGER WARNING: Problem with tcmd_read\n");
        return -EINVAL;
    }

    if (!logger_tcmd_request_recvd) /* Check that a request has been sent */
    {
        DPRINTT("Warning: No request received before read\n");
        return -EIO;
    }
    
    /* Sleep until a response is ready */
    if (!logger_tcmd_response_ready)
    {
        DPRINTT("Response is not ready\n");
        if (file_ptr->f_flags & O_NONBLOCK)
            return -EAGAIN;
        else
            wait_event_interruptible_timeout(logger_tcmd_wait,
                    logger_tcmd_response_ready, LOGGER_TCMD_MAX_WAIT * HZ);
        
        if (!logger_tcmd_response_ready)
            return -EBUSY;
    }

    /* Check the start, length, and offset so valid data is sent */
    if (*offset > logger_tcmd_response_size)
    {
        DPRINT("ERROR: Offset longer than what we have\n");
        return -EINVAL;
    }
    else if (count < logger_tcmd_response_size - *offset)
    {
        DPRINT("WARNING: Count less than response length\n");
        copy_len = count;
    }
    else
    {
        copy_len = logger_tcmd_response_size - *offset;
        DPRINT("Copy length: %d\n", copy_len);
    }
    
    /* Check for validity and copy data */
    if (copy_len > 0 && logger_tcmd_response != NULL)
    {
        if (copy_to_user(buffer, logger_tcmd_response + *offset, copy_len))
        {
            /* Returned non zero; Could not copy for some reason */
            DPRINTT("TCMD Could not copy response\n");
            copy_len = 0;
        }
    }
    else
    {
        DPRINTT("TCMD Response NOT sent: copy_len = %d; ltr 0x%X\n",
                copy_len, (unsigned int) logger_tcmd_response);
        copy_len = 0;
    }

    /* Move offset ahead */
    *offset += copy_len;
    
    /* If the complete response has been sent */
    if (*offset >= logger_tcmd_response_size || copy_len == 0)
    {
        /* Free the no longer needed request and response */
        if (logger_tcmd_request != NULL)
            kfree(logger_tcmd_request);
        if (logger_tcmd_response != NULL)
            kfree(logger_tcmd_response);
        DPRINTT("Request and response freed\n");
        
        /* Reset and prepare for a new request */
        logger_tcmd_response_ready = FALSE;
        logger_tcmd_request_recvd = FALSE;
        logger_tcmd_request_size = 0;
        logger_tcmd_response_size = 0;
        logger_tcmd_request = NULL;
        logger_tcmd_response = NULL;
        atomic_dec(&tcmd_inuse);
        DPRINTS("Unlocked TCMD mutex\n");
    }
    else
    {
        DPRINTT("Complete TCMD response not read; Holding TCMD lock\n");
    }

    return (ssize_t)copy_len;
}

/* logger_tcmd_poll()
 *
 * Description: This funciton will return an unsigned long bitwise 'OR'ed with
 * 0x0001 if the response is ready, or 0x0004 if it is ready for a command
 *
 * @file_ptr: pointer to log file device
 * @poll_table: pointer to the things request in poll
 * 
 * Returns: (unsigned long) bits to signify state of TCMD
 */
unsigned int logger_tcmd_poll(struct file * file_ptr,
        struct poll_table_struct * poll_table)
{
    unsigned int ret_val = 0;
    
    DPRINTT("ENTER POLL\n");
    if (logger_tcmd_request_recvd)
    {
        DPRINTT("TCMD command in progress\n");
        
        DPRINTT("Waiting for TCMD response\n");
        poll_wait(file_ptr, &logger_tcmd_wait, poll_table);
            
        DPRINTT("TCMD response ready for read\n");
        ret_val = ret_val | POLLIN;
    }
    else
    {
        DPRINTT("TCMD ready for command\n");
        ret_val = ret_val | POLLOUT;
    }

    DPRINTT("Poll returning %d\n", ret_val);

    return ret_val;
}

/* logger_tcmd_response_builder()
 *
 * Description: This function mallocs the space for, then builds the command
 * response and places it into logger_tcmd_response
 *
 * NOTE: This function does NOT set logger_tcmd_response_ready to TRUE;
 * This function will convert the length of the response to net-order, but
 * the response data must be pre-converted
 *
 * @command_code: one byte command code
 * @result_code: one byte response code
 * @data_len: length (in bytes) of the response data
 * @data_ptr: pointer to the response data
 *
 * Return: (int) 0 on success, -1 on failure
 */
static int logger_tcmd_response_builder(char command_code, char response_code,
        short unsigned int data_len, char * data_ptr)
{
    char local_command_code = command_code;
    char local_response_code = response_code;
    short unsigned int local_data_len = data_len;
    
    /* Calculate and malloc the appropriate size response */
    logger_tcmd_response_size = (RESULT_COMMAND_SIZE + RESULT_CODE_SIZE +
            RESULT_LENGTH_SIZE + data_len);
    DPRINTT("DEBUG: Response size = %d\n", logger_tcmd_response_size);
    logger_tcmd_response = (char *) kmalloc(logger_tcmd_response_size,
            GFP_KERNEL);
    if (logger_tcmd_response == NULL)
    {
        DPRINT("ERROR: Could not allocate space for a TCMD response\n");
        return -1;
    }

    /* Begin copying the response data */
    DPRINTT("DEBUG: Beginning response copy\n");
    memcpy(logger_tcmd_response + RESULT_COMMAND_OFFSET, &local_command_code,
            RESULT_COMMAND_SIZE);
    memcpy(logger_tcmd_response + RESULT_CODE_OFFSET, &local_response_code,
            RESULT_CODE_SIZE);
    local_data_len = htons(data_len);
    memcpy(logger_tcmd_response + RESULT_LENGTH_OFFSET, &local_data_len,
            RESULT_LENGTH_SIZE);
    if (data_len > 0)
    {
        memcpy(logger_tcmd_response + RESULT_DATA_OFFSET, data_ptr, data_len);
    }
    else
    {
        DPRINTT("DEBUG: No extra data to be copied to result\n");
    }
    
    return 0;
}

/* logger_tcmd_action()
 *
 * Description: This function is called as a new thread from the
 * logger_tcmd_write() function. It parses the received command, and takes the
 * appropriate command action
 *
 * @data_in: unused; required to run as a thread
 * 
 * Returns: (int) Always 0
 */
static int logger_tcmd_action(void * data_in)
{
    char command_byte = 0;                      //Command code: one byte
    short unsigned int command_data_len = 0;    //Command data length: two bytes
    char * command_data = NULL;                 //Pointer to command data
    int i = 0;                                  //Array index
    
    /* Check that request has been set */
    if (logger_tcmd_request == NULL)
    {
        DPRINT("No request received for action\n");
        return 0;
    }

    /* Copy in command code */
    memcpy(&command_byte,
            logger_tcmd_request + REQUEST_COMMAND_OFFSET, REQUEST_COMMAND_SIZE);
    /* Copy in data length */
    memcpy(&command_data_len,
            logger_tcmd_request + REQUEST_LENGTH_OFFSET, REQUEST_LENGTH_SIZE);
    /* Convert the length to host endian */
    DPRINTT("Data length before conversion: %d\n", command_data_len);
    command_data_len = ntohs(command_data_len);
    DPRINTT("Data length after conversion: %d\n", command_data_len);
    /* Point command data to the data in the request */
    command_data = logger_tcmd_request + REQUEST_DATA_OFFSET;
    
    /* Itterate through the list of commands looking for the requested one */
    while (logger_tcmd_table[i].code != TCMD_EOL)
    {
       if (command_byte == logger_tcmd_table[i].code)
       {
           (*logger_tcmd_table[i].handler)(command_data_len, command_data);
           break;
       } 
       i++;
    }

    /* If no command with that code was found or no response was built in the
     * command */
    if (logger_tcmd_response == NULL)
    {
        logger_tcmd_response_builder(command_byte, RESULT_INVALID_MESSAGE_TYPE,
                0, NULL);
    }

    /* Alert logger_tcmd_read, if a read is blocked, that a response is ready */
    logger_tcmd_response_ready = TRUE;
    wake_up_interruptible(&logger_tcmd_wait);
    
    return 0;
}


/* logger_tcmd_logging_start()
 *
 * Description: This function immediately enables logging, and updates the
 * config file to enable logging at the next logger startup
 *
 * @len: currently unused
 * @data: currently unused
 *
 * Return: None
 */
static void logger_tcmd_logging_start(short unsigned int len, char * data)
{
    /* Start logging now */
    DPRINTT("Enabling Logging\n");
    logger_tcmd_enable_logging = TRUE;
    DPRINTT("Logging enabled\n");
    logger_write_config();
    logger_tcmd_response_builder(TCMD_LOGGING_START, RESULT_SUCCESS, 0, NULL);
    return;
}


/* logger_tcmd_logging_stop()
 *
 * Description: This function immediately disables logging and updates the
 * config file to disable logging at the next logger startup.
 *
 * @len: currently unused
 * @data: currently unused
 *
 * Return: None
 */
static void logger_tcmd_logging_stop(short unsigned int len, char * data)
{
    /* Stop logging now */
    DPRINTT("Disabling logging\n");
    logger_tcmd_enable_logging = FALSE;
    DPRINTT("Logging disabled\n");
    logger_write_config();
    logger_tcmd_response_builder(TCMD_LOGGING_STOP, RESULT_SUCCESS, 0, NULL);
    return;
}


/* logger_tcmd_suapi_port_retrieve()
 *
 * Description: This function gets the list of enabled ports from SUAPI, and
 * sends a list of them back in the TCMD response
 *
 * @len: Unused
 * @data: Unused
 *
 * Return: None
 */
static void logger_tcmd_suapi_port_retrieve(short unsigned int len, char * data)
{
#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
    char port_list[LOGGER_TCMD_SUAPI_NUM][LOGGER_TCMD_SUAPI_SIZE + 1];
    int i = 0;                          // Loop control
    int port_num = 0;                   // Number of ports
    int resp_len = 0;                    // Total length of response list
    int temp_len = 0;                   // Temporary length
    void * cur_resp_port = NULL;         // Current port in resp_data
    char * resp_data = NULL;             // Pointer to response list

    /* Get the port list */
    port_num = logger_get_suapi_port_list(port_list[0], LOGGER_TCMD_SUAPI_NUM,
            LOGGER_TCMD_SUAPI_SIZE + 1);

    /* Figure out min length needed */
    DPRINTT("Figuring min length needed\n");
    for (i = 0; i < port_num; i++)
    {
        DPRINTT("Checking for port %d\n", i);
        resp_len += strlen(port_list[i]) + 1; // String plus NULL char
    }
    DPRINTT("Min length needed: %d\n", resp_len);

    /* Malloc area for response */
    DPRINTT("Mallocing response\n");
    resp_data = (char *) kmalloc(resp_len, GFP_KERNEL);
    if (resp_data == NULL)
    {
        DPRINTT("ERROR! Could not allocate memory for resp_data\n");
        logger_tcmd_response_builder(TCMD_SUAPI_PORT_RETRIEVE,
                RESULT_PROCESSING_FAILURE, 0, NULL);
        return;
    }

    /* Copy response in */
    cur_resp_port = resp_data;
    DPRINTT("Copying in response\n");
    for (i = 0; i < port_num; i++)
    {
        temp_len = strlen(port_list[i]) + 1;
        DPRINTT("Port %d: %d long\n", i, temp_len);
        strncpy(cur_resp_port, port_list[i], temp_len);
        cur_resp_port += temp_len;
    }
    
    /* Respond */
    DPRINTT("Responding\n");
    logger_tcmd_response_builder(TCMD_SUAPI_PORT_RETRIEVE, RESULT_SUCCESS,
            resp_len, resp_data);

    if (resp_data)
        kfree(resp_data);

    return;
    
#else /* !NOSUAPI && CONFIG_SUAPI */
    DPRINTT("SUAPI support not built in\n");
    logger_tcmd_response_builder(TCMD_SUAPI_PORT_RETRIEVE,
            RESULT_INVALID_MESSAGE_TYPE, 0, NULL);
    return;
#endif /* !NOSUAPI && CONFIG_SUAPI */

}


/* logger_tcmd_suapi_port_enable()
 *
 * Description: This function receives a list of SUAPI ports to enable. It
 * parses this list, and enables each port in SUAPI.
 *
 * @len: Length of the data variable
 * @data: holds the names of the SUAPI ports to enable
 *
 * Return: None
 */
static void logger_tcmd_suapi_port_enable(short unsigned int len, char * data)
{
#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
    char port_list[LOGGER_TCMD_SUAPI_NUM][LOGGER_TCMD_SUAPI_SIZE + 1];
    char * port_start = port_list[0];
    int port_num = 0;
    int temp_num = 0;
    int i = 0;
    int suapi_error;
    SU_PORT_HANDLE  current_port = NULL;
    int fd = -1;
    mm_segment_t old_fs;
    struct file * config_file = NULL;
    ssize_t ret;

    if (data == NULL && len > 0)
    {
        DPRINTT("ERROR: Length is not 0, but data is NULL\n");
        logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
                RESULT_INVALID_MESSAGE_PARAMETER, 0, NULL);
        return;
    }
    
    
    /* Get the current SUAPI list */
    DPRINTT("Getting SUAPI port list\n");
    port_num = logger_get_suapi_port_list(port_list[0], LOGGER_TCMD_SUAPI_NUM,
            LOGGER_TCMD_SUAPI_SIZE + 1);

    /* Disable logging on the current ports */
    DPRINTT("Disabling SUAPI logging ports\n");
    for (i = 0; i < port_num; i++)
    {
        DPRINTT("Disabling port %d %s\n", i, port_list[i]);
        current_port = (SU_PORT_HANDLE) suapi_findname(port_list[i], 0,
                &suapi_error);
        if (suapi_error == 0)
        {
            DPRINTT("Found port; Disabling\n");
            suapi_disable_port_logging(current_port);
        }
        else
            DPRINTT("ERROR! Could not find port\n");
    }

    /* Flush the SUAPI port list */
    memset(suapi_global_data_ptr->logger_config, 0x00,
            LOGGER_TCMD_SUAPI_NUM * LOGGER_TCMD_SUAPI_SIZE);

    if (len == 0)
    {
        /* If length is 0, then assume disable all ports */
        DPRINTT("Nothing to enable\n");
        logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
                RESULT_SUCCESS, 0, NULL);
        return;
    }
    
    /*
     * We shouldn't need locking around this file update - only SUAPI module init reads
     * this file, and it must complete before AP logger module can be installed.
     */
    BEGIN_KMEM_DEFINED;
    fd = sys_open(SUAPI_PORT_CONFIG_FILE, O_RDWR | O_CREAT | O_TRUNC, 00600);
    if (fd < 0)
    {
        DPRINTT("WARNING: Could not load SUAPI logger port config file %d %s\n", fd,
               SUAPI_PORT_CONFIG_FILE);
        END_KMEM_DEFINED;

        logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
                                     RESULT_PROCESSING_FAILURE, 0, NULL);
        return;
    }
    config_file = fget(fd);
    if (config_file == NULL)
    {
        sys_close(fd);
        DPRINTT("ERROR: Could not get SUAPI logger port config file pointer\n");
        END_KMEM_DEFINED;

        logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
                                     RESULT_PROCESSING_FAILURE, 0, NULL);
        return;
    }

    /* Format of the file calls for first line to say "ENABLED". */
    ret = vfs_write(config_file, SUAPI_PORT_CONFIG_KEYWORD,
              strlen(SUAPI_PORT_CONFIG_KEYWORD), &config_file->f_pos);

    if (ret < 0)
    {
        fput(config_file);
        sys_close(fd);
        END_KMEM_DEFINED;
        DPRINTT("ERROR: Could not write to SUAPI logger port config file\n");
        logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
                                     RESULT_PROCESSING_FAILURE, 0, NULL);
    }
    
    /* Pull the null terminated ports from argument passed */
    DPRINTT("Getting ports to enable\n");
    port_start = data;
    i = 0;
    temp_num = 0;
    while (i < len && temp_num < LOGGER_TCMD_SUAPI_NUM)
    {
        /* Fill my local list */
        current_port = (SU_PORT_HANDLE) suapi_findname(port_start, 0,
                                                       &suapi_error);
        if (suapi_error == 0)
        {
            DPRINTT("Enabling port %s\n", port_start);
            suapi_enable_port_logging(current_port);
        }
        DPRINTT("Adding port %s to logger_config\n", port_start);
        strncpy(suapi_global_data_ptr->logger_config +
                (temp_num * LOGGER_TCMD_SUAPI_SIZE), port_start,
                LOGGER_TCMD_SUAPI_SIZE);
        
        /* Rewrite SUAPI port config file. */
        ret = vfs_write(config_file, port_start, strlen(port_start), &config_file->f_pos);
        if (ret < 0 || vfs_write(config_file, "\n", 1, &config_file->f_pos) < 0)
        {
            fput(config_file);
            sys_close(fd);
            END_KMEM_DEFINED;
            DPRINTT("ERROR: Could not write to SUAPI logger port config file\n");
            logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
                                         RESULT_PROCESSING_FAILURE, 0, NULL);
        }
        
        i += strlen(port_start) + 1;
        if (i >= len)
            break;
        port_start += strlen(port_start) + 1;
        temp_num++;
    }

    fput(config_file);
    sys_close(fd);
    END_KMEM_DEFINED;

    /* TCMD Reply */
    logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE, 
            RESULT_SUCCESS, 0, NULL);
    return;
    
#else /* !NOSUAPI && CONFIG_SUAPI */
    DPRINTT("SUAPI support not built in\n");
    logger_tcmd_response_builder(TCMD_SUAPI_PORT_ENABLE,
            RESULT_INVALID_MESSAGE_TYPE, 0, NULL);
    return;
#endif /* !NOSUAPI && CONFIG_SUAPI */

}



/* logger_tcmd_pid_table()
 *
 * Description: This command retrieves the process numbers and names for a
 * pc logging tool.
 *
 * @len: Not used
 * @data: Not used
 */
static void logger_tcmd_pid_table(short unsigned int len, char * data)
{
    struct task_struct * task = current;
    char buf[MAX_PID_TABLE_SIZE];
    int tmp = 0;

    read_lock(&tasklist_lock);
    
    do
    {
        DPRINTT("Current process: %d %s\n", task->pid, task->comm);

        memset(buf, 0, MAX_PID_TABLE_SIZE);
        
        tmp = snprintf(buf, MAX_PID_TABLE_SIZE, "/%d/%s", task->pid,
                task->comm);
        
        if (tmp <= 0)
        {
            DPRINTT("Warning: problems writing log msg for %d %s\n",
                    task->pid, task->comm);
        }
        else
        {
            if (tmp > MAX_PID_TABLE_SIZE)
                tmp = MAX_PID_TABLE_SIZE;
            
            logger_cirbuf_write_pair(AL_T_PS, 0, buf, tmp, NULL, 0);
        }

        task = prev_task(task);
    } while (task != current);
    
    read_unlock(&tasklist_lock);
    
    logger_tcmd_response_builder(TCMD_PID_TABLE, RESULT_SUCCESS, 0, NULL);
    
    return;
}


/* logger_tcmd_fork_exec_start()
 *
 * Description: This command enables fork and exec logging.
 *
 * @len: Not used
 * @data: Not used
 */
static void logger_tcmd_fork_exec_start(short unsigned int len, char * data)
{
    if (logger_tcmd_fork_exec)
    {
        logger_tcmd_response_builder(TCMD_FORK_EXEC_START, RESULT_SUCCESS, 0,
                NULL);
        return;
    }
    
    if (logger_hooksysexecve(logger_new_execve_log) != 0)
    {
        logger_tcmd_response_builder(TCMD_FORK_EXEC_START,
                RESULT_PROCESSING_FAILURE, 0, NULL);
        return;
    }

    if (logger_hooksysfork(logger_new_fork_log) != 0)
    {
        logger_unhooksysexecve();
        logger_tcmd_response_builder(TCMD_FORK_EXEC_START,
                RESULT_PROCESSING_FAILURE, 0, NULL);
        return;
    }

    logger_tcmd_fork_exec = TRUE;
    logger_write_config();
    logger_tcmd_response_builder(TCMD_FORK_EXEC_START, RESULT_SUCCESS, 0, NULL);
    return;
}


/* logger_tcmd_fork_exec_stop()
 *
 * Description: This command disables fork and exec logging.
 *
 * @len: Not used
 * @data: Not used
 */
static void logger_tcmd_fork_exec_stop(short unsigned int len, char * data)
{
    if (!logger_tcmd_fork_exec)
    {
        logger_tcmd_response_builder(TCMD_FORK_EXEC_STOP, RESULT_SUCCESS, 0,
                NULL);
        return;
    }
    
    logger_unhooksysexecve();
    logger_unhooksysfork();
    logger_tcmd_fork_exec = FALSE;
    logger_write_config();
    logger_tcmd_response_builder(TCMD_FORK_EXEC_STOP, RESULT_SUCCESS, 0, NULL);
    return;
}


/* logger_tcmd_net_logging_start()
 *
 * Description: This command starts IP logging, and updates the config file.
 *
 * @len: Not used
 * @data: Not used
 */
static void logger_tcmd_net_logging_start(short unsigned int len, char * data)
{
    if (logger_net_hooked == FALSE)
    {
        if (logger_hook_network() != 0)
        {
            logger_tcmd_response_builder(TCMD_NET_LOGGING_START,
                    RESULT_PROCESSING_FAILURE, 0, NULL);
            return;
        }

        logger_net_enable = TRUE;
        logger_write_config();
    }
    
    logger_tcmd_response_builder(TCMD_NET_LOGGING_START,
            RESULT_SUCCESS, 0, NULL);
}
    

/* logger_tcmd_net_logging_stop()
 *
 * Description: This command stops IP logging, and updates the config file.
 *
 * @len: Not used
 * @data: Not used
 */
static void logger_tcmd_net_logging_stop(short unsigned int len, char * data)
{
    if (logger_net_hooked == TRUE)
    {
        logger_unhook_network();
        logger_net_enable = FALSE;
        logger_write_config();
    }

    logger_tcmd_response_builder(TCMD_NET_LOGGING_STOP,
            RESULT_SUCCESS, 0, NULL);
}


static void logger_tcmd_force_panic(short unsigned int len, char * data)
{
    panic("AP Logger forcing debug panic!\n");
}

