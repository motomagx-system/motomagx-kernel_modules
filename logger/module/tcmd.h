/*********************************************************
**  File: tcmd.h - Test command support header for AP datalogger
**
**        Copyright 2006, 2007 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**
*********************************************************/

/*********************************************************************
 * Revision History
 * Change #    Date     Author          Comment
 * ========  ========  ==============  ====================================
 *       1  10/03/2006  Motorola        Initial version
 *       2  01/24/2007  Motorola        Fixed SUAPI log port enable command
 *       3  03/25/2007  Motorola        Modify for libaplog/aplogd support
 *
 *********************************************************************/

/*
 * NOTE: This header file is for tcmd.c local use only. Items which
 * must be used in other .c files must go in common.h
 */

/************************************************
 * Local #includes
 ************************************************/
#include <linux/mtd/mtd.h>
#include <linux/vmalloc.h>


/************************************************
 * Local Function Prototypes
 ************************************************/
/* TCMD Functions */
static int logger_tcmd_response_builder(char, char, short unsigned int, char *);
static int logger_tcmd_action(void *);
/* TCMD Actions */
static void logger_tcmd_logging_start(short unsigned int, char *);      //0x40
static void logger_tcmd_logging_stop(short unsigned int, char *);       //0x41
static void logger_tcmd_suapi_port_enable(short unsigned int, char *);  //0x48
static void logger_tcmd_suapi_port_retrieve(short unsigned int, char *);//0x49
static void logger_tcmd_pid_table(short unsigned int, char *);          //0x51
static void logger_tcmd_fork_exec_start(short unsigned int, char *);    //0x52
static void logger_tcmd_fork_exec_stop(short unsigned int, char *);     //0x53
static void logger_tcmd_net_logging_start(short unsigned int, char *);  //0x54
static void logger_tcmd_net_logging_stop(short unsigned int, char *);   //0x55
static void logger_tcmd_force_panic(short unsigned int len, char * data);


/************************************************
 * Local Data Structure Definitions
 ************************************************/
/* logger_tcmd_t stores a test command code, and the pointer to its handler */
struct logger_tcmd_table_t{
    char code;
    void (* handler)(short unsigned int len, char *);
};

/************************************************
 * Local Definitions and Macros
 ************************************************/
/* Test command request layout
 *
 * Command ID:      1 byte
 * Data length (n): 2 bytes
 * Data:            n bytes
 */
#define REQUEST_COMMAND_OFFSET  0
#define REQUEST_COMMAND_SIZE    1
#define REQUEST_LENGTH_OFFSET   REQUEST_COMMAND_OFFSET + REQUEST_COMMAND_SIZE
#define REQUEST_LENGTH_SIZE     2
#define REQUEST_DATA_OFFSET     REQUEST_LENGTH_OFFSET + REQUEST_LENGTH_SIZE

#define SUAPI_PORT_CONFIG_FILE  "/etc/suapi/sulog.conf"

/* Test command request code list */
typedef enum {
    TCMD_EOL = 0x00,            // 0x00
    TCMD_LOGGING_START = 0x40,  // 0x40
    TCMD_LOGGING_STOP,          // 0x41
    TCMD_DUMPING_START,         // 0x42 - no longer supported
    TCMD_DUMPING_STOP,          // 0x43 - no longer supported
    TCMD_BUFFER_ERASE,          // 0x44 - no longer supported
    TCMD_BUFFER_CONFIG,         // 0x45 - no longer supported
    TCMD_BUFFER_GET_CONFIG,     // 0x46 - no longer supported
    TCMD_BUFFER_RETRIEVE,       // 0x47 - no longer supported
    TCMD_SUAPI_PORT_ENABLE,     // 0x48
    TCMD_SUAPI_PORT_RETRIEVE,   // 0x49
    TCMD_PANIC_KERNEL_RETRIEVE, // 0x4A - no longer supported
    TCMD_PANIC_AP_CONFIG,       // 0x4B - no longer supported
    TCMD_PANIC_AP_GET_CONFIG,   // 0x4C - no longer supported
    TCMD_PANIC_AP_LIST,         // 0x4D - no longer supported
    TCMD_PANIC_AP_RETRIEVE,     // 0x4E - no longer supported
    TCMD_PANIC_AP_ERASE,        // 0x4F - no longer supported
    TCMD_RESET,                 // 0x50 - no longer supported
    TCMD_PID_TABLE,             // 0x51
    TCMD_FORK_EXEC_START,       // 0x52
    TCMD_FORK_EXEC_STOP,        // 0x53
    TCMD_NET_LOGGING_START,     // 0x54
    TCMD_NET_LOGGING_STOP,      // 0x55
    TCMD_OUT_BUFFER_CONFIG,     // 0x56 - no longer supported
    TCMD_OUT_BUFFER_GET_CONFIG  // 0x57 - no longer supported
} LOGGER_TCMD_TYPE_T;

#define TCMD_FORCE_PANIC            0x91


/* Result layout
 *
 * Command ID:      1 byte
 * Result code:     1 byte
 * Data legnth (n): 2 bytes
 * Data:            n bytes
 */
#define RESULT_COMMAND_OFFSET       0
#define RESULT_COMMAND_SIZE         1
#define RESULT_CODE_OFFSET          RESULT_COMMAND_OFFSET + RESULT_COMMAND_SIZE
#define RESULT_CODE_SIZE            1
#define RESULT_LENGTH_OFFSET        RESULT_CODE_OFFSET + RESULT_CODE_SIZE
#define RESULT_LENGTH_SIZE          2
#define RESULT_DATA_OFFSET          RESULT_LENGTH_OFFSET + RESULT_LENGTH_SIZE

/* Result codes */
typedef enum {
    RESULT_SUCCESS = 0x01,              // 0x01
    RESULT_INVALID_MESSAGE_TYPE,        // 0x02
    RESULT_INVALID_MESSAGE_SIZE,        // 0x03
    RESULT_INVALID_MESSAGE_PARAMETER,   // 0x04
    RESULT_PROCESSING_FAILURE,          // 0x05
    RESULT_UNEXPECTED_MESSAGE,          // 0x06
    RESULT_TIMEOUT                      // 0x07
} LOGGER_TCMD_RESPONSE_T;

#define LOGGER_TCMD_MAX_WAIT        90

#define MAX_TCMD_RESP_LEN           65535

#define MAX_PID_TABLE_SIZE          64

#define SUAPI_PORT_CONFIG_KEYWORD "ENABLED\n"
