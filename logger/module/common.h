/*********************************************************
**  File: common.h - Common header file for AP logger module files
**
**        Copyright 2006, 2007 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**
*********************************************************/

/*********************************************************************
 * Revision History
 *  #   Date        Author          Comment
 * === ==========  ==============  ======================================
 *   1 10/03/2006   Motorola        Initial version
 *   2 11/02/2006   Motorola        Modified default logging paths
 *   3 03/25/2007   Motorola        Modify for libaplog/aplogd support
 *
 *********************************************************************/

/*
 * NOTE: This file is meant to be used for definitions / data types / function
 * prototypes / etc which will be used across more than one .c file in this
 * directory.
 */


#ifndef __APLOGGER_COMMON_HDR
#define __APLOGGER_COMMON_HDR


/************************************************
 * Common includes
 ************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/poll.h>
#include <linux/circ_buf.h>
#include <linux/suspend.h>
#include <asm/poll.h>
#include <linux/syscalls.h>
#include <linux/spinlock.h>
#include <asm/atomic.h>
#include <linux/file.h>
#include <linux/namei.h>
#include <net/sock.h>
#include <linux/in.h>


/************************************************
 * AP Logger Hook Functions
 ************************************************/
#include <linux/aplogger_hook.h>


/************************************************
 * AP logger module specific headers
 ************************************************/
#include "al_util.h"
#include "al_format.h"


#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
/*==========================================
 * SUAPI sepecific headers
 *=========================================*/
#include <linux/suapi_module.h>
#endif /* !NOSUAPI && CONFIG_SUAPI */

/************************************************
 * Common defines and macros
 ************************************************/
#define FALSE 0
#define TRUE 1

#define LOGGER_DEVICE_NAME_SIZE 129
#define LOGGER_XMIT_BUFFER_SIZE (PAGE_SIZE * (0x1 << (buffer_pages)))
                                    // Size of circ buffer in bytes

#define LOGGER_TCMD_SUAPI_NUM       64
#define LOGGER_TCMD_SUAPI_SIZE      16

/*==========================================
 * Network definitions
 *=========================================*/
#define LOGGER_IP_PORT          11005       // IP Port to listen on


/*==========================================
 * Path definitions
 *=========================================*/
#define LOG_CONFIG_DIR              "/ezxlocal/logger"

/************************************************
 * Common data structure types
 ************************************************/

struct logger_private_t {
        struct tty_driver *tty_driver;
        int tty_driver_registered;      // non-zero if tty_driver registered

        struct tty_struct *tty;         // non-null if tty open
        struct tty_struct *gtty;        // global tty for kernel write

        int open_wait_count;            // count of (possible) blocked
        int exiting;                    // True if module exiting

        unsigned char throttle;         // non-zero if we are throttled
        unsigned char clocal;           // non-zero if clocal set
        unsigned char connected;        /* non-zero if connected to host
                                           (configured) */

        unsigned int writesize;         // packetsize * 4
        unsigned int ctrlin;            // line state device sends to host
        unsigned int ctrlout;           // line state device received from host

        struct circ_buf    xmit;        // write buffer
        char device_name[LOGGER_DEVICE_NAME_SIZE]; // log filename to write to
        int max_write_size;             // max number of bytes can be written

        struct file * file;             // file for log dev
        struct file * file1;            // file for redirect
        int     fd;
        int     can_write;              // for thread, if can out put?
};


#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
/************************************************
 * Common SUAPI exports
 ************************************************/
extern int log_active;
#endif /* !NOSUAPI && CONFIG_SUAPI */

/************************************************
 * Common net exports
 ************************************************/
extern char logger_net_enable;
extern char logger_net_hooked;

/************************************************
 * aplogger-core.c common functions
 ************************************************/
void logger_write_config(void);
int logger_cirbuf_write_pair(int type,  int from_user, const char * buf,  int count1, const char* buf2, int count2);
int logger_cirbuf_raw_write(int type,  const char * buf1,  int count1,
        const char* buf2, int count2);
int logger_cirbuf_space(void);
int __logger_init(void);
void __logger_exit(void);
void logger_new_execve_log(char *, char __user * __user *,
                char __user * __user *);
void logger_new_fork_log(struct task_struct *, pid_t);
int logger_header(char *buf, int type, int cnt, char level);




#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
/************************************************
 * suapi.c common functions
 ************************************************/
void logger_suapi_init(void);
void logger_suapi_exit(void);
void logger_wake_suapi_thread(void);
int logger_get_suapi_port_list(char *, int, int);
#endif  /* !NOSUAPI && CONFIG_SUAPI */

/************************************************
 * tcmd.c common functions
 ************************************************/
ssize_t logger_tcmd_write(struct file *, const char __user *, size_t, loff_t *);
ssize_t logger_tcmd_read(struct file *, char *, size_t, loff_t *);
unsigned int logger_tcmd_poll(struct file *, struct poll_table_struct *);

/************************************************
 * net.c common functions
 ************************************************/
int logger_hook_network(void);
void logger_unhook_network(void);


#endif /* !__APLOGGER_COMMON_HDR */
