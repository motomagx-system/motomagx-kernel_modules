/*********************************************************
**  File: net.c - Network logging support
**
**        Copyright 2006 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
*********************************************************/

/*********************************************************************
 * Revision History
 *  #   Date        Author          Comment
 * === ==========  ==============  =========================================
 *   1 10/03/2006   Motorola        Initial version. Ported from Motorola EzX
 *                                  logger. Added robustness and TCMD support.
 *
 *********************************************************************/

#include "common.h"
#include "net.h"

char logger_net_enable = FALSE;     // If network logging is enabled
char logger_net_hooked = FALSE;     // If network logging has been hooked

/************************************************
 * Net filter operations
 ************************************************/
static struct nf_hook_ops logger_net_op_in =
    { { NULL, NULL}, logger_net_io, THIS_MODULE, PF_INET,
        NF_IP_LOCAL_IN, NF_IP_PRI_FILTER };

static struct nf_hook_ops logger_net_op_out =
    { { NULL, NULL }, logger_net_io, THIS_MODULE, PF_INET,
        NF_IP_LOCAL_OUT, NF_IP_PRI_FILTER };


/************************************************
 * logger_hook_network()
 *
 * Dsecription: This function registers the logger_net_io function as a
 * netfilter.
 *
 * Return: (int) 0 if the hook was a sucess, -1 if hook failed
 *
 *************************************************/

int logger_hook_network(void)
{
    int ret_val = 0;
    if (logger_net_hooked)
    {
        DPRINTN("Netfilter hooks already registered\n");
        return 0;
    }

    ret_val = nf_register_hook(&logger_net_op_in);

    if (ret_val != 0)
    {
        DPRINTN("Problem registering 'in' hook\n");
        return -1;
    }

    /* Try to register the output hook */
    nf_register_hook(&logger_net_op_out);

    if (ret_val != 0)
    {
        DPRINTN("Problem registering 'out' hook\n");
        nf_unregister_hook(&logger_net_op_in);
        return -1;
    }
    DPRINTN("Hooks registered\n");
    logger_net_hooked = TRUE;
    return 0;
}

/************************************************
 * logger_unhook_network()
 *
 * Description: Function to remove logger_net_io() from netfilter
 *
 * Return: None
 *
 ************************************************/

void logger_unhook_network(void)
{
    if (!logger_net_hooked)
    {
        DPRINTN("Netfilter hooks not installed\n");
        return;
    }

    DPRINTN("Removing netfilter hooks\n");
    nf_unregister_hook(&logger_net_op_in);
    nf_unregister_hook(&logger_net_op_out);
    logger_net_hooked = FALSE;
}

/************************************************
 * logger_net_io()
 *
 * Description: This function does the actual logging of the IP data.
 *
 * @hooknum: The location in packet handling 'chain' where the packet is
 *           coming from
 * @skb: A pointer to the address of the socket buffer
 * @in: Pointer to the network device the packet enter
 * @out: Pointer to the network device the packet will be leaving
 * @okfn: Pointer to the functions which check the packet
 *
 * Return: (unsigned int) The 'acceptance' code for the packet. Always
 *         NF_ACCPET.
 * 
 ************************************************/

unsigned int logger_net_io(unsigned int hooknum, struct sk_buff **skb,
        const struct net_device *in, const struct net_device *out,
        int (*okfn)(struct sk_buff *))
{
    struct iphdr *ip = NULL;
    struct tcphdr *tcp = NULL;
    int length = 0;

    /* Check if sk_buff is non-linear. If not try to make it linear.
     * If we can't make it linear, just accept the packet */
    if (skb_is_nonlinear(*skb) && skb_linearize(*skb, GFP_KERNEL) != 0)
    {
        DPRINTN("Nonlinear sk_buff, could not linearize\n");
        return NF_ACCEPT;
    }
    
    /* Get the IP header */
    if ((*skb)->data == NULL)
        return NF_ACCEPT;
    
    ip = (*skb)->nh.iph;
    if (ip == NULL)
    {
        DPRINTN("(*skb)->nh.iph = NULL\n");
        return NF_ACCEPT;
    }

    /* Check if the protocol is TCP, and if the source / destination is the 
     * AP logger network port */
    if (ip->protocol == IPPROTO_TCP)
    {
        DPRINTN("TCP Packet Received\n");
        tcp = (struct tcphdr *)((*skb)->data + ((*skb)->nh.iph->ihl * 4));
        if (tcp == NULL)
            return NF_ACCEPT;
        DPRINTN("LOGGER: TCP Packet: src = %d; dest = %d log = %d\n",
                ntohs(tcp->source), ntohs(tcp->dest), LOGGER_IP_PORT);
        /* If the destination is the AP logger port, don't log it */
        if (hooknum == NF_IP_LOCAL_IN && ntohs(tcp->dest) == LOGGER_IP_PORT)
            return NF_ACCEPT;
        /* If the source is from the AP logger port, don't log it */
        else if (hooknum == NF_IP_LOCAL_OUT &&
                ntohs(tcp->source) == LOGGER_IP_PORT )
            return NF_ACCEPT;
    }
    
    /* We've passed all the checks. Log the packet */
    length = ntohs(ip->tot_len);
    logger_cirbuf_write_pair(AL_T_IP, FALSE, (unsigned char*)ip, length,
            NULL, 0);
    
    return NF_ACCEPT;
}
