/*********************************************************
**  File: aplogger-core.c - Core functionality of the aplogger
**
**        Copyright (C) 2006,2007 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**
**
*********************************************************/

/*********************************************************************
 * Revision History
 *   #    Date        Author          Comment
 * === ==========  ==============  ========================================
 *   1 10/05/2006   Motorola        Initial version. Based on Motoroal EzX
 *                                  logger, ported to Linux 2.6. Some changes
 *                                  include TCMD support, IP output, IP logging,
 *                                  fork / exec logging, 'reliable' circular log
 *                                  file, aplog_input device, buffering of file
 *                                  system writes.
 *   2  10/11/2006  Motorola        Added support to send over IP from other
 *                                  AP logger source files.
 *   3  10/12/2006  Motorola        Fixed a bug where threads were being
 *                                  notified even when the buffering limit had
 *                                  not been hit.
 *   4  10/19/2006  Motorola        Fixed bug in notify which prevented rmmod
 *   5  10/26/2006  Motorola        Corrected issues with hooking / unhooking
 *                                  by using fd instead of fd_array
 *   6  11/02/2006  Motorola        Changed default log path to mass storage
 *   7  11/07/2006  Motorola        Use RTC for ms since boot, not jiffies
 *   8  11/29/2006  Motorola        Fix write routine mem overflow
 *   9  12/06/2006  Motorola        Added missing fput() and sys_close calls
 *  10  03/25/2007  Motorola        Modify for libaplog/aplogd support
 *  11  05/18/2007  Motorola        Fix for 'RAM BUFFER FULL' messages
 *  12  08/06/2007  Motorola        Add support to enable logging for secure phone
 *  13  11/06/2007  Motorola        Upmerge from 6.1. (Files are missing or corrupted on phone after restart) 
 *********************************************************************/

#include "common.h"
#include "aplogger-core.h"


/*==========================================
 * Module parameters, and generic module setup
 *=========================================*/

#ifdef DEBUG_WRITE
static int ptk = FALSE;              // true: log kernel printks
#else
static int ptk = TRUE;              // true: log kernel printks
#endif /* DEBUG_WRITE */
static int console = TRUE;          //
int buffer_pages = 2;               // 2^buffer_pages pages in xmit buffer
int cache_order = 1;                // 2^cache_order pages in each cache

/****************** 
 * The following #if check investigates whether SUAPI dependent components
 * should be built with the module. NOSUAPI is the local definition for
 * debug compilation. CONFIG_SUAPI is the kernel configuration option
 * for SUAPI components.
 ******************/
#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
char suapi_thread_run = TRUE; // whether the suapi log thread should run
#endif //(!defined NOSUAPI) && (defined CONFIG_SUAPI)
int enable_tcmd = TRUE;      // true: enable TCMD support

/* Module information */
MODULE_AUTHOR( DRIVER_AUTHOR );
MODULE_DESCRIPTION( DRIVER_DESC );
MODULE_LICENSE( DRIVER_LICENSE );

/* Module parameters */
module_param(ptk, int, 0400);           /* Whether to register console and log
                                           printk()'s */
module_param(console, int, 0400);
module_param(buffer_pages, int, 0400);  // 2^buffer_pages pages in RAM buffer
module_param(enable_tcmd, int, 0400);   // Whether to enable TCMD support
module_param(cache_order, int, 0400);   // 2^cache_order pages in each cache

/*==========================================
 * Non-parameter global variables
 *=========================================*/
unsigned char security_setting = MOT_SECURITY_MODE_PRODUCTION;
/* Basic logger variables */
struct logger_private_t logger_private; //'main' module variable
static char logger_printk_level[] = LOGGER_PRINT_LEVEL;
                                        /*Holds desired printk level for setup*/
static struct tty_struct * logger_shell_tty;

/* Waitqueues */
DECLARE_WAIT_QUEUE_HEAD(logger_tcmd_wait);   //TCMD waitqueue

DECLARE_WAIT_QUEUE_HEAD(logger_out_device_wait); //output device read waitqueue


/* Thread completion variables */
DECLARE_COMPLETION(suapi_thread_complete);   //SUAPI thread complete

/* Spinlocks */
spinlock_t bufferptr_spinlock = SPIN_LOCK_UNLOCKED;  //Spin lock for buff ptrs

/* Atomics */
atomic_t tcmd_inuse = ATOMIC_INIT(0);       //atomic for tcmd use

/* Cache buffers and corresponding semaphores */
static char *logger_cirbuf_cache_in;     //used for logger_write 
DECLARE_MUTEX(logger_cirbuf_cache_in_sem); //Mutex to protect the cache in

/* Function pointers */
static long (*logger_sys_poll)(struct pollfd * ufds, unsigned int nfds,
        long timeout);
static long (*logger_sys_fcntl)(unsigned int fd, unsigned int cmd,
        unsigned long arg);
static int (*logger_sys_fsync)(unsigned int fd);
static long (*logger_sys_lseek)(unsigned int fd, off_t offset,
        unsigned int origin);
static ssize_t (*logger_old_stdout_write)(struct file * file,
        const char * buf, size_t count, loff_t *ppos);
static ssize_t (*logger_old_console_write)(struct file * file,
        const char * buf, size_t count, loff_t *ppos);
static long (* logger_old_open)(const char * filename, int flags, int mode);
static int (*logger_old_null_ioctl)(struct inode *, struct file *,
        unsigned int, unsigned long);

static struct file_operations logger_tcmd_fops = {
    owner:      THIS_MODULE,
    llseek:     NULL,
    read:       logger_tcmd_read,
    aio_read:   NULL,
    write:      logger_tcmd_write,
    aio_write:  NULL,
    poll:       logger_tcmd_poll,
    ioctl:      NULL,
    mmap:       NULL,
    open:       NULL,
    flush:      NULL,
    release:    NULL,
    fsync:      NULL,
    lock:       NULL,
    readv:      NULL,
    writev:     NULL,
    fsync:      NULL,
};
static struct miscdevice logger_tcmd_device = {
    minor:      LOGGER_TCMD_MINOR,
    name:       LOGGER_TCMD_NAME,
    fops:       &logger_tcmd_fops,
    devfs_name: LOGGER_TCMD_NAME,
};

/* Logger device entries */
static struct file_operations logger_device_fops = {
    owner:      THIS_MODULE,
    llseek:     NULL,
    read:       logger_device_read,
    aio_read:   NULL,
    write:      logger_device_write,
    aio_write:  NULL,
    poll:       logger_device_poll,
    ioctl:      NULL,
    mmap:       NULL,
    open:       NULL,
    flush:      NULL,
    release:    NULL,
    fsync:      NULL,
    lock:       NULL,
    readv:      NULL,
    writev:     NULL,
    fsync:      NULL,
};
static struct miscdevice logger_device = {
    minor:      LOGGER_DEVICE_MINOR,
    name:       LOGGER_DEVICE_NAME,
    fops:       &logger_device_fops,
    devfs_name: LOGGER_DEVICE_NAME,
};

/* Logger device entries */
static struct file_operations logger_out_device_fops = {
    owner:      THIS_MODULE,
    llseek:     NULL,
    read:       logger_out_device_read,
    aio_read:   NULL,
    write:      NULL,
    aio_write:  NULL,
    poll:       logger_out_device_poll,
    ioctl:      NULL,
    mmap:       NULL,
    open:       NULL,
    flush:      NULL,
    release:    NULL,
    fsync:      NULL,
    lock:       NULL,
    readv:      NULL,
    writev:     NULL,
    fsync:      NULL,
};
static struct miscdevice logger_out_device = {
    minor:      LOGGER_OUT_DEVICE_MINOR,
    name:       LOGGER_OUT_DEVICE_NAME,
    fops:       &logger_out_device_fops,
    devfs_name: LOGGER_OUT_DEVICE_NAME,
};

static char logger_registered_devices = 0;

/* The variables used to store the request and response; allocated at write,
 * free'd at read */
char logger_tcmd_enable_logging = TRUE;      // logging dis/en-abled
char logger_tcmd_fork_exec = FALSE;          // Fork / Exec logging control

/* logging console variable */
static struct console loggercons = {
    name:        "logger",
    write:        logger_console_write,
/*    wait_key:    logger_console_wait_key,*/
/*    setup:        logger_console_setup,*/
    flags:        CON_PRINTBUFFER|CON_ENABLED,
    index:       -1,
};


/* Logged Inode list */
static LIST_HEAD(inode_list);               // Linked list of inodes to log
static char logger_write_diff = FALSE;             // Check for quicker writes


/* logger_header()
 *
 * Description: This function sets up the header fields pointed to by @buf. 
 *
 * @buf: pointer to the header
 * @type: type of header to create
 * @cnt: length of the data in the msg to which this header applies
 * @level: severity level of this message
 *
 * Return: int; size (in bytes) of the created header
 */
int logger_header(char *buf, int type, int cnt, char level)
{
    struct al_header_t *l_header =  (struct al_header_t *)buf;

    l_header->reserved = 0;
    l_header->magic = AL_MAGIC;
    l_header->type = type;
    l_header->pid = current->pid & 0xffff;
    l_header->timestamp = rtc_sw_msfromboot();
    l_header->length = cnt & 0xffff;
    l_header->level =  level;
    l_header->port_id = 0; // should be filled in by daemon
    
    return sizeof(struct al_header_t);
}

/*
 * CIRC_COPY_IN()
 *
 * Description: This function will copy data into the circ buffer at the head,
 * and wraps where necessary. It will _NOT_ move the head or tail pointer.
 *
 * @cir_buf: pointer to the circular buffer
 * @src_buf: pointer to data to be copied into the circ buffer
 * @head: offset of bytes, from the start of the buffer, to the last valid
 *        byte in the buffer
 * @tail: offset in bytes, from the start of the buffer, to the first valid
 *        byte in the buffer
 * @bufsize: total number of bytes in the circular buffer
 * @cnt: total size of the data to be copied in
 *
 * Return: None
 */
static inline void CIRC_COPY_IN(char *cir_buf, const char * src_buf,
        int head, int tail, int bufsize, int cnt)
{
    int tmp = CIRC_SPACE_TO_END(head, tail, bufsize);
    if(tmp >= cnt){
        memcpy(cir_buf + head, src_buf, cnt);
    }else{
        memcpy(cir_buf + head, src_buf, tmp);
        memcpy(cir_buf, src_buf + tmp, cnt - tmp);
    }        
}

/*
 * CIRC_COPY_OUT()
 *
 * Description: This function will copy data out of the circ buffer at the tail,
 * and wraps where necessary. It will _NOT_ move the head or tail pointer.
 *
 * @cir_buf: pointer to the circular buffer
 * @dest_buf: pointer of where to copy from the circ buffer in to
 * @head: offset of bytes, from the start of the buffer, to the last valid
 *        byte in the buffer
 * @tail: offset in bytes, from the start of the buffer, to the first valid
 *        byte in the buffer
 * @bufsize: total number of bytes in the circular buffer
 * @cnt: total size of the data to be copied out
 *
 * Return: None
 */
static inline void CIRC_COPY_OUT(char *cir_buf, char * dest_buf,
        int head, int tail, int bufsize, int cnt)
{
    int tmp = CIRC_CNT_TO_END(head, tail, bufsize);
    if(tmp >= cnt){
        memcpy(dest_buf , cir_buf + tail, cnt);
    }else{
        memcpy(dest_buf, cir_buf + tail, tmp);
        memcpy(dest_buf + tmp, cir_buf, cnt - tmp);
    }        
}

/*
 * CIRC_COPY_OUT_TO_USER()
 *
 * Description: This function will copy data out of the circ buffer at the tail,
 * and wraps where necessary. It will _NOT_ move the head or tail pointer.  Used
 * when copying to user space memory.
 *
 * @cir_buf: pointer to the circular buffer
 * @dest_buf: pointer of where to copy from the circ buffer in to
 * @head: offset of bytes, from the start of the buffer, to the last valid
 *        byte in the buffer
 * @tail: offset in bytes, from the start of the buffer, to the first valid
 *        byte in the buffer
 * @bufsize: total number of bytes in the circular buffer
 * @cnt: total size of the data to be copied out
 *
 * Return: 0 on success, -1 on failure
 */
static inline int CIRC_COPY_OUT_TO_USER(char *cir_buf, char * __user dest_buf,
        int head, int tail, int bufsize, int cnt)
{
    int ret;
    int tmp = CIRC_CNT_TO_END(head, tail, bufsize);
    if(tmp >= cnt){
        ret = copy_to_user(dest_buf , cir_buf + tail, cnt);
    }else{
        ret = copy_to_user(dest_buf, cir_buf + tail, tmp);
        if (!ret)
            ret = copy_to_user(dest_buf + tmp, cir_buf, cnt - tmp);
    }

    if (ret)
        return -1;
    else
        return 0;
} 



/*=====================================================================
**below is console driver interface
**
**
**===================================================================*/
static void logger_console_write(struct console *co, const char *s, unsigned count)
{
    UNUSED_PARAM(logger_console_write);
    UNUSED_PARAM(co);

    if(console == 1 && logger_tcmd_enable_logging)
        logger_cirbuf_write(AL_T_PTK, 0, s, count);
}

static struct file *logger_fget_by_task(struct task_struct * task, int fd)
{
    struct file * file = NULL;
    struct files_struct *files = NULL;

    if (task == NULL)
        return NULL;

    files = task->files;

    if (fd < files->max_fds){
        file = files->fd[fd];
    }

    return file;
}



/* logger_stdout_write()
 *
 * Description: This is the new function which replaces the write calls for
 * hooked files. It logs the data going to the hooked write call, then calls
 * the original write call for the file
 *
 * @file: file pointer of the hooked file
 * @buf: What is being written to the file
 * @count: size of the data to read from @buf
 * @ppos: pointer to the offset
 *
 * return: (int) immediate return from orig write call
 */
              
static int logger_stdout_write(struct file * file, const char * buf, size_t count, loff_t *ppos)
{
    struct list_head * head_ptr = NULL;
    struct logger_inode_elem * this_inode = NULL;
    
    DPRINTW("coming into new_tty_write\n");
    DPRINTW("Logger_old_stdout_write: 0x%X\n",
            (unsigned int) logger_old_stdout_write);
    DPRINTW("logger_write_diff: 0x%X\n", logger_write_diff);

    logger_cirbuf_write(AL_T_STD, 1, buf, count);

    if (!logger_write_diff)     // No diff in write calls; can be quick
        return (*logger_old_stdout_write)(file, buf, count, ppos);
    else                        // Writes differ, need to check
    {
        DPRINTW("WARNING: Slower writing enabled\n");
        /* Check if inode number is in our list */
        list_for_each(head_ptr, &inode_list)
        {
            this_inode = list_entry(head_ptr, struct logger_inode_elem,
                    list);
            if (file->f_dentry->d_inode->i_ino == this_inode->inode_num
                    && file->f_dentry->d_inode->i_rdev == this_inode->inode_dev)
            {
                return (this_inode->orig_write)(file, buf, count, ppos);
            }
        }
    }

    /* This condition should never occur, and if it does, there is probably
     * something very wrong with the system. We'll let it continue so it can
     * panic on its own (hopefully) */
    DPRINT("ERROR: Inode has been hooked but is not in our list.\n");
    return 0;
}                            

/*fake ioctl for /dev/null, isatty(1) then can return ture, heihei:)*/
static int logger_new_null_ioctl(struct inode * inode, struct file * file,
        unsigned int cmd, unsigned long arg)
{
    int err = -ENOTTY;

    UNUSED_PARAM(logger_new_null_ioctl);
    UNUSED_PARAM(file);
    UNUSED_PARAM(arg);
    UNUSED_PARAM(inode);
    
    if ((strcmp(current->comm, "bash")==0) && (cmd == TCGETS))
        err = 0;

    return err;
}    

/* logger_new_execve_log()
 *
 * Description: This function is called whenever a do_exec() occurs. It is
 * called from within the do_exec() function itself, and logs the applicable
 * data.
 *
 * @filenamei: the file to execute
 * @argv: pointer to array of the command and options
 * @envp: pointer to array of the users environment variables
 *
 * Return: None
 */
void logger_new_execve_log(char *filenamei, char __user * __user *argv,
        char __user * __user *envp)
{
    char buf[MAX_EXEC_CMDLINE_SIZE];
    int ret_val = -1;
    int count = 0;
    int i = 0;
    int copy_size = 0;
    char __user * str = NULL;

    if (!logger_tcmd_enable_logging || filenamei == NULL || argv == NULL)
    {
        DPRINT("Invalid argument: %d, 0x%X, 0x%X\n", logger_tcmd_enable_logging,
                (unsigned int) filenamei, (unsigned int) argv);
        return;
    }
    
    /* Write all of the arguments, or until we have no space left */
    while (1)
    {
        /* Convert the string pointer to kernel space */
        if (get_user(str, (argv + i)))
        {
            printk("APLOGGER: Warning: Could not get argument array\n");
            printk("APLOGGER: Throwing away exec message\n");
            return;
        }

        /* Copy the argument string into kernel space */
        if (count == MAX_EXEC_CMDLINE_SIZE - 2 || str == NULL)
        {
            count++;
            break;
        }
        else if (count != 0 && *(buf + count) == '\0')
        {
            *(buf + count) = ' ';
            count++;
        }
        
        copy_size = strnlen_user(str, MAX_EXEC_CMDLINE_SIZE - count - 1);
        ret_val = copy_from_user(buf + count, str, copy_size);

        if (ret_val < 0)
        {
            printk("APLOGGER: Warning could not copy arg for exec logging\n");
            printk("APLOGGER: Throwing away message\n");
            return;
        }
        else if (ret_val > 0)
        {
            buf[MAX_EXEC_CMDLINE_SIZE - 1] = '\0';
            count = MAX_EXEC_CMDLINE_SIZE;
            break;
        }

        /* strnlen_user returns with NULL character */
        count += copy_size - 1;
        
        /* See if we ran out of space */
        if (MAX_EXEC_CMDLINE_SIZE - count <= 1)
        {
            buf[MAX_EXEC_CMDLINE_SIZE - 1] = '\0';
            count = MAX_EXEC_CMDLINE_SIZE;
            break;
        }
        i++;                        /* Increment our argument counter */
    }
    
    logger_cirbuf_write(AL_T_EXE, 0, buf, count);

    return;

}    

/* atoi()
 *
 * Description: A simple replacement for the stdlib atoi which converts a
 * string value to its integer counter part.
 *
 * @s: pointer to the string to convert
 *
 * Return: (int) The value displayed by the string
 */
static int atoi( char *s) 
{
    int i=0;
    while (isdigit(*s))
          i = i*10 + *(s++) - '0';
    return i;
}

    
/* logger_new_fork_log()
 *
 * Description: The function which logs pertinent data about fork system calls
 *
 * @task: pointer to the calling task structure
 * @new_pid: process id number of the newly fork program
 *
 * Return: None
 */
void logger_new_fork_log(struct task_struct * task, pid_t new_pid)
{
    char buf[LOGGER_LOADAVG_PROC_BUFFER_SIZE];
    int ret_val = -1;

    if (!logger_tcmd_enable_logging)
        return;
    
    ret_val = snprintf(buf, LOGGER_LOADAVG_PROC_BUFFER_SIZE - 1,
            "%s (%d) forked process %d", task->comm, task->pid, new_pid);
    if (ret_val >= LOGGER_LOADAVG_PROC_BUFFER_SIZE)
        buf[LOGGER_LOADAVG_PROC_BUFFER_SIZE - 1] = '\0';

    if (ret_val < 0)
    {
        printk("APLOGGER: Warning: Could not create fork log message\n");
        printk("APLOGGER: Dropping fork log\n");
        return;
    }
        
    logger_cirbuf_write(AL_T_FORK, 0, buf,
            min(ret_val + 1, LOGGER_LOADAVG_PROC_BUFFER_SIZE));

    return;

}    

/* logger_new_open()
 *
 * Wrapper function which acts as the system call for open. Checks if the open
 * is to the system console and calls hook if it is
 */
static long logger_new_open(const char * filename, int flags, int mode)
{
    long fd;
    struct file * l_f = NULL;
    struct list_head * head_ptr = NULL;
    struct logger_inode_elem * inode_ptr = NULL;

    fd = (*logger_old_open)(filename, flags, mode);
    
    /* We only need to check file STDIN, STDOUT, and STDERR */
    if (fd < 0 || fd > STDERR_FILENO)
        return fd;

    list_for_each(head_ptr, &inode_list)
    {
        inode_ptr = list_entry(head_ptr, struct logger_inode_elem,
                list);
        l_f = logger_fget_by_task(current, fd);
        DPRINT("file inode: %ld; list inode: %ld\n",
                l_f->f_dentry->d_inode->i_ino, inode_ptr->inode_num);
        /* This if check makes sure we haven't lsot any of the pointers
         * before checking if the inode number and rdev are a file we
         * are looking for */
        if (l_f != NULL && l_f->f_dentry && l_f->f_dentry->d_inode &&
                l_f->f_dentry->d_inode->i_ino == inode_ptr->inode_num
		&& l_f->f_dentry->d_inode->i_rdev == inode_ptr->inode_dev)
        {
            logger_hookfdtty(fd);
            DPRINT("Hooked %s\n", filename);
            if (l_f->f_op != NULL && l_f->f_op->write != logger_stdout_write)
            {
                if (logger_old_console_write != NULL
                        && logger_old_console_write != l_f->f_op->write)
                {
                    logger_write_diff = TRUE;
                    DPRINT("WARNING: Clobbering preset old_console_write\n");
                }
                logger_old_console_write = l_f->f_op->write;
                
                if (inode_ptr->orig_write != NULL
                        && inode_ptr->orig_write != l_f->f_op->write)
                {
                    DPRINT("WARNING: Clobbering preset orig_write\n");
                }
                inode_ptr->orig_write = l_f->f_op->write;
                l_f->f_op->write = logger_stdout_write;
            }
            break;
        }
    }

    DPRINT("New open call %s\n", filename);

    return fd;
}


/* logger_hookstdfops()
 *
 * Description: This function re-routes the write calls to the system console
 * through our modified functions which log the data before calling the actual
 * write call.
 * 
 * NOTE: we regard stdout and stderr is the same device, so we only hook stdout
 *
 * Return: (int) 1 on success, -1 on failure
 */
static int logger_hookstdoutfops(void)
{
    struct file * l_f;

    l_f = fget(STDOUT_FILENO);

    if (IS_ERR(l_f) || (l_f == NULL) ) {
        DPRINT("err opening stdout!\n");
        return -1;
    }
    
    if (l_f->f_op != NULL )  {
        DPRINT("hook stdout write OK\n");
        if (logger_old_stdout_write == NULL
                && l_f->f_op->write != logger_stdout_write) // Initialize
        {
            DPRINTW("Setting old_stdout_write to 0x%X\n",
                    (unsigned int)logger_stdout_write);
            logger_old_stdout_write = l_f->f_op->write;
        }
        l_f->f_op->write = logger_stdout_write;    
    }

    /* if the stdout is /dev/null */
    if (l_f->f_dentry && l_f->f_dentry->d_inode 
        && (l_f->f_dentry->d_inode->i_rdev == MKDEV(MEM_MAJOR, LOGGER_NULL_MINOR))
        && l_f->f_op){
        DPRINT("hook null dev ioctl\n");
        logger_old_null_ioctl = l_f->f_op->ioctl;
        l_f->f_op->ioctl = logger_new_null_ioctl;        
    }
    fput(l_f);
            
    return 1;
}

/* logger_hookall()
 *
 * Description: This function calls the applicable hook functions which provide
 * the modified system calls through which the logger reads
 *
 * Return: (int) 0 in success, -1 in failure
 */
static int logger_hookall(void)
{
    logger_old_console_write = NULL;
    logger_write_diff = FALSE;
    logger_old_open = NULL;

    if (console == 1){
        logger_hookstdoutfops();
        logger_old_open = logger_hooksysopen(logger_new_open);
        if (logger_old_open == NULL)
            return -1;
    }

    if (logger_tcmd_fork_exec)
    {
        if (logger_hooksysexecve(logger_new_execve_log) < 0)
            return -1;
        if (logger_hooksysfork(logger_new_fork_log) < 0)
            return -1;
    }

    if (logger_net_enable)
        if (logger_hook_network())
            return -1;

    return 0;
}

/* logger_unhookall()
 *
 * Description: This function resets the modified system calls to their original
 * so the logger module can be removed and the system can continue to function
 * normally
 *
 * Return: (int) Always 0
 */
static int logger_unhookall(void)
{
    /* Always unhook fork and exec. There is no harm in doing this when 
     * fork and exec logging is diabled */
    logger_unhooksysexecve();
    logger_unhooksysfork();
    logger_unhooksysopen(logger_old_open);
    logger_unhookwriteops();
    logger_unhookfdfops();
    logger_unhook_network();

    return 0;
}

/*=====================================
**get a fd and replace it's file operations
**  if the file is normal char dev, replace
**with new_char_XX.
**  if the file is normal tty dev, replace
**with new_tty_XX.
=======================================*/
static int logger_hookfdtty(int fd)
{
    return logger_hookfdtty_by_task(current, fd);
}

static int logger_hookfdtty_by_task(struct task_struct * task, int fd)
{
    struct file *file=NULL;
    struct tty_struct * tty=NULL;
    unsigned long flags;
    
    if ((fd != STDIN_FILENO) && (fd != STDOUT_FILENO) && (fd != STDERR_FILENO))
    {
       DPRINT("ERROR! Invalid fd number %d\n", fd);
       return -2;        
    }

    if (task == NULL) 
    {
        DPRINT("ERROR! Invalid task\n");
        return -3;
    }
    
    /*replace the std out write func*/
    file = logger_fget_by_task(task, fd);

    if(IS_ERR(file) || file == NULL)
        return -1;

    tty = file->private_data;

    if(tty != NULL){
        local_irq_save(flags);
        logger_shell_tty = tty;        
        local_irq_restore(flags);
    }

    return 1;
}

/* logger_unhookwriteops()
 *
 * Description: This function goes through all of the open task's file
 * descriptors, checking to see that all references to
 * logger_stdout_write() are removed, before the module is shutdown.
 *
 * Return: Always 0 at this time
 */
static int logger_unhookwriteops(void)
{
    struct task_struct * task = current;
    int i = 0;
    
    read_lock(&tasklist_lock);
    do
    {
        if (task->files)
        {
            spin_lock(&task->files->file_lock);
            for (i = 0; i <= task->files->max_fds && i <= STDERR_FILENO; i++)
            {
                if (test_bit(i, task->files->open_fds->fds_bits) == 1 &&
                        (task->files->fd[i]) &&
                        task->files->fd[i]->f_op->write ==
                        logger_stdout_write)
                {
                    task->files->fd[i]->f_op->write =
                        logger_old_stdout_write;
                }
            }
            spin_unlock(&task->files->file_lock);
        }
        task = prev_task(task);
    } while (task != current);
    read_unlock(&tasklist_lock);

    return 0;
}

static int logger_unhookfdfops(void)
{
    struct file * l_f;

    l_f = fget(STDOUT_FILENO);

    if (IS_ERR(l_f) || (l_f == NULL) ) {
        DPRINT("err opening stdout!\n");
        return -1;
    }
    
    if (l_f->f_op != NULL && logger_old_stdout_write != NULL)  {
        DPRINT("unhook stdout write OK\n");                    
        l_f->f_op->write = logger_old_stdout_write;
    }

    if (l_f->f_op->ioctl == logger_new_null_ioctl)
    {
        l_f->f_op->ioctl = logger_old_null_ioctl;
    }
    fput(l_f);

    return 0;    
}    

/*===================================================================
**below is printk output level
**
**
**===================================================================*/
static void logger_printk_set_level(void)
{
    struct file   *l_f = NULL;
    mm_segment_t old_fs;
        
    BEGIN_KMEM_DEFINED; 

    l_f = filp_open(LOGGER_PRTINK_FILE, O_RDWR, 00600); 

    if (IS_ERR(l_f) || l_f == NULL) {
        DPRINT("Error %ld opening %s\n", -PTR_ERR(l_f), LOGGER_PRTINK_FILE); 
    } else {
        if (WRITABLE(l_f))
            vfs_write(l_f, logger_printk_level, sizeof(logger_printk_level),
                    &l_f->f_pos);
        else {
             DPRINT("%s does not have a write method\n", LOGGER_PRTINK_FILE);
        }

        if ( filp_close(l_f,NULL))
            DPRINT("Error closing %s\n", LOGGER_PRTINK_FILE);
    }

    END_KMEM_DEFINED;
}

/* logger_cirbuf_write()
 *
 * Description: This function calls cirbuf_write_pair to write data to the 
 * circular ram buffer.
 *
 * @type: type of data to be logged
 * @from_user: 0 if from kernel space, 1 if from user space
 * @buf: Pointer to the buffer to write to the circular buffer
 * @count: number of bytes to write
 *
 * Return: (int) number of bytes written
 */
int logger_cirbuf_write(int type,  int from_user, const char * buf,  int count)
{
    return logger_cirbuf_write_pair(type, from_user, buf, count, NULL, 0);
}


/* logger_cirbuf_write_pair()
 *
 * Description: This function handles the checking of various options about
 * the circular buffer before a write is attempted. After a write succeeds,
 * this function notifies the poll handler there is data to read.
 *
 * @type: type of data to be logged
 * @from_user: 0 if from kernel space, 1 if from user space
 * @buf: Pointer to the buffer to write to the circular buffer
 * @count: number of bytes to write in @buf
 * @second_buf: Pointer to second buffer to write
 * @count2: Length of data in @second_buf
 *
 * Return: (int) number of bytes written
 */
int logger_cirbuf_write_pair(int type,  int from_user, const char * buf,  int count1, const char* second_buf, int count2)
{
    struct logger_private_t *l_logger = &logger_private;
    int count = count1 + count2;
    int remain1 = count1;
    int remain2 = count2;
    int to_wrt1, to_wrt2;
    int max_data_wrt = l_logger->max_write_size - sizeof(struct al_header_t) - 4;

    if (count < 0){
        return -EINVAL;
    }        

    if (count == 0)
        return 0;

    if (from_user)
    {
        if (count > l_logger->max_write_size)
        {
            printk("APLOGGER WARNING! Cache size not big enough to log.\n");
            printk("MSG: Process: %s, count: %d\n", current->comm,
                    count1 + count2);
            return -1;
        }

    }
    
    /*split data if data if too big */
    while(remain1 + remain2 > 0){
        DPRINTW("remain1: %d, remain2: %d\n", remain1, remain2);
        to_wrt1 = remain1;
        to_wrt2 = remain2;
        
        /* Cap first buffer if greater than max. */
        if (to_wrt1 > max_data_wrt)
        {
            DPRINTW("to_wrt1 is too big!\n");
            to_wrt1 = max_data_wrt;
            to_wrt2 = 0;
        }
        
        /* Cap second buffer if total is greater than max. */
        if (to_wrt1 + to_wrt2 > max_data_wrt)
            to_wrt2 = max_data_wrt - to_wrt1;

        /* Copy from user memory, if necessary, and write to buffer. */
        if (from_user)
        {
            DPRINTW("From userspace\n");
            down(&logger_cirbuf_cache_in_sem);
            copy_from_user(logger_cirbuf_cache_in, buf, to_wrt1);
            
            if (to_wrt2 > 0)
                copy_from_user(logger_cirbuf_cache_in, second_buf, to_wrt2);

            DPRINTW("Writing to the cirbuf\n");
            logger_cirbuf_raw_write(type, logger_cirbuf_cache_in, to_wrt1,
                                    logger_cirbuf_cache_in + to_wrt1, to_wrt2);
            up(&logger_cirbuf_cache_in_sem);
        }
        else
        {
            logger_cirbuf_raw_write(type, buf, to_wrt1, second_buf, to_wrt2);
        }
        
        /* Update pointers and remaining data counts. */
        buf += to_wrt1;
        second_buf += to_wrt2;
        remain1 -= to_wrt1;
        remain2 -= to_wrt2;
        DPRINTW("Buf: %x; sec buf: %x, remain1: %d, remain2: %d\n", (unsigned int)buf, (unsigned int)second_buf, remain1, remain2);
    }    

    /* Notify poll handler there is output to do */
    wake_up_interruptible(&logger_out_device_wait);
    
    return count;
}

/* logger_cirbuf_raw_write()
 *
 * Description: This function handles the actual writing of data to the
 * circular buffer. This function will lock the xmit buffer (and overwrite if
 * necessary) to protect the data. 
 *
 * @type: Type of data to log
 * @buf: string buffer of data to log
 * @count: number of bytes in @buf
 *
 * Return: (int) Number of bytes written
 *
 * Notes: Buffers pointers passed into this function _MUST_ be in kernel space
 */
int logger_cirbuf_raw_write(int type,  const char * buf1,  int count1,
        const char* buf2, int count2)
{
    struct logger_private_t *l_logger = &logger_private;
    int count = count1 + count2;
    char l_buf[sizeof(struct al_header_t)];
    int ret = 0;

    /* Check that it is valid, and will fit in the cache / buffer */
    if (count < 0
            || count >= (LOGGER_XMIT_BUFFER_SIZE - sizeof(struct al_header_t)))
    {
        printk("Incomming message too big for input buffer\n");
        return -EINVAL;
    }

    if (!logger_tcmd_enable_logging)
        return 0;
    
    if (count == 0)
        return 0;

    if (!l_logger->xmit.buf || !logger_cirbuf_cache_in)
        return 0;

    if (type != AL_T_NOHEAD)
    {
        /* default_message_loglevel is a Linux global for the default level
         * assigned to log messages.
         */
        int level = default_message_loglevel;
            
        if (type == AL_T_PTK)
        {
            if (buf1[0] == '<')
            {
                level = atoi((char*)&buf1[1]);
                if (level == 0 && buf1[1] != '0')
                    level = default_message_loglevel;
            }
        }

        /* Linux log levels are one less than equivalent aplog log levels. */
        level++;
        
        logger_header(l_buf, type, count, level);
    }

    DPRINTW("logger_cirbuf_write, going to write \n");

    /* Lock the read pointer */
    spin_lock(&bufferptr_spinlock);

    /* Check if there is enough space */
    ret = CIRC_SPACE(l_logger->xmit.head, l_logger->xmit.tail,
                LOGGER_XMIT_BUFFER_SIZE);
    if (ret < (count +
               (type == AL_T_NOHEAD ? 0 : sizeof(struct al_header_t)) +
               LOGGER_LOST_DATA_MSG_SIZE + sizeof(struct al_header_t)))
    {
        if (ret >= LOGGER_LOST_DATA_MSG_SIZE + sizeof(struct al_header_t))
        {
            /* Copy in the lost data msg */
            logger_header(l_buf, AL_T_MISC, LOGGER_LOST_DATA_MSG_SIZE,
                          default_message_loglevel+1);
            /* Copy in the header */
            CIRC_COPY_IN(l_logger->xmit.buf, l_buf, l_logger->xmit.head,
                    l_logger->xmit.tail, LOGGER_XMIT_BUFFER_SIZE,
                    sizeof(struct al_header_t));
            CIRC_MOV_AHEAD_N(l_logger->xmit.head, sizeof(struct al_header_t),
                    LOGGER_XMIT_BUFFER_SIZE);
            /* Copy in the message text */
            CIRC_COPY_IN(l_logger->xmit.buf, LOGGER_LOST_DATA_MSG,
                    l_logger->xmit.head, l_logger->xmit.tail,
                    LOGGER_XMIT_BUFFER_SIZE, LOGGER_LOST_DATA_MSG_SIZE);
            CIRC_MOV_AHEAD_N(l_logger->xmit.head, LOGGER_LOST_DATA_MSG_SIZE,
                    LOGGER_XMIT_BUFFER_SIZE);
        }
        /* There isn't; unlock, print, and return */
        spin_unlock(&bufferptr_spinlock);

        return 0;
    }
    ret = 0;
    
    /* If is of type AL_T_NOHEAD, then we don't need  aheader */
    if (type != AL_T_NOHEAD)
    {
        CIRC_COPY_IN(l_logger->xmit.buf, l_buf, l_logger->xmit.head,
                l_logger->xmit.tail, LOGGER_XMIT_BUFFER_SIZE,
                sizeof(struct al_header_t));
        CIRC_MOV_AHEAD_N(l_logger->xmit.head, sizeof(struct al_header_t),
                LOGGER_XMIT_BUFFER_SIZE);
        ret += sizeof(struct al_header_t);
    }
        
    /* Copy the contents of buf1 */
    if (buf1 != NULL && count1 > 0)
    {
        CIRC_COPY_IN(l_logger->xmit.buf, buf1, l_logger->xmit.head,
                l_logger->xmit.tail, LOGGER_XMIT_BUFFER_SIZE, count1);
        CIRC_MOV_AHEAD_N(l_logger->xmit.head, count1, LOGGER_XMIT_BUFFER_SIZE);
        DPRINTW("Wrote %i bytes from buffer 1 into circ_buf\n", count1);
    }
    
    /* Copy the contents of buf2 */
    if (buf2 != NULL && count2 > 0)
    {
        CIRC_COPY_IN(l_logger->xmit.buf, buf2, l_logger->xmit.head,
                l_logger->xmit.tail, LOGGER_XMIT_BUFFER_SIZE, count2);
        CIRC_MOV_AHEAD_N(l_logger->xmit.head, count2, LOGGER_XMIT_BUFFER_SIZE);
        DPRINTW("Wrote %i bytes from buffer 2 into circ_buf\n", count2);
    }

    /* Unlock the write pointer */
    spin_unlock(&bufferptr_spinlock);

    ret += count;   // Increment the return value
    
    DPRINTW("logger_cirbuf_write, after buffer insert \n");        


    return ret;
}

/*
 * logger_write_config()
 *
 * Description: This file updates the configuration file with the current
 * values
 *
 * Return: None
 *
 */
void logger_write_config(void)
{
    int fd = -1;
    struct file * config_file = NULL;
    char temp_buf[LOGGER_CONFIG_BUFFER_SIZE] = "\0";
    int temp_cnt = 0;
    mm_segment_t old_fs;

    BEGIN_KMEM_DEFINED;
    
    /* Open the config file */
    fd = sys_open(LOGGER_CONFIG_FILE_PATH, O_RDWR | O_CREAT | O_TRUNC, 00600);
    if (fd < 0)
    {
        DPRINT("WARNING: Could not load configuration file %d %s\n", fd,
                LOGGER_CONFIG_FILE_PATH);
        END_KMEM_DEFINED;
        return;
    }
    config_file = fget(fd);
    if (config_file == NULL)
    {
        sys_close(fd);
        printk("ERROR: Could not get config file pointer\n");
        END_KMEM_DEFINED;
        return;
    }

    /* Logging enabled */
    vfs_write(config_file, CONFIG_STR_COLLECT, strlen(CONFIG_STR_COLLECT),
            &config_file->f_pos);
    vfs_write(config_file, (logger_tcmd_enable_logging) ? "1\n": "0\n", 2,
            &config_file->f_pos);

    /* AP Panic core Size */
    
    /* AP Panic max cores */

    /* AP Panic core loc */

    /* AP panic core path */

    /* log printk */
    vfs_write(config_file, CONFIG_STR_LOG_PRINTK, strlen(CONFIG_STR_LOG_PRINTK),
            &config_file->f_pos);
    vfs_write(config_file, (ptk) ? "1\n" : "0\n", 2, &config_file->f_pos);

    /* ram buff pages */
    vfs_write(config_file, CONFIG_STR_RAM_BUFF_PAGES,
            strlen(CONFIG_STR_RAM_BUFF_PAGES), &config_file->f_pos);
    temp_cnt = snprintf(temp_buf, LOGGER_CONFIG_BUFFER_SIZE - 1, "%d\n",
            buffer_pages);
    vfs_write(config_file, temp_buf, temp_cnt, &config_file->f_pos);

    /* Fork / Exec logging */
    vfs_write(config_file, CONFIG_STR_FORK_EXEC,
            strlen(CONFIG_STR_FORK_EXEC), &config_file->f_pos);
    vfs_write(config_file, (logger_tcmd_fork_exec) ? "1\n" : "0\n", 2,
            &config_file->f_pos);

    /* IP Logging */
    vfs_write(config_file, CONFIG_STR_LOG_NET, strlen(CONFIG_STR_LOG_NET),
              &config_file->f_pos);
    vfs_write(config_file, (logger_net_enable) ? "1\n" : "0\n", 2,
              &config_file->f_pos);

    fput(config_file);
    sys_close(fd);
    END_KMEM_DEFINED;
    return;
}


/*
 * logger_load_config()
 *
 * Description: This function loads the configuration file supplied by the
 * defined value LOGGER_CONFIG_FILE_PATH.
 *
 * Returns: None
 */

static void logger_load_config(void)
{
    int fd = -1;                        // Config file descriptor
    struct file * config_file = NULL;   // Config file pointer
    char * file_buffer = NULL;          // char buffer; same size as filelength
    unsigned short int file_size = 0;   // size of file
    char * type = NULL;                 // pointer to whats after the =
    char * setting = NULL;              /* pointer to beginng of configuration
                                           line */
    mm_segment_t old_fs;                // for BEGIN_KMEM_DEFINED
    struct kstat config_stat;

    BEGIN_KMEM_DEFINED;
    DPRINTT("Beginning module config\n");
    /* Open the file */
    fd = sys_open(LOGGER_CONFIG_FILE_PATH, O_RDONLY, 00600);
    
    if (fd < 0 || vfs_fstat(fd, &config_stat) < 0)
    {
        DPRINT("WARNING: Could not load configuration file %d %s\n", fd,
                LOGGER_CONFIG_FILE_PATH);
        logger_write_config();
        END_KMEM_DEFINED;
        return;
    }
    config_file = fget(fd);
    if (config_file == NULL)
    {
        sys_close(fd);
        printk("ERROR: Could not get config file pointer\n");
        END_KMEM_DEFINED;
        return;
    }
    
    /* Malloc a buffer the size of the file */
    file_size = config_stat.size;
    DPRINTT("File size %d\n", file_size);
    /* Reset the file pointer to the beginning */
    vfs_llseek(config_file, 0, 0);
    file_buffer = kmalloc(file_size, GFP_KERNEL);
    if (file_buffer == NULL)
    {
        printk("ERROR: Could not allocate enough space for the file buffer\n");
        fput(config_file);
        sys_close(fd);
        END_KMEM_DEFINED;
        return;
    }
    vfs_read(config_file, file_buffer, file_size, &config_file->f_pos);
    fput(config_file);
    sys_close(fd);
    END_KMEM_DEFINED;
    type = file_buffer;
    
    while(type != file_buffer + file_size)
    {
        /* Find beginning of type */
        while((*type == ' ' || *type == '\n') &&
                type != file_buffer + file_size)
        {
            type++;
        }

        /* If at end of file, breakout */
        if (type == file_buffer + file_size)
            break;

        if (*type == '#')
        {
            while (*type != '\n' && type != file_buffer + file_size)
            {
                type++;
            }
            /* If at end of file, breakout */
            if (type == file_buffer + file_size)
                break;
            else if (*type == '\n')
            {
                type++;
                continue;
            }
        }
        /* At this point we should be pointing to a config name */
        setting = type;
        while (*setting != '=' && *setting != '\n'
                && setting != file_buffer + file_size)
        {
            setting++;
        }
        if (setting == file_buffer + file_size)
            break;
        setting++;              // to get past the '='
        if (*setting == '\n')
        {
            setting++;
            type = setting;
            continue;
        }
        
        if (!strncmp(type, CONFIG_STR_COLLECT,
                    strlen(CONFIG_STR_COLLECT)))
        {   /* Whether to collect logged data */
            if (*setting == '1')
                logger_tcmd_enable_logging = TRUE;
            else
                logger_tcmd_enable_logging = FALSE;
            DPRINT("Loaded config; enable logging %s\n", 
                    logger_tcmd_enable_logging ? "true" : "false");
        }
        else if (!strncmp(type, CONFIG_STR_AP_PANIC_CORE_SIZE,
                    strlen(CONFIG_STR_AP_PANIC_CORE_SIZE)))
        {   /* Max size of a panic core dump */
        }
        else if (!strncmp(type, CONFIG_STR_AP_PANIC_MAX_CORES,
                    strlen(CONFIG_STR_AP_PANIC_MAX_CORES)))
        {   /* Max number of cores per process */
        }
        else if (!strncmp(type, CONFIG_STR_AP_PANIC_CORE_LOC,
                    strlen(CONFIG_STR_AP_PANIC_CORE_LOC)))
        {   /* Location of cores; RAM, flash, MMC */
        }
        else if (!strncmp(type, CONFIG_STR_AP_PANIC_CORE_PATH,
                    strlen(CONFIG_STR_AP_PANIC_CORE_PATH)))
        {   /* Path to cores on MMC */
        }
        else if (!strncmp(type, CONFIG_STR_LOG_PRINTK,
                    strlen(CONFIG_STR_LOG_PRINTK)))
        {   /* Whether to log printks */
            if (*setting == '1')
                ptk = TRUE;
            else
                ptk = FALSE;
            DPRINT("Loaded config; ptk %s\n",
                    ptk ? "true" : "false");
        }
        else if (!strncmp(type, CONFIG_STR_RAM_BUFF_PAGES,
                    strlen(CONFIG_STR_RAM_BUFF_PAGES)))
        {   /* Number of pages in the ram buffer */
            buffer_pages = atoi(setting);
            DPRINT("Loaded config; buffer_pages = %d\n",
                    buffer_pages);
        }
        else if (!strncmp(type, CONFIG_STR_FORK_EXEC,
                    strlen(CONFIG_STR_FORK_EXEC)))
        {   /* Fork / Exec logging */
            if (*setting == '1')
                logger_tcmd_fork_exec = TRUE;
            else
                logger_tcmd_fork_exec = FALSE;
            DPRINT("Loaded config; fork_exec = %s\n",
                    (logger_tcmd_fork_exec) ? "true" : "false");
        }
        else if (!strncmp(type, CONFIG_STR_LOG_NET, strlen(CONFIG_STR_LOG_NET)))
        {   /* Whether to log IP or not */
            if (*setting == '1')
                logger_net_enable = TRUE;
            else
                logger_net_enable = FALSE;
            DPRINT("Loaded config; logger_net_enable = %s\n",
                   logger_net_enable ? "true" : "false");
        }
        else
        {
            DPRINT("WARNING: Unknown config option\n");
        }
        
        type = setting;

        /* Move to next line on the configuration file */
        while (*type != '\n' && type != file_buffer + file_size)
        {
            type++;
        }
    }

    if (file_buffer != NULL)
        kfree(file_buffer);
    return;
}


/* logger_add_inode()
 *
 * Description: This function call adds an inode number to the inode
 * linked list, which gets checked at start to enable logging on already
 * running processes.
 *
 * @path : Path string
 *
 * Return: 0 on success; -1 on failure
 */
static int logger_add_inode(char * path)
{
    int fd = -1;
    struct file * file_ptr = NULL;
    unsigned long inode_num = 0;
    dev_t inode_dev;
    struct logger_inode_elem * this_inode = NULL;
    mm_segment_t old_fs;
    
    DPRINT("Adding %s to check list\n", path);
    /* Check that the path exists, and get the inode number */
    BEGIN_KMEM_DEFINED;
    fd = sys_open(path, O_RDONLY, 00600);
    if (fd < 0)
    {
        DPRINT("WARNING: Could not open %s\n", path);
        return -1;
    }
    file_ptr = logger_fget_by_task(current, fd);
    if (file_ptr == NULL)
    {
        sys_close(fd);
        DPRINT("WARNING: Could get file ptr to %s\n", path);
        return -1;
    }
    inode_num = file_ptr->f_dentry->d_inode->i_ino;
    inode_dev = file_ptr->f_dentry->d_inode->i_rdev;
    sys_close(fd);
    END_KMEM_DEFINED;

    /* Malloc the new elem */
    this_inode = (struct logger_inode_elem *) 
        kmalloc(sizeof(struct logger_inode_elem), GFP_KERNEL);
    if (this_inode == NULL)
    {
        DPRINT("WARNING: Could not malloc space for %s\n", path);
        return -1;
    }

    /* Init the list element */
    this_inode->inode_num = inode_num;
    this_inode->inode_dev = inode_dev;
    this_inode->orig_write = NULL;

    /* Add the elem to the list */
    list_add(&(this_inode->list), &inode_list);
    DPRINT("Added %s to path list (#%ld)\n", path, this_inode->inode_num);
    
    return 0;
}


/*
 * logger_inode_list_cleanup()
 *
 * Description: This function cleans up all of the inode list entries
 *
 * Return: None
 */
static void logger_inode_list_cleanup(void)
{
    struct list_head * this_elem = NULL;
    struct list_head * next_elem = NULL;
    struct logger_inode_elem * this_inode = NULL;

    /* Itterate the list */
    list_for_each_safe(this_elem, next_elem, &inode_list){
        this_inode = list_entry(this_elem, struct logger_inode_elem, list);
        DPRINT("Removing inode %ld from list\n", this_inode->inode_num);
        /* Remove the element */
        list_del(this_elem);
        /* Free what it pointed to */
        if (this_inode != NULL)
            kfree(this_inode);
    }
    
    return;
}


/* logger_hook_openfds();
 *
 * Description: This function goes through the applicable open file descriptors
 * and hooks the necessary ones.
 *
 * Return: None
 */
static void logger_hook_openfds(void)
{
    int i = 0;
    struct task_struct * task = current;
    struct list_head * head_ptr = NULL;
    struct logger_inode_elem * inode_ptr = NULL;
    struct file * file_ptr = NULL;
    
    read_lock(&tasklist_lock);
    do
    {
        if (task->files)
        {
            spin_lock(&task->files->file_lock);
            for (i = 0; i <= task->files->max_fds && i <= STDERR_FILENO; i++)
            {
                /* Check to make sure not null */
                if (test_bit(i, task->files->open_fds->fds_bits) == 0 ||
                        task->files->fd[i] == NULL)
                    continue;
                
                file_ptr = logger_fget_by_task(task, i);
                if (file_ptr == NULL)
                    continue;
                
                /* Check if inode number is in our list */
                list_for_each(head_ptr, &inode_list)
                {
                    inode_ptr = list_entry(head_ptr, struct logger_inode_elem,
                            list);
                    /* Check if inode # is the same, and write is not hooked */
                    if (file_ptr->f_dentry->d_inode->i_ino ==
                            inode_ptr->inode_num
                            && file_ptr->f_dentry->d_inode->i_rdev ==
                            inode_ptr->inode_dev
                            && file_ptr->f_op->write !=
                            logger_stdout_write)
                    {
                        DPRINT("Hooking inode %ld\n", inode_ptr->inode_num);
                        if (inode_ptr->orig_write != NULL
                                && inode_ptr->orig_write !=
                                file_ptr->f_op->write)
                            DPRINT("WARNING! Clobbering orig old write\n");

                        DPRINT("old_stdout: %X; list orig: %X, write: %X\n",
                                (unsigned int) logger_old_stdout_write,
                                (unsigned int) inode_ptr->orig_write,
                                (unsigned int) file_ptr->f_op->write);
                        if (logger_old_stdout_write == NULL)
                        {
                            DPRINT("Changing old stdout write to: 0x%X\n",
                                    (unsigned int)file_ptr->f_op->write);
                            logger_old_stdout_write = file_ptr->f_op->write;
                        }
                        else if (logger_old_stdout_write != 
                                file_ptr->f_op->write)
                        {
                            DPRINT("WARNING! Different write calls "
                                    "Changing to write checking mode\n");
                            logger_write_diff = TRUE;
                        }
                        inode_ptr->orig_write = file_ptr->f_op->write;
                        file_ptr->f_op->write = logger_stdout_write;
                        /* Breaks us out of list_for_each() */
                        break;
                    }
                }
            }
            spin_unlock(&task->files->file_lock);
        }
        task = prev_task(task);
    } while (task != current);
    read_unlock(&tasklist_lock);
}


/*
 * logger_unhook_openfds();
 *
 * Description: This function goes through the applicable open file descriptors
 * and unhooks the necessary ones.
 *
 * Return: None
 */
static void logger_unhook_openfds(void)
{
    int i = 0;
    struct task_struct * task = current;
    struct list_head * head_ptr = NULL;
    struct logger_inode_elem * inode_ptr = NULL;
    struct file * file_ptr = NULL;
    
    read_lock(&tasklist_lock);
    do
    {
        if (task->files)
        {
            spin_lock(&task->files->file_lock);
            for (i = 0; i <= task->files->max_fds && i <= STDERR_FILENO; i++)
            {
                /* Check to make sure not null */
                if (test_bit(i, task->files->open_fds->fds_bits) == 0 ||
                        task->files->fd[i] == NULL)
                    continue;
                
                file_ptr = logger_fget_by_task(task, i);

                if (file_ptr->f_op->write == logger_stdout_write)
                {
                    list_for_each(head_ptr, &inode_list)
                    {
                        inode_ptr = list_entry(head_ptr,
                                struct logger_inode_elem, list);
                        if (inode_ptr->inode_num ==
                                file_ptr->f_dentry->d_inode->i_ino
                                && inode_ptr->inode_dev ==
                                file_ptr->f_dentry->d_inode->i_rdev)
                        {
                            file_ptr->f_op->write = inode_ptr->orig_write;
                            break;
                        }
                    }
                }
            }
            spin_unlock(&task->files->file_lock);
        }
        task = prev_task(task);
    } while (task != current);
    read_unlock(&tasklist_lock);
}

/* logger_device_read()
 *
 * Description: A Dummy function that simply returns 0 in place of a read on
 * the device
 *
 * Return: (ssize_t) Always 0
 */
static ssize_t logger_device_read(struct file *filp, char __user *buf,
        size_t count, loff_t *offp)
{
    return 0;
}

/* logger_device_write()
 *
 * Description: The function that places the data from the write call, into
 * the circular RAM buffer.
 *
 * Return: (ssize_t) -EIO on error, count on sucess
 */
static ssize_t logger_device_write(struct file *filp, const char __user *buf,
        size_t count, loff_t *offp)
{
    ssize_t ret_val = 0;
    
    if (!logger_tcmd_enable_logging)
        return count;
    
    ret_val = logger_cirbuf_write(AL_T_STD, 1, buf, count);
    if (ret_val < 0)
        return -EIO;
    return ret_val;
}

/* logger_device_poll()
 *
 * Description: A Dummy function that always returns POLLOUT
 *
 * Return: (ssize_t) Always POLLOUT
 */
static unsigned int logger_device_poll(struct file * filp,
        struct poll_table_struct * poll_table)
{
    return POLLOUT;
}


/* logger_out_device_read()
 *
 * Description: read handler for the character device "aplog_output".
 * Currently, the driver defaults to always work in non-blocking mode.
 *
 * @filp: pointer to the file structure for this device
 * @buf: pointer to user space memory to place read data
 * @count: amount of data requested to be read
 * @offp: offset into the device to read (ignored for this device)
 *
 * Return: (ssize_t) The number of bytes read into the provided buffer.
 */
static ssize_t logger_out_device_read(struct file *filp, char __user *buf,
        size_t count, loff_t *offp)
{
    int tmp_tail=-1;
    int tmp_head=-1;
    int avail;
    
    /* Getting the current head offset */
    DPRINTS("DEBUG: Locking buffer pointer spinlock\n");
    spin_lock(&bufferptr_spinlock);
    tmp_head = logger_private.xmit.head;
    tmp_tail = logger_private.xmit.tail;
    spin_unlock(&bufferptr_spinlock);
    DPRINTS("DEBUG: Unlocking buffer pointer spinlock\n");

    avail = CIRC_CNT(tmp_head, tmp_tail, LOGGER_XMIT_BUFFER_SIZE);

    if (avail == 0)
    {
        DPRINTS("No bytes available in buffer\n");
        return 0;
    }

    if (count > avail)
        count = avail;
    
    if (CIRC_COPY_OUT_TO_USER(logger_private.xmit.buf, buf, tmp_head,
                              tmp_tail, LOGGER_XMIT_BUFFER_SIZE, count))
    {
        /* Returned non zero; Could not copy for some reason */
        DPRINTS("Could not copy to user for read function\n");
        return -EIO;
    }

    spin_lock(&bufferptr_spinlock);
    CIRC_MOV_AHEAD_N(logger_private.xmit.tail, count, LOGGER_XMIT_BUFFER_SIZE);
    spin_unlock(&bufferptr_spinlock);

    return count;
}

/* logger_out_device_poll()
 *
 * Description: poll handler for the character device "aplogger_output".
 * If data is written into the circular buffer, this should return POLLIN.
 *
 * @filp: pointer to the file structure for this device
 * @poll_table: pointer to poll_table_struct used by kernel to wait on device
 * Return: (ssize_t) poll bitmask.  Currently only supports POLLIN.
 */
static unsigned int logger_out_device_poll(struct file * filp,
        struct poll_table_struct * poll_table)
{
    unsigned int ret_val = 0;
    int tmp_tail=-1;
    int tmp_head=-1;
    
    poll_wait(filp, &logger_out_device_wait, poll_table);

    DPRINTS("DEBUG: Locking buffer pointer spinlock\n");
    spin_lock(&bufferptr_spinlock);
    tmp_head = logger_private.xmit.head;
    tmp_tail = logger_private.xmit.tail;
    spin_unlock(&bufferptr_spinlock);
    DPRINTS("DEBUG: Unlocking buffer pointer spinlock\n");
    
    /* Is there data to read in the buffer? */
    if (CIRC_CNT(tmp_head, tmp_tail, LOGGER_XMIT_BUFFER_SIZE) > 0)
    {
        ret_val |= POLLIN;
    }

    return ret_val;    
}

/* logger_init()
 *
 * Description: This function starts the AP logger. It hooks the necessary
 * system calls and starts the applicable threads.
 *
 * Return: (int) -1 on failure to start, 0 on success
 */
static int __init logger_init(void)
{
    DPRINT("Entering dummy init\n");
    return __logger_init();
}


int __logger_init(void)
{
    /*
     * Initial setup checks
     */

#ifdef CONFIG_MOT_FEAT_OTP_EFUSE
    unsigned char production_state = LAUNCH_SHIP;
    unsigned char scs1_value = SCS1_DISABLE_LOGGING;
    
    int security_err = 0;
    int logging_allowed =FALSE;
    
    mot_otp_get_mode(&security_setting, &security_err);
    if (security_err != 0){
    	
        DPRINT("Security mode: %d, Err: %d\n", security_setting, security_err);
        security_setting = MOT_SECURITY_MODE_PRODUCTION; 
    }
    else if (security_setting == MOT_SECURITY_MODE_NO_SECURITY)
    {
	    security_setting = MOT_SECURITY_MODE_ENGINEERING;
    }
    else if (security_setting != MOT_SECURITY_MODE_ENGINEERING 
		    && security_setting != MOT_SECURITY_MODE_NO_SECURITY)
    {
	    printk(KERN_ERR "Security settings is not engineering. \n");
	    security_setting = MOT_SECURITY_MODE_PRODUCTION;
    }
    if (security_setting == MOT_SECURITY_MODE_ENGINEERING)
    {
	    logging_allowed = TRUE;
    }

    if (security_setting == MOT_SECURITY_MODE_PRODUCTION)
    {
	    mot_otp_get_productstate(&production_state, &security_err);

	    if (security_err != 0 || production_state != PRE_ACCEPTANCE_ACCEPTANCE)
	    {
		    printk(KERN_ERR "production state is not PRE_ACCEPTANCE_ACCEPTANCE.\n");
		    DPRINT("Production state: %d, Err: %d\n", production_state, security_err);
		    production_state = LAUNCH_SHIP;
	    }
	    else if (production_state == PRE_ACCEPTANCE_ACCEPTANCE)
	    {
		    logging_allowed = TRUE;
	    }

	    if (production_state == LAUNCH_SHIP)
	    {
		    mot_otp_get_scs1(&scs1_value, &security_err);
		    if(security_err != 0 || ((scs1_value & SCS1_ENABLE_LOGGING) != SCS1_ENABLE_LOGGING) && ((scs1_value & SCS1_DISABLE_LOGGING) != SCS1_DISABLE_LOGGING))
		    {
			    printk(KERN_ERR "scs1 value is not SCS1_ENABLE_LOGGING\n");
			    DPRINT("scs1 value: %d, Err: %d\n", scs1_value, security_err);
			    scs1_value = SCS1_DISABLE_LOGGING;
		    }
		    else if (( scs1_value & SCS1_ENABLE_LOGGING) == SCS1_ENABLE_LOGGING)
		    {
			    logging_allowed = TRUE;
		    }
	    }
    }

    if (!logging_allowed)
    {
	    printk(KERN_ERR "Security setting have disabled AP logger\n");
	    return -1;
    }

#else /* CONFIG_MOT_FEAT_OTP_EFUSE */
    printk(KERN_ERR "Error: Motrola OTP security is not enabled\n");
    return -1;
#endif /* CONFIG_MOT_FEAT_OTP_EFUSE */

    if (buffer_pages <= 0)  /* Checking for valid number of buffer pages */
    {
        printk(KERN_ERR "Invalid number of buffer pages specified\n");
        return -1;
    }
    DPRINT("Buffer size: %ld\n", LOGGER_XMIT_BUFFER_SIZE);
    if (cache_order > buffer_pages)
    {
        printk(KERN_ERR "Cache space is more than that of the RAM buffer.\n");
        return -1;
    }

    /* Add the paths we would like to log */
    logger_add_inode(ATP_SYS_CONSOLE_NAME);
    
    /*
     * Begin logger setup
     */

    logger_private.max_write_size = PAGE_SIZE * (1<<cache_order);
    DPRINT("configuring aplogger \n");
    logger_load_config();
    
    DPRINT("starting ap logger\n");
    logger_printk_set_level();

    DPRINT("before get syscall\n");
    if (logger_syscall_getall((void **)&logger_sys_poll,
            (void **)&logger_sys_fcntl, (void **)&logger_sys_fsync,
            (void **)&logger_sys_lseek) < 0)
        return -1;
    
    logger_private.file = NULL;
    logger_private.fd    = -1;


    /*
     * Buffer allocations
     */
    logger_cirbuf_cache_in = (char*)__get_free_pages(GFP_KERNEL, cache_order);
    if (!logger_cirbuf_cache_in)
        return -ENOMEM;
    logger_private.xmit.buf = (char *)__get_free_pages(GFP_KERNEL, buffer_pages);
    if (!logger_private.xmit.buf){
        free_page((unsigned long)logger_cirbuf_cache_in);
        return -ENOMEM;
    }
    bufferptr_spinlock = SPIN_LOCK_UNLOCKED;

    logger_private.xmit.head = 0;
    logger_private.xmit.tail = 0;
    
    DPRINT("alloc mem for xmit buf (size:%ld), cache(size:%ld)\n", LOGGER_XMIT_BUFFER_SIZE, PAGE_SIZE);

    logger_shell_tty = NULL;

    if (ptk == 1)
        register_console(&loggercons);

    /*
     * Module thread startup
     */

#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
    logger_suapi_init();
#else  /* !NOSUAPI && CONFIG_SUAPI */
    complete(&suapi_thread_complete);
#endif /* !NOSUAPI && CONFIG_SUAPI */

    /*
     * Setup actual logging mechanisms
     */
    /* Search the existing file descriptors for these inodes */
    // TODO: find out why these result in undefined symbols
    logger_old_stdout_write = NULL;
    logger_hook_openfds();
    /* Hook system calls */
    
    if (logger_hookall() < 0)
    {
        printk(KERN_ERR "APLOGGER ERROR: Problem hooking the kernel; Removing\n");
        __logger_exit();
        return -1;
    }
    
    /*
     * Setup input device
     */
    if (misc_register(&logger_device))
    {
        printk(KERN_ERR "APLOGGER ERROR: Could not register input device\n");
        __logger_exit();
        return -1;
    }
    logger_registered_devices |= LOGGER_INPUT_DEVICE_INSTALLED;
    DPRINT("Got input minor: %d\n", logger_device.minor);

    if (misc_register(&logger_out_device))
    {
        printk(KERN_ERR "APLOGGER ERROR: Could not register output device\n");
        __logger_exit();
        return -1;
    }
    logger_registered_devices |= LOGGER_OUTPUT_DEVICE_INSTALLED;
    DPRINT("Got output minor: %d\n", logger_out_device.minor);

    /*
     * Setup TCMD interface
     */
    if (enable_tcmd)
    {
        if (misc_register(&logger_tcmd_device))
        {
            printk(KERN_ERR "APLOGGER ERROR: Could not register TCMD interface\n");
            __logger_exit();
            return -1;
        }
        logger_registered_devices |= LOGGER_TCMD_DEVICE_INSTALLED;
        DPRINT("Got TCMD minor: %d\n", logger_tcmd_device.minor);
    }

    DPRINT("logger tty driver successfully installed version: %s\n", DRIVER_VERSION);

    return 0;
}

/* logger_exit()
 *
 * Description: This function stops the running threads, and reset the system
 * to a state where normal operation can occur after module removal.
 *
 * Return: None
 */
static void __exit logger_exit(void)
{
    __logger_exit();
}

void __logger_exit(void)
{

    //Deregister TCMD
    DPRINT("Waiting for TCMD engine to complete\n");
    if (enable_tcmd)
    {
        while (atomic_read(&tcmd_inuse) != 0)
        {
            DPRINTT("Waiting for TCMD to finish\n");
            schedule();
        }
        DPRINT("Taking down /dev/aplog\n");
        if (logger_registered_devices & LOGGER_TCMD_DEVICE_INSTALLED)
            misc_deregister(&logger_tcmd_device);
    }

    //Deregister input device
    if (logger_registered_devices & LOGGER_INPUT_DEVICE_INSTALLED)
        misc_deregister(&logger_device);

    //Deregister output device
    if (logger_registered_devices & LOGGER_OUTPUT_DEVICE_INSTALLED)
        misc_deregister(&logger_out_device);

    /* Unhook the kernel system calls */
    // TODO: find out why these result in undefined symbols
    DPRINT("logger_exit: start!\n");
    logger_unhookall();
    DPRINT("Kernel hooks 'unhooked'\n");

    /* Go through the open file descriptors and unhook them */
    logger_unhook_openfds();
    
    /* Cleanup the inode list */
    logger_inode_list_cleanup();

    /* Wake up SUAPI thread to get any last data */
#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
    logger_wake_suapi_thread();
#endif
    
    /* Tell SUAPI thread to stop logging */
#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)
    logger_suapi_exit();
#endif

    if (ptk==1)
    {
        DPRINT("Unregister console next\n");
        unregister_console(&loggercons);
    }

    if (logger_private.file != NULL)
    {
        fput(logger_private.file);
        logger_private.file = NULL;
    }

    if (logger_private.fd >= 0)
    {
        sys_close(logger_private.fd);
        logger_private.fd = -1;
    }
    
    //Begin free-ing the global memory spaces
    DPRINT("Free cache in next\n");
    if(logger_cirbuf_cache_in)
        free_pages((unsigned long)logger_cirbuf_cache_in, cache_order);

    DPRINT("Free private next\n");
    if(logger_private.xmit.buf)
        free_pages((unsigned long)logger_private.xmit.buf, buffer_pages);

    DPRINT("Done; Returning to exit!\n");
    return;
}

#ifdef __arch_um__
int init_module(void){return logger_init();}
void cleanup_module(void){logger_exit();}
#else
module_init(logger_init);
module_exit(logger_exit);
#endif
