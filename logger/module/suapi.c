/*********************************************************
**  File: suapi.c - SUAPI specific portions of the AP datalogger
**
**        Copyright 2006 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
*********************************************************/

/*********************************************************************
 * Revision History
 *  #   Date        Author          Comment
 * === ==========  ===========     ===================================
 *   1 10/03/2006   Motorola        Initial version.
 *                                    
 *
 *********************************************************************/

#include "common.h"
#include "suapi.h"

#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)

/* logger_suapi_init()
 *
 * Description: This function is called at init time to initialize SUAPI
 * specific variables, before the SUAPI logging thread is started
 *
 * Return: None
 */
void logger_suapi_init()
{
    /*
     * If the SUAPI buffer has already overwritten old data, reposition the
     * read pointer so that it starts outputting the full buffer of data but
     * leaves room for the buffer full message.  Once log_active is set to
     * TRUE, SUAPI will no longer overwrite messages in the buffer when it
     * fills.
     */
    if (log_buffer_overwritten)
    {
        log_read_ptr = log_write_ptr +
            SIZE_HEADER_BUFFER_FULL + LOGGER_BUFFER_MINIMUM_SEPARATION;

        /* Readjust read pointer if it wrapped. */
        if (log_read_ptr >= log_buffer + log_buffer_size)
            log_read_ptr -= log_buffer_size;
        
        DPRINT("SUAPI log buffer has overwritten old data.\n");
        DPRINT("New read ptr is %x\n", (unsigned int)log_read_ptr);
    }

    /* Notify the SUAPI module that the AP logger is reading from its buffer. */
    log_active = TRUE;

    /* Write memory barrier. */
    wmb();
    
    /* Start the thread to read from the buffer. */
    kernel_thread(logger_thread_suapi, NULL, CLONE_FS | CLONE_FILES);

}

/*
 * logger_suapi_exit()
 *
 * Description: This function is called when the module is being removed, and
 * the SUAPI thread must be shut down
 *
 * Returns: None
 */
void logger_suapi_exit()
{
    // Shut down suapi logging thread.
    DPRINT("Waiting for SUAPI Logging thread completion\n");
    suapi_thread_run = FALSE;
    log_active = FALSE;
    
    /* Write memory barrier. */
    wmb();
    
    wake_up_interruptible(&log_wait_q);
    wait_for_completion(&suapi_thread_complete);
    DPRINT("SUAPI Logging thread has quit\n");
}

/*
 * logger_wake_suapi_thread()
 *
 * Description: This function has been added to allow wake_up_interruptible()
 * calls to be made on log_wait_q.
 *
 * Return: None
 */
void logger_wake_suapi_thread()
{
    wake_up_interruptible(&log_wait_q);
}


/* logger_get_suapi_port_list()
 *
 * Description: This function reads the list of SUAPI ports from the
 * SUAPI module memory space, and places them in the pointer provided to the
 * function.
 *
 * @dest: The pointer on where to store the port names
 * @max_ports: Maximum number of ports in the list
 * @max_len: Max length of a single port name
 *
 * Return: The number of SUAPI ports retrieved
 *
 * Notes:
 * -- @max_len should / must be at least LOGGER_TCMD_SUAPI_SIZE
 * -- if a port name is >= @max_len, the copy will not be null terminated.
 *    if @max_len is > the port name it will be padded by null characters
 * -- max_len * max_port should be the same size as is allocated at dest
 * -- Will never try to retrieve more than LOGGER_TCMD_SUAPI_NUM
 */

int logger_get_suapi_port_list(char * dest, int max_ports, int max_len)
{
    int port_num = 0;
    char * cur_suapi_port = suapi_global_data_ptr->logger_config;
    char * cur_dest_port = dest;
    int copy_size = (max_len < LOGGER_TCMD_SUAPI_SIZE) ? max_len :
        LOGGER_TCMD_SUAPI_SIZE;

    /* Set max_ports to be lesser of LOGGER_TCMD_SUAPI_NUM and itself */
    if (LOGGER_TCMD_SUAPI_NUM < max_ports)
        max_ports = LOGGER_TCMD_SUAPI_NUM;

    DPRINTT("Check max len and max_ports\n");
    if (max_len <= 0 || max_ports <= 0)
        return 0;
    
    DPRINTT("Flushing dest\n");
    memset(dest, 0x00, max_len * max_ports);
    
    while (*cur_suapi_port != '\0' && port_num < max_ports)
    {
        DPRINTT("Copying port\n");
        strncpy(cur_dest_port, cur_suapi_port, copy_size);
        DPRINTT("Current suapi port name: %s\n", cur_suapi_port);
        cur_dest_port += max_len;
        cur_suapi_port += LOGGER_TCMD_SUAPI_SIZE;
        DPRINTT("Next suapi port name: %s\n", cur_suapi_port);
        port_num++;
    }
    
    DPRINTT("Got %d port names from SUAPI\n", port_num);
    return port_num;
}



/*===================================================================
 * logger_thread_suapi
 * This thread is woken when the buffer storing SUAPI log messages
 * reaches a certain amount full, or after a several second timeout
 * to flush the buffer when the log rate is slow.  It reads from the
 * SUAPI buffer and integrates the messages into the primary AP logger
 * buffer.
 * 
 * @data: not used (standard thread parameter).
 * Return value not used.
 *=================================================================*/
static int logger_thread_suapi(void * data)
{
    unsigned long irq_flags;
    char* local_write_ptr;
    char* local_read_ptr;
    int ptr_diff, ptr_diff2;
    
    DPRINT("starting suapi logging thread\n");
    
    /*just do daemonize but reserve fs and files*/
    daemonize("logger_suapi_thread");

    while (suapi_thread_run)
    {
        /*
         * Wait until the SUAPI log buffer quota to be reached and no SUAPI tasks
         * are in the middle of logging.  Wait a maximum of 3 seconds, so if the
         * quota isn't reached very often, the user still sees their data.
         */
        wait_event_interruptible_timeout
            (log_wait_q,
             ((log_busy_count == 0) && (log_write_ptr != log_read_ptr)) ||
             suapi_thread_run == FALSE,
             HZ * 3);

        /* Check if the thread is ordered to quit. */
        if (suapi_thread_run == FALSE)
            break;
             
        /* Check if we were woken up because we need to freeze */
        if (current->flags & PF_FREEZE)
        {
            DPRINT("Freezing AP Logger: 'SUAPI thread'\n");
            refrigerator(PF_FREEZE);
            DPRINT("AP Logger unfrozen: 'SUAPI thread'\n");
            continue;
        }
        
        spin_lock_irqsave(&log_buffer_lock, irq_flags);
        
        DPRINTW("suapi logging thread woken up\n");
        DPRINTW("log_busy_count is %i; log_read_ptr is %x; log_write_ptr is %x\n",
               log_busy_count,
               (unsigned int)log_read_ptr, (unsigned int)log_write_ptr);
        DPRINTW("log_buffer is %x\n", (unsigned int)log_buffer);

        if (log_busy_count == 0)
        {
            DPRINTW("Getting SUAPI data\n");

            local_read_ptr = log_read_ptr;
            local_write_ptr = log_write_ptr;
            
            spin_unlock_irqrestore(&log_buffer_lock, irq_flags);

            /* Find the remaining data that can be safely flushed. */
            ptr_diff = local_write_ptr - local_read_ptr;
            
            /* Handle circular buffer wrapping. */
            if (ptr_diff < 0)
            {
                ptr_diff = log_buffer + log_buffer_size - local_read_ptr;
                ptr_diff2 = local_write_ptr - log_buffer;
            }
            else
                ptr_diff2 = 0;
            
            DPRINTW("ptr_diff is %i\n", ptr_diff);
            DPRINTW("ptr_diff2 is %i\n", ptr_diff2);
            
            if (ptr_diff != 0)
            {
                /* Write 1 or possibly 2 buffer segments. */
                logger_cirbuf_write_pair(AL_T_SUAPI, 0,
                                         local_read_ptr, ptr_diff,
                                         log_buffer, ptr_diff2);

                /* Handle circular buffer wrapping. */
                if (ptr_diff2 > 0)
                {
                    DPRINTW("suapi log buffer wrapped\n");
                    local_read_ptr = log_buffer + ptr_diff2;
                }
                else
                {
                    local_read_ptr += ptr_diff;

                    /*
                     * This should only occur if the write_pointer equals the
                     * start of the buffer...
                     */
                    if (local_read_ptr >= log_buffer + log_buffer_size)
                    {
                        DPRINTW("suapi log buffer wrapped\n");
                        local_read_ptr = log_buffer;
                    }
                }
                
                /* Update real read ptr, once it is calculated. */
                log_read_ptr = local_read_ptr;
                
                DPRINTW("log_read_ptr is now %x\n",(unsigned int)log_read_ptr);
            }
        }
        else
        {
            spin_unlock_irqrestore(&log_buffer_lock, irq_flags);
        }
    }

    complete(&suapi_thread_complete);

    return 0;
}

#endif /* !NOSUAPI && CONFIG_SUAPI */
