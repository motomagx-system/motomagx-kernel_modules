/*********************************************************
**  File: al_util.h
**
**        Copyright 2006 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**  
*********************************************************/

/*********************************************************************
 * Revision History
 *  #    Date       Author          Comment
 * ===  ========== ==============  =======================================
 *   1  10/04/2006  Motorola        Initial port for EzX logger. Added
 *                                  revision history. Updated DPRINTS. Added
 *                                  network DPRINT.
 *********************************************************************/

#ifndef __AL_UTIL_H__
#define __AL_UTIL_H__

#define LOGGER_MOD_NAME "LOGGER: "

#define BEGIN_KMEM_DEFINED old_fs = get_fs(); set_fs(get_ds()); 
#define END_KMEM_DEFINED set_fs(old_fs);

#ifdef DEBUGS
#define DPRINT(fmt, args...) printk_serial(LOGGER_MOD_NAME "<%s> " fmt, __FUNCTION__, ##args)
#endif

#ifdef DEBUG_ALL    //Turn on all debugging

#ifndef DEBUGS      //Check for serial printk's
#define DPRINT(fmt, args...) printk(LOGGER_MOD_NAME "<%s> " fmt, __FUNCTION__, ##args)
#endif

//Define the rest of the DPRINT msgs
#define DEBUG_WRITE
#define DEBUG_POLL
#define DEBUG_READ
#define DEBUG_SEMAPHORE
#define DEBUG_TCMD

#endif

#ifdef DEBUG 
#define DPRINT(fmt, args...) printk(LOGGER_MOD_NAME "<%s %d> " fmt, __FUNCTION__, __LINE__, ##args)
#else
#define DPRINT(fmt, args...) 
#endif 

#ifdef DEBUG_WRITE
#define DPRINTW(fmt, args...) printk(LOGGER_MOD_NAME "<%s %d> " fmt, __FUNCTION__, __LINE__, ##args)
#else
#define DPRINTW(fmt, args...)
#endif

#ifdef DEBUG_POLL
#define DPRINTP(fmt, args...) printk(LOGGER_MOD_NAME "<%s %d> " fmt, __FUNCTION__, __LINE__, ##args)
#else
#define DPRINTP(fmt, args...)
#endif

#ifdef DEBUG_READ
#define DPRINTR(fmt, args...) printk(LOGGER_MOD_NAME "<%s %d> " fmt, __FUNCTION__, __LINE__, ##args)
#else
#define DPRINTR(fmt, args...)
#endif

#ifdef DEBUG_SEMAPHORE
#define DPRINTS(fmt, args...) printk(LOGGER_MOD_NAME "<%s %d> " fmt, __FUNCTION__, __LINE__, ##args)
#else
#define DPRINTS(fmt, args...)
#endif

#ifdef DEBUG_TCMD
#define DPRINTT(fmt, args...) printk(LOGGER_MOD_NAME "<%s %d> " fmt, __FUNCTION__, __LINE__, ##args)
#else
#define DPRINTT(fmt, args...)
#endif

#ifdef DEBUG_NET
#define DPRINTN(fmt, args...) printk(LOGGER_MOD_NAME "<%s> " fmt, __FUNCTION__, ##args)
#else
#define DPRINTN(fmt, args...)
#endif

#define LOG_DEV "/dev/apl0"
#define LOGGER_DEV "/dev/aplog0"

#ifdef FILE_LOG
#undef LOG_DEV    
#define    LOG_DEV "/root/testlog"
#endif

#ifdef SERIAL_LOG
#undef LOG_DEV    
#define    LOG_DEV "/dev/ttyS0"
#endif

#ifdef NULL_LOG
#undef LOG_DEV    
#define    LOG_DEV "/dev/null"
#endif

#define ATP_SYS_CONSOLE_NAME    "/dev/console"
#define TERMINAL_NAME           "ttymxc2"
#define FULL_TERM_PATH          "/dev/ttymxc/2"

#ifndef STDOUT_FILENO
#define STDIN_FILENO  0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2
#endif

#define _write(f, buf, sz) (f->f_op->write(f, buf, sz, &f->f_pos)) 
#define WRITABLE(f) (f->f_op && f->f_op->write) 

#define _read(f, buf, sz) (f->f_op->read(f, buf, sz, &f->f_pos)) 
#define READABLE(f) (f->f_op && f->f_op->read) 

#endif
