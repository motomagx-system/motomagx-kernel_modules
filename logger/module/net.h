/*********************************************************
**  File: net.h - Network logging support header
**
**        Copyright 2006 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**
*********************************************************/

/*********************************************************************
 * Revision History
 *  #   Date        Author          Comment
 * === ==========  ==============  =======================================
 *   1 10/03/2006   Motorola        Initial version. Ported from Motorola EzX
 *                                  logger.
 *
 *********************************************************************/

#include <linux/netfilter.h>
#include <linux/ip.h>
#include <net/tcp.h>
#include <linux/skbuff.h>
#include <linux/netfilter_ipv4.h>

#ifndef __APLOGGER_NET_HDR_
#define __APLOGGER_NET_HDR_

/************************************************
 * Function Prototypes
 ************************************************/

unsigned int logger_net_io( unsigned int, struct sk_buff **,
        const struct net_device *, const struct net_device *,
        int (*okfn)(struct sk_buff *));


#endif /* _APLOGGER_NET_HDR_ */
