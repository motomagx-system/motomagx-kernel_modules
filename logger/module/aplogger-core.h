/*********************************************************
 **  File: aplogger-core.h - Local header for aplogger-core.h
 **
 **        Copyright (C) 2006,2007 Motorola, Inc.
 **
 ** This program is free software; you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation; either version 2 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program; if not, write to the Free Software
 ** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 **
 **
 *********************************************************/

/*********************************************************************
 * Revision History
 *   #    Date        Author          Comment
 * === ==========  ==============  ================================
 *   1 10/04/2006   Motorola        Initial version. Based on Motorola EzX
 *                                  logger, ported to Linux 2.6. Some changes
 *                                  include TCMD support, IP output, IP logging,
 *                                  fork / exec logging, 'reliable' circular log
 *                                  file, aplog_input device, buffering of file
 *                                  system writes.
 *   2 10/11/2006   Motorola        Added support to use IP specific items
 *                                  from other source files.
 *   3 11/07/2006   Motorola        Added rtc.h include    
 *   4 03/25/2007   Motorola        Modify for libaplog/aplogd support
 *   5 05/18/2007   Motorola        Fixed 'RAM BUFFER FULL' messages
 *
 *********************************************************************/

/*
 * NOTE: This header file is for aplogger-core.c local use only. Items which
 * must be used in other .c files must go in common.h
 */

/*===========================================
 * Generic kernel inclusions
 *==========================================*/
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/tty.h>
#include <linux/tty_flip.h>
#include <linux/tty_driver.h>
#include <linux/interrupt.h>
#include <linux/console.h>
#include <linux/sys.h>
#include <linux/time.h>
#include <linux/sched.h>
#include <linux/file.h>
#include <linux/mount.h>
#include <linux/security.h>
#include <linux/mm.h>
#include <linux/list.h>
#include <linux/rtc.h>


/*===========================================
 * IP Network specific headers
 *==========================================*/
#include <linux/net.h>
#include <linux/socket.h>
#include <net/tcp.h>


/*==========================================
 * Mot Security Settings
 *=========================================*/
#ifdef CONFIG_MOT_FEAT_OTP_EFUSE
#include <linux/ks_otp_api.h>
#endif /* CONFIG_MOT_FEAT_OTP_EFUSE */


/*==========================================
 * Processor specific headers
 *=========================================*/
#include <asm/processor.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <asm/hardirq.h>
#include <asm/fcntl.h>
#include <asm/unistd.h>
#include <asm/ptrace.h>
//#include <asm/poll.h>
#include <stdarg.h>


/*==========================================
 * TCMD specific headers
 *=========================================*/
#include <linux/miscdevice.h>


/*==========================================
 * Module info definitions
 *=========================================*/
#define DRIVER_VERSION "v1.0.0"
#define DRIVER_AUTHOR "Motorola"
#define DRIVER_LICENSE "GPL"
#define DRIVER_DESC "SCM-A11 Port of EzX AP Datalogging Module"


/*==========================================
 * Generic definitions
 *=========================================*/
#ifndef STDOUT_FILENO               // Define numbers for STDOUT and STDERR
#define STDOUT_FILENO 1             
#define STDERR_FILENO 2
#endif

#ifndef UNUSED_PARAM
#define UNUSED_PARAM(v) (void)(v)
#endif

#define isdigit(c) (c >= '0' && c <= '9')
#define LOGGER_NULL_MINOR 3

/*==========================================
 * Code inclusion definitions
 *=========================================*/
#define LOGGER_IMPLEMENTED  FALSE   // Include non-working portions?
#define LOGGER_HOOK         FALSE   // Include portions covered by kernel hooks


/*==========================================
 * Logging definitions
 *=========================================*/
#define LOGGER_PROCESS_NAME_SIZE            24  // max length of a process name
#define LOGGER_MAX_MASK_NUM                 10  // # of mask levels
#define LOGGER_EZXLOGLEVEL_DEFAULT          3   // default logging level


#define LOGGER_LOADAVG_PROC_BUFFER_SIZE 64      // /proc/loadavg buffer size
#define LOGGER_LOADAVG_PROC "/proc/loadavg"     // location for fork logging

#define LOGGER_LOST_DATA_MSG "APLOGGER WARNING: RAM BUFFER FILLED"
#define LOGGER_LOST_DATA_MSG_SIZE (strlen(LOGGER_LOST_DATA_MSG) + 1)
#define LOGGER_LOST_DATA_MSG_SIZE_WITH_HEADER \
    (LOGGER_LOST_DATA_MSG_SIZE + sizeof(struct al_header_t))


/*==========================================
 * Printk logging levels
 *=========================================*/
#define LOGGER_PRTINK_FILE "/proc/sys/kernel/printk"
#define LOGGER_PRINT_LEVEL "7 4 1 7"


/*==========================================
 * Circular buffer definitions / macros
 *=========================================*/
#define CIRC_MOV_AHEAD_N(pos, n, size)  ((pos) = ((pos) + n) & ((size) -1))
                                    // Move pos ahead n spaces (wraps)
#define CIRC_MOV_AHEAD(pos, size)  CIRC_MOV_AHEAD_N((pos), 1, (size))
                                    // Move pos ahead one space (wraps)
#define LOGGER_CIRBUF_WRITE_MAX   10*1024*1024  // Max write size (10M)

#define LOGGER_COMM_DOWNLOAD_FILENAME_SIZE  64


/*==========================================
 * Macro definitions
 *=========================================*/


/*==========================================
 * Config file defines
 *=========================================*/
#define LOGGER_CONFIG_FILENSIZE 63
#define LOGGER_CONFIG_BUFFER_SIZE 1024
#define LOGGER_CONFIG_ENTRY_REPLAY "replay="
#define LOGGER_CONFIG_ENTRY_REPLAY_LEN (sizeof(LOGGER_CONFIG_ENTRY_REPLAY)-1)
//#define CONFIG_FILE_PATH    "/var/tmp/aplog_config"
#define LOGGER_CONFIG_FILE_PATH    "/ezxlocal/logger/aplog_config"

/*==========================================
 * Logger shell task definitions
 *=========================================*/
#define MAX_EXEC_CMDLINE_SIZE 512
#define MAX_EXEC_PARAM 10

/*==========================================
 * TCMD definitions
 *=========================================*/
#define LOGGER_TCMD_MINOR           MISC_DYNAMIC_MINOR
#define LOGGER_TCMD_NAME            "aplog"

/*==========================================
 * Device definitions
 *=========================================*/
#define LOGGER_DEVICE_MINOR         MISC_DYNAMIC_MINOR
#define LOGGER_DEVICE_NAME          "aplog_input"
#define LOGGER_OUT_DEVICE_MINOR     MISC_DYNAMIC_MINOR
#define LOGGER_OUT_DEVICE_NAME      "aplog_output"


/*==========================================
 * Config file definitions
 *=========================================*/
#define CONFIG_STR_COLLECT              "collect="
#define CONFIG_STR_AP_PANIC_CORE_SIZE   "ap_panic_core_size="
#define CONFIG_STR_AP_PANIC_MAX_CORES   "ap_panic_max_cores="
#define CONFIG_STR_AP_PANIC_CORE_LOC    "ap_panic_core_loc="
#define CONFIG_STR_AP_PANIC_CORE_PATH   "ap_panic_core_path="
#define CONFIG_STR_LOG_PRINTK           "log_printk="
#define CONFIG_STR_RAM_BUFF_PAGES       "ram_buff_pages="
#define CONFIG_STR_FORK_EXEC            "fork_exec="
#define CONFIG_STR_LOG_NET              "log_net="
#define CONFIG_STR_OUT_BUFFER           "output_buffer_size="


/*==========================================
 * Log device status bitmasks
 *=========================================*/
#define LOGGER_INPUT_DEVICE_INSTALLED           0x01
#define LOGGER_TCMD_DEVICE_INSTALLED            0x02
#define LOGGER_OUTPUT_DEVICE_INSTALLED          0x04

/*==========================================
 * New variable types
 *=========================================*/

struct logger_inode_elem {
    struct list_head list;
    unsigned long inode_num;
    dev_t inode_dev;
    ssize_t (*orig_write)(struct file * file, const char * buf, size_t count,
            loff_t *ppos);
};


/*==========================================
 * Function prototypes
 *=========================================*/



/* Circular buffer functions */
static inline void CIRC_COPY_IN(char *cir_buf, const char * src_buf,
        int head, int tail, int bufsize, int cnt);
static inline void CIRC_COPY_OUT(char *cir_buf, char * dest_buf, int head,
        int tail, int bufsize, int cnt);
int logger_cirbuf_write(int type,  int from_user, const char * buf,  int count);



/* File functions */
static struct file *logger_fget_by_task(struct task_struct * task, int fd);

/* Logging processing functions */
static void logger_printk_set_level(void);


/* Replacement syscall functions */
static int logger_new_null_ioctl(struct inode * inode, struct file * file,
        unsigned int cmd, unsigned long arg);
static long logger_new_open(const char * filename, int flags, int mode);


/* Hook / Unhook calls */
static int logger_hookstdoutfops(void);
static int logger_hookfdtty(int fd);
static int logger_hookfdtty_by_task(struct task_struct * task, int fd);
static int logger_unhookwriteops(void);
static int logger_unhookfdfops(void);
static int logger_hookall(void);
static int logger_unhookall(void);


/* Console driver functions */
static void logger_console_write(struct console *co, const char *s,
        unsigned count);


/* Configuration functions */
static void logger_load_config(void);


/* init and exit */
static int __init logger_init(void);
static void __exit logger_exit(void);


/* Miscellaneous functions */
static int atoi( char *s);
static int logger_add_inode(char *);
static void logger_inode_list_cleanup(void);
static void logger_hook_openfds(void);
static void logger_unhook_openfds(void);

/* Device functions */
static ssize_t logger_device_read(struct file *, char __user *, size_t,
                                  loff_t *);
static ssize_t logger_device_write(struct file *, const char __user *, size_t,
                                   loff_t *);
static unsigned int logger_device_poll(struct file *,
                                       struct poll_table_struct *);

static ssize_t logger_out_device_read(struct file *, char __user *, size_t,
                                      loff_t *);
static unsigned int logger_out_device_poll(struct file *,
                                           struct poll_table_struct *);

