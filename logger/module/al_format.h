/*********************************************************
**  File: al_format.h
**
**        Copyright 2006, 2007 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**  
*********************************************************/

/*********************************************************************
 * Revision History
 *  #     Date      Author          Comment
 * ===  ========== =============   ========================================
 *   1  10/03/2006  Motorola        Ported EzX logger to Linux 2.6. Added SUAPI,
 *                                  KERN_PANIC, APP_PANIC, and IP types to
 *                                  AL_LOG_TYPE_T. Added revision history.
 *   2  03/25/2007  Motorola        Modify for libaplog/aplogd support
 *
 *********************************************************************/


#ifndef AL_FORMAT_H
#define AL_FORMAT_H

#ifndef WIN32
#include <asm/byteorder.h>
#else
#define __LITTLE_ENDIAN_BITFIELD 1
#endif

typedef enum{
    AL_T_HK = 0,
    AL_T_TS,
    AL_T_MU,
    AL_T_MD,
    AL_T_FORK,
    AL_T_PTK,
    AL_T_STD,
    AL_T_EXE,
    AL_T_FILE,
    AL_T_PS,
    AL_T_VER,
    AL_T_MISC,
    AL_T_SUAPI = 64,
    AL_T_KERN_PANIC,
    AL_T_APP_PANIC,
    AL_T_IP
} AL_LOG_TYPE_T;

#define AL_T_NOHEAD 255

#define AL_T_LOW AL_T_HK
#define AL_T_HIGH AL_T_IP

#define AL_REPLAY 1
#define AL_NON_REPLAY 0

#define AL_MAGIC 0x83

#if defined(__arch_xscale__)
#define AL_LOG_DEV "/dev/aplog0"
#elif defined(__arch_um__)
#define AL_LOG_DEV "/dev/tts/aplog0"
#else
#define AL_LOG_DEV "/dev/tty1"
#endif

#ifdef FILE_LOG
#undef AL_LOG_DEV    
#define    AL_LOG_DEV "./testlog"
#endif

#if defined(__arch_um__)
#define vsnprintf(buf, cnt, fmt, arg...) vsprintf(buf, fmt, ## arg)
#endif


struct al_header_t{
    char magic;
    char type;
    unsigned short port_id;
    unsigned int timestamp;  // ms since boot
    unsigned int length;     // length of data portion in bytes
    unsigned short pid;
    char level;
    char reserved;
#ifndef WIN32
} __attribute__ ((packed));
#else
};
#endif

#endif

