/*********************************************************
**  File: suapi.h - SUAPI specific code for AP datalogger
**
**        Copyright 2006 Motorola, Inc.
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
**
**
*********************************************************/

/*********************************************************************
 * Revision History
 *  #   Date        Author          Comment
 * === ==========  ==============  =====================================
 *   1 10/03/2006   Motorola        Initial version.
 *                                    
 *
 *********************************************************************/

/*
 * NOTE: This header file is for suapi.c local use only. Items which
 * must be used in other .c files must go in common.h
 */


#if (!defined NOSUAPI) && (defined CONFIG_SUAPI)

/************************************************
 * SUAPI Logging Message Sizes
 ************************************************/
#define SIZE_HEADER_LOG_DATA 12
#define SIZE_HEADER_BUFFER_FULL SIZE_HEADER_LOG_DATA
#define LOGGER_BUFFER_MINIMUM_SEPARATION 1


/************************************************
 * SUAPI Module Exports
 ************************************************/
extern char* log_buffer;
extern int log_buffer_size;
extern unsigned int log_lost_bytes;
extern unsigned int log_attempted_bytes;
extern char* log_read_ptr;
extern char* log_write_ptr;
extern int log_buffer_overwritten;
extern int log_busy_count;
extern wait_queue_head_t log_wait_q;
extern spinlock_t log_buffer_lock;

/************************************************
 * AP Logger Module Externs
 ************************************************/
extern char suapi_thread_run;
extern struct completion suapi_thread_complete;


/************************************************
 * Function Prototypes
 ************************************************/
static int logger_thread_suapi(void *);

#endif /* !NOSUAPI && CONFIG_SUAPI */
