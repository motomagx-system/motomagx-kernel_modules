#!/bin/sh

#==============================================================================
#
#   File Name: logger.sh
#
#   General Description: This file executes the startup and shutdown procedures
#   for the AP Datalogger Module
#
#==============================================================================
#
#        Copyright (c) 2006-2007 Motorola, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# Revision History
#  #   Date         Author          Comment
# === ==========   ============    ============================
#   1 10/03/2006    Motorola        Initial version. 
#   2 03/28/2007    Motorola        Removed app_dump from script
#   3 04/11/2007    Motorola        Added APP_DUMP
#



INSMOD="/sbin/insmod"
RMMOD="/sbin/rmmod"
AP_PATH="/lib/modules/2.6.10_dev/kernel/drivers/aplogger"
AP_MOD_NAME="aplogger.ko"
AP_INPUT_DEV="/dev/aplog_input"
APP_DUMP_PATH="/lib/modules/2.6.10_dev/kernel/drivers/app_dump"
APP_DUMP_MOD_NAME="coredump.ko"

[ `/usr/bin/id -u` = 0 ] || exit 1

startup () {
    if [ -f ${APP_DUMP_PATH}/${APP_DUMP_MOD_NAME} ]; then
        echo "Starting AP Application Coredump"
        ${INSMOD} ${APP_DUMP_PATH}/${APP_DUMP_MOD_NAME} corefiledirectory="/ezxlocal/app_dump" aprcorefiledirectory="/ezxlocal/apr_app_dump"
    else
        echo "AP Application Coredump module doesn't exist; not starting"
    fi

    if [ -f ${AP_PATH}/${AP_MOD_NAME} ]; then
        echo "Starting AP Datalogger"
        ${INSMOD} ${AP_PATH}/${AP_MOD_NAME}
        if [ -c ${AP_INPUT_DEV} ]; then
            echo "Changing permissions on ${AP_INPUT_DEV}"
            chmod 666 ${AP_INPUT_DEV}
        else
            echo "No character device at ${AP_INPUT_DEV}"
        fi
    else
        echo "AP Datalogger module doesn't exist; not starting"
    fi
}

shutdown () {
    if [ -f ${AP_PATH}/${AP_MOD_NAME} ]; then
        echo "Stopping AP Datalogger"
        ${RMMOD} ${AP_MOD_NAME}
    else
        echo "AP Datalogger module doesn't exist; not stopping"
    fi

    if [ -f ${APP_DUMP_PATH}/${APP_DUMP_MOD_NAME} ]; then
        echo "Stopping AP Application Coredump"
        ${RMMOD} ${APP_DUMP_MOD_NAME}
    else
        echo "AP Application Coredump module doesn't exist; not stopping"
    fi
}

restart () {
    shutdown
    startup
}

case "$1" in
    start)
        startup
    ;;
    stop)
        shutdown
    ;;
    restart)
        restart
    ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
esac
