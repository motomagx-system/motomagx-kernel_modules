#!/bin/sh

#==============================================================================
#
#   File Name: appdump.sh
#
#   General Description: This file executes the startup and shutdown procedures
#   for the AP Coredump Module
#
#==============================================================================
#
#        Copyright (C) 2007-2008 Motorola, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# Revision History
#  #   Date         Author          Comment
# === ==========   ============    ============================
#   1 03/29/2007    Motorola        Initial version. 
#   2 05/14/2008    Motorola        Disable apr coredump according to apr.cfg. 
#



INSMOD="/sbin/insmod"
RMMOD="/sbin/rmmod"
APP_DUMP_PATH="/lib/modules/2.6.10_dev/kernel/drivers/app_dump"
APP_DUMP_MOD_NAME="coredump.ko"
APR_CFG_FILE="/usr/setup/apr.cfg"
APR_COREDUMP_CFG="enableCoreDumps"

[ `/usr/bin/id -u` = 0 ] || exit 1

startup () {
    if [ -f ${APR_CFG_FILE} ]; then
        APR_DUMP_ENABLE=`sed -n -e "/${APR_COREDUMP_CFG}/s/ //gp" ${APR_CFG_FILE} | sed -n -e "s/${APR_COREDUMP_CFG}/enableAprCoredump/p"`
    fi
    if [ -f ${APP_DUMP_PATH}/${APP_DUMP_MOD_NAME} ]; then
        echo "Starting AP Application Coredump"
        ${INSMOD} ${APP_DUMP_PATH}/${APP_DUMP_MOD_NAME} corefiledirectory="/ezxlocal/app_dump" aprcorefiledirectory="/ezxlocal/apr_app_dump" ${APR_DUMP_ENABLE}
    else
        echo "AP Application Coredump module doesn't exist; not starting"
    fi
}

shutdown () {
    if [ -f ${APP_DUMP_PATH}/${APP_DUMP_MOD_NAME} ]; then
        echo "Stopping AP Application Coredump"
        ${RMMOD} ${APP_DUMP_MOD_NAME}
    else
        echo "AP Application Coredump module doesn't exist; not stopping"
    fi
}

restart () {
    shutdown
    startup
}

case "$1" in
    start)
        startup
    ;;
    stop)
        shutdown
    ;;
    restart)
        restart
    ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
esac
