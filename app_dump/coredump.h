/*
 * Copyright (C) 2005-2006 Motorola Inc.
 *
 * This Program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version. You should have received a copy of the GNU
 * General Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA
 *
 * Filename: coredump.h
 *
 * Purpose: This is a header file for the AP application coredump module,
 *          implemented by coredump.c
 *
 * ChangeLog:
 * Date (mm/dd/yyyy)        Author        Comment
 * 11/15/2005               Motorola      Initial creation
 * 12/15/2006               Motorola      Added power_ic_kernel.h include
 *
 */

#ifndef COREDUMP_H
#define COREDUMP_H

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/time.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/stat.h>
#include <linux/sched.h>
#include <linux/version.h>
#include <linux/smp_lock.h>
#include <linux/proc_fs.h>
#include <linux/interrupt.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/rtc.h>
#include <linux/spinlock.h>
#include <linux/power_ic_kernel.h>
#include <asm/system.h>
#include <asm/uaccess.h>
#include <asm/fcntl.h>
#include <asm/unistd.h>
#include <asm/ptrace.h>
#include <asm/rtc.h>

#define DRIVER_VERSION "v0.1.0"
#define DRIVER_AUTHOR "Motorola"
#define DRIVER_DESC "Linux-Java Port of EzX AP Application Core Dump Module"
#define DRIVER_LICENSE "GPL"

#ifdef DEBUG
#define DPRINT(fmt, args...) printk(fmt, ##args)
#else
#define DPRINT(fmt, args...)
#endif

#ifndef UNUSED_PARAM
#define UNUSED_PARAM(v) (void)(v)
#endif

/* macro for core file name max size */
#define FILENAMESIZE 128 // path(64)+core(4)+.(1)+comm(16)+.(1)+num(1)+'\0'(1)
/* offset for stack top, which should backward 72 bytes(18 registers * 4 bytes) */
#define FRAMESIZE 72

/* emergency log information */
#define EMG_PROC_ENTRY_NAME "emg"
#define EMG_LOG_FILE_NAME "log.emg"
#define EMG_LOG_FILE_NAME_BAK "log.emg.old"
#define EMG_ERROR_CODE_SIZE sizeof(unsigned long)
#define EMG_LOG_START_FLAG "<EMG>"
#define EZM_LOG_FILE_MAX 2048

/* below is used for reading bp & ap version file */
#define BEGIN_KMEM { mm_segment_t old_fs = get_fs(); set_fs(get_ds()); 
#define END_KMEM set_fs(old_fs); }

#define LOGGER_VERSION_BUFFER_MAX 256
#define LOGGER_VERSION_BP "/tmp/VersionInfo"
#define LOGGER_VERSION_AP "/etc/ap_version.txt"

#define _read(f, buf, sz) (f->f_op->read(f, buf, sz, &f->f_pos)) 
#define READABLE(f) (f->f_op && f->f_op->read) 

/* align output based on 4 bytes, duplicated from fs/binfmt_elf.c */
#define roundup(x, y)  ((((x)+((y)-1))/(y))*(y))

/* 
 * Below are the structures for the core dump file.
 * XX_note contains the size information for uncertain variables.
 */

struct panic_regs
{
	struct pt_regs * regs;
	unsigned long flags;
	unsigned int irq_on;    	// interrupts_enabled: 1 - "n", 0 - "ff"
	unsigned int fiq_on;    	// fast_interrupts_enabled: 1 - "n", 0 - "ff"
	unsigned int processor_mode;
	unsigned int thumb_mode;
	unsigned int psegment;  	// 1 - "kernel", 0 - "user" 
} __attribute__ ((packed));

/* for general registers */
struct panic_regs_note
{
	unsigned long pregsz;
	unsigned long flags;
	unsigned int irq_on; 		// interrupts_enabled: 1 - "n", 0 - "ff"
	unsigned int fiq_on; 		// fast_interrupts_enabled: 1 - "n", 0 - "ff"
	unsigned int processor_mode;
	unsigned int thumb_mode;
	unsigned int psegment; 		// 1 - "kernel", 0 - "user" 
} __attribute__ ((packed));

/* for task info */
struct panic_tm {
	int tm_sec;
	int tm_min;
	int tm_hour;
	int tm_mday;
	int tm_mon;
	int tm_year;
} __attribute__ ((packed));

struct panic_task
{
	char * pname;
	pid_t pid;
	long psignr;
	struct panic_tm *ptime;
} __attribute__ ((packed));

struct panic_task_note
{
	long pnamesz;
	long ptimesz;
	pid_t pid;
	long psignr;
} __attribute__ ((packed));

/* for memory map lines */
struct panic_mmap
{
	unsigned long vm_start;
	unsigned long vm_end;
	int flags;
	char * objname;
} __attribute__ ((packed));

struct panic_mmap_note
{
	long objnamesz;
	unsigned long vm_start;
	unsigned long vm_end;
	int flags;
} __attribute__ ((packed));

/* for stack array size */
struct panic_stack_note
{
	long stacksz;
} __attribute__ ((packed));

/* for version string size */
struct panic_version_note
{
	long bp_ap_version_sz;
} __attribute__ ((packed));

#endif /* COREDUMP_H */

