/*
 * Copyright (C) 2005-2008 Motorola,Inc.
 *
 * This Program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTIBILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version. You should have received a copy of the GNU
 * General Public License along with this program; if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA
 *
 * Filename: coredump.c
 *
 * Purpose: This file implements the mechanism to dump a smaller sized 
 *          core of the application, when it crashes.
 *
 * ChangeLog:
 * Date (mm/dd/yyyy)        Author        Comment
 * 11/15/2005               Motorola      Initial creation
 * 11/15/2006               Motorola      Changes timestamp to use secure clock
 * 03/20/2007               Motorola      Added PC and LR to APR dumps
 * 03/12/2008		    Motorola      Support multi-thread and parse partial dump
 * 05/14/2008               Motorola      Changes APR dump setting
 * 06/24/2008               Motorola      Make aplog do coredump to MMC/SD card if it is available
 */

#include "coredump.h"

/* module information */
MODULE_AUTHOR( DRIVER_AUTHOR );
MODULE_DESCRIPTION( DRIVER_DESC );
MODULE_LICENSE( DRIVER_LICENSE );

/* ensure that only a single crashed application is allowed to dump its core at a time */
DECLARE_MUTEX(app_coredump_mutex);

/* 
 * Default values for the configurable module parameters:
 *    Maximum Core File Size = 10Kbytes
 *    Maximum Core Files Per Application = 4
 *    Maximum APR Core Files Per Application = 10
 *    Maximum Core Files On Phone = 10
 *    Maximum APR Core Files On Phone = 1000
 *    Core File Directory = NULL
 *    APR Core File Directory = NULL
 *    APR coredump enabled
 */
static unsigned int maxcorefilesize       = 10240;
static unsigned int maxcorefilesperapp    = 4;
static unsigned int maxaprcorefilesperapp = 10;
static unsigned int maxcorefiles          = 10;
static unsigned int maxaprcorefiles       = 1000;
static unsigned int enableAprCoredump       = 1;

static char corefiledirectory[FILENAMESIZE] = {0, };
static char aprcorefiledirectory[FILENAMESIZE] = {0, };
module_param(maxcorefilesize, uint, S_IRUGO|S_IWUSR);
module_param(maxcorefilesperapp, uint, S_IRUGO|S_IWUSR);
module_param(maxaprcorefilesperapp, uint, S_IRUGO|S_IWUSR);
module_param(maxcorefiles, uint, S_IRUGO|S_IWUSR);
module_param(maxaprcorefiles, uint, S_IRUGO|S_IWUSR);
module_param(enableAprCoredump, uint, S_IRUGO|S_IWUSR);
module_param_string(corefiledirectory, corefiledirectory, sizeof(corefiledirectory), S_IRUGO|S_IWUSR);
module_param_string(aprcorefiledirectory, aprcorefiledirectory, sizeof(aprcorefiledirectory), S_IRUGO|S_IWUSR);

/* it is used to record the version string and backtrace based on FP */
static char *panic_core_buffer = NULL;
/* counter to keep track of the total number of core files generated */
static unsigned int* update_core_count;

static unsigned int current_core_file_count    = 0;
static unsigned int current_aprcore_file_count = 0;

/* forward function declarations */
static int panic_dump_regs(struct panic_regs *pregs, struct pt_regs *gregs, mm_segment_t fs, int core_dump_level);
static int panic_dump_mmap (struct panic_mmap *pmmap, struct vm_area_struct *map);
static void panic_dump_time(struct panic_tm *tm);
static int panic_read_tiny_file(char * filename, char * buf, int count);
static int panic_get_version(char *buf, unsigned long bufsize);
static long panic_dump_task(struct panic_task *ptask, task_t * p, long signr);
static int panic_set_file_path(char *file_dir, char *file_name, char *file_path, int max_path_size);

struct aplog_coredump_cfg{
    unsigned int maxcorefilesize;
    unsigned int maxcorefilesperapp;
};
extern void (*get_aplog_coredump_cfg)(struct aplog_coredump_cfg * apcore);
extern struct rw_semaphore get_aplog_coredump_cfg_lock;
static void provide_aplog_coredump_parameter(struct aplog_coredump_cfg * apcore)
{
    apcore->maxcorefilesize = maxcorefilesize;
    apcore->maxcorefilesperapp = maxcorefilesperapp;
}



/* core dump stub exported by fs/exec.c */
extern int (*aplog_do_coredump)(long , struct pt_regs * , int, int);
/* core dump stub lock exported by fs/exec.c */
extern struct rw_semaphore aplog_do_coredump_lock;
/* core_print stub exported by arch/arm/kernel/traps.c */
extern int (*core_print)(const char *fmt, ...);
/* core_print stub lock exported by arch/arm/kernel/traps.c */
extern struct rw_semaphore core_print_lock;
/* entry of backtrace.S */
extern void c_backtrace (unsigned long fp, int pmode);

/*
 * Need to keep this in-sync with fs/exec.c
 */
enum {
	NO_CORE_DUMP = 0,
	LIMITED_CORE_DUMP,
	FULL_CORE_DUMP,
	APR_CORE_DUMP
};

/*
 * Internal versions of the ELF core dump functions. Being redefined here to
 * avoid the need to export these symbols.
 */

/* Identical to dump_write(), inside fs/binfmt_elf.c */
static int dump_write_internal(struct file *file, const void *addr, int nr)
{
	return file->f_op->write(file, addr, nr, &file->f_pos) == nr;
}

/* Identical to dump_seek(), inside fs/binfmt_elf.c */
static int dump_seek_internal(struct file *file, off_t off)
{
	if (file->f_op->llseek) {
		if (file->f_op->llseek(file, off, 0) != off)
			return 0;
	} else
		file->f_pos = off;
	return 1;
}

static int g_dump_write_err = 0;

/* Identical to the one defined inside fs/binfmt_elf.c */
#define DUMP_WRITE(addr, nr)    \
	if (dump_write_internal(file, (addr), (nr)) < nr) \
		g_dump_write_err = -1;

/* Identical to the one defined inside fs/binfmt_elf.c */
#define DUMP_SEEK(off)  \
	if (!dump_seek_internal(file, (off))) \
		g_dump_write_err = -1;

/*
 * panic_dump_regs() - dump general registers and flags into core file
 */
static int panic_dump_regs(struct panic_regs *pregs, struct pt_regs *gregs, mm_segment_t fs, int core_dump_level)
{
	int i;

	if (pregs == NULL || gregs == NULL || !user_mode(gregs)) {
		DPRINT("[%s]error: invalid arguments passed\n", __FUNCTION__);
		return -1;
	}

	pregs->regs = gregs;
	/* zero out R0-R12 regs if a limited version of the core needs to be generated */
	if (core_dump_level == LIMITED_CORE_DUMP) {
		for (i=0; i<13; i++)
			(pregs->regs->uregs)[i] = 0;
	}
	pregs->flags = condition_codes(gregs);
	pregs->irq_on = interrupts_enabled(gregs);
	pregs->fiq_on = fast_interrupts_enabled(gregs);
	pregs->processor_mode = processor_mode(gregs);
	pregs->thumb_mode = thumb_mode(gregs);

	if(fs == get_ds())
		pregs->psegment = 1;
	else
		pregs->psegment = 0;

	return 0;
}

/* 
 * panic_dump_mmap() - dump task memory map lines into core file
 */
static int panic_dump_mmap (struct panic_mmap *pmmap, struct vm_area_struct *map)
{
	char *line, *tmp;
	char str[5];
	int flags;
	int len;
	int ret = -1;
	char *fmt = "exiting...";

	if (pmmap == NULL || map == NULL) {
		fmt = "error: invalid arguments passed";
		goto panic_dump_mmap_exit;
	}

    	flags = map->vm_flags;
    	str[0] = flags & VM_READ ? 'r' : '-';
    	str[1] = flags & VM_WRITE ? 'w' : '-';
    	str[2] = flags & VM_EXEC ? 'x' : '-';
    	str[3] = flags & VM_MAYSHARE ? 's' : 'p';
    	str[4] = '\0';

	/* just dump the r-x(s/p) type pages, which is used for code section */
	if(str[0] == 'r' && str[1] == '-' && str[2] == 'x')
	{
		tmp = (char *)__get_free_page(GFP_KERNEL);
		if (tmp == NULL)
		{
			fmt = "error: cannot malloc page";
			goto panic_dump_mmap_exit;
		}
		memset(tmp, 0, PAGE_SIZE);
 
		if (map->vm_file != NULL) 
		{
			line = d_path(map->vm_file->f_dentry, map->vm_file->f_vfsmnt, tmp, PAGE_SIZE);
			if (IS_ERR(line)) {
				fmt = "error: invalid d_path entry";
				goto panic_dump_mmap_exit;
			}
			if(line < tmp)
				line = tmp;
		}
		else
			line = tmp;

		len = strlen(line) + 1;  
		pmmap->vm_start = map->vm_start; 
		pmmap->vm_end = map->vm_end;
		pmmap->flags = flags;
		pmmap->objname = (char*)kmalloc(len * sizeof(char), GFP_KERNEL);
		if (pmmap->objname == NULL) {
			fmt = "error: cannot malloc for pmmap->objname";
			goto panic_dump_mmap_exit;
		}
		memcpy(pmmap->objname, line, len);
		DPRINT("[%s] %d, %08lx, %08lx, %s, %s\n", __FUNCTION__ , len, pmmap->vm_start, 
			pmmap->vm_end, str, pmmap->objname);

		free_page((unsigned long)tmp);
		ret = 1;
		goto panic_dump_mmap_exit;
	}
	ret = 0;

panic_dump_mmap_exit:
	DPRINT("[%s]%s\n", __FUNCTION__, fmt);
	return ret;
}

/*
 * do_core_print() - it is called by dump_backtrace_entry() function, which in turn is invoked by backtrace.S
 */
static int do_core_print(const char *fmt, ...)
{
	va_list args;
	int printed_len = 0;
	unsigned int panic_core_buffer_strlen = 0;

	if (panic_core_buffer == NULL) {
		DPRINT("[%s]panic_core_buffer is NULL\n", __FUNCTION__);
		return -1;
	}

	panic_core_buffer_strlen = strnlen(panic_core_buffer, PAGE_SIZE);

	va_start(args, fmt);
	printed_len = vsnprintf(panic_core_buffer+panic_core_buffer_strlen, PAGE_SIZE-panic_core_buffer_strlen, fmt, args);
	va_end(args);

	return printed_len;
}

/* 
 * panic_dump_time() - save the current date and time information to the core file
 */
static void panic_dump_time(struct panic_tm *tm)  
{ 
	struct timeval tv; 
	struct rtc_time rtc_tm;

	power_ic_rtc_get_time(&tv);
	rtc_time_to_tm((unsigned long)tv.tv_sec, &rtc_tm);

	/*
	 * We need to keep the panic_tm structure - it is a different size and
	 * format from the rtc_time structure, and it needs to be written with
	 * the panic information.
	 */
	tm->tm_sec = rtc_tm.tm_sec;
	tm->tm_min = rtc_tm.tm_min;
	tm->tm_hour = rtc_tm.tm_hour;
	tm->tm_mday = rtc_tm.tm_mday;
	tm->tm_mon = rtc_tm.tm_mon;
	tm->tm_year = rtc_tm.tm_year;
}

/* 
 * panic_read_tiny_file() - read ap & bp version file 
 */
static int panic_read_tiny_file(char * filename, char * buf, int count)
{
	int tmp;
	int total;
	struct file * l_f;

    	if ((filename == NULL) || (buf == NULL) || (count <=0))
        	return -1;
    
    	l_f = filp_open(filename, O_RDONLY, 00600);

    	if (IS_ERR(l_f) || (l_f == NULL) ) {
		DPRINT("[%s]cannot open <%s>, file %ld\n", __FUNCTION__, filename, -PTR_ERR(l_f));
        	return -1;
    	}

    	if (!READABLE(l_f)) {
		DPRINT("[%s]cannot read <%s>\n", __FUNCTION__, filename);
        	filp_close(l_f, NULL);        
        	return -1;
    	}

    	total = 0;

    	BEGIN_KMEM;
    	while(total < count){
        	tmp = _read(l_f, buf + total, count - total);
        	if (tmp <= 0)
            		break;
        	total += tmp;
    	}
    	END_KMEM;

    	filp_close(l_f, NULL);

    	return total;
}

static int panic_get_version(char *buf, unsigned long bufsize)
{
	char tmp[LOGGER_VERSION_BUFFER_MAX];
	int count, len = 0;

	if (buf == NULL)
        	return -1;

	/* get bp version */
    	tmp[0] = 'B'; tmp[1] = 'V' ;tmp[2] = ':';
    	count = panic_read_tiny_file(LOGGER_VERSION_BP, tmp + 3, LOGGER_VERSION_BUFFER_MAX);
    	if (count < 0) 
        	count = 0;
    	tmp[count+3]='\0';

	DPRINT("[%s]BP Version = %s\n", __FUNCTION__, tmp);

	if(strlen(tmp) < bufsize - len -1)
        	len += snprintf(buf, (bufsize - len), "%s", tmp);
    	else 
        	return -1;

	buf += strlen(tmp);

	/* get ap version */
    	tmp[0] = 'A'; 
    	count = panic_read_tiny_file(LOGGER_VERSION_AP, tmp + 3, LOGGER_VERSION_BUFFER_MAX);
    	if (count < 0)
        	count = 0;    
    	tmp[count+3]='\0';
 
    	DPRINT("[%s]AP Version = %s\n", __FUNCTION__, tmp);

    	if(strlen(tmp) < bufsize - len -1)
        	len += snprintf(buf, (bufsize - len), "%s", tmp);
    	else
        	return -1;

	return len+1;
}

/* 
 * panic_dump_task() - dump task info into core file, including pid, the full name of task, signr and time
 */
static long panic_dump_task(struct panic_task *ptask, task_t * p, long signr)
{
	unsigned long len;
    	char *tmp, *line = NULL;
    	struct vm_area_struct *map;
    	int ret = -1;
	char *fmt = "exiting...";

	if ((p == NULL) || (p->mm == NULL) || (p->mm->mmap == NULL)) {
		fmt = "error: invalid arguments passed";
		goto panic_dump_task_exit;
	}

	map = p->mm->mmap;
 
    	tmp = (char *)__get_free_page(GFP_KERNEL);
    	if (tmp == NULL)
    	{
        	fmt = "error: cannot malloc page";
		goto panic_dump_task_exit;
    	}
    	memset(tmp, 0, PAGE_SIZE);

    	if (map->vm_file != NULL) 
    	{
        	line = d_path(map->vm_file->f_dentry, map->vm_file->f_vfsmnt, tmp, PAGE_SIZE);
		if (IS_ERR(line)) {
			fmt = "error: invalid d_path entry";
			goto panic_dump_task_freepage;
		}
    	}
	else {
		fmt = "error: map->vm_file is NULL";
		goto panic_dump_task_freepage;
	}

    	len = strlen(line)+1;

	ptask->pname = (char*)kmalloc(len*sizeof(char), GFP_KERNEL);
    	if (ptask->pname == NULL)
    	{
        	fmt = "error: cannot malloc for pname";
		goto panic_dump_task_freepage;
    	}
    	memcpy(ptask->pname, line, len);
    	ptask->pid = p->pid;
    	ptask->psignr = signr;
    	ptask->ptime = (struct panic_tm*)kmalloc(sizeof(struct panic_tm), GFP_KERNEL); 
    	if (ptask->ptime == NULL)
    	{
        	fmt = "error: cannot malloc for ptime";
		goto panic_dump_task_freepname;
    	}

    	ret = 0;
	panic_dump_time(ptask->ptime);   
	goto panic_dump_task_freepage;

panic_dump_task_freepname:
	kfree(ptask->pname);

panic_dump_task_freepage:
	free_page((unsigned long)tmp);

panic_dump_task_exit:
	DPRINT("[%s]%s\n",__FUNCTION__, fmt);
	return ret;
}

static int panic_count_fill_dir(void *__buf, const char *name, int namlen, loff_t offset, ino_t ino, unsigned int d_type)
{
	UNUSED_PARAM(__buf);
	UNUSED_PARAM(offset);
	UNUSED_PARAM(ino);
	UNUSED_PARAM(d_type);

	if (name == NULL || namlen < 0)
	{
		DPRINT("[%s]error: invalid parameters passed\n", __FUNCTION__);
		return -1;
	}

	/* if it is a core file, update the counter */
	if (strstr(name, "core"))
	{
		/* update the global current core file counter */
		(*update_core_count)++;
	}

	DPRINT("[%s]current_core_file_count = %u\n", __FUNCTION__, *update_core_count);

	return 0;
}

static int panic_collect_info(char *path, unsigned int* core_count)
{
	struct file *file = NULL;
	int error = 0;

	if (path == NULL)
	{
		DPRINT("[%s]error: invalid pathname\n", __FUNCTION__);
		return -1;
	}

	/* open the directory where coredump data is stored */
	file = filp_open(path, O_RDONLY | O_DIRECTORY, 0666);
	if (IS_ERR(file))
	{
		DPRINT("[%s]error: opening %s directory failed (%ld)\n", __FUNCTION__, path, PTR_ERR(file));
		return -1;
	}

	update_core_count = core_count;

	/* reset the global current core file counter */
	*update_core_count = 0;

	/* read the directory */
	BEGIN_KMEM;
	error = vfs_readdir(file, panic_count_fill_dir, NULL);
	END_KMEM;
	if (error < 0)
	{
		DPRINT("[%s]error: vfs_readdir failed, error=%d\n", __FUNCTION__, error);
		filp_close(file, NULL);
		return -1;
	}

	filp_close(file, NULL);
	return 0;
}


#define SD_APLOG_CORE_DUMP_DIR "/mmc/mmca1/app_dump"
#define sd_card_is_available() (sd_exist==1)
/* Core file size limit on MMC/SD card, set to no limit now */
#define SD_APLOG_CORE_MAX_FILE_SIZE 0xffffffff

/*
 * aplog_coredump() - it is the actual core dump function, porting from binfmt_elf.c
 * This is a two-pass process; first we find the offsets of the bits, and then they are
 * actually written out.  If we run out of core limit we just truncate.
 * If MMC/SD card is available we will dump a complete core to MMC/SD card.
 */
static int aplog_coredump(long signr, struct pt_regs * regs, int core_dump_level, int sd_exist)
{
	char corename[FILENAMESIZE] = "\0";

	/* defaulting to APR core dump values */
	char *corefiledir_ptr = aprcorefiledirectory;
	int maxcorefilesperapp_internal = maxaprcorefilesperapp;
    
    	struct file * file = NULL;
    	struct inode * inode = NULL;
    	int corenamesize = 0;
    	int i = 0, tmpret = 0;
	/* -1 = error, 0 = success */
	int retval = -1;
        int dump_mmap;
    
    	/* registers info */
    	struct panic_regs *pregs = NULL;
    	struct panic_regs_note pregs_note;
        long pc_register;
        long lr_register;

    	/* task info */
    	struct panic_task *ptask = NULL;
    	struct panic_task_note ptask_note;

    	/* memory map info */
    	struct vm_area_struct *map = NULL;
    	struct panic_mmap *pmmap = NULL;
    	struct panic_mmap_note pmmap_note;

    	/* user stack info */
    	unsigned long start_addr = 0, end_addr = 0;
    	unsigned long addr = 0;
    	struct panic_stack_note pstack_note;

    	/* ap & bp version info */
    	struct panic_version_note pversion_note;
    	long mmap_record = 0;
    	long pre_fpos = 0, cur_fpos = 0;

    	/* size of backtrace based on FP */
    	long btsize = 0;

	/* this string buffer will point to panic_core_buffer */
    	char *buf = NULL;
    
	mm_segment_t fs;
    	size_t size = 0;
    
	/*
     	 * limit: core dump file size limitation
     	 * currentsize: current core dump file size
     	 */
    	unsigned long limit = 0, currentsize = 0;
	
	/* For padding */
	int pad = 0;
    
	down(&app_coredump_mutex);

	/* Clear error status */
	g_dump_write_err = 0;

	/* for non-APR core dumps, choose the correct directory and max number of core files allowed per process */
	if (core_dump_level != APR_CORE_DUMP)
	{
		corefiledir_ptr = corefiledirectory;
		maxcorefilesperapp_internal = maxcorefilesperapp;
	}

        if (sd_card_is_available() && core_dump_level == FULL_CORE_DUMP)
        {
            corefiledir_ptr = SD_APLOG_CORE_DUMP_DIR;
            DPRINT("SD card is available, aplog full core dump to %s\n", corefiledir_ptr);
        }
    
	if (panic_set_file_path(corefiledir_ptr, "\0", corename, FILENAMESIZE) != 0)
	{
		goto fail;
	}

	/* exit out of this routine if the max. core file limit has been reached, for non-APR core dumps */
	if(core_dump_level == APR_CORE_DUMP)
	{
		if(panic_collect_info(corefiledir_ptr, &current_aprcore_file_count) != 0)
			goto fail;

		if(current_aprcore_file_count >= maxaprcorefiles)
		{
			DPRINT("[%s]error: max. apr core file limit of %u has been reached\n", __FUNCTION__, maxaprcorefiles);
			goto fail;
		}
	}
	else
	{
		if(panic_collect_info(corefiledir_ptr, &current_core_file_count) != 0)
			goto fail;

		if(current_core_file_count >= maxcorefiles)
		{
			DPRINT("[%s]error: max. core file limit of %u has been reached\n", __FUNCTION__, maxcorefiles);
			goto fail;
		}
	}

	corenamesize = strlen(corename);
    	if( corenamesize < FILENAMESIZE )
    	{
		snprintf(&corename[corenamesize], sizeof(corename)-corenamesize, "core.%s", current->comm);
    	}
    	else
    	{
		DPRINT("[%s]error: core file name is too long\n", __FUNCTION__);
        	goto fail;
    	}

	corenamesize=strlen(corename);
	for(i = 0; i < maxcorefilesperapp_internal; i++)
	{
		snprintf(&corename[corenamesize], sizeof(corename)-corenamesize, ".%d", i);
		corename[strlen(corename)]='\0';

		DPRINT("[%s]core file name is %s\n", __FUNCTION__, corename);

		/* O_NOFOLLOW: if pathname refer to a symbol link, it will fail to open the file */
		file = filp_open(corename, O_RDONLY | O_NOFOLLOW, 0400);
 
		if (IS_ERR(file))  
		{
			DPRINT("[%s]creating core file %s\n", __FUNCTION__, corename);
			break;
		}
		else
		{
			DPRINT("[%s]core file %s existed\n", __FUNCTION__, corename);
			filp_close(file, NULL);
		}
	}

	if (i < maxcorefilesperapp_internal)
	{
		DPRINT("[%s]trying to open/create core file name = %s\n", __FUNCTION__, corename);
		file = filp_open(corename, O_CREAT | O_RDWR | O_NOFOLLOW, 0666);
		if (IS_ERR(file)) {
			DPRINT("[%s]error: could not open file %s\n", __FUNCTION__, corename);
			goto fail;
		}
	}
	else
	{
		DPRINT("[%s]error: exceeded the number of core files for a process, which is %d\n", __FUNCTION__, maxcorefilesperapp_internal);
		goto fail;
	}
    
    	inode = file->f_dentry->d_inode;

    	if ((inode->i_nlink > 1) || (d_unhashed(file->f_dentry)) || (!S_ISREG(inode->i_mode)) || 
		(!file->f_op) || (!file->f_op->write) || (do_truncate(file->f_dentry, 0) != 0)) {
        	goto close_fail;
    	}

    	/* limit is the max size of core file */
    	limit = maxcorefilesize;

        if (sd_card_is_available() && core_dump_level == FULL_CORE_DUMP)
        {
            limit = SD_APLOG_CORE_MAX_FILE_SIZE;
        }
    
    	panic_core_buffer = (char *)__get_free_page(GFP_KERNEL);
    	if (panic_core_buffer == NULL)
    	{
		DPRINT("[%s]error: cannot malloc page for panic_core_buffer\n", __FUNCTION__);
        	goto close_fail;
    	}

    	DPRINT("[%s]malloc %lu for panic_core_buffer, limit=%lu\n", __FUNCTION__, PAGE_SIZE, limit);

    	memset(panic_core_buffer, 0, PAGE_SIZE);
    	buf = panic_core_buffer;
            
    	fs = get_fs();
    	set_fs(get_ds());

	/* Dump bp & ap version into core file */
    	DPRINT("[%s]Dumping AP/BP version information...\n", __FUNCTION__);
    	pversion_note.bp_ap_version_sz = panic_get_version(buf, PAGE_SIZE);
    	if (pversion_note.bp_ap_version_sz == -1)
    	{
		DPRINT("[%s]error: could not get AP/BP version information\n", __FUNCTION__);
        	goto end_coredump;
    	}
	/* only dump the version information if both the version note and size can fit into the core file */
	if ((currentsize += pversion_note.bp_ap_version_sz + sizeof(pversion_note)) <= limit) {
		DUMP_WRITE(&pversion_note, sizeof(pversion_note));
		/* dump version string - include '\0'*/
		DUMP_WRITE(buf, pversion_note.bp_ap_version_sz);
		currentsize += 4 - (file->f_pos % 4);
		DUMP_SEEK(roundup((unsigned long)file->f_pos, 4));
	}
	else {
		goto end_coredump;
	}

    	/* Dump registers into core file, actually pt_regs is long uregs[18], refer to ptrace.h*/
	DPRINT("[%s]Dumping registers...\n", __FUNCTION__);
    	pregs = (struct panic_regs *) kmalloc(sizeof(struct panic_regs), GFP_KERNEL);
    	if (pregs == NULL)
	{
		DPRINT("[%s]error: cannot malloc for panic_regs\n", __FUNCTION__);
        	goto end_coredump;
    	}

	if (panic_dump_regs(pregs, regs, fs, core_dump_level) == -1)
    	{
		goto free_pregs;
    	}

    	pregs_note.pregsz = sizeof(struct pt_regs);
    	pregs_note.flags = pregs->flags;
    	pregs_note.irq_on = pregs->irq_on;
	pregs_note.fiq_on = pregs->fiq_on;
    	pregs_note.processor_mode = pregs->processor_mode;
    	pregs_note.thumb_mode = pregs->thumb_mode;
    	pregs_note.psegment = pregs->psegment;

        if(core_dump_level == APR_CORE_DUMP)
        {
                if((currentsize += sizeof(pregs_note) + sizeof(pregs->regs->ARM_pc) + sizeof(pregs->regs->ARM_lr)) > limit)
                        goto free_pregs;

                if(current->mm)
                        map = current->mm->mmap;

                pc_register = 0xFFFFDEAD;
                lr_register = 0xFFFFDEAD;

                while (map)
                {
                        if(map->vm_flags&VM_EXEC)
                        {
                                if(pregs->regs->ARM_pc >= map->vm_start && pregs->regs->ARM_pc < map->vm_end)
                                        pc_register = pregs->regs->ARM_pc; 

                                if(pregs->regs->ARM_lr >= map->vm_start && pregs->regs->ARM_lr < map->vm_end)
                                        lr_register = pregs->regs->ARM_lr;
                        }

                        map = map->vm_next;
                }

                DUMP_WRITE(&pregs_note, sizeof(pregs_note));
                DUMP_WRITE(&pc_register, sizeof(pregs->regs->ARM_pc));
                DUMP_WRITE(&lr_register, sizeof(pregs->regs->ARM_lr));
        }
        else
        {
                /* only dump the registers' information if both the sections can fit into the core file */
                if ((currentsize += sizeof(pregs_note) + pregs_note.pregsz) <= limit) {
                        DUMP_WRITE(&pregs_note, sizeof(pregs_note));
                        DUMP_WRITE(pregs->regs, pregs_note.pregsz);
                }
                else
                {
                        goto free_pregs;
                }
        }

    	/* Dump task info into core file, including pid, pname, signr, time */
	DPRINT("[%s]Dumping task information...\n", __FUNCTION__);
	ptask = (struct panic_task *) kmalloc(sizeof(struct panic_task), GFP_KERNEL);
    	if (ptask == NULL)
    	{
        	DPRINT("[%s]cannot malloc for struct panic_task\n", __FUNCTION__);
        	goto free_pregs;
    	}

    	tmpret = panic_dump_task(ptask, current, signr); 
    	if (tmpret == -1)
    	{
		goto free_ptask_pname_ptime;
    	}
   
    	ptask_note.pnamesz = strlen(ptask->pname)+ 1;
    	ptask_note.ptimesz = sizeof(struct panic_tm);
    	ptask_note.pid = ptask->pid;
    	ptask_note.psignr = ptask->psignr;

	if ((currentsize += sizeof(ptask_note) + ptask_note.pnamesz + ptask_note.ptimesz) <= limit) {
		DUMP_WRITE(&ptask_note, sizeof(ptask_note));
		DUMP_WRITE(ptask->pname, ptask_note.pnamesz);
		currentsize += 4 - (file->f_pos % 4);
		DUMP_SEEK(roundup((unsigned long)file->f_pos, 4));
		DUMP_WRITE(ptask->ptime, ptask_note.ptimesz);
	}
	else {
		goto free_ptask_pname_ptime;
	}

	/* Dump task memory map into core file, including start_addr, end_addr, type, obj file name */
	DPRINT("[%s]Dumping task memory map information...\n", __FUNCTION__);
    	if(current->mm)
        	map = current->mm->mmap;
    
    	/* keep 4 bytes to record the mmap size */
    	pre_fpos = file->f_pos;
    	DUMP_WRITE(&mmap_record, sizeof(long));

	pmmap = (struct panic_mmap *) kmalloc(sizeof(struct panic_mmap), GFP_KERNEL);
	if (pmmap == NULL) {
		DPRINT("[%s]error: cannot malloc for panic_mmap\n", __FUNCTION__);
		goto free_ptask_pname_ptime;
	}

	get_user(start_addr, (int __user *)&regs->ARM_sp);

	/* the addr is stack top, which should backward 72 bytes(18 registers * 4 bytes) */
	addr      = start_addr - FRAMESIZE;
	end_addr  = 0;
	dump_mmap = 1;

	while (map)
	{
		if(addr >= map->vm_start && addr < map->vm_end && (map->vm_flags&(VM_READ|VM_WRITE)))
			end_addr = map->vm_end;

		if(dump_mmap)
		{
			memset((char *) pmmap, 0, sizeof(struct panic_mmap));
			tmpret = panic_dump_mmap(pmmap, map);
			if (tmpret == -1)
			{
				goto free_pmmap;
			}
			else if (tmpret == 1)
			{
				mmap_record++;
				pmmap_note.objnamesz = strlen(pmmap->objname)+ 1;
				pmmap_note.vm_start = pmmap->vm_start;
				pmmap_note.vm_end = pmmap->vm_end;
				pmmap_note.flags = pmmap->flags;

				if (currentsize+sizeof(pmmap_note)+pmmap_note.objnamesz <= limit) {
					currentsize += sizeof(pmmap_note) + pmmap_note.objnamesz;
					DUMP_WRITE(&pmmap_note, sizeof(pmmap_note));
					DUMP_WRITE(pmmap->objname, pmmap_note.objnamesz);
					if(file->f_pos % 4)
						DUMP_WRITE(&pad, 4 - (file->f_pos % 4));
					kfree(pmmap->objname);
				}
				else {
					dump_mmap = 0;
					mmap_record--;

					kfree(pmmap->objname);
				}
			}
		}

		map = map->vm_next;
	}

	if(mmap_record)
    	{
        	cur_fpos = file->f_pos;
        	DUMP_SEEK(pre_fpos);
        	DUMP_WRITE(&mmap_record, sizeof(long));
        	DUMP_SEEK(cur_fpos);
    	}

	if(!end_addr)
	{
		printk(KERN_WARNING"sp might be corrupted. %x.\n", start_addr);
		goto free_pmmap;
	}

	/* Dump user stack into core file */
	DPRINT("[%s]Dumping user stack information...\n", __FUNCTION__);
	DPRINT("[%s]sp_top = %x, sp_bottom = %x\n", __FUNCTION__, addr, end_addr);

	pstack_note.stacksz = end_addr - addr;
	if(currentsize + sizeof(pstack_note) + pstack_note.stacksz > limit)
	{
		/* If we couldn't dump full stack, dump part */
		pstack_note.stacksz = limit - (currentsize + sizeof(pstack_note));
		printk(KERN_WARNING"Coredump: Only part of stack dumped.%d/%d.\n", pstack_note.stacksz, (int)(end_addr - addr));
	}


	DPRINT("[%s](uc*)end_addr=%x, (uc*)addr=%x, stacksz=%ld\n", __FUNCTION__, end_addr, addr, pstack_note.stacksz);

	currentsize += sizeof(pstack_note) + pstack_note.stacksz;
	DUMP_WRITE(&pstack_note, sizeof(pstack_note));
	/* only dump the stack if a full version of the core is required */
	if (core_dump_level == FULL_CORE_DUMP) {
		DUMP_WRITE((char *)addr, pstack_note.stacksz);
		if(file->f_pos % 4)
			DUMP_WRITE(&pad, 4 - (file->f_pos % 4));
	}

    	/* Dump backtrace with the FP register. It is available when the compiler optimization is turned off */
	DPRINT("[%s]Dumping backtrace information...\n", __FUNCTION__);
    	if(strlen(panic_core_buffer) < PAGE_SIZE - 1)
        	buf = panic_core_buffer + strlen(panic_core_buffer);
    	else
    	{
        	DPRINT("[%s]error: panic_core_buffer is full\n", __FUNCTION__);
		goto free_pmmap;
    	}

    	c_backtrace(regs->ARM_fp, processor_mode(regs));
    	btsize = strlen(buf) + 1;

    	DPRINT("[%s]btsize = %ld\n", __FUNCTION__, btsize);

	/* only dump the backtrace to the core file, if all the sections can fit */
	if ((currentsize+=sizeof(long)+btsize*sizeof(char)) <= limit) {
		if (core_dump_level != APR_CORE_DUMP) {
			DUMP_WRITE(&btsize, sizeof(long));
			DUMP_WRITE(buf, btsize*sizeof(char));
			currentsize += 4 - (file->f_pos % 4);
			if(file->f_pos % 4)
				DUMP_WRITE(&pad, 4 - (file->f_pos % 4));
		}
	}
	else {
		/* Only dump bt size. 4 bytes - not too much */
		currentsize -= btsize*sizeof(char);
		btsize = 0;
		DUMP_WRITE(&btsize, sizeof(long));
	}

	/* success case */
	retval = 0;

free_pmmap:
	kfree(pmmap);

free_ptask_pname_ptime:
	if (ptask != NULL) {
		kfree(ptask->pname);
		kfree(ptask->ptime);
	}
	kfree(ptask);

free_pregs:
	kfree(pregs);

end_coredump:
	set_fs(fs);

close_fail:    
	filp_close(file, NULL);

fail:
	DPRINT("[%s]free panic_core_buffer\n", __FUNCTION__);
	if(panic_core_buffer != NULL)
	{
		DPRINT("[%s]panic_core_buffer is not NULL\n", __FUNCTION__);
		free_page((unsigned long)panic_core_buffer);
		panic_core_buffer = NULL;
	}
	if(!g_dump_write_err)
	{
		printk("Partial coredump error!\n");
		g_dump_write_err = 0;
	}
	up(&app_coredump_mutex);

	return retval;
}

/*
 * Wrapper for aplog_coredump function, in order to satisfy security/APR
 * requirements. Need to generate two types of core for different purposes.
 */
static int aplog_do_coredump_new(long signr, struct pt_regs * regs, int core_dump_level, int sd_exist)
{
	int retval = 0;

	/* Don't coredump if core_dump_level 0 and apr coredump is disabled. */
	if((core_dump_level == NO_CORE_DUMP) && !enableAprCoredump)
	{
		printk(KERN_EMERG"No coredump in product/ship phone.\n");
		return -1;
	}

	/* Dump the special coredump for APR */
	if ((aplog_coredump(signr, regs, APR_CORE_DUMP, sd_exist) == -1)) {
		printk(KERN_EMERG "Error: Could not dump core to %s directory\n", aprcorefiledirectory);
		retval = -1;
	}
	if ((core_dump_level == NO_CORE_DUMP) || (aplog_coredump(signr, regs, core_dump_level, sd_exist) == -1)) {
		printk(KERN_EMERG "Error: Could not dump core to %s directory\n", corefiledirectory);
		retval = -1;
	}

	return retval;
}

static void hook_do_coredump(void)
{
	down_write(&aplog_do_coredump_lock);
    	aplog_do_coredump = aplog_do_coredump_new;
	up_write(&aplog_do_coredump_lock);
	down_write(&core_print_lock);
	core_print = do_core_print;
	up_write(&core_print_lock);
        down_write(&get_aplog_coredump_cfg_lock);
        get_aplog_coredump_cfg = provide_aplog_coredump_parameter;
        up_write(&get_aplog_coredump_cfg_lock);
}

static int panic_set_file_path(char *file_dir, char *file_name, char *file_path, int max_path_size)
{
	int currentsize;
	char *fmt = "exiting...";

	if ((!file_dir) || (!file_name) || (!file_path)) {
		fmt = "error: invalid log file path";
		goto panic_set_file_path_error;
	}

	currentsize = strlen(file_dir) + strlen(file_name) + 2;

    	if (currentsize < max_path_size) {
		if (snprintf(file_path, currentsize, "%s/%s", file_dir, file_name) != currentsize - 1) {
			fmt = "error: snprintf failed to write all bytes to the buffer";
			goto panic_set_file_path_error;
		}
    	}
    	else {
		fmt = "error: log file path is too long";
		goto panic_set_file_path_error;
	}

	return 0;

panic_set_file_path_error:
	DPRINT("[%s]%s\n", __FUNCTION__, fmt);
	return -1;
}

/*
 * Below is the code for the proc driver handler for /proc/emg which is for zEmergency.
 * Any write to /proc/emg will be redirected to a single file which maintains the
 * emergency log.
 */

static DECLARE_MUTEX(emg_file_sem);
static struct proc_dir_entry *emg_proc_entry = NULL;

extern asmlinkage long sys_rename(const char __user *oldname, const char __user *newname);
#define RENAME(old,new) sys_rename(old,new);

static int dump_seek_raw(struct file *file, off_t off, int whence)
{
        if (whence < 0 || whence > 2)
                return -1;

        if (file->f_op && file->f_op->llseek) {
                if (file->f_op->llseek(file, off, whence) != off)
                        return 0;
                else
                        return off;
        }

        return 0;
}

static ssize_t emg_write(struct file * file, const char * buf, size_t count, loff_t *ppos)
{
	struct file *tfilp;
	char init_status=0;
	char ver_buf[LOGGER_VERSION_BUFFER_MAX];
    	char emg_log_file[FILENAMESIZE] = "\0";
    	char emg_log_file_bak[FILENAMESIZE] = "\0";
    	int ver_size;
    	int tmp_pos;
    	int old_uid = -1, old_gid = -1;

    	UNUSED_PARAM(file);
    	UNUSED_PARAM(ppos);

    	if ((buf == NULL) || (count != EMG_ERROR_CODE_SIZE)) {
		DPRINT("[%s]trying to write invalid data to %s file\n", __FUNCTION__, EMG_LOG_FILE_NAME);
        	return -EINVAL;
	}

    	/* exclude any other access */
    	down(&emg_file_sem);

    	/* open emg log file, if not exist, create one with header */
    	if (panic_set_file_path(corefiledirectory, EMG_LOG_FILE_NAME, emg_log_file, FILENAMESIZE) == -1) {
		DPRINT("[%s]emg log file path is too long!\n", __FUNCTION__);
		goto exit_write;
	}

    	/* change to root to write the emg file */
    	old_uid = current->uid;
    	old_gid = current->gid;
    	current->uid = 0;
    	current->gid = 0;
    
    	tfilp = filp_open(emg_log_file, O_RDWR, 0600);
	if (PTR_ERR(tfilp) == -ENOENT ) {
        	tfilp = filp_open(emg_log_file, O_RDWR | O_CREAT, 0600);
        	init_status = 1;
    	}
    
	if (IS_ERR(tfilp)) {
		goto exit_write;
	}

    	BEGIN_KMEM;
    	/* write emg log header: version <emg> */
	if (init_status) {
        	ver_size = panic_get_version(ver_buf, LOGGER_VERSION_BUFFER_MAX);
        	if (ver_size > 0) {
            		/* we don't use safe write here because total write size is negligible;
              	   	   we assume write once is OK */
            		dump_write_internal(tfilp, ver_buf, ver_size);
       		}
		dump_write_internal(tfilp, EMG_LOG_START_FLAG, sizeof(EMG_LOG_START_FLAG));
	}

	/* seek to the end of the file */
	dump_seek_raw(tfilp, 0, 2);

    	/* write time stamp and error code */
    	dump_write_internal(tfilp, &xtime.tv_sec, sizeof(xtime.tv_sec));
    	dump_write_internal(tfilp, buf, count);

    	tmp_pos = tfilp->f_pos;

    	/* close file */
	filp_close(tfilp, NULL);

	if (tmp_pos >= EZM_LOG_FILE_MAX) {
    		if (panic_set_file_path(corefiledirectory, EMG_LOG_FILE_NAME_BAK, emg_log_file_bak, FILENAMESIZE) == -1) {
			DPRINT("[%s]emg log file bak path is too long!\n", __FUNCTION__);
			goto emg_write_end_kmem;
        	}
		RENAME(emg_log_file, emg_log_file_bak);
	}

emg_write_end_kmem:
	END_KMEM;

exit_write:   
	if (old_uid != -1)
		current->uid = old_uid;
	if (old_gid != -1)
		current->gid = old_gid;
	/* release semphone */
	up(&emg_file_sem);
	return count;
}

static struct file_operations proc_emg_operations = {
	write:     emg_write
};

static void emg_init (void) {
	emg_proc_entry = create_proc_entry(EMG_PROC_ENTRY_NAME, 0222, NULL);
	if (emg_proc_entry)
		emg_proc_entry->proc_fops = &proc_emg_operations;
}

static void emg_exit (void) {
	if (emg_proc_entry)
		remove_proc_entry(EMG_PROC_ENTRY_NAME, NULL);
}

/* 
 * Module initialization routine 
 */
static int __init coredump_init (void) {

	DPRINT("[%s]AP Application Core Dump Module Loaded\n", __FUNCTION__);

	emg_init();

	hook_do_coredump();
	return 0;
}

/*
 * Module cleanup routine
 */
static void __exit coredump_exit (void) {

    	DPRINT("[%s]AP Application Core Dump Module Removed\n", __FUNCTION__);

	down_write(&aplog_do_coredump_lock);
    	aplog_do_coredump = NULL;
	up_write(&aplog_do_coredump_lock);
	down_write(&core_print_lock);
	core_print = NULL;
	up_write(&core_print_lock);
        down_write(&get_aplog_coredump_cfg_lock);
        get_aplog_coredump_cfg = NULL;
        up_write(&get_aplog_coredump_cfg_lock);

    	emg_exit();
}

module_init(coredump_init);
module_exit(coredump_exit);

