/*

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 2 as
   published by the Free Software Foundation.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307	 USA

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
   or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU General Public License
   for more details.


   Copyright (C) 2006-2008 - Motorola

   Date		Author		 Comment
   -----------	--------------	 --------------------------------
   2006-Apr-28	Motorola	 The kernel module for running the Bluetooth(R)
				 Sleep-Mode Protocol from the Host side
   2006-Sep-08	Motorola	 Added workqueue for handling sleep work.
   2007-Jan-24	Motorola	 Added mbm_handle_ioi() call to ISR.
   2007-Feb-28  Motorola         Fix merge error
   2008-Feb-19  Motorola         Remove some dbg information
   
   2007-Sep-17  Motorola   Supporting BT/WLAN single antenna solution (HW_BT_WLAN_SINGLE_ANTENNA)
*/




#if defined(CONFIG_MODVERSIONS)
#define	MODVERSIONS
#include <linux/modversions.h>
#endif

#include <linux/module.h>	/* kernel module definitions */
#include <linux/config.h>
#include <linux/errno.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/notifier.h>
#include <linux/proc_fs.h>
#include <linux/spinlock.h>
#include <linux/timer.h>
#include <asm/uaccess.h>
#include <linux/version.h>
#include <linux/workqueue.h>
#include<linux/mpm.h>

#include <asm/irq.h>
#include <asm/arch/gpio.h>
#include <asm/mot-gpio.h>

#include <net/bluetooth/bluetooth.h>
#include <net/bluetooth/hci_core.h> /* event notifications */

//Can be moved to your build options
#define HW_BT_WLAN_SINGLE_ANTENNA

//Antenna switch pwr ctrl
#ifdef HW_BT_WLAN_SINGLE_ANTENNA
#include <linux/power_ic.h>
#include <linux/power_ic_kernel.h>
#include <linux/delay.h>
#endif //HW_BT_WLAN_SINGLE_ANTENNA
//End antenna switch pwr ctrl

/*
 * Defines
 */

#define VERSION		"2.0"
#define NAME		"bluesleep"
#define PROC_DIR	"bluetooth/sleep"

/* work function */
void bluesleep_sleep_work(void *arg);

/* work queue */
DECLARE_WORK(sleep_workqueue, bluesleep_sleep_work, NULL);

/* macros to abstract away dpm from the tx and rx state machines */
/* use the event thread in case we need to sleep at some point */
#define bluesleep_rx_busy()	schedule_work(&sleep_workqueue)
#define bluesleep_tx_busy()	schedule_work(&sleep_workqueue)
#define bluesleep_rx_idle()	schedule_work(&sleep_workqueue)
#define bluesleep_tx_idle()	schedule_work(&sleep_workqueue)

/* 1 second timeout */
#define TX_TIMER_INTERVAL	1

/* state variable names and bit positions */
#define BT_PROTO	0x01
#define BT_TXDATA	0x02
#define BT_ASLEEP	0x04

#ifdef HW_BT_WLAN_SINGLE_ANTENNA
//Antenna switch pwr ctrl
/* ATLAS REGISTERS defines */
#define ATLAS_START_CHANGE_BIT_9       9
#define ATLAS_START_CHANGE_BIT_23     23
#define ATLAS_START_CHANGE_BIT_21     21
#define ATLAS_START_CHANGE_BIT_3       3
#define ATLAS_START_CHANGE_BIT_15     15
#define ATLAS_START_CHANGE_BIT_4       4
#define NB_BITS_TO_CHANGE_3            3
#define NB_BITS_TO_CHANGE_1            1
#define NB_BITS_TO_CHANGE_2            2
#define VOLTAGE_3_0V_VAL_7             7
#define VOLTAGE_1_8V_VAL_0             0
#define VOLTAGE_1_8V_VAL_3             3
#define SET_ATLAS_BIT_TO_1             1 
#define SET_ATLAS_BIT_TO_0             0 

#define ANTENNA_REQUIRED               1
#define ANTENNA_NOT_REQUIRED           0
//End Antenna switch pwr ctrl
#endif // HW_BT_WLAN_SINGLE_ANTENNA

/* global pointer to a single hci device. */
static struct hci_dev *bluesleep_hdev;

/* module usage */
static atomic_t open_count = ATOMIC_INIT(1);

/*
 * Local function prototypes
 */

static int bluesleep_hci_event(struct notifier_block *this,
			    unsigned long event, void *data);

/*
 * Global variables
 */

/** Global state flags */
static unsigned long flags;

/** Tasklet to respond to change in hostwake line */
static struct tasklet_struct hostwake_task;

/** Transmission timer */
static struct timer_list tx_timer;

/** Lock for state transitions */
static spinlock_t rw_lock;

/** Notifier block for HCI events */
struct notifier_block hci_event_nblock = {
	notifier_call: bluesleep_hci_event,
};



/*
 * Local functions
 */

#ifdef HW_BT_WLAN_SINGLE_ANTENNA
/* antenna_switch_supply 
 * Description:
 *     Actually WLAN module power supply turn on/off.
 *     Allowing the needed supply for the antenna switch setting kept on
 * Parameters:
 *     POWER_IC_PERIPH_ONOFF_T onoff =: 
 *         POWER_IC_PERIPH_OFF - turn off
 *         POWER_IC_PERIPH_ON  - turn on
 * Return Value:
 *     None.
 */
static void antenna_switch_supply(POWER_IC_PERIPH_ONOFF_T onoff, int antennaRequest)
{
    if (POWER_IC_PERIPH_ON == onoff) 
    {
        /* VMMC2ESIMEN regulator setting*/
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REGEN_ASSIGN, ATLAS_START_CHANGE_BIT_23,
                           SET_ATLAS_BIT_TO_0, NB_BITS_TO_CHANGE_1);
                    
        /* VESIMESIMEN regulator setting*/
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REGEN_ASSIGN, ATLAS_START_CHANGE_BIT_21,
                           SET_ATLAS_BIT_TO_0, NB_BITS_TO_CHANGE_1);
                    
        /* VMMC2 regulator setting*/
        power_ic_set_reg_value(POWER_IC_REG_ATLAS_REG_SET_1, ATLAS_START_CHANGE_BIT_9,
                           VOLTAGE_3_0V_VAL_7, NB_BITS_TO_CHANGE_3);

        /* VDIG regulator setting*/
        power_ic_set_reg_value(POWER_IC_REG_ATLAS_REG_SET_0, ATLAS_START_CHANGE_BIT_4,
                           VOLTAGE_1_8V_VAL_3, NB_BITS_TO_CHANGE_2);
         
        /* VESIM regulator setting*/
        power_ic_set_reg_value(POWER_IC_REG_ATLAS_REG_SET_0, ATLAS_START_CHANGE_BIT_15,
                           VOLTAGE_1_8V_VAL_0, NB_BITS_TO_CHANGE_1);
                   
        /* Set VMMC2EN bit to 1 to power on VMMC2 regulator */
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REG_MODE_1 , ATLAS_START_CHANGE_BIT_21,
                           SET_ATLAS_BIT_TO_1, NB_BITS_TO_CHANGE_1);
                   
        /* Set VDIGEN bit to 1 to power on VDIG regulator */
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REG_MODE_0 , ATLAS_START_CHANGE_BIT_9,
                           SET_ATLAS_BIT_TO_1 , NB_BITS_TO_CHANGE_1);

        /* Set VESIMEN bit to 1 to power on VESIM regulator */
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REG_MODE_1 , ATLAS_START_CHANGE_BIT_3,
                           SET_ATLAS_BIT_TO_1 , NB_BITS_TO_CHANGE_1);
    }
    else
    {    

        /* Set VESIMEN bit to 0 to power off VESIM regulator */
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REG_MODE_1 , ATLAS_START_CHANGE_BIT_3,
                           SET_ATLAS_BIT_TO_0 , NB_BITS_TO_CHANGE_1);
                   
        /* Set VDIGEN bit to 0 to power off VDIG regulator */
        power_ic_set_reg_value( POWER_IC_REG_ATLAS_REG_MODE_0 , ATLAS_START_CHANGE_BIT_9,
                           SET_ATLAS_BIT_TO_0 , NB_BITS_TO_CHANGE_1);

        /* Set VMMC2EN bit to 0 to power off VMMC2 regulator */
        if(antennaRequest == ANTENNA_NOT_REQUIRED)
        {
            power_ic_set_reg_value( POWER_IC_REG_ATLAS_REG_MODE_1 , ATLAS_START_CHANGE_BIT_21,
                               SET_ATLAS_BIT_TO_0, NB_BITS_TO_CHANGE_1);
        }
    }
    return;
}

/* antenna_ctrl 
 * Description:
 *     Handles requests for the antenna switch to be enabled/disabled.
 * Parameters:
 *     Antenna switch request.
 * Return Value:
 *     None.
 */
static void antenna_ctrl(int antennaRequest)
{
    //Only handle antenna switch request if WLAN is not already on
    //If on, the switch is handled as it should already
    if(gpio_wlan_powerdown_get_data() == GPIO_DATA_LOW)
    {
        if(antennaRequest == ANTENNA_REQUIRED)
        {
            //Make sure the wlan device is powered up and reset, then powered down except for the
            //appropriate switch supply
            gpio_wlan_reset_set_data(GPIO_DATA_LOW);
            gpio_wlan_powerdown_set_data(GPIO_DATA_HIGH);
            
            // WLAN module power supply turn on 
            antenna_switch_supply(POWER_IC_PERIPH_ON, ANTENNA_REQUIRED);
            
            // Wait for 20ms and set WLAN_RESET pin to high
            mdelay(20); 
            gpio_wlan_reset_set_data(GPIO_DATA_HIGH);
            gpio_wlan_powerdown_set_data(GPIO_DATA_LOW);
        }
        //Shut down and leave appropiate supplies on
        antenna_switch_supply(POWER_IC_PERIPH_OFF, antennaRequest);
    }
}
#endif //HW_BT_WLAN_SINGLE_ANTENNA

/**
 * @return 1 if the Host can go to sleep, 0 otherwise.
 */
static inline int bluesleep_can_sleep(void)
{
	/* check if GPIO_BT_HOST_WAKE and GPIO_BT_WAKE are both deasserted */
	return ( gpio_bluetooth_hostwake_get_data() && gpio_bluetooth_wake_get_data());
 }

/**
 * Performs any actions necessary when entering/exiting the sleep state.
 */
void bluesleep_sleep_work(void *arg)
{
	if (bluesleep_can_sleep()) {
		/* already asleep, this is an error case */
		if (test_bit(BT_ASLEEP, &flags)) {
			BT_INFO("already asleep");
			return;
		}

		BT_DBG("going to sleep...");
		if (bluesleep_hdev && bluesleep_hdev->suspend)
			hci_suspend_dev(bluesleep_hdev);
		set_bit(BT_ASLEEP, &flags);
	} else {
		if (test_bit(BT_ASLEEP, &flags)) {

			BT_DBG("waking up...");
			if (bluesleep_hdev && bluesleep_hdev->resume)
				hci_resume_dev(bluesleep_hdev);
			clear_bit(BT_ASLEEP, &flags);
		} else {
			BT_DBG("already awake");
		}
	}
}

/**
 * A tasklet function that runs in tasklet context and reads the value
 * of the <code>HOST_WAKE</code> GPIO pin.
 * @param data Not used.
 */
static void bluesleep_hostwake_task(unsigned long data)
{
	BT_DBG("hostwake line change");

	spin_lock(&rw_lock);

	if (gpio_bluetooth_hostwake_get_data()) {
		bluesleep_rx_idle();
	} else {
		bluesleep_rx_busy();
	}

	spin_unlock(&rw_lock);
}

/**
 * Handles proper timer action when outgoing data is delivered to the
 * HCI line discipline. Sets BT_TXDATA.
 */
static void bluesleep_outgoing_data(void)
{
	unsigned int irq_flags;

	spin_lock_irqsave(&rw_lock, irq_flags);

	/* log data passing by */
	set_bit(BT_TXDATA, &flags);

	/* if the tx side is sleeping... */
	if (gpio_bluetooth_wake_get_data()) {

		BT_DBG("tx was sleeping");

		/* assert bt wake */
		gpio_bluetooth_wake_set_data(0);

		/* start the tx timer */
		tx_timer.expires = jiffies + (TX_TIMER_INTERVAL*HZ);
		add_timer(&tx_timer);

		bluesleep_tx_busy();
	}

	spin_unlock_irqrestore(&rw_lock, irq_flags);
}

/**
 * Handles HCI device events.
 * @param this Not used.
 * @param event The event that occurred.
 * @param data The HCI device associated with the event.
 * @return <code>NOTIFY_DONE</code>.
 */
static int bluesleep_hci_event(struct notifier_block *this, unsigned long event, void *data)
{
	struct hci_dev *hdev = (struct hci_dev *) data;

	if (!hdev)
		return NOTIFY_DONE;

	switch (event) {
	case HCI_DEV_REG:
		if (!bluesleep_hdev)
			bluesleep_hdev = hdev;
		break;
	case HCI_DEV_UNREG:
		bluesleep_hdev = NULL;
		break;

	case HCI_DEV_WRITE:
		bluesleep_outgoing_data();
		break;

	}

	return NOTIFY_DONE;
}

/**
 * Handles transmission timer expiration.
 * @param data Not used.
 */
static void bluesleep_tx_timer_expire(unsigned long data)
{
	unsigned long irq_flags;

	spin_lock_irqsave(&rw_lock, irq_flags);

//	BT_DBG("Tx timer expired"); //    2008-Feb-19  Motorola         Remove some dbg information

	/* were we silent during the last timeout? */
	if (!test_bit(BT_TXDATA, &flags)) {

		BT_DBG("Tx has been idle");

		/* clear BT_WAKE */
		gpio_bluetooth_wake_set_data(1);

		bluesleep_tx_idle();
	} else { /* data last period */
		/* reset the timer - better than modtimer because the it
		   already expired */

//		BT_DBG("Tx data during last period"); //    2008-Feb-19  Motorola         Remove some dbg information

		tx_timer.expires = jiffies + (TX_TIMER_INTERVAL*HZ);
		add_timer(&tx_timer);
	}

	/* clear the incoming data flag */
	clear_bit(BT_TXDATA, &flags);

	spin_unlock_irqrestore(&rw_lock, irq_flags);
}

/**
 * Schedules a tasklet to run when receiving an interrupt on the
 * <code>HOST_WAKE</code> GPIO pin.
 * @param irq Not used.
 * @param dev_id Not used.
 * @param regs Not used.
 */
static irqreturn_t bluesleep_hostwake_isr(int irq, void* dev_id, struct pt_regs* regs)
{
	gpio_bluetooth_hostwake_clear_int();

	mpm_handle_ioi();

	/* schedule a tasklet to handle the change in the host wake line */
	tasklet_schedule(&hostwake_task);
	return IRQ_RETVAL(1);
}

/**
 * Starts the Sleep-Mode Protocol on the Host.
 * @return On success, 0. On error, -1, and <code>errno</code> is set
 * appropriately.
 */
static int bluesleep_start(void)
{
	int retval;
	unsigned long irq_flags;

	spin_lock_irqsave(&rw_lock, irq_flags);

	if (test_bit(BT_PROTO, &flags)) {
		spin_unlock_irqrestore(&rw_lock, irq_flags);
		return 0;
	}

	if (!atomic_dec_and_test (&open_count))
	{
	   atomic_inc(&open_count);
	   return -EBUSY;
	}

	/* start the timer */
	tx_timer.expires = jiffies + (TX_TIMER_INTERVAL*HZ);
	add_timer(&tx_timer);

	/* assert BT_WAKE */
	gpio_bluetooth_wake_set_data(0);
	retval = gpio_bluetooth_hostwake_request_irq(bluesleep_hostwake_isr,
					    SA_INTERRUPT,
					    "bluetooth hostwake",
					    NULL);
	if (retval  < 0) {
		BT_ERR("Couldn't acquire BT_HOST_WAKE IRQ");
		goto fail;
	}

	set_bit(BT_PROTO, &flags);

	spin_unlock_irqrestore(&rw_lock, irq_flags);

	return 0;
fail:
	del_timer(&tx_timer);

	atomic_inc(&open_count);

	spin_unlock_irqrestore(&rw_lock, irq_flags);

	return retval;
}

/**
 * Stops the Sleep-Mode Protocol on the Host.
 */
static void bluesleep_stop(void)
{
	unsigned long irq_flags;

	spin_lock_irqsave(&rw_lock, irq_flags);

	if (!test_bit(BT_PROTO, &flags)) {
		spin_unlock_irqrestore(&rw_lock, irq_flags);
		return;
	}

	gpio_bluetooth_hostwake_free_irq(NULL);

	del_timer(&tx_timer);

	/* assert BT_WAKE */
	gpio_bluetooth_wake_set_data(0);
	clear_bit(BT_PROTO, &flags);

	atomic_inc(&open_count);

	spin_unlock_irqrestore(&rw_lock, irq_flags);
}
/**
 * Read the <code>BT_WAKE</code> GPIO pin value via the proc interface.
 * When this function returns, <code>page</code> will contain a 1 if the
 * pin is high, 0 otherwise.
 * @param page Buffer for writing data.
 * @param start Not used.
 * @param offset Not used.
 * @param count Not used.
 * @param eof Whether or not there is more data to be read.
 * @param data Not used.
 * @return The number of bytes written.
 */
static int bluepower_read_proc_btwake(char *page, char **start, off_t offset,
				      int count, int *eof, void *data)
{
	*eof = 1;
	return sprintf(page, "btwake:%u\n", !gpio_bluetooth_wake_get_data());
}

/**
 * Write the <code>BT_WAKE</code> GPIO pin value via the proc interface.
 * @param file Not used.
 * @param buffer The buffer to read from.
 * @param count The number of bytes to be written.
 * @param data Not used.
 * @return On success, the number of bytes written. On error, -1, and
 * <code>errno</code> is set appropriately.
 */
static int bluepower_write_proc_btwake(struct file *file, const char *buffer,
				       unsigned long count, void *data)
{
	char *buf;

	if (count < 1)
		return -EINVAL;

	if (!(buf = kmalloc(count, GFP_KERNEL)))
		return -ENOMEM;

	if (copy_from_user(buf, buffer, count)) {
		kfree(buf);
		return -EFAULT;
	}

	if (buf[0] == '0') {
		gpio_bluetooth_wake_set_data(1);
	} else if (buf[0] == '1') {
		gpio_bluetooth_wake_set_data(0);
	} else {
		kfree(buf);
		return -EINVAL;
	}

	kfree(buf);
	return count;
}

/**
 * Read the <code>BT_HOST_WAKE</code> GPIO pin value via the proc interface.
 * When this function returns, <code>page</code> will contain a 1 if the pin
 * is high, 0 otherwise.
 * @param page Buffer for writing data.
 * @param start Not used.
 * @param offset Not used.
 * @param count Not used.
 * @param eof Whether or not there is more data to be read.
 * @param data Not used.
 * @return The number of bytes written.
 */
static int bluepower_read_proc_hostwake(char *page, char **start, off_t offset,
					int count, int *eof, void *data)
{
	*eof = 1;
	return sprintf(page, "hostwake: %u \n",	 !gpio_bluetooth_hostwake_get_data());
}


/**
 * Read the low-power status of the Host via the proc interface.
 * When this function returns, <code>page</code> contains a 1 if the Host
 * is asleep, 0 otherwise.
 * @param page Buffer for writing data.
 * @param start Not used.
 * @param offset Not used.
 * @param count Not used.
 * @param eof Whether or not there is more data to be read.
 * @param data Not used.
 * @return The number of bytes written.
 */
static int bluesleep_read_proc_asleep(char *page, char **start, off_t offset,
				   int count, int *eof, void *data)
{
	unsigned int asleep;

	asleep = test_bit(BT_ASLEEP, &flags) ? 1 : 0;
	*eof = 1;
	return sprintf(page, "asleep: %u\n", asleep);
}

/**
 * Read the low-power protocol being used by the Host via the proc interface.
 * When this function returns, <code>page</code> will contain a 1 if the Host
 * is using the Sleep Mode Protocol, 0 otherwise.
 * @param page Buffer for writing data.
 * @param start Not used.
 * @param offset Not used.
 * @param count Not used.
 * @param eof Whether or not there is more data to be read.
 * @param data Not used.
 * @return The number of bytes written.
 */
static int bluesleep_read_proc_proto(char *page, char **start, off_t offset,
				  int count, int *eof, void *data)
{
	unsigned int proto;

	proto = test_bit(BT_PROTO, &flags) ? 1 : 0;
	*eof = 1;
	return sprintf(page, "proto: %u\n", proto);
}

/**
 * Modify the low-power protocol used by the Host via the proc interface.
 * @param file Not used.
 * @param buffer The buffer to read from.
 * @param count The number of bytes to be written.
 * @param data Not used.
 * @return On success, the number of bytes written. On error, -1, and
 * <code>errno</code> is set appropriately.
 */
static int bluesleep_write_proc_proto(struct file *file, const char *buffer,
				   unsigned long count, void *data)
{
	char proto;

	if (count < 1)
		return -EINVAL;

	if (copy_from_user(&proto, buffer, 1))
		return -EFAULT;

	if (proto == '0') {
		bluesleep_stop();
	} else {
		bluesleep_start();
	}

	/* claim that we wrote everything */
	return count;
}

/**
 * Initializes the module.
 * @return On success, 0. On error, -1, and <code>errno</code> is set
 * appropriately.
 */
static int __init bluesleep_init(void)
{
	int retval;
	struct proc_dir_entry *ent;
	struct proc_dir_entry *sleep_dir;

	BT_INFO("Bluetooth Sleep Mode Module ver %s", VERSION);
	BT_INFO("Copyright (C) 2006 Motorola Inc");

	bluesleep_hdev = NULL;

	/* Install /proc interface XXX bluez module must already be
		insmod'd */

	sleep_dir = proc_mkdir(PROC_DIR, NULL);
	if (sleep_dir == NULL) {
		BT_ERR("Unable to create /proc/%s directory", PROC_DIR);
		return -ENOMEM;
	}

	ent = create_proc_entry("btwake", 0, sleep_dir);
	if (ent == NULL) {
		BT_ERR("Unable to create /proc/%s/btwake entry", PROC_DIR);
		retval = -ENOMEM;
		goto fail;
	}
	ent->read_proc = bluepower_read_proc_btwake;
	ent->write_proc = bluepower_write_proc_btwake;

	/* read only proc entries */
	if (create_proc_read_entry("hostwake", 0, sleep_dir, bluepower_read_proc_hostwake, NULL) == NULL) {
		BT_ERR("Unable to create /proc/%s/hostwake entry", PROC_DIR);
		retval = -ENOMEM;
		goto fail;
	}

	/* read/write proc entries */
	ent = create_proc_entry(PROC_DIR"/proto", 0, NULL);
	if (ent == NULL) {
		BT_ERR("Unable to create /proc/%s/proto entry", PROC_DIR);
		retval = -ENOMEM;
		goto fail;
	}
	ent->read_proc = bluesleep_read_proc_proto;
	ent->write_proc = bluesleep_write_proc_proto;

	/* read only proc entries */
	if (create_proc_read_entry(PROC_DIR"/asleep", 0, NULL, bluesleep_read_proc_asleep, NULL) == NULL) {
		BT_ERR("Unable to create /proc/%s/asleep entry", PROC_DIR);
		retval =  -ENOMEM;
		goto fail;
	}

	flags = 0; /* clear all status bits */

	/* Initialize spinlock. */
	spin_lock_init(&rw_lock);

	/* Initialize timer */
	init_timer(&tx_timer);
	tx_timer.function = bluesleep_tx_timer_expire;
	tx_timer.data = 0;

	/* initialize host wake tasklet */
	tasklet_init(&hostwake_task, bluesleep_hostwake_task, 0);

	/* assert bt wake */
	gpio_bluetooth_wake_set_data(0);
	hci_register_notifier(&hci_event_nblock);

    #ifdef HW_BT_WLAN_SINGLE_ANTENNA
    /* Enable antenna switch (wlan/BT single antenna solution) */
    antenna_ctrl(ANTENNA_REQUIRED);
    #endif //HW_BT_WLAN_SINGLE_ANTENNA
	return 0;

fail:
	remove_proc_entry(PROC_DIR"/proto", 0);
	remove_proc_entry(PROC_DIR"/asleep", 0);
	remove_proc_entry(PROC_DIR"/hostwake", 0);
	remove_proc_entry(PROC_DIR"/btwake", 0);
	remove_proc_entry(PROC_DIR, 0);
	return retval;
}

/**
 * Cleans up the module.
 */
static void __exit bluesleep_exit(void)
{
    #ifdef HW_BT_WLAN_SINGLE_ANTENNA
    antenna_ctrl(ANTENNA_NOT_REQUIRED);
    #endif //HW_BT_WLAN_SINGLE_ANTENNA

	/* assert bt wake */
	gpio_bluetooth_wake_set_data(0);

	hci_unregister_notifier(&hci_event_nblock);

	remove_proc_entry(PROC_DIR"/proto", 0);
	remove_proc_entry(PROC_DIR"/asleep", 0);
	remove_proc_entry(PROC_DIR"/hostwake", 0);
	remove_proc_entry(PROC_DIR"/btwake", 0);
	remove_proc_entry(PROC_DIR, 0);
}

module_init(bluesleep_init);
module_exit(bluesleep_exit);


MODULE_DESCRIPTION("Bluetooth Sleep Mode Module ver " VERSION);
#ifdef MODULE_LICENSE
MODULE_LICENSE("GPL");
#endif
