#!/bin/sh

#==============================================================================
#
#   File Name: tvout.sh
#
#   General Description: This file executes the startup and shutdown procedures
#   for the TV Out Module
#
#==============================================================================
#
#        Copyright (c) 2007-2008 Motorola, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# Revision History
#  #   Date         Author          Comment
# === ==========   ============    ============================
#   1 08/12/2007    Motorola        Initial version. 
#   2 03/07/2008    Motorola        OSS CV fix. 
#


INSMOD="/sbin/insmod"
RMMOD="/sbin/rmmod"
TVOUT_MOD_PATH="/lib/modules"
TVOUT_MOD_NAME="tvout-driver.ko"
TVOUT_DEV="/dev/tvout"

[ `/usr/bin/id -u` = 0 ] || exit 1

startup () {
    if [ -f ${TVOUT_MOD_PATH}/${TVOUT_MOD_NAME} ]; then
        echo "Starting TV out"
        ${INSMOD} ${TVOUT_MOD_PATH}/${TVOUT_MOD_NAME}
        if [ -c ${TVOUT_DEV} ]; then
            echo "Changing permissions on ${TVOUT_DEV}"
            chmod 666 ${TVOUT_DEV}
        else
            echo "No character device at ${TVOUT_DEV}"
        fi
    else
        echo "TV out module doesn't exist; not starting"
    fi
}

shutdown () {
    if [ -f ${TVOUT_MOD_PATH}/${TVOUT_MOD_NAME} ]; then
        echo "Stopping TV out"
        ${RMMOD} ${TVOUT_MOD_NAME}
    else
        echo "TV out module doesn't exist; not stopping"
    fi
}

restart () {
    shutdown
    startup
}

case "$1" in
    start)
        startup
    ;;
    stop)
        shutdown
    ;;
    restart)
        restart
    ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
esac
