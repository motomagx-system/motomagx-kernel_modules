/*
 * Copyright (C) 2007-2008 Motorola, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 */

/*
 * Date         Author      Comment
 * ===========  ==========  ==================================================
 * 22-May-2007  Motorola    Initial revision
 * 18-Aug-2007  Motorola    Rework
 * 14-May-2008  Motorola    Fixed TVout toggle problem
 * 23-May-2008  Motorola    OSS CV fix
 * 06-Jun-2008  Motorola    Fixed TVout toggle fix problem
 * 03-Jul-2008  Motorola    OSS CV fix
 * 04-Aug-2008  Motorola    Added PAL-M
 */

/*==================================================================================================
                                         INCLUDES
==================================================================================================*/

#include <asm/mot-gpio.h>
#include <asm/arch/mxc_i2c.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/errno.h>
#include <linux/i2c.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/power_ic.h>
#include <linux/power_ic_kernel.h>
#include <asm/mot-gpio.h>
#include <asm/module.h>
#include <asm/semaphore.h>

#include "tvout-driver.h"

/*==================================================================================================
                                       LOCAL DEFINITIONS
==================================================================================================*/

// Remember to comment out before releasing BR
// #define DEBUG

#ifdef DEBUG
# define DEBUG_MSG(fmt, args...) printk( KERN_DEBUG "tvout-driver [DEBUG]: " fmt, ## args)
#else
# define DEBUG_MSG(fmt, args...)
#endif // DEBUG
#define ERROR_MSG(fmt, args...) printk( KERN_ERR "tvout-driver [ERROR]: " fmt, ## args)
#define INFO_MSG(fmt, args...) printk( KERN_INFO "tvout-driver [INFO]: " fmt, ## args)
#define STOPPED             0
#define STARTED             1
#define STOPPED_POWERSAVE   2
#define STARTED_POWERSAVE   3
#define PROC_BUFFER_LEN     10000
#define REG_SIZE            0x5B + 1 // Index starts at 0x01, but still add 1
#define SAP2973_STATUS      0x7F // To construct 0xFEFF
#define TVOUT_I2C_ADDR      0x20 // The real i2c slave address is 0x40

// Bit masks
#define TVOUT_ZERO          0x0000
#define TVOUT_ON_MASK       0x0001
#define TVOUT_ADDSETUP_MASK 0x0100
#define TVOUT_PAL_MASK      0x0001
#define TVOUT_PAL_M_MASK    0x0200
#define TVOUT_VBST_MASK     0x007F
#define TVOUT_UBST_MASK     0x003F
#define TVOUT_DSPOFST_MASK  0x03FF
#define TVOUT_DSPBUF_MASK   0x03FF
#define TVOUT_SCAN_MASK     0x01C0
#define TVOUT_VLSTOP_MASK   0x0008
#define TVOUT_SCALEHX4_MASK 0x0200
#define TVOUT_SCALEVX4_MASK 0x0400
#define TVOUT_MINSCALE_MASK 0x0080
#define TVOUT_MAXSCALE_MASK 0x00FF
#define TVOUT_SDRAM_ON_MASK 0x1A00
#define TVOUT_DIV_MN_MASK   0x0FFF
#define TVOUT_DIV_P_MASK    0x00FF
#define TVOUT_DIV_RES_MASK  0x0A00
#define TVOUT_PLL_ON_MASK   0x000C // Set VCLK mode ON as well
#define TVOUT_CLK_ON_MASK   0x00C0
#define TVOUT_SRESET_MASK   0x0001
#define TVOUT_MEMSET1_MASK  0x0027
#define TVOUT_MEMSET1N_MASK 0x0088
#define TVOUT_MEMSET2_MASK  0x034B // 781
#define TVOUT_MEMSET3_MASK  0x0880
#define TVOUT_ENHANCE_MASK  0x0008
#define TVOUT_SYSCTL2_MASK  0x4174
#define TVOUT_UVPOLREV_MASK 0x0002
#define TVOUT_INT_MASK      0x7FFF
#define TVOUT_MEMINIT_MASK  0x0002
#define TVOUT_SMEMINIT_MASK 0x0002
#define TVOUT_DACON_MASK    0x0010
#define TVOUT_INTACT_MASK   0x0040
#define TVOUT_SDSTDBY_MASK  0x0020
#define TVOUT_SCALEHON_MASK 0x0002
#define TVOUT_SCALEVON_MASK 0x0004
#define TVOUT_BLUEBACK_MASK 0x0008
#define BYTE_MASK 0xFF

#define BIT6  0x06
#define BIT8  0x08

// Bit values
#define TVOUT_NTSC_M_VBST    0x0000
#define TVOUT_NTSC_M_UBST    0x0020 // 32
#define TVOUT_PAL_VBST       0x0018 // 24
#define TVOUT_PAL_UBST       0x0018 // 24
#define TVOUT_WRITE_HLENP    0x00F0 // 240
#define TVOUT_WRITE_VLENP    0x0140 // 320
#define TVOUT_WRITE_HLENL    0x0140 // 320
#define TVOUT_WRITE_VLENL    0x00F0 // 240

//Defines for normal mode out layout
#define TVOUT_MODE_NORMAL_NTSC_M_HLENP    0x018E // 398
#define TVOUT_MODE_NORMAL_NTSC_M_HLENL    0x0274 // 628
#define TVOUT_MODE_NORMAL_NTSC_M_VLENP    0x01B6 // 438
#define TVOUT_MODE_NORMAL_NTSC_M_VLENL    0x01B2 // 434
#define TVOUT_MODE_NORMAL_PAL_HLENP       0x0194 // 404
#define TVOUT_MODE_NORMAL_PAL_HLENL       0x0280 // 640
#define TVOUT_MODE_NORMAL_PAL_VLENP       0x021F // 543
#define TVOUT_MODE_NORMAL_PAL_VLENL       0x01E1 // 481 - Not 540 px, due to chip x4 flaw
#define TVOUT_MODE_NORMAL_PAL_HOFSTP      0x0094 // 148
#define TVOUT_MODE_NORMAL_PAL_HOFSTL      0x0020 // 32
#define TVOUT_MODE_NORMAL_NTSC_M_HOFSTP   0x0098 // 152
#define TVOUT_MODE_NORMAL_NTSC_M_HOFSTL   0x0026 // 38
#define TVOUT_MODE_NORMAL_PAL_VOFSTP      0x0009 // 9
#define TVOUT_MODE_NORMAL_PAL_VOFSTL      0x0019 // 19
#define TVOUT_MODE_NORMAL_NTSC_M_VOFST    0x000B // 11
#define TVOUT_MODE_NORMAL_PAL_HSCALEP     0x00D6 // 214, 1.68 scale (x2) - This odd setting is required to see rightmost pixel
#define TVOUT_MODE_NORMAL_PAL_VSCALEP     0x00D8 // 216, 1.7 scale (x2)
#define TVOUT_MODE_NORMAL_PAL_HSCALEL     0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_NORMAL_PAL_VSCALEL     0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_NORMAL_NTSC_M_HSCALEP  0x00D3 // 211, 1.66 scale (x2)
#define TVOUT_MODE_NORMAL_NTSC_M_VSCALEP  0x00AE // 174, 1.5 scale (x2)
#define TVOUT_MODE_NORMAL_NTSC_M_HSCALEL  0x00FA // 250, 1.96 scale (x2)
#define TVOUT_MODE_NORMAL_NTSC_M_VSCALEL  0x00E6 // 230, 1.80 scale (x2)
#define TVOUT_MODE_NORMAL_NTSC_M_SCALEHON 0x0002
#define TVOUT_MODE_NORMAL_NTSC_M_SCALEVON 0x0004
#define TVOUT_MODE_NORMAL_PAL_SCALEHON    0x0002
#define TVOUT_MODE_NORMAL_PAL_SCALEVON    0x0004

//Defines for test mode output layout
#define TVOUT_MODE_TEST_NTSC_M_HLENP    0x01E0 // 480
#define TVOUT_MODE_TEST_NTSC_M_HLENL    0x0280 // 640
#define TVOUT_MODE_TEST_NTSC_M_VLENP    0x0280 // 640
#define TVOUT_MODE_TEST_NTSC_M_VLENL    0x01E0 // 480
#define TVOUT_MODE_TEST_PAL_HLENP       0x01E0 // 480
#define TVOUT_MODE_TEST_PAL_HLENL       0x0280 // 640
#define TVOUT_MODE_TEST_PAL_VLENP       0x0280 // 640
#define TVOUT_MODE_TEST_PAL_VLENL       0x01E0 // 480
#define TVOUT_MODE_TEST_PAL_HOFSTP      0x0000 // 0
#define TVOUT_MODE_TEST_PAL_HOFSTL      0x0000 // 0
#define TVOUT_MODE_TEST_NTSC_M_HOFSTP   0x0000 // 0
#define TVOUT_MODE_TEST_NTSC_M_HOFSTL   0x0000 // 0
#define TVOUT_MODE_TEST_PAL_VOFSTP      0x0000 // 0
#define TVOUT_MODE_TEST_PAL_VOFSTL      0x0000 // 0
#define TVOUT_MODE_TEST_NTSC_M_VOFST    0x0000 // 0
#define TVOUT_MODE_TEST_PAL_HSCALEP     0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_PAL_VSCALEP     0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_PAL_HSCALEL     0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_PAL_VSCALEL     0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_NTSC_M_HSCALEP  0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_NTSC_M_VSCALEP  0x00FF // 155, 2.0 scale (x2)
#define TVOUT_MODE_TEST_NTSC_M_HSCALEL  0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_NTSC_M_VSCALEL  0x00FF // 255, 2.0 scale (x2)
#define TVOUT_MODE_TEST_NTSC_M_SCALEHON 0x0002
#define TVOUT_MODE_TEST_NTSC_M_SCALEVON 0x0004
#define TVOUT_MODE_TEST_PAL_SCALEHON    0x0002
#define TVOUT_MODE_TEST_PAL_SCALEVON    0x0004

#define TVOUT_DIV_M          0x0051 // 81
#define TVOUT_DIV_N          0x000D // 13
#define TVOUT_DIV_P          0x0003 // 3
#define TVOUT_ENCMODE_INIT   0x3010
#define TVOUT_ENCGAIN1_INIT  0x0745
#define TVOUT_ENCGAIN2_INIT  0x0286
#define TVOUT_OSDCONT_1_INIT 0x2040
#define TVOUT_OSDCONT1N_INIT 0x0080
#define TVOUT_OSDCONT_2_INIT 0x2040
#define TVOUT_OSDCONT2N_INIT 0x0080
#define TVOUT_VIFHACTSTA     0x001B
#define TVOUT_VIFHACTEND     0x010B
#define TVOUT_VIFVACTSTA     0x0003
#define TVOUT_VIFVACTEND     0x0143
#define TVOUT_ENCBBPLT       0x841D
#define TVOUT_AVIEWSYS_3BUF  0x0002
#define TVOUT_BUF2_VOFFSET   0x0140
#define TVOUT_BUF3_VOFFSET   0x0280

/*! @brief TV out devfs name */
#define TVOUT_DEV_NAME "tvout"
 
// TV-OUT_1.5V on / off
#define ATLAS_REG_PWR_MISC_GPO1EN_06_MASK      0x000040
#define ATLAS_REG_PWR_MISC_GPO1EN_ON           0x000040
#define ATLAS_REG_PWR_MISC_GPO1EN_OFF          0x000000

// TV-OUT_1.8V on / off (Old)
#define ATLAS_REG_PWR_MISC_PWGT1SPIEN_15_MASK  0x008000
#define ATLAS_REG_PWR_MISC_PWGT1SPIEN_ON       0x008000
#define ATLAS_REG_PWR_MISC_PWGT1SPIEN_OFF      0x008000 // result!?! ATLAS ACCESS NEEDS TO BE RECHECKED!

// TV-OUT_1.8V on / off (New)
#define ATLAS_REG_PWR_MISC_GPO2EN_08_MASK      0x000100
#define ATLAS_REG_PWR_MISC_GPO2EN_ON           0x000100
#define ATLAS_REG_PWR_MISC_GPO2EN_OFF          0x000000
 
// TV-OUT_3.0V on / off
#define ATLAS_REG_MOD_1_VMMC1EN_18_MASK        0x040000
#define ATLAS_REG_MOD_1_VMMC1EN_ON             0x040000
#define ATLAS_REG_MOD_1_VMMC1EN_OFF            0x000000


/*==================================================================================================
                                         LOCAL TYPE DEFINITIONS
==================================================================================================*/

/*! @brief The issued proc command, used for proc read */
typedef enum
{
    proc_A,  // Print all registers
    proc_B,  // Print proc read buffer 
    proc_C,  // Power off + i2c off
    proc_D,  // Delete proc read buffer
    proc_E,  // Power on + i2c on
    proc_F,  // PAL/NTSC_M
    proc_H,  // Display help
    proc_N,  // Null register
    proc_O,  // Or register
    proc_P,  // Portrait/landscape
    proc_R,  // Read register
    proc_S,  // Set register
    proc_T,  // Enable output
    proc_Q   // Disable output
} proc_command_T;

/*! @brief TV out LSI Registers (LC822973) */
typedef enum
{
  SAP2973_CLKCONT = 0x01,
  SAP2973_DIV_M,
  SAP2973_DIV_N,
  SAP2973_DIV_P,

  SAP2973_INT = 0x06,
  SAP2973_INTEN,
  SAP2973_SYSCTL1,
  SAP2973_SYSCTL2,
  SAP2973_SYSCTL3,
  SAP2973_MEMSET1,
  SAP2973_MEMSET2,
  SAP2973_MEMSET3,
  SAP2973_IMGWRITE,
  SAP2973_IMGREADGO,
  SAP2973_IMGREAD, // 0x10
  SAP2973_IMGABORT,
  SAP2973_SCALE,
  SAP2973_WFBHLEN = 0x15,
  SAP2973_WFBVLEN,
  SAP2973_WFBHSTART,
  SAP2973_WFBVSTART,
  SAP2973_RFBHOFST,
  SAP2973_RFBVOFST,
  SAP2973_DSPHOFST,
  SAP2973_DSPVOFST,
  SAP2973_DSPHLEN,
  SAP2973_DSPVLEN,
  SAP2973_BGCOLOR1,
  SAP2973_BGCOLOR2, // 0x20
  SAP2973_ENCMODE,
  SAP2973_ENCGAIN1,
  SAP2973_ENCGAIN2,
  SAP2973_ENCBST1,
  SAP2973_ENCBST2,
  SAP2973_ENCBBPLT,
  SAP2973_ENCRHVAL,
  SAP2973_ENCHBLK,
  SAP2973_ENCVBLK,
  SAP2973_VERSION,
  SAP2973_TESTMODE,
  SAP2973_OSDCONT_1,
  SAP2973_OSDWFBHSTART,
  SAP2973_OSDWFBVSTART,
  SAP2973_OSDWFBHLEN,
  SAP2973_OSDWFBVLEN, // 0x30
  SAP2973_OSDRFBHOFST_1,
  SAP2973_OSDRFBVOFST_1,
  SAP2973_OSDHOFST_1,
  SAP2973_OSDVOFST_1,
  SAP2973_OSDHLEN_1,
  SAP2973_OSDVLEN_1,
  SAP2973_OSDCONT_2,
  SAP2973_OSDRFBHOFST_2,
  SAP2973_OSDRFBVOFST_2,
  SAP2973_OSDHOFST_2,
  SAP2973_OSDVOFST_2,
  SAP2973_OSDHLEN_2,
  SAP2973_OSDVLEN_2,
  SAP2973_OSDCOLOR_Y,
  SAP2973_OSDCOLOR_U,
  SAP2973_OSDCOLOR_V, // 0x40
  SAP2973_OSDWRITE,
  SAP2973_OSDABORT,
  SAP2973_VIFSYS,
  SAP2973_VIFHACTSTA,
  SAP2973_VIFHACTEND,
  SAP2973_VIFVACTSTA,
  SAP2973_VIFVACTEND,
  SAP2973_AVIEWSYS,
  SAP2973_AUTOVIEWON,
  SAP2973_AUTOVIEWOFF,
  SAP2973_AVWFBHSTART_0,
  SAP2973_AVWFBVSTART_0,
  SAP2973_AVWFBHSTART_1,
  SAP2973_AVWFBVSTART_1,
  SAP2973_AVWFBHSTART_2,
  SAP2973_AVWFBVSTART_2, // 0x50
  SAP2973_CGMSA_CODE,
  SAP2973_CGMSA_TRM,
  SAP2973_WSS_CODE,
  SAP2973_WSS_TRM,
  SAP2973_TEST = 0x5B
} LC822973_REG_T;

/*! @brief Scandirection types */
typedef enum 
{
  TVOUT_SCANDIRECTION_TL_RSCAN = 0x0,
  TVOUT_SCANDIRECTION_TL_DSCAN,
  TVOUT_SCANDIRECTION_TR_LSCAN,
  TVOUT_SCANDIRECTION_BL_USCAN,
  TVOUT_SCANDIRECTION_BL_RSCAN,
  TVOUT_SCANDIRECTION_TR_DSCAN,
  TVOUT_SCANDIRECTION_BR_LSCAN,
  TVOUT_SCANDIRECTION_BR_USCAN,
} TVOUT_SCANDIRECTION_T;

/*! @brief Settings types */
typedef enum
{
    TVOUT_SETTINGS_MODE = 0x0,
    TVOUT_SETTINGS_LAST
} TVOUT_SETTINGS_T;

/*! @brief Mode types */
typedef enum
{
    TVOUT_MODE_NORMAL = 0x0,
    TVOUT_MODE_TEST,
    TVOUT_MODE_LAST
} TVOUT_MODES_T;

/*! @brief Mode dependent registers */
typedef enum
{
    TVOUT_NTSC_M_HLENP = 0x0,
    TVOUT_NTSC_M_HLENL,
    TVOUT_NTSC_M_VLENP,
    TVOUT_NTSC_M_VLENL,
    TVOUT_PAL_HLENP,
    TVOUT_PAL_HLENL,
    TVOUT_PAL_VLENP,
    TVOUT_PAL_VLENL,
    TVOUT_PAL_HOFSTP,
    TVOUT_PAL_HOFSTL,
    TVOUT_NTSC_M_HOFSTP,
    TVOUT_NTSC_M_HOFSTL,
    TVOUT_PAL_VOFSTP,
    TVOUT_PAL_VOFSTL,
    TVOUT_NTSC_M_VOFST,
    TVOUT_PAL_HSCALEP,
    TVOUT_PAL_VSCALEP,
    TVOUT_PAL_HSCALEL,
    TVOUT_PAL_VSCALEL,
    TVOUT_NTSC_M_HSCALEP,
    TVOUT_NTSC_M_VSCALEP,
    TVOUT_NTSC_M_HSCALEL,
    TVOUT_NTSC_M_VSCALEL,
    TVOUT_NTSC_M_SCALEHON,
    TVOUT_NTSC_M_SCALEVON,
    TVOUT_PAL_SCALEHON,
    TVOUT_PAL_SCALEVON,
    TVOUT_MODE_REGISTERS_LAST
} TVOUT_MODE_REGISTERS;

/*! @brief TV out I2C data type */
typedef struct {
    uint8_t   reg_addr;
    uint16_t  data;
} TVOUT_I2C_DATA_T;

typedef struct {
    TVOUT_OUTPUT_T      output;
    TVOUT_ENCODING_T    encoding;
    TVOUT_ORIENTATION_T orientation;
    TVOUT_MODE_T        mode;
} TVOUT_CONFIG_T;

typedef struct {
    TVOUT_MODE_REGISTERS mode[TVOUT_MODE_LAST][TVOUT_MODE_REGISTERS_LAST];
} TVOUT_SETTINGS_STRUCT_T;

/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*! @brief  Function prototypes to detach and attach I2C driver */
static int tvout_i2c_detach_client(struct i2c_client *client);
static int tvout_i2c_attach_adapter(struct i2c_adapter *adap);

/*! @brief  Function prototypes for the TVout device driver operations */
static int tvout_file_operation_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
static int tvout_file_operation_open(struct inode *inode, struct file *file);
static int tvout_file_operation_close(struct inode *inode, struct file *file);

static int tvout_proc_read(char *buf, char **start, off_t pos, int count, int *eof, void *data);
#ifdef DEBUG
/*! @brief  Function prototypes for proc interface read and write operations */
static int tvout_proc_write(struct file *filep, const char __user *buff, unsigned long len, void *data);
#endif // DEBUG

void LOCAL_power_off(void);
void LOCAL_power_on(void);
TVOUT_STATUS_T LOCAL_write_reg(uint16_t register_addr, uint16_t value);
TVOUT_STATUS_T LOCAL_set_display_pointer(uint16_t startX, uint16_t startY, uint16_t hLen, uint16_t vLen);
TVOUT_STATUS_T LOCAL_set_scaling(uint16_t hscale, uint16_t vscale, uint16_t hscalex4, uint16_t vscalex4, uint16_t hscaleon, uint16_t vscaleon);
TVOUT_STATUS_T LOCAL_set_scan_direction(TVOUT_SCANDIRECTION_T scandir);
uint16_t LOCAL_read_status(void);
TVOUT_STATUS_T LOCAL_set_encoding_format(TVOUT_ENCODING_T encoding);
TVOUT_ENCODING_T LOCAL_get_encoding_format(void);
TVOUT_STATUS_T LOCAL_set_wfblen(uint16_t wfbhlen, uint16_t wfbvlen);
TVOUT_STATUS_T LOCAL_freeze_display(void);
TVOUT_STATUS_T LOCAL_unfreeze_display(void);
TVOUT_STATUS_T LOCAL_init_chip(void);
TVOUT_STATUS_T LOCAL_set_orientation(TVOUT_ORIENTATION_T orientation);
TVOUT_ORIENTATION_T LOCAL_get_orientation(void);
TVOUT_STATUS_T LOCAL_set_mode(TVOUT_MODE_T mode);
TVOUT_MODE_T LOCAL_get_mode(void);
TVOUT_STATUS_T LOCAL_enable_output(void);
TVOUT_STATUS_T LOCAL_disable_output(void);
static int LOCAL_i2c_read(TVOUT_I2C_DATA_T *data);
static int LOCAL_i2c_write(TVOUT_I2C_DATA_T *data);
static int LOCAL_i2c_register_driver(int i2c_addr);
static int LOCAL_i2c_unregister_driver(void);

/*==================================================================================================
                                        LOCAL CONSTANTS
==================================================================================================*/

/*! @brief This structure defines the file operations for the TVout device */
static const struct file_operations tvout_fops =
{
    .owner =    THIS_MODULE,
    .ioctl =    tvout_file_operation_ioctl,
    .open  =    tvout_file_operation_open,
    .release =  tvout_file_operation_close,
};

/*! @brief Creation of the i2c_driver for TVOUT */ 
static const struct i2c_driver driver = {
    name:           "TVOUT I2C Driver",
    flags:          I2C_DF_NOTIFY,
    attach_adapter: tvout_i2c_attach_adapter,
    detach_client:  tvout_i2c_detach_client,
};

/*==================================================================================================
                                        LOCAL VARIABLES
==================================================================================================*/

/*! @brief Store the I2C slave address */
static uint8_t tvout_i2c_address = 0;

/*! @brief Store the device major number */
static int tvout_driver_module_major = 0;

/*! @brief Store lc822973 (lsi) registers */
static uint16_t lc822973_regs[REG_SIZE];

/*! @brief The I2C client structure obtained for communication with the TVOUT LSI Chip */
static struct i2c_client *tvout_lsi_i2c_client = NULL;

static TVOUT_CONFIG_T tvout =
{
    output:         DISABLE,       // default output
    encoding:       PAL,           // default encoding
    orientation:    PORTRAIT,      // default orientation
    mode:           NORMAL         // default mode
};

static TVOUT_SETTINGS_STRUCT_T settings =
{
    mode:
    {
        { // mode_normal
            TVOUT_MODE_NORMAL_NTSC_M_HLENP,
            TVOUT_MODE_NORMAL_NTSC_M_HLENL,
            TVOUT_MODE_NORMAL_NTSC_M_VLENP,
            TVOUT_MODE_NORMAL_NTSC_M_VLENL,
            TVOUT_MODE_NORMAL_PAL_HLENP,
            TVOUT_MODE_NORMAL_PAL_HLENL,
            TVOUT_MODE_NORMAL_PAL_VLENP,
            TVOUT_MODE_NORMAL_PAL_VLENL,
            TVOUT_MODE_NORMAL_PAL_HOFSTP,
            TVOUT_MODE_NORMAL_PAL_HOFSTL,
            TVOUT_MODE_NORMAL_NTSC_M_HOFSTP,
            TVOUT_MODE_NORMAL_NTSC_M_HOFSTL,
            TVOUT_MODE_NORMAL_PAL_VOFSTP,
            TVOUT_MODE_NORMAL_PAL_VOFSTL,
            TVOUT_MODE_NORMAL_NTSC_M_VOFST,
            TVOUT_MODE_NORMAL_PAL_HSCALEP,
            TVOUT_MODE_NORMAL_PAL_VSCALEP,
            TVOUT_MODE_NORMAL_PAL_HSCALEL,
            TVOUT_MODE_NORMAL_PAL_VSCALEL,
            TVOUT_MODE_NORMAL_NTSC_M_HSCALEP,
            TVOUT_MODE_NORMAL_NTSC_M_VSCALEP,
            TVOUT_MODE_NORMAL_NTSC_M_HSCALEL,
            TVOUT_MODE_NORMAL_NTSC_M_VSCALEL,
            TVOUT_MODE_NORMAL_NTSC_M_SCALEHON,
            TVOUT_MODE_NORMAL_NTSC_M_SCALEVON,
            TVOUT_MODE_NORMAL_PAL_SCALEHON,
            TVOUT_MODE_NORMAL_PAL_SCALEVON
	},
        { // mode_test
            TVOUT_MODE_TEST_NTSC_M_HLENP,
            TVOUT_MODE_TEST_NTSC_M_HLENL,
            TVOUT_MODE_TEST_NTSC_M_VLENP,
            TVOUT_MODE_TEST_NTSC_M_VLENL,
            TVOUT_MODE_TEST_PAL_HLENP,
            TVOUT_MODE_TEST_PAL_HLENL,
            TVOUT_MODE_TEST_PAL_VLENP,
            TVOUT_MODE_TEST_PAL_VLENL,
            TVOUT_MODE_TEST_PAL_HOFSTP,
            TVOUT_MODE_TEST_PAL_HOFSTL,
            TVOUT_MODE_TEST_NTSC_M_HOFSTP,
            TVOUT_MODE_TEST_NTSC_M_HOFSTL,
            TVOUT_MODE_TEST_PAL_VOFSTP,
            TVOUT_MODE_TEST_PAL_VOFSTL,
            TVOUT_MODE_TEST_NTSC_M_VOFST,
            TVOUT_MODE_TEST_PAL_HSCALEP,
            TVOUT_MODE_TEST_PAL_VSCALEP,
            TVOUT_MODE_TEST_PAL_HSCALEL,
            TVOUT_MODE_TEST_PAL_VSCALEL,
            TVOUT_MODE_TEST_NTSC_M_HSCALEP,
            TVOUT_MODE_TEST_NTSC_M_VSCALEP,
            TVOUT_MODE_TEST_NTSC_M_HSCALEL,
            TVOUT_MODE_TEST_NTSC_M_VSCALEL,
            TVOUT_MODE_TEST_NTSC_M_SCALEHON,
            TVOUT_MODE_TEST_NTSC_M_SCALEVON,
            TVOUT_MODE_TEST_PAL_SCALEHON,
            TVOUT_MODE_TEST_PAL_SCALEVON
        }
    }
};

static int tvout_session = STOPPED;

static struct proc_dir_entry *proc_entry;
struct semaphore cmd_in_progress;
#ifdef DEBUG
static proc_command_T proc_command;
char proc_buffer[PROC_BUFFER_LEN];
char* proc_buffer_ptr = proc_buffer;
#endif // DEBUG

/*==================================================================================================
                                        LOCAL FUNCTIONS
==================================================================================================*/

/*! @brief Power on the TV out chip */
void LOCAL_power_on(void)
{
    DEBUG_MSG("Entering LOCAL_power_on...\n");

    // Turn off power and wait 10mS
    LOCAL_power_off();
    mdelay( 10 ); 
 
    // Turn off TV-out_1.5V (GPO1) (New)
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_PWR_MISC,
			ATLAS_REG_PWR_MISC_GPO1EN_06_MASK, ATLAS_REG_PWR_MISC_GPO1EN_ON);  
    mdelay( 2 ); 

    // Turn on TV-out_1.8V (PWGT1DRV) and wait 2mS (Old)
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_PWR_MISC,
			ATLAS_REG_PWR_MISC_PWGT1SPIEN_15_MASK, ATLAS_REG_PWR_MISC_PWGT1SPIEN_ON);  
    mdelay( 2 );
 
    // Turn on TV-out_1.8V (GPO2) and wait 2mS (New)
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_PWR_MISC,
			ATLAS_REG_PWR_MISC_GPO2EN_08_MASK, ATLAS_REG_PWR_MISC_GPO2EN_ON);  
    mdelay( 2 );
 
    // Turn on TV-out_3.0V (VMMC1_DRV) and wait 2mS
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_REG_MODE_1,
			ATLAS_REG_MOD_1_VMMC1EN_18_MASK, ATLAS_REG_MOD_1_VMMC1EN_ON); 
    mdelay( 2 );
 
    // Unreset TV-Out chip (TV_OUT_RST pin to high) and wait 2mS
    gpio_signal_set_data(GPIO_SIGNAL_TV_OUT_RST, GPIO_DATA_HIGH);
    mdelay( 2 );
}

/*! @brief Power off the TV out chip */
void LOCAL_power_off(void)
{
    DEBUG_MSG("Entering LOCAL_power_off...\n");

    // Reset TV-Out chip (TV_OUT_RST pin to low) and wait 2mS
    gpio_signal_set_data(GPIO_SIGNAL_TV_OUT_RST, GPIO_DATA_LOW);
    mdelay( 2 );

    // Turn off TV-out_3.0V (VMMC1_DRV) and wait 2mS
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_REG_MODE_1,
                          ATLAS_REG_MOD_1_VMMC1EN_18_MASK, ATLAS_REG_MOD_1_VMMC1EN_OFF);
    mdelay( 2 );

    // Turn off TV-out_1.8V (PWGT1DRV) (OLD) and wait 2mS
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_PWR_MISC,
                          ATLAS_REG_PWR_MISC_PWGT1SPIEN_15_MASK, ATLAS_REG_PWR_MISC_PWGT1SPIEN_OFF);
    mdelay( 2 );

    // Turn off TV-out_1.8V (GPO2) (New) and wait 2mS
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_PWR_MISC,
                          ATLAS_REG_PWR_MISC_GPO2EN_08_MASK, ATLAS_REG_PWR_MISC_GPO2EN_OFF);  
    mdelay( 2 ); 

    // Turn off TV-out_1.5V (GPO1) (New)
    power_ic_set_reg_mask(POWER_IC_REG_ATLAS_PWR_MISC,
                          ATLAS_REG_PWR_MISC_GPO1EN_06_MASK, ATLAS_REG_PWR_MISC_GPO1EN_OFF);  
}

/*!
 * @brief Reads a specific TVout Chip Register
 *
 * This function implements a read of a given TVout register.  The read is done using i2c_transfer
 * which is a set of transactions.  The only transaction will be an I2C read which will read
 * the contents of the register of the TVout Chip.  
 *
 * @param reg_value  Pointer to which the read data should be stored
 *
 * @return This function returns 0 if successful
 */
static int LOCAL_i2c_read(TVOUT_I2C_DATA_T *data)
{
    unsigned char read_data[2];
    unsigned char write_data[2];
    int retval;
    char *dataptr;

    struct i2c_msg msgs[2] = 
    {
        { tvout_lsi_i2c_client->addr, I2C_M_NOSTART, 1, &write_data[0] },
        { tvout_lsi_i2c_client->addr, I2C_M_RD, 1, &read_data[0] },			
    };

    struct i2c_msg msgs2[2] = 
    {
        { tvout_lsi_i2c_client->addr, I2C_M_NOSTART, 1, &write_data[1] },
        { tvout_lsi_i2c_client->addr, I2C_M_RD, 1, &read_data[1] },			
    };

    /* Fail if we weren't able to initialize (yet) */
    if (tvout_lsi_i2c_client == NULL)
    {
        return -EINVAL;
    }

    write_data[0] = data->reg_addr * 2 ;

    DEBUG_MSG("R:(0x%02X) 0x%02X = ",data->reg_addr, write_data[0]); 

    retval = i2c_transfer(tvout_lsi_i2c_client->adapter,msgs,2);

    DEBUG_MSG("0x%02X, ",read_data[0]); 

    write_data[1] = data->reg_addr * 2 + 1 ;

    DEBUG_MSG("0x%02X = ",write_data[1]); 

    retval = i2c_transfer(tvout_lsi_i2c_client->adapter,msgs2,2);

    DEBUG_MSG("0x%02X\n",read_data[1]); 

    if (retval >= 0) 
    {
      dataptr = (char *)&data->data;  
      *dataptr++ = read_data[1];
      *dataptr = read_data[0];
      retval = TVOUT_OK;
    }

    return retval;
}

 /*!
 * @brief Writes a value to a specified TVOUT register
 *
 * This function implements a write to a specified TVout register.  The write is accomplished
 * by sending the new contents to the i2c function i2c_master_send.
 *
 * @param reg_value  Register value to write
 *
 * @return This function returns 0 if successful
 */
static int LOCAL_i2c_write(TVOUT_I2C_DATA_T *data)
{

    unsigned char write_data[2];
    unsigned char write_data2[2];
    int result;
    char *dataptr;

    /* Fail if we weren't able to initialize */
    if(tvout_lsi_i2c_client == NULL)
    {
        return -EINVAL;
    }

    /* write two bytes, upper byte first, then lower byte */
    write_data[0] = data->reg_addr * 2 ;
    write_data2[0] = data->reg_addr * 2 + 1 ;
    dataptr = (char *)&data->data;  
    write_data2[1] = *dataptr++;
    write_data[1] = *dataptr;
	
    DEBUG_MSG("W:(0x%02X) 0x%02X = 0x%02X, 0x%02X = 0x%02X\n", data->reg_addr,
                  write_data[0], write_data[1], write_data2[0], write_data2[1]); 
   
    result = i2c_master_send(tvout_lsi_i2c_client,write_data,2);
    result = i2c_master_send(tvout_lsi_i2c_client,write_data2,2);

    return result < 0 ? result : 0;
}

/*!
 * @brief Initializes communication with the TVout I2C controller
 *
 * This function performs any initialization required for the TVout Chip.  It
 * starts the process mentioned above for initiating I2C communication with the TVout
 * Chip by registering the I2C driver.  The rest of the process is handled
 * through the callback function tvout_i2c_attach_adapter().
 *
 * @param reg_init_fcn  Pointer to a function to call when communications are available
 */
static int LOCAL_i2c_register_driver(int i2c_addr)
{
    int result;
    if (tvout_lsi_i2c_client == NULL)
    {
        /* Set the I2C addr of device */
        tvout_i2c_address = (uint8_t)i2c_addr;

        /* Register our driver */
        if ((result = i2c_add_driver((struct i2c_driver *)&driver)) < 0)
        {
            return result;
        }
    }
    else if (tvout_i2c_address != (uint8_t) i2c_addr )
    {
        /* Does not support multiple I2C devices */
        return -EFAULT;
    }
    return 0;
}

static int LOCAL_i2c_unregister_driver(void)
{
    int result;
    
    result = i2c_del_driver( ( struct i2c_driver * ) &driver );
    if (result)
    {
        ERROR_MSG("result unregistering driver with I2C system\n");
        return result;
    }
    
    tvout_lsi_i2c_client = NULL;
    
    return result;
}

TVOUT_STATUS_T LOCAL_write_reg(uint16_t register_addr, uint16_t value)
{
    uint16_t result;
    TVOUT_I2C_DATA_T data;

    data.reg_addr = register_addr;
    data.data = value;

    if ( ( result  = LOCAL_i2c_write(&data) ) != 0 )
    {
        DEBUG_MSG("result writing I2C register ( 0x%2X ): ioctl %7d\n", register_addr, result );
        return TVOUT_ERROR;
    }
    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_set_display_pointer(uint16_t startX, uint16_t startY, uint16_t hLen, uint16_t vLen)
{
    DEBUG_MSG("Entering LOCAL_set_display_pointer...\n");

    /***** Set display buffer area *****/
    if ( ( LOCAL_write_reg( SAP2973_DSPHOFST, lc822973_regs[SAP2973_DSPHOFST]
       = startX & TVOUT_DSPOFST_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_DSPVOFST, lc822973_regs[SAP2973_DSPVOFST]
       = startY & TVOUT_DSPOFST_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_DSPHLEN, lc822973_regs[SAP2973_DSPHLEN]
       = hLen & TVOUT_DSPBUF_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_DSPVLEN, lc822973_regs[SAP2973_DSPVLEN]
       = vLen & TVOUT_DSPBUF_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_set_scaling(uint16_t hscale, uint16_t vscale, uint16_t hscalex4, uint16_t vscalex4, uint16_t hscaleon, uint16_t vscaleon)
{
    DEBUG_MSG("Entering LOCAL_set_scaling...\n");

    if ( ! ( hscale & TVOUT_MINSCALE_MASK ) && ( vscale & TVOUT_MINSCALE_MASK ) )
    {
        DEBUG_MSG("LOCAL_set_scaling FAILURE: hscale or vscale to small\n");
        return TVOUT_ERROR;
    }
    lc822973_regs[ SAP2973_SCALE ] = ( ( vscale & TVOUT_MAXSCALE_MASK ) << BIT8 
                                     | ( hscale & TVOUT_MAXSCALE_MASK ) ) ; // Scaling
    lc822973_regs[ SAP2973_SYSCTL1 ] &= ~( TVOUT_SCALEHX4_MASK | TVOUT_SCALEVX4_MASK 
                                     | TVOUT_SCALEHON_MASK | TVOUT_SCALEVON_MASK );
    lc822973_regs[ SAP2973_SYSCTL1 ] |= ( ( hscalex4 & TVOUT_SCALEHX4_MASK ) 
                                     | ( vscalex4 & TVOUT_SCALEVX4_MASK ) ) ;
    lc822973_regs[ SAP2973_SYSCTL1 ] |= ( ( hscaleon & TVOUT_SCALEHON_MASK ) 
                                     | ( vscaleon & TVOUT_SCALEVON_MASK ) ) ;

    if ( ( LOCAL_write_reg( SAP2973_SCALE, lc822973_regs[SAP2973_SCALE])
       != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_SYSCTL1, lc822973_regs[SAP2973_SYSCTL1])
       != TVOUT_OK ) ) return TVOUT_ERROR ; 

    return TVOUT_OK ;
}

TVOUT_STATUS_T LOCAL_set_scan_direction(TVOUT_SCANDIRECTION_T scandir)
{
    DEBUG_MSG("Entering LOCAL_set_scan_direction...\n");

    lc822973_regs[ SAP2973_SYSCTL1 ] &= ~TVOUT_SCAN_MASK ; // clear, defaults to TL RSCAN
    lc822973_regs[ SAP2973_SYSCTL1 ] |= ( scandir << BIT6 ) & TVOUT_SCAN_MASK ;

    if ( ( LOCAL_write_reg( SAP2973_SYSCTL1, lc822973_regs[SAP2973_SYSCTL1])
       != TVOUT_OK ) ) return TVOUT_ERROR ; 

    return TVOUT_OK;
}

uint16_t LOCAL_read_status(void)
{
    TVOUT_I2C_DATA_T data;
    int result;

    DEBUG_MSG("Entering LOCAL_read_status...\n");

    /* read the SAP2973_STATUS register */
    data.reg_addr = SAP2973_STATUS;
    data.data = 0;
    
    if ((result = LOCAL_i2c_read(&data)) != 0)
    {
        DEBUG_MSG("result reading I2C register - LOCAL_read_status ( SAP2973_STATUS ): ioctl %7d\n", result);
    }

    return data.data;
}

TVOUT_STATUS_T LOCAL_set_encoding_format(TVOUT_ENCODING_T encoding)
{
    DEBUG_MSG("Entering LOCAL_set_encoding_format\n");

    tvout.encoding = encoding;

    LOCAL_set_orientation( LOCAL_get_orientation() );

    if ( ( tvout.output == DISABLE ) || ( tvout_session == STOPPED_POWERSAVE ) || ( tvout_session == STARTED_POWERSAVE ) )
    {
        return TVOUT_OK;
    }
    
    lc822973_regs[SAP2973_ENCMODE] &= ~(TVOUT_ADDSETUP_MASK | TVOUT_PAL_M_MASK | TVOUT_PAL_MASK);
    
    switch( encoding )
    {
    case NTSC_M:
        lc822973_regs[SAP2973_ENCMODE] |= TVOUT_ADDSETUP_MASK ;
        lc822973_regs[SAP2973_ENCBST1] = ( ( TVOUT_NTSC_M_VBST & TVOUT_VBST_MASK ) << BIT6 ) | ( TVOUT_NTSC_M_UBST & TVOUT_UBST_MASK );
        break;

    case PAL_M:
        lc822973_regs[SAP2973_ENCMODE] |= TVOUT_PAL_M_MASK ;
        lc822973_regs[SAP2973_ENCBST1] = ( ( TVOUT_PAL_VBST & TVOUT_VBST_MASK ) << BIT6 ) | ( TVOUT_PAL_UBST & TVOUT_UBST_MASK );
        break;

    case PAL:
    default:
        lc822973_regs[SAP2973_ENCMODE] |= TVOUT_PAL_MASK ;
        lc822973_regs[SAP2973_ENCBST1] = ( ( TVOUT_PAL_VBST & TVOUT_VBST_MASK ) << BIT6 ) | ( TVOUT_PAL_UBST & TVOUT_UBST_MASK );
        break;
    }

    if ( ( LOCAL_write_reg( SAP2973_ENCMODE, lc822973_regs[SAP2973_ENCMODE])
       != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_ENCBST1, lc822973_regs[SAP2973_ENCBST1])
       != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_ENCBST2, lc822973_regs[SAP2973_ENCBST2]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    return TVOUT_OK;
}

TVOUT_ENCODING_T LOCAL_get_encoding_format(void)
{
    DEBUG_MSG("Entering LOCAL_get_encoding_format...\n");
    return tvout.encoding;
}

TVOUT_STATUS_T LOCAL_set_wfblen(uint16_t wfbhlen, uint16_t wfbvlen)
{
    DEBUG_MSG("Entering LOCAL_set_wfblen...\n");
    if ( ( LOCAL_write_reg( SAP2973_WFBHLEN, lc822973_regs[SAP2973_WFBHLEN]
       = wfbhlen & TVOUT_DSPBUF_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_WFBVLEN, lc822973_regs[SAP2973_WFBVLEN]
       = wfbvlen & TVOUT_DSPBUF_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_freeze_display(void)
{
    if ( ( LOCAL_write_reg( SAP2973_AUTOVIEWOFF, lc822973_regs[SAP2973_AUTOVIEWOFF]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 
    mdelay(80);
    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_unfreeze_display(void)
{
    if ( ( LOCAL_write_reg( SAP2973_ENCMODE, lc822973_regs[SAP2973_ENCMODE]
       |= TVOUT_BLUEBACK_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 
    if ( ( LOCAL_write_reg( SAP2973_AUTOVIEWON, lc822973_regs[SAP2973_AUTOVIEWON]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 
    mdelay(80);
    if ( ( LOCAL_write_reg( SAP2973_ENCMODE, lc822973_regs[SAP2973_ENCMODE]
       &= ~TVOUT_BLUEBACK_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_init_chip(void)
{
    volatile uint32_t guard_timer;

    DEBUG_MSG("Entering LOCAL_init_chip...\n");

    /* RAM Power ON */
    if ( ( LOCAL_write_reg( SAP2973_CLKCONT, lc822973_regs[SAP2973_CLKCONT]
       = TVOUT_SDRAM_ON_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* PLL (M,N,P) Set, 26 MHz */
    lc822973_regs[ SAP2973_DIV_P ] = ( TVOUT_DIV_P & TVOUT_DIV_P_MASK ) | TVOUT_DIV_RES_MASK ;

    if ( ( LOCAL_write_reg( SAP2973_DIV_M, lc822973_regs[SAP2973_DIV_M]
       = TVOUT_DIV_M & TVOUT_DIV_MN_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_DIV_N, lc822973_regs[SAP2973_DIV_N]
       = TVOUT_DIV_N & TVOUT_DIV_MN_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_DIV_P, lc822973_regs[SAP2973_DIV_P])
       != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_CLKCONT, lc822973_regs[SAP2973_CLKCONT]
       |= TVOUT_PLL_ON_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* 10ms wait( Waiting time for PLL stabilization) */
    mdelay(10);
  
    /* clocks ON */
    if ( ( LOCAL_write_reg( SAP2973_CLKCONT, lc822973_regs[SAP2973_CLKCONT]
       |= TVOUT_CLK_ON_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* soft reset enable */
    if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       = TVOUT_SRESET_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /***** SDRAM initialize *****/
    /* SDRAM Parm set */
    lc822973_regs[ SAP2973_MEMSET1 ] &= ~TVOUT_MEMSET1N_MASK; 
    if ( ( LOCAL_write_reg( SAP2973_MEMSET1, lc822973_regs[SAP2973_MEMSET1]
       = TVOUT_MEMSET1_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_MEMSET2, lc822973_regs[SAP2973_MEMSET2]
       = TVOUT_MEMSET2_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_MEMSET3, lc822973_regs[SAP2973_MEMSET3]
       = TVOUT_MEMSET3_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* SYSCTL1-3 set */
    if ( ( LOCAL_write_reg( SAP2973_SYSCTL1, lc822973_regs[SAP2973_SYSCTL1]
       = TVOUT_ENHANCE_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       = TVOUT_SYSCTL2_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_SYSCTL3, lc822973_regs[SAP2973_SYSCTL3]
       = TVOUT_UVPOLREV_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* INT flag clear */
    if ( ( LOCAL_write_reg( SAP2973_INTEN, lc822973_regs[SAP2973_INTEN]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_INT, lc822973_regs[SAP2973_INT]
       = TVOUT_INT_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* SDRAM INIT START */
   if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       |= TVOUT_MEMINIT_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* SDRAM INIT check (bit1: 0=end 1=init) */
    guard_timer = 0;
    while( LOCAL_read_status() & TVOUT_SMEMINIT_MASK )
    {
        if (guard_timer++ > 0x1700000)            // 1sec(min 40ns RDpulse) - 2sec
        {
            DEBUG_MSG( "SAP_VDO_result_TIMEOUT\n" );
        }
    }

    /***** Reg initialize *****/
    /* VSYNC Latch disable */
    if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       &= ~TVOUT_VLSTOP_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    /* -- -- Register Initialize -- */
    if ( ( LOCAL_write_reg( SAP2973_ENCMODE, lc822973_regs[SAP2973_ENCMODE]
       = TVOUT_ENCMODE_INIT ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    if ( ( LOCAL_write_reg( SAP2973_ENCGAIN1, lc822973_regs[SAP2973_ENCGAIN1]
       = TVOUT_ENCGAIN1_INIT ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    if ( ( LOCAL_write_reg( SAP2973_ENCGAIN2, lc822973_regs[SAP2973_ENCGAIN2]
       = TVOUT_ENCGAIN2_INIT ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    lc822973_regs[ SAP2973_OSDCONT_1 ] &= ~TVOUT_OSDCONT1N_INIT | TVOUT_OSDCONT_1_INIT ;
    lc822973_regs[ SAP2973_OSDCONT_2 ] &= ~TVOUT_OSDCONT2N_INIT | TVOUT_OSDCONT_2_INIT ;

    if ( ( LOCAL_write_reg( SAP2973_OSDCONT_1, lc822973_regs[SAP2973_OSDCONT_1])
       != TVOUT_OK ) ) return TVOUT_ERROR ;

    if ( ( LOCAL_write_reg( SAP2973_OSDCONT_2, lc822973_regs[SAP2973_OSDCONT_2])
       != TVOUT_OK ) ) return TVOUT_ERROR ;

    if ( ( LOCAL_write_reg( SAP2973_TEST, lc822973_regs[SAP2973_TEST]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    /* VSYNC Latch enable */
    if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       |= TVOUT_VLSTOP_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    /***** LSI Initialize *****/
    /* DAC Power ON -- */
    if ( ( LOCAL_write_reg( SAP2973_CLKCONT, lc822973_regs[SAP2973_CLKCONT]
       |= TVOUT_DACON_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    if ( ( LOCAL_write_reg( SAP2973_BGCOLOR1, lc822973_regs[SAP2973_BGCOLOR1]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    //    if ( ( LOCAL_write_reg( SAP2973_BGCOLOR2, lc822973_regs[SAP2973_BGCOLOR2]
    //       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ;

    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_set_orientation( TVOUT_ORIENTATION_T orientation )
{
    TVOUT_ENCODING_T encoding;

    DEBUG_MSG("Entering LOCAL_set_orientation...\n");

    tvout.orientation = orientation;

    if ( ( tvout.output == DISABLE ) || ( tvout_session == STOPPED_POWERSAVE ) || ( tvout_session == STARTED_POWERSAVE ) )
    {
        return TVOUT_OK;
    }
    
    encoding = LOCAL_get_encoding_format();

    switch( orientation )
    {
        case LANDSCAPE:
            switch( encoding )
            {
                case NTSC_M:
		case PAL_M:
                    LOCAL_set_display_pointer( settings.mode[tvout.mode][TVOUT_NTSC_M_HOFSTL],
                                               settings.mode[tvout.mode][TVOUT_NTSC_M_VOFST], 
                                               settings.mode[tvout.mode][TVOUT_NTSC_M_HLENL],
                                               settings.mode[tvout.mode][TVOUT_NTSC_M_VLENL] );
                    LOCAL_set_scaling( settings.mode[tvout.mode][TVOUT_NTSC_M_HSCALEL],
                                       settings.mode[tvout.mode][TVOUT_NTSC_M_VSCALEL],
                                       TVOUT_ZERO, TVOUT_ZERO, settings.mode[tvout.mode][TVOUT_NTSC_M_SCALEHON],
                                       settings.mode[tvout.mode][TVOUT_NTSC_M_SCALEVON] );
                    LOCAL_set_scan_direction( TVOUT_SCANDIRECTION_BL_USCAN );
                    LOCAL_set_wfblen( TVOUT_WRITE_HLENL, TVOUT_WRITE_VLENL );
                    break;

		case PAL:
                default:
                    LOCAL_set_display_pointer( settings.mode[tvout.mode][TVOUT_PAL_HOFSTL],
                                               settings.mode[tvout.mode][TVOUT_PAL_VOFSTL], 
                                               settings.mode[tvout.mode][TVOUT_PAL_HLENL],
                                               settings.mode[tvout.mode][TVOUT_PAL_VLENL] );
                    LOCAL_set_scaling( settings.mode[tvout.mode][TVOUT_PAL_HSCALEL],
                                       settings.mode[tvout.mode][TVOUT_PAL_VSCALEL], TVOUT_ZERO,
                                       TVOUT_ZERO, settings.mode[tvout.mode][TVOUT_PAL_SCALEHON],
                                       settings.mode[tvout.mode][TVOUT_PAL_SCALEVON] );
                    LOCAL_set_scan_direction( TVOUT_SCANDIRECTION_BL_USCAN );
                    LOCAL_set_wfblen( TVOUT_WRITE_HLENL, TVOUT_WRITE_VLENL );
                    break;
            }
            break;
        
        case PORTRAIT:
        default:
	    switch( encoding )
	    {
	        case NTSC_M:
		case PAL_M:
	            LOCAL_set_display_pointer( settings.mode[tvout.mode][TVOUT_NTSC_M_HOFSTP],
                                               settings.mode[tvout.mode][TVOUT_NTSC_M_VOFST], 
                                               settings.mode[tvout.mode][TVOUT_NTSC_M_HLENP],
                                               settings.mode[tvout.mode][TVOUT_NTSC_M_VLENP] );
	            LOCAL_set_scaling( settings.mode[tvout.mode][TVOUT_NTSC_M_HSCALEP],
                                       settings.mode[tvout.mode][TVOUT_NTSC_M_VSCALEP], TVOUT_ZERO,
                                       TVOUT_ZERO, settings.mode[tvout.mode][TVOUT_NTSC_M_SCALEHON],
                                       settings.mode[tvout.mode][TVOUT_NTSC_M_SCALEVON] );
	            LOCAL_set_scan_direction( TVOUT_SCANDIRECTION_TL_RSCAN );
                LOCAL_set_wfblen( TVOUT_WRITE_HLENP, TVOUT_WRITE_VLENP );
                break;
                
	        case PAL:
	        default:
	            LOCAL_set_display_pointer( settings.mode[tvout.mode][TVOUT_PAL_HOFSTP],
                                               settings.mode[tvout.mode][TVOUT_PAL_VOFSTP],
                                               settings.mode[tvout.mode][TVOUT_PAL_HLENP],
                                               settings.mode[tvout.mode][TVOUT_PAL_VLENP] );
	            LOCAL_set_scaling( settings.mode[tvout.mode][TVOUT_PAL_HSCALEP],
                                       settings.mode[tvout.mode][TVOUT_PAL_VSCALEP], TVOUT_ZERO,
                                       TVOUT_ZERO, settings.mode[tvout.mode][TVOUT_PAL_SCALEHON],
                                       settings.mode[tvout.mode][TVOUT_PAL_SCALEVON] );
                LOCAL_set_scan_direction( TVOUT_SCANDIRECTION_TL_RSCAN );
                LOCAL_set_wfblen( TVOUT_WRITE_HLENP, TVOUT_WRITE_VLENP );
                break;
	    }
	    break;
    }

    return TVOUT_OK ;
}

TVOUT_ORIENTATION_T LOCAL_get_orientation(void)
{
    DEBUG_MSG("Entering LOCAL_get_orientation...\n");
    return tvout.orientation;
}

TVOUT_STATUS_T LOCAL_set_mode(TVOUT_MODE_T mode)
{
    if ( mode != tvout.mode )
    {
        tvout.mode = mode;
        if ( tvout_session == STARTED )
	{
            LOCAL_power_off();
            if ( tvout.output == ENABLE )
                LOCAL_enable_output();
	}
    }
    return TVOUT_OK;
}

TVOUT_MODE_T LOCAL_get_mode(void)
{
    return tvout.mode;
}

TVOUT_STATUS_T LOCAL_enable_output(void)
{
    DEBUG_MSG("Entering LOCAL_enable_output...\n");

    // Flag TV out output to be enabled
    tvout.output = ENABLE;

    LOCAL_power_on();

    /* Init SI register table */
    memset( lc822973_regs, 0, sizeof( lc822973_regs ) );
    
    /***** Initialize chip (memory, clocks, DAC etc.)  *****/
    LOCAL_init_chip();
    
    /***** Set encoding to PAL *****/ 
    LOCAL_set_encoding_format( tvout.encoding );

    DEBUG_MSG("Setting write FB area...\n");

    /***** Set write frame buffer area *****/
    if ( ( LOCAL_write_reg( SAP2973_WFBHSTART, lc822973_regs[SAP2973_WFBHSTART]
       = TVOUT_ZERO & TVOUT_DSPBUF_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_WFBVSTART, lc822973_regs[SAP2973_WFBVSTART]
       = TVOUT_ZERO & TVOUT_DSPBUF_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    /* Always set up 3 frame buffers */
    if ( ( LOCAL_write_reg( SAP2973_AVIEWSYS, lc822973_regs[SAP2973_AVIEWSYS]
       = TVOUT_AVIEWSYS_3BUF ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_AVWFBVSTART_1, lc822973_regs[SAP2973_AVWFBVSTART_1]
       = TVOUT_BUF2_VOFFSET ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_AVWFBVSTART_2, lc822973_regs[SAP2973_AVWFBVSTART_2]
       = TVOUT_BUF3_VOFFSET ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    DEBUG_MSG("Setting read FB area...\n");

    /***** Set read frame buffer area *****/
    if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       |= TVOUT_VLSTOP_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_RFBHOFST, lc822973_regs[SAP2973_RFBHOFST]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_RFBVOFST, lc822973_regs[SAP2973_RFBVOFST]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_SYSCTL2, lc822973_regs[SAP2973_SYSCTL2]
       &= ~TVOUT_VLSTOP_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    DEBUG_MSG("Setting autoview...\n");
  
    if ( ( LOCAL_write_reg( SAP2973_VIFSYS, lc822973_regs[SAP2973_VIFSYS]
       |= TVOUT_INTACT_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

     if ( ( LOCAL_write_reg( SAP2973_VIFHACTSTA, lc822973_regs[SAP2973_VIFHACTSTA]
       |= TVOUT_VIFHACTSTA ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_VIFHACTEND, lc822973_regs[SAP2973_VIFHACTEND]
       |= TVOUT_VIFHACTEND ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_VIFVACTSTA, lc822973_regs[SAP2973_VIFVACTSTA]
       |= TVOUT_VIFVACTSTA ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_VIFVACTEND, lc822973_regs[SAP2973_VIFVACTEND]
       |= TVOUT_VIFVACTEND ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_ENCBBPLT, lc822973_regs[SAP2973_ENCBBPLT]
       = TVOUT_ENCBBPLT ) != TVOUT_OK ) ) return TVOUT_ERROR ; 

    if ( ( LOCAL_write_reg( SAP2973_AUTOVIEWON, lc822973_regs[SAP2973_AUTOVIEWON]
       = TVOUT_ZERO ) != TVOUT_OK ) ) return TVOUT_ERROR ; 
  
    DEBUG_MSG("Turning display on...\n");
    //    if ( ( LOCAL_write_reg( SAP2973_SYSCTL1, lc822973_regs[SAP2973_SYSCTL1]
    //       |= TVOUT_ON_MASK ) != TVOUT_OK ) ) return TVOUT_ERROR ; 



    DEBUG_MSG("...done init!\n");

    return TVOUT_OK;
}

TVOUT_STATUS_T LOCAL_disable_output(void)
{
    DEBUG_MSG("Entering LOCAL_disable_output...\n");

    LOCAL_freeze_display();

    LOCAL_power_off();

    // Flag TV out output to be disabled
    tvout.output = DISABLE;
    
    return TVOUT_OK;
}

TVOUT_OUTPUT_T LOCAL_get_output( void )
{
    DEBUG_MSG("Entering LOCAL_get_output...\n");
    return tvout.output;
}

/*!
 * @brief Power on and initialization of the TV-out circuit 
 *
 * This function initializes the TV-out circuit by:
 * -Power on the TV-out circuit
 * -Initializes the i2c bus to the tv-out
 * -Initializes the TV-out circuit
 * -Enables the TV-out
 * -Disables the TV-out
 */
TVOUT_OUTPUT_T LOCAL_init_ic( void )
{
    int retval = TVOUT_OK;
    /* Temporary fix to get TV out chip out of crazy carnival mode - TO BE REMOVED */

    DEBUG_MSG("Entering LOCAL_init_ic...\n");

    LOCAL_i2c_register_driver(TVOUT_I2C_ADDR);

    if (LOCAL_enable_output() != TVOUT_OK)
    {
        INFO_MSG("could not enable TV-out\n");   
        retval = TVOUT_ERROR;
        goto power_off;
    }
    else
    {
        INFO_MSG("TV-out enabled\n");   
    }
    if (LOCAL_set_orientation(PORTRAIT) != TVOUT_OK)
    {
        INFO_MSG("could not set format : PORTRAIT\n"); 
        retval = TVOUT_ERROR;
        goto power_off;
    }
    else
    {
        INFO_MSG("format set: PORTRAIT\n");   
    }
    if (LOCAL_set_encoding_format(PAL) != TVOUT_OK)
    {
        INFO_MSG("could not set format : PAL\n");   
        retval = TVOUT_ERROR;
        goto power_off;
    }
    else
    {
        INFO_MSG("format set: PAL\n");   
    }
    if (LOCAL_disable_output() != TVOUT_OK)
    {
        INFO_MSG("could not disable TV-out\n");   
        retval = TVOUT_ERROR;
        goto power_off;
    }
    else
    {
        INFO_MSG("TV-out disabled\n");   
    }
    LOCAL_i2c_unregister_driver();

power_off:
    return TVOUT_OK;
}




/*==================================================================================================
                                        GLOBAL FUNCTIONS
==================================================================================================*/


 /*!
 * @brief Handles the addition of an I2C adapter capable of supporting the TVout Chip 
 *
 * This function is called by the I2C driver when an adapter has been added to the
 * system that can support communications with the TVout Chip.  The function will
 * attempt to register a I2C client structure with the adapter so that communication
 * can start.  If the client is successfully registered, the TVout initialization
 * function will be called to do any register writes required at power-up.
 *
 * @param    adap   A pointer to I2C adapter 
 *
 * @return   This function returns 0 if successful
 */
static int tvout_i2c_attach_adapter (struct i2c_adapter *adap)
{
    int retval;

    /* Allocate memory for client structure */
    tvout_lsi_i2c_client = kmalloc(sizeof(struct i2c_client), GFP_KERNEL);
      
    if(tvout_lsi_i2c_client == NULL)
    {
        return -ENOMEM;
    }

    memset(tvout_lsi_i2c_client, 0, (sizeof(struct i2c_client)));
    
    /* Fill in the required fields */
    tvout_lsi_i2c_client->adapter = adap;
    tvout_lsi_i2c_client->addr = tvout_i2c_address;
    tvout_lsi_i2c_client->driver = (struct i2c_driver *)&driver;
    
    /* Register our client */
    retval = i2c_attach_client(tvout_lsi_i2c_client);
    
    if(retval != 0)
    {
        /* Request failed, free the memory that we allocated */
        kfree(tvout_lsi_i2c_client);
        tvout_lsi_i2c_client = NULL;
    }

    return retval;
}

 /*!
 * @brief Handles the removal of the I2C adapter being used for the TVOUT Chip
 *
 * This function call is an indication that our I2C client is being disconnected
 * because the I2C adapter has been removed.  Calling the i2c_detach_client() will
 * make sure that the client is destroyed properly.
 *
 * @param   client   Pointer to the I2C client that is being disconnected
 *
 * @return  This function returns 0 if successful
 */
static int tvout_i2c_detach_client(struct i2c_client *client)
{
    return i2c_detach_client(client);
}

TVOUT_STATUS_T tvout_test_then_poweron( void )
{
    TVOUT_STATUS_T result;

    DEBUG_MSG("Entering tvout_test_then_poweron...\n");
    if ( tvout_session == STOPPED_POWERSAVE )
        tvout_session = STOPPED;

    if ( tvout_session == STARTED_POWERSAVE )
    {
        tvout_session = STARTED;
        result = LOCAL_i2c_register_driver(TVOUT_I2C_ADDR);
        if (result != 0)
            return TVOUT_ERROR_I2C_REGISTER_DRIVER;
	if ( tvout.output == ENABLE )
	{
            LOCAL_enable_output();
	}
    }
    return TVOUT_OK;
}
EXPORT_SYMBOL(tvout_test_then_poweron);

TVOUT_STATUS_T tvout_test_then_poweroff( void )
{
    TVOUT_STATUS_T result;

    DEBUG_MSG("Entering tvout_test_then_poweroff...\n");
    if ( tvout_session == STOPPED )
        tvout_session = STOPPED_POWERSAVE;
    if ( tvout_session == STARTED )
    {
        tvout_session = STARTED_POWERSAVE;
        result = LOCAL_i2c_unregister_driver();
        if (result != 0)
            return TVOUT_ERROR_I2C_UNREGISTER_DRIVER;
        LOCAL_power_off();
    }
    return TVOUT_OK;
}
EXPORT_SYMBOL(tvout_test_then_poweroff);

/*!
 * @brief The TV out ioctl callback function          
 *
 * This is the TV out ioctl callback function which handles 
 * the TV out ioctl commands.
 *
 * @param        inode       inode pointer
 * @param        file        file pointer
 * @param        cmd         the ioctl command
 * @param        arg         the ioctl argument
 *
 * @return 0 if successful
 */
static int tvout_file_operation_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
    int return_val = 0;
    DEBUG_MSG("Entering tvout_file_operation_ioctl ( 0x%hX, 0x%lX ) ...\n",cmd ,arg );
    /* Do not run non-TVOUT commands */
    if (_IOC_TYPE(cmd) != TVOUT_IOCTL_MAGIC)
    {
        ERROR_MSG("Invalid ioctl command type\n");
        return_val = -ENOTTY;
    }

    /* Do not run invalid commands */
    if (_IOC_NR(cmd) > TVOUT_IOCTL_MAX_COMMAND_NUMBER)
    {
        ERROR_MSG("Invalid ioctl command\n");
        return_val =  -ENOTTY;
    }

    if ( (cmd == TVOUT_IOCTL_START) && ( (tvout_session == STARTED ) || (tvout_session == STARTED_POWERSAVE) ) )
    {
        ERROR_MSG("Session already started\n");
        return_val = TVOUT_ERROR_SESSION;
    }

    /* Return error on TV out commands if session is not started */
    if ( (cmd != TVOUT_IOCTL_STOP) && (cmd != TVOUT_IOCTL_START) && ( (tvout_session == STOPPED) || (tvout_session == STOPPED_POWERSAVE ) ) )
    {
        ERROR_MSG("Unable to execute command when no session is started\n");
        return_val = TVOUT_ERROR_SESSION;
    }
    
    /* Return error on TV out commands if we are in powersave, and command is not a simple configuration command */
    if ( ( cmd != TVOUT_IOCTL_START ) && ( cmd != TVOUT_IOCTL_STOP ) && ( cmd != TVOUT_IOCTL_SET_ORIENTATION )
       && ( cmd != TVOUT_IOCTL_SET_ENCODING) && !( ( cmd == TVOUT_IOCTL_SET_POWERSTATE ) && ( arg == POWERON ) ) &&
       ( ( tvout_session == STOPPED_POWERSAVE ) || ( tvout_session == STARTED_POWERSAVE ) ) )
    {
        ERROR_MSG("Unable to execute command when powersave is started, only start, stop and (orientation, encoding and power save stop) changes are allowed\n");
        return_val = TVOUT_ERROR_SESSION;
    }

    /* Handle ioctl command */
    if ( down_interruptible( &cmd_in_progress ) )
    {
        return_val = EINTR;
    }
    else switch(cmd)
    {
        TVOUT_STATUS_T result;

        case TVOUT_IOCTL_START:
	    if ( tvout_session == STOPPED_POWERSAVE )
            {
                tvout_session = STARTED_POWERSAVE;
            }
            else // if tvout_session == STOPPED
	    {
                LOCAL_power_on();
                result = LOCAL_i2c_register_driver(TVOUT_I2C_ADDR);
                if (result != 0)
                    return TVOUT_ERROR_I2C_REGISTER_DRIVER;
                tvout_session = STARTED;
            }
            return_val = TVOUT_OK;
            break;

        case TVOUT_IOCTL_STOP:
	    if ( tvout_session == STARTED_POWERSAVE )
            {
                tvout_session = STOPPED_POWERSAVE;
            }
            else // if tvout_session == STARTED
	    {
                result = LOCAL_i2c_unregister_driver();
                if (result != 0)
                    return TVOUT_ERROR_I2C_UNREGISTER_DRIVER;
                LOCAL_power_off();
                tvout_session = STOPPED;
            }
            return_val = TVOUT_OK;
            break;

        case TVOUT_IOCTL_SET_OUTPUT:
            if ( ( arg == ENABLE ) && ( tvout.output != ENABLE ) )
                LOCAL_enable_output();
            if ( ( arg == DISABLE ) && ( tvout.output != DISABLE ) )
                LOCAL_disable_output();
            return_val = TVOUT_OK;
            break;

        case TVOUT_IOCTL_GET_OUTPUT:
            return_val = LOCAL_get_output();
            break;            

        case TVOUT_IOCTL_SET_ENCODING:
            if ( tvout.encoding != arg  )
	    {
                if ( tvout.output == ENABLE )
                {
	            LOCAL_freeze_display();
                    LOCAL_set_encoding_format(arg);
		    LOCAL_unfreeze_display();
                }
                else
                    return_val = LOCAL_set_encoding_format(arg);
	    }
            break;            
            
        case TVOUT_IOCTL_GET_ENCODING:
            return_val = LOCAL_get_encoding_format();
            break;            
            
        case TVOUT_IOCTL_SET_ORIENTATION:
	    if ( tvout.orientation != arg )
	    {
                if ( tvout.output == ENABLE )
                {
	            LOCAL_freeze_display();
                    LOCAL_set_orientation(arg);
	            LOCAL_unfreeze_display();
                }
                else
                    return_val = LOCAL_set_orientation(arg);
            }
            break;            

        case TVOUT_IOCTL_GET_ORIENTATION:
            return_val = LOCAL_get_orientation();
            break;                        

        case TVOUT_IOCTL_SET_MODE:
            return_val = LOCAL_set_mode(arg);
            break;            

        case TVOUT_IOCTL_GET_MODE:
            return_val = LOCAL_get_mode();
            break;                        
        
        case TVOUT_IOCTL_SET_POWERSTATE:
            if ( arg == POWERON )
                return_val = tvout_test_then_poweron();
            else if ( arg == POWEROFF )
                return_val = tvout_test_then_poweroff();
            break;            

        default: /* This shouldn't be able to happen, but just in case... */
            return_val = -ENOTTY;
            break;
    }
    up( &cmd_in_progress );
    return return_val;
}

/*!
 * @brief the open() handler for the TVout device node
 *
 * @param        inode       inode pointer
 * @param        file        file pointer
 *
 * @return 0 if successful
 */
static int tvout_file_operation_open(struct inode *inode, struct file *file)
{
    DEBUG_MSG("tvout_file_operation_open\n");
    return 0;
}

/*!
 * @brief the close() handler for the TVout device node
 *
 * @param        inode       inode pointer
 * @param        file        file pointer
 *
 * @return 0 if successful
 */
static int tvout_file_operation_close(struct inode *inode, struct file *file)
{
    DEBUG_MSG("tvout_file_operation_close\n");
    return 0;
}

/*!
 * @brief TVout LSI initialization function
 *
 * This function implements the initialization function of the TVout  driver.  
 *
 * @return 0 if successful
 */
int __init tvout_module_init(void)
{
    tvout_driver_module_major = register_chrdev(0, TVOUT_DEV_NAME,(struct file_operations *)&tvout_fops);

    if (tvout_driver_module_major < 0)
    {
        ERROR_MSG("Registering character device failed with %d\n", tvout_driver_module_major);
        return tvout_driver_module_major;
    }

    if (devfs_mk_cdev(MKDEV(tvout_driver_module_major,0), S_IFCHR | S_IRUGO |  S_IWUGO, TVOUT_DEV_NAME))
    {
        ERROR_MSG("Could not create character device\n");
        return -ENODEV;
    }
   
    proc_entry = create_proc_entry("tvout",0644,NULL);

    if (proc_entry == NULL)
    {
        ERROR_MSG("tvout_module_init: could not create proc entry\n");
	    return -ENOMEM;
    }
    else
    {
        proc_entry->read_proc = tvout_proc_read;
#ifdef DEBUG
        proc_entry->write_proc = tvout_proc_write;
#endif // DEBUG
        proc_entry->owner = THIS_MODULE;
        init_MUTEX( &cmd_in_progress );
    }

// HW bug workaround
 //   LOCAL_init_ic();

    INFO_MSG("Driver loaded\n");

    return(0);
}

/*!
 * @brief TVout cleanup function
 *
 * This function is called when the TVout driver is closed. Future change will
 * need to be made to free the tvout_lsi_i2c_client.
 */
void __exit tvout_module_exit(void)
{
    int result = 0;

#ifdef DEBUG
    remove_proc_entry(TVOUT_DEV_NAME,&proc_root);
#endif // DEBUG

    devfs_remove(TVOUT_DEV_NAME);

    result = unregister_chrdev(tvout_driver_module_major, TVOUT_DEV_NAME);
    if (result != 0)
        ERROR_MSG("result unregistering character device\n");

    if (tvout_lsi_i2c_client != NULL)
        result = LOCAL_i2c_unregister_driver();
        
    if (result == 0)
        INFO_MSG("Driver unloaded\n");
}

#ifdef DEBUG

/*!
 * @brief TVout proc interface write function
 *
 * This function initiates a read or a write depending on the given arguments.
    The argument formats are as follows:
    "A" ( write all registers to proc read buffer )
    "B" ( print proc read buffer )
    "C" ( close down I2C interface and power off )
    "D" ( delete proc read buffer )
    "E" ( run init sequence ) 
    "F" ( PAL/NTSC-M toggle)
    "H" ( print out help )
    "I addr" ( set I2C address and power on )
    "N addr value" ( null some bits in a register )
    "O addr value" ( or some bits into a register )
    "P" ( portrait/panorama toggle )
    "R addr" ( read register )
    "S addr value" ( set register )
    "T" ( enable output )
    "Q" ( disable output )
 *
 * This function is called in response to the echo "..." > /proc/tvout command
 */

static int tvout_proc_write(struct file *filep, const char __user *buff, unsigned long len, void *data)
{
    int result;
    unsigned char command;
    TVOUT_I2C_DATA_T data2;
    TVOUT_ORIENTATION_T orientation;
    TVOUT_ENCODING_T encoding;
    int temp1,temp2,i;

    sscanf(buff,"%c",&command);

    if ( tvout_lsi_i2c_client == NULL && command != 'H' && command != 'I' && command != 'T' )
    {
        DEBUG_MSG("tvout_proc_write: Sending command without I2C init. Use I command first.\n"); 
    }

    switch (command)
    {
        case 'A':
            // write all registers to proc read buffer
            DEBUG_MSG("tvout_proc_write(A): Writing all registers to proc read buffer:\n"); 
            proc_command = proc_A;
      	    for ( i = SAP2973_CLKCONT ; i < SAP2973_IMGWRITE ; i++ )
            {
                data2.reg_addr = i;
                data2.data = 0;
                result = LOCAL_i2c_read(&data2);
                if (result)
                { 
                    return result;
                }
            } //split to avoid IMGWRITE and IMGREADGO
            
            data2.reg_addr = SAP2973_IMGREAD;
            data2.data = 0;
            result = LOCAL_i2c_read(&data2);                      
            if (result)
            {
                return result;
            }  //split to avoid IMGABORT
            for ( i = SAP2973_IMGABORT + 1 ; i < SAP2973_OSDWRITE ; i++ )
            {
                data2.reg_addr = i;
                data2.data = 0;
                result = LOCAL_i2c_read(&data2);
                if (result)
                {
                    return result;
                }  
            } //split to avoid OSDWRITE and OSDABORT
      	    for ( i = SAP2973_OSDABORT + 1 ; i < SAP2973_AUTOVIEWOFF ; i++ )
            {
                data2.reg_addr = i;
                data2.data = 0;
                result = LOCAL_i2c_read(&data2);
                if (result)
                {
                    return result;
                }  
            } //split to avoid AUTOVIEWOFF
            for ( i = SAP2973_AUTOVIEWOFF + 1 ; i <= SAP2973_WSS_TRM ; i++ )
            {
                data2.reg_addr = i;
                data2.data = 0;
                result = LOCAL_i2c_read(&data2);
                if (result)
                {
                    return result;
                }  
            }
            data2.reg_addr = SAP2973_TEST;
            data2.data = 0;
            result = LOCAL_i2c_read(&data2);                      
            if (result)
            {
                return result;
            }  
            data2.reg_addr = SAP2973_STATUS;
            data2.data = 0;
            result = LOCAL_i2c_read(&data2);                      
            if (result)
            {
                return result;
            }  
            break;

        case 'B':
            // print proc read buffer
            DEBUG_MSG("tvout_proc_write(B): Preparing proc read buffer print\n"); 
            proc_command = proc_B;
            break;
        
        case 'C':
            // power off
            DEBUG_MSG("tvout_proc_write(C): closing I2C interface\n"); 
            proc_command = proc_C;
            LOCAL_i2c_unregister_driver();
            LOCAL_power_off();
            break;

        case 'D':
            // delete read proc buffer
            DEBUG_MSG("tvout_proc_write(D): deleting proc read buffer\n"); 
            proc_command = proc_D;
            proc_buffer_ptr = proc_buffer;
            break;

        case 'E':
            // power on
            proc_command = proc_E;
            DEBUG_MSG("tvout_proc_write(E): Power on\n");
            LOCAL_power_on();
            LOCAL_i2c_register_driver(TVOUT_I2C_ADDR);
            break;

        case 'F':
            // PAL/NTSC_M toggle
            proc_command = proc_F;
            DEBUG_MSG("tvout_proc_write(F): PAL/NTSC_M/PAL_M encoding\n");
            encoding = LOCAL_get_encoding_format();
	    switch (encoding)
	    {
		case PAL:
		    LOCAL_set_encoding_format( NTSC_M );
		    break;
		case NTSC_M:
		    LOCAL_set_encoding_format( PAL_M );
		    break;
		case PAL_M:
		    LOCAL_set_encoding_format( PAL );
		    break;
	    }
            break;

        case 'H':
            // set I2C address
            DEBUG_MSG("tvout_proc_write(H): Displaying help:\n");
            proc_command = proc_H;
            DEBUG_MSG("*************************************************\n");
            DEBUG_MSG("\"A\" ( write all registers to proc read buffer )\n");
            DEBUG_MSG("\"B\" ( print proc read buffer )\n");
            DEBUG_MSG("\"C\" ( close down I2C interface and power off )\n");
            DEBUG_MSG("\"D\" ( delete proc read buffer )\n");
            DEBUG_MSG("\"E\" ( power on and start I2C interface )\n");
            DEBUG_MSG("\"F\" ( PAL/NTSC_M/PAL_M toggle )\n");
            DEBUG_MSG("\"H\" ( print out help )\n");
            DEBUG_MSG("\"N addr value\" ( null some bits in a register )\n");
            DEBUG_MSG("\"O addr value\" ( or some bits into a register )\n");
            DEBUG_MSG("\"P \" ( landscape/portrait toggle)\n");
            DEBUG_MSG("\"R addr\" ( read register )\n");
            DEBUG_MSG("\"S addr value\" ( set register )\n");
            DEBUG_MSG("\"T\" enable output\n");
            DEBUG_MSG("\"Q\" disable output\n");
            DEBUG_MSG("*************************************************\n");
            DEBUG_MSG("echo <command [addr [value]]> > /proc/tvout ( to send command )\n");
            DEBUG_MSG("cat /proc/tvout ( to read proc read buffer out )\n");
            DEBUG_MSG("*************************************************\n");
            break;

        case 'N':
            // null some bits in a register
            proc_command = proc_N;
            sscanf(buff,"%*c %X %X",&temp1,&temp2);
            data2.reg_addr = temp1;
            data2.data = 0;
            result = LOCAL_i2c_read(&data2);
            if (result)
            {
                return result;
            }  
            DEBUG_MSG("tvout_proc_write(N): setting 0x%02X(0x%4X) &= ~0x%04X -> 0x%04X\n",
                          temp1,data2.data,  temp2, data2.data & ~temp2 ); 
            data2.data &= ~temp2;
            LOCAL_i2c_write(&data2);
	    break;

        case 'O':
            // or some bits into a register
            proc_command = proc_O;
            sscanf(buff,"%*c %X %X",&temp1,&temp2);
            data2.reg_addr = temp1;
            data2.data = 0;
            result = LOCAL_i2c_read(&data2);
            if (result)
            {
                return result;
            }  
            DEBUG_MSG("tvout_proc_write(O): setting 0x%02X(0x%04X) |= 0x%04X -> 0x%04X\n",
                          temp1, data2.data, temp2, data2.data | temp2 ); 
            data2.data |= temp2;
            result = LOCAL_i2c_write(&data2);
            if (result)
            {
                return result;
            }  
            break;

        case 'P':
            // portrait/landscape toggle
            proc_command = proc_P;
            DEBUG_MSG("tvout_proc_write(P): Landscape/Portrait mode\n");
            orientation = LOCAL_get_orientation();
            orientation == LANDSCAPE ? LOCAL_set_orientation( PORTRAIT )
                                                       : LOCAL_set_orientation( LANDSCAPE );
            break;

        case 'Q':
            // disable output
            proc_command = proc_Q;
            LOCAL_disable_output(); 
            break;
            
        case 'R':
            // read register
            proc_command = proc_R;
            sscanf(buff,"%*c %X",&temp1);	
            data2.reg_addr = temp1;
            result = LOCAL_i2c_read(&data2);
            DEBUG_MSG("tvout_proc_write(R): 0x%02X (0x%02X,0x%02X) = 0x%04X\n",
                          temp1, temp1 * 2, temp1 * 2 + 1, data2.data ); 
            if (result)
            {
                return result;
            }  
            break;

        case 'S':
            // set register
            proc_command = proc_S;
            sscanf(buff,"%*c %X %X",&temp1,&temp2);
            data2.reg_addr = temp1;
            data2.data = 0;
            result = LOCAL_i2c_read(&data2);
            if (result)
            {
                return result;
            }  
            DEBUG_MSG("tvout_proc_write(S): setting 0x%02X(0x%04X) = 0x%04X\n",
                          temp1, data2.data, temp2 ); 
            data2.data = temp2;
            result = LOCAL_i2c_write(&data2);
            if (result)
            {
                return result;
            }  
            break;

        case 'T':
            // enable output
            proc_command = proc_T;
            LOCAL_enable_output(); 
            break;
           
        default:
            break;
    }

    return len;
}
#endif // DEBUG

#ifndef DEBUG
/* Finds least significant set bit */
int least_significant_set_bit_index( int value )
{
    int retval = -1;
    if ( value )
    {
        retval = 0;
        while ( ! ( value & 0x1 ) )
	{
	    value >>= 0x1;
            retval++;
	} 
    }
    return retval;
}
#endif //DEBUG

/*!
 * @brief TVout proc interface read function
 *
 * This function does an I2C read and returns the two bytes read. The I2C register address
 * needs to be defined beforehand in the proc interface write function.
 *
 * This function is run in response to the 'cat' command
 */
static int tvout_proc_read(char *buf, char **start, off_t pos, int count, int *eof, void *data)
{
    char *p = buf;
#ifdef DEBUG
    char* ptr;
    int i;

    switch (proc_command)
    {
        case proc_A:
        case proc_B:
        case proc_C:
        case proc_D:
        case proc_E:
        case proc_F:
        case proc_H:
        case proc_N:
        case proc_O:
        case proc_P:
        case proc_Q:
        case proc_R:
        case proc_S:
        case proc_T:
            i = 0;
            for ( ptr = proc_buffer ; ptr < proc_buffer_ptr ; ptr++ )
            {
                p += sprintf( p, "%c", proc_buffer[ i++ ] );
            }
            proc_buffer_ptr = proc_buffer;
            break;        

        default:
            p += sprintf(p, "tvout_proc_read(%d): Nothing to do for proc_command\n", proc_command);
            break;        
    }
	
#else // if simply reading status

    int * value_ptr;
    int value1 = 0;
    int value2 = 0;
    int retval = 0;
    value_ptr = &value1;
    TVOUT_ENCODING_T encoding;
    TVOUT_ORIENTATION_T orientation;

    retval = power_ic_get_reg_value( POWER_IC_REG_ATLAS_PWR_MISC, least_significant_set_bit_index( ATLAS_REG_PWR_MISC_GPO1EN_06_MASK ), value_ptr, 0x1 );
    if ( value1 )
        value2 += ATLAS_REG_PWR_MISC_GPO1EN_06_MASK;

    value1 = 0;
    retval = power_ic_get_reg_value( POWER_IC_REG_ATLAS_PWR_MISC, least_significant_set_bit_index( ATLAS_REG_PWR_MISC_GPO2EN_08_MASK ), value_ptr, 0x1 );
    if ( value1 )
        value2 += ATLAS_REG_PWR_MISC_GPO2EN_08_MASK;

    value1 = 0;
    retval = power_ic_get_reg_value( POWER_IC_REG_ATLAS_REG_MODE_1, least_significant_set_bit_index( ATLAS_REG_MOD_1_VMMC1EN_18_MASK ), value_ptr, 0x1 );
    if ( value1 )
        value2 += ATLAS_REG_MOD_1_VMMC1EN_18_MASK;

    if ( value2 == ( ATLAS_REG_PWR_MISC_GPO1EN_06_MASK | ATLAS_REG_PWR_MISC_GPO2EN_08_MASK | ATLAS_REG_MOD_1_VMMC1EN_18_MASK ) )
        p+= sprintf( p, "TV Out Status: ON\n");
    else
        p+= sprintf( p, "TV Out Status: OFF\n");
        
    encoding = LOCAL_get_encoding_format();
    switch (encoding)
    {
	case PAL:
	    p += sprintf( p, "TV Out Format: PAL\n");
	    break;
	case NTSC_M:
	    p += sprintf( p, "TV Out Format: NTSC\n");
	    break;
	case PAL_M:
	    p += sprintf( p, "TV Out Format: PAL-M\n");
	    break;
    }

    orientation = LOCAL_get_orientation();
    switch (orientation)
    {
	case LANDSCAPE:
	    p += sprintf( p, "TV Out Orientation: LANDSCAPE\n");
	    break;
	case PORTRAIT:
	    p += sprintf( p, "TV Out Orientation: PORTRAIT\n");
	    break;
    }
	    
#endif //DEBUG
    return (p-buf);
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
/*
 * Module entry points
 */
module_init(tvout_module_init);
module_exit(tvout_module_exit);

MODULE_DESCRIPTION("TV out device driver v1.0");
MODULE_AUTHOR("Motorola");
MODULE_LICENSE("GPL");

#endif
