/*
 * Copyright (C) 2007-2008 Motorola, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 */

/*
 * Date         Author      Comment
 * ===========  ==========  ==================================================
 * 22-May-2007  Motorola    Initial revision
 * 18-Aug-2007  Motorola    Reworked TV out ioctl commands
 *                          Added type definitions for output, encoding, and orientation
 * 23-May-2008  Motorola    OSS CV fix
 * 04-Aug-2008  Motorola    Added PAL-M
 */
 
#ifndef __TVOUT_DRIVER_H__
#define __TVOUT_DRIVER_H__

/*==================================================================================================
                                            INCLUDES
==================================================================================================*/

#include <linux/types.h>

/*==================================================================================================
                                       GLOBAL DEFINITIONS
==================================================================================================*/

/*! @brief TV out device path */
#define TVOUT_DEV "/dev/tvout"

/*! @brief TV out ioctl magic value */
#define TVOUT_IOCTL_MAGIC 'T'

/*!
 * @brief Start TV out session
 *
 * This ioctl command starts the TV out session (initialize driver, power on TV out chip)
 */
#define TVOUT_IOCTL_START _IO(TVOUT_IOCTL_MAGIC,0)

/*!
 * @brief Stop TV out session
 *
 * This ioctl command stops the TV out session (power off TV out chip, deinitialize driver)
 */
#define TVOUT_IOCTL_STOP _IO(TVOUT_IOCTL_MAGIC,1)

/*!
 * @brief Set TV out output
 *
 * This ioctl command enables or disables the TV out signal
 */
#define TVOUT_IOCTL_SET_OUTPUT _IOW(TVOUT_IOCTL_MAGIC,2,TVOUT_OUTPUT_T)

/*!
 * @brief Get TV out output
 *
 * This ioctl command retrieves the current TV out output state
 */
#define TVOUT_IOCTL_GET_OUTPUT _IOR(TVOUT_IOCTL_MAGIC,3,TVOUT_OUTPUT_T)

/*!
 * @brief Set TV out encoding format
 *
 * This ioctl command sets the TV out encoding
 */
#define TVOUT_IOCTL_SET_ENCODING _IOW(TVOUT_IOCTL_MAGIC,4,TVOUT_ENCODING_T)

/*!
 * @brief Get TV out encoding format
 *
 * This ioctl command retrieves the current TV out encoding
 */
#define TVOUT_IOCTL_GET_ENCODING _IOR(TVOUT_IOCTL_MAGIC,5,TVOUT_ENCODING_T)

/*!
 * @brief Set TV out orientation
 *
 * This ioctl command sets the TV out orientation
 */
#define TVOUT_IOCTL_SET_ORIENTATION _IOW(TVOUT_IOCTL_MAGIC,6,TVOUT_ORIENTATION_T)

/*!
 * @brief Get TV out orientation
 *
 * This ioctl command retrieves the current TV out orientation
 */
#define TVOUT_IOCTL_GET_ORIENTATION _IOR(TVOUT_IOCTL_MAGIC,7,TVOUT_ORIENTATION_T)

/*!
 * @brief Set TV out mode
 *
 * This ioctl command sets the TV out mode
 */
#define TVOUT_IOCTL_SET_MODE _IOW(TVOUT_IOCTL_MAGIC,8,TVOUT_MODE_T)

/*!
 * @brief Get TV out mode
 *
 * This ioctl command retrieves the current TV out mode
 */
#define TVOUT_IOCTL_GET_MODE _IOR(TVOUT_IOCTL_MAGIC,9,TVOUT_MODE_T)

/*!
 * @brief Set TV out powerstate
 *
 * This ioctl command sets the current TV out powerstate
 */
#define TVOUT_IOCTL_SET_POWERSTATE _IOW(TVOUT_IOCTL_MAGIC,10,TVOUT_POWERSTATE_T)

#define TVOUT_IOCTL_MAX_COMMAND_NUMBER 10

/*==================================================================================================
                                     GLOBAL TYPE DEFINITIONS
==================================================================================================*/

/*!
 * @brief TV out return status type
 *
 * Defines return status
 */  
typedef enum 
{
    TVOUT_OK,
    TVOUT_ERROR,
    TVOUT_ERROR_SESSION,
    TVOUT_ERROR_I2C_REGISTER_DRIVER,
    TVOUT_ERROR_I2C_UNREGISTER_DRIVER
} TVOUT_STATUS_T;

/*!
 * @brief Output signal state type
 *
 * Defines TV out output states
 */  
typedef enum 
{
    ENABLE,
    DISABLE
} TVOUT_OUTPUT_T;

/*!
 * @brief Encoding format type
 *
 * Defines supported encoding formats
 */  
typedef enum 
{
    NTSC_M,
    PAL,
    PAL_M
} TVOUT_ENCODING_T;

/*!
 * @brief Orientation type
 *
 * Defines supported TV out orientations
 */  
typedef enum 
{
    PORTRAIT,
    LANDSCAPE
} TVOUT_ORIENTATION_T;

/*!
 * @brief Mode type
 *
 * Defines supported TV out modes
 */  
typedef enum 
{
    NORMAL,
    TEST
} TVOUT_MODE_T;

/*!
 * @brief Mode type
 *
 * Defines supported TV out powerstates
 */  
typedef enum 
{
    POWERON,
    POWEROFF
} TVOUT_POWERSTATE_T;

#endif /* __TVOUT_DRIVER_H__ */
