/*
 * FILE NAME
 *             profiler.h
 *
 * BRIEF MODULE DESCRIPTION:
 *             Header file for Profiler
 *
 * Copyright (c) 2006-2007 Motorola, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.
 *
 * Date        Author            Comment
 * ==========  ================  ========================
 * 10/04/2006  Motorola          Initial version.
 * 12/14/2006  Motorola          Increased interval to 100ms.
 * 01/04/2007  Motorola          Going down, just one up interval.
 * 09/29/2007  Motorola          Remove time related content
 */


#ifndef PROFILER_H
#define PROFILER_H

#include <linux/profiler_interface.h>
#include <asm/atomic.h>
// 20070104 -- Decided that slowing down fast is also a win.
#define MAX_WIN_NUM 1
#define SLIDING_NUM 2

//default profiling rate is 100ms
#define DEFAULT_PROFILER_INTERVAL 100

extern int profeq_clear(void);
extern int profeq_put(profiler_event_t * pe);

extern void arch_idle_wrapper(void);
extern unsigned long get_cycles_wrapper(void);
extern volatile int hlt_counter;

extern profiler_threshold_t prof_threshold;

extern void prof_init(void);
extern void prof_exit(void);
extern void profiler_start(void);
extern void profiler_stop(void);
extern volatile int profiler_interval;

typedef struct profiler_event_queue{
    atomic_t empty_flag;
    profiler_event_t event;
    wait_queue_head_t waitq;
}profiler_event_queue_t;

typedef struct profiler_window_queue{
    int head;
    int len;
    unsigned short data[MAX_WIN_NUM];
}profiler_window_queue_t;

#ifdef PROFILER_DEBUG
#define PROFILER_DPRINTK(format, args...)   printk(format, ##args)
#else
#define PROFILER_DPRINTK(format, args...)   
#endif

#endif  //end PROFILER_H
