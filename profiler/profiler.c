/* profiler.c - Profiler is responsible for monitoring CPU utilization
 *                               
 * Copyright (c) 2006-2007 Motorola, Inc.
 *
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Date        Author            Comment
 * ==========  ================  ========================
 * 10/04/2006  Motorola          Initial version.
 * 01/18/2007  Motorola          Tie DSM to IDLE and reduce IOI timeout to 10ms 
 * 09/27/2007  Motorola          Adjust idle calculation to make sure it is not
 *                               too high
 */

#include <linux/module.h>
#include <linux/pm.h>
#include <linux/timer.h>
#include <linux/jiffies.h>
#include <linux/time.h>
#include <linux/mpm.h>
#include <asm/arch/timex.h>
#include "profiler.h"

#ifdef PROFILER_DEBUG
#include <asm/arch/clock.h>
#include <linux/netlink.h>
#include <linux/relayfs_fs.h>
#include <linux/profiler_logger.h>
#include <linux/ip.h>
#include <linux/inet.h>

static struct sock *netlink_sock;
static int logging=0;
static int chan=-1;
static char str[500],buf[300];
static struct timeval tv;
static data_channel_conf_t *dconf=NULL;
static int pid=0;
#endif

unsigned long prof_start_time = 0;
// profiling interval is in ms
volatile int profiler_interval;
int current_profiler_interval;

static void process_result(void);


volatile short start_prof = 0;
unsigned long curr_idle_start_time = 0;
unsigned long curr_idle_stop_time = 0;

unsigned long aggregate_idle_time = 0; 
unsigned long prof_timeout = 0;
static void (*original_idle)(void);
unsigned short cpu_utilization = 0;


static profiler_window_queue_t prof_queue;

#ifdef PROFILER_DEBUG
/* the following are copied from mpm.c. 
   we will try to figure out how to use the exported function by another dynamic load module
*/  
#ifdef NOT_CONSTRAINING_TO_399
static mpm_op_t valid_op[] = { 133000000, 266000000, 399000000 };
#else
static mpm_op_t valid_op[] = { 399000000 };
#endif

static int num_op = sizeof (valid_op) / sizeof (valid_op[0]);
mpm_op_t mpm_getcur_op(void)
{
    mpm_op_t cur_op;
    int i;
    cur_op = mxc_get_clocks(CPU_CLK);
    /* round it off to the next mhz. Generally, the freq
       returned will be slightly less than what we requested */
    cur_op += 999999;
    cur_op = (cur_op/1000000) * 1000000;

    /* check if we got a valid freq */
    for ( i = 0; i < num_op ; i++ )
    {
        if ( cur_op == valid_op[i])
        {
            return cur_op;
        }
    }
    return 0;
}
#endif



static int profwin_full(void) {
    return (prof_queue.len < MAX_WIN_NUM) ? 0 : 1;
}

static void profwin_clear(void) {
    prof_queue.len = 0;
    prof_queue.head = 0;
}

static void profwin_put(unsigned short cpu_utilization) {
    prof_queue.data[prof_queue.head] = cpu_utilization;
    if(prof_queue.len != MAX_WIN_NUM)
        prof_queue.len++;
    prof_queue.head = (prof_queue.head + 1) % MAX_WIN_NUM;
}

#ifdef PROFILER_DEBUG
static char * profwin_display(void) {
    int len,head,start;
    len = prof_queue.len;
    head = prof_queue.head;
    start = (head + (MAX_WIN_NUM - len)) % MAX_WIN_NUM; 
    sprintf(buf,"Number of Data: %d ->",len);
    //PROFILER_DPRINTK(KERN_INFO "Buffer len : %d %d\n",len,start);
    while(len > 0){
        //PROFILER_DPRINTK("%d ",prof_queue.data[start]);
        sprintf(buf+strlen(buf),"%d ",prof_queue.data[start]);
        len--;
        start = (start + 1) % MAX_WIN_NUM;
    }
    //PROFILER_DPRINTK("\n");
    return buf;
}
#endif



static unsigned short profwin_compute_average(void) {
    int sum = 0;
    int i;
    for(i=0;i<MAX_WIN_NUM;i++){
        sum = sum + prof_queue.data[i];
    }
    return (unsigned short)(sum / MAX_WIN_NUM);      
} 

static void profwin_slide(unsigned short s) {
    if(s > prof_queue.len)
        prof_queue.len = 0;
    else
        prof_queue.len = prof_queue.len - s;
} 

static struct timer_list profiler_timer;

static void profiler_timer_timeout(unsigned long data)
{
    //compute CPU utilization
    process_result();
}

static void timer_init(void)
{
    init_timer(&profiler_timer);
    profiler_timer.function = profiler_timer_timeout;
    profiler_timer.data = 0;
}



void profiler_start(void)
{
    unsigned long jiff_diff = 0;
    if(start_prof == 1)
        return;
    start_prof = 1;
    PROFILER_DPRINTK(KERN_INFO "PROFILER_START called!\n");
    prof_start_time = jiffies;    
    curr_idle_start_time = 0;
    curr_idle_stop_time = 0;
    aggregate_idle_time = 0;
    current_profiler_interval = profiler_interval;
    //start profiler timer
    jiff_diff = msecs_to_jiffies(current_profiler_interval);
    profiler_timer.expires = jiffies + jiff_diff;
    prof_timeout = profiler_timer.expires;
    add_timer(&profiler_timer);	
#ifdef PROFILER_DEBUG
    if(logging){
        do_gettimeofday(&tv);
        sprintf(str,"%ld.%6ld,%s, CPU Frequecy: %ld Tresholds:(%d %d) Inteval:%d\n",
                tv.tv_sec,tv.tv_usec,"Start",mpm_getcur_op(), prof_threshold.high_threshold,
                prof_threshold.low_threshold, current_profiler_interval);
        relay_write(chan,str,strlen(str),-1,NULL);
    } 
#endif

}

void profiler_stop(void)
{
    if(start_prof == 0)
        return;
    start_prof = 0;
    del_timer(&profiler_timer);
    profwin_clear();
    profeq_clear();
    PROFILER_DPRINTK(KERN_INFO "PROFILER_STOP called\n");
#ifdef PROFILER_DEBUG
    if(logging){
        do_gettimeofday(&tv);
        sprintf(str,"%ld.%6ld,%s\n",tv.tv_sec,tv.tv_usec,"Stop");
        relay_write(chan,str,strlen(str),-1,NULL);
    } 
#endif
}



void post_process(void)
{
    long ticks_diff = 0;
    /* grab the idle end time */
    curr_idle_stop_time = get_cycles_wrapper(); 
    /* calculate total time for this idle period. 
    The time diff should beless than 2^31 */
    ticks_diff = (long)curr_idle_stop_time - (long)curr_idle_start_time;
#ifdef PROFILER_DEBUG
    if(ticks_diff <=0){
        PROFILER_DPRINTK(KERN_INFO "idle_start: %lu stop: %lu\n",
                         curr_idle_start_time, curr_idle_stop_time);
    }
#endif

    aggregate_idle_time += ticks_diff;
}


void profiler_idle(void)
{
    unsigned short average;
    profiler_event_t pe;
#ifdef PROFILER_DEBUG     
    struct timeval start_tv,stop_tv;
#endif
    local_irq_disable();
#ifdef PROFILER_DEBUG
        if(logging)
            do_gettimeofday(&start_tv);      
#endif   
    if(start_prof == 1){ 
        /* grab the idle start time */
        curr_idle_start_time = get_cycles_wrapper();   
        if(profwin_full()){
            average = profwin_compute_average();
            if(average < prof_threshold.low_threshold){ 
                pe.type = PROFILER_DEC_FREQ;
                pe.info = average;
                profeq_put(&pe);
                //PROFILER_DPRINTK(KERN_INFO "Post Bump Down event\n");
#ifdef PROFILER_DEBUG
               if(logging){
                   do_gettimeofday(&tv);
                   sprintf(str,"%ld.%6ld,%s,CPU:%u\n",tv.tv_sec,tv.tv_usec,"BUMPDOWN",average);
                   relay_write(chan,str,strlen(str),-1,NULL);
               }
#endif

            }
            if(average > prof_threshold.high_threshold){
                pe.type = PROFILER_INC_FREQ;
                pe.info = average;
                profeq_put(&pe);
                //PROFILER_DPRINTK(KERN_INFO "Post Bump UP event\n");
#ifdef PROFILER_DEBUG
               if(logging){
                   do_gettimeofday(&tv);
                   sprintf(str,"%ld.%6ld,%s,CPU:%u\n",tv.tv_sec,tv.tv_usec,"BUMPUP",average);
                   relay_write(chan,str,strlen(str),-1,NULL);
               }
#endif
            }
            profwin_slide(SLIDING_NUM);
#ifdef PROFILER_DEBUG
            //profwin_display();
#endif
        }
    }

    if(!need_resched() && !hlt_counter) {   
        latency_check();
        if (mpm_start_sleep() == 0)
            arch_idle_wrapper();

#ifdef PROFILER_DEBUG
        if(logging)
            do_gettimeofday(&stop_tv);
#endif
	if(start_prof == 1)
            post_process();
    }
#ifdef PROFILER_DEBUG
    else{
        if(logging)
             do_gettimeofday(&stop_tv);
    }
    if(logging){
        sprintf(str,"%ld.%6ld %ld.%6ld,%s\n",start_tv.tv_sec,start_tv.tv_usec,
                stop_tv.tv_sec, stop_tv.tv_usec,"Idle"); 
        relay_write(chan,str,strlen(str),-1,NULL);
    }
#endif
    local_irq_enable();
}


static void above_highwatermark(unsigned short cpu)
{
    profiler_event_t pe;
    profwin_put(cpu);
    pe.type = PROFILER_INC_FREQ;
    pe.info = cpu; 
    profeq_put(&pe);
    //PROFILER_DPRINTK(KERN_INFO "post bump UP \n");
#ifdef PROFILER_DEBUG
    if(logging){
      do_gettimeofday(&tv);
      sprintf(str,"%ld.%6ld,%s,CPU:%u\n",tv.tv_sec,tv.tv_usec,"BUMPUP",cpu);
      relay_write(chan,str,strlen(str),-1,NULL);
    }
#endif
}	


static void process_result(void)
{
    unsigned short idle;
    unsigned long jiff_diff;

    idle= (100*aggregate_idle_time)/((current_profiler_interval*CLOCK_TICK_RATE)/1000);

    /* this is a hack to deal with the precision of the divisor */
    if (idle > 100)
        idle = 100;

    cpu_utilization = 100 - idle; 
    //PROFILER_DPRINTK(KERN_INFO "CPU : %u idle : %lu interval : %u \n",
    //                 cpu_utilization,aggregate_idle_time,current_profiler_interval);
   
    if(cpu_utilization > prof_threshold.high_threshold)
        above_highwatermark(cpu_utilization);
    else 
        profwin_put(cpu_utilization);

     
#ifdef PROFILER_DEBUG
    //profwin_display();
#endif

#ifdef PROFILER_DEBUG
    if(logging){
      do_gettimeofday(&tv);
      sprintf(str,"%ld.%6ld,%s,CPU:%u %s\n",tv.tv_sec,tv.tv_usec,
              "Profiling",cpu_utilization, profwin_display());
      relay_write(chan,str,strlen(str),-1,NULL);
    }
#endif
    
    /* record the start time for new profiling */
    prof_start_time = jiffies;   
    jiff_diff =  msecs_to_jiffies(current_profiler_interval);
    prof_timeout = prof_start_time + jiff_diff;
    aggregate_idle_time = 0;      
    curr_idle_start_time = 0;
    curr_idle_stop_time = 0;
    mod_timer(&profiler_timer,prof_timeout);
}


#ifdef PROFILER_DEBUG
static void msg_process(struct sk_buff *skb)
{
    struct nlmsghdr *nlh = NULL;
    void *data;
    int channel_flags;
    nlh = (struct nlmsghdr *)skb->data;
    pid = nlh->nlmsg_pid;
    data = NLMSG_DATA(nlh);
    switch (nlh->nlmsg_type) {
    case APP_START:
        if(chan >= 0){
            printk("relay channel has already established\n");
            nlh->nlmsg_type = APP_START_ACK_FAILURE;
            netlink_unicast(netlink_sock,skb,pid,MSG_DONTWAIT); 
            break;
         }   
        dconf = (data_channel_conf_t *)data; 
        channel_flags = RELAY_DELIVERY_PACKET|RELAY_USAGE_GLOBAL|RELAY_SCHEME_ANY|RELAY_TIMESTAMP_ANY;
        chan = relay_open(dconf->filename,dconf->buf_size,
			  dconf->number_bufs, channel_flags,NULL,0,0,0,0,0,0,NULL,0);
	if(chan<0) {
	       printk("relay channel creation failed\n");
               nlh->nlmsg_type = APP_START_ACK_FAILURE;
               netlink_unicast(netlink_sock,skb,pid,MSG_DONTWAIT);
	}else {
            nlh->nlmsg_type = APP_START_ACK_SUCCESS;
            netlink_unicast(netlink_sock,skb,pid,MSG_DONTWAIT); 
         } 
	break;
	case APP_STOP:
            logging = 0;
            relay_close(chan);
            chan = -1;
            break;
	case LOGGING_START:
	    logging = 1;
	    break;
	case LOGGING_STOP:
	    logging = 0;
	    break;
	default:
            break;	
    }
}


static void netlink_handler(struct sock *sk, int len)
{
    struct sk_buff * skb;
    while((skb = skb_dequeue(&sk->sk_receive_queue))!= NULL){
        msg_process(skb);
    }
}

static int log_init(void)
{
    
    netlink_sock = netlink_kernel_create(NETLINK_USERSOCK,netlink_handler);
    if(!netlink_sock){
        printk("Can not create the netlink channel\n");
        return -1;
    }else
        return 0;  
}   

static void log_exit(void)
{
	struct sk_buff *skb;
	struct nlmsghdr *nlh;
	int size;
        int err;
		
	size = NLMSG_SPACE(0);
	skb = alloc_skb(size, GFP_KERNEL);
	nlh = NLMSG_PUT(skb, pid,1,KERNEL_EXIT, size - sizeof(*nlh));
	nlh->nlmsg_flags = 0;
        err = netlink_unicast(netlink_sock, skb, pid, MSG_DONTWAIT);
 nlmsg_failure:
	if(skb)
	    kfree_skb(skb);
}
#endif


void prof_init(void)
{
    original_idle = pm_idle;
    pm_idle = profiler_idle;
    start_prof = 0;
#ifdef PROFILER_DEBUG
    logging = 0;
    log_init();
#endif 
    PROFILER_DPRINTK(KERN_INFO "prof_init got called \n");
    timer_init();
}


void prof_exit(void)
{	
    pm_idle = original_idle;
    profiler_stop();
#ifdef PROFILER_DEBUG
    if(pid!=0)
      log_exit();
#endif      
    PROFILER_DPRINTK("prof_exit.\n");
}
