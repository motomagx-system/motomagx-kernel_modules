/*
 * FILE NAME
 *             profiler_interface.c
 *
 * BRIEF MODULE DESCRIPTION:
 *             Profiler Interface between PMDaemon and Profiler
 *
 * Copyright 2006 Motorola, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Date        Author            Comment
 * ==========  ================  ========================
 * 10/04/2006  Motorola          Initial version.
 */


#include <linux/init.h>
#include <linux/config.h>
#include <linux/module.h>

#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/capability.h>
#include <asm/atomic.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/ioctl.h>
#include <linux/fcntl.h>
#include <linux/poll.h>
#include <linux/spinlock.h>
#include <linux/profiler_interface.h>
#include "profiler.h"

MODULE_LICENSE("GPL");

profiler_event_queue_t prof_event_queue;
profiler_interval_t prof_interval;
profiler_threshold_t prof_threshold;

static atomic_t open_count = ATOMIC_INIT(1);

static DEFINE_SPINLOCK(event_lock);
static DECLARE_MUTEX(sem_interval); // semaphore for interval
static DECLARE_MUTEX(sem_threshold); //semaphore for threshold 


static int profeq_init(void)
{
    atomic_set(&prof_event_queue.empty_flag,1);
    init_waitqueue_head(&prof_event_queue.waitq);
    return 0;
}

static int profeq_empty(void)
{
    return atomic_read(&prof_event_queue.empty_flag)!=0;
}

static profiler_event_t* profeq_get(void)
{
    atomic_set(&prof_event_queue.empty_flag, 1);
    return  &prof_event_queue.event;
}

int profeq_clear(void)
{
    atomic_set(&prof_event_queue.empty_flag, 1);
    return 0;
}

int profeq_put(profiler_event_t *pe)
{ 
    unsigned long flags;
    spin_lock_irqsave(&event_lock,flags);
    memcpy(&prof_event_queue.event, pe, sizeof(profiler_event_t));
    atomic_set(&prof_event_queue.empty_flag,0);
    spin_unlock_irqrestore(&event_lock,flags);
    /* wake up the waiting process */
    wake_up_interruptible(&prof_event_queue.waitq);
    return 0;
}


static int profiler_open(struct inode *inode, struct file *filep)
{

    if (!atomic_dec_and_test (&open_count)) 
    {
        atomic_inc(&open_count);
        return -EBUSY; 
    }
    if((!capable(CAP_SYS_ADMIN))||((filep->f_flags & O_ACCMODE) != O_RDONLY ))
    {
        atomic_inc(&open_count);
        return -EPERM; 
    }
    profiler_interval = DEFAULT_PROFILER_INTERVAL;
    prof_interval.lower_bound_interval = DEFAULT_PROFILER_INTERVAL;
    prof_interval.upper_bound_interval = DEFAULT_PROFILER_INTERVAL;
    memset(&prof_threshold,0, sizeof(profiler_threshold_t));
    PROFILER_DPRINTK(KERN_INFO "Profiler_Interface: Opened successful.\n");
    return 0;      
}

static int profiler_close( struct inode *inode, struct file *filep )
{
    atomic_inc(&open_count); 
    profiler_stop();
    PROFILER_DPRINTK(KERN_INFO "Profiler_Interface: Closed successful.\n");
    return 0;
}



static ssize_t profiler_read(struct file *filep, char *buf, size_t count,
                             loff_t *f_pos)
{
    DECLARE_WAITQUEUE(wait, current);
    profiler_event_t pe;
    ssize_t retval = 0;
    unsigned long flags;

    if (! capable(CAP_SYS_ADMIN))
        return - EPERM;

    if (count < sizeof(profiler_event_t))
        return -EINVAL;

    add_wait_queue(&prof_event_queue.waitq, &wait);
    set_current_state(TASK_INTERRUPTIBLE);
    PROFILER_DPRINTK(KERN_INFO "Got a read request from user space! \n");

    for (;;) {
        /* Check if there is an event. */	
        spin_lock_irqsave(&event_lock,flags);
        if (!profeq_empty()){
            memcpy(&pe,profeq_get(),sizeof(profiler_event_t));
	    retval = 0;
            spin_unlock_irqrestore(&event_lock,flags);
	    break;
        }
        spin_unlock_irqrestore(&event_lock,flags);
       	/* Check if it is in nonblock mode */ 		
        if (filep->f_flags & O_NONBLOCK) {
            retval = -EAGAIN;
            break;
        }
        if (signal_pending(current)) {
            retval = -ERESTARTSYS;
            break;
        }
	/* No events, block it.	*/
        PROFILER_DPRINTK(KERN_INFO "No Event in Queue, Going to Sleep!\n");
        schedule();
        PROFILER_DPRINTK(KERN_INFO "Got a new event and woke up\n");
    }
    /* pass the event to user space.	*/
    if(retval == 0){
        retval = copy_to_user( (profiler_event_t *)buf, &pe, 
                               sizeof(profiler_event_t));
        if(retval) {
            retval = -EFAULT;
        }else{
            retval = sizeof(profiler_event_t);
            PROFILER_DPRINTK(KERN_INFO "Read request is served!\n");
        }
    }	
    set_current_state(TASK_RUNNING);
    remove_wait_queue(&prof_event_queue.waitq, &wait);
    return retval;
}


/* poll for profiler event */
static unsigned int profiler_poll(struct file *file, poll_table *wait) 
{
    if (! capable(CAP_SYS_ADMIN))
        return - EPERM;

    poll_wait(file, &prof_event_queue.waitq, wait);
    PROFILER_DPRINTK(KERN_INFO "Profiler_Interface: polled successful.\n");
    if (!profeq_empty())
        return POLLIN | POLLRDNORM;
    return 0;
}

/* ioctl function  */
static int profiler_ioctl (struct inode *inode, struct file *file, 
                           unsigned int cmd, unsigned long arg)
{
    int result=0;
#ifdef PROFILER_DEBUG
    profiler_event_t pe; 
#endif
    switch ( cmd ) {
    case PROFILER_IOC_SET_PROF_INTERVAL:	
        if(down_interruptible(&sem_interval)){
            return -ERESTARTSYS;
        }
        if(copy_from_user(&prof_interval, (void *)arg, 
                          sizeof(profiler_interval_t))){
            up(&sem_interval);
            return -EFAULT;
        
        } 
        PROFILER_DPRINTK(KERN_INFO "threshold: %d %d\n", 
                prof_interval.upper_bound_interval,
                prof_interval.lower_bound_interval);
        up(&sem_interval);
        /* fixed profiler interval. If get wrong interval, then stop profiler */
        if((prof_interval.lower_bound_interval != prof_interval.upper_bound_interval) ||
           prof_interval.upper_bound_interval > 500 || 
           prof_interval.lower_bound_interval < 10){
            profiler_interval = DEFAULT_PROFILER_INTERVAL;
            prof_interval.lower_bound_interval = DEFAULT_PROFILER_INTERVAL;
            prof_interval.upper_bound_interval = DEFAULT_PROFILER_INTERVAL;
            profiler_stop();
            return -EINVAL;
        } 
        profiler_interval = prof_interval.lower_bound_interval; 
        break;

    case PROFILER_IOC_GET_PROF_INTERVAL:
        PROFILER_DPRINTK(KERN_INFO "Got GET_PROF_INTERVAL request.\n");
        if(down_interruptible(&sem_interval))
             return -ERESTARTSYS;
        if(copy_to_user((void *)arg,&prof_interval,
                        sizeof(profiler_interval_t))){
            up(&sem_interval);
            return -EFAULT;
        }
        up(&sem_interval);
        break;

    /* Set threshold(assuming profiler is stopped before starting profiler) */	
    case PROFILER_IOC_SET_THRESHOLD:
        if(down_interruptible(&sem_threshold))
             return -ERESTARTSYS;
        if (copy_from_user(&prof_threshold,(void *)arg, 
                           sizeof(profiler_threshold_t))){
            up(&sem_threshold);
            return -EFAULT;
        }
        up(&sem_threshold);
        PROFILER_DPRINTK(KERN_INFO "### threshold: %d %d\n", prof_threshold.low_threshold,
                prof_threshold.high_threshold);
        /* If PMDaemon set wrong threshold, then stop profiler and set threshold to 0 */
        if((prof_threshold.low_threshold > prof_threshold.high_threshold)||
           prof_threshold.low_threshold > 100 || prof_threshold.high_threshold > 100){
            profiler_stop();
            memset(&prof_threshold,0, sizeof(profiler_threshold_t));
            return -EINVAL;
        } 
        if((prof_threshold.low_threshold == 0 || prof_threshold.low_threshold == 100) &&
            (prof_threshold.high_threshold == 0 || prof_threshold.high_threshold == 100)){
            profiler_stop();
        }else {
            profiler_start();
        }
        break;
	
    case PROFILER_IOC_GET_THRESHOLD:
        if(down_interruptible(&sem_threshold))
            return -ERESTARTSYS;
        PROFILER_DPRINTK( KERN_INFO "Got GET_THRESHOLD request.\n");
        if (copy_to_user((void *)arg, &prof_threshold,
                         sizeof(profiler_threshold_t))){
            up(&sem_threshold);
            return -EFAULT;
        }
        up(&sem_threshold);
        break;		

    /* get rid of the event in queue */	
    case PROFILER_IOC_CLEAR_EVENTQ:
        profeq_clear();
	break;
#ifdef PROFILER_DEBUG
    case PROFILER_IOC_SET_EVENT:
        PROFILER_DPRINTK(KERN_INFO "Got set event\n");
        if ( copy_from_user( &pe, (profiler_event_t *)arg, 
                    sizeof(profiler_event_t)) ) {
                return -EFAULT;
        }
        event_post(&pe);
        break;
#endif

    default:
        result = -ENOIOCTLCMD;
        break;
    }
    return result;
}	

static struct file_operations profiler_fops = {
    .owner = THIS_MODULE,
    .open = profiler_open,
    .read = profiler_read,
    .poll = profiler_poll,
    .ioctl = profiler_ioctl,
    .release = profiler_close,
};

static struct miscdevice profiler_misc_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .name = PROFILER_DEVICE_NAME,
    .fops = &profiler_fops,
    .devfs_name = PROFILER_DEVICE_NAME
};


static int profiler_suspend(struct device * dev, u32 state, u32 level)
{
    switch (level) {
    case SUSPEND_NOTIFY:
        break;
    case SUSPEND_SAVE_STATE:
        break;
    case SUSPEND_POWER_DOWN:
        break;
    }
    return 0;
};

static int profiler_resume(struct device * dev, u32 level)
{
    switch (level) {
    case RESUME_RESTORE_STATE:
        break;
    case RESUME_POWER_ON:
        break;
    }
    return 0;
};


static void profiler_release(struct device *dev)
{
    PROFILER_DPRINTK(KERN_INFO "profiler_release\n");
}


static struct device_driver profiler_driver = {
    .name = PROFILER_DEVICE_NAME,
    .bus = &platform_bus_type,
    .probe = NULL,
    .remove = NULL,
    .suspend = profiler_suspend,
    .resume = profiler_resume,
};

static struct platform_device profiler_device = {
    .name = PROFILER_DEVICE_NAME,
    .id = 1,
    .dev = {
               .release = profiler_release,
           },
};


/*
 * This function is used to initialize the profiler_interface driver module. 
 * The function registers the profiler_interface callback functions with 
 * the core misc driver and registers the power management callback functions
 * with the kernel PM system. 
 */

static int __init profiler_init( void )
{
    int rc;
    profeq_init();
    prof_init();
    if ( (rc = misc_register( &profiler_misc_device )) != 0 ) {
        return -EBUSY;
    }else{
        rc = driver_register(&profiler_driver);
        if (rc == 0){
            rc = platform_device_register(&profiler_device);
            if( rc != 0)
                driver_unregister(&profiler_driver);
        }
    }
    PROFILER_DPRINTK(KERN_INFO "Profiler_Interface:Loaded successful.\n");
    return 0;
}

/* This function is called whenever the module is removed from the kernel. */

static void __exit profiler_exit( void )
{
    prof_exit();
    platform_device_unregister(&profiler_device);
    driver_unregister(&profiler_driver);
    misc_deregister(&profiler_misc_device);
    PROFILER_DPRINTK(KERN_INFO "Profiler_Interface: Unloaded successful.\n");
}

module_init(profiler_init);
module_exit(profiler_exit);
