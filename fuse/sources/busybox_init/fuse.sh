#!/bin/sh

#==============================================================================
#
#   File Name: fuse.sh
#
#   General Description: This file executes the startup and shutdown procedures
#   for the FUSE Module.
#
#==============================================================================
#
#   Copyright (C) 2006 Motorola, Inc.
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#  
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#  
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
#   Date         Author           Comment
#   -----------  --------------   --------------------------------
#   2006-06-21   Motorola         Initial file creation










INSMOD="/sbin/insmod"
RMMOD="/sbin/rmmod"
FUSE_PATH="/lib/modules"
FUSE_MOD_NAME="fuse.ko"
FUSE_DEV_NODE="/dev/fuse"
FUSE_DEV_TYPE="c"
FUSE_NODE_MAJOR="10"
FUSE_NODE_MINOR="229"

[ `/usr/bin/id -u` = 0 ] || exit 1

startup () {

    if [ ! -e ${FUSE_DEV_NODE} ]; then
        mknod ${FUSE_DEV_NODE} ${FUSE_DEV_TYPE} ${FUSE_NODE_MAJOR} ${FUSE_NODE_MINOR}
    fi
    echo "Starting FUSE"
    ${INSMOD} ${FUSE_PATH}/${FUSE_MOD_NAME}
}

shutdown () {
    rm -rf ${FUSE_DEV_NODE}
    echo "Stopping FUSE"
    ${RMMOD} ${FUSE_MOD_NAME}
}

restart () {
    shutdown
    startup
}

case "$1" in
    start)
        startup
    ;;
    stop)
        shutdown
    ;;
    restart)
        restart
    ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
esac
