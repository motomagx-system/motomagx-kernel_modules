/*
 *  camera_misc.c
 *
 *  Camera Misc driver.
 *
 *  Copyright (C) 2006-2007 Motorola Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 Revision History:
 Modification     Tracking
 Author                 Date          Number     Description of Changes
 Motorola               May 15, 2006                Initial Version
 Motorola               Oct 20, 2006                Remove sensor_active and 
                                                      sensor_inactive call.
 Motorola               Dec 27, 2006                Clean unused code
 Motorola               Nov 03, 2007                Add necessary log
 Motorola               Nov 06, 2007                Add retry logic fro i2c write
 Motorola               Dec 01, 2007                Update retry logic fro i2c write
----------------   ------------    ----------   -------------------------
*/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/types.h>
#include <linux/delay.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/miscdevice.h>
#include <linux/power_ic.h>
#include <linux/power_ic_kernel.h>
#include <camera_misc.h>
#include <asm/uaccess.h>
#include <asm/arch/gpio.h>
#include <asm/arch/mxc_i2c.h>
#include <asm/mot-gpio.h>


static int camera_dev_open(struct inode *inode, struct file *file);
static int camera_dev_release(struct inode *inode, struct file *file);
static int camera_dev_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);

#define DEBUG   0

#define err_print(fmt, args...) printk(KERN_ERR "fun %s "fmt"\n", __FUNCTION__, ##args)

#if DEBUG > 0

#define dbg_print(fmt, args...) printk(KERN_INFO "fun %s "fmt"\n", __FUNCTION__, ##args)

#if DEBUG > 1
#define ddbg_print(fmt, args...) printk(KERN_INFO "fun %s "fmt"\n", __FUNCTION__, ##args)
#else
#define ddbg_print(fmt, args...) ;
#endif

#else

#define dbg_print(fmt, args...)  ;
#define ddbg_print(fmt, args...) ;

#endif

#define I2C_BUS         0   /* bus id on SCM-A11 */

#define I2C_CAMERA      0x86

static int g_open_count = 0;
static int g_slave_set = 0;
static int g_slave_addr = 0x5D;

static struct file_operations camera_dev_fops = {
    .open           = camera_dev_open,
    .release        = camera_dev_release,
    .ioctl          = camera_dev_ioctl
};

static struct miscdevice cam_misc_device0 = {
    .minor          = CAMERA0_MINOR,
    .name           = "camera0",
    .fops           = &camera_dev_fops
};

static int camera_dev_open(struct inode *inode, struct file *file)
{
    dbg_print("Camera_MiscDriver: enter camera_dev_open\n");
    if (0==g_open_count)
    {
        /* Enable power to camera */
        power_ic_periph_set_camera_on(POWER_IC_PERIPH_ON);
        /* VCAM rise-time measured to be about 100us */
        /* insert 200us delay to be safe */
        err_print("Camera_MiscDriver: set POWER_IC_PERIPH_ON\n");
        udelay(200);
        g_slave_set = 0;
    }

    g_open_count ++;
    return 0;
}

static int camera_dev_release(struct inode *inode, struct file *file)
{
    dbg_print("Camera_MiscDriver: enter camera_dev_release\n");

    g_open_count --;

    if (0==g_open_count)
    {
        /* Disable power to camera */
        power_ic_periph_set_camera_on(POWER_IC_PERIPH_OFF);
        /* VCAM fall-time measured to be about 15ms */
        /* insert 20ms-delay to be safe */
        err_print("Camera_MiscDriver: set POWER_IC_PERIPH_OFF\n");
        mdelay(20);
        g_slave_set = 0;
    }
    return 0;
}

static char *camera_buffer_ready(unsigned long arg, camera_i2c_register_t *param)
{
    char *pbuffer=NULL;

    if (0==g_slave_set)
    {
        err_print("Camera_MiscDriver: camera_buffer_ready g_slave_set is ZERO!");
        return NULL;
    }

    if (copy_from_user(param, (void *)arg, sizeof(camera_i2c_register_t)))
    {
        err_print("Camera_MiscDriver: I2C_READ copy_from_user error! 0x%04x\n",
                  param->size);
        return NULL;
    }

    dbg_print("Camera_MiscDriver: I2C_READ size=%d\n", param->size);

    if (param->size > CAMERA_DATA_ARRAY_LEN)
    {
        pbuffer = kmalloc( param->size, GFP_KERNEL);
        if (NULL==pbuffer)
        {
            err_print("Camera_MiscDriver: I2C_READ kmalloc error!\n");
            return NULL;
        }

        if ( copy_from_user(pbuffer, param->data.pointer, 
            param->size) )
        {
            err_print("Camera_MiscDriver: I2C_READ copy_from_user error!\n");
            kfree(pbuffer);
            pbuffer=NULL;
        }
    }
    else
    {
        pbuffer = param->data.array;
    }

    return pbuffer;
}

static int camera_read_buffer(camera_i2c_register_t param, char *pbuffer)
{
#if DEBUG > 0
    int i = 0;
#endif

    int ret = 0;

    if(param.size>CAMERA_DATA_ARRAY_LEN)
    {
#if DEBUG > 0
        for ( i=0; i< param.size; i++)
        {
            dbg_print("Camera_MiscDriver: camera ioctl data[%d]=0x%02x\n", i, 
                param.data.pointer[i]);
        }
#endif

        if ( copy_to_user(
            param.data.pointer,
            pbuffer,
            param.size))
        {
            err_print("Camera_MiscDriver: I2C_READ copy_to_user error!\n");
            ret=-EFAULT;
        }
    }
#if DEBUG > 0
    else
    {
        for ( i=0; i< param.size; i++)
        {
            dbg_print("Camera_MiscDriver: camera ioctl data[%d]=0x%02x\n", i, 
                param.data.array[i]);
        }
    }
#endif
    return ret;
}

static int camera_cmd_read(unsigned long arg)
{
    int ret = 0;
    char *pbuffer=NULL;
    camera_i2c_register_t    param;

    dbg_print("Camera_MiscDriver: enter camera_cmd_read\n");
    pbuffer=camera_buffer_ready(arg, &param);
    if (NULL==pbuffer)
    {
        err_print("Camera_MiscDriver: camera_buffer_ready return NULL!\n");
        return (-EFAULT);
    }

    dbg_print("Camera_MiscDriver: camera i2c_transfer to read\n");
    if ( 0 != (ret=mxc_i2c_raw_read(I2C_BUS, 
        g_slave_addr,
        param.size, pbuffer)))
    {
        err_print("Camera_MiscDriver: mxc_i2c_raw_read FAILED!\n");
        err_print("Camera_MiscDriver: mxc_i2c_raw_read return %d, size = %d\n",
                  ret, param.size);

        ret=-EFAULT;
        goto read_exit;
    }
    dbg_print("Camera_MiscDriver: camera i2c_transfer to read ok\n");

    ret=camera_read_buffer(param, pbuffer);
    if (ret!=0)
    {
        err_print("Camera_MiscDriver: camera_read_buffer error %d!\n", ret);
        ret=-EFAULT;
        goto read_exit;
    }

    if (copy_to_user((void *)arg, &param, sizeof(camera_i2c_register_t)))
    {
        err_print("Camera_MiscDriver: I2C_READ copy_to_user error %d!\n", ret);
        ret=-EFAULT;
        goto read_exit;
    }

read_exit:
    if ((pbuffer) && (pbuffer != param.data.array))
    {
        kfree(pbuffer);
    }

    return ret;
}

#define CAM_MISC_CMDWRITE_RETRY        0
static int camera_cmd_write(unsigned long arg)
{
    int i = 0;
    int ret = 0;
    char *pbuffer=NULL;
    camera_i2c_register_t    param;

    dbg_print("Camera_MiscDriver: enter camera_cmd_write\n");
    pbuffer=camera_buffer_ready(arg, &param);
    if (NULL==pbuffer)
    {
        err_print("Camera_MiscDriver: camera_buffer_ready return NULL!\n");
        return (-EFAULT);
    }

#if DEBUG > 0
    for ( i=0; i< param.size; i++)
    {
        dbg_print("Camera_MiscDriver: camera ioctl data[%d]=0x%02x\n", i, 
            pbuffer[i]);
    }
#endif

    dbg_print("Camera_MiscDriver: camera i2c_transfer to write\n");

    i = 0;
    ret = mxc_i2c_raw_write(I2C_BUS, g_slave_addr, param.size, pbuffer);
    while ((ret) && (CAM_MISC_CMDWRITE_RETRY > i))
    {
        err_print("Camera_MiscDriver: mxc_i2c_raw_write Failed: %d! Retry %d!",
                   ret, i);
        /* usleep(5); */
        ret = mxc_i2c_raw_write(I2C_BUS, g_slave_addr, param.size, pbuffer);
        i++;
    }

    if (ret)
    {
        err_print("Camera_MiscDriver: mxc_i2c_raw_write FAILED!\n");
        err_print("Camera_MiscDriver: mxc_i2c_raw_write return %d, size = %d",
                  ret, param.size);

        for(i = 0; i < param.size; i++)
        {
            err_print("Camera_MiscDriver: camera ioctl data[%d]=0x%02x\n", i,
                      pbuffer[i]);
        }

        if ((pbuffer) && (pbuffer != param.data.array))
        {
            kfree(pbuffer);
        }
        return (-EFAULT);
    }

    dbg_print("Camera_MiscDriver: camera i2c_transfer to write ok\n");

    return ret;
}
#undef CAM_MISC_CMDWRITE_RETRY

static int camera_dev_ioctl(struct inode *inode, 
                            struct file *file, 
                            unsigned int cmd, 
                            unsigned long arg)
{
    int ret = 0;

    dbg_print("Camera_MiscDriver: camera ioctl cmd=%d slave_set=%d\n", cmd, g_slave_set);

    switch(cmd)
    {
        case CAMERA_I2C_READ:
            ret=camera_cmd_read(arg);
            break;

        case CAMERA_I2C_WRITE:
            ret=camera_cmd_write(arg);
            break;

        case CAMERA_I2C_SLAVE:
            dbg_print("Camera_MiscDriver: camera_i2c_client->addr=0x%02x\n", 
                g_slave_addr);
            g_slave_addr=arg;
            g_slave_set=1;
            dbg_print("Camera_MiscDriver: camera slave address 0x%02x\n", 
                g_slave_addr);
            break;

        case CAMERA_I2C_PROBE:
            break;

        default:
            break;
    }

    return ret;
}

#include <asm/arch/clock.h>
static int __init camera_misc_init(void)
{
#ifdef CONFIG_MOT_FEAT_PM
    int ret_val = 0;
    ret_val = mxc_pll_request_pll(USBPLL);
    if(ret_val != 0) {
            err_print("Camera_MiscDriver: Unable to request USBPLL\n");
            return ret_val;
    }
#endif

    err_print("Camera_MiscDriver: camera i2c init\n" );

    g_open_count = 0;

    dbg_print("misc_register\n");
    if(misc_register(&cam_misc_device0))
    {
        err_print("Camera_MiscDriver: error in register camera i2c misc device!\n" );
        return -EIO;
    }

    return 0;
}

static void __exit camera_misc_exit(void)
{
#ifdef CONFIG_MOT_FEAT_PM
    int ret_val = 0;
    ret_val = mxc_pll_release_pll(USBPLL);
    if(ret_val != 0) {
            err_print("Camera_MiscDriver: Unable to release USBPLL\n");
            // ignore this error & Continue with the rest of exit
    }
#endif
    misc_deregister( &cam_misc_device0 );
    err_print("Camera_MiscDriver: camera i2c exit\n");
}

module_init(camera_misc_init);
module_exit(camera_misc_exit);

MODULE_AUTHOR("Motorola");
MODULE_DESCRIPTION("camera misc driver");
MODULE_SUPPORTED_DEVICE("camera");
MODULE_LICENSE("GPL");

