/*
 *  camera_misc.h
 *
 *  Camera Misc driver.
 *
 *  Copyright (C) 2006 Motorola Inc.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 Revision History:
 Modification     Tracking
 Author                 Date          Number     Description of Changes
 Motorola               05/15/2006                 Initial Version
 Motorola               12/18/2006                 Remove gpio-related code
----------------   ------------    ----------   -------------------------
*/

#ifndef _CAMERA_MISC_H
#define _CAMERA_MISC_H

#define CAMERA_DATA_ARRAY_LEN       4

typedef struct camera_i2c_register_s {
    unsigned int size;
    union {
        char array[CAMERA_DATA_ARRAY_LEN];
        char *pointer;
    } data;
} camera_i2c_register_t ;

enum {
    CAMERA_I2C_READ     = 100,
    CAMERA_I2C_WRITE    = 101,
    CAMERA_I2C_SLAVE    = 102,
    CAMERA_I2C_PROBE    = 103
};

#define CAMERA_DEVICE0  "/dev/misc/camera0"

#endif /* _CAMERA_MISC_H */

