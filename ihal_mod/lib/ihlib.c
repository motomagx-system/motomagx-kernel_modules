/* IHAL                                                                   <ihlib.c> */
/*                                                                                  */
/* Copyright (c) 2006-2008 Motorola, Inc.                                           */
/*                                                                                  */

/*
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 5-Jun-2006  Motorola    Initial revision.
 * 3-Jul-2007  Motorola    Compliance issue fix
 * 15-Jul-2007 Motorola    Support setting of ADC serial interface bit width
 * 24-Jul-2007 Motorola    Compliance issue fix
 * 15-Oct-2007 Motorola    Free IPU 3M reserved RAM
 * 18-Dec-2007 Motorola    Fix uninitialized variable
 * 28-Feb-2008 Motorola    Added support for generic interrupt handling
 * 10-Mar-2008 Motorola    Add three interface functions
 * 17-Apr-2008  Motorola   Add function to set IC color key/alpha and YUV offset
 * 07-May-2008 Motorola    introduced ipu_disable_channel_no_delay
 * 09-May-2008  Motorola   Add function to set DI signal polarity
 * 15-Jul-2008  Motorola   Added support for reading register for RENESAS LCD pannel 
 * 23-Jul-2008 Motorola    Add function to reset IC_EN
 * 07-AUG-2008 Motorola    Add setup/restore ipu access memory priority function
 * 20-AUG-2008 Motorola    Add support for ESD issue 
 * 04-SEP-2008 Motorola    Add support for CSI position and size
 * 14-NOV-2008 Motorola    Add support for querying the status of flip 
 */

/*
 * @file ihlib.c
 *
 * @General Description: IHAL user space library which mimics the IPU kernel library 
 * provided by FSL. This library makes available the IHAL API (ih.h) and exclusively 
 * communicates to a kernel device driver (used in a helper role) named "IHAL". By 
 * interfacing to and invoking functions contained in this library, a client can 
 * command and configure the IPU at a low level.

 * The basic idea of implementation is to instruct the kernel driver (via ioctl) to 
 * carry out a specific function. For each IHAL function, there is a corresponding 
 * ioctl value that is given to the driver for processing.
 */

/*!
 * Include files
 */

#include <stdio.h>
#define __USE_UNIX98 // to access semaphore API
#include <pthread.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <ih.h>
#include <ih_types.h>
#include <ihal_ioctl.h>

#define IHAL_DEV(id) (dev_info_ihal[id].dev_hndl)
#define IHAL_DEV_ID_0 0

#define IHAL_FALSE 0
#define IHAL_TRUE  1

static struct
{
    const char * dev_name;
    int          dev_hndl;
}   dev_info_ihal[] = 
    {
        {"/dev/ihal0",-1} // IHAL_DEV_ID_0
    };

static ipu_channel_fmt_t sdc_pnl_chan_fmt =
{
    .width  = 0, 
    .height = 0, 
    .pixel_fmt = 0
};

static pthread_mutex_t ipu_mutex = PTHREAD_MUTEX_INITIALIZER;
static IH_BOOL crit_sec_init = IH_FALSE;
static IH_U32 init_cnt = 0;

/*====================================================================================

FUNCTION: IHAL_enter_crit_sec

DESCRIPTION: 
    Serializes code execution.

ARGUMENTS:
    None.

RETURN VALUE:
    None.

PRE-CONDITIONS:
    None.

POST-CONDITIONS:
    None.

IMPORTANT NOTES:
    None.

====================================================================================*/

static void
IHAL_enter_crit_sec
(
    void
)
{
    if(IH_FALSE == crit_sec_init)
    {
        pthread_mutexattr_t oAttr;

        if(!pthread_mutexattr_init(&oAttr)
           &&!pthread_mutexattr_settype(&oAttr, PTHREAD_MUTEX_RECURSIVE)
           &&!pthread_mutex_init(&ipu_mutex, &oAttr))
        {
            crit_sec_init = IH_TRUE;
        }
        else
        {
            return;
        }
    }

    pthread_mutex_lock(&ipu_mutex);
}

/*====================================================================================

FUNCTION: IHAL_exit_crit_sec

DESCRIPTION: 
    Ends the serialization of code execution.

ARGUMENTS:
    None.

RETURN VALUE:
    None.

PRE-CONDITIONS:
    None.

POST-CONDITIONS:
    None.

IMPORTANT NOTES:
    None.

====================================================================================*/

static void
IHAL_exit_crit_sec
(
    void
)
{
    if(IH_FALSE == crit_sec_init)
    {
        return;
    }

    pthread_mutex_unlock(&ipu_mutex);
}

/*==================================================================================*/

static IH_S32 
check_dev_open
(
    int dev, 
    IH_BOOL implicit_open
)
{
    if(IHAL_DEV(dev) < 0 && implicit_open)
    {
        IHAL_DEV(dev) = open (dev_info_ihal[dev].dev_name, O_RDWR);
        if(IHAL_DEV(dev) < 0) 
        {
            printf("open failed for %s\n", dev_info_ihal[dev].dev_name);
        }
        else
        {
            if(IHAL_VERSION != 0xFFFFFFFF)
            {
                int vers;

                vers = ioctl (IHAL_DEV(dev), IHAL_IOC_GET_VERSION, 0);

                if(0xFFFFFFFF != vers && IHAL_VERSION != vers)
                {
                    IHAL_DEV(dev) = -1;
                    
                    goto leaveOpen;
                }
            }    
        }
    }

leaveOpen:
    return IHAL_DEV(dev);
}

/************************************************************************************/

static void 
dev_close
(
    int dev
)
{
    if(IHAL_DEV(dev) > 0)
    {
        close (IHAL_DEV(dev));
        IHAL_DEV(dev) = -1;
    }
}

/*============================== Channel and Buffer ================================*/

IH_S32 
ipu_init_channel
(
    ipu_channel_t channel, 
    ipu_channel_params_t * params
)
{
    IH_S32 rc;
	struct ih_init_chan_info chan_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    chan_info.channel = channel;
    chan_info.params  = params;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_INIT_CHAN, &chan_info);
}

/************************************************************************************/

IH_S32 
ipu_is_init_channel
(
    ipu_channel_t channel
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_IS_INIT_CHAN, channel);
}

/************************************************************************************/

IH_S32
ipu_uninit_channel
(
    ipu_channel_t channel
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_UNINIT_CHAN, channel);
}

/************************************************************************************/

IH_S32 
ipu_init_channel_buffer
(
    ipu_channel_t channel, 
    ipu_buffer_t type,
    ipu_channel_fmt_t * chan_fmt, 
    IH_U32 stride,
    void * phyaddr_0, 
    void * phyaddr_1
)
{
    IH_S32 rc;
	struct ih_init_chan_buff_info chan_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    chan_info.channel   = channel;
    chan_info.type      = type;
    chan_info.chan_fmt  = *chan_fmt;
    chan_info.stride    = stride;
    chan_info.phyaddr_0 = phyaddr_0; 
    chan_info.phyaddr_1 = phyaddr_1;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_INIT_CHAN_BUF, &chan_info);
}

/************************************************************************************/

IH_S32 
ipu_enable_channel
(
    ipu_channel_t channel
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ENABLE_CHAN, channel);
}

/************************************************************************************/

IH_S32 
ipu_disable_channel
(
    ipu_channel_t channel
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_DISABLE_CHAN, channel);
}

/************************************************************************************/

IH_S32 
ipu_disable_channel_no_delay
(
    ipu_channel_t channel
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_DISABLE_CHAN_NO_DELAY, channel);
}

/************************************************************************************/

IH_S32 
ipu_select_buffer
(
    ipu_channel_t channel,
    ipu_buffer_t type,
    IH_U32 bufNum
)
{
    IH_S32 rc;
    struct ih_sel_buff_info sel_buff_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    sel_buff_info.channel   = channel;
    sel_buff_info.type      = IPU_BUF_TYPE(type);
    sel_buff_info.bufNum    = bufNum;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SELECT_BUF, &sel_buff_info);
}

/************************************************************************************/

IH_S32 
ipu_update_channel_buffer
(
    ipu_channel_t channel, 
    ipu_buffer_t type,
    IH_U32 bufNum, 
    void * phyaddr
)
{
    IH_S32 rc;
	struct ih_upd_chan_buff_info chan_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    chan_info.channel   = channel;
    chan_info.type      = IPU_BUF_TYPE(type);
    chan_info.bufNum    = bufNum;
    chan_info.phyaddr   = phyaddr; 

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_UPDATE_CHAN_BUF, &chan_info);
}

/************************************************************************************/

IH_S32
ipu_update_channel_yuvoffset_buffer
(
    ipu_channel_t channel,
    ipu_buffer_t type,
    IH_U16 u_offset,
    IH_U16 v_offset
)
{
    IH_S32 rc;
    struct ih_upd_chan_yuvoffset_buff_info chan_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    chan_info.channel   = channel;
    chan_info.type      = IPU_BUF_TYPE(type);
    chan_info.u_offset  = u_offset;
    chan_info.v_offset  = v_offset;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_UPDATE_YUV_CHAN_BUF, &chan_info);
}


/************************************************************************************/

IH_S32 
ipu_link_channels
(
    ipu_channel_t src_ch, 
    ipu_channel_t dest_ch
)
{
    IH_S32 rc;
    struct ih_link_channels_info link_chans;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    link_chans.src_ch   = src_ch;
    link_chans.dest_ch  = dest_ch;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_LINK_CHAN, &link_chans);
}

/************************************************************************************/

IH_S32 
ipu_unlink_channels
(
    ipu_channel_t src_ch, 
    ipu_channel_t dest_ch
)
{
    IH_S32 rc;
    struct ih_link_channels_info link_chans;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    link_chans.src_ch   = src_ch;
    link_chans.dest_ch  = dest_ch;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_UNLINK_CHAN, &link_chans);
}

/************************************************************************************/

IH_S32 
ipu_is_channel_busy
(
    ipu_channel_t channel
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_IS_CHAN_BUSY, channel);
}

/************************************************************************************/

IH_BOOL
ipu_is_buf_ready
(
    ipu_channel_t channel,
    ipu_buffer_t buf_type,
    IH_U32 buf_num
)
{
    IH_S32 rc;
    struct ih_buf_ready_info buf_ready_info;

    buf_ready_info.chan = channel;
    buf_ready_info.buf_type = buf_type;
    buf_ready_info.buf_num = buf_num;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_IS_BUF_READY, &buf_ready_info);
    if(rc != 0)
    {
        return IHAL_TRUE;
    }

    return IHAL_FALSE;
}

/*======================================= SDC ======================================*/

IH_S32 
ipu_sdc_init_panel
(
    ipu_panel_t panel, 
    IH_U8 refreshRateHz,
    IH_U16 width, 
    IH_U16 height,
    IH_U32 pixel_fmt, 
    IH_U16 hStartWidth, 
    IH_U16 hSyncWidth, 
    IH_U16 hEndWidth, 
    IH_U16 vStartWidth, 
    IH_U16 vSyncWidth, 
    IH_U16 vEndWidth,
    ipu_di_signal_cfg_t sig, 
    IH_U32 num_bus_cycles
)
{
    IH_S32 rc;
    struct ih_sdc_init_panel_info sdc_init_pnl_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    sdc_init_pnl_info.panel             = panel;
    sdc_init_pnl_info.refreshRateHz     = refreshRateHz;
    sdc_init_pnl_info.width             = width;
    sdc_init_pnl_info.height            = height;
    sdc_init_pnl_info.pixel_fmt         = pixel_fmt;
    sdc_init_pnl_info.hStartWidth       = hStartWidth;
    sdc_init_pnl_info.hSyncWidth        = hSyncWidth;
    sdc_init_pnl_info.hEndWidth         = hEndWidth;
    sdc_init_pnl_info.vStartWidth       = vStartWidth;
    sdc_init_pnl_info.vSyncWidth        = vSyncWidth;
    sdc_init_pnl_info.vEndWidth         = vEndWidth;
    sdc_init_pnl_info.sig               = sig;
    sdc_init_pnl_info.num_bus_cycles    = num_bus_cycles;

    /*
      Save the panel's config for a possible retrieval later.
    */
    sdc_pnl_chan_fmt.width  = width;
    sdc_pnl_chan_fmt.height = height;
    sdc_pnl_chan_fmt.pixel_fmt = pixel_fmt;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_INIT_PANEL, &sdc_init_pnl_info);
}

/************************************************************************************/

IH_S32 
ipu_sdc_set_window_pos
(
    ipu_channel_t channel, 
    IH_S16 x_pos, 
    IH_S16 y_pos
)
{
    IH_S32 rc;
    struct ih_sdc_set_window_pos_info set_wdw_pos_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    set_wdw_pos_info.channel = channel;
    set_wdw_pos_info.x_pos   = x_pos;
    set_wdw_pos_info.y_pos   = y_pos;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_SET_WINDOW_POS, &set_wdw_pos_info);
}

/************************************************************************************/

IH_S32 
ipu_sdc_set_global_alpha
(
    IH_BOOL enable, 
    IH_U8 alpha
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_SET_GLOBAL_ALPHA, 
                 (enable << 31) | alpha);
}

/************************************************************************************/

IH_S32 
ipu_sdc_set_color_key
(
    ipu_channel_t channel, 
    IH_BOOL enable, 
    IH_U32 colorKey
)
{
    IH_S32 rc;
    struct ih_sdc_set_color_key_info ck_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    ck_info.channel    = channel;
    ck_info.enable     = enable;
    ck_info.colorKey   = colorKey;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_SET_COLOR_KEY, &ck_info);
}

/************************************************************************************/

IH_S32
ipu_ic_set_global_alpha
(
    IH_BOOL enable,
    IH_U8 alpha
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_IC_SET_GLOBAL_ALPHA,
                 (enable << 31) | alpha);
}

/************************************************************************************/

IH_S32
ipu_ic_set_color_key
(
    ipu_channel_t channel,
    IH_BOOL enable,
    IH_U32 colorKey
)
{
    IH_S32 rc;
    struct ih_sdc_set_color_key_info ck_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    ck_info.channel    = channel;
    ck_info.enable     = enable;
    ck_info.colorKey   = colorKey;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_IC_SET_COLOR_KEY, &ck_info);
}

/************************************************************************************/

IH_S32 
ipu_sdc_set_graphics_window
(
    ipu_channel_t channel
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_SET_GRAPHIC_WINDOW, channel);
}

/************************************************************************************/

IH_S32 
ipu_sdc_enable_clk
(
    IH_BOOL enable
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_ENABLE_CLK, enable);
}

/************************************************************************************/

IH_S32 ipu_sdc_get_panel_fmt
(
    ipu_channel_fmt_t * chan_fmt
)
{
    /*
      Simply assign what we saved earlier in init_panel.
    */
    *chan_fmt = sdc_pnl_chan_fmt;

    return 0;
}

IH_S32 ipu_sdc_set_di_signals(ipu_di_signal_cfg_t sig)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_SET_DI_SIGNALS, &sig);

}

IH_S32 ipu_sdc_get_di_signals(ipu_di_signal_cfg_t * p_sig)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SDC_GET_DI_SIGNALS, p_sig);
}

/*====================================== ADC ======================================*/

IH_S32 ipu_adc_init_panel
(
    display_port_t disp, 
    IH_U16 width, 
    IH_U16 height,
    IH_U32 pixel_fmt,
    IH_U32 stride,
    ipu_adc_sig_cfg_t sig,
    display_addressing_t addr,
    IH_U32 vsync_width, 
    vsync_t mode
)
{
    IH_S32 rc;
    struct ih_adc_init_panel_info init_pnl_info;

    init_pnl_info.disp = disp;
    init_pnl_info.width = width; 
    init_pnl_info.height = height;
    init_pnl_info.pixel_fmt = pixel_fmt;
    init_pnl_info.stride = stride;
    init_pnl_info.sig = sig;
    init_pnl_info.addr = addr;
    init_pnl_info.vsync_width = vsync_width;
    init_pnl_info.mode = mode;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_INIT_PANEL, &init_pnl_info);
}

/************************************************************************************/

IH_S32 ipu_adc_init_ifc_timing
(
    display_port_t disp, 
    IH_BOOL read, 
    IH_U32 cycle_time,
    IH_U32 up_time,
    IH_U32 down_time,
    IH_U32 read_latch_time,
    IH_U32 pixel_clk
)
{
    IH_S32 rc;
    struct ih_adc_init_ifc_timing_info init_ifc_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    init_ifc_info.disp = disp;
    init_ifc_info.read = read;
    init_ifc_info.cycle_time = cycle_time;
    init_ifc_info.up_time = up_time;
    init_ifc_info.down_time = down_time;
    init_ifc_info.read_latch_time = read_latch_time;
    init_ifc_info.pixel_clk = pixel_clk;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_INIT_IFC_TIMING, &init_ifc_info);
}

/************************************************************************************/
IH_S32 setup_ipu_access_memory_priority(void)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_SETUP_IPU_ACCESS_MEMORY_PRIORITY, 0);

}

/************************************************************************************/
IH_S32 restore_ipu_access_memory_priority(void)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_RESTORE_IPU_ACCESS_MEMORY_PRIORITY, 0);

}

/************************************************************************************/

IH_S32 ipu_adc_write_template
(
    display_port_t disp, 
    IH_U32 * pCmd, 
    IH_BOOL write
)
{
    IH_S32 rc;
    struct ih_adc_write_template_info adc_write;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    adc_write.disp = disp;
    adc_write.pCmd = pCmd;
    adc_write.write = write;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_WRITE_TEMPLATE, &adc_write);
}

/************************************************************************************/

IH_S32 ipu_adc_set_update_mode
(
    ipu_channel_t channel, 
    ipu_adc_update_mode_t mode,
    IH_U32 refresh_rate, 
    IH_U32 addr, 
    IH_U32 * size
)
{
    IH_S32 rc;
    struct ih_adc_set_update_mode_info adc_upd_mode;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    adc_upd_mode.channel = channel;
    adc_upd_mode.mode = mode;
    adc_upd_mode.refresh_rate = refresh_rate; 
    adc_upd_mode.addr = addr;
    adc_upd_mode.size = size;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_SET_UPDATE_MODE, &adc_upd_mode);
}

/************************************************************************************/

IH_S32 ipu_adc_write_cmd
(
    display_port_t disp, 
    cmddata_t type,
    IH_U32 cmd, 
    const IH_U32 * params, 
    IH_U16 numParams
)
{
    IH_S32 rc;
    struct ih_adc_write_cmd_info adc_write;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    adc_write.disp = disp;
    adc_write.type = type;
    adc_write.cmd = cmd;
    adc_write.params = params;
    adc_write.num_params = numParams;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_WRITE_CMD, &adc_write);
}

/************************************************************************************/

IH_S32 ipu_adc_set_ifc_width
(
    display_port_t disp, 
    IH_U32 pixel_fmt,
    IH_U16 width
)
{
    IH_S32 rc;
    struct ih_adc_set_ifc_width_info adc_set_ifc_width;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    adc_set_ifc_width.disp = disp;
    adc_set_ifc_width.pixel_fmt = pixel_fmt;
    adc_set_ifc_width.ifc_width = width;
    
    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_IFC_WIDTH_CMD, &adc_set_ifc_width);
}

/************************************************************************************/
IH_U32 ipu_adc_read_cmd
(
    display_port_t disp, 
    IH_U32 cmd,
    IH_U8*  ibuf,
    IH_U16  ilen
)
{
    IH_S32 rc;
    struct ih_adc_read_cmd_info cmd_info;
    
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }
    cmd_info.disp = disp;
    cmd_info.cmd  = cmd;
    cmd_info.ibuf = ibuf;
    cmd_info.ilen = ilen;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_READ_CMD, &cmd_info);
}

/************************************************************************************/
IH_S32 ipu_adc_esd_ops
(
    ipu_esd_ops_t op  
)
{
    IH_S32 rc, esd;
   
    esd = (IH_S32)(op); 
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_ESD_OPS, &esd);
}

/************************************************************************************/
IH_S32 ipu_adc_flip_status
(
)
{
    IH_S32 rc;
   
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ADC_FLIP_STATUS, 0);
}

/*======================================= DI =======================================*/

void ipu_di_set_pixel_fmt
(
    display_port_t disp, 
    IH_U32 pixel_fmt
)
{
}

/*====================================== CSI =======================================*/

IH_S32 ipu_csi_init_interface
(
    ipu_channel_fmt_t * chan_fmt,
    ipu_csi_signal_cfg_t sig
)
{
    IH_S32 rc;
    struct ih_csi_init_if_info csi_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    csi_info.chan_fmt  = *chan_fmt;
    csi_info.sig       = sig;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_INIT_INTERFACE, &csi_info);
}

/************************************************************************************/

IH_S32 ipu_csi_enable_mclk
(
    IH_BOOL flag
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_ENABLE_MCLK, flag);
}

/************************************************************************************/
IH_S32 ipu_csi_set_mclk
(
    IH_U32 *p_mclk_freq
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_SET_MCLK_RATE, p_mclk_freq);
}
/************************************************************************************/

IH_S32 ipu_csi_get_channel_fmt
(
    ipu_channel_fmt_t * chan_fmt
)
{
    return 0;
}

/************************************************************************************/

IH_S32 ipu_csi_set_channel_fmt
(
    ipu_channel_fmt_t * chan_fmt
)
{
    return 0;
}

/************************************************************************************/

IH_S32 ipu_csi_set_sensor_size
(
    IH_S16 width, 
    IH_S16 height
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_SET_SENSOR_SIZE, 
                  (((height-1) << 16) | (width-1)));
}

/************************************************************************************/

IH_S32 ipu_csi_set_actual_size
(
    IH_S16 width, 
    IH_S16 height
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_SET_ACTUAL_SIZE, 
                  (((height-1) << 16) | (width-1)));
}

IH_S32 ipu_csi_set_window_pos
(
    IH_U16 x_pos,
    IH_U16 y_pos
)
{
    IH_S32 rc;
    struct ih_csi_set_window_pos_info set_wdw_pos_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    set_wdw_pos_info.x_pos = x_pos;
    set_wdw_pos_info.y_pos = y_pos;

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_SET_WINDOW_POS, &set_wdw_pos_info);
    return rc;
}

IH_S32 ipu_csi_set_window_size
(
    IH_U16 width,
    IH_U16 height
)
{
    IH_S32 rc;
    struct ih_csi_set_window_size_info set_wdw_size_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    set_wdw_size_info.width = width;
    set_wdw_size_info.height = height;

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CSI_SET_WINDOW_SIZE, &set_wdw_size_info);
    return rc;
}

/*======================================WRITE REG ============================================*/
IH_S32 ipu_write_reg
(
    IH_U32 reg, 
    IH_U32 val 
)
{
    IH_S32 rc;
    struct ih_reg_rw reg_w;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    reg_w.reg = reg;
    reg_w.val = val;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_WRITE_IPU_REG, &reg_w);
}

IH_S32 ipu_read_reg
(
    IH_U32 reg, 
    IH_U32 * val 
)
{
    IH_S32 rc;
    struct ih_reg_rw reg_r;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    reg_r.reg = reg;
    reg_r.val = 0;

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_READ_IPU_REG, &reg_r);
    *val = reg_r.val;
    return rc;
}

#if defined(DUMP_IPU_REGISTER)
IH_S32 ipu_write_register
(
    IH_U32 reg,
    IH_U32 val
)
{
    IH_S32 rc;
    struct ih_reg_rw reg_r;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    reg_r.reg = reg;
    reg_r.val = val;

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_WRITE_IPU_REGISTER, &reg_r);
    return rc;
}

IH_S32 ipu_read_register
(
    IH_U32 reg,
    IH_U32 * val
)
{
    IH_S32 rc;
    struct ih_reg_rw reg_r;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    reg_r.reg = reg;

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_READ_IPU_REGISTER, &reg_r);
    *val = reg_r.val;
    return rc;
}

IH_S32 ipu_dump_registers
(
    IH_U32 reg_number,
    const IH_U32 *array
)
{
    IH_S32 rc;
    struct ih_register_dump reg_dump;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    reg_dump.number = reg_number;
    reg_dump.array = array;

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_DUMP_IPU_REGISTER, &reg_dump);

    return 0;

}
#endif // DUMP_IPU_REGISTER

IH_S32 ipu_reset_icen_atomic()
{

    IH_S32 rc = 0;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_RESET_ICEN_ATOMIC, NULL);

    return rc;


}
/*======================================= IRQ ======================================*/

IH_S32 
ipu_register_int_handler
(
    ipu_irq_line_t irq_line, 
    ipu_irq_hdlr_t irq_hdlr
)
{
    IH_S32 rc;
    struct ih_irq_reg_info irq_reg_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    irq_reg_info.irq_line   = irq_line;
    irq_reg_info.irq_hdlr   = irq_hdlr;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_REG_INT_HDLR, &irq_reg_info);
}

/************************************************************************************/

IH_S32 
ipu_enable_int
(
    ipu_irq_line_t irq_line
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }
    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ENABLE_IRQ, irq_line);
}

/************************************************************************************/

IH_S32 
ipu_disable_int
(
    ipu_irq_line_t irq_line
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_DISABLE_IRQ, irq_line);
}

/************************************************************************************/

IH_S32 
ipu_wait_for_int_event
(
    ipu_irq_hdlr_t irq_hdlr
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }
    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_WAIT_FOR_IRQ, irq_hdlr);
}

/************************************************************************************/

IH_S32 
ipu_cam_wait_for_int_event
(
    int * pirq,
    IH_U32 timeout
)
{
    int ret;
    struct ih_irq_info irq_info;

    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    irq_info.timeout = timeout;
    ret = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CAM_WAIT_FOR_IRQ, &irq_info);

    *pirq = irq_info.line;

    rc = IPU_CAM_WAIT_INT_FAULT;
    if(0 == ret)
        rc = IPU_CAM_WAIT_INT_OCCUR;
    else if(ret < 0)
    {
        switch(errno)
        {
            case ETIME:
                rc = IPU_CAM_WAIT_INT_TIMEOUT;
            break;
            case ECANCELED:
                rc = IPU_CAM_WAIT_INT_CANCELED;
            break;
            case EFAULT:
                rc = IPU_CAM_WAIT_INT_FAULT;
            break;
            case EOVERFLOW:
                rc = IPU_CAM_WAIT_INT_OVERFLOW;
            break;
            default:
            break;
        }
    }
    return rc;
}

/************************************************************************************/

IH_S32 ipu_cam_cancel_wait_for_int_event //cancel ipu_cam_wait_for_int_event
(
    void
) 
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CAM_WAKEUP_WAIT_IRQ, 0);
}

/************************************************************************************/

IH_S32 
ipu_cam_request_irq
(
    ipu_irq_line_t irq_line
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CAM_REQUEST_IRQ, irq_line);

}

/************************************************************************************/

IH_S32 
ipu_cam_free_irq
(
    ipu_irq_line_t irq_line
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_CAM_FREE_IRQ, irq_line);
}

/************************************************************************************/

IH_S32 
ipu_wait_for_int
(
    ipu_irq_line_t irq_line,
    IH_U32 timeout
)
{
    IH_S32 rc;
    struct ih_irq_info irq_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    irq_info.timeout = timeout;
    irq_info.line = irq_line;
    return( ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_WAIT_FOR_IPU_IRQ, &irq_info));
}

/************************************************************************************/

IH_S32
ipu_request_irq
(
    ipu_irq_line_t irq_line
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_REQUEST_IRQ, irq_line);

}

/************************************************************************************/

IH_S32
ipu_free_irq
(
    ipu_irq_line_t irq_line
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_FREE_IRQ, irq_line);
}

/************************************************************************************/

IH_BOOL 
ipu_is_int_set
(
    ipu_irq_line_t irq_line,
    IH_BOOL clear_if_set 
)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_GET_IRQ_STS, 
                  (clear_if_set << 31) | irq_line);
}

#define IHAL_CAPTURE_REUSE_MEM
#ifdef IHAL_CAPTURE_REUSE_MEM

/*====================================still capture reuse  Memory ======================================*/

static  struct ih_buf_info test_BufInfo;
/* PAGE_SHIFT determines the page size */
#define PAGE_SIZE		4096
#define PAGE_MASK		(~(PAGE_SIZE-1))

void * ihal_test_malloc
(
    IH_U32 size
)
{
    IH_S32 rc;
    IH_U32 virt_addr;    
    struct ih_buf_info BufInfo;
     void * tmp_malloc;   
    char * ch;
  
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        printf(" ihal_test_malloc error!!! \n");
        return NULL;
    }
    
    tmp_malloc = malloc(size + PAGE_SIZE);

    test_BufInfo.size = size + PAGE_SIZE;
    test_BufInfo.virt_addr = (IH_U32)tmp_malloc; 

    virt_addr = ((IH_U32)tmp_malloc + PAGE_SIZE - 1)& PAGE_MASK;

    BufInfo.size = size;
    BufInfo.virt_addr = (IH_U32)virt_addr;

    // enter crit sec
    IHAL_enter_crit_sec();
    ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_TEST_ALLOC, &BufInfo);
    // exit crit sec
    IHAL_exit_crit_sec();
    return (void *)virt_addr;
}

void ihal_test_free
(
    void * mem
)
{
    IH_S32 rc;
    struct ih_buf_info BufInfo;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        printf(" ihal_test_free error!!! \n");
        return;
    }

    // enter crit sec
    IHAL_enter_crit_sec();
    ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_TEST_FREE, &BufInfo);
    // exit crit sec
    IHAL_exit_crit_sec();

    free((void *)test_BufInfo.virt_addr); 
    test_BufInfo.virt_addr = 0;
    test_BufInfo.size = 0;
    

    return;
}
#endif

/*==================================== Memory ======================================*/

IH_S32 
ipu_alloc_channel_buffer
(
    struct ih_buf_info * pBufInfo
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    // enter crit sec
    IHAL_enter_crit_sec();
    pBufInfo->virt_addr = (IH_U32)mmap (0, pBufInfo->size, PROT_READ | PROT_WRITE, 
                                        MAP_SHARED, IHAL_DEV(IHAL_DEV_ID_0), 0);
    
    if((IH_U32)MAP_FAILED == pBufInfo->virt_addr)
    {
        IHAL_exit_crit_sec();
        return -1;
    }

    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ALLOC_BUFFER, pBufInfo);
    // exit crit sec
    IHAL_exit_crit_sec();

    return rc;
}

/************************************************************************************/

IH_S32 
ipu_free_channel_buffer
(
    struct ih_buf_info * pBufInfo
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    // enter crit sec
    IHAL_enter_crit_sec();
    munmap ((void *)pBufInfo->virt_addr, pBufInfo->size);
    
    rc = ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_FREE_BUFFER, pBufInfo);
    // exit crit sec
    IHAL_exit_crit_sec();

    return rc;
}

/************************************************************************************/

IH_U32 
ipu_alloc_mem
(
    IH_U32 size
)
{
    struct ih_buf_info buf_info;

    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    buf_info.virt_addr = 0; 
    buf_info.phys_addr = 0; 
    buf_info.size = size;
    ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_ALLOC_MEM, &buf_info);
    
    return buf_info.phys_addr;
}

/************************************************************************************/

void 
ipu_free_mem
(
    IH_U32 mem_phys_addr,
    IH_U32 size
)
{
    struct ih_buf_info buf_info;

    if((check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return ;
    }

    buf_info.virt_addr = 0; 
    buf_info.phys_addr = mem_phys_addr; 
    buf_info.size = size;
    ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_FREE_MEM, &buf_info);
}

/************************************************************************************/

IH_U32 
ipu_map_mem
(
    IH_U32 mem_phys_addr, 
    IH_U32 size
)
{
    struct ih_buf_info buf_info;

    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    buf_info.virt_addr = 0; 
    buf_info.phys_addr = mem_phys_addr; 
    buf_info.size = size;
    ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_MAP_MEM, &buf_info);

    return (IH_U32)mmap (0, size, PROT_READ | PROT_WRITE, MAP_SHARED, 
                         IHAL_DEV(IHAL_DEV_ID_0), 0);
}

/************************************************************************************/

void 
ipu_unmap_mem
(
    IH_U32 mem_virt_addr, 
    IH_U32 size
)
{
    munmap ((void *)mem_virt_addr, size);
}

/************************************************************************************/

IH_S32 ipu_generic_irq_request(ipu_irq_line_t line, IH_BOOL disable_irq)
{
    struct ih_generic_request_irq_info irq_info;

    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    irq_info.line = line;
    irq_info.disable_irq = disable_irq;
    return( ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_GENERIC_REQUEST_IRQ, &irq_info));
}

IH_S32 ipu_generic_irq_free(ipu_irq_line_t line)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_GENERIC_FREE_IRQ, line);
}

IH_S32 ipu_generic_irq_wait(ipu_irq_line_t line, IH_U32 timeout_ms, IH_U32 timewait_after_trigger_us, IH_U32 min_num_int_b4_trigger)
{
    struct ih_generic_wait_irq_info irq_wait;

    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    irq_wait.line                      = line;
    irq_wait.timeout_ms                = timeout_ms;
    irq_wait.timewait_after_trigger_us = timewait_after_trigger_us;
    irq_wait.min_num_int_b4_trigger    = min_num_int_b4_trigger;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_GENERIC_WAIT_FOR_IRQ, &irq_wait);
}

IH_S32 ipu_generic_irq_enable(ipu_irq_line_t line)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_GENERIC_ENABLE_IRQ, line);
}

IH_S32 ipu_generic_irq_disable(ipu_irq_line_t line)
{
    IH_S32 rc;
    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_GENERIC_DISABLE_IRQ, line);
}

/************************************************************************************/

void 
ipu_init
(
    void
)
{
	check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE);
    init_cnt++;
}

/************************************************************************************/

void 
ipu_uninit
(
    void
)
{
    if(0 == init_cnt)
    {
        return;
    }

    init_cnt--;

    if(0 == init_cnt)
    {
        dev_close(IHAL_DEV_ID_0);
    }
}

/************************************************************************************/

IH_S32 
ipu_test_unselect_buffer
(
    ipu_channel_t channel,
    ipu_buffer_t type,
    IH_U32 bufNum
)
{
    IH_S32 rc;
    struct ih_sel_buff_info sel_buff_info;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    sel_buff_info.channel   = channel;
    sel_buff_info.type      = IPU_BUF_TYPE(type);
    sel_buff_info.bufNum    = bufNum;

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_TEST_UNSELECT_BUF, &sel_buff_info);
}


/************************************************************************************/

IH_S32 
ipu_test_adc_enable
(
    IH_BOOL enable
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_TEST_ADC_ENABLE, enable);
}
/************************************************************************************/

IH_S32 
ipu_test_di_enable
(
    IH_BOOL enable
)
{
    IH_S32 rc;

    if((rc = check_dev_open(IHAL_DEV_ID_0, IHAL_TRUE)) < 1)
    {
        return rc;
    }

    return ioctl (IHAL_DEV(IHAL_DEV_ID_0), IHAL_IOC_TEST_DI_ENABLE, enable);
}
/************************************************************************************/
