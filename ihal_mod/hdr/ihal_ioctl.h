/* IHAL                                                              <ihal_ioctl.h> */
/*                                                                                  */
/* Copyright (c) 2006-2008 Motorola, Inc.                                           */
/*                                                                                  */

/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 5-Jun-2006  Motorola    Initial revision.
 * 3-Jul-2007  Motorola    Compliance issue fix
 * 15-Jul-2007 Motorola    Support setting of ADC serial interface bit width
 * 24-Jul-2007 Motorola    Compliance issue fix
 * 15-Oct_2007 Motorola    Free IPU 3M reserved RAM
 * 28-Feb-2008  Motorola    Added support for generic interrupt handling
 * 10-Mar-2008 Motorola    Add three interface functions
 * 17-Apr-2008  Motorola   Add function to set IC color key/alpha and update YUV offset
 * 07-May-2008 Motorola    introduced IHAL_IOC_DISABLE_CHAN_NO_DELAY
 * 09-May-2008  Motorola   Add function to set DI signal polarity
 * 15-Jul-2008  Motorola   Added support for reading register for RENESAS LCD pannel
 * 23-Jul-2008 Motorola    Add function to reset IC_EN
 * 07-AUG-2008 Motorola    Add setup/restore ipu access memory priority function
 * 20-AUG-2008 Motorola    Add support for ESD issue 
 * 04-SEP-2008 Motorola    Add support for CSI position and size
 * 14-NOV-2008 Motorola    Add support for querying the status of flip 
 */

/*!
 * @defgroup IPU MXC Image Processing Unit (IPU) Helper Driver
 */
/*!
 * @file ihal_ioctl.h
 * 
 * @brief Internal structures and definitions exchanged between IHAL library and driver.
 * 
 * @ingroup IPU
 */

#ifndef _IHAL_IOCTL_H
#define _IHAL_IOCTL_H

#include <linux/ioctl.h>
#include <ih_types.h>

/*
  Define the IOCTL values passed as an argument to the driver from the library. Try to
  use an unlikely IOCTL number (referenced from Documentation/ioctl-number.txt).
*/
#define IHAL_IOC_NUMBER                  0xBB

#define IHAL_IOC_INIT_CHAN              _IO(IHAL_IOC_NUMBER, 0)
#define IHAL_IOC_IS_INIT_CHAN           _IO(IHAL_IOC_NUMBER, 1)
#define IHAL_IOC_UNINIT_CHAN            _IO(IHAL_IOC_NUMBER, 2)
#define IHAL_IOC_INIT_CHAN_BUF          _IO(IHAL_IOC_NUMBER, 3)
#define IHAL_IOC_ENABLE_CHAN            _IO(IHAL_IOC_NUMBER, 4)
#define IHAL_IOC_DISABLE_CHAN           _IO(IHAL_IOC_NUMBER, 5)
#define IHAL_IOC_SELECT_BUF             _IO(IHAL_IOC_NUMBER, 6)
#define IHAL_IOC_UPDATE_CHAN_BUF        _IO(IHAL_IOC_NUMBER, 7)
#define IHAL_IOC_LINK_CHAN              _IO(IHAL_IOC_NUMBER, 8)
#define IHAL_IOC_UNLINK_CHAN            _IO(IHAL_IOC_NUMBER, 9)
#define IHAL_IOC_IS_CHAN_BUSY           _IO(IHAL_IOC_NUMBER, 10)
#define IHAL_IOC_IS_BUF_READY           _IO(IHAL_IOC_NUMBER, 11)
#define IHAL_IOC_DISABLE_CHAN_NO_DELAY  _IO(IHAL_IOC_NUMBER, 13)
#define IHAL_IOC_UPDATE_YUV_CHAN_BUF    _IO(IHAL_IOC_NUMBER, 12)


#define IHAL_IOC_WRITE_IPU_REG		_IO(IHAL_IOC_NUMBER, 15)
#define IHAL_IOC_READ_IPU_REG		_IO(IHAL_IOC_NUMBER, 16)

#if defined(DUMP_IPU_REGISTER)
#define IHAL_IOC_WRITE_IPU_REGISTER  _IO(IHAL_IOC_NUMBER, 17)
#define IHAL_IOC_READ_IPU_REGISTER   _IO(IHAL_IOC_NUMBER, 18)
#define IHAL_IOC_DUMP_IPU_REGISTER   _IO(IHAL_IOC_NUMBER, 19)
#endif

#define IHAL_IOC_SDC_INIT_PANEL         _IO(IHAL_IOC_NUMBER, 20)
#define IHAL_IOC_SDC_SET_WINDOW_POS     _IO(IHAL_IOC_NUMBER, 21)
#define IHAL_IOC_SDC_SET_GLOBAL_ALPHA   _IO(IHAL_IOC_NUMBER, 22)
#define IHAL_IOC_SDC_SET_COLOR_KEY      _IO(IHAL_IOC_NUMBER, 23)
#define IHAL_IOC_SDC_SET_GRAPHIC_WINDOW _IO(IHAL_IOC_NUMBER, 24)
#define IHAL_IOC_SDC_ENABLE_CLK         _IO(IHAL_IOC_NUMBER, 25)
#define IHAL_IOC_SDC_SET_DI_SIGNALS     _IO(IHAL_IOC_NUMBER, 26)
#define IHAL_IOC_SDC_GET_DI_SIGNALS     _IO(IHAL_IOC_NUMBER, 27)

#define IHAL_IOC_REG_INT_HDLR           _IO(IHAL_IOC_NUMBER, 30)
#define IHAL_IOC_ENABLE_IRQ             _IO(IHAL_IOC_NUMBER, 31)
#define IHAL_IOC_DISABLE_IRQ            _IO(IHAL_IOC_NUMBER, 32)
#define IHAL_IOC_WAIT_FOR_IRQ           _IO(IHAL_IOC_NUMBER, 33)
#define IHAL_IOC_GET_IRQ_STS            _IO(IHAL_IOC_NUMBER, 34)

#define IHAL_IOC_CAM_WAIT_FOR_IRQ       _IO(IHAL_IOC_NUMBER, 37)
#define IHAL_IOC_CAM_WAKEUP_WAIT_IRQ    _IO(IHAL_IOC_NUMBER, 38)
#define IHAL_IOC_CAM_REQUEST_IRQ        _IO(IHAL_IOC_NUMBER, 35)
#define IHAL_IOC_CAM_FREE_IRQ           _IO(IHAL_IOC_NUMBER, 36)

#define IHAL_IOC_CSI_INIT_INTERFACE     _IO(IHAL_IOC_NUMBER, 40)
#define IHAL_IOC_CSI_ENABLE_MCLK        _IO(IHAL_IOC_NUMBER, 41)
#define IHAL_IOC_CSI_SET_ACTUAL_SIZE    _IO(IHAL_IOC_NUMBER, 42)
#define IHAL_IOC_CSI_SET_SENSOR_SIZE    _IO(IHAL_IOC_NUMBER, 43)
#define IHAL_IOC_CSI_SET_MCLK_RATE      _IO(IHAL_IOC_NUMBER, 45)
#define IHAL_IOC_CSI_SET_WINDOW_POS     _IO(IHAL_IOC_NUMBER, 46)
#define IHAL_IOC_CSI_SET_WINDOW_SIZE    _IO(IHAL_IOC_NUMBER, 47)

#define IHAL_IOC_ALLOC_BUFFER           _IO(IHAL_IOC_NUMBER, 50)
#define IHAL_IOC_FREE_BUFFER            _IO(IHAL_IOC_NUMBER, 51)
#define IHAL_IOC_ALLOC_MEM              _IO(IHAL_IOC_NUMBER, 52)
#define IHAL_IOC_FREE_MEM               IHAL_IOC_FREE_BUFFER // same internal operation
#define IHAL_IOC_MAP_MEM                _IO(IHAL_IOC_NUMBER, 54)
#define IHAL_IOC_TEST_ALLOC             _IO(IHAL_IOC_NUMBER, 55)
#define IHAL_IOC_TEST_FREE              _IO(IHAL_IOC_NUMBER, 56)

#define IHAL_IOC_ADC_INIT_PANEL         _IO(IHAL_IOC_NUMBER, 60)
#define IHAL_IOC_ADC_INIT_IFC_TIMING    _IO(IHAL_IOC_NUMBER, 61)
#define IHAL_IOC_ADC_WRITE_CMD          _IO(IHAL_IOC_NUMBER, 62)
#define IHAL_IOC_ADC_WRITE_TEMPLATE     _IO(IHAL_IOC_NUMBER, 63)
#define IHAL_IOC_ADC_SET_UPDATE_MODE    _IO(IHAL_IOC_NUMBER, 64)
#define IHAL_IOC_ADC_IFC_WIDTH_CMD      _IO(IHAL_IOC_NUMBER, 65)
#define IHAL_IOC_ADC_READ_CMD           _IO(IHAL_IOC_NUMBER, 66)
#define IHAL_IOC_ADC_ESD_OPS            _IO(IHAL_IOC_NUMBER, 67)
#define IHAL_IOC_ADC_FLIP_STATUS        _IO(IHAL_IOC_NUMBER, 68)

/*test debug only*/
#define IHAL_IOC_TEST_UNSELECT_BUF      _IO(IHAL_IOC_NUMBER, 70)
#define IHAL_IOC_TEST_ADC_ENABLE        _IO(IHAL_IOC_NUMBER, 71)
#define IHAL_IOC_TEST_DI_ENABLE         _IO(IHAL_IOC_NUMBER, 72)

#define	IHAL_IOC_REQUEST_IRQ		_IO(IHAL_IOC_NUMBER, 80)	
#define	IHAL_IOC_FREE_IRQ		_IO(IHAL_IOC_NUMBER, 81)	
#define	IHAL_IOC_WAIT_FOR_IPU_IRQ	_IO(IHAL_IOC_NUMBER, 82)	

#define IHAL_IOC_GENERIC_REQUEST_IRQ    _IO(IHAL_IOC_NUMBER, 83)
#define IHAL_IOC_GENERIC_FREE_IRQ       _IO(IHAL_IOC_NUMBER, 84)
#define IHAL_IOC_GENERIC_WAIT_FOR_IRQ   _IO(IHAL_IOC_NUMBER, 85)
#define IHAL_IOC_GENERIC_ENABLE_IRQ     _IO(IHAL_IOC_NUMBER, 86)
#define IHAL_IOC_GENERIC_DISABLE_IRQ    _IO(IHAL_IOC_NUMBER, 87)

#define IHAL_IOC_IC_SET_GLOBAL_ALPHA   _IO(IHAL_IOC_NUMBER, 90)
#define IHAL_IOC_IC_SET_COLOR_KEY      _IO(IHAL_IOC_NUMBER, 91)
#define IHAL_IOC_RESET_ICEN_ATOMIC     _IO(IHAL_IOC_NUMBER, 92)

#define IHAL_IOC_SETUP_IPU_ACCESS_MEMORY_PRIORITY     _IO(IHAL_IOC_NUMBER, 93)
#define IHAL_IOC_RESTORE_IPU_ACCESS_MEMORY_PRIORITY     _IO(IHAL_IOC_NUMBER, 94)

#define IHAL_IOC_GET_VERSION            _IO(IHAL_IOC_NUMBER, 99)

/*
  Define the structures passed as an argument to the driver from the library.
*/
struct ih_init_chan_info 
{
    ipu_channel_t         channel;
    ih_channel_params_t * params;
};

struct ih_init_chan_buff_info 
{
    ipu_channel_t     channel;
    ih_buffer_t       type;
    ipu_channel_fmt_t chan_fmt;
    IH_U32            stride;
    void            * phyaddr_0; 
    void            * phyaddr_1;
};

struct ih_upd_chan_buff_info 
{
    ipu_channel_t channel;
    ih_buffer_t   type;
    IH_U32        bufNum;
    void        * phyaddr; 
};

struct ih_upd_chan_yuvoffset_buff_info
{
    ipu_channel_t channel;
    ih_buffer_t   type;
    IH_U16        u_offset;
    IH_U16        v_offset;
};

struct ih_sel_buff_info
{
    ipu_channel_t channel;
    ih_buffer_t   type;
    IH_U32        bufNum;
};

struct ih_link_channels_info
{
    ipu_channel_t src_ch;
    ipu_channel_t dest_ch;
};

struct ih_sdc_init_panel_info
{
    ipu_panel_t         panel;
    IH_U8               refreshRateHz;
    IH_U16              width;
    IH_U16              height;
    IH_U32              pixel_fmt;
    IH_U16              hStartWidth;
    IH_U16              hSyncWidth;
    IH_U16              hEndWidth;
    IH_U16              vStartWidth;
    IH_U16              vSyncWidth;
    IH_U16              vEndWidth;
    ipu_di_signal_cfg_t sig;
    IH_U32              num_bus_cycles;
};

struct ih_sdc_set_window_pos_info
{
    ipu_channel_t channel;
    IH_S16        x_pos;
    IH_S16        y_pos;
};

struct ih_sdc_set_color_key_info
{
    ipu_channel_t channel;
    IH_BOOL       enable;
    IH_U32        colorKey;
};

struct ih_irq_reg_info
{
    IH_U32 irq_line;
    IH_U32 irq_hdlr;
};

struct ih_reg_rw
{
    IH_U32 val;
    IH_U32 reg;
};

#if defined(DUMP_IPU_REGISTER)
struct ih_register_dump
{
    IH_U32 number;
    IH_U32 *array;
};
#endif

struct ih_csi_init_if_info 
{
    ipu_channel_fmt_t    chan_fmt;
    ipu_csi_signal_cfg_t sig;
};

struct ih_csi_set_window_pos_info
{
    IH_U16        x_pos;
    IH_U16        y_pos;
};

struct ih_csi_set_window_size_info
{
    IH_U16        width;
    IH_U16        height;
};

struct ih_adc_init_panel_info
{
    display_port_t disp;
    IH_U16 width;
    IH_U16 height;
    IH_U32 pixel_fmt;
    IH_U32 stride;
    ipu_adc_sig_cfg_t sig;
    display_addressing_t addr;
    IH_U32 vsync_width;
    vsync_t mode;
};

struct ih_adc_init_ifc_timing_info
{
    display_port_t disp;
    IH_BOOL read;
    IH_U32 cycle_time;
    IH_U32 up_time;
    IH_U32 down_time;
    IH_U32 read_latch_time;
    IH_U32 pixel_clk;
};

struct ih_adc_write_template_info
{
    display_port_t disp;
    IH_U32 * pCmd;
    IH_BOOL write;
};

struct ih_adc_set_update_mode_info
{
    ipu_channel_t channel;
    ipu_adc_update_mode_t mode;
    IH_U32 refresh_rate;
    IH_U32 addr;
    IH_U32 * size;
};

struct ih_adc_write_cmd_info
{
    display_port_t disp;
    cmddata_t type;
    IH_U32 cmd;
    const IH_U32 * params;
    IH_U16 num_params;
};

struct ih_irq_info
{
    IH_U32 line;
    IH_U32 timeout;
};

struct ih_buf_ready_info
{
    ipu_channel_t chan;
    ipu_buffer_t buf_type;
    IH_U32 buf_num;
};

struct ih_generic_request_irq_info
{
    IH_U32  line;
    IH_BOOL disable_irq;
};

struct ih_generic_wait_irq_info
{
    IH_U32 line;
    IH_U32 timeout_ms;                //In milli seconds
    IH_U32 timewait_after_trigger_us; //In micro seconds
    IH_U32 min_num_int_b4_trigger;    //Number of interrupts to wait for
};


#ifdef __KERNEL__
struct ih_buf_info
{
    IH_U32 phys_addr;
    IH_U32 virt_addr;
    IH_U32 size;
};
#endif

struct ih_adc_set_ifc_width_info
{
    display_port_t disp;
    IH_U32 pixel_fmt;
    IH_U16 ifc_width;
};

struct ih_adc_read_cmd_info
{
    display_port_t disp;
    IH_U32   cmd;
    IH_U8*   ibuf;
    IH_U16   ilen; 
};

#endif
