/* IHAL                                                                    <ihal.h> */
/*                                                                                  */
/* Copyright (c) 2006, 2008 Motorola, Inc.                                               */
/*                                                                                  */

/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 5-Jun-2006  Motorola    Initial revision.
 * 17-Apr-2008  Motorola   Add function to set IC color key/alpha
 */
 
/*!
 * @defgroup IPU MXC Image Processing Unit (IPU) Helper Driver
 */
/*!
 * @file ihal.h
 * 
 * @brief Internal file that contains extensions to the IPU driver API declarations. 
 * 
 * @ingroup IPU
 */

#ifndef _INCLUDE_IHAL_H_
#define _INCLUDE_IHAL_H_

#include <linux/types.h>
#include <linux/interrupt.h>
#include <asm/arch/hardware.h>

/*!
 * Enumeration of system memory sizes
 */
typedef enum
{
    IPU_MEM_SIZE_16 = 0,
    IPU_MEM_SIZE_32
}   ipu_mem_size_t;

/*!
 * Enumeration of types of buffers for a logical channel.
 */
typedef enum 
{
    IPU_INPUT_BUFFER_ROTATE_NONE           = IPU_INPUT_BUFFER | (IPU_ROTATE_NONE << 4),
    IPU_INPUT_BUFFER_ROTATE_VERT_FLIP      = IPU_INPUT_BUFFER | (IPU_ROTATE_VERT_FLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_HORIZ_FLIP     = IPU_INPUT_BUFFER | (IPU_ROTATE_HORIZ_FLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_180            = IPU_INPUT_BUFFER | (IPU_ROTATE_180 << 4),
    IPU_INPUT_BUFFER_ROTATE_90_RIGHT       = IPU_INPUT_BUFFER | (IPU_ROTATE_90_RIGHT << 4),
    IPU_INPUT_BUFFER_ROTATE_90_RIGHT_VFLIP = IPU_INPUT_BUFFER | (IPU_ROTATE_90_RIGHT_VFLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_90_RIGHT_HFLIP = IPU_INPUT_BUFFER | (IPU_ROTATE_90_RIGHT_HFLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_270_RIGHT      = IPU_INPUT_BUFFER | (IPU_ROTATE_90_LEFT << 4)
}   ih_buffer_t;

#define IPU_BUF_TYPE(buf) (ih_buffer_t)(buf & 3)
#define IPU_ROT_MODE(buf) (ipu_rotate_mode_t)(buf >> 4)
#define IPU_INPUT_BUF_TO_ROTATE(rot) ((ih_buffer_t)(IPU_INPUT_BUFFER | (rot << 4)))

/*!
 * RGB to YUV conversion coefficients. Q8 format (s.xxxxxxxx)
 */
typedef struct
{
    uint32_t Yr, Yg, Yb; // Y = Yr*R + Yg*G + Yb*B + Ya
    uint32_t Ur, Ug, Ub; // U = Ur*R - Ug*G + Ub*B + Ua
    uint32_t Vr, Vg, Vb; // V = Vr*R - Vg*G - Vb*B + Va
    uint32_t Ya, Ua, Va;
}   ipu_csc_rgb_yuv_t;

/*!
 * YUV to RGB conversion coefficients. Q8 format (s.xxxxxxxx)
 */
typedef struct
{
    uint32_t C1, C2, C3, C4; // R = Y + C1*U, G = Y - C2*U - C3*V, B = Y - C4*V
}   ipu_csc_yuv_rgb_t;


/*!
 * Definition of stride macro for ADC channel XY addressing mode (must be a power of 2).
 */
#define XY_STRIDE(sl) (1 << sl)

/*!
 * Information required for each IPU channel.
 */
typedef struct 
{
    uint16_t    width;
    uint16_t    height;
    uint32_t    pixel_fmt;
}   ipu_channel_fmt_t;

/*!
 * Union of initialization parameters for a logical channel.
 */
typedef union 
{
    struct {
            bool graphics_combine_en;
    } csi_prp_vf_mem;
    struct {
            bool graphics_combine_en;
            display_port_t disp;
            uint16_t x_pos;
            uint16_t y_pos;
    } csi_prp_vf_adc;
    struct {
            bool graphics_combine_en;
            uint32_t out_pixel_fmt;
    } mem_prp_vf_mem;
    struct {
            bool graphics_combine_en;
            bool global_alpha_en;
            bool key_color_en;
    } mem_pp_mem;
    struct {
            bool graphics_combine_en;
            uint32_t out_left;
            uint32_t out_top;
    } mem_pp_adc;
    struct {
            pf_operation_t operation;
            int32_t h264_pause_row; //set to 0 to disable pause (field ignored if not H264 operation)
    } mem_pf_mem;
    struct {
            display_port_t disp;
            mcu_mode_t ch_mode;
            uint16_t x_pos;
            uint16_t y_pos;
    } adc_sys;
}   ih_specific_params_t;

/*!
 * Amalgamated form of channel information.
 */
typedef struct 
{
    ipu_channel_fmt_t    in;
    ipu_channel_fmt_t    out;
    ih_specific_params_t specific;
}   ih_channel_params_t;

typedef enum
{
    ipu_irq_hdlr_SDC_FG_EOF_VT = 0,
    ipu_irq_hdlr_SDC_BG_EOF_VT,
    ipu_irq_hdlr_MEM_ROT_IN_PP_MEM,
    ipu_irq_hdlr_BLiT_data_ready,
    ipu_irq_hdlr_BLiT_SDC_BG_EOF,
    ipu_irq_hdlr_BLiT_MEM_ROT_IN_PP_MEM,
    ipu_irq_hdlr_NULL,
    num_ipu_irq_hdlr
}   ipu_irq_hdlr_t;

#endif
