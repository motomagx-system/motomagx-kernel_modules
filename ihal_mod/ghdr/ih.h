/* IHAL                                                                      <ih.h> */
/*                                                                                  */
/* Copyright (c) 2006-2008 Motorola, Inc.                                               */
/*                                                                                  */

/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 5-Jun-2006  Motorola    Initial revision.
 * 3-Jul-2007  Motorola    Compliance issue fix
 * 15-Jul-2007 Motorola    Support setting of ADC serial interface bit width
 * 24-Jul-2007 Motorola    Compliance issue fix
 * 15-Oct-2007 Motorola    Free IPU 3M reserved RAM
 * 28-Feb-2008 Motorola    Added support for generic interrupt handling
 * 10-Mar-2008 Motorola    Add three interface functions
 * 17-Apr-2008  Motorola   Add function to set IC color key/alpha and YUV offset
 * 07-May-2008 Motorola    introduced ipu_disable_channel_no_delay
 * 09-May-2008  Motorola   Add function to set Di signal polarity
 * 15-Jul-2008  Motorola   Added support for reading register for RENESAS LCD pannel
 * 23-Jul-2008 Motorola    Add function to reset IC_EN
 * 07-AUG-2008 Motorola    Add setup/restore ipu access memory priority function
 * 20-AUG-2008 Motorola    Add support for ESD issue 
 * 04-SEP-2008 Motorola    Add support for CSI position and size
 * 14-NOV-2008 Motorola    Add support for querying the status of flip  
 */

/*!
 * @defgroup IPU MXC Image Processing Unit (IPU) Helper Driver
 */
/*!
 * @file ih.h
 * 
 * @brief This file contains the IHAL library API declarations. 
 * 
 * @ingroup IPU
 */

#ifndef _INCLUDE_IH_H_
#define _INCLUDE_IH_H_

#ifdef __cplusplus  /* allow #include in a C++ file (only put this in .h files) */
extern "C" {
#endif

#include <ih_types.h>

/*
    Namespace definitions
*/
#define ipu_init_channel            ih_init_channel
#define ipu_uninit_channel          ih_uninit_channel
#define ipu_init_channel_buffer     ih_init_channel_buffer
#define ipu_update_channel_buffer   ih_update_channel_buffer
#define ipu_select_buffer           ih_select_buffer
#define ipu_toggle_buffer           ih_toggle_buffer
#define ipu_get_cur_buffer          ih_get_cur_buffer
#define ipu_link_channels           ih_link_channels
#define ipu_unlink_channels         ih_unlink_channels
#define ipu_enable_channel          ih_enable_channel
#define ipu_disable_channel         ih_disable_channel
#define ipu_is_channel_busy         ih_is_channel_busy
#define _ipu_write_param_mem        _ih_write_param_mem
#define setup_ipu_access_memory_priority      ih_setup_ipu_access_memory_priority
#define restore_ipu_access_memory_priority      ih_restore_ipu_access_memory_priority

#define ipu_di_set_pixel_fmt        ih_di_set_pixel_fmt

#define ipu_sdc_init_panel          ih_sdc_init_panel
#define ipu_sdc_set_window_pos      ih_sdc_set_window_pos
#define ipu_sdc_set_global_alpha    ih_sdc_set_global_alpha
#define ipu_sdc_set_color_key       ih_sdc_set_color_key
#define ipu_sdc_set_graphics_window ih_sdc_set_graphics_window
#define ipu_sdc_enable_clk          ih_sdc_enable_clk
#define ipu_sdc_get_panel_fmt       ih_sdc_get_panel_fmt

#define ipu_adc_init_ifc_timing     ih_adc_init_ifc_timing
#define ipu_adc_init_panel          ih_adc_init_panel
#define ipu_adc_write_template      ih_adc_write_template
#define ipu_adc_set_update_mode     ih_adc_set_update_mode
#define ipu_adc_write_cmd           ih_adc_write_cmd

#define ipu_csi_set_window_pos      ih_csi_set_window_pos
#define ipu_csi_set_window_size     ih_csi_set_window_size

#define ipu_channel_params_t        ih_channel_params_t
#define ipu_buffer_t                ih_buffer_t

struct ih_buf_info
{
    IH_U32 phys_addr;
    IH_U32 virt_addr;
    IH_U32 size;
};

/*!
 * Enumeration of IPU rotation modes
 */
typedef enum 
{
    // Note the enum values correspond to BAM value
    IPU_ROTATE_NONE           = 0,
    IPU_ROTATE_VERT_FLIP      = 1,
    IPU_ROTATE_HORIZ_FLIP     = 2,
    IPU_ROTATE_180            = 3,
    IPU_ROTATE_90_RIGHT       = 4,
    IPU_ROTATE_90_RIGHT_VFLIP = 5,
    IPU_ROTATE_90_RIGHT_HFLIP = 6,
    IPU_ROTATE_270_RIGHT      = 7
}   ipu_rotate_mode_t;

/*!
 * Enumeration of Post Filter modes
 */
typedef enum 
{
    PF_DISABLE_ALL          = 0,
    PF_MPEG4_DEBLOCK        = 1,
    PF_MPEG4_DERING         = 2,
    PF_MPEG4_DEBLOCK_DERING = 3,
    PF_H264_DEBLOCK         = 4
}   pf_operation_t;

/*!
 * Enumeration of Synchronous (Memory-less) panel types
 */
typedef enum 
{
    IPU_PANEL_SHARP_TFT,
    IPU_PANEL_TFT
}   ipu_panel_t;

#define ipu_fourcc(a,b,c,d)\
        (((IH_U32)(a)<<0)|((IH_U32)(b)<<8)|((IH_U32)(c)<<16)|((IH_U32)(d)<<24))

/*!
 * @name IPU Pixel Formats
 *
 * Pixel formats are defined with ASCII FOURCC code. The pixel format codes are
 * the same used by V4L2 API.
 */

/*! @{ */
/*! @name Generic or Raw Data Formats */ /*! @{ */
#define IPU_PIX_FMT_GENERIC ipu_fourcc('I','P','U','0') /*!< IPU Generic Data */
#define IPU_PIX_FMT_GENERIC32 ipu_fourcc('I','P','U','1') /*!< IPU Generic Data 32 bit */
#define IPU_PIX_FMT_GENERIC16 ipu_fourcc('I','P','U','2') /*!< IPU Generic Data 32 bit */
/*! @} */

/*! @name RGB Formats */ /*! @{ */
#define IPU_PIX_FMT_RGB332  ipu_fourcc('R','G','B','1') /*!<  8  RGB-3-3-2     */
#define IPU_PIX_FMT_RGB555  ipu_fourcc('R','G','B','O') /*!< 16  RGB-5-5-5     */
#define IPU_PIX_FMT_RGB565  ipu_fourcc('R','G','B','P') /*!< 16  RGB-5-6-5     */
#define IPU_PIX_FMT_BGR565  ipu_fourcc('B','G','R','P') /*!< 16  BGR-5-6-5     */
#define IPU_PIX_FMT_RGB555X ipu_fourcc('R','G','B','Q') /*!< 16  RGB-5-5-5 BE  */
#define IPU_PIX_FMT_RGB565X ipu_fourcc('R','G','B','R') /*!< 16  RGB-5-6-5 BE  */
#define IPU_PIX_FMT_RGB666  ipu_fourcc('R','G','B','6') /*!< 18  RGB-6-6-6     */
#define IPU_PIX_FMT_BGR666  ipu_fourcc('B','G','R','0') /*!< 18  BGR-6-6-6     */
#define IPU_PIX_FMT_BGRA6666  ipu_fourcc('B','G','R','6') /*!< 24  BGR-6-6-6-6 */
#define IPU_PIX_FMT_BGR24   ipu_fourcc('B','G','R','3') /*!< 24  BGR-8-8-8     */
#define IPU_PIX_FMT_RGB24   ipu_fourcc('R','G','B','3') /*!< 24  RGB-8-8-8     */
#define IPU_PIX_FMT_BGR32   ipu_fourcc('B','G','R','4') /*!< 32  BGR-8-8-8-8   */
#define IPU_PIX_FMT_BGRA32  ipu_fourcc('B','G','R','A') /*!< 32  BGR-8-8-8-8   */
#define IPU_PIX_FMT_RGB32   ipu_fourcc('R','G','B','4') /*!< 32  RGB-8-8-8-8   */
#define IPU_PIX_FMT_RGBA32  ipu_fourcc('R','G','B','A') /*!< 32  RGB-8-8-8-8   */
#define IPU_PIX_FMT_ABGR    ipu_fourcc('A','B','G','R') /*!< 32  RGB-8-8-8-8   */

/*! @} */

/*! @name YUV Interleaved Formats */ /*! @{ */
#define IPU_PIX_FMT_YUYV    ipu_fourcc('Y','U','Y','V') /*!< 16  YUV 4:2:2     */
#define IPU_PIX_FMT_UYVY    ipu_fourcc('U','Y','V','Y') /*!< 16  YUV 4:2:2     */
#define IPU_PIX_FMT_Y41P    ipu_fourcc('Y','4','1','P') /*!< 12  YUV 4:1:1     */
/*! @} */

/*! @name YUV Planar Formats */ /*! @{ */
#define IPU_PIX_FMT_GREY    ipu_fourcc('G','R','E','Y') /*!< 8  Greyscale     */
#define IPU_PIX_FMT_YVU410P ipu_fourcc('Y','V','U','9') /*!< 9  YVU 4:1:0     */
#define IPU_PIX_FMT_YUV410P ipu_fourcc('Y','U','V','9') /*!< 9  YUV 4:1:0     */
#define IPU_PIX_FMT_YVU420P ipu_fourcc('Y','V','1','2') /*!< 12  YVU 4:2:0     */
#define IPU_PIX_FMT_YUV420P ipu_fourcc('I','4','2','0') /*!< 12  YUV420 planar */
#define IPU_PIX_FMT_YUV420P2 ipu_fourcc('Y','U','1','2') /*!< 12  YUV 4:2:0 planar */
#define IPU_PIX_FMT_YVU422P ipu_fourcc('Y','V','1','6') /*!< 16  YVU422 planar */
/*! @} */

//#define IPU_PIX_FMT_YUV422P ipu_fourcc('4','2','2','P') /* 16  YVU422 planar */
//#define IPU_PIX_FMT_YUV411P ipu_fourcc('4','1','1','P') /* 16  YVU411 planar */
//#define IPU_PIX_FMT_YYUV    ipu_fourcc('Y','Y','U','V') /* 16  YUV 4:2:2     */


#define IPU_PIX_FMT_CD8      30 /*!< 8  Cmd/Data 8-bit */
/*! @} */


// IPU Driver channels definitions. 
// Note these are different from IDMA channels
#define _MAKE_CHAN(num, in, out, sec)    ((num << 24) | (sec << 16) | (out << 8) | in) 
#define IPU_CHAN_ID(ch)         (ch >> 24)
#define IPU_CHAN_SEC_DMA(ch)    ((IH_U32) (ch >> 16) & 0xFF)
#define IPU_CHAN_OUT_DMA(ch)    ((IH_U32) (ch >> 8) & 0xFF)
#define IPU_CHAN_IN_DMA(ch)     ((IH_U32) (ch & 0xFF))
#define IPU_CHAN_ID_CSI_MEM          1
#define IPU_CHAN_ID_CSI_PRP_ENC_MEM  2
#define IPU_CHAN_ID_MEM_PRP_ENC_MEM  3
#define IPU_CHAN_ID_MEM_ROT_ENC_MEM  4
#define IPU_CHAN_ID_CSI_PRP_VF_MEM   5
#define IPU_CHAN_ID_CSI_PRP_VF_ADC   6
#define IPU_CHAN_ID_MEM_PRP_VF_MEM   7
#define IPU_CHAN_ID_MEM_PRP_VF_ADC   8
#define IPU_CHAN_ID_MEM_ROT_VF_MEM   9
#define IPU_CHAN_ID_MEM_PP_MEM      10
#define IPU_CHAN_ID_MEM_ROT_PP_MEM  11
#define IPU_CHAN_ID_MEM_PP_ADC      12
#define IPU_CHAN_ID_MEM_SDC_BG      14
#define IPU_CHAN_ID_MEM_SDC_FG      15
#define IPU_CHAN_ID_MEM_SDC_MASK    16
#define IPU_CHAN_ID_ADC_SYS1        17
#define IPU_CHAN_ID_ADC_SYS2        18
#define IPU_CHAN_ID_MEM_PF_Y_MEM    19
#define IPU_CHAN_ID_MEM_PF_U_MEM    20
#define IPU_CHAN_ID_MEM_PF_V_MEM    21
#define IPU_CHAN_ID_MAX             IPU_CHAN_ID_MEM_PF_V_MEM
#define IPU_CHAN_ID_NONE            0xFF

/*!
 * Enumeration of IPU logical channels. An IPU logical channel is defined as a
 * combination of an input (memory to IPU), output (IPU to memory), and/or 
 * secondary input IDMA channels and in some cases an Image Converter task. 
 * Some channels consist of only an input or output.
 */
typedef enum 
{
    CSI_MEM         = _MAKE_CHAN(IPU_CHAN_ID_CSI_MEM, /*!< CSI raw sensor data to memory */
                                 IPU_CHAN_ID_NONE, // IDMAC Chan Num Input
                                 7,                // IDMAC Chan Num Output
                                 IPU_CHAN_ID_NONE),// IDMAC Chan Num Secondary    

    CSI_PRP_ENC_MEM = _MAKE_CHAN(IPU_CHAN_ID_CSI_PRP_ENC_MEM, /*!< CSI to IC Encoder PreProcessing to Memory */
                                 IPU_CHAN_ID_NONE,
                                 0,
                                 IPU_CHAN_ID_NONE),
                                 
    MEM_PRP_ENC_MEM = _MAKE_CHAN(IPU_CHAN_ID_MEM_PRP_ENC_MEM, /*!< Memory to IC Encoder PreProcessing to Memory */
                                 6,
                                 0,
                                 IPU_CHAN_ID_NONE),       
                                 
    MEM_ROT_ENC_MEM = _MAKE_CHAN(IPU_CHAN_ID_MEM_ROT_ENC_MEM, /*!< Memory to IC Encoder Rotation to Memory */
                                 10,
                                 8,
                                 IPU_CHAN_ID_NONE),      
                                 
    CSI_PRP_VF_MEM  = _MAKE_CHAN(IPU_CHAN_ID_CSI_PRP_VF_MEM, /*!< CSI to IC Viewfinder PreProcessing to Memory */
                                 IPU_CHAN_ID_NONE,
                                 1,
                                 IPU_CHAN_ID_NONE),    
                                 
    CSI_PRP_VF_ADC  = _MAKE_CHAN(IPU_CHAN_ID_CSI_PRP_VF_ADC, /*!< CSI to IC Viewfinder PreProcessing to ADC */
                                 IPU_CHAN_ID_NONE,
                                 1,
                                 IPU_CHAN_ID_NONE),
                                 
    MEM_PRP_VF_MEM  = _MAKE_CHAN(IPU_CHAN_ID_MEM_PRP_VF_MEM, /*!< Memory to IC Viewfinder PreProcessing to Memory */
                                 6,
                                 1,
                                 3),          
                                 
    MEM_PRP_VF_ADC  = _MAKE_CHAN(IPU_CHAN_ID_MEM_PRP_VF_ADC, /*!< Memory to IC Viewfinder PreProcessing to ADC */
                                 6,
                                 1,
                                 3),
                                 
    MEM_ROT_VF_MEM  = _MAKE_CHAN(IPU_CHAN_ID_MEM_ROT_VF_MEM, /*!< Memory to IC Viewfinder Rotation to Memory */
                                 11,
                                 9,
                                 IPU_CHAN_ID_NONE),
                                 
    MEM_PP_MEM      = _MAKE_CHAN(IPU_CHAN_ID_MEM_PP_MEM, /*!< Memory to IC PostProcessing to Memory */
                                 5,
                                 2,
                                 4),
                                 
    MEM_ROT_PP_MEM  = _MAKE_CHAN(IPU_CHAN_ID_MEM_ROT_PP_MEM, /*!< Memory to IC PostProcessing Rotation to Memory */
                                 13,
                                 12,
                                 IPU_CHAN_ID_NONE),
                                 
    MEM_PP_ADC      = _MAKE_CHAN(IPU_CHAN_ID_MEM_PP_ADC, /*!< Memory to IC PostProcessing to ADC */
                                 5,
                                 2,
                                 4),

    MEM_SDC_BG      = _MAKE_CHAN(IPU_CHAN_ID_MEM_SDC_BG, /*!< Memory to SDC Background plane */
                                 14,
                                 IPU_CHAN_ID_NONE,
                                 IPU_CHAN_ID_NONE),
                                 
    MEM_SDC_FG      = _MAKE_CHAN(IPU_CHAN_ID_MEM_SDC_FG, /*!< Memory to SDC Foreground plane */    
                                 15,
                                 IPU_CHAN_ID_NONE,
                                 IPU_CHAN_ID_NONE),
                                 
    MEM_SDC_MASK    = _MAKE_CHAN(IPU_CHAN_ID_MEM_SDC_MASK, /*!< Memory to SDC Mask*/
                                 16,
                                 IPU_CHAN_ID_NONE,
                                 IPU_CHAN_ID_NONE),

    ADC_SYS1        = _MAKE_CHAN(IPU_CHAN_ID_ADC_SYS1, /*!< Memory to ADC System Channel 1 */
                                 18,
                                 22,
                                 20),

    ADC_SYS2        = _MAKE_CHAN(IPU_CHAN_ID_ADC_SYS2, /*!< Memory to ADC System Channel 2 */
                                 19,
                                 23,
                                 21),

    MEM_PF_Y_MEM    = _MAKE_CHAN(IPU_CHAN_ID_MEM_PF_Y_MEM, /*!< Y and PF Memory to Post-filter to Y Memory */
                                 26,
                                 29,
                                 24),
                                 
    MEM_PF_U_MEM    = _MAKE_CHAN(IPU_CHAN_ID_MEM_PF_U_MEM, /*!< U and PF Memory to Post-filter to U Memory */
                                 27,
                                 30,
                                 25),
                                 
    MEM_PF_V_MEM    = _MAKE_CHAN(IPU_CHAN_ID_MEM_PF_V_MEM, /*!< V Memory to Post-filter to V Memory */
                                 28,
                                 31,
                                 IPU_CHAN_ID_NONE)

}   ipu_channel_t;

/*!
 * Enumeration of types of buffers for a logical channel.
 */
typedef enum 
{
    IPU_INPUT_BUFFER,       /*!< Buffer for input to IPU */
    IPU_OUTPUT_BUFFER,      /*!< Buffer for output from IPU */
    IPU_SEC_INPUT_BUFFER,   /*!< Buffer for second input to IPU */
    IPU_INPUT_BUFFER_ROTATE_NONE           = IPU_INPUT_BUFFER | (IPU_ROTATE_NONE << 4),
    IPU_INPUT_BUFFER_ROTATE_VERT_FLIP      = IPU_INPUT_BUFFER | (IPU_ROTATE_VERT_FLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_HORIZ_FLIP     = IPU_INPUT_BUFFER | (IPU_ROTATE_HORIZ_FLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_180            = IPU_INPUT_BUFFER | (IPU_ROTATE_180 << 4),
    IPU_INPUT_BUFFER_ROTATE_90_RIGHT       = IPU_INPUT_BUFFER | (IPU_ROTATE_90_RIGHT << 4),
    IPU_INPUT_BUFFER_ROTATE_90_RIGHT_VFLIP = IPU_INPUT_BUFFER | (IPU_ROTATE_90_RIGHT_VFLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_90_RIGHT_HFLIP = IPU_INPUT_BUFFER | (IPU_ROTATE_90_RIGHT_HFLIP << 4),
    IPU_INPUT_BUFFER_ROTATE_270_RIGHT      = IPU_INPUT_BUFFER | (IPU_ROTATE_270_RIGHT << 4)
}   ipu_buffer_t;

#define IPU_BUF_TYPE(buf) (ipu_buffer_t)(buf & 3)
#define IPU_ROT_MODE(buf) (ipu_rotate_mode_t)(buf >> 4)
#define IPU_INPUT_BUF_TO_ROTATE(rot) ((ipu_buffer_t)(IPU_INPUT_BUFFER | (rot << 4)))

/*!
 * RGB to YUV conversion coefficients. Q8 format (s.xxxxxxxx)
 */
typedef struct
{
    IH_U32 Yr, Yg, Yb; // Y = Yr*R + Yg*G + Yb*B + Ya
    IH_U32 Ur, Ug, Ub; // U = Ur*R - Ug*G + Ub*B + Ua
    IH_U32 Vr, Vg, Vb; // V = Vr*R - Vg*G - Vb*B + Va
    IH_U32 Ya, Ua, Va;
}   ipu_csc_rgb_yuv_t;

/*!
 * YUV to RGB conversion coefficients. Q8 format (s.xxxxxxxx)
 */
typedef struct
{
    IH_U32 C1, C2, C3, C4; // R = Y + C1*U, G = Y - C2*U - C3*V, B = Y - C4*V
}   ipu_csc_yuv_rgb_t;

/*!
 * Enumeration of DI ports.
 */
typedef enum 
{
    DISP0,
    DISP1,
    DISP2,
    DISP3
}   display_port_t;

/*!
 * Enumeration of ADC channel operation mode.
 */
typedef enum 
{
    Disable,
    WriteTemplateNonSeq,
    ReadTemplateNonSeq,
    WriteTemplateUnCon,
    ReadTemplateUnCon,
    WriteDataWithRS,
    WriteDataWoRS,
    WriteCmd 
}   mcu_mode_t;

/*!
 * Enumeration of ADC channel addressing mode.
 */
typedef enum 
{
    FullWoBE,
    FullWithBE,
    XY
}   display_addressing_t;

/*!
 * Definition of stride macro for ADC channel XY addressing mode (must be a power of 2).
 */
#define XY_STRIDE(sl) (1 << sl)

/*!
 * Information required for each IPU channel.
 */
typedef struct 
{
    IH_U16    width;
    IH_U16    height;
    IH_U32    pixel_fmt;
}   ipu_channel_fmt_t;

/*!
 * Union of initialization parameters for a logical channel.
 */
typedef union 
{
    struct {
            IH_BOOL graphics_combine_en;
    } csi_prp_vf_mem;
    struct {
            IH_BOOL graphics_combine_en;
            display_port_t disp;
            IH_U16 x_pos;
            IH_U16 y_pos;
    } csi_prp_vf_adc;
    struct {
            IH_BOOL graphics_combine_en;
            IH_U32 out_pixel_fmt;
    } mem_prp_vf_mem;
    struct {
            IH_BOOL graphics_combine_en;
            IH_BOOL global_alpha_en;
            IH_BOOL key_color_en;
    } mem_pp_mem;
    struct {
            IH_BOOL graphics_combine_en;
            IH_U32 out_left;
            IH_U32 out_top;
    } mem_pp_adc;
    struct {
            pf_operation_t operation;
            IH_S32 h264_pause_row; //set to 0 to disable pause (field ignored if not H264 operation)
    } mem_pf_mem;
    struct {
            display_port_t disp;
            mcu_mode_t ch_mode;
            IH_U16 x_pos;
            IH_U16 y_pos;
    } adc_sys;
}   ipu_specific_params_t;

/*!
 * Amalgamated form of channel information.
 */
typedef struct 
{
    ipu_channel_fmt_t     in;
    ipu_channel_fmt_t     out;
    ipu_specific_params_t specific;
}   ipu_channel_params_t;

/*!
 * Return value for ipu_cam_wait_for_int_event. 
 */
typedef enum
{
    IPU_CAM_WAIT_INT_OCCUR = 0,
    IPU_CAM_WAIT_INT_TIMEOUT,
    IPU_CAM_WAIT_INT_CANCELED,
    IPU_CAM_WAIT_INT_OVERFLOW,

    IPU_CAM_WAIT_INT_FAULT = -1,

}   ipu_cam_wait_ret_t;

/*!
 * Enumeration of IPU interrupt sources.
 */
typedef enum 
{
    IPU_IRQ_PRP_ENC_OUT_EOF             = 0,
    IPU_IRQ_PRP_VF_OUT_EOF              = 1,
    IPU_IRQ_PP_OUT_EOF                  = 2,
    IPU_IRQ_PRP_GRAPH_IN_EOF            = 3,
    IPU_IRQ_PP_GRAPH_IN_EOF             = 4,
    IPU_IRQ_PP_IN_EOF                   = 5,
    IPU_IRQ_PRP_IN_EOF                  = 6,
    IPU_IRQ_SENSOR_OUT_EOF              = 7,
    IPU_IRQ_PRP_ENC_ROT_OUT_EOF         = 8,
    IPU_IRQ_PRP_VF_ROT_OUT_EOF          = 9,
    IPU_IRQ_PRP_ENC_ROT_IN_EOF          = 10,
    IPU_IRQ_PRP_VF_ROT_IN_EOF           = 11,
    IPU_IRQ_PP_ROT_OUT_EOF              = 12,
    IPU_IRQ_PP_ROT_IN_EOF               = 13,
    IPU_IRQ_SDC_BG_EOF                  = 14,   
    IPU_IRQ_SDC_FG_EOF                  = 15,   
    IPU_IRQ_SDC_MASK_EOF                = 16,
    IPU_IRQ_SDC_BG_PART_EOF             = 17,
    IPU_IRQ_ADC_SYS1_WR_EOF             = 18,
    IPU_IRQ_ADC_SYS2_WR_EOF             = 19,
    IPU_IRQ_ADC_SYS1_CMD_EOF            = 20,
    IPU_IRQ_ADC_SYS2_CMD_EOF            = 21,
    IPU_IRQ_ADC_SYS1_RD_EOF             = 22,
    IPU_IRQ_ADC_SYS2_RD_EOF             = 23,
    IPU_IRQ_PF_QP_IN_EOF                = 24,
    IPU_IRQ_PF_BSP_IN_EOF               = 25,
    IPU_IRQ_PF_Y_IN_EOF                 = 26,
    IPU_IRQ_PF_U_IN_EOF                 = 27,
    IPU_IRQ_PF_V_IN_EOF                 = 28,
    IPU_IRQ_PF_Y_OUT_EOF                = 29,
    IPU_IRQ_PF_U_OUT_EOF                = 30,
    IPU_IRQ_PF_V_OUT_EOF                = 31,

    IPU_IRQ_PRP_ENC_OUT_NF              = 32,
    IPU_IRQ_PRP_VF_OUT_NF               = 33,
    IPU_IRQ_PP_OUT_NF                   = 34,
    IPU_IRQ_PRP_GRAPH_IN_NF             = 35,
    IPU_IRQ_PP_GRAPH_IN_NF              = 36,
    IPU_IRQ_PP_IN_NF                    = 37,
    IPU_IRQ_PRP_IN_NF                   = 38,
    IPU_IRQ_SENSOR_OUT_NF               = 39,
    IPU_IRQ_PRP_ENC_ROT_OUT_NF          = 40,
    IPU_IRQ_PRP_VF_ROT_OUT_NF           = 41,
    IPU_IRQ_PRP_ENC_ROT_IN_NF           = 42,
    IPU_IRQ_PRP_VF_ROT_IN_NF            = 43,
    IPU_IRQ_PP_ROT_OUT_NF               = 44,
    IPU_IRQ_PP_ROT_IN_NF                = 45,
    IPU_IRQ_SDC_FG_NF                   = 46,
    IPU_IRQ_SDC_BG_NF                   = 47,
    IPU_IRQ_SDC_MASK_NF                 = 48,
    IPU_IRQ_SDC_BG_PART_NF              = 49,
    IPU_IRQ_ADC_SYS1_WR_NF              = 50,
    IPU_IRQ_ADC_SYS2_WR_NF              = 51,
    IPU_IRQ_ADC_SYS1_CMD_NF             = 52,
    IPU_IRQ_ADC_SYS2_CMD_NF             = 53,
    IPU_IRQ_ADC_SYS1_RD_NF              = 54,
    IPU_IRQ_ADC_SYS2_RD_NF              = 55,
    IPU_IRQ_PF_QP_IN_NF                 = 56,
    IPU_IRQ_PF_BSP_IN_NF                = 57,
    IPU_IRQ_PF_Y_IN_NF                  = 58,
    IPU_IRQ_PF_U_IN_NF                  = 59,
    IPU_IRQ_PF_V_IN_NF                  = 60,
    IPU_IRQ_PF_Y_OUT_NF                 = 61,
    IPU_IRQ_PF_U_OUT_NF                 = 62,
    IPU_IRQ_PF_V_OUT_NF                 = 63,

    IPU_IRQ_BREAKRQ                     = 64,
    IPU_IRQ_SDC_BG_OUT_EOF              = 65,
    IPU_IRQ_SDC_FG_OUT_EOF              = 66,
    IPU_IRQ_SDC_MASK_OUT_EOF            = 67,
    IPU_IRQ_ADC_SERIAL_DATA_OUT         = 68,
    IPU_IRQ_SENSOR_NF                   = 69,
    IPU_IRQ_SENSOR_EOF                  = 70,
    IPU_IRQ_SDC_DISP3_VSYNC             = 80,
    IPU_IRQ_ADC_DISP0_VSYNC             = 81,
    IPU_IRQ_ADC_DISP12_VSYNC            = 82,
    IPU_IRQ_ADC_PRP_EOF                 = 83,
    IPU_IRQ_ADC_PP_EOF                  = 84,
    IPU_IRQ_ADC_SYS1_EOF                = 85,
    IPU_IRQ_ADC_SYS2_EOF                = 86,

    IPU_IRQ_PRP_ENC_OUT_NFB4EOF_ERR     = 96,
    IPU_IRQ_PRP_VF_OUT_NFB4EOF_ERR      = 97,
    IPU_IRQ_PP_OUT_NFB4EOF_ERR          = 98,
    IPU_IRQ_PRP_GRAPH_IN_NFB4EOF_ERR    = 99,
    IPU_IRQ_PP_GRAPH_IN_NFB4EOF_ERR     = 100,
    IPU_IRQ_PP_IN_NFB4EOF_ERR           = 101,
    IPU_IRQ_PRP_IN_NFB4EOF_ERR          = 102,
    IPU_IRQ_SENSOR_OUT_NFB4EOF_ERR      = 103,
    IPU_IRQ_PRP_ENC_ROT_OUT_NFB4EOF_ERR = 104,
    IPU_IRQ_PRP_VF_ROT_OUT_NFB4EOF_ERR  = 105,
    IPU_IRQ_PRP_ENC_ROT_IN_NFB4EOF_ERR  = 106,
    IPU_IRQ_PRP_VF_ROT_IN_NFB4EOF_ERR   = 107,
    IPU_IRQ_PP_ROT_OUT_NFB4EOF_ERR      = 108,
    IPU_IRQ_PP_ROT_IN_NFB4EOF_ERR       = 109,
    IPU_IRQ_SDC_FG_NFB4EOF_ERR          = 110,
    IPU_IRQ_SDC_BG_NFB4EOF_ERR          = 111,
    IPU_IRQ_SDC_MASK_NFB4EOF_ERR        = 112,
    IPU_IRQ_SDC_BG_PART_NFB4EOF_ERR     = 113,
    IPU_IRQ_ADC_SYS1_WR_NFB4EOF_ERR     = 114,
    IPU_IRQ_ADC_SYS2_WR_NFB4EOF_ERR     = 115,
    IPU_IRQ_ADC_SYS1_CMD_NFB4EOF_ERR    = 116,
    IPU_IRQ_ADC_SYS2_CMD_NFB4EOF_ERR    = 117,
    IPU_IRQ_ADC_SYS1_RD_NFB4EOF_ERR     = 118,
    IPU_IRQ_ADC_SYS2_RD_NFB4EOF_ERR     = 119,
    IPU_IRQ_PF_QP_IN_NFB4EOF_ERR        = 120,
    IPU_IRQ_PF_BSP_IN_NFB4EOF_ERR       = 121,
    IPU_IRQ_PF_Y_IN_NFB4EOF_ERR         = 122,
    IPU_IRQ_PF_U_IN_NFB4EOF_ERR         = 123,
    IPU_IRQ_PF_V_IN_NFB4EOF_ERR         = 124,
    IPU_IRQ_PF_Y_OUT_NFB4EOF_ERR        = 125,
    IPU_IRQ_PF_U_OUT_NFB4EOF_ERR        = 126,
    IPU_IRQ_PF_V_OUT_NFB4EOF_ERR        = 127,

    IPU_IRQ_BAYER_BUFOVF_ERR            = 128,
    IPU_IRQ_ENC_BUFOVF_ERR              = 129,
    IPU_IRQ_VF_BUFOVF_ERR               = 130,
    IPU_IRQ_ADC_PP_TEAR_ERR             = 131,
    IPU_IRQ_ADC_SYS1_TEAR_ERR           = 132,
    IPU_IRQ_ADC_SYS2_TEAR_ERR           = 133,
    IPU_IRQ_AHB_M1_ERR                  = 134,
    IPU_IRQ_AHB_M12_ERR                 = 135,
    IPU_IRQ_SDC_BGD_ERR                 = 136,
    IPU_IRQ_SDC_FGD_ERR                 = 137,
    IPU_IRQ_SDC_MASKD_ERR               = 138,
    IPU_IRQ_BAYER_FRM_LOST_ERR          = 139,
    IPU_IRQ_ENC_FRM_LOST_ERR            = 140,
    IPU_IRQ_VF_FRM_LOST_ERR             = 141,
    IPU_IRQ_ADC_LOCK_ERR                = 142,
    IPU_IRQ_DI_LLA_LOCK_ERR             = 143,

    IPU_IRQ_COUNT
}   ipu_irq_line_t;

/*!
 * Bitfield of Display Interface signal polarities.
 */
typedef struct 
{
    unsigned datamask_en: 1;
    unsigned clksel_en  : 1;
    unsigned clkidle_en : 1;
    unsigned data_pol   : 1;        // true = inverted
    unsigned clk_pol    : 1;         // true = rising edge
    unsigned enable_pol : 1;
    unsigned Hsync_pol  : 1;       // true = active high
    unsigned Vsync_pol  : 1;
    unsigned read_pol   : 1;
    unsigned write_pol  : 1;
    unsigned rsvd       :24;
}   ipu_di_signal_cfg_t;

/*!
 * Bitfield of CSI signal polarities and modes.
 */
typedef struct 
{
    unsigned data_width : 3;
    unsigned clk_mode   : 2;
    unsigned ext_vsync  : 1;
    unsigned Vsync_pol  : 1;
    unsigned Hsync_pol  : 1;
    unsigned pixclk_pol : 1;
    unsigned data_pol   : 1;
    unsigned sens_clksrc: 1;
    unsigned rsvd       :21;
}   ipu_csi_signal_cfg_t;

/*!
 * Enumeration of CSI data bus widths.
 */
enum 
{
    IPU_CSI_DATA_WIDTH_4,
    IPU_CSI_DATA_WIDTH_8,
    IPU_CSI_DATA_WIDTH_10,
    IPU_CSI_DATA_WIDTH_16
};

/*!
 * Enumeration of CSI clock modes.
 */
enum 
{
    IPU_CSI_CLK_MODE_GATED_CLK,
    IPU_CSI_CLK_MODE_NONGATED_CLK,
    IPU_CSI_CLK_MODE_CCIR656_PROGRESSIVE,
    IPU_CSI_CLK_MODE_CCIR656_INTERLACED
};

/*!
 * Enumeration of ADC vertical sync mode.
 */
typedef enum
{
    VsyncNone,
    VsyncInternal,
    VsyncCSI,
    VsyncExternal
}   vsync_t;

typedef enum
{
    DAT,
    CMD
}   cmddata_t;

/*!
 * Enumeration of ADC display update mode.
 */
typedef enum 
{
    IPU_ADC_REFRESH_NONE,
    IPU_ADC_AUTO_REFRESH,
    IPU_ADC_AUTO_REFRESH_SNOOP,
    IPU_ADC_SNOOPING
}   ipu_adc_update_mode_t;

/*!
 * Enumeration of ADC display interface types (serial or parallel).
 */
enum
{
    IPU_ADC_IFC_MODE_SYS80_TYPE1,
    IPU_ADC_IFC_MODE_SYS80_TYPE2,
    IPU_ADC_IFC_MODE_SYS68K_TYPE1,
    IPU_ADC_IFC_MODE_SYS68K_TYPE2,
    IPU_ADC_IFC_MODE_3WIRE_SERIAL,
    IPU_ADC_IFC_MODE_4WIRE_SERIAL,
    IPU_ADC_IFC_MODE_5WIRE_SERIAL_CLK,
    IPU_ADC_IFC_MODE_5WIRE_SERIAL_CS
};

enum
{
    IPU_ADC_IFC_WIDTH_8,
    IPU_ADC_IFC_WIDTH_16,
};

/*!
 * Enumeration of ADC display parallel interface burst mode.
 */
enum
{
    IPU_ADC_BURST_WCS,
    IPU_ADC_BURST_WBLCK,
    IPU_ADC_BURST_NONE,
    IPU_ADC_BURST_SERIAL
};

/*!
 * Enumeration of ADC display serial interface RW signal timing modes.
 */
enum
{
    IPU_ADC_NO_RW,
    IPU_ADC_RW_BEFORE_RS,
    IPU_ADC_RW_AFTER_RS
};

/*!
 * Bitfield of ADC signal polarities and modes.
 */
typedef struct
{
    unsigned data_pol:1;
    unsigned clk_pol:1;
    unsigned cs_pol:1;
    unsigned addr_pol:1;
    unsigned read_pol:1;
    unsigned write_pol:1;
    unsigned Vsync_pol:1;
    unsigned burst_pol:1;
    unsigned burst_mode:2;
    unsigned ifc_mode:3;
    unsigned ifc_width:5;
    unsigned ser_preamble_len:4;
    unsigned ser_preamble:8;
    unsigned ser_rw_mode:2;
}   ipu_adc_sig_cfg_t;

/*!
 * Enumeration of ADC template commands.
 */
enum 
{
    RD_DATA,
    RD_ACK,
    RD_WAIT,
    WR_XADDR,
    WR_YADDR,
    WR_ADDR,
    WR_CMND,
    WR_DATA
};

/*!
 * Enumeration of ADC template command flow control.
 */
enum 
{
    SINGLE_STEP,
    PAUSE,
    STOP
};

//Define template constants
#define     ATM_ADDR_RANGE      0x20 //offset address of DISP
#define     TEMPLATE_BUF_SIZE   0x20 //size of template

/*!
 * Define to create ADC template command entry.
 */
#define ipu_adc_template_gen(oc, rs, fc, dat) ( rs<<29 | fc<<27 | oc<<24 | dat )

typedef enum
{
    ipu_irq_hdlr_SDC_FG_EOF_VT = 0,
    ipu_irq_hdlr_SDC_BG_EOF_VT,
    ipu_irq_hdlr_MEM_ROT_IN_PP_MEM,
    ipu_irq_hdlr_BLiT_data_ready,
    ipu_irq_hdlr_BLiT_SDC_BG_EOF,
    ipu_irq_hdlr_BLiT_MEM_ROT_IN_PP_MEM,
    ipu_irq_hdlr_NULL
}   ipu_irq_hdlr_t;

/*!
 * Enumeration of ADC ESD recovery operations.
 */
typedef enum 
{
    ESD_OPS_DISABLE_PIXEL_CLOCK,
    ESD_OPS_ENABLE_PIXEL_CLOCK,
    ESD_OPS_SER_EN_LOW,
    ESD_OPS_SER_EN_HIGH,
    ESD_OPS_SER_RST_LOW,
    ESD_OPS_SER_RST_HIGH,
    ESD_OPS_DISP_RST_LOW,
    ESD_OPS_DISP_RST_HIGH,
    ESD_OPS_DISP_RD_POL_LOW,
    ESD_OPS_DISP_RD_POL_HIGH
} ipu_esd_ops_t;

/* Common IPU API */
void ipu_init(void);
void ipu_uninit(void);

IH_S32 ipu_init_channel(ipu_channel_t channel, ipu_channel_params_t * params);
IH_S32 ipu_uninit_channel(ipu_channel_t channel);
IH_S32 ipu_is_init_channel(ipu_channel_t channel);

IH_S32 ipu_init_channel_buffer(ipu_channel_t channel, ipu_buffer_t type,
                               ipu_channel_fmt_t * chan_fmt, IH_U32 stride,
                               void * phyaddr_0, void * phyaddr_1);

IH_S32 ipu_update_channel_buffer(ipu_channel_t channel, ipu_buffer_t type,
                                 IH_U32 bufNum, void * phyaddr);
IH_S32  ipu_update_channel_yuvoffset_buffer(ipu_channel_t channel, ipu_buffer_t type, 
                                            IH_U16 u_offset, IH_U16 v_offset);

IH_S32 ipu_select_buffer(ipu_channel_t channel,
                         ipu_buffer_t type,
                         IH_U32 bufNum);
    
IH_S32 ipu_toggle_buffer(ipu_channel_t channel,
                         ipu_buffer_t type);
    
IH_S32 ipu_get_cur_buffer(ipu_channel_t channel,
                          ipu_buffer_t type);
    
IH_S32 ipu_link_channels(ipu_channel_t src_ch, 
                         ipu_channel_t dest_ch);
IH_S32 ipu_unlink_channels(ipu_channel_t src_ch, 
                           ipu_channel_t dest_ch);

IH_S32 ipu_enable_channel(ipu_channel_t channel);
IH_S32 ipu_disable_channel(ipu_channel_t channel);
IH_S32 ipu_is_channel_busy(ipu_channel_t channel);
IH_BOOL ipu_is_buf_ready(ipu_channel_t channel, ipu_buffer_t buf_type, IH_U32 buf_num);
IH_S32 ipu_set_channel_planar_offsets(ipu_channel_t channel, 
                                      IH_U32 u_offset, IH_U32 v_offset);
IH_S32 ipu_set_channel_csc_rgb_yuv(ipu_channel_t channel, ipu_csc_rgb_yuv_t * csc);
IH_S32 ipu_set_channel_csc_yuv_rgb(ipu_channel_t channel, ipu_csc_yuv_rgb_t * csc);

IH_S32 ipu_alloc_channel_buffer(struct ih_buf_info *);
IH_S32 ipu_free_channel_buffer(struct ih_buf_info *);

IH_U32 ipu_alloc_mem(IH_U32 size);
void ipu_free_mem(IH_U32 mem_phys_addr, IH_U32 size);
IH_U32 ipu_map_mem(IH_U32 mem_phys_addr, IH_U32 size);
void ipu_unmap_mem(IH_U32 mem_virt_addr, IH_U32 size);

void * ihal_test_malloc(IH_U32 size);
void ihal_test_free(void * mem);

/* SDC API */
IH_S32 ipu_sdc_init_panel(ipu_panel_t panel, 
                          IH_U8 refreshRateHz,
                          IH_U16 width, IH_U16 height,
                          IH_U32 pixel_fmt, 
                          IH_U16 hStartWidth, IH_U16 hSyncWidth, IH_U16 hEndWidth, 
                          IH_U16 vStartWidth, IH_U16 vSyncWidth, IH_U16 vEndWidth,
                          ipu_di_signal_cfg_t sig, IH_U32 num_bus_cycles);
IH_S32 ipu_sdc_set_window_pos(ipu_channel_t channel, IH_S16 x_pos, IH_S16 y_pos);
IH_S32 ipu_sdc_set_global_alpha(IH_BOOL enable, IH_U8 alpha);
IH_S32 ipu_sdc_set_color_key(ipu_channel_t channel, IH_BOOL enable, IH_U32 colorKey);
IH_S32 ipu_sdc_set_graphics_window(ipu_channel_t channel);
IH_S32 ipu_sdc_enable_clk(IH_BOOL enable);
IH_S32 ipu_sdc_get_panel_fmt(ipu_channel_fmt_t *);

IH_S32 ipu_sdc_set_di_signals(ipu_di_signal_cfg_t sig);
IH_S32 ipu_sdc_get_di_signals(ipu_di_signal_cfg_t * p_sig);

/* ADC API */
IH_S32 ipu_adc_write_template(display_port_t disp, IH_U32 * pCmd, IH_BOOL write);

IH_S32 ipu_adc_set_update_mode(ipu_channel_t channel, ipu_adc_update_mode_t mode,
                               IH_U32 refresh_rate, 
                               IH_U32 addr, IH_U32 * size);

IH_S32 ipu_adc_write_cmd(display_port_t disp, cmddata_t type,
                          IH_U32 cmd, const IH_U32 * params, 
                          IH_U16 numParams);

IH_S32 ipu_adc_init_panel(display_port_t disp, 
                               IH_U16 width, IH_U16 height,
                               IH_U32 pixel_fmt,
                               IH_U32 stride,
                               ipu_adc_sig_cfg_t sig,
                               display_addressing_t addr,
                          IH_U32 vsync_width, vsync_t mode);

IH_S32 ipu_adc_init_ifc_timing(display_port_t disp, IH_BOOL read, 
                               IH_U32 cycle_time,
                               IH_U32 up_time,
                               IH_U32 down_time,
                               IH_U32 read_latch_time,
                               IH_U32 pixel_clk);

IH_S32 ipu_adc_set_ifc_width(display_port_t disp, IH_U32 pixel_fmt, IH_U16 width);
IH_S32 setup_ipu_access_memory_priority(void);
IH_S32 restore_ipu_access_memory_priority(void);
IH_U32 ipu_adc_read_cmd(display_port_t disp, IH_U32 cmd
				, IH_U8* ibuf, IH_U16 ilen);
IH_S32 ipu_adc_esd_ops(ipu_esd_ops_t op);
IH_S32 ipu_adc_flip_status();

/* DI API */
void ipu_di_set_pixel_fmt(display_port_t disp, IH_U32 pixel_fmt);

/* CMOS Sensor Interface API */
IH_S32 ipu_csi_init_interface(ipu_channel_fmt_t * chan_fmt,
                              ipu_csi_signal_cfg_t sig);

IH_S32 ipu_csi_get_channel_fmt(ipu_channel_fmt_t * chan_fmt);
IH_S32 ipu_csi_set_channel_fmt(ipu_channel_fmt_t * chan_fmt);

IH_S32 ipu_csi_enable_mclk(IH_BOOL flag);
IH_S32 ipu_csi_set_sensor_size(IH_S16 width, IH_S16 height);
IH_S32 ipu_csi_set_actual_size(IH_S16 width, IH_S16 height);
IH_S32 ipu_csi_set_mclk(IH_U32 *p_mclk_freq);
IH_S32 ipu_csi_set_window_pos(IH_U16 x_pos, IH_U16 y_pos);
IH_S32 ipu_csi_set_window_size(IH_U16 width, IH_U16 height);

IH_S32 ipu_write_reg(IH_U32 reg, IH_U32 val);
IH_S32 ipu_read_reg(IH_U32 reg, IH_U32 * val);


IH_S32 ipu_write_register(IH_U32 reg, IH_U32 val);
IH_S32 ipu_read_register(IH_U32 reg, IH_U32 * val);
IH_S32 ipu_dump_registers(IH_U32 reg_number, const IH_U32 *array);




IH_S32 ipu_reset_icen_atomic();
/* Interrupt API */
IH_S32 ipu_register_int_handler(ipu_irq_line_t irq_line, ipu_irq_hdlr_t irq_hdlr);
IH_S32 ipu_wait_for_int_event(ipu_irq_hdlr_t irq_hdlr); // receive notification from ISR
/* for camera component */
IH_S32 ipu_cam_wait_for_int_event(int * pirq, IH_U32 timeout); // receive notification from ISR for camera
IH_S32 ipu_cam_cancel_wait_for_int_event(void); //cancel ipu_cam_wait_for_int_event 
IH_S32 ipu_cam_request_irq(ipu_irq_line_t irq_line);
IH_S32 ipu_cam_free_irq(ipu_irq_line_t irq_line);

/* for VSYNC_EOF interrupt */
IH_S32 ipu_request_irq(ipu_irq_line_t irq_line);
IH_S32 ipu_free_irq(ipu_irq_line_t irq_line);
IH_S32 ipu_wait_for_int(ipu_irq_line_t irq_line, IH_U32 timeout); // receive notification from ISR

IH_S32 ipu_enable_int(ipu_irq_line_t irq_line);
IH_S32 ipu_disable_int(ipu_irq_line_t irq_line);

IH_BOOL ipu_is_int_set(ipu_irq_line_t irq_line, IH_BOOL clear_if_set);


/* for generic interrupt waiting */
IH_S32 ipu_generic_irq_request (ipu_irq_line_t line, IH_BOOL disable_irq);
IH_S32 ipu_generic_irq_free    (ipu_irq_line_t line);
IH_S32 ipu_generic_irq_wait    (ipu_irq_line_t line, IH_U32 timeout_ms, IH_U32 timewait_after_trigger_us, IH_U32 min_num_int_b4_trigger);
IH_S32 ipu_generic_irq_enable  (ipu_irq_line_t line);
IH_S32 ipu_generic_irq_disable (ipu_irq_line_t line);


/* test debug api only */
IH_S32 ipu_test_unselect_buffer(ipu_channel_t channel,
                         ipu_buffer_t type,
                         IH_U32 bufNum);
IH_S32 ipu_test_adc_enable(IH_BOOL enable);
IH_S32 ipu_test_di_enable(IH_BOOL enable);

IH_S32 ipu_disable_channel_no_delay(ipu_channel_t channel);
IH_S32 ipu_ic_set_global_alpha(IH_BOOL enable, IH_U8 alpha);
IH_S32 ipu_ic_set_color_key(ipu_channel_t channel, IH_BOOL enable, IH_U32 colorKey);

/*==================================================================================*/

#ifdef __cplusplus /* allow #include in a C++ file (only put this in .h files) */
}
#endif

#endif // _INCLUDE_IH_H_
