/* IHAL                                                                <ih_types.h> */
/*                                                                                  */
/* Copyright (c) 2006 Motorola, Inc.                                               */
/*                                                                                  */

/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 5-Jun-2006  Motorola    Initial revision.
 */

/*!
 * @defgroup IPU MXC Image Processing Unit (IPU) Helper Driver
 */
/*!
 * @file ih_types.h
 * 
 * @brief Type definitions exchanged between IHAL library and user.
 * 
 * @ingroup IPU
 */

#ifndef _INCLUDE_IH_TYPES_H_
#define _INCLUDE_IH_TYPES_H_

/*********************************** CONSTANTS **************************************/

#define IH_FALSE 0
#define IH_TRUE  1

/************************************* TYPES ****************************************/

typedef int            IH_BOOL;
typedef unsigned char  IH_U8;
typedef short          IH_S16;
typedef unsigned short IH_U16;
typedef long           IH_S32;
typedef unsigned long  IH_U32;

#endif // _INCLUDE_IH_TYPES_H_
