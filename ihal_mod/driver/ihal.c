/* IHAL                                                                    <ihal.c> */
/*                                                                                  */
/* Copyright (c) 2006-2008 Motorola, Inc.                                               */
/*                                                                                  */

/*
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */
/* Date         Author      Comment
 * ===========  ==========  ==================================================
 * 5-Jun-2006   Motorola    Initial revision.
 * 3-Jul-2007   Motorola    Compliance issue fix
 * 10-Jul-2007  Motorola    Get rid of wait while disable ipu channel
 * 19-Jul-2007  Motorola    Resume wait_for_stop in ipu_disable_channel
 * 24-Jul-2007  Motorola    Compliance issue fix
 * 15-Oct-2007  Motorola    Free IPU 3M reserved RAM
 * 21-Oct-2007  Motorola    Introduce the image resolution information into FIQ handler
 * 01-Nov-2007  Motorola    Update mem cache read mode
 * 15-Nov-2007  Motorola    Added support for ipu clk check
 * 15-Nov-2007  Motorola    Remove memory leak
 * 01-Dec-2007  Motorola    Flick GPIO_SIGNAL_SER_EN
 * 14-Dec-2007  Motorola    Remove ipu clk check due to phone reset issue introduced by ipu clk 
 * 28-Jan-2008  Motorola    one kernal_unlock is missed in ihal's code
 * 10-Mar-2008  Motorola    add three interface functions
 * 26-Feb-2008  Motorola    Fix default value for ioctl(read reg)
 * 17-Apr-2008  Motorola    Add function to set IC color key/alpha
 * 07-May-2008 Motorola    Handling IHAL_IOC_DISABLE_CHAN_NO_DELAY
 * 09-May-2008  Motorola    Add function to set DI signal polarity
 * 23-May-2008  Motorola    Added support for 6800 MCU interface bus to read registers (for Nevis)
 * 15-Jul-2008  Motorola    Added support for reading register for RENESAS LCD pannel 
 * 23-Jul-2008  Motorola    Add interface to reset IC_EN
 * 07-AUG-2008  Motorola    Add setup/restore ipu access memory priority function
 * 07-Aug-2008  Motorola    Fix OSS issue
 * 20-Aug-2008  Motorola    Add support for ESD issue 
 * 04-SEP-2008  Motorola    Add support for CSI position and size
 * 14-Nov-2008  Motorola    Add support for querying the status of flip 
 * 14-OCT-2008  Motorola    resotre ih_phys_size to 0 when fail in ihal_mmap.
 */

/*
 * @file ihal.c
 *
 * @brief MXC IPU helper driver
 *
 */
 
/*!
 * Include files
 */

#include <linux/config.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/errno.h>
#include <linux/string.h>
#include <linux/interrupt.h>
#include <linux/pagemap.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/ioport.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/mm.h>
#include <linux/mman.h>
#include <linux/proc_fs.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/dma-mapping.h>
#include <asm/hardware.h>
#include <asm/io.h>
#include <asm/mach-types.h>
#include <asm/uaccess.h>
#include <asm/arch/clock.h>
#include <linux/semaphore.h>
#include <asm/fiq.h>
#include <asm/page.h>
#include <asm/pgtable.h>

#include <asm/processor.h>
#include <asm/atomic.h>


#include <asm/mot-gpio.h>

#include <ipu.h>
#include <ipu_regs.h>
#include <ipu_prv.h>

#include <ihal.h>
#include <ihal_ioctl.h>

#ifdef CONFIG_MOT_FEAT_2MP_CAMERA_WRKARND 
#include <capture_2mp_wrkarnd.h>
#endif

#define IHAL_DEV_NAME_FMT_STR "ihal%d"
#define IHAL_DEV_NAME_FMT_STR_LEN 8

#define IHAL_MAX_NUM_ADC_WR_CMDS 512
#define IHAL_MAX_NUM_ADC_RD_CMDS (16*1024)
#define ADC_SYS1_CHN_TMO         (10)


struct semaphore pp_sema;

static irqreturn_t ipu_irq_event_handler(int irq, void *dev_id, struct pt_regs *regs);
static inline void ih_cam_pushq(int irq);

static uint32_t ih_phys_addr = 0, ih_phys_size = 0;

#define CAM_IRQ_NUM        1024 /* it should be a power of 2 */ 
#define IRQ_CAM_NONE       -1   /* no irq in camera interrupt queue */ 
#define IRQ_CAM_DEL        -1   /* irq have been freed */ 
#define IRQ_CAM_OVERFLOW   -2   /* for interrupt queue overflow flag */ 

typedef struct camera_int_queue
{
    unsigned short head;
    unsigned short tail;
    unsigned short len;
    bool overflow;
    spinlock_t s_lock;
    wait_queue_head_t wait_int;
    int irq_line[CAM_IRQ_NUM];
}camera_int_queue;

static camera_int_queue cam_int_q;

static spinlock_t disp_lla_lock = SPIN_LOCK_UNLOCKED;

/* delete irq status from queue */
static inline void ih_cam_delq(int irq)
{
    unsigned long flags;
    int irq_n = 0;

    spin_lock_irqsave(&cam_int_q.s_lock, flags);
    if(!cam_int_q.len)
    {
        spin_unlock_irqrestore(&cam_int_q.s_lock, flags);
        return;
    }

    while(irq_n < cam_int_q.len) /* */
    {
        if(irq == cam_int_q.irq_line[(cam_int_q.head + irq_n)&(CAM_IRQ_NUM - 1)]) 
            cam_int_q.irq_line[(cam_int_q.head + irq_n)&(CAM_IRQ_NUM - 1)] = IRQ_CAM_DEL;
         irq_n++;
    }

    spin_unlock_irqrestore(&cam_int_q.s_lock, flags);
}

/* initialize ipu interrupt queue for camera */
static void ih_cam_initq(void)
{
    cam_int_q.head = cam_int_q.tail = 0;
    cam_int_q.len = 0;
    cam_int_q.overflow = 0;
    cam_int_q.s_lock = SPIN_LOCK_UNLOCKED;
    init_waitqueue_head(&cam_int_q.wait_int);   
}

static inline void ih_cam_clearq(void)
{
    cam_int_q.head = cam_int_q.tail = 0;
    cam_int_q.len = 0;
    cam_int_q.overflow = 0;
}

/* get irq line from queue */
static inline int ih_cam_pullq(void)
{
    unsigned long flags;
    int irq = IRQ_CAM_DEL;

    spin_lock_irqsave(&cam_int_q.s_lock, flags);

    if(!cam_int_q.len)
    {
        spin_unlock_irqrestore(&cam_int_q.s_lock, flags);
        return IRQ_CAM_NONE;
    }
    else if(cam_int_q.overflow)
    {
        ih_cam_clearq();
        spin_unlock_irqrestore(&cam_int_q.s_lock, flags);
        return IRQ_CAM_OVERFLOW;
    }

    while( (irq == IRQ_CAM_DEL) && (cam_int_q.len > 0) )
    {
        irq = cam_int_q.irq_line[cam_int_q.head];
        cam_int_q.head++;
        cam_int_q.head &= CAM_IRQ_NUM - 1;
        cam_int_q.len--;
#ifdef CAM_INT_DBG 
        printk(" ================================== \n");
        printk(" pull - irq queue irq:%d \n", irq);
        printk(" pull - irq queue tail:%d \n", cam_int_q.tail);
        printk(" pull - irq queue head:%d \n", cam_int_q.head);
        printk(" pull - irq queue len:%d \n", cam_int_q.len);
#endif
    }

    spin_unlock_irqrestore(&cam_int_q.s_lock, flags);

    return irq;
}

/* put irq line into queue */
static inline void ih_cam_pushq(int irq)
{
    unsigned long flags;

    spin_lock_irqsave(&cam_int_q.s_lock, flags);

    if(cam_int_q.len < CAM_IRQ_NUM)
    {
        cam_int_q.irq_line[cam_int_q.tail] = irq;
        cam_int_q.tail++;
        cam_int_q.tail &= CAM_IRQ_NUM - 1;
        cam_int_q.len++;
#ifdef CAM_INT_DBG 
        printk(" ------------------------------ \n");
        printk(" push - irq queue irq:%d \n", irq);
        printk(" push - irq queue tail:%d \n", cam_int_q.tail);
        printk(" push - irq queue head:%d \n", cam_int_q.head);
        printk(" push - irq queue len:%d \n", cam_int_q.len);
#endif
    }
    else
    {
       cam_int_q.overflow = 1; 
    }
    spin_unlock_irqrestore(&cam_int_q.s_lock, flags);

    return;
}

static uint32_t          irq_c = 0; 

#define IHAL_CAPTURE_REUSE_MEM
#ifdef IHAL_CAPTURE_REUSE_MEM

/* ======================================= */
/* test for still capture reuse memory  */

extern unsigned long ipu_reserved_buff_fiq[1203];
extern int mxc_fiq_irq_switch(int vector, int irq2fiq);
extern void consistent_sync(void *vaddr, size_t size, int direction);

static IH_U32 user_malloc_addr = 0;
static struct page **test_pages = NULL;
static int nr_pages; 

static void * ihal_test_reuse_free(void);
static void test_buf_alloc(IH_U32 size)
{
    int err,i;

    nr_pages = (size + PAGE_SIZE - 1)/PAGE_SIZE; 

    test_pages = kmalloc(nr_pages * sizeof(struct page*),GFP_KERNEL);
    memset(test_pages, 0, nr_pages * sizeof(struct page *));

    if (NULL == test_pages)
    {
        printk(KERN_ALERT" IHAL.KO: ---------- test_buf_alloc kmalloc error!!! \n");
        return;
    }

    down_read(&current->mm->mmap_sem);
    err = get_user_pages(current,current->mm,
                         user_malloc_addr & PAGE_MASK, nr_pages,
			 1, 0, /* force */
			 test_pages, NULL);
    up_read(&current->mm->mmap_sem);
    if (err != nr_pages) 
    {
        printk("error ---- get_user_pages nr_pages:%d,  err:%d, call free\n",nr_pages, err);
        for(i=0; i<err; i++)
        {
           page_cache_release(test_pages[i]);
        }
        return; 
    }
    printk("ipu_reserved_buff_fiq=0x%x,nr_pages=%d\n",ipu_reserved_buff_fiq,nr_pages);
    for(i=0;i<nr_pages;i++)
    {
        ipu_reserved_buff_fiq[i] = __virt_to_phys((uint32_t)kmap(test_pages[i]));
        if(ipu_reserved_buff_fiq[i] == 0)
            printk(KERN_ALERT" test_buf_alloc pages error !!!\n"); 
    }
    ipu_reserved_buff_fiq[1200] = 2;//IDMA interrupt counter 
    ipu_reserved_buff_fiq[1201] = nr_pages + 2;//interrupts totally
    ipu_reserved_buff_fiq[1202] = 0;//flag for IDMA over
    return;
}

static void * ihal_test_reuse_free(void)
{
    int i = 0;


    printk(KERN_ALERT"ihal_test_reuse_free \n"); 

    for(i=0; i<nr_pages; i++)
    {
        kunmap(test_pages[i]);
        if (! PageReserved(test_pages[i]))
        {
            SetPageDirty(test_pages[i]);
        }
        page_cache_release(test_pages[i]);
        ipu_reserved_buff_fiq[i] = 0;
    }
    //ipu_reserved_buff_fiq[1200] = 2;//not clear for print log when debug
    ipu_reserved_buff_fiq[1201] = 0;
    ipu_reserved_buff_fiq[1202] = 0;

    kfree(test_pages); /* release the mess */
    test_pages = NULL;
    nr_pages = 0;

    return NULL;
}

static int32_t  lines_per_int;
static uint32_t shot_num = 0;

static void still_capture_test_init(void)
{

    uint32_t pixel_fmt;
    uint16_t width;
    uint16_t height;
    uint32_t stride;
    uint32_t tmp;

    printk(KERN_ALERT " ihal.ko: ===== still_capture_test_init \n");

    tmp = readl(CSI_ACT_FRM_SIZE);

    width = ((uint16_t)(tmp) + 1)>>2; 
    height = (uint16_t)(tmp>>16) + 1; 
    stride = width;
    pixel_fmt = IPU_PIX_FMT_GENERIC_32;
    lines_per_int = PAGE_SIZE/(width*4);

    /* count shot number */
    shot_num++;

    __raw_writel(0x80, IPU_INT_STAT_2); 
    /* change to use fiq handler*/
    
    ipu_init_channel_buffer(CSI_MEM, IPU_OUTPUT_BUFFER,
                            IPU_PIX_FMT_GENERIC_32, 
                            width,
                            lines_per_int, 
                            stride,
                            IPU_ROT_MODE(IPU_OUTPUT_BUFFER),
                            (void *)(ipu_reserved_buff_fiq[0]), (void *)(ipu_reserved_buff_fiq[1]));


    ipu_select_buffer(CSI_MEM, IPU_OUTPUT_BUFFER,0);
    ipu_select_buffer(CSI_MEM, IPU_OUTPUT_BUFFER,1);

}

#endif

/* ======================================= */

static uint32_t          cam_wakeup_f = 0; /* for wakeup type, 0: interrupt occurs, 1: wakeen up by ioctl */

/* cam irq handler */
static irqreturn_t ipu_irq_event_handler(int irq, void *dev_id, struct pt_regs *regs)
{
        ih_cam_pushq(irq); 
        irq_c = 1;
        wake_up_interruptible(&cam_int_q.wait_int);

        return IRQ_HANDLED;
}

/* ======================================= */

#ifdef CONFIG_MOT_FEAT_2MP_CAMERA_WRKARND 
/* to read state sdc fg channel */
static uint32_t sdc_fg_init_flag = 0; 

static inline bool ihal_is_fg_init(void)
{
    uint32_t reg;

    reg = __raw_readl(SDC_COM_CONF);
    if(reg & SDC_COM_FG_EN)
    {
        return true;
    }
    return false;
}
#endif

/* Turn the Display-Interface (DI) component off/on
 * to disable/enable pixel clock by writing to IPU_CONF
 * DI_EN is 7th bit in the least-significant byte of the 
 * IPU register
 * */
void ipu_disable_pixel_clock(void)
{
    uint32_t lock_flags;
    uint32_t pixel_clk;
    uint32_t dma_en;

    spin_lock_irqsave(&ipu_lock, lock_flags);

    pixel_clk = __raw_readl(IPU_CONF);
    pixel_clk &= ~IPU_CONF_DI_EN;

#ifndef NEVIS
    dma_en = __raw_readl(SDC_COM_CONF);
    dma_en &= ~SDC_COM_BG_EN;
    __raw_writel(dma_en, SDC_COM_CONF);
    wmb();
#endif
    
    __raw_writel(pixel_clk, IPU_CONF);

    spin_unlock_irqrestore(&ipu_lock, lock_flags);
}

void ipu_enable_pixel_clock(void)
{
    uint32_t lock_flags;
    uint32_t pixel_clk;
    uint32_t dma_en;

    spin_lock_irqsave(&ipu_lock, lock_flags);

    pixel_clk = __raw_readl(IPU_CONF);
    pixel_clk |= IPU_CONF_DI_EN;

#ifndef NEVIS
    dma_en = __raw_readl(SDC_COM_CONF);
    dma_en |= SDC_COM_BG_EN;
#endif
    
    __raw_writel(pixel_clk, IPU_CONF);

#ifndef NEVIS
    wmb();
    __raw_writel(dma_en, SDC_COM_CONF);
#endif

    spin_unlock_irqrestore(&ipu_lock, lock_flags);
}

/* ======================================= */
/* For VSYNC_EOF interrupt process */
static wait_queue_head_t vseof_irq_q;
static uint32_t          vseof_irq_cond = 0;

#ifdef NEVIS
static uint32_t          vseof_irq_set = 0;
#endif

/* VSYNC_EOF irq handler */
static irqreturn_t ipu_vseof_irq_handler(int irq, void *dev_id, struct pt_regs *regs)
{

#ifdef NEVIS
    if (vseof_irq_set)
    {
        vseof_irq_set = 0;
        ipu_enable_channel(ADC_SYS1);
 	 
    }
    else
    {
#endif
        vseof_irq_cond = 1;
        
        /* 
         * Reset serializer for recovery from an esd event
         * Reset must be done within display blanking period (2 pixel clock)
         * otherwise will incur display flicker 
         */
        ipu_disable_pixel_clock();
        
        /* 
         * Pull the serializer out of STBY. Must occur at least 
         * 20us after calling gpio_lcd_serializer_reset
         */
    
#ifndef CONFIG_MACH_XPIXL
        gpio_signal_set_data(GPIO_SIGNAL_SER_EN, GPIO_DATA_LOW);
#endif
        udelay(20);
#ifndef CONFIG_MACH_XPIXL
        gpio_signal_set_data(GPIO_SIGNAL_SER_EN, GPIO_DATA_HIGH); 
#endif
        udelay(50); 
        
        ipu_enable_pixel_clock();
        
        wake_up_interruptible(&vseof_irq_q);
#ifdef NEVIS
    }
    ipu_disable_irq(IPU_IRQ_ADC_DISP0_VSYNC);
#endif

    return IRQ_HANDLED;
}

/* ======================================= */

enum
{
    IHAL_DEV = 0,
    FIRST_IHAL_DEV = IHAL_DEV,
    NUM_IHAL_DEV
};

struct ihal_dev
{
    struct cdev cdev;
};

static struct ihal_dev ihal_devices[NUM_IHAL_DEV];


static wait_queue_head_t irq_q[num_ipu_irq_hdlr];
static uint32_t          irq_cond[num_ipu_irq_hdlr];

#ifdef THIS_IS_FOR_INT_TESTING_ONLY
static int fg_count = 0, event_fg_count = 0;

struct irq_hdlr_blit_info
{
    uint32_t inp_buf_num;
    uint32_t outp_buf_num;
    uint16_t evt_limit;
    uint16_t evt_count;
    uint32_t evt_irq;
    uint32_t src_irq;
    uint32_t blit_irq;
};

static struct irq_hdlr_blit_info dev_blit_info;

/************************************************************************************/

static
irqreturn_t ipu_SDC_FG_EOF_VT_int
(
    int irq, 
    void * dev_id, 
    struct pt_regs * regs
)
{
    if(fg_count++ > 21)
    {
        fg_count = 0;

        /*
          A frame of data from the imager is ready. However, defer updating the SDC BG 
          buffer until the SDC_BG_EOF interrupt occurs (i.e. when it's safe(er) to do so).
        */

        if(event_fg_count++ > 25)
        {
            event_fg_count = 0;
            irq_cond[0]++;
            wake_up_interruptible(&irq_q[0]);
        }
        else
        {
            /*
              Enable SDC_BG_EOF interrupt.
            */
            ipu_enable_irq(IPU_IRQ_SDC_BG_OUT_EOF);
        }
    }

    return IRQ_HANDLED;
}

/*==================================================================================*/

static irqreturn_t 
ipu_SDC_BG_EOF_VT_int
(
    int irq, 
    void * dev_id, 
    struct pt_regs * regs
)
{
    static int bufNum = 0;
    /*
      Now it's safe to transfer an image onto the SDC BG buffer. By using the
      Rotation Unit, we ensure that the transfer occurs as fast as possible.
    */
//    configure_GPIO_dbg2(1);
    /*
      Enable VF_ROT_IN_EOF interrupt.
      Set VF_ROT intput buffer 0 (VF_PRP) ready.
      Set VF_ROT output buffer 0 (SDC_BG) ready (Initiates complete transfer).
      Disable SDC_BG_EOF interrupt.
    */
    ipu_enable_irq(IPU_IRQ_PP_ROT_IN_EOF);

    ipu_select_buffer(MEM_ROT_PP_MEM, IPU_INPUT_BUFFER, bufNum);
    ipu_select_buffer(MEM_ROT_PP_MEM, IPU_OUTPUT_BUFFER, 0);

    ipu_disable_irq(IPU_IRQ_SDC_BG_OUT_EOF);

    bufNum ^= 1;
    return IRQ_HANDLED;
}

/*==================================================================================*/

static irqreturn_t
ipu_MEM_ROT_IN_PP_MEM_int
(
    int irq, 
    void * dev_id, 
    struct pt_regs * regs
)
{
    /*
      Set PRP_VF output buffer N ready.
      Disable VF_ROT_IN_EOF interrupt.
    */
//    ipu_select_buffer(CSI_PRP_VF_MEM, IPU_OUTPUT_BUFFER, 0);

    ipu_disable_irq(IPU_IRQ_PP_ROT_IN_EOF);

    return IRQ_HANDLED;
}

/************************************************************************************/

static irqreturn_t 
ipu_BLiT_data_ready_int
(
    int irq, 
    void * dev_id, 
    struct pt_regs * regs
)
{
    struct irq_hdlr_blit_info * blit_info;

    blit_info = (struct irq_hdlr_blit_info *)dev_id;
    
    if(blit_info->evt_count++ > blit_info->evt_limit) // dummy way to simulate an imager frame rate
    {
        ipu_disable_irq(irq);

        blit_info->evt_count = 0;

        /*
          Image data from some source is ready to BLit into the BG buffer. However, 
          defer updating the SDC BG buffer until the SDC_BG_EOF interrupt occurs 
          (i.e. when it's safe(er) to do so).
        */

        /*
          Enable SDC_BG_EOF interrupt.
        */
        ipu_enable_irq(blit_info->evt_irq);
    } // if

    return IRQ_HANDLED;
}

/*==================================================================================*/

static irqreturn_t 
ipu_BLiT_SDC_BG_EOF_int
(
    int irq, 
    void * dev_id, 
    struct pt_regs * regs
)
{
    /*
      Now it's safe to transfer an image onto the SDC BG buffer. By using the
      Rotation Unit, we ensure that the transfer occurs as fast as possible.
    */
    struct irq_hdlr_blit_info * blit_info;

    blit_info = (struct irq_hdlr_blit_info *)dev_id;

//    configure_GPIO_dbg2(1);
    /*
      Enable VF_ROT_IN_EOF interrupt.
      Set VF_ROT intput buffer 0 (VF_PRP) ready.
      Set VF_ROT output buffer 0 (SDC_BG) ready (Initiates complete transfer).
      Disable SDC_BG_EOF interrupt.
    */
    ipu_enable_irq(blit_info->blit_irq);

    ipu_select_buffer(MEM_ROT_PP_MEM, IPU_INPUT_BUFFER, blit_info->inp_buf_num);
    ipu_select_buffer(MEM_ROT_PP_MEM, IPU_OUTPUT_BUFFER, blit_info->outp_buf_num);

    ipu_disable_irq(irq);

    return IRQ_HANDLED;
}

/************************************************************************************/

static irqreturn_t
ipu_BLiT_MEM_ROT_IN_PP_MEM_int
(
    int irq, 
    void * dev_id, 
    struct pt_regs * regs
)
{
    struct irq_hdlr_blit_info * blit_info;

    blit_info = (struct irq_hdlr_blit_info *)dev_id;

    ipu_disable_irq(irq);

    ipu_enable_irq(blit_info->src_irq);

    return IRQ_HANDLED;
}

/*==================================================================================*/

static void init_dev_blit_info(ipu_irq_hdlr_t irq_hdlr)
{
    dev_blit_info.inp_buf_num  = 0;
    dev_blit_info.outp_buf_num = 0;
    dev_blit_info.evt_limit    = 2;
    dev_blit_info.evt_count    = 0;
    dev_blit_info.evt_irq      = IPU_IRQ_SDC_BG_OUT_EOF;
    dev_blit_info.src_irq      = IPU_IRQ_SDC_FG_OUT_EOF;
    dev_blit_info.blit_irq     = IPU_IRQ_PP_ROT_IN_EOF;
}

/*==================================================================================*/

static irqreturn_t (* irq_hdlr[num_ipu_irq_hdlr])(int, void *, struct pt_regs *) =
{
    ipu_SDC_FG_EOF_VT_int,
    ipu_SDC_BG_EOF_VT_int,
    ipu_MEM_ROT_IN_PP_MEM_int,
    ipu_BLiT_data_ready_int,
    ipu_BLiT_SDC_BG_EOF_int,
    ipu_BLiT_MEM_ROT_IN_PP_MEM_int,
    NULL
};

static void * irq_data[num_ipu_irq_hdlr] =
{
    NULL,
    NULL,
    NULL,
    &dev_blit_info,
    &dev_blit_info,
    &dev_blit_info,
    NULL
};

static void (* irq_data_init[num_ipu_irq_hdlr])(ipu_irq_hdlr_t) =
{
    NULL,
    NULL,
    NULL,
    init_dev_blit_info,
    NULL,
    NULL,
    NULL
};
#endif // THIS_IS_FOR_...

/* For generic interrupt handling*/
struct ih_generic_irq_entry
{
    volatile uint32_t     irq_cond;   // keeps track of number of times interrupt called
    wait_queue_head_t     irq_queue;
};

static struct ih_generic_irq_entry * gp_generic_irq_array[IPU_IRQ_COUNT] = {0};

/* Generic irq handler */
static irqreturn_t ipu_generic_irq_handler(int irq, void *dev_id, struct pt_regs *regs)
{
    struct ih_generic_irq_entry * p_entry = (struct ih_generic_irq_entry *) dev_id;

    if(p_entry != NULL)
    {
        p_entry->irq_cond++;
        wake_up_interruptible(&p_entry->irq_queue);
        DPRINTK("GENERIC_IRQ: Int on %d\n", irq);
    }
    else
    {
        DPRINTK("GENERIC_IRQ: NULL dev_id\n");
    }

    return IRQ_HANDLED;
}







static int ic_reset()
{
    unsigned long ipu_conf_reg = 0;
    int warnning_flag= 0;

    ipu_conf_reg = readl(IPU_CONF);

    /* test if IC_EN is set */
    if(ipu_conf_reg&0x00000002)
    {
        /* clear */
        ipu_conf_reg &= ~0x00000002;
        /* write back to clear */
        writel(ipu_conf_reg, IPU_CONF);
        /* read agagin to verify write successfully */
        ipu_conf_reg = readl(IPU_CONF);

        if(ipu_conf_reg&0x00000002)
        {    /* failed to write back to clear */
            warnning_flag = 2;
        }
        else
        {   /* reset */
            ipu_conf_reg |= 0x00000002;
            writel(ipu_conf_reg, IPU_CONF);
            warnning_flag = 0;
        }
    }
    else
    {   /*don't need to reset*/
        warnning_flag = 1;
    }

    return warnning_flag;
}

/*raw*/
static unsigned long check_CSI_MEM_READY()
{
    unsigned long return_value=0;
    /* active || pause */
    if((0x0000000C&readl(IPU_TASKS_STAT))==0x00000004)
        return_value |= 0x00000001;
    return return_value;
}
/*vf*/
static unsigned long check_CSI_PRP_VF_MEM_READY()
{

    unsigned long return_value=0;

    if((0x00000600&readl(IPU_TASKS_STAT))==0x00000200)
        return_value |= 0x00000010;
    return return_value;

}
/*preview*/
static unsigned long check_MEM_PRP_VF_MEM_READY()
{
    unsigned long return_value=0;
    /* enc and vf active;                       vf active*/
    if(((0x00000070&readl(IPU_TASKS_STAT))==0x00000010)||((0x00000030&readl(IPU_TASKS_STAT))==0x00000030))
        return_value |= 0x00000100;
    return return_value;

}
/*thumbnail*/
static unsigned long check_MEM_PRP_ENC_MEM_READY()
{
    unsigned long return_value=0;
    /* enc and vf active;                       enc active */
    if(((0x00000070&readl(IPU_TASKS_STAT))==0x00000010) || ((0x00000070&readl(IPU_TASKS_STAT))==0x00000020))
        return_value |= 0x00001000;
    return return_value;
}
/*PP*/
static unsigned long check_MEM_PP_MEM_READY()
{
    unsigned long return_value=0;
    /* active,                        pause */
    if((0x00001800&readl(IPU_TASKS_STAT))==0x00000800)
        return_value |= 0x00010000;
    return return_value;
}

/*ROT_PP*/
static unsigned long check_MEM_ROT_PP_MEM_READY()
{
    unsigned long return_value=0;
    /* active */
    if((0x00300000&readl(IPU_TASKS_STAT))==0x00100000)
        return_value |= 0x00100000;
    return return_value;
}


static int wait_for_ipu_ic_free_block()
{

    unsigned long flags = 0;
    unsigned long ipu_conf_reg=0;
    unsigned long ready_result=0;
    int return_value = 0;
    int i;

    if(down_interruptible(&pp_sema))
        panic("IHAL:%d down_interruptible failed\n", __LINE__);

    for(i=0;;i++)
    {
        ready_result = 0;

        spin_lock_irqsave(&ipu_lock, flags);
        ready_result = check_CSI_PRP_VF_MEM_READY() | check_MEM_PRP_VF_MEM_READY()
                     | check_MEM_PRP_ENC_MEM_READY() | check_CSI_MEM_READY()
                     | check_MEM_PP_MEM_READY() | check_MEM_ROT_PP_MEM_READY();

        if(0 == ready_result)
        {
            return_value = ic_reset();
            spin_unlock_irqrestore(&ipu_lock, flags);
            printk("\n\nreset ic_en!\n");
#ifdef DUMP_IPU_REGISTER
            printk("IPU_CHA_BUF0_RDY    = 0x%08x\n", readl(IPU_CHA_BUF0_RDY));
            printk("IPU_CHA_BUF1_RDY    = 0x%08x\n", readl(IPU_CHA_BUF1_RDY));
            printk("IDMAC_CHA_BUSY      = 0x%08x\n", readl(IDMAC_CHA_BUSY));
            printk("IPU_TASKS_STAT      = 0x%08x\n", readl(IPU_TASKS_STAT));
            printk("IPU_CONF            = 0x%08x\n", readl(IPU_CONF));
            printk("IC_CONF             = 0x%08x\n", readl(IC_CONF));
            printk("IPU_CHA_CUR_BUF     = 0x%08x\n", readl(IPU_CHA_CUR_BUF));
            printk("IPU_CHA_DB_MODE_SEL = 0x%08x\n", readl(IPU_CHA_DB_MODE_SEL));
            printk("IDMAC_CHA_EN        = 0x%08x\n", readl(IDMAC_CHA_EN));
            printk("IPU_INT_STAT_1      = 0x%08x\n", readl(IPU_INT_STAT_1));
            printk("IPU_INT_STAT_2      = 0x%08x\n", readl(IPU_INT_STAT_2));
            printk("IPU_INT_STAT_3      = 0x%08x\n", readl(IPU_INT_STAT_3));
            printk("IPU_INT_STAT_4      = 0x%08x\n", readl(IPU_INT_STAT_4));
            printk("IPU_INT_STAT_5      = 0x%08x\n", readl(IPU_INT_STAT_5));
            printk("IDMAC_CHA_PRI       = 0x%08X\n", readl(IDMAC_CHA_PRI));
            printk("IDMAC_CONF          = 0x%08X\n", readl(IDMAC_CONF));
#endif
            up(&pp_sema);
            return return_value;
        }
        else
        {
            spin_unlock_irqrestore(&ipu_lock, flags);
            if( 0 == (i%100) )
            {
                printk("%s()%d - waited %d ms!!! 0x%08x\n", __func__, __LINE__, i*5, return_value);
#ifdef DUMP_IPU_REGISTER
                printk("IPU_CHA_BUF0_RDY    = 0x%08x\n", readl(IPU_CHA_BUF0_RDY));
                printk("IPU_CHA_BUF1_RDY    = 0x%08x\n", readl(IPU_CHA_BUF1_RDY));
                printk("IDMAC_CHA_BUSY      = 0x%08x\n", readl(IDMAC_CHA_BUSY));
                printk("IPU_TASKS_STAT      = 0x%08x\n", readl(IPU_TASKS_STAT));
                printk("IPU_CONF            = 0x%08x\n", readl(IPU_CONF));
                printk("IC_CONF             = 0x%08x\n", readl(IC_CONF));
                printk("IPU_CHA_CUR_BUF     = 0x%08x\n", readl(IPU_CHA_CUR_BUF));
                printk("IPU_CHA_DB_MODE_SEL = 0x%08x\n", readl(IPU_CHA_DB_MODE_SEL));
                printk("IDMAC_CHA_EN        = 0x%08x\n", readl(IDMAC_CHA_EN));
                printk("IPU_INT_STAT_1      = 0x%08x\n", readl(IPU_INT_STAT_1));
                printk("IPU_INT_STAT_2      = 0x%08x\n", readl(IPU_INT_STAT_2));
                printk("IPU_INT_STAT_3      = 0x%08x\n", readl(IPU_INT_STAT_3));
                printk("IPU_INT_STAT_4      = 0x%08x\n", readl(IPU_INT_STAT_4));
                printk("IPU_INT_STAT_5      = 0x%08x\n", readl(IPU_INT_STAT_5));
                printk("IDMAC_CHA_PRI       = 0x%08X\n", readl(IDMAC_CHA_PRI));
                printk("IDMAC_CONF          = 0x%08X\n", readl(IDMAC_CONF));
#endif 
            }
            msleep(5);
            continue;
        }
    }
}


/*==================================================================================*/

__inline static uint32_t 
channel_2_dma(ipu_channel_t ch, ipu_buffer_t type) 
{
    return ((IPU_BUF_TYPE(type)==IPU_INPUT_BUFFER) ? ((uint32_t)ch & 0xFF) : \
           ((IPU_BUF_TYPE(type)==IPU_OUTPUT_BUFFER) ? (((uint32_t)ch>>8) & 0xFF) : \
           (((uint32_t)ch>>16) & 0xFF)));
}

/*==================================================================================*/
static IH_S32 get_adc_pannel_read_len(IH_U32 cmd)
{
    struct ih_reg_rw pannel_reg_len_pair[] = {
	{3,  0x04}, {4, 0x09}, {1, 0x0a}, {1, 0x0b}, 
	{1,  0x0c}, {1, 0x0d}, {1, 0x0e}, {1, 0x0f},
	{0,  0x2e}, {1, 0xb9}, {2, 0xc4}, {4, 0xc5},
	{4,  0xc6}, {4, 0xc7}, {1, 0xc8}, {1, 0xc9}, 
	{1,  0xca}, {1, 0xcb}, {3, 0xd0}, {3, 0xd1},
	{3,  0xd2}, {4, 0xd4}, {4, 0xd5}, {4, 0xd6},
	{4,  0xd7}, {1, 0xda}, {1, 0xdb}, {1, 0xdc},
	{3,  0xe3}, {2, 0xe4}, {2, 0xe5}, {1, 0xe6},
	{2,  0xe8}, {2, 0xec}, {1, 0xf4}
    };
       
    IH_S32 i, j, ret;

    ret = -1;
    j = sizeof(pannel_reg_len_pair)/sizeof(struct ih_reg_rw);
    for(i=0; i< j; i++)
    {
        if (pannel_reg_len_pair[i].reg == cmd) 
	{
	    ret = pannel_reg_len_pair[i].val;	
	    break;
	}
    }
    return ret;
}

/*==================================================================================*/
static int _ipu_is_channel_busy(ipu_channel_t arg)
{
    uint32_t sec_dma;
    uint32_t in_dma;
    uint32_t out_dma;
    uint32_t chan_mask = 0;

    out_dma = channel_2_dma(arg, IPU_OUTPUT_BUFFER);
    if(out_dma != CHAN_NONE)
        chan_mask = 1UL << out_dma;
    in_dma = channel_2_dma(arg, IPU_INPUT_BUFFER);
    if(in_dma != CHAN_NONE)
        chan_mask |= 1UL << in_dma;
    sec_dma = channel_2_dma(arg, IPU_SEC_INPUT_BUFFER);
    if(sec_dma != CHAN_NONE)
        chan_mask |= 1UL << sec_dma;
    return ((readl(IDMAC_CHA_BUSY) & chan_mask) ||
            (readl(IPU_CHA_BUF0_RDY) & chan_mask) ||
            (readl(IPU_CHA_BUF1_RDY) & chan_mask));
 	
}

/*==================================================================================*/

static int
ihal_open
(
    struct inode * inode, 
    struct file * file
)
{
    struct ihal_dev * dev; /* device information */

    dev = container_of(inode->i_cdev, struct ihal_dev, cdev);
    file->private_data = dev; /* for other methods */

    return 0;
}

/*==================================================================================*/

static int 
ihal_ioctl
(
    struct inode * inode, 
    struct file * file, 
    unsigned int cmd,
    unsigned long arg
)
{
    int ret_code = 0;
    static uint32_t gChannelInitMask = 0;

    switch(cmd)
    {
        case IHAL_IOC_GET_VERSION: // get IHAL version
            return IHAL_VERSION;
        break;

        case IHAL_IOC_INIT_CHAN: // init_channel
        {
            struct ih_init_chan_info ch_info;
            ih_channel_params_t  ih_params;
            ipu_channel_params_t ch_params, * pChInfo;

            if(copy_from_user(&ch_info, (void *)arg, sizeof(ch_info)))
            {
                return -EFAULT;
            }

            if(ch_info.params != NULL)
            {
                if(copy_from_user(&ih_params, (void *)ch_info.params, sizeof(ih_params)))
                {
                    return -EFAULT;
                }

                pChInfo = &ch_params;

                switch(ch_info.channel)
                {
                    case CSI_PRP_VF_MEM:
                    case MEM_PRP_VF_MEM:

                        ch_params.mem_pp_mem.graphics_combine_en = ih_params.specific.csi_prp_vf_mem.graphics_combine_en;
                    case CSI_PRP_ENC_MEM:
                    case MEM_PRP_ENC_MEM:
                    case MEM_ROT_ENC_MEM:
                        ch_params.csi_prp_enc_mem.in_width      = ih_params.in.width;
                        ch_params.csi_prp_enc_mem.in_height     = ih_params.in.height;
                        ch_params.csi_prp_enc_mem.in_pixel_fmt  = ih_params.in.pixel_fmt;
                        ch_params.csi_prp_enc_mem.out_width     = ih_params.out.width;
                        ch_params.csi_prp_enc_mem.out_height    = ih_params.out.height;
                        ch_params.csi_prp_enc_mem.out_pixel_fmt = ih_params.out.pixel_fmt;
                    break;

                    case MEM_PP_MEM:
                        ch_params.mem_pp_mem.graphics_combine_en = ih_params.specific.mem_pp_mem.graphics_combine_en;
                        ch_params.mem_pp_mem.global_alpha_en = ih_params.specific.mem_pp_mem.global_alpha_en;
                        ch_params.mem_pp_mem.key_color_en = ih_params.specific.mem_pp_mem.key_color_en; 
                        ch_params.mem_pp_mem.in_width = ih_params.in.width;
                        ch_params.mem_pp_mem.in_height = ih_params.in.height;
                        ch_params.mem_pp_mem.in_pixel_fmt  = ih_params.in.pixel_fmt;
                        ch_params.mem_pp_mem.out_width     = ih_params.out.width;
                        ch_params.mem_pp_mem.out_height    = ih_params.out.height;
                        ch_params.mem_pp_mem.out_pixel_fmt = ih_params.out.pixel_fmt;            
                    break;

                    case MEM_PF_Y_MEM:
                        ch_params.mem_pf_mem.operation      = ih_params.specific.mem_pf_mem.operation;
                        if(ih_params.specific.mem_pf_mem.h264_pause_row)
                            ipu_pf_set_pause_row(ih_params.specific.mem_pf_mem.h264_pause_row);
                    break;

                    /* enable IDMA support used by CLI viewfinder */
                    case ADC_SYS1:  
                        ch_params.adc_sys1.disp     = ih_params.specific.adc_sys.disp;
                        ch_params.adc_sys1.ch_mode  = ih_params.specific.adc_sys.ch_mode;
                        ch_params.adc_sys1.out_left = ih_params.specific.adc_sys.x_pos;
                        ch_params.adc_sys1.out_top  = ih_params.specific.adc_sys.y_pos;
#ifdef NEVIS
                        ipu_request_irq(IPU_IRQ_ADC_DISP0_VSYNC, ipu_vseof_irq_handler, 0, NULL, NULL);
#endif
                    break;

                    case ADC_SYS2:
                        ch_params.adc_sys2.disp     = ih_params.specific.adc_sys.disp;
                        ch_params.adc_sys2.ch_mode  = ih_params.specific.adc_sys.ch_mode;
                        ch_params.adc_sys2.out_left = ih_params.specific.adc_sys.x_pos;
                        ch_params.adc_sys2.out_top  = ih_params.specific.adc_sys.y_pos;
                    break;

                    default:
                        pChInfo = NULL;
                    break;

                } // switch

            }
            else
            {
                pChInfo = NULL;
            }

            gChannelInitMask |= 1L << IPU_CHAN_ID(ch_info.channel);
            ret_code = ipu_init_channel(ch_info.channel, pChInfo);
        }
        break;

        case IHAL_IOC_IS_INIT_CHAN: // query_channel_init_status
            return gChannelInitMask & (1L << IPU_CHAN_ID((ipu_channel_t)arg));
        break;
        
        case IHAL_IOC_UNINIT_CHAN: // uninit_channel
            gChannelInitMask &= ~(1L << IPU_CHAN_ID((ipu_channel_t)arg));
            
#ifdef NEVIS
            if ((ADC_SYS1 == (ipu_channel_t)arg))
            {
                ipu_clear_irq(IPU_IRQ_ADC_DISP0_VSYNC);
                ipu_free_irq(IPU_IRQ_ADC_DISP0_VSYNC, NULL);
            }
            else
#endif
            ipu_uninit_channel((ipu_channel_t)arg);
        break;
        
        case IHAL_IOC_INIT_CHAN_BUF: // init_channel_buffer
        {
            struct ih_init_chan_buff_info ch_info;

            if (copy_from_user(&ch_info, (void *)arg, sizeof(ch_info)))
            {
                return -EFAULT;
            }
#ifndef IHAL_IPU_RESERVED_MEM /*for nevis using reserved memory*/
#ifdef IHAL_CAPTURE_REUSE_MEM
            if (ch_info.channel == CSI_MEM)
            {
                return 0;
            }
#endif
#endif
            ret_code = ipu_init_channel_buffer(ch_info.channel, IPU_BUF_TYPE(ch_info.type),
                                               ch_info.chan_fmt.pixel_fmt, 
                                               ch_info.chan_fmt.width, 
                                               ch_info.chan_fmt.height, ch_info.stride,
                                               IPU_ROT_MODE(ch_info.type),
                                               ch_info.phyaddr_0, ch_info.phyaddr_1);
        }
        break;

        case IHAL_IOC_ENABLE_CHAN: // enable_channel
        {
            if(CSI_MEM == (ipu_channel_t)arg)
            {
#ifdef CONFIG_MOT_FEAT_2MP_CAMERA_WRKARND
                /* setup workarounds */
                max_slave0_disable();
                enable_gem_clock();
                gem_setup();
                enable_rtic_clock();
                rtic_setup();
                max_rtic_incr_setup();

                /* enable workarounds */
                enable_fake_sdram();
                start_gem();
                start_rtic();
                setup_dma_chan_priority();

                if(ihal_is_fg_init()) 
                {
                    sdc_fg_init_flag = 1;
                    ipu_sdc_fg_uninit();
                }
#endif 
#ifndef IHAL_IPU_RESERVED_MEM /*for nevis using reserved memory*/               
#ifdef IHAL_CAPTURE_REUSE_MEM
                still_capture_test_init();
#endif
#endif
            }
           
#ifdef NEVIS
            if (ADC_SYS1 ==  (ipu_channel_t)arg)
            {
                vseof_irq_set = 1;
                ipu_enable_irq(IPU_IRQ_ADC_DISP0_VSYNC);
            }
            else
#endif
                ret_code = ipu_enable_channel((ipu_channel_t)arg);
        }
        break;

        case IHAL_IOC_DISABLE_CHAN: // disable_channel
        {
#ifdef CONFIG_MOT_FEAT_2MP_CAMERA_WRKARND 
            if(CSI_MEM == (ipu_channel_t)arg)
            {
                /* disable workaround */
                restore_max_configuration();
                stop_gem();
                disable_gem_clock();
                stop_rtic();
                disable_rtic_clock();
                disable_fake_sdram();
                max_slave0_restore();
                if(sdc_fg_init_flag)
                {  
                    ipu_sdc_fg_init();
                    sdc_fg_init_flag = 0;
                }
                restore_dma_chan_priority();
            }
#endif
            ret_code = ipu_disable_channel((ipu_channel_t)arg, 1);
        }
        break;

#ifdef CONFIG_MACH_XPIXL
        case IHAL_IOC_DISABLE_CHAN_NO_DELAY: // disable_channel with no delay
        {
            ret_code = ipu_disable_channel((ipu_channel_t)arg, 0);
        }
        break;
#endif
        
        case IHAL_IOC_SELECT_BUF: // select_buffer
        {
            struct ih_sel_buff_info sel_buff_info;

            if(copy_from_user(&sel_buff_info, (void *)arg, sizeof(sel_buff_info)))
            {
                return -EFAULT;
            }
#ifndef IHAL_IPU_RESERVED_MEM /*for nevis using reserved memory*/
#ifdef IHAL_CAPTURE_REUSE_MEM
            if(sel_buff_info.channel == CSI_MEM)
            {
                return 0;
            }
#endif
#endif
            

            if(down_interruptible(&pp_sema))
                panic("IHAL:%d down_interruptible failed\n", __LINE__);

            ret_code = ipu_select_buffer(sel_buff_info.channel, sel_buff_info.type,
                                         sel_buff_info.bufNum);
            up(&pp_sema);

        }
        break;

        case IHAL_IOC_UPDATE_CHAN_BUF: // update_channel_buffer
        {
            struct ih_upd_chan_buff_info upd_buff_info;

            if(copy_from_user(&upd_buff_info, (void *)arg, sizeof(upd_buff_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_update_channel_buffer(upd_buff_info.channel, upd_buff_info.type,
                                                 upd_buff_info.bufNum, upd_buff_info.phyaddr);
        }
        break;

        case IHAL_IOC_UPDATE_YUV_CHAN_BUF: // update_channel_yuvoffset_buffer
        {
            struct ih_upd_chan_yuvoffset_buff_info upd_yuv_buff_info;

            if(copy_from_user(&upd_yuv_buff_info, (void *)arg, sizeof(upd_yuv_buff_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_update_channel_yuvoffset_buffer(upd_yuv_buff_info.channel, upd_yuv_buff_info.type,
                                                 upd_yuv_buff_info.u_offset, upd_yuv_buff_info.v_offset);
        }
        break;

        case IHAL_IOC_LINK_CHAN: // link_channels
        {
            struct ih_link_channels_info link_info;

            if(copy_from_user(&link_info, (void *)arg, sizeof(link_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_link_channels(link_info.src_ch, link_info.dest_ch);
        }
        break;

        case IHAL_IOC_UNLINK_CHAN: // unlink_channels
        {
            struct ih_link_channels_info link_info;

            if(copy_from_user(&link_info, (void *)arg, sizeof(link_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_unlink_channels(link_info.src_ch, link_info.dest_ch);
        }
        break;

        case IHAL_IOC_IS_CHAN_BUSY: // is_chan_busy
        {

            ret_code = _ipu_is_channel_busy(arg);
        }
        break;

        case IHAL_IOC_IS_BUF_READY: // is_chan_busy
        {
            uint32_t dma_chan;
            uint32_t dma_mask = 0;
            struct ih_buf_ready_info buf_ready_info;

            if(copy_from_user(&buf_ready_info, (void *)arg, sizeof(buf_ready_info)))
            {
                return -EFAULT;
            }

            dma_chan = channel_2_dma(buf_ready_info.chan, buf_ready_info.buf_type);
            if(dma_chan != CHAN_NONE)
                dma_mask = 1UL << dma_chan;
            if(buf_ready_info.buf_num == 0)
                ret_code = readl(IPU_CHA_BUF0_RDY) & dma_mask;
            else
                ret_code = readl(IPU_CHA_BUF1_RDY) & dma_mask;
        }
        break;

        case IHAL_IOC_SDC_INIT_PANEL: // sdc_init_panel
        {
            struct ih_sdc_init_panel_info sdc_init_pnl_info;

            if(copy_from_user(&sdc_init_pnl_info, (void *)arg, sizeof(sdc_init_pnl_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_sdc_init_panel(sdc_init_pnl_info.panel, sdc_init_pnl_info.refreshRateHz, 
                                          sdc_init_pnl_info.width, sdc_init_pnl_info.height, 
                                          sdc_init_pnl_info.pixel_fmt, sdc_init_pnl_info.hStartWidth, 
                                          sdc_init_pnl_info.hSyncWidth, sdc_init_pnl_info.hEndWidth, 
                                          sdc_init_pnl_info.vStartWidth, sdc_init_pnl_info.vSyncWidth, 
                                          sdc_init_pnl_info.vEndWidth, sdc_init_pnl_info.sig);
        }
        break;

        case IHAL_IOC_SDC_SET_WINDOW_POS: // sdc_set_window_pos
        {
            struct ih_sdc_set_window_pos_info sdc_set_wdw_pos_info;

            if(copy_from_user(&sdc_set_wdw_pos_info, (void *)arg, sizeof(sdc_set_wdw_pos_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_sdc_set_window_pos(sdc_set_wdw_pos_info.channel, 
                                              sdc_set_wdw_pos_info.x_pos, 
                                              sdc_set_wdw_pos_info.y_pos);
        }
        break;

        case IHAL_IOC_SDC_SET_GLOBAL_ALPHA: // sdc_set_global_alpha
            ret_code = ipu_sdc_set_global_alpha(arg >> 31, arg & 0xff);
        break;

        case IHAL_IOC_SDC_SET_COLOR_KEY: // sdc_set_color_key
        {
            struct ih_sdc_set_color_key_info ck_info;

            if(copy_from_user(&ck_info, (void *)arg, sizeof(ck_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_sdc_set_color_key(ck_info.channel, ck_info.enable, ck_info.colorKey);
        }
        break;

        case IHAL_IOC_IC_SET_GLOBAL_ALPHA: // sdc_set_global_alpha
             if(arg >> 31)
             {
                 printk("global alpha enable\n");
                 writel((readl(IC_CONF) | IC_CONF_IC_GLB_LOC_A), IC_CONF);
                 writel(((readl(IC_CMBP_1) & ~0x0000FF00) | ((arg & 0x000000FF)<<8)), IC_CMBP_1);
             }
             else
             {
                 printk("Local alpha enable\n");
                 writel((readl(IC_CONF) & ~IC_CONF_IC_GLB_LOC_A), IC_CONF); 
                 writel((readl(IC_CONF)), IC_CONF);
             }   
        break;

        case IHAL_IOC_IC_SET_COLOR_KEY: // sdc_set_color_key
        {
            struct ih_sdc_set_color_key_info ck_info;

            if(copy_from_user(&ck_info, (void *)arg, sizeof(ck_info)))
            {
                return -EFAULT;
            }
            if(ck_info.enable)
            {
                printk("color key enable\n");
                writel((ck_info.colorKey & 0x00FFFFFFL), IC_CMBP_2); 
                writel((readl(IC_CONF) | IC_CONF_KEY_COLOR_EN), IC_CONF);          
            }
            else
            {
                printk("color key disable\n");
                writel((readl(IC_CONF) & ~ (IC_CONF_KEY_COLOR_EN)), IC_CONF);
            }            
        }
        break;

        case IHAL_IOC_SDC_SET_GRAPHIC_WINDOW: // sdc_set_graphics_window
        {
            uint32_t val = readl(SDC_COM_CONF);

            if(arg == MEM_SDC_BG)
            {
                val &= ~0x00000020;
            }
            else
            {
                if(arg == MEM_SDC_FG)
                {
                    val |= 0x00000020;
                }
            }

            writel(val, SDC_COM_CONF);
        }
        break;

        case IHAL_IOC_SDC_ENABLE_CLK: // sdc_enable_clk
        {
            uint32_t val = readl(DI_DISP_IF_CONF);

            if(arg)
            {
                val &= ~0x06000000;
            }
            else
            {
                val |= 0x06000000;
            }

            writel(val, DI_DISP_IF_CONF);
        }
        break;

        case IHAL_IOC_SDC_SET_DI_SIGNALS:
        {
            ipu_di_signal_cfg_t sig;
            uint32_t val;
            uint32_t lock_flags;

            if(copy_from_user(&sig, (void *)arg, sizeof(ipu_di_signal_cfg_t)))
            {
                return -EFAULT;
            }

            spin_lock_irqsave(&ipu_lock, lock_flags);

            /* pixel clock bits */
            val = readl(DI_DISP_IF_CONF) & 0xF8FFFFFF;
            val |= sig.datamask_en << DI_D3_DATAMSK_SHIFT |
                   sig.clksel_en << DI_D3_CLK_SEL_SHIFT |
                   sig.clkidle_en << DI_D3_CLK_IDLE_SHIFT;
            writel(val, DI_DISP_IF_CONF);

            /* update the signal polarities */
            val = readl(DI_DISP_SIG_POL) & 0xE0FFFFE7;
            val |= sig.data_pol   << DI_D3_DATA_POL_SHIFT |
                   sig.clk_pol    << DI_D3_CLK_POL_SHIFT |
                   sig.enable_pol << DI_D3_DRDY_SHARP_POL_SHIFT |
                   sig.Hsync_pol  << DI_D3_HSYNC_POL_SHIFT |
                   sig.Vsync_pol  << DI_D3_VSYNC_POL_SHIFT |
                   sig.read_pol   << DI_D0_READ_POL_SHIFT |
                   sig.write_pol  << DI_D0_WRITE_POL_SHIFT;
            writel(val, DI_DISP_SIG_POL);

            spin_unlock_irqrestore(&ipu_lock, lock_flags);
        }
        break;

        case IHAL_IOC_SDC_GET_DI_SIGNALS:
        {
            ipu_di_signal_cfg_t sig;
            uint32_t val;

            /* pixel clock bits */
            val = readl(DI_DISP_IF_CONF);
            sig.datamask_en = (val >> DI_D3_DATAMSK_SHIFT)  & 1;
            sig.clksel_en   = (val >> DI_D3_CLK_SEL_SHIFT)  & 1;
            sig.clkidle_en  = (val >> DI_D3_CLK_IDLE_SHIFT) & 1;

            /* update the signal polarities */
            val = readl(DI_DISP_SIG_POL);
            sig.data_pol   = (val >> DI_D3_DATA_POL_SHIFT)      & 1;
            sig.clk_pol    = (val >> DI_D3_CLK_POL_SHIFT)       & 1;
            sig.enable_pol = (val >> DI_D3_DRDY_SHARP_POL_SHIFT)& 1;
            sig.Hsync_pol  = (val >> DI_D3_HSYNC_POL_SHIFT)     & 1;
            sig.Vsync_pol  = (val >> DI_D3_VSYNC_POL_SHIFT)     & 1;

            if(copy_to_user((void *)arg, &sig, sizeof(ipu_di_signal_cfg_t)))
            {
                return -EFAULT;
            }
        }
        break;
        /* reset IC_EN in IPU_CONF */
        case IHAL_IOC_RESET_ICEN_ATOMIC:
        {
            printk("ihal: %d in reset \n", __LINE__);
            wait_for_ipu_ic_free_block();
            printk("ihal: %d out reset \n", __LINE__);

        }
        break;
        case IHAL_IOC_ADC_INIT_PANEL: // adc_init_panel
        {
            struct ih_adc_init_panel_info adc_init_pnl_info;

            if(copy_from_user(&adc_init_pnl_info, (void *)arg, sizeof(adc_init_pnl_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_adc_init_panel(adc_init_pnl_info.disp, adc_init_pnl_info.width, 
                                          adc_init_pnl_info.height, 
                                          adc_init_pnl_info.pixel_fmt, 
                                          adc_init_pnl_info.stride, 
                                          adc_init_pnl_info.sig,
                                          adc_init_pnl_info.addr,
                                          adc_init_pnl_info.vsync_width, 
                                          adc_init_pnl_info.mode);
        }
        break;

        case IHAL_IOC_ADC_INIT_IFC_TIMING: // adc_init_ifc_timing
        {
            struct ih_adc_init_ifc_timing_info adc_init_ifc_info;

            if(copy_from_user(&adc_init_ifc_info, (void *)arg, sizeof(adc_init_ifc_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_adc_init_ifc_timing(adc_init_ifc_info.disp, adc_init_ifc_info.read, 
                                               adc_init_ifc_info.cycle_time,
                                               adc_init_ifc_info.up_time, 
                                               adc_init_ifc_info.down_time,
                                               adc_init_ifc_info.read_latch_time,
                                               adc_init_ifc_info.pixel_clk);
        }
        break;

        case IHAL_IOC_ADC_WRITE_CMD: // adc_write_cmd
        {
            IH_U32 params[IHAL_MAX_NUM_ADC_WR_CMDS];
            struct ih_adc_write_cmd_info adc_write_cmd_info;

            if(copy_from_user(&adc_write_cmd_info, (void *)arg, sizeof(adc_write_cmd_info)))
            {
                return -EFAULT;
            }

            if(adc_write_cmd_info.num_params > IHAL_MAX_NUM_ADC_WR_CMDS)
            {
                printk(KERN_ALERT "IHAL: Too many params for adc_write_cmd to handle\n");
                return -EFAULT;
            }

            if(adc_write_cmd_info.params != NULL && adc_write_cmd_info.num_params > 0)
            {
                if(copy_from_user(params, (void *)adc_write_cmd_info.params, 
                                  adc_write_cmd_info.num_params * sizeof(IH_U32)))
                {
                    return -EFAULT;
                }
            }

	    //printk(KERN_ALERT "IHAL: IHAL_IOC_ADC_WRITE_CMD: cmd=0x%x\n", adc_write_cmd_info.cmd);
            spin_lock(&disp_lla_lock);
	    
            ret_code = ipu_adc_write_cmd(adc_write_cmd_info.disp, adc_write_cmd_info.type,
                                         adc_write_cmd_info.cmd, params, 
                                         adc_write_cmd_info.num_params);

            spin_unlock(&disp_lla_lock);
        }
        break;
        case IHAL_IOC_SETUP_IPU_ACCESS_MEMORY_PRIORITY: //
        {
            ret_code = setup_ipu_access_memory_priority();
        }
        break;
        case IHAL_IOC_RESTORE_IPU_ACCESS_MEMORY_PRIORITY: //
        {
            ret_code = restore_ipu_access_memory_priority();
        }
        break;
        case IHAL_IOC_ADC_WRITE_TEMPLATE: // adc_write_template
        {
            IH_U32 cmd[TEMPLATE_BUF_SIZE];
            struct ih_adc_write_template_info adc_write_template_info;

            if(copy_from_user(&adc_write_template_info, (void *)arg, sizeof(adc_write_template_info)))
            {
                return -EFAULT;
            }

            if(adc_write_template_info.pCmd != NULL)
            {
                if(copy_from_user(cmd, (void *)adc_write_template_info.pCmd, 
                                  sizeof(cmd)))
                {
                    return -EFAULT;
                }
            }

            ret_code = ipu_adc_write_template(adc_write_template_info.disp, cmd, 
                                              adc_write_template_info.write);
        }
        break;

        case IHAL_IOC_ADC_IFC_WIDTH_CMD:
        {
            struct ih_adc_set_ifc_width_info adc_set_ifc_width;

            if(copy_from_user(&adc_set_ifc_width, (void *)arg, sizeof(adc_set_ifc_width)))
            {
                return -EFAULT;
            }

            ret_code = ipu_adc_set_ifc_width(adc_set_ifc_width.disp, 
                                            adc_set_ifc_width.pixel_fmt, 
                                            adc_set_ifc_width.ifc_width);
        }
        break;

        case IHAL_IOC_ADC_READ_CMD:
        {
            struct ih_adc_read_cmd_info read_cmd_info;
	    IH_S32 i, len;
	    IH_U8 tmp[16], *p;
	    volatile IH_U32 cmd_conf, dat_conf;
            
	    i = len = 0;
	    if (copy_from_user(&read_cmd_info, (void *)arg, sizeof(read_cmd_info)))
            {
                printk(KERN_ALERT "!copy_from_user error!\n");
                return -EFAULT;
            }
            len = get_adc_pannel_read_len(read_cmd_info.cmd);
	    if ((read_cmd_info.ibuf == NULL) || (len < 0) || (read_cmd_info.ilen < 0) || (read_cmd_info.ilen < len))
            {
                printk(KERN_ALERT "!buffer is NULL or buffer len error!\n");
                return -EFAULT;
            }
	    if (len == 0) 
	    {	    
                len = read_cmd_info.ilen;
	        if (len > IHAL_MAX_NUM_ADC_RD_CMDS)
                {
                    printk(KERN_ALERT "!buffer len too max!\n");
                    return -EFAULT;
                }
	        p = (IH_U8 *) kmalloc(len, GFP_KERNEL);	
		if (p == NULL)
		{
		    printk(KERN_ALERT "!kmalloc  error!\n");	
		    return -ENOMEM;
		}
	    }
	    else
            {
	        p = tmp;	    
            }

	    spin_lock(&disp_lla_lock); 
            if (_ipu_is_channel_busy(ADC_SYS1))
            {	
                spin_unlock(&disp_lla_lock);    
                //test channel ADC_SYS1 is busy or not
                while (_ipu_is_channel_busy(ADC_SYS1))
                {
                    msleep(1);
		    if (++i > ADC_SYS1_CHN_TMO)
		    {
                        printk(KERN_ALERT "!ADC_SYS1 wait NOT busy timeout!\n");
		        return -EFAULT;
		    }
	        }
		spin_lock(&disp_lla_lock);
	    }
	    //printk(KERN_ALERT "!cmd=0x%x, len=%d, i=%d!\n", read_cmd_info.cmd, len, i);
	    
	    cmd_conf = (IH_U32) ((read_cmd_info.disp << 1) | 0x10); // disp num and cmd mode
	    writel(cmd_conf, DI_DISP_LLA_CONF);
            
	    writel(0x0, DI_DISP_LLA_DATA); //write NOP cmd
	    writel(read_cmd_info.cmd, DI_DISP_LLA_DATA);
            
	    dat_conf = cmd_conf | 0x1; // data mode
	    writel(dat_conf, DI_DISP_LLA_CONF);

	    //dummy read first
	    readl(DI_DISP_LLA_DATA);
	    rmb();
	    readl(DI_DISP_LLA_DATA);
	    rmb();

	    //read real value
            for (i = 0; i < len; i++)
            {
                p[i] = (IH_U8)(0xff & readl(DI_DISP_LLA_DATA));
		rmb();
            }
            //bring signal E back to low
            writel(0x08, DI_DISP_SIG_POL);
            writel(cmd_conf, DI_DISP_LLA_CONF);
            writel(0x0, DI_DISP_LLA_DATA);
            writel(0x18, DI_DISP_SIG_POL);

	    spin_unlock(&disp_lla_lock); 
	    
	    copy_to_user((void *)(read_cmd_info.ibuf), (void*)p, len * sizeof(IH_U8));
	    if (p != tmp)
	    {
                kfree(p);		    
	    }
	    ret_code = len;
        }
        break;

        case IHAL_IOC_ADC_ESD_OPS:
	{
            IH_S32 op;
	    
            if ( copy_from_user(&op, (void *)arg, sizeof(op)) )
            {   
                return -EFAULT;
            }
	    //printk(KERN_ALERT "!IHAL_IOC_ADC_ESD_OPS: op=%d!\n", op);
	    switch (op)
            {
                case 0: //ESD_OPS_DISABLE_PIXEL_CLOCK:
                    ipu_disable_pixel_clock();
                    break;

                case 1: //ESD_OPS_ENABLE_PIXEL_CLOCK:
                    ipu_enable_pixel_clock();
                    break;

                case 2: //ESD_OPS_SER_EN_LOW:
                    gpio_signal_set_data(GPIO_SIGNAL_SER_EN, GPIO_DATA_LOW);
                    break;

                case 3: //ESD_OPS_SER_EN_HIGH:
                    gpio_signal_set_data(GPIO_SIGNAL_SER_EN, GPIO_DATA_HIGH);
                    break;

                case 4: //ESD_OPS_SER_RST_LOW:
                    gpio_signal_set_data(GPIO_SIGNAL_SER_RST_B, GPIO_DATA_LOW);
                    break;

                case 5: //ESD_OPS_SER_RST_HIGH:
                    gpio_signal_set_data(GPIO_SIGNAL_SER_RST_B, GPIO_DATA_HIGH);
                    break;

                case 6: //ESD_OPS_DISP_RST_LOW:
                    gpio_signal_set_data(GPIO_SIGNAL_DISP_RST_B, GPIO_DATA_LOW);
                    break;

                case 7: //ESD_OPS_DISP_RST_HIGH:
                    gpio_signal_set_data(GPIO_SIGNAL_DISP_RST_B, GPIO_DATA_HIGH);
                    break;

                case 8: //ESD_OPS_DISP_RD_POL_LOW:
                    writel(0x08, DI_DISP_SIG_POL);
                    break;
		    
                case 9: //ESD_OPS_DISP_RD_POL_HIGH:
                    writel(0x18, DI_DISP_SIG_POL);
                    break;

                default:
                    printk(KERN_ALERT "!IHAL_IOC_ADC_ESD_OPS: error, undefined op=%d!\n", op);
                    return -EFAULT;
            }
        }
        break;
	
        case IHAL_IOC_ADC_FLIP_STATUS:
	{
	    IH_U32 st;	
	    
            gpio_signal_get_data(GPIO_SIGNAL_FLIP_OPEN, &st);    		
	    ret_code = st;
	}
	break;
	
#ifdef THIS_IS_FOR_INT_TESTING_ONLY
        case IHAL_IOC_REG_INT_HDLR: // register interrupt handler
        {
            struct ih_irq_reg_info irq_reg;

            if(copy_from_user(&irq_reg, (void *)arg, sizeof(irq_reg)))
            {
                return -EFAULT;
            }

            if(irq_hdlr[irq_reg.irq_hdlr] == NULL)
            {
                ipu_disable_irq(irq_reg.irq_line);
                ipu_free_irq(irq_reg.irq_line, irq_data[irq_reg.irq_hdlr]);
            }
            else
            {
                if(irq_data_init[irq_reg.irq_hdlr] != NULL)
                {
                    irq_data_init[irq_reg.irq_hdlr](irq_reg.irq_hdlr);
                }

                ipu_request_irq(irq_reg.irq_line, irq_hdlr[irq_reg.irq_hdlr], 0, 
                                NULL, irq_data[irq_reg.irq_hdlr]);
                ipu_disable_irq(irq_reg.irq_line);
            }
        }
        break;
#endif

        case IHAL_IOC_ENABLE_IRQ: // enable interrupt
            ipu_enable_irq(arg);
        break;

        case IHAL_IOC_DISABLE_IRQ: // disable interrupt
            ipu_disable_irq(arg);
        break;

        case IHAL_IOC_CAM_REQUEST_IRQ:
        {
            ipu_clear_irq(arg);
            ret_code = ipu_request_irq(arg, ipu_irq_event_handler, 0, NULL, NULL);
            if(ret_code)
            {
                printk(KERN_ALERT " ihal.ko: ipu_request_irq IPU_IRQ_SDC_BG_EOF error \n");
            }
        }
        break;

        case IHAL_IOC_CAM_FREE_IRQ:
        {
            ipu_free_irq(arg, NULL);
            ih_cam_delq(arg); 
        }
        break;

        case IHAL_IOC_WAIT_FOR_IRQ: // wait for interrupt
            if(wait_event_interruptible(irq_q[arg], (irq_cond[arg] != 0)))
            {
                return -ERESTARTSYS;
            }

            irq_cond[arg] = 0;
        break;

/* ======================================================================= */

        case IHAL_IOC_REQUEST_IRQ:
        {

#ifndef NEVIS
            ipu_clear_irq(arg);
            DPRINTK(KERN_ALERT " ihal.ko: ++++++++++++++++++++ request irq:%d\n",(int)arg);
            ret_code = ipu_request_irq(arg, ipu_vseof_irq_handler, 0, NULL, NULL);
            if(ret_code)
            {
                printk(KERN_ALERT " ihal.ko: ipu_request_irq error \n");
#else
            DPRINTK(KERN_ALERT " ihal.ko: ++++++++++++++++++++ request irq:%d\n",(int)arg);
            ipu_enable_irq(arg);
#endif
            }
        }
        break;

        case IHAL_IOC_FREE_IRQ:
        {
            DPRINTK(KERN_ALERT " ihal.ko: ++++++++++++++++++++ free irq:%d\n",(int)arg);
            ipu_clear_irq(arg);
            ipu_free_irq(arg, NULL);
        }
        break;

        case IHAL_IOC_WAIT_FOR_IPU_IRQ: // wait for interrupt
        {
            struct ih_irq_info ipu_irq_info;

            if(copy_from_user(&ipu_irq_info, (void *)arg, sizeof(ipu_irq_info)))
            {
                return -EFAULT;
            }

            DPRINTK(KERN_ALERT " ihal.ko: ++++++++++++++++++++ wait irq:%d, timeout:%d\n",(int)ipu_irq_info.line, (int)ipu_irq_info.timeout);
            ret_code = wait_event_interruptible_timeout(vseof_irq_q, vseof_irq_cond, (ipu_irq_info.timeout * HZ)/1000);
            vseof_irq_cond = 0;
            if (!ret_code)
            {
            	printk(" @@@@ wait ipu int timeout\n");
            	return ret_code;
            }
            else if (signal_pending(current))
            {
            	printk(" @@@@ wait ipu int interrupted by signal\n");
            	return -ERESTARTSYS;
            }
            return ret_code;
        }
        break;
/* ======================================================================= */

        case IHAL_IOC_CAM_WAIT_FOR_IRQ: // wait for interrupt
        {
            struct ih_irq_info irq_info;
            int irq_line_n;
            int ret;

            if(copy_from_user(&irq_info, (void *)arg, sizeof(irq_info)))
            {
                return -EFAULT;
            }

            if(IRQ_CAM_NONE == (irq_line_n = ih_cam_pullq()))
            {
                ret = wait_event_interruptible_timeout(cam_int_q.wait_int, irq_c, (irq_info.timeout * HZ)/1000);
                irq_c = 0;
                if (!ret)
                {
                    return -ETIME;
                }
                else if (signal_pending(current))
                {
                    printk(" @@@@ wait event interrupted by signal\n");
                    return -ERESTARTSYS;
                }
                if(cam_wakeup_f)
                {
                    cam_wakeup_f = 0;
                    return -ECANCELED;
                }
                else
                {
                    irq_line_n = ih_cam_pullq();
                }
            }

            if(irq_line_n == IRQ_CAM_OVERFLOW)
            {
                printk(" @@@@ cam_int_q queue overflow!! \n");
                return -EOVERFLOW; 
            }            

            if(copy_to_user((void *)arg, &irq_line_n, sizeof(int)))
            {
                return -EFAULT;
            }
        }
        break;

        case IHAL_IOC_CAM_WAKEUP_WAIT_IRQ: //camera wakeup wait int 
        {
            cam_wakeup_f = 1;
            irq_c = 1;
            wake_up_interruptible(&cam_int_q.wait_int);
        }
        break;

        case IHAL_IOC_GET_IRQ_STS: // get irq status
        {
            uint32_t irq_line = arg & 0x7FFFFFFF;
            uint32_t irq_sts = readl(IPU_INT_STAT_1 + ((irq_line >> 5) << 2));

            ret_code = (irq_sts & (1 << (irq_line % 32))) ? IH_TRUE : IH_FALSE;

            if(ret_code && (arg & 0x80000000)) // Are we instructed to clear the status bit?
            {
                writel(irq_sts & (1 << (irq_line % 32)), IPU_INT_STAT_1 + ((irq_line >> 5) << 2));
            }
        }
        break;

        case IHAL_IOC_CSI_SET_MCLK_RATE: // csi_init_interface
        {
            uint32_t mclk_freq;
            uint32_t div;
            
            if(copy_from_user(&mclk_freq, (void *)arg, sizeof(uint32_t)))
            {
                return -EFAULT;
            }

            // Calculate the divider using the requested, minimum mclk freq
            div = (mxc_get_clocks_parent(CSI_BAUD) *2) / mclk_freq;
            /* Calculate and return the actual mclk frequency.
               The integer division error/truncation will ensure the actual freq is
               greater than the requested freq.
            */
            if (mclk_freq < (mxc_get_clocks_parent(CSI_BAUD) *2) / div) {
                    div++;
            }
            mclk_freq = (mxc_get_clocks_parent(CSI_BAUD) * 2)/ div;
            mxc_set_clocks_div(CSI_BAUD, div);

            if(copy_to_user((void *)arg, &mclk_freq, sizeof(uint32_t)))
            {
                return -EFAULT;
            }
        }
        break;
        case IHAL_IOC_CSI_INIT_INTERFACE: // csi_init_interface
        {
            struct ih_csi_init_if_info csi_info;

            if(copy_from_user(&csi_info, (void *)arg, sizeof(csi_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_csi_init_interface(csi_info.chan_fmt.width-1, csi_info.chan_fmt.height-1,
                                              csi_info.chan_fmt.pixel_fmt,csi_info.sig);
        }
        break;

        case IHAL_IOC_CSI_ENABLE_MCLK: // csi_enable_mclk
        {   
            ret_code = ipu_csi_enable_mclk(CSI_MCLK_VF|CSI_MCLK_ENC|CSI_MCLK_RAW|CSI_MCLK_I2C, (bool)arg, 1);
        }
        break;

        case IHAL_IOC_CSI_SET_ACTUAL_SIZE: // csi_set_act_size
            writel(arg, CSI_ACT_FRM_SIZE);
        break;

        case IHAL_IOC_CSI_SET_SENSOR_SIZE: // csi_set_sens_size
            writel(arg, CSI_SENS_FRM_SIZE);
        break;

        case IHAL_IOC_CSI_SET_WINDOW_POS: // csi_set_window_pos
        {
            struct ih_csi_set_window_pos_info csi_set_wdw_pos_info;

            if(copy_from_user(&csi_set_wdw_pos_info, (void *)arg, sizeof(csi_set_wdw_pos_info)))
            {
                return -EFAULT;
            }

            ipu_csi_set_window_pos(csi_set_wdw_pos_info.x_pos,
                                   csi_set_wdw_pos_info.y_pos);
        }
        break;

        case IHAL_IOC_CSI_SET_WINDOW_SIZE: // csi_set_window_size
        {
            struct ih_csi_set_window_size_info csi_set_wdw_size_info;

            if(copy_from_user(&csi_set_wdw_size_info, (void *)arg, sizeof(csi_set_wdw_size_info)))
            {
                return -EFAULT;
            }

            ipu_csi_set_window_size(csi_set_wdw_size_info.width,
                                    csi_set_wdw_size_info.height);
        }
        break;

        case IHAL_IOC_WRITE_IPU_REG: // write ipu reg
        {
            struct ih_reg_rw reg_w;

            if (copy_from_user(&reg_w, (void *)arg, sizeof(reg_w)))
            {
                return -EFAULT;
            }

            if(reg_w.reg <= DI_DISP_LLA_DATA)
            {
                writel(reg_w.val, IPU_REG_BASE + reg_w.reg);
            }
        }
        break;

        case IHAL_IOC_READ_IPU_REG: // write ipu reg
        {
            struct ih_reg_rw reg_r;
            if ((ipu_reserved_buff_fiq[1202]) == (unsigned long)1)
            {
                printk("FIQ reported IDMA transfer over in IHAL\n");

                int i = 0;
                for(i = 0; i < nr_pages; i++)
                {
                    consistent_sync(__va(ipu_reserved_buff_fiq[i]),PAGE_SIZE,DMA_FROM_DEVICE);
                }

                printk("sync vaddr for cache coherence\n");

                ipu_free_irq(IPU_IRQ_SENSOR_OUT_EOF,NULL);
                ih_cam_pushq(IPU_IRQ_SENSOR_OUT_EOF);
                irq_c = 1;
                wake_up_interruptible(&cam_int_q.wait_int); // notify omx camera EOF comming
                printk("FIQ reported IDMA over in IHAL\n");
                if (copy_from_user(&reg_r, (void *)arg, sizeof(reg_r)))
                {
                    printk("FIQ: Copy from user failed!\n");
                    return -EFAULT;
                }

                reg_r.val =0x40;//workaround,ask omx Eof thread to exit immediately

                copy_to_user((void *)arg, &reg_r, sizeof(reg_r));
                printk("FIQ: Copy from user and to user succeed!\n");
            }
            else
            {
                if (copy_from_user(&reg_r, (void *)arg, sizeof(reg_r)))
                {
                   return -EFAULT;
                }
                reg_r.val =0x00;
                copy_to_user((void *)arg, &reg_r, sizeof(reg_r));
            }
        }
        break;
#ifdef DUMP_IPU_REGISTER
        case IHAL_IOC_WRITE_IPU_REGISTER:
        {
            struct ih_reg_rw reg_w;

            /* get arguments */
            if (copy_from_user(&reg_w, (void *)arg, sizeof(reg_w)))
            {
                printk("ihal:%d write register, copy from user error\n", __LINE__);
                return -EFAULT;
            }




            /* arguments varification */
            if((IPU_REG_BASE<=reg_w.reg) && (reg_w.reg<=DI_DISP_LLA_DATA))
            {
                 writel(reg_w.val, reg_w.reg);

            }
            else
            {
                printk("ihal:%d invalid register! 0x%x\n", __LINE__, reg_w.reg);
                printk("ihal:%d correct range should be 0x%x - 0x%x \n", __LINE__, IPU_REG_BASE, DI_DISP_LLA_DATA);
                return -EFAULT;
            }

        }
        break;



        case IHAL_IOC_READ_IPU_REGISTER:
        {
            struct ih_reg_rw reg_w;

            /* get arguments */
            if (copy_from_user(&reg_w, (void *)arg, sizeof(reg_w)))
            {
                printk("ihal:%d read register, copy from user error\n", __LINE__);
                return -EFAULT;
            }




            /* arguments varification */
            if((IPU_REG_BASE<=reg_w.reg) && (reg_w.reg<=DI_DISP_LLA_DATA))
            {
                reg_w.val = readl(reg_w.reg);

            }
            else
            {
                printk("ihal:%d invalid register! 0x%x\n", __LINE__, reg_w.reg);
                printk("ihal:%d correct range should be 0x%x - 0x%x \n", __LINE__, IPU_REG_BASE, DI_DISP_LLA_DATA);
                return -EFAULT;
            }

            /* send result */
            if (copy_to_user((void *)arg, &reg_w,  sizeof(reg_w)))
            {
                printk("ihal:%d read register, copy to user error\n", __LINE__);
                return -EFAULT;
            }

        }
        break;



        case IHAL_IOC_DUMP_IPU_REGISTER:
        {
            struct ih_register_dump reg_dump;
            unsigned int reg_number;
            unsigned int * reg_dump_mem;
            int i;

            /* get arguments */
            if (copy_from_user(&reg_dump, (void *)arg, sizeof(reg_dump)))
            {
                printk("ihal:%d dump register, copy from user error\n", __LINE__);
                return -EFAULT;
            }

            if(NULL == reg_dump.array)
            {
                printk("ihal:%d dump register, mem invalid\n", __LINE__);
                return -EFAULT;
            }

            /* get number */
            reg_number = reg_dump.number;
            if (reg_number>112)
            {
                printk("ihal:%d dumpe too many registers, num=0x%x\n", __LINE__, reg_dump.number);
                reg_number = 112;
            }

            /* get kernel dump mem */
            reg_dump_mem = kmalloc(reg_number*4, GFP_KERNEL);
            if(NULL == reg_dump_mem)
            {
                printk("ihal:%d dump register, kmalloc failed\n", __LINE__);
                return -EFAULT;
            }

            /* dump registers */
            for(i=0; i<=reg_number; i++)
            {
                *(reg_dump_mem+i) = readl(IPU_REG_BASE  + (4*i) );

            }

            /* send dump result */
            if (copy_to_user(reg_dump.array, reg_dump_mem, reg_number*4))
            {
                printk("ihal:%d read register, copy to user error\n", __LINE__);
                return -EFAULT;
            }

            kfree(reg_dump_mem);

        }
        break;
#endif
        case IHAL_IOC_ALLOC_BUFFER: // alloc buffer
        {
            struct ih_buf_info buf_info;

            if(copy_from_user(&buf_info, (void *)arg, sizeof(buf_info)))
            {
                return -EFAULT;
            }

            if(0 == ih_phys_addr || 0 == ih_phys_size)
            {
                printk(KERN_ALERT "!ih_phys_addr not assigned!\n");
                return -EFAULT;
            }

            buf_info.phys_addr  = ih_phys_addr;
            buf_info.size       = ih_phys_size;

            /* create an entry in /proc/iomem */
            request_mem_region(ih_phys_addr, ih_phys_size, "IHAL-alloc"); 

            ih_phys_addr = ih_phys_size = 0;

            copy_to_user((void *)arg, &buf_info, sizeof(buf_info));
        }
        break;

        case IHAL_IOC_FREE_BUFFER: // free buffer
        {
            struct ih_buf_info buf_info;

            if(copy_from_user(&buf_info, (void *)arg, sizeof(buf_info)))
            {
                return -EFAULT;
            }

            ipu_free(buf_info.phys_addr);

            /* remove our entry in /proc/iomem */
            release_mem_region(buf_info.phys_addr, buf_info.size);
        }
        break;

#ifdef IHAL_CAPTURE_REUSE_MEM
/* for IPU memory allocation test */
        case IHAL_IOC_TEST_ALLOC: // 
        {
            struct ih_buf_info buf_info1;
            
            if(copy_from_user(&buf_info1, (void *)arg, sizeof(buf_info1)))
            {
                return -EFAULT;
            }
      
            user_malloc_addr = buf_info1.virt_addr;
            test_buf_alloc(buf_info1.size);

            ih_phys_addr = ih_phys_size = 0;

        }
        break;

        case IHAL_IOC_TEST_FREE: //
        {
            ihal_test_reuse_free();
        }
        break;
#endif

        case IHAL_IOC_ALLOC_MEM: // alloc phys addr
        {
            struct ih_buf_info buf_info;

            if(copy_from_user(&buf_info, (void *)arg, sizeof(buf_info)))
            {
                return -EFAULT;
            }

            if((buf_info.phys_addr = ipu_malloc(buf_info.size)) == 0)
            {
                printk("ihal: ipu_malloc() failed\n");
                return -EINVAL;
            }

            /* create an entry in /proc/iomem */
            request_mem_region(buf_info.phys_addr, buf_info.size, "IHAL-alloc"); 

            copy_to_user((void *)arg, &buf_info, sizeof(buf_info));
        }
        break;

        case IHAL_IOC_MAP_MEM: // map phys addr
        {
            struct ih_buf_info buf_info;

            if(copy_from_user(&buf_info, (void *)arg, sizeof(buf_info)))
            {
                return -EFAULT;
            }

            ih_phys_addr = buf_info.phys_addr;
            ih_phys_size = buf_info.size;
        }
        break;

        case IHAL_IOC_GENERIC_REQUEST_IRQ:
        {
            /* re-entrancy possible in this function */
            struct ih_generic_request_irq_info info;
            struct ih_generic_irq_entry * p_entry;

            if(copy_from_user(&info, (void *)arg, sizeof(struct ih_generic_request_irq_info)))
            {
                return -EFAULT;
            }

            /* sanity checks */
            if(info.line >= IPU_IRQ_COUNT)
            {
                return -EFAULT;
            }

            DPRINTK(KERN_ALERT "GENERIC_IRQ: got irq request for:%d\n",(int)info.line);

            /* Check if irq has been requested */
            if(gp_generic_irq_array[info.line] != NULL)
            {
                printk(KERN_ALERT "GENERIC_IRQ: gp_generic_irq_array[%d] is not NULL!!\n", (int)info.line);
                return -EFAULT;
            }

            /* Allocate memory for the structure */
            //allocate and populate p_entry
            p_entry = (struct ih_generic_irq_entry *) kmalloc(sizeof(struct ih_generic_irq_entry),GFP_KERNEL);
            p_entry->irq_cond = 0;
            init_waitqueue_head(&p_entry->irq_queue);

    
            /* register for interrupt */
            ipu_clear_irq(info.line);
            ret_code = ipu_request_irq(info.line, ipu_generic_irq_handler, 0, "ihal_generic", p_entry);
            if(ret_code == 0)
            {
                DPRINTK(KERN_ALERT "GENERIC_IRQ: ipu_request_irq successful \n");
                if(info.disable_irq)
                {
                    ipu_disable_irq(info.line);
                }

                /* update our array */
                gp_generic_irq_array[info.line] = p_entry;
            }
            else
            {
                printk(KERN_ALERT "GENERIC_IRQ: ipu_request_irq error \n");
                //Do cleanup
                kfree(p_entry);
            }
        }
        break;

        case IHAL_IOC_GENERIC_FREE_IRQ:
        {
            /* sanity checks */
            if(arg >= IPU_IRQ_COUNT)
            {
                return -EFAULT;
            }

            if(gp_generic_irq_array[arg] == NULL)
            {
                printk(KERN_ALERT "GENERIC_IRQ: free irq failed, irq %d was not requested\n", (int)arg);
                return -EFAULT;
            }

            ipu_free_irq(arg, gp_generic_irq_array[arg]);

            kfree(gp_generic_irq_array[arg]);
            gp_generic_irq_array[arg] = NULL;
        }
        break;

        case IHAL_IOC_GENERIC_WAIT_FOR_IRQ:
        {
            struct ih_generic_wait_irq_info info;
            struct ih_generic_irq_entry * p_entry;

            if(copy_from_user(&info, (void *)arg, sizeof(struct ih_generic_wait_irq_info)))
            {
                return -EFAULT;
            }

            if(info.line >= IPU_IRQ_COUNT)
            {
                return -EFAULT;
            }

            DPRINTK(KERN_ALERT "GENERIC_IRQ: wait irq:%d, timeout:%d (ms)\n",(int)info.line, (int)info.timeout_ms);

            if(gp_generic_irq_array[info.line] == NULL)
            {
                printk(KERN_ALERT "GENERIC_IRQ: wait for irq failed, irq %d was not requested\n", (int)info.line);
                return -EFAULT;
            }
            p_entry = gp_generic_irq_array[info.line];
            ret_code = wait_event_interruptible_timeout(p_entry->irq_queue, (p_entry->irq_cond >= info.min_num_int_b4_trigger), (info.timeout_ms * HZ)/1000);
            DPRINTK(KERN_ALERT "GENERIC_IRQ: return from wait irq:%d, triggered %d, ret=%d\n",(int)info.line, (int)p_entry->irq_cond, (int)ret_code);

            if (ret_code == 0)
            {
                //timed-out
    	        printk("GENERIC_IRQ: @@@@ wait ipu int timeout\n");
    	        return -ETIME;
            }
            else if (ret_code == -ERESTARTSYS)
            {
                //interrupted by signal
    	        printk("GENERIC_IRQ: @@@@ wait ipu int interrupted by signal\n");
    	        return -ERESTARTSYS;
            }
            else
            {
                //success, so clear the count to zero
                p_entry->irq_cond = 0;

                //wait for some time if user specified
                if(info.timewait_after_trigger_us > 0)
                {
                    if(info.timewait_after_trigger_us <= 200)
                    {
                        udelay(info.timewait_after_trigger_us);
                    }
                    else
                    {
                        msleep((info.timewait_after_trigger_us + 999)/1000);
                    }
                }

    	        DPRINTK("GENERIC_IRQ: @@@@ wait ipu int success\n");
        
                return 0;
             }
        }
        break;

        case IHAL_IOC_GENERIC_ENABLE_IRQ:
        {
            /* sanity checks */
            if(arg >= IPU_IRQ_COUNT)
            {
                return -EFAULT;
            }

            if(gp_generic_irq_array[arg] == NULL)
            {
                printk(KERN_ALERT "GENERIC_IRQ: enable irq failed, irq %d was not requested\n", (int)arg);
                return -EFAULT;
            }

            ipu_enable_irq(arg);
        }
        break;

        case IHAL_IOC_GENERIC_DISABLE_IRQ:
        {
            /* sanity checks */
            if(arg >= IPU_IRQ_COUNT)
            {
                return -EFAULT;
            }

            if(gp_generic_irq_array[arg] == NULL)
            {
                printk(KERN_ALERT "GENERIC_IRQ: disable irq failed, irq %d was not requested\n", (int)arg);
                return -EFAULT;
            }

            ipu_disable_irq(arg);
        }
        break;


        case IHAL_IOC_TEST_UNSELECT_BUF: // test_unselect_buffer
        {
            struct ih_sel_buff_info sel_buff_info;

            if(copy_from_user(&sel_buff_info, (void *)arg, sizeof(sel_buff_info)))
            {
                return -EFAULT;
            }

            ret_code = ipu_test_unselect_buffer(sel_buff_info.channel, sel_buff_info.type,
                                         sel_buff_info.bufNum);
        }
        break;

        case IHAL_IOC_TEST_ADC_ENABLE: // test_adc_enable
        {
            ipu_test_adc_enable(arg);
        }
        break;

        case IHAL_IOC_TEST_DI_ENABLE: // test_di_enable
        {
            ipu_test_di_enable(arg);
        }
        break;
    } // switch

    return ret_code;
}

/*==================================================================================*/

static int 
ihal_mmap
(
    struct file * file, 
    struct vm_area_struct * vma
)
{
    int clear_phys_info = 0;
    uint32_t off, len;

    if(vma->vm_pgoff > (~0UL >> PAGE_SHIFT))
    {
        printk("ihal: vma->pgoff failed\n");
        return -EINVAL;
    }

    off = vma->vm_pgoff << PAGE_SHIFT;

    if(ih_phys_size == 0 && ih_phys_addr == 0)
    {
        ih_phys_size = vma->vm_end - vma->vm_start;

        lock_kernel();

        /* frame buffer memory */
        if((ih_phys_addr = ipu_malloc(ih_phys_size)) == 0)
        {
            printk("ihal: ipu_malloc() failed\n");
            // restore ih_phys_size to 0, or next time allocation will fail.
            ih_phys_size = 0;
	    unlock_kernel();
            return -EINVAL;
        }

        unlock_kernel();
    }
    else
    {
        clear_phys_info = 1;
    }

    len = PAGE_ALIGN((ih_phys_addr & ~PAGE_MASK) + ih_phys_size);

    ih_phys_addr &= PAGE_MASK;
    if(ih_phys_size > len)
    {
        printk("ihal: size > len failed\n");
        return -EINVAL;
    }

    off += ih_phys_addr;
    vma->vm_pgoff = off >> PAGE_SHIFT;

    /* This is an IO map - tell maydump to skip this VMA */
    vma->vm_flags |= VM_IO | VM_RESERVED;

    if(io_remap_page_range(vma, vma->vm_start, off, ih_phys_size, pgprot_noncached(vma->vm_page_prot)))
    {
        printk("ihal: ioremap failed\n");
        return -EAGAIN;
    }

    if(clear_phys_info)
    {
        ih_phys_addr = ih_phys_size = 0;
    }

    return 0;
}

/*==================================================================================*/

static ssize_t
ihal_read
(
    struct file * file, 
    char __user * buf, 
    size_t fg_count, 
    loff_t * ppos
)
{
    struct ihal_dev * dev; /* device information */
    static int cat_count = 0;

    dev = file->private_data;


    printk(KERN_ALERT "!!! inside ihal easter albumin(%d, %d) !!!\n", MINOR(dev->cdev.dev), cat_count);

    switch(cat_count)
    {
        case 0:
            printk(KERN_ALERT "\tidmac_conf = 0x%08X, idmac_cha_en = 0x%08X\n" \
                              "\tidmac_cha_pri = 0x%08X, idmac_cha_busy = 0x%08X\n", 
                            readl(IDMAC_CONF), readl(IDMAC_CHA_EN),
                            readl(IDMAC_CHA_PRI), readl(IDMAC_CHA_BUSY));

            printk(KERN_ALERT "\tbuf0_rdy = 0x%08X, buf1_rdy = 0x%08X\n" \
                              "\ttstat = 0x%08X vers=%08X\n", 
                            readl(IPU_CHA_BUF0_RDY), readl(IPU_CHA_BUF1_RDY),
                            readl(IPU_TASKS_STAT), IHAL_VERSION);
        break;

        case 1:
            printk(KERN_ALERT "int_ctl1 = 0x%08X, int_ctl2 = 0x%08X\n" \
                              "int_ctl3 = 0x%08X, int_ctl4 = 0x%08X\n" \
                              "int_ctl5 = 0x%08X\n",  
                            readl(IPU_INT_CTRL_1), readl(IPU_INT_CTRL_2),
                            readl(IPU_INT_CTRL_3), readl(IPU_INT_CTRL_4),
                            readl(IPU_INT_CTRL_5));

            printk(KERN_ALERT "int_stat1 = 0x%08X, int_stat2 = 0x%08X\n" \
                              "int_stat3 = 0x%08X, int_stat4 = 0x%08X\n" \
                              "int_stat5 = 0x%08X\n",  
                            readl(IPU_INT_STAT_1), readl(IPU_INT_STAT_2),
                            readl(IPU_INT_STAT_3), readl(IPU_INT_STAT_4),
                            readl(IPU_INT_STAT_5));
        break;

        case 2:
            
            printk(KERN_ALERT "IPU_CONF = %08X\n", readl(IPU_CONF));
            printk(KERN_ALERT "IPU_CHA_BUF0_RDY = %08X\n", readl(IPU_CHA_BUF0_RDY));
            printk(KERN_ALERT "IPU_CHA_BUF1_RDY = %08X\n", readl(IPU_CHA_BUF1_RDY));
            printk(KERN_ALERT "IPU_CHA_DB_MODE_SEL = %08X\n", readl(IPU_CHA_DB_MODE_SEL));
            printk(KERN_ALERT " IPU_CHA_CUR_BUF= %08X\n", readl(IPU_CHA_CUR_BUF));
            printk(KERN_ALERT " IPU_FS_PROC_FLOW= %08X\n", readl(IPU_FS_PROC_FLOW));
            printk(KERN_ALERT " IPU_FS_DISP_FLOW= %08X\n", readl(IPU_FS_DISP_FLOW));
            printk(KERN_ALERT " IPU_TASKS_STAT= %08X\n", readl(IPU_TASKS_STAT));
            printk(KERN_ALERT " IPU_IMA_ADDR= %08X\n", readl(IPU_IMA_ADDR));
            printk(KERN_ALERT " IPU_IMA_DATA= %08X\n", readl(IPU_IMA_DATA));
            printk(KERN_ALERT " IPU_INT_CTRL_1= %08X\n", readl(IPU_INT_CTRL_1));
            printk(KERN_ALERT " IPU_INT_CTRL_2= %08X\n", readl(IPU_INT_CTRL_2));
            printk(KERN_ALERT " IPU_INT_CTRL_3= %08X\n", readl(IPU_INT_CTRL_3));
            printk(KERN_ALERT " IPU_INT_CTRL_4= %08X\n", readl(IPU_INT_CTRL_4));
            printk(KERN_ALERT " IPU_INT_CTRL_5= %08X\n", readl(IPU_INT_CTRL_5));
            printk(KERN_ALERT " IPU_INT_STAT_1= %08X\n", readl(IPU_INT_STAT_1));
            printk(KERN_ALERT " IPU_INT_STAT_2= %08X\n", readl(IPU_INT_STAT_2));
            printk(KERN_ALERT " IPU_INT_STAT_3= %08X\n", readl(IPU_INT_STAT_3));
            printk(KERN_ALERT " IPU_INT_STAT_4= %08X\n", readl(IPU_INT_STAT_4));
            printk(KERN_ALERT " IPU_INT_STAT_5= %08X\n", readl(IPU_INT_STAT_5));
            printk(KERN_ALERT " IPU_BRK_CTRL_1= %08X\n", readl(IPU_BRK_CTRL_1));
            printk(KERN_ALERT " IPU_BRK_CTRL_2= %08X\n", readl(IPU_BRK_CTRL_2));

            printk(KERN_ALERT " IC_CONF= %08X\n", readl(IC_CONF));
            printk(KERN_ALERT " IC_PRP_ENC_RSC= %08X\n", readl(IC_PRP_ENC_RSC));
            printk(KERN_ALERT " IC_PRP_VF_RSC= %08X\n", readl(IC_PRP_VF_RSC));
            printk(KERN_ALERT " IC_PP_RSC= %08X\n", readl(IC_PP_RSC));
            printk(KERN_ALERT " IC_CMBP_1= %08X\n", readl(IC_CMBP_1));
            printk(KERN_ALERT " IC_CMBP_2= %08X\n", readl(IC_CMBP_2));
            printk(KERN_ALERT " PF_CONF= %08X\n", readl(PF_CONF));
            printk(KERN_ALERT " IDMAC_CONF= %08X\n", readl(IDMAC_CONF));
            printk(KERN_ALERT " IDMAC_CHA_EN= %08X\n", readl(IDMAC_CHA_EN));
            printk(KERN_ALERT " IDMAC_CHA_PRI= %08X\n", readl(IDMAC_CHA_PRI));
            printk(KERN_ALERT " IDMAC_CHA_BUSY= %08X\n", readl(IDMAC_CHA_BUSY));


            printk(KERN_ALERT " SDC_COM_CONF= %08X\n", readl(SDC_COM_CONF));
            printk(KERN_ALERT " SDC_GW_CTRL= %08X\n", readl(SDC_GW_CTRL));
            printk(KERN_ALERT " SDC_FG_POS= %08X\n", readl(SDC_FG_POS));
            printk(KERN_ALERT " SDC_BG_POS= %08X\n", readl(SDC_BG_POS));
            printk(KERN_ALERT " SDC_CUR_POS= %08X\n", readl(SDC_CUR_POS));
            printk(KERN_ALERT " SDC_PWM_CTRL= %08X\n", readl(SDC_PWM_CTRL));
            printk(KERN_ALERT " SDC_CUR_MAP= %08X\n", readl(SDC_CUR_MAP));
            printk(KERN_ALERT " SDC_HOR_CONF= %08X\n", readl(SDC_HOR_CONF));
            printk(KERN_ALERT " SDC_VER_CONF= %08X\n", readl(SDC_VER_CONF));
            printk(KERN_ALERT " SDC_SHARP_CONF_1= %08X\n", readl(SDC_SHARP_CONF_1));
            printk(KERN_ALERT " SDC_SHARP_CONF_2= %08X\n", readl(SDC_SHARP_CONF_2));
            
            printk(KERN_ALERT " ADC_CONF = %08X\n", readl(ADC_CONF));
            printk(KERN_ALERT " ADC_SYSCHA1_SA= %08X\n", readl(ADC_SYSCHA1_SA));
            printk(KERN_ALERT " ADC_SYSCHA2_SA= %08X\n", readl(ADC_SYSCHA2_SA));
            printk(KERN_ALERT " ADC_PRPCHAN_SA= %08X\n", readl(ADC_PRPCHAN_SA));
            printk(KERN_ALERT " ADC_PPCHAN_SA= %08X\n", readl(ADC_PPCHAN_SA));
            printk(KERN_ALERT "ADC_DISP0_CONF = %08X\n", readl(ADC_DISP0_CONF));
            printk(KERN_ALERT "ADC_DISP0_RD_AP = %08X\n", readl(ADC_DISP0_RD_AP));
            printk(KERN_ALERT "ADC_DISP0_RDM = %08X\n", readl(ADC_DISP0_RDM));
            printk(KERN_ALERT "ADC_DISP0_SS = %08X\n", readl(ADC_DISP0_SS));
            printk(KERN_ALERT "ADC_DISP1_CONF = %08X\n", readl(ADC_DISP1_CONF));
            printk(KERN_ALERT "ADC_DISP1_RD_AP = %08X\n", readl(ADC_DISP1_RD_AP));
            printk(KERN_ALERT "ADC_DISP1_RDM = %08X\n", readl(ADC_DISP1_RDM));
            printk(KERN_ALERT "ADC_DISP12_SS = %08X\n", readl(ADC_DISP12_SS));
            printk(KERN_ALERT "ADC_DISP2_CONF = %08X\n", readl(ADC_DISP2_CONF));
            printk(KERN_ALERT "ADC_DISP2_RD_AP = %08X\n", readl(ADC_DISP2_RD_AP));
            printk(KERN_ALERT "ADC_DISP2_RDM = %08X\n", readl(ADC_DISP2_RDM));
            printk(KERN_ALERT "ADC_DISP_VSYNC = %08X\n", readl(ADC_DISP_VSYNC));

            printk(KERN_ALERT " DI_DISP_IF_CONF = %08X\n", readl(DI_DISP_IF_CONF));
            printk(KERN_ALERT " DI_DISP_SIG_POL = %08X\n", readl(DI_DISP_SIG_POL));
            printk(KERN_ALERT " DI_SER_DISP1_CONF= %08X\n", readl(DI_SER_DISP1_CONF));
            printk(KERN_ALERT " DI_SER_DISP2_CONF= %08X\n", readl(DI_SER_DISP2_CONF));
            printk(KERN_ALERT " DI_HSP_CLK_PER= %08X\n", readl(DI_HSP_CLK_PER));

            printk(KERN_ALERT "D0: Time_config 1,2,3 %08X, %08X, %08X\n",
                   readl(DI_DISP0_TIME_CONF_1), readl(DI_DISP0_TIME_CONF_2), readl(DI_DISP0_TIME_CONF_3));

            printk(KERN_ALERT "D1 Time_config 1,2,3 %08X, %08X, %08X\n",
                   readl(DI_DISP1_TIME_CONF_1), readl(DI_DISP1_TIME_CONF_2), readl(DI_DISP1_TIME_CONF_3));

            printk(KERN_ALERT "D2 Time_config 1,2,3 %08X, %08X, %08X\n",
                   readl(DI_DISP2_TIME_CONF_1), readl(DI_DISP2_TIME_CONF_2), readl(DI_DISP2_TIME_CONF_3));



            printk(KERN_ALERT "D0: CB_0,1,2 %08X, %08X, %08X\n", 
                   readl(DI_DISP0_CB0_MAP), readl(DI_DISP0_CB1_MAP),readl(DI_DISP0_CB2_MAP));
            printk(KERN_ALERT "D0: DB_0,1,2 %08X, %08X, %08X\n", 
                   readl(DI_DISP0_DB0_MAP), readl(DI_DISP0_DB1_MAP), readl(DI_DISP0_DB2_MAP));
            
            printk(KERN_ALERT "D1: CB_0,1,2 %08X, %08X, %08X\n", 
                   readl(DI_DISP1_CB0_MAP), readl(DI_DISP1_CB1_MAP),readl(DI_DISP1_CB2_MAP));
            printk(KERN_ALERT "D1: DB_0,1,2 %08X, %08X, %08X\n", 
                   readl(DI_DISP1_DB0_MAP), readl(DI_DISP1_DB1_MAP), readl(DI_DISP1_DB2_MAP));

            printk(KERN_ALERT "D2: CB_0,1,2 %08X, %08X, %08X\n", 
                   readl(DI_DISP2_CB0_MAP), readl(DI_DISP2_CB1_MAP),readl(DI_DISP2_CB2_MAP));
            printk(KERN_ALERT "D2: DB_0,1,2 %08X, %08X, %08X\n", 
                   readl(DI_DISP2_DB0_MAP), readl(DI_DISP2_DB1_MAP), readl(DI_DISP2_DB2_MAP));
                        
            printk(KERN_ALERT " DI_DISP_ACC_CC= %08X\n", readl(DI_DISP_ACC_CC));
            printk(KERN_ALERT " DI_DISP_LLA_CONF= %08X\n", readl(DI_DISP_LLA_CONF));
            printk(KERN_ALERT " DI_DISP_LLA_DATA= %08X\n", readl(DI_DISP_LLA_DATA));
            printk(KERN_ALERT " IDMAC_CHA_PRI= %08X\n", readl(IDMAC_CHA_PRI));
            printk(KERN_ALERT " IDMAC_CONF= %08X\n", readl(IDMAC_CONF));

/*
            printk(KERN_ALERT " Write some data to IP reg \n");
            writel(0x20510005, IPU_IMA_DATA);
            writel(0x00004000, IDMAC_CHA_EN);
            writel(0x00000211, SDC_COM_CONF);
            writel(0x00001000, DI_DISP_LLA_DATA);

            printk(KERN_ALERT " Read them back\n");
            printk(KERN_ALERT " IPU_IMA_DATA= %08X\n", readl(IPU_IMA_DATA));
            printk(KERN_ALERT " IDMAC_CHA_EN= %08X\n", readl(IDMAC_CHA_EN));
            printk(KERN_ALERT " SDC_COM_CONF= %08X\n", readl(SDC_COM_CONF));
            printk(KERN_ALERT " DI_DISP_LLA_DATA= %08X\n", readl(DI_DISP_LLA_DATA));

*/

  
        break;

        default:
            cat_count = 0;
            return 0;
    } // switch

    cat_count++;

    return 0;
}

/*==================================================================================*/
#ifdef SAVE_FOR_IHAL_DEBUG
static ssize_t
ihal_write
(
    struct file * file, 
    const char __user * buf, 
    size_t fg_count, 
    loff_t * ppos
)
{
    return fg_count;
}
#endif
/*==================================================================================*/

static int 
ihal_release
(
    struct inode * inode, 
    struct file * file
)
{
    struct ihal_dev * dev; /* device information */

    dev = file->private_data;

    return 0;
}

/*==================================================================================*/

static struct file_operations ihal_fops = 
{
    .owner =    THIS_MODULE,
    .read =     ihal_read,
    .write =    NULL,
    .ioctl =    ihal_ioctl,
    .mmap =     ihal_mmap,
    .open =     ihal_open,
    .release =  ihal_release,
};

/*==================================================================================*/

/*!
 * Main entry function for the ihal device.
 *
 * @return      Error code indicating success or failure
 */
int __init
ihal_init
(
    void
)
{
    char dev_name[IHAL_DEV_NAME_FMT_STR_LEN];
    int i, ret_code = 0;
    dev_t dev;

    /*
      Dynamically allocate a character device region with the kernel that is named 
      after our family of drivers. The kernel assigns the region a major code.
    */
    if((ret_code = alloc_chrdev_region(&dev, FIRST_IHAL_DEV, NUM_IHAL_DEV, "ihal")) < 0)
    {
        printk("unable to alloc chrdev region for ihal dev\n");
        mdelay(5000);
        return ret_code;
    }

    /*
      Initialize and add each ihal character device into the cdev framework. 
      Also create a /dev entry for each device.
    */
    for(i = FIRST_IHAL_DEV; i < NUM_IHAL_DEV; i++)
    {
        cdev_init(&ihal_devices[i].cdev, &ihal_fops);
        ihal_devices[i].cdev.owner = THIS_MODULE;
        ihal_devices[i].cdev.ops = &ihal_fops;

        dev = MKDEV(MAJOR(dev), i);

        if((ret_code = cdev_add(&ihal_devices[i].cdev, dev, 1)) < 0)
        {
            printk("ihal:unable to add cdev for major (%d)\n", MAJOR(dev));
            mdelay(5000);
            return ret_code;
        }

        /*
          Create a /dev entry to support reference by name.
        */
        sprintf(dev_name, IHAL_DEV_NAME_FMT_STR, i);
        devfs_mk_cdev(dev, S_IFCHR | S_IRUSR | S_IWUSR, dev_name);
    }

    /*
      Initialize our wait queues that are used by our sleeping threads 
      and interrupt handlers.
    */
    for(i = 0; i < num_ipu_irq_hdlr; i++)
    {
        init_waitqueue_head(&irq_q[i]);
    }
    ih_cam_initq();
    init_waitqueue_head(&vseof_irq_q);


    init_MUTEX(&pp_sema);


    return ret_code;
}

/*==================================================================================*/

/*!
 * Main exit function for the ihal device.
 *
 */
void 
ihal_exit
(
    void
)
{
    int i;
    char dev_name[IHAL_DEV_NAME_FMT_STR_LEN];

    dev_t dev = MKDEV(MAJOR(ihal_devices[FIRST_IHAL_DEV].cdev.dev), FIRST_IHAL_DEV);

    for(i = FIRST_IHAL_DEV; i < NUM_IHAL_DEV; i++)
    {
        sprintf(dev_name, IHAL_DEV_NAME_FMT_STR, i);
        devfs_remove(dev_name);

        cdev_del(&ihal_devices[i].cdev);
    }

    unregister_chrdev_region(dev, NUM_IHAL_DEV);
}

module_init(ihal_init);
module_exit(ihal_exit);

MODULE_AUTHOR("Motorola");
MODULE_DESCRIPTION("MXC IHAL driver");
MODULE_SUPPORTED_DEVICE("ihal");
MODULE_LICENSE("GPL");
