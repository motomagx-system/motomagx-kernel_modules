/*
 * Copyright (C) 2007-2008 Motorola, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  
 * 02111-1307, USA
 *
 * Motorola 2007-July 17 - Initial creation.
 * Motorola 2008-Feb  20 - OSS compliance
 */

/*==================================================================================================
                                         INCLUDE FILES
==================================================================================================*/

#include <asm/uaccess.h>
#include <asm/arch/gpio.h>
#include <asm/mot-gpio.h>
#include <asm/arch/mxc_i2c.h>
#include <linux/devfs_fs_kernel.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/proc_fs.h>

#include "fm_radio_driver_ext.h"

// #define FM_RADIO_DEBUG_PROC 0
// #define FM_RADIO_DEBUG 1
/*==================================================================================================
                                           CONSTANTS
==================================================================================================*/

#ifdef FM_RADIO_DEBUG
# define DEBUG_MSG(fmt, args...) printk( "fm_radio driver [DEBUG]: " fmt, ## args )
#else
# define DEBUG_MSG(fmt, args...)
#endif

/*! @brief The issued proc command, used for proc read */
typedef enum
{
	proc_I,	 /* initialize I2C adress */
	proc_W,  /* Write */
	proc_R,  /* Read */
	proc_T,  /* Test */
	proc_D,  /* Debug */ 
	proc_C,  /* Close */ 	
	proc_P,  /* Print */
	proc_B 	 /* Bus Speed */
} proc_command_T;



/*==================================================================================================
                                        LOCAL VARIABLES
==================================================================================================*/

/*! @brief Store the I2C address */
static uint8_t fm_radio_i2c_address = 0;

/*! @brief Store the device major number */
static int fm_radio_driver_module_major = 0;

/*! @brief Store the I2C register address used for proc interface */
static unsigned char fm_radio_proc_i2c;

/*! determines if we want to override the default busspeed */
static int fm_radio_bus_speed_override;

/*==================================================================================================
                                       GLOBAL VARIABLES
==================================================================================================*/

/*==================================================================================================
                          LOCAL TYPEDEFS (STRUCTURES, UNIONS, ENUMS)
==================================================================================================*/

typedef struct {
    uint8_t reg_addr;
	uint8_t length;
	uint8_t data1;
	uint8_t data2;
} FM_data;

#ifdef FM_RADIO_DEBUG_PROC

static proc_command_T proc_command;
static struct proc_dir_entry *proc_entry;

#define PROC_BUFFER_LEN 10000
char proc_buffer[PROC_BUFFER_LEN];
char* proc_buffer_ptr = proc_buffer;
/* TO BE REMOVED. ONLY FOR TESTING PROC INTERFACE */
static int test_proc_read_length;


#endif

/*==================================================================================================
                                   LOCAL FUNCTION PROTOTYPES
==================================================================================================*/

/*! @brief  Function prototype to detach the client when finished with communication */
static int client_detach(struct i2c_client *client);

/*! @brief  Function prototype to attach the client to begin communication */
static int adapter_attach (struct i2c_adapter *adap);

/*! @brief  Function prototypes for the fm device operations */
static int  fm_radio_driver_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg);
static int  fm_radio_driver_open(struct inode *inode, struct file *file);
static int  fm_radio_driver_close(struct inode *inode, struct file *file);
static int  fm_radio_driver_i2c_read (FM_data *data);
static int  fm_radio_driver_i2c_write(FM_data *data);
static int  fm_radio_driver_init_i2c_device(int i2c_addr);

#ifdef FM_RADIO_DEBUG_PROC
static int fm_radio_write_proc(struct file *filep, const char __user *buff, unsigned long len, void *data);
static int fm_radio_read_proc(char *buf, char **start, off_t pos, int count, int *eof, void *data);
#endif



/*==================================================================================================
                                          STRUCTURES
==================================================================================================*/

/*! @brief The I2C client structure obtained for communication with the FM IC Chip */
static struct i2c_client *fm_ic_i2c_client = NULL;

/*! @brief This structure defines the file operations for the fm radio device */
static const struct file_operations fmradio_fops =
{
    .owner   = THIS_MODULE,
    .ioctl   = fm_radio_driver_ioctl,
    .open    = fm_radio_driver_open,
    .release = fm_radio_driver_close,
};

/*! @brief Creation of the i2c_driver for FM RADIO */ 
static const struct i2c_driver driver = {
    name:           "FM RADIO I2C Driver",
    flags:          I2C_DF_NOTIFY,
    attach_adapter: adapter_attach,
    detach_client:  client_detach,
};

/*==================================================================================================
                                        LOCAL FUNCTIONS
==================================================================================================*/

#ifdef FM_RADIO_DEBUG_PROC
/* prints a formated string to the buffer */
void printf_buffer(char *fmt,...)
{
    char temp_buffer[1024];
	int nof_bytes;

	va_list ap;
    va_start(ap,fmt);	

    nof_bytes = vsprintf(temp_buffer, fmt, ap);

	if (nof_bytes > PROC_BUFFER_LEN - (proc_buffer_ptr + 1 - proc_buffer))
		return;
	else
        proc_buffer_ptr += vsprintf(proc_buffer_ptr, fmt, ap);

    va_end(ap);
}
#endif


 /*!
 * @brief Handles the addition of an I2C adapter capable of supporting the FM RADIO Chip 
 *
 * This function is called by the I2C driver when an adapter has been added to the
 * system that can support communications with the FM Radio Chip.  The function will
 * attempt to register a I2C client structure with the adapter so that communication
 * can start.  If the client is successfully registered, the FM radio initialization
 * function will be called to do any register writes required at power-up.
 *
 * @param    adap   A pointer to I2C adapter 
 *
 * @return   This function returns 0 if successful
 */
static int adapter_attach (struct i2c_adapter *adap)
{
    DEBUG_MSG("adapter_attach enter\n");
	
    int retval;

    /* Allocate memory for client structure */
    fm_ic_i2c_client = kmalloc(sizeof(struct i2c_client), GFP_KERNEL);
      
    if(fm_ic_i2c_client == NULL)
    {
        return -ENOMEM;
    }

    memset(fm_ic_i2c_client, 0, (sizeof(struct i2c_client)));
    
    /* Fill in the required fields */
    fm_ic_i2c_client->adapter = adap;
    fm_ic_i2c_client->addr = fm_radio_i2c_address;
    fm_ic_i2c_client->driver = (struct i2c_driver *)&driver;
    
    /* Register our client */
    retval = i2c_attach_client(fm_ic_i2c_client);
    
    if(retval != 0)
    {
        /* Request failed, free the memory that we allocated */
        kfree(fm_ic_i2c_client);
        fm_ic_i2c_client = NULL;
    }

	DEBUG_MSG("adapter_attach exit. retval: %d\n", retval);
    return retval;
}

 /*!
 * @brief Handles the removal of the I2C adapter being used for the FM RADIO Chip
 *
 * This function call is an indication that our I2C client is being disconnected
 * because the I2C adapter has been removed.  Calling the i2c_detach_client() will
 * make sure that the client is destroyed properly.
 *
 * @param   client   Pointer to the I2C client that is being disconnected
 *
 * @return  This function returns 0 if successful
 */
static int client_detach(struct i2c_client *client)
{
    DEBUG_MSG("client_detach enter and exit\n");
    return i2c_detach_client(client);
}
 
/*!
 * @brief Reads a specific FM RADIO Chip Register
 *
 * This function implements a read of a given FM register.  The read is done using i2c_transfer
 * which is a set of transactions.  The only transaction will be an I2C read which will read
 * the contents of the register of the FM Radio Chip.  
 *
 * @param reg_value  Pointer to which the read data should be stored
 *
 * @return This function returns 0 if successful
 */
static int fm_radio_driver_i2c_read (FM_data *data)
{
    DEBUG_MSG("fm_radio_driver_i2c_read enter\n");
    unsigned char read_data[2];
	unsigned char write_data[1];
    int retval;
	int len;
	int i;

    struct i2c_msg msgs[2] = 
    {
	    { fm_ic_i2c_client->addr, I2C_M_NOSTART, 1, write_data },
	    { fm_ic_i2c_client->addr, I2C_M_RD, 0, read_data },			
    };

    len = data->length;

    if (len != 1 && len != 2)
    {
        return -EINVAL;
    }
	
	msgs[1].len = len + 1;
    write_data[0] = data->reg_addr;


    /* Fail if we weren't able to initialize (yet) */
    if (fm_ic_i2c_client == NULL)
    {
        return -EINVAL;
    }

    retval = i2c_transfer(fm_ic_i2c_client->adapter,msgs,2);
    if (retval >= 0) 
    {
        if (len == 1) {
            data->data1 = read_data[0];
        } else if (len == 2) {
            data->data1 = read_data[0];
			data->data2 = read_data[1];
        }

       retval = 0;
    }
#ifdef FM_RADIO_DEBUG_PROC
    /* Debugging */
	printf_buffer("R[%02X]: ",write_data[0]);
    for (i=0; i< len;i++)
    {
        printf_buffer("%02X ",read_data[i]); 
    } 
    printf_buffer("\n");
#endif
    return retval;
}

 /*!
 * @brief Writes a value to a specified FM Radio register
 *
 * This function implements a write to a specified FM Radio register.  The write is accomplished
 * by sending the new contents to the i2c function i2c_master_send.
 *
 * @param reg_value  Register value to write
 *
 * @return This function returns 0 if successful
 */
static int fm_radio_driver_i2c_write(FM_data *data)
{
    DEBUG_MSG("fm_radio_driver_i2c_write enter\n");

    unsigned char write_data[3];
	int ret;
	int i;

    struct i2c_msg msgs[1] = 
    {
	{ fm_ic_i2c_client->addr, 0, 0, write_data },		
    };

    if (fm_radio_bus_speed_override == 1)
	msgs[0].flags = I2C_M_BUS_SPEED_OVERRIDE;

    /* Fail if we weren't able to initialize */
    if(fm_ic_i2c_client == NULL)
    {
        return -EINVAL;
    }

    /* write one byte */
    if (data->length == 1) {
        write_data[0] = data->reg_addr;
	write_data[1] = data->data1;
#ifdef FM_RADIO_DEBUG_PROC
	/* DEBUG PURPOSES */
	printf_buffer("W[%02X]: ",write_data[0]);
	for(i=1; i<2; i++)
    	{   
	    printf_buffer("%02X ",write_data[i]); 
    	} 
	printf_buffer("\n");
#endif
	msgs[0].len = 2;
	ret = i2c_transfer(fm_ic_i2c_client->adapter,msgs,1);
	return (ret < 0) ? ret : 0;

		/* write two bytes after a byte swap */	
    } else if (data->length == 2) {
        write_data[0] = data->reg_addr;
	write_data[1] = data->data1;
	write_data[2] = data->data2;
		
#ifdef FM_RADIO_DEBUG_PROC
	/*DEBUG PURPOSES */
	printf_buffer("W[%02X]: ",write_data[0]);
	for(i=1; i<3; i++)
	{   
	   printf_buffer("%02X ",write_data[i]); 
	} 
	printf_buffer("\n");
#endif
	msgs[0].len = 3;
	ret = i2c_transfer(fm_ic_i2c_client->adapter,msgs,1);		

	return (ret < 0) ? ret : 0;        
        //return (i2c_master_send(fm_ic_i2c_client,write_data,3 ));
    } else {	
	return -EINVAL;
    }	
}

/*!
 * @brief Initializes communication with the FM Radio
 *
 * This function performs any initialization required for the FM Radio Chip.  It
 * starts the process mentioned above for initiating I2C communication with the FM Radio
 * Chip by registering the I2C driver.  The rest of the process is handled
 * through the callback function adapter_attach().
 *
 * @param reg_init_fcn  Pointer to a function to call when communications are available
 */
static int fm_radio_driver_init_i2c_device(int i2c_addr)
{
    DEBUG_MSG("fm_radio_driver_init_i2c_device enter\n");

    int res;

    if (fm_ic_i2c_client == NULL)
    {
        /* Set the I2C addr of device */
        fm_radio_i2c_address = (uint8_t)i2c_addr;

        /* Register our driver */
		if ((res = i2c_add_driver((struct i2c_driver *)&driver)) < 0) {
            return res;
		};    
    }
    else if (fm_radio_i2c_address != (uint8_t) i2c_addr )
    {
        // Does not support multiple I2C devices 
        return -EFAULT;
    }

    return 0;
}

/*!
 * @brief the ioctl() handler for the fm radio device node
 *
 * This function implements the ioctl() system call for the fm radio ic
 * The command is handled directly by calling the appropriate function
 *
 * @param        inode       inode pointer
 * @param        file        file pointer
 * @param        cmd         the ioctl() command
 * @param        arg         the ioctl() argument
 *
 * @return 0 if successful
 */
static int fm_radio_driver_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg)
{
    DEBUG_MSG("fm_radio_driver_ioctl enter\n");

    int error;
    FM_data temp_data;

    /* Handle the request. */
    switch(cmd)
    {
        case FM_RADIO_IOCTL_SET_I2C_ADDR:
            {
                DEBUG_MSG("FM_RADIO_DRIVER IOCTL: FM_RADIO_IOCTL_SET_I2C_ADDR\n");
                return (fm_radio_driver_init_i2c_device(arg)); 
            }
            break;
                          
        case FM_RADIO_IOCTL_CLOSE_I2C:
            {
                DEBUG_MSG("FM_RADIO_DRIVER IOCTL: FM_RADIO_IOCTL_CLOSE_I2C\n");
		fm_ic_i2c_client = NULL;
                return (i2c_del_driver(&driver)); 
            }
            break;

        case FM_RADIO_IOCTL_READ_I2C:
            DEBUG_MSG("FM_RADIO_DRIVER IOCTL: FM_RADIO_IOCTL_READ_I2C\n");
            if (copy_from_user((void*)&temp_data, (void*)arg, sizeof(temp_data)) != 0)
            {
                DEBUG_MSG("FM_RADIO_DRIVER IOCTL: READ FAILURE\n");
                return -EFAULT;
            }

            error = fm_radio_driver_i2c_read(&temp_data);
            if (error)
            {
                DEBUG_MSG("FM_RADIO_DRIVER IOCTL: READ I2C FAILURE %d\n", error);
                return error;
            }
            
            /* Return the read value back to the caller */
            return copy_to_user((void*)arg, (void*)&temp_data, sizeof(temp_data)) ? -EFAULT : 0;

            break;

        case FM_RADIO_IOCTL_WRITE_I2C:
            DEBUG_MSG("FM_RADIO_DRIVER IOCTL: FM_RADIO_IOCTL_WRITE_I2C\n");
            if (copy_from_user((void*)&temp_data, (void*)arg, sizeof(temp_data)) != 0)
            {
                DEBUG_MSG("FM_RADIO_DRIVER IOCTL: WRITE FAILURE\n");
                return -EFAULT;
            }

            error = fm_radio_driver_i2c_write(&temp_data);
            if (error)
            {
                DEBUG_MSG("FM_RADIO_DRIVER IOCTL: I2C WRITE FAILURE %d \n",error);
                return error;
            }
            break;    
	  
        case FM_RADIO_IOCTL_BUS_SPEED_OVERRIDE:
            DEBUG_MSG("FM_RADIO_DRIVER IOCTL: FM_RADIO_IOCTL_BUS_SPEED_OVERRIDE\n");
            fm_radio_bus_speed_override = (int) arg;
	    return 0;
            break;	    
	    
        default: /* This shouldn't be able to happen, but just in case... */
            DEBUG_MSG("FM_RADIO_DRIVER IOCTL: default\n");
            return -ENOTTY;
            break;
    
    }
    return 0;
}

/*!
 * @brief the open() handler for the fm radio device node
 *
 * @param        inode       inode pointer
 * @param        file        file pointer
 *
 * @return 0 if successful
 */
static int fm_radio_driver_open(struct inode *inode, struct file *file)
{
    DEBUG_MSG("fm_radio_driver_open enter\n");

    /* Configure and enable the UH2_SPEED GPIO to powerup the FM RADIO IC */
    //gpio_config(GPIO_SP_A_PORT,9, GPIO_GDIR_OUTPUT, GPIO_DATA_HIGH);
    //gpio_set_data(GPIO_SP_A_PORT,9,GPIO_DATA_HIGH);
#ifdef FM_RADIO_DEBUG_PROC
    printf_buffer("fm_radio_driver_open\n");
 #endif
    return 0;
}

/*!
 * @brief the close() handler for the fm radio device node
 *
 * @param        inode       inode pointer
 * @param        file        file pointer
 *
 * @return 0 if successful
 */
static int fm_radio_driver_close(struct inode *inode, struct file *file)
{
    DEBUG_MSG("fm_radio_driver_close enter\n");

    /* Configure and disable the UH2_SPEED GPIO to power off the FM RADIO IC */
    //gpio_config(GPIO_SP_A_PORT,9, GPIO_GDIR_OUTPUT, GPIO_DATA_HIGH);
    //gpio_set_data(GPIO_SP_A_PORT,9,GPIO_DATA_LOW);   
#ifdef FM_RADIO_DEBUG_PROC
    printf_buffer("fm_radio_driver_close\n");
#endif
    return 0;
}

/*!
 * @brief FM Radio IC initialization function
 *
 * This function implements the initialization function of the fm radio
 * driver.  
 *
 * @return 0 if successful
 */
int __init fm_radio_driver_init(void)
{
    DEBUG_MSG("fm_radio_driver_init enter\n");

#ifdef FM_RADIO_DEBUG_PROC
    printf_buffer("fm_radio_driver_init\n");
#endif
    
    fm_radio_driver_module_major = register_chrdev(0, FM_RADIO_DEV_NAME,(struct file_operations *)&fmradio_fops);

    if (fm_radio_driver_module_major < 0)
    {
        return fm_radio_driver_module_major;
    }

    devfs_mk_cdev(MKDEV(fm_radio_driver_module_major,0), S_IFCHR | S_IRUGO |  S_IWUGO, FM_RADIO_DEV_NAME);

#ifdef FM_RADIO_DEBUG_PROC

    proc_entry = create_proc_entry("fm_radio",0644,NULL);

	if (proc_entry == NULL) {
        printk(KERN_INFO "fm_radio_driver_init: could not create proc entry\n");
		return -ENOMEM;
	} else {
	    proc_entry->read_proc = fm_radio_read_proc;
		proc_entry->write_proc = fm_radio_write_proc;
		proc_entry->owner = THIS_MODULE;
	}

#endif

    return(0);
}

/*!
 * @brief FM radio cleanup function
 *
 * This function is called when the fm radio driver is closed. Future change will
 * need to be made to free the fm_ic_i2c_client.
 */
void __exit fm_radio_driver_exit(void)
{
    DEBUG_MSG("fm_radio_driver_exit enter\n");

    int res;
#ifdef FM_RADIO_DEBUG_PROC
    remove_proc_entry("fm_radio",&proc_root);
#endif
    devfs_remove(FM_RADIO_DEV_NAME);    
    res = unregister_chrdev(fm_radio_driver_module_major, FM_RADIO_DEV_NAME);
    if (res < 0)
        printk(KERN_INFO "fm_radio_driver_exit: could not unregister char driver\n");

}

#ifdef FM_RADIO_DEBUG_PROC

// the 8 variables below are only used for test 
int temp_c1;
int temp_c2;
int temp_c3;
int temp_c4;

unsigned char c1;
uint8_t c2;
int c3;
uint8_t c4;


/*!
 * @brief FM radio proc interface write function
 *
 * This function does initiates a read or a write depending on the given arguments.
    The argument formats are as follows:
    "I   addr" (set I2C address)
    "R  addr len" (I2C read)
    "W addr len data1 [data2]" (I2C write)
    "T" (test)
    "D" (debug: write out debug buffer)
    "C" (close down I2C interface)
    "P" (print string)
  *
  * This function is called in response to the echo "..." > /proc/fm_radio command
 */
static int fm_radio_write_proc(struct file *filep, const char __user *buff, unsigned long len, void *data)
{
    int error;
    FM_data temp_data;
    unsigned char command;
    uint8_t temp_fm_radio_i2c_address = 0;
    int temp1,temp2,temp3;
 
	sscanf(buff,"%c",&command);

	switch (command) {
		case 'I':
			proc_command = proc_I;
		    sscanf(buff,"%*c %X",&temp1);
			temp_fm_radio_i2c_address = (uint8_t) temp1;
			printf_buffer("fm_radio_write_proc(I): temp_fm_radio_i2c_address = %02X\n",temp_fm_radio_i2c_address); 
			error = fm_radio_driver_init_i2c_device(temp_fm_radio_i2c_address);
			printf_buffer("fm_radio_write_proc(I): error = %02X\n",error); 
			if (error)
            {
                return error;
            }
			break;
			
        case 'W':
			proc_command = proc_W;
		    sscanf(buff,"%*c %*X %i",&temp1);
			temp_data.length = (uint8_t)temp1;
			
			if (temp_data.length == 1) {
                sscanf(buff,"%*c %2X %*i %2X",&temp1,&temp2);

				temp_data.reg_addr = (uint8_t)temp1;
				temp_data.data1 = (uint8_t)temp2;

				printf_buffer("fm_radio_write_proc: temp_data.reg_addr = %02X\n",temp_data.reg_addr); 
				printf_buffer("fm_radio_write_proc: temp_data.data1 = %02X\n",temp_data.data1); 
					
			} else if (temp_data.length == 2) {
                sscanf(buff,"%*c %2X %*i %2X %2X",&temp1,&temp2,&temp3);

				temp_data.reg_addr = (uint8_t)temp1;
				temp_data.data1 = (uint8_t)temp2;
				temp_data.data2 = (uint8_t)temp3;

				printf_buffer("fm_radio_write_proc: temp_data.reg_addr = %02X\n",temp_data.reg_addr); 
				printf_buffer("fm_radio_write_proc: temp_data.data1 = %02X\n",temp_data.data1); 
				printf_buffer("fm_radio_write_proc: temp_data.data2 = %02X\n",temp_data.data2); 
			};

		    error = fm_radio_driver_i2c_write(&temp_data);
            if (error)
            {
                return error;
            }
			break;

	    case 'R':
			sscanf(buff,"%*c %X %i",&temp1,&test_proc_read_length);	
			fm_radio_proc_i2c = (uint8_t)temp1;
			temp_data.reg_addr = fm_radio_proc_i2c;
			temp_data.length = test_proc_read_length;
			
			printf_buffer("fm_radio_write_proc: fm_radio_proc_i2c = %02X\n",fm_radio_proc_i2c);
			error = fm_radio_driver_i2c_read(&temp_data);
			if (error)
			{
			   return error;
			}  
			proc_command = proc_R;

			break;

		case 'T':
			// test that we can skip input items and write directly to bytes
			sscanf(buff,"%c %*X %X %*X %i %*X %X",&temp_c1,&temp_c2,&temp_c3,&temp_c4);
                        c1 = (unsigned char) temp_c1;
			c2 = (uint8_t) temp_c1;
			c4 = (int) temp_c1;
			c1 = (uint8_t) temp_c1;
			printf_buffer("fm_radio_write_proc: c1 = %02X, c2 = %02X, c3 = %i, c4 = %02X\n",c1,c2,c3,c4);
                        proc_command = proc_T;
			break;

		case 'D':
			// Initiate debug mode; starts to put debug info into a buffer
                        proc_command = proc_D;
			break;

		case 'C':
			// Close down I2C interface
                        proc_command = proc_C;
			fm_ic_i2c_client = NULL;
			error = i2c_del_driver(&driver);
			if (error)
                        {
                            return error;
                        }
			break;

		case 'P':
			// Initiate debug mode; starts to put debug info into a buffer
                        proc_command = proc_P;
			printf_buffer("%.*s",len - 2,buff + 2);
			break;
			
                case 'B':
                       // Set Bus speed to either 100KHz or 400KHz
		       proc_command = proc_B;
		       break;

		default:
			break;
    }

    return len;
	
}



/*!
 * @brief FM radio proc interface read function
 *
 * This function does an I2C read and returns the two bytes read. The I2C register address
 * needs to be defined beforehand in the proc interface write function.
 *
 * This function is run in response to the 'cat' command
 */
static int fm_radio_read_proc(char *buf, char **start, off_t pos, int count, int *eof, void *data)
{
    int error;
    FM_data temp_data;
    char    *p = buf;
	char* ptr;
	int i;

    switch (proc_command) {

		case proc_I:
			 p += sprintf(p, "fm_radio_read_proc proc_I I2C address:[0x%2X]\n",fm_radio_i2c_address);
			 break;

	    case proc_R:

			temp_data.reg_addr = fm_radio_proc_i2c;
			temp_data.length = test_proc_read_length;
			
			p += sprintf(p, "fm_radio_read_proc: proc_R: temp_data.reg_addr = [%2X]\n", temp_data.reg_addr);
			
			error = fm_radio_driver_i2c_read(&temp_data);
			if (error)
			{
			   return error;
			}  
			
			if (temp_data.length == 1) {
				p += sprintf(p, "fm_radio_read_proc: I2C proc_R: temp_data.data1 = [0x%2X]\n", temp_data.data1);
			} else if (temp_data.length == 2) {
				p += sprintf(p, "fm_radio_read_proc: I2C proc_R: temp_data.datax = [0x%2X,0x%2X]\n", temp_data.data1,temp_data.data2);
			}
			break;

		case proc_T:
            p += sprintf(p, "fm_radio_read_proc proc_T c1=%c c2=%02X c3=%i c4=%X\n",c1,c2,c3,c4);
			break;


		case proc_D:
			i = 0;
			for (ptr = proc_buffer;ptr < proc_buffer_ptr; ptr++) {
                p += sprintf(p, "%c",proc_buffer[i++]);
			}
			proc_buffer_ptr = proc_buffer;
			
			break;        

		default:
			p += sprintf(p, "fm_radio_read_proc: Nothing to do for proc_command = %d]\n", proc_command);
			break;        
    }

    return (p-buf);
	
}
#endif


/*
 * Module entry points
 */
module_init(fm_radio_driver_init);
module_exit(fm_radio_driver_exit);

MODULE_DESCRIPTION("FM Radio device driver");
MODULE_AUTHOR("Jan Oerbech Pedersen & Peter Kjaer, Motorola Inc.");
MODULE_LICENSE("GPL");


