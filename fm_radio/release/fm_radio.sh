#!/bin/sh
#
#       Copyright (c) 2006 Motorola, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
#==============================================================================
#
#   File Name: fm_radio.sh
#
#   General Description: This file executes the startup and shutdown procedures
#   for the FM Radio Device Driver.
#
#==============================================================================

INSMOD="/sbin/insmod"
RMMOD="/sbin/rmmod"
M="/lib/modules"
FMRADIO="$M/fm_radio.ko"
NODES="fm_radio"

insert () {
    ko_name=$1

    $INSMOD $ko_name
    if [ $? -ne 0 ]; then
	echo "Could not insmod $ko_name module\n"
	exit 1
    fi
}

makenod () {
    drv_name=$1

	NUM=`grep $drv_name /proc/devices | sed "s/^ [ ]*//" | cut -d\  -f 1`
	if [ "$NUM" = "" ]; then
	    echo "Device $drv_name not in /proc/devices"
	    exit 1
	fi
	mknod -m 666 /dev/$drv_name c $NUM 0
}

remove () {
    ko_name=$1
    $RMMOD $ko_name 2> /dev/null
}

startup () {
    echo -n "Starting FM Radio Device Driver..."
    insert $FMRADIO
    for i in $NODES
      do
      makenod $i
    done
    echo "OK"
}

shutdown () {
    echo -n "Stopping FM Radio Device Driver..."
    for i in $NODES
      do
       rm -f /dev/$i 2> /dev/null
    done
    remove $FMRADIO

    echo "OK"
}

restart () {
    shutdown
    startup
}

case "$1" in
    start)
        startup
    ;;
    stop)
        shutdown
    ;;
    restart)
        restart
    ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
esac
