/*
 * Copyright � 2007-2008, Motorola, All Rights Reserved.
 *
 * This program is licensed under a BSD license with the following terms:
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted
 * provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this list of conditions
 *   and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this list of
 *   conditions and the following disclaimer in the documentation and/or other materials provided
 *   with the distribution.
 * - Neither the name of Motorola nor the names of its contributors may be used to endorse or
 *   promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 * OF SUCH DAMAGE.
 *
 * Motorola 2007-July-17 - Initial Creation
 * Motorola 2008-Feb -20 - OSS compliance
 */
#ifndef __FM_RADIO_DRIVER_EXT_H__
#define __FM_RADIO_DRIVER_EXT_H__

/*==================================================================================================
                                         INCLUDE FILES
==================================================================================================*/

#include <linux/types.h>

/*==================================================================================================
                                           CONSTANTS
==================================================================================================*/

/*! The name of the device in /dev. */
#define FM_RADIO_DEV_NAME "fm_radio"

/*! for overriding the bus speed
   This flag complements the ones for i2c_msg.flags */
#define I2C_M_BUS_SPEED_OVERRIDE   0x1000 

#ifndef DOXYGEN_SHOULD_SKIP_THIS 

/*!
 * @brief Reads the FM radio registers.
 *
 * This command reads the FM radio registers
 *
 * @note int is unused
 */
#define FM_RADIO_IOCTL_READ_I2C \
        _IOR(0, 0x0, uint16_t *)

/*!
 * @brief Writes to the FM radio registers
 *
 * This command writes to the FM radio registers
 *
 * @note int is unused
 */
#define FM_RADIO_IOCTL_WRITE_I2C \
        _IOW(0, 0x01, uint16_t *)

/*!
 * @brief Initializes the FM radio driver
 *
 * This command sends the I2C address of the FM radio 
 *
 * @note int is unused
 */
#define FM_RADIO_IOCTL_SET_I2C_ADDR \
        _IOW(0, 0x02, int) 


/*!
 * @brief Close down the FM radio driver
 *
 * This command closes down I2C device driver of the FM radio 
 *
 * @note int is unused
 */
#define FM_RADIO_IOCTL_CLOSE_I2C \
        _IO(0, 0x03) 


/*!
 * @brief I2C bus speed override
 *
 * This command sets the I2C bus speed by writing to the 
 *    IFDR register in the SCMA11. It enables override
 *
 * @note int is unused
 */
#define FM_RADIO_IOCTL_BUS_SPEED_OVERRIDE \
        _IOW(0, 0x04, int) 
		

#endif /* Doxygen */
#endif /* __FM_RADIO_EXT_DRIVER_H__ */
