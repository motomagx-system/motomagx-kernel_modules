/*
* This program is free software; you can redistribute it 
* and/or modify it under the terms of the GNU General 
* Public License as published by the Free Software       
* Foundation; either version 2 of the License, or (at      
* your option) any later version.  You should have        
* received a copy of the GNU General Public License  
* along with this program; if not, write to the Free        
* Software Foundation, Inc., 675 Mass Ave,                  
*  Cambridge, MA 02139, USA

* This Program is distributed in the hope that it 
* will be useful, but WITHOUT ANY WARRANTY;   
* without even the implied warranty of
* MERCHANTIBILITY or FITNESS FOR A          
* PARTICULAR PURPOSE.  See the GNU           
* General Public License for more details.

* Copyright � 2006, 2008 Motorola, Inc. 

* Date		Author	 Reference   Comment
* ----------    -------- ---------   ------------------
* 2006/12/11	Motorola             Create
* 2008/08/19    Motorola             Drop packet if eth0 interface is not available 
*/


/*
* We are limiting the data rate by delaying all the packets
* by a fixed amount of time(delay). It serves the purpose. 
*/

/* #define __KERNEL__ */
/* #define MODULE */

#include <linux/module.h> 
#include <linux/netdevice.h> 
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/in.h>

#define DEBUG_RLIM 1
  
#define RLIM_SIMPLEQ_HEAD(name, type)                         \
struct name {                                                 \
    struct type *sqh_first; /* first element */               \
    struct type **sqh_last; /* addr of last next element */   \
}

#define RLIM_SIMPLEQ_ENTRY(type)                              \
struct {                                                      \
    struct type *sqe_next; /* next element */                 \
}

#define	RLIM_SIMPLEQ_INIT(head) do {                          \
    (head)->sqh_first = NULL;                                 \
    (head)->sqh_last = &(head)->sqh_first;                    \
} while (/*CONSTCOND*/0)

#define RLIM_SIMPLEQ_INSERT_TAIL(head, elm, field) do {       \
    (elm)->field.sqe_next = NULL;                             \
    *(head)->sqh_last = (elm);                                \
    (head)->sqh_last = &(elm)->field.sqe_next;                \
} while (/*CONSTCOND*/0)

#define RLIM_SIMPLEQ_REMOVE_HEAD(head, elm, field) do {       \
    if (((head)->sqh_first = (elm)->field.sqe_next) == NULL)  \
        (head)->sqh_last = &(head)->sqh_first;                \
} while (/*CONSTCOND*/0)

#define	RLIM_SIMPLEQ_FIRST(head) ((head)->sqh_first)

#define	RLIM_SIMPLEQ_NEXT(elm, field) ((elm)->field.sqe_next)

#define UMA_CONTROL_PORT1 0x36B0 /* 14000 */
#define UMA_CONTROL_PORT2 0x36B1 
#define UMA_CONTROL_PORT3 0x36B2
#define UMA_CONTROL_PORT4 0x36B3

#define DEFAULT_DELAY_JIFF 10 /* Default delay in Jiffies */

extern unsigned long volatile jiffies;
/*
 * Delay in Jiffies
 */
static unsigned short int delay = DEFAULT_DELAY_JIFF; 

#if 0
unsigned short int
htons(unsigned short int x)
{
    return ((((x) >> 8) & 0xff) | (((x) & 0xff) <<8));
}
#endif

/* 
 * Delay-buffer. A queue.
 */
RLIM_SIMPLEQ_HEAD(_pkt_q_head, pkt_tim) pkt_q_head;


/*
 * netfilter hook function. We will filter the packets to be
 * delayed and return NF_QUEUE for all those packets. All 
 * other packets, which should not be delayed will be 
 * accepted (by returning NF_ACCEPT). Those packets for which
 * we returned NF_QUEUE will come to the queue handler,
 * which is registered in the module initialisation routine.
 */

unsigned int hook_func(unsigned int hooknum,
                       struct sk_buff **skb,
                       const struct net_device *in,
                       const struct net_device *out,
                       int (*okfn)(struct sk_buff *))
{
    struct sk_buff *sb = *skb;
    struct iphdr *iph = NULL;
    unsigned char *raw = NULL;
    struct net_device *eth0 = NULL;

#ifdef DEBUG_RLIM
    printk("rlim: Inside hook_func()\n");
#endif

    if (!sb )
    {
        goto __accept;
    }
    
    if (!(sb->nh.iph))
    {
        goto __accept;
    }
    
    if( sb->len < sizeof(struct iphdr)
        || sb->nh.iph->ihl * 4 < sizeof(struct iphdr))
    {
        goto __accept;
    }

    iph = sb->nh.iph;

    if (sb->h.raw) 
    {
    	raw = (unsigned char *)sb->h.raw + iph->ihl*4;
    }
    else
    {
        if (sb->nh.raw)
        {
            raw = sb->nh.raw + iph->ihl*4;
        }
    	else
        {
    	    raw = (unsigned char *)iph + iph->ihl*4;
        }
    }

    /*
     * Only the packets coming in through 'eth0' should be 
     * delayed. 
     */
    eth0 = dev_get_by_name("eth0");
    
    if (!eth0) /* No 'eth0' in the phone! */
    {
       	return NF_DROP;  /* if eth0 = 0 , packet should drop */ 
    } 
    else if (eth0->ifindex != in->ifindex) /* Filter 1 */
    {
#ifdef DEBUG_RLIM
        printk("rlim: Input device is not eth0. Bypassing the buffer...\n");
#endif
        goto __accept;
    }

    /*
     * Delay TCP packets only
     */
    if (iph->protocol == IPPROTO_TCP) /* Filter 2 */
    {
        struct tcphdr *th = NULL;
        unsigned short int sport; /* Source Port */

        th = (struct tcphdr *)raw;
        sport = th->source;

        /*
         * Delay the packet only if it is NOT a UMA control 
         * packet.
         */
        if (sport != htons(UMA_CONTROL_PORT1) && /* Filter 3 */
            sport != htons(UMA_CONTROL_PORT2) &&
            sport != htons(UMA_CONTROL_PORT3) &&
            sport != htons(UMA_CONTROL_PORT4))
        {
#ifdef DEBUG_RLIM			     
            printk("rlim: Queuing the packet...\n");
#endif
            goto __queue;
        }
        else
        {
#ifdef DEBUG_RLIM				
            printk("rlim: Port check failed. Bypassing the buffer...\n");
#endif
        }
        
    }
    
    else
    {
#ifdef DEBUG_RLIM			
        printk("rlim: Protocol check failed. Bypassing the buffer...\n");
#endif
    }
	
__accept:
    dev_put(eth0);
    return NF_ACCEPT;

    /*
     * Only the packets to be delayed will come here
     */
__queue:
    dev_put(eth0);
    return NF_QUEUE;
}

/*
 * Objects of this structure will be the nodes in the
 * delay-buffer queue. In other words, this structure
 * is used to keep track of the delayed(queued) 
 * packets' info.
 */
struct pkt_tim {
    struct sk_buff *skb;
    struct timer_list *tim;
    struct nf_info *info;

    RLIM_SIMPLEQ_ENTRY(pkt_tim) next;
};

/*
 * Function registed with the timer.
 */
void
rel_pkt(unsigned long info)
{
    struct pkt_tim *node = (struct pkt_tim *) info;

#ifdef DEBUG_RLIM
    printk("rlim: Inside rel_pkt()\n");
#endif

    nf_reinject(node->skb, node->info ,NF_ACCEPT);

    kfree(node->tim);
    RLIM_SIMPLEQ_REMOVE_HEAD(&pkt_q_head, node, next);
    kfree(node);
}

/*
 * Queue handler registered. All the packets, for which we
 * returned NF_QUEUE in the hook function will come to this
 * function. We'll delay them here (and maitain the delayed
 * packets' info in the delay-buffer). 
 */
int
enq_pkt (struct sk_buff *skb, struct nf_info *info, void *data)
{
    struct pkt_tim *p = NULL;

#ifdef DEBUG_RLIM
    printk("rlim: Inside enq_pkt()\n");
    printk(KERN_INFO "rlim: Module initialised with a packet delay of %hd jiffies.\n", delay);
#endif
    
    p = kmalloc (sizeof(struct pkt_tim), GFP_ATOMIC);
    if (p == NULL)
    {
        printk(KERN_CRIT "rlim: Could not allocate memory for the buffer. Dropping the packet...\n");
        nf_reinject(skb, info, NF_DROP);
        return -1;
    }

    p->info = info; /* This is needed to reinject the packet (when the timer fires) */
    p->skb = skb;
    p->tim = kmalloc(sizeof (struct timer_list), GFP_ATOMIC);
    if (p->tim == NULL)
    {
        printk(KERN_CRIT "rlim: Could not allocate memory for timer. Dropping the packet...\n");
        nf_reinject(skb, info ,NF_DROP);
        return -1;
    }
    init_timer(p->tim);
    p->tim->expires = jiffies + delay;
    p->tim->function = rel_pkt;
    p->tim->data = (unsigned long) p;
       
    RLIM_SIMPLEQ_INSERT_TAIL(&pkt_q_head, p, next);
    add_timer(p->tim);

    return 0;
}


/* 
 * Module Functions 
 */
static struct nf_hook_ops nf_rlim_ops 
  = { { NULL, NULL }, hook_func, THIS_MODULE, PF_INET, 
      NF_IP_LOCAL_IN, NF_IP_PRI_FILTER };

static int 
__init rlim_init(void)
{
    int err = -1;

    err = nf_register_hook(&nf_rlim_ops);
    if (err != 0)
    {
        printk(KERN_ERR "rlim: Error %d registering the hook function.\n",err);
        return err;
    }

    err = nf_register_queue_handler(PF_INET, enq_pkt, NULL);
    if (err != 0) 
    {
        printk(KERN_ERR "rlim: Could not register the queue handler. Error %d\n", err);
        return err;
    }

    RLIM_SIMPLEQ_INIT(&pkt_q_head);

    printk(KERN_INFO "rlim: Module initialised with a packet delay of %hd jiffies.\n", delay);

    return 0;
}

static void
__exit rlim_cleanup(void)
{
    struct pkt_tim *node =  NULL;
    
    nf_unregister_hook(&nf_rlim_ops);
    nf_unregister_queue_handler(PF_INET);

    /* 
     * The module is being removed. Do justice by
     * sending out all the packets we have in the
     * delay-buffer.
     */
    for(node = RLIM_SIMPLEQ_FIRST(&pkt_q_head);
        node != NULL; node = node->next.sqe_next) 
    {

        /* 
         * Delete the timer registered for this packet first.
         * NOTE: Chances of deleting an already deleted
         * timer? (This passed n+1 tests, though!)
         */
        if (!del_timer (node->tim))
        {
            printk(KERN_ERR "rlim: Error in deleting the timer\n");
        }
        rel_pkt((unsigned long) node);
    }
      
}

/*
 * 'delay' is a module parameter. 
 *
 * NOTE: An interface to change the delay
 * at run time (probable through /proc) would
 * be better (TBD). Even though we don't need
 * so much control(over delay) now.
 */
MODULE_PARM(delay, "h");

module_init(rlim_init);
module_exit(rlim_cleanup);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("The Rate Limiter Module");
MODULE_AUTHOR("Motorola");

